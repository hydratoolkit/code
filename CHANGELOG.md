================================================================================

Version 1.4.0

General:

o Updated documentation for Euler solver

o Revised material model documentation for material strength and equation
  of state models

Hybrid FVM/FEM Navier-Stokes Solver:

Euler Solver:

o Revised IC's that are consistent with general EOS interface

Cell-Centered Lagrangian Hydra:

o Overall cleanup of source files, make variables consistent with other
  parts of the Hydra Toolkit

o Revised screen output and global energy tallies

o Revised IC's that are consistent with general EOS interface

Material Models:

o Activated ideal gas, stiffened gas and Mie-Gruneisen EOS models

o Revised Prandtl-Reuss and Johnson-Cook material strength models with
  independent EOS interface

o Added Elastic material strength model

o Removal of legacy sound-speed, pressure evaluation and Prandtl-Reuss,
  Johnson-Cook interfaces

o gamma keyword deprecated for eos -- end keyword syntax

================================================================================

Version 1.3.0

General:

o Patch minor bug in creation of merged sidesets

Documentation:

o Added tutorial for model setup with Simmetrix SimModeler

o Cleanup output of boundary conditions, source terms to include the
  type (CONSTANT, TIME DEPENDENT, SPACE DEPENDENT, SPACE-TIME DEPENDENT,
  USER_DEFINED)

o Expand user-defined functions to set-based boundary conditions and
  source terms

o Cleanup material model interface to correct data values used for
  temperature/pressure dependent tabular properties.

o Converted to Trilinos 12.10.1 on TPL stack

o New C++ source code documentation in the documentation suite, updated
  installation, keyword, and V&V documentation.

o Revised binary packaging for Linux and MacOS ahead of future docker
  container distribution

Hybrid FVM/FEM Navier-Stokes Solver:

o Expanded user-defined functions for volumetric heat sources and
  boundary conditions with keyword documentation

o New gravity body force for buoyancy in variable density calculations
  where density is a function of pressure or temperature

o ALE + deforming mesh

o Revised glob file format for added precision

o Cleanup interface to access temperature/pressure dependent material
  properties -- particularly for variable density calculations

o Added nodal coordinate keyword input for specifying the hydrostatic
  pressure

o Revised documentation for user-defined functions, hydrostatic pressure

o Reverted to using 30 restart vectors as the default for PETSc GMRES and
  FGMRES solvers

================================================================================
Version 1.2.0

General:

o The CMake code, tpl and superbuild scripts have been cleaned up to
  simplify the build process, and to permit using Intel/MKL libraries
  with the newest Intel Parallel Studio XE Cluster Edition.  See the
  Hydra Toolkit code wiki (https://gitlab.com/hydratoolkit/code/wikis/home).

o Minimum required CMake version is now 3.5.1

o Bug fix for time-history name mangling for variables and MPI rank

o Patch parsing for print interval keyword (prti)

o Increase fan-in buffer size for Exodus-II fanin writer to improve
  performance on larger models

o The load_curve -- end syntax has been changed to table -- end.
  load_curve is now an alias for table, and similarly keyword blocks
  that used the lcid keyword has changed to table.  The legacy
  keywords may be used as aliases, but they are considered deprecated
  as of this release.  Please refer to the Hydra Keywords Manual
  for additional details.

o The merged_sideset -- end keyword block has been added to permit
  combining multiple sidesets in a complex model for use in surface output.
  This permits grouping sideset output for the output of quantities like
  surface forces, tractions, as well as distributions of surface
  quantities such as y+, y*, etc.

o Removed the restriction on using surface output with the serial filetype
  for plot output in the Exodus-II file format.  Although not recommended
  for large models, or large number of MPI ranks, the serial filetype now
  support surface output request.

o FronTier front-tracking interface presented in the toolkit again.
  http://www.ams.sunysb.edu/~chenx/FronTier++_Manual/index.html

Compressible Euler Solver:

o Fix error handling in block-based initial conditions to examine block
  Id's for valid blocks.

Hybrid FVM/FEM Navier-Stokes Solver:

o Patch time-step estimation to properly synchronize across processors.

o Fix minor bug in stable time-step/CFL calculation for the WEDGE-6
  element.

o Patch default label for wall-functions to be TWOLAYER.

o Fix robustness issues associated with coarse meshing between adjacent
  no-slip/no-penetration surfaces when using RANS models.  Also patches
  for coarse boundary layers when using the hybrid two-layer wall
  treatment.

o Revised time-history element location based on coordinate to select
  the minimum user element Id across processors in parallel when the
  time-history point resides in more than one processor bounding box.

o Patch parsing for initial_block -- end keyword block, error handling
  now examines block Id's, i.e., element classes, for valid blocks.
  Cleanup of general material keyword input for use with SimModeler
  GUI and Hydra Plugin.

o Block initial conditions now fully functional.

o Revised user-defined boundary condition and initial condition coding
  with simplified UserFunctions directory containing prototypical
  implementation.  Keyword documentation revised to explain the
  user-defined function keywords and implementation of a
  user-defined function for boundary conditions and initial
  conditions.

o Cleanup material evaluation, thermal wall functions to be consistent
  with TWOLAYER wall-function treatment.

o Consolidation and cleanup of code for k-epsilon models.

o New or revised output delegates:
  - Element viscosity, keywords - "elem viscosity"

o Cleanup initialization of transport equations for consistent control
  options.

Documentation:

o Minor typos cleaned up, added list of executables to installation manual.

o Revised material input keywords to support tabular and functional
  material properties.

o Cleanup source-level doxygen documentation, and make the source headers
  consistent.

Utilities:

o Fix usage text for norm executable

================================================================================

Version 1.1.0

General:

o Added user-defined set names and set Ids to output of Exodus-II files.
  User-defined names are preserved and appended with the user Id.
  All set labels now appear in the Sets and Blocks dialogue boxes in
  ParaView.

o Extended installation documentation to include a superbuild from the
  source code.

o Revised tutorial documentation with additional problems to demonstrate
  use of turbulence models, porous media approximation, material sets,
  etc.

Hybrid FVM/FEM Navier-Stokes Solver:

o Added the hybrid two-layer wall treatment for k-epsilon models in the
  hybrid FVM/FEM Navier-Stokes solver.  The hybrid two-layer treatment
  is now the default.  Use of the so-called "scalable" wall functions
  should be limited to high-Reynolds number meshes that don't resolve
  boundary layers.

o Expose conjugate heat transfer capabilities including the ability to
  use rigid element blocks in fluid-flow only problems

o Hide non-linear k-epsilon model until there is further testing
  and validation, and/or there is additional demand for the model.

o Expose generic surface chemistry for the hybrid FVM/FEM Navier-Stokes
  solver with associated output delegates.  Fixed update_energy_only
  keywords to permit frozen-flow fields with thermal analysis.

o Minor bug patch to print out correct maximum CFL on restart in the
  Hybrid FVM/FEM Navier-Stokes solver.

o Revised least-squares gradient estimation to improve boundary accuracy
  for the FVM-based solvers.  This includes changes to the edge-centered
  and element-centered least-squared operators.

o Reduced memory footprint in the flow solver associated with the gradient
  gradient estimators, and output delegates.

o Revised output delegates for the Hybrid FVM/FEM Navier-Stokes and
  multiphase solvers that corrects a number of issues related to the
  vorticity computation.

  - Q-criterion field output keyword is now "qcriterion", and "vginv2"
    has been deprecated.

================================================================================

Version 1.0.3

o Added --precheck as a command-line argument.  This performs all
  steps of a normal calculation, but does not do the actual solve.
  In parallel, sub-domain plot files are written to facilitate
  visualization of large-scale problems when filetype=distributed is
  specified.

o Exposed the standard, Realizable, and non-linear k-epsilon models
  with scalable wall functions

o Activated the following boundary conditions for the
  Hybrid FVM/FEM Navier-Stokes solver:
  - Mass flux inflow/outflow
  - Mass flow inflow/outflow
  - Volume flow inflow/outflow
  - P-V dependent pressure BC's
  - Traction boundary conditions
  - Nodal and surface displacement BC's for reactivated for ALE problems

o Cleanup output varaibles for the Hybrid FVM/FEM Navier-Stokes solver:
  - Q-criteria output now activated using the 'qcriteria' name,
    'vginv2' name is now deprecated.

o Coordinate-based node and element time history interface now
  permits specifying a point coordinate rather than user Id's for
  either node or element time-history output.

o Memory cleanup
  - Changes several memory allocations to be conditional based on
    feature options, e.g., interface sidesets and P-V dependent
    pressure BC's.

o Cleanup error handling, errors and warnings, termination process
  for true exceptions and soft (cumulative) errors.

o Revised mesh converters for consistency with improvde support for all
  element types.

o Patch to channel mesh utility to print out correct mesh bounding box.

o Mesh converters:
  - Exodus-II (.exo) to Fluent (.msh) mesh converter
  - Fluent (.msh) to Exodus-II (.exo) mesh converter

o Linux binary distribution now includes libquadmath for improved portability.

================================================================================

Version 1.0.2

o RCB set as default partitioner, ParMetis provided only for research
  purposes consistent with U. of Minnesota licensing.  Binary
  distributions to not include ParMetis.

  The Hydra Toolkit may be used with ParMETIS for load-balancing.
  However, in order to use ParMETIS with Hydra, users must meet the
  conditions associated with the Copyright & License for ParMETIS.  For
  commercial application, a license is required from the University of
  Minnesota.   Please contact

  Andrew Morrow
  University of Minnesota
  Office for Technology Commercialization
  E-Mail: amorrow@umn.edu
  Phone : +1 (612) 626-7283

  ParMETIS may be obtained from:

  http://glaros.dtc.umn.edu/gkhome/metis/parmetis/download 

o Permit y+ calculations as long as wall-normal distance BC's are in 
  place.   Permits y+ to be computed for ILES and all turbulence
  models.

o surfcoord output delegate added for sideset data to make
  plotting over lines in ParaView easier.

o Patch echo functions for mesh transformation

o Switch to qcriteria as the output keyword for Q-critera in the
  Hybrid FVM/FEM flow solver

================================================================================

Version 1.0.1

o Initial release of on-line documentation.

o Fixed buffer size in netcdf to be compatible with Exodus-II to
  improve I/O performance.

o Cleanup packaging for binary distribution

o Cleanup of superbuild functionality

o Reorganize mesh converters, placed in src/Utilities/converters
