#!/bin/tcsh

set TEST_PATH = /mnt/scratch/nightly

set HYDRA_PATH = $TEST_PATH/hydra

# === Hydra Testing ===

cd $HYDRA_PATH

echo "Freshen the code and test repositories"
# Freshen the code
if (-d code) then
  echo "Pulling code repository"
  cd code
  git pull
  cd ..
else
  echo "Cloning code repository"
  git clone git@gitlab.com:hydratoolkit/code.git
  cd code
  git pull
  cd ..
endif

# Freshen the tests
if (-d long_regression) then
  echo "Pulling long_regression repository"
  cd long_regression
  git pull
  cd ..
else
  echo "Cloning long_regression repository"
  git clone git@gitlab.com:hydratoolkit/long_regression.git
  cd long_regression
  git pull
  cd ..
endif

if (-d regression) then
  echo "Pulling regression repository"
  cd regression
  git pull
  cd ..
else
  echo "Cloning regression repository"
  git clone git@gitlab.com:hydratoolkit/regression.git
  cd regression
  git pull
  cd ..
endif

if (-d tutorials) then
  echo "Pulling tutorials repository"
  cd tutorials
  git pull
  cd ..
else
  echo "Cloning tutorials repository"
  git clone git@gitlab.com:hydratoolkit/tutorials.git
  cd tutorials
  git pull
  cd ..
endif

if (-d verification) then
  echo "Pulling verification repository"
  cd verification
  git pull
  cd ..
else
  echo "Cloning verification repository"
  git clone git@gitlab.com:hydratoolkit/verification.git
  cd verification
  git pull
  cd ..
endif

# Build
if (-d build) then
  cd build
  /bin/rm -rf CMakeCache.txt rtest Testing
else
  mkdir build
  cd build
endif

# Turn on build testing for 16-core machine
# By default, long, parallel tutorials and verification are off,
# all other tests are on
cmake \
  -D CMAKE_BUILD_TYPE:STRING=RELEASE \
  -D CMAKE_Fortran_COMPILER:PATH=mpif90 \
  -D CMAKE_C_COMPILER:PATH=mpicc \
  -D CMAKE_CXX_COMPILER:PATH=mpic++ \
  -D GFORTRAN_LIBRARY=/usr/lib/gcc/x86_64-linux-gnu/9/libgfortran.a \
  -D QUADMATH_LIBRARY=/usr/lib/gcc/x86_64-linux-gnu/9/libquadmath.a \
  -D BUILD_TESTING=ON \
  -D HYDRA_LONG_REGRESSION_TESTS=ON \
  -D HYDRA_PARALLEL_TUTORIALS_TESTS=ON \
  -D HYDRA_VERIFICATION_TESTS=ON \
  ../code/src  >& ../CMAKE_LOG

make clean

# ctest -j 4 -D Nightly -M Nightly
# ctest -j 8 -D Nightly -M Nightly
# Used with --bind-to none for mpirun
ctest -j 32 -D Nightly -M Nightly
# ctest -j 4 -L "Hydra Serial Regression" -D Nightly -M Nightly
# ctest -j 1 -L "Hydra Parallel Regression" -D Nightly -M Nightly
# ctest -j 4 -L "Hydra Serial Verification" -D Nightly -M Nightly
# ctest -j 1 -L "Hydra Parallel Verification" -D Nightly -M Nightly
