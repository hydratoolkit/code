#!/bin/csh
#MSUB -N 3x3
#MSUB -l walltime=4:00:00
#MSUB -l nodes=8:ppn=12
#MSUB -m ae
#MSUB -j oe
#MSUB -V
#MSUB -o TTY
#MSUB -M jbakosi@lanl.gov

# --bind-to-socket  : Bind processes to processor sockets
# --report-bindings : Report any bindings for launched processes
# -l nodes=8:ppn=12 : Request 8 nodes with using 12 cores per node.
# On a machine with 24 cores per node (e.g. mustang), this translates to
# running half-stream, i.e. using half of each socket.

# cd into dir where job was submitted
cd $PWD

mpirun -n $SLURM_NPROCS --bind-to-socket --report-bindings \
       /panfs/scratch3/vol10/jbakosi/code/hydra/install/gnu/bin/hydra \
       -i elmahdi-3x3_2M.exo -c elmahdi-3x3.cntl
