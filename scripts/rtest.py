#!/usr/bin/env python

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys, os, subprocess, shlex, signal, time, pwd, shutil
import cl_options
import datetime   # Get the date/time for dashboard updates
import urllib2    # For calling the php page on the server for dashboard updates
import socket     # Get host name for dashboard updates
import platform   # Get the operating system info for dashboard updates
import threading  # for threading tests
import time       # For timing tests
from numdiff import Numdiff

################################################################################
#
# Regression test harness
#
# Author: Mark A. Christon, Rob Lowrie, Nate Barnett
#
# Date  : 08/16/2011
#
################################################################################

_usage = '''Usage: %prog [options]

  Hydra Regression Test Driver

'''

# Set this to False if you want to skip the user prompt for --update-baseline.
# USE WITH CAUTION. DO NOT COMMIT TO REPOSITORY AS FALSE.
#######DEBUG
# updateBaselinePrompt = True
updateBaselinePrompt = False
#######DEBUG

# global threading lock
lock = threading.Lock()

class statCodes:
    '''
    Stores info about status codes for the various executables that are run.
    '''
    success    = 0 # everything worked
    hydraFail  = 1 # hydra failed
    exoFail    = 2 # exodiff failed
    statFail   = 3 # exodiff failed for stats
    globFail   = 4 # numdiff says glob files were different
    mesg = ['Success',
            'Hydra failed',
            'Exodus failed',
            'Exodus failed (on stats)',
            'Glob failed']
    count = len(mesg) # number of codes defined above

class bcolors:
    OKBOLDGREEN = '\033[92m'
    MESSAGE = '\033[0m'
    WARNING = '\033[94m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.OKBOLDGREEN = ''
        self.MESSAGE = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''
# end of class bcolors

class TestTimer(object):

    def __init__(self,verbose=False):
       self.verbose=verbose

    def __enter__(self):
       self.start=time.time()
       return self

    def __exit__(self,*args):
       self.end=time.time()
       self.interval=self.end-self.start
       if self.verbose:
           print 'Elapsed time: %.4f s\n' % self.interval
# end of TestTimer class

def system(c):
    '''
    Wrapper for system calls.  Use this instead of os.system() so that cntrl-C
    really kills this script.
    '''
    p = subprocess.Popen(c, shell=True)
    return p.wait()
# end of function system

class MpiCommand(object):

  def __init__(self,mpirun_exe='mpirun',
                    nproc_flag='-n',
                    mpirun_preflags=None,
                    mpirun_postflags=None):
    self.mpirun_exe=mpirun_exe
    self.nproc_flag=nproc_flag
    self.mpirun_preflags=mpirun_preflags
    self.mpirun_postflags=mpirun_postflags

  def build_command(self,nprocs,executable,args=None):
    cmd=[self.mpirun_exe,self.nproc_flag,str(nprocs)]
    if self.mpirun_preflags != None:
      cmd.append(self.mpirun_preflags)
    cmd.append(executable)
    if self.mpirun_postflags != None:
      cmd.append(self.mpirun_postflags)
    if args != None:
      cmd.append(args)
    return ' '.join(cmd)
# end of MpiCommand class

class OpenmpiCommand(MpiCommand):

  def build_command(self,nprocs,executable,args=None):
    cmd=[self.mpirun_exe,self.mpirun_preflags,self.nproc_flag,str(nprocs)]
    cmd.append(executable)
    if args != None:
      cmd.append(args)
    if self.mpirun_postflags != None:
      cmd.append(self.mpirun_postflags)
    return ' '.join(cmd)
# end of OpenmpiCommand class

class PoeMpiCommand(MpiCommand):

  def build_command(self,nprocs,executable,args=None):
    cmd=[self.mpirun_exe,executable]
    if args != None:
      cmd.append(args)
    cmd.append(self.nproc_flag)
    cmd.append(str(nprocs))
    if self.mpirun_postflags != None:
      cmd.append(self.mpirun_postflags)
    return ' '.join(cmd)
# end of PoeMpiCommand class

def SelectMpiInterface(type):
  print "SelectMpiInterface: type = %s " % type
  if type == 'openmpi':
      return OpenmpiCommand(mpirun_exe='mpirun', nproc_flag='-n',
        mpirun_preflags='--bind-to none')
# Old way
#     return OpenmpiCommand(mpirun_exe='mpirun', nproc_flag='-n',
#       mpirun_preflags='-mca opal_paffinity_alone 0 --map-by node')
#    return OpenmpiCommand(mpirun_exe='mpirun',nproc_flag='-n',
#      mpirun_preflags='--bind-to core --map-by core')
  elif type == 'mpich':
    return MpiCommand(mpirun_exe='mpiexec',nproc_flag='-np')
  elif type == 'cray':
    return MpiCommand(mpirun_exe='aprun',nproc_flag='-n')
  elif type == 'poe':
    return PoeMpiCommand(mpirun_exe='poe',nproc_flag='-procs')
  else:
    print 'MPI %s is not supported' % type
    raise
# end of SelectMpiInterface function

class CL_Options(cl_options.CL_Options):
    'Parses and stores command line options.'

    def __init__(self, usage):
        cl_options.CL_Options.__init__(self, usage, '.rtest')
        p = self.parser
        p.add_option('-s', '--suite', dest='suite', action='store',
                     default = None,
                     help='The XML suite file describing the tests. '
                     'If this option is not given, then suite must '
                     'be set in the config file [%default]')
        p.add_option('', '--hydra_exe', dest='hydra_exe', action='store',
                     default=None,
                     help='The hydra exectable name. Takes precedence over --build_dir and --install_dir [%default]')
        p.add_option('', '--exodiff_exe', dest='exodiff_exe', action='store',
                     default=None,
                     help='The exodiff exectable name. Takes precedence over --build_dir and --install_dir [%default]')
        p.add_option('-b', '--build_dir', dest='build_dir', action='store',
                     default = './',
                     help='May be used to form path to Hydra executables'
                     ' (hydra, exodiff, etc.).  This option takes precedence over --install_dir [%default]')
        p.add_option('-i', '--install_dir', dest='install_dir', action='store',
                     default = '/usr/local/gnu/bin',
                     help='May be used to form path to Hydra executables'
                     ' (hydra, exodiff, etc.).  This option is overridden by --build_dir [%default]')
        p.add_option('-t', '--test_dir', dest='test_dir', action='store',
                     default = './',
                     help='Path to Hydra test directory [%default]')
        p.add_option('-d', '--dump_dir', dest='dump_dir', action='store',
                     default = './rtest',
                     help='All output is written to this directory [%default]')
        p.add_option('-j', '--cpus', dest='cpus', type=int, action='store',
                     default=1,
                     help='Try to keep this many cpus active.  Each test'
                     ' is run on a separate thread. The sum of '
                     'Nproc across all threads is kept less'
                     ' than or equal to this value, except that at least '
                     'one test at time will always be run.  [%default]')
        p.add_option('-o', '--ostype', dest='ostype', action='store',
                     default="linux",
                     help='The name of the OS type we are running on: '
                          'i.e. linux, cray. '
                          '[%default]')
        p.add_option('-m', '--mpitype', dest='mpitype', action='store',
                     default="openmpi",
                     help='The name of the MPI type we are using: '
                          'i.e. openmpi, mpich, cray, poe. '
                          '[%default]')
        p.add_option('-n', '--nproc', dest='nproc', action='store',
                     default = None,
                     help='Number of processors to run each test. '
                     'Overrides the Nproc value set in the xml suite file '
                     '[%default]')
        p.add_option('-p', '--path', dest='path', action='store',
                     default = None,
                     help='Run the test located in this diretory (full path). '
                     'Must be equivalent to a "path" for a test in '
                     'the suite file. None means run all tests in '
                     'the suite file [%default]')
        p.add_option('', '--dashboard-url', dest='dashboardURL', action='store',
                     default = "None",
                     help='The results can be output as data to update a '
                          'web-based dashboard. This option points to the '
                          'URL that accepts the input. '
                          '[%default]')
        p.add_option('', '--dashboard-file', dest='dashboardFile', action='store',
                     default = "dashboard_results.xml",
                     help='The results can be output as an xml file that can then '
                          'be pushed to the dashboard server by hand. This is useful '
                          'in cases where a firewall prevents the server running the '
                          'regression from talking to the dashboard. '
                          '[%default]')
        p.add_option('-u', '--update-server', dest='sendToServer', action='store_true',
                     default=False,
                     help='The xml file of test results will be sent to the server '
                          'specified by option --dashboard-url. '
                          '[%default]')
        p.add_option('', '--update-baseline', dest='updateBaseline', action='store_true',
                     default=False,
                     help='If the baseline file does not exist for a test, prompt to see '
                          'if it should be created from the code output. '
                          'To avoid overlapping user prompts, this option requires --cpus=1. '
                          '[%default]')

    def parse(self):
        args = cl_options.CL_Options.parse(self)
        # convert paths to absolute paths
        cl.opts.test_dir = os.path.abspath(cl.opts.test_dir)
        cl.opts.build_dir = os.path.abspath(cl.opts.build_dir)
        cl.opts.install_dir = os.path.abspath(cl.opts.install_dir)
        # Find the hydra executable
        if cl.opts.hydra_exe is None:
            search_dirs = (cl.opts.build_dir + '/Main', cl.opts.install_dir)
            for d in search_dirs:
                f = d + '/hydra'
                if os.path.isfile(f):
                    cl.opts.hydra_exe = f
                    break
            if cl.opts.hydra_exe is None:
                self.parser.error('unable to find hydra executable in directories %s.'
                              ' See options --build_dir and --install_dir' % (`search_dirs`))
        elif not os.path.isfile(cl.opts.hydra_exe):
                self.parser.error('unable to find hydra executable %s.' % cl.opts.hydra_exe)
        # Find the exodiff executable
        if cl.opts.exodiff_exe is None:
            search_dirs = (cl.opts.build_dir + '/ThirdParty/exodiff', cl.opts.install_dir)
            for d in search_dirs:
                f = d + '/exodiff'
                if os.path.isfile(f):
                    cl.opts.exodiff_exe = f
                    break
            if cl.opts.exodiff_exe is None:
                self.parser.error('unable to find exodiff executable in directories %s.'
                              ' See options --build_dir and --install_dir' % (`search_dirs`))
        elif not os.path.isfile(cl.opts.exodiff_exe):
                self.parser.error('unable to find exodiff executable %s.' % cl.opts.exodiff_exe)
        # create and/or check dump dir
        if not os.path.isdir(cl.opts.dump_dir):
            print 'Creating directory %s ...' % cl.opts.dump_dir
            os.makedirs(cl.opts.dump_dir)
        self.check_dir('dump_dir')
        # check the remaining input
        self.check_dir('test_dir')
        if cl.opts.path is not None:
            self.check_dir('path')
        if cl.opts.suite is None:
            self.parser.error('suite file must be specifies (--suite)')
        self.check_file('suite')
        return args

    def check_dir(self, x):
        '''
        Checks option x to see if it points at an existing directory.
        '''
        d = getattr(self.opts, x)
        if not os.path.isdir(d):
            self.parser.error('ERROR: Directory %s does not exist.  Check or set --%s' % (d, x))
            sys.exit(1)

    def check_file(self, x):
        '''
        Checks option x to see if it points at an existing file.
        '''
        d = getattr(self.opts, x)
        if not os.path.isfile(d):
            self.parser.error('ERROR: File %s does not exist.  Check or set --%s' % (d, x))
            sys.exit(1)
# end of class CL_Options

class TestData:
    'Data for a single test.'
    def __init__(self):
        self.name         = '' # Name of test
        self.path         = '' # Path to test directory
        self.input        = '' # Input file name
        self.control      = '' # Control file name
        self.output       = '' # Output prefix for files
        self.baseline     = '' # Baseline output file prefix
        self.diff         = '' # Exodiff diff file
        self.Nproc        = '' # No. of processors for test
        self.author       = '' # Test author
        self.compare      = '' # comparison type
        self.restart      = '' # if defined, run this many steps, then restart and run to the end
        self.id           = 0  # ID number.  This is set in runAllTests()
        self.status       = 0  # status of the test, after running it (see statCodes)
        self.time         = 0  # test execution wall clock time in seconds
    # function helpers for various output file names
    def out_diffs(self):
        return self.output + '.diffs'
    def out_statdiffs(self):
        return self.output + '_stat.diffs'
    def out_conv(self):
        return self.output + '.conv'
    def out_dump(self):
        return self.output + '.dump'
    def out_execntl(self):
        return self.output + '.ecntl'
    def out_exo(self):
        return self.output + '.exo'
    def out_statexo(self):
        return self.output + '_stat.exo'
    def out_glob(self):
        return self.output + '.glob'
    def out_glob_diff(self):
        return self.output + '.glob_diff'
    def out_out(self):
        return self.output + '.out'
    def out_hist(self):
        return self.output + '_hist'
    def out_tty(self):
        return self.output + '.tty'
    def out_prev(self):
        # stores info from the previous hydra run
        return self.output + '.prev'
    def inst_diff(self):
        return self.diff + '.diff'
    def stat_diff(self):
        return self.diff + '_stat.diff'
    def glob_baseline(self):
        return self.baseline + '.glob'
    def inst_baseline(self):
        return self.baseline + '.exo'
    def stat_baseline(self):
        return self.baseline + '_stat.exo'
    def restart_text(self):
        if len(self.restart) > 0:
            return self.restart
        else:
            return 'No'
# end of class TestData

class TestParser(ContentHandler):
    'Parses a test suite using SAX parser'

    def __init__(self, opts):
        self.opts = opts

        self.regressionName = ""
        self.testParsers = []

        # Lists for traversing/firing tests
        self.testlist = []
        self.test = TestData()
        self.testAttr = None # the current attribute in TestData being parsed

    def startElement(self, name, attrs):
        self.testAttr = None
        if (name == "regression") :
            self.regressionName = attrs.get("name")
        elif (name == "metatest") :
            path = attrs.getValue("path")
            filename = attrs.getValue("name")
            import copy
            subopts = copy.deepcopy(self.opts)
            subopts.test_dir = self.opts.test_dir + path
            subopts.suite = self.opts.test_dir + path + "/" + filename
            if self.opts.dump_dir.endswith("/"):
                subopts.dump_dir = self.opts.dump_dir+filename
            else:
                subopts.dump_dir = self.opts.dump_dir+"/"+filename
            subtests = TestParser(subopts)
            saxparser = make_parser()
            saxparser.setContentHandler(subtests)
            datasource = open(subopts.suite, "r")
            saxparser.parse(datasource)
            self.testlist.extend(subtests.testlist)
            for st in subtests.testlist:
                st.metapath = path
                st.metaname = filename
                if attrs.has_key("Nproc"):
                    nproc = attrs.getValue("Nproc")
                    st.Nproc = nproc

        elif (name == "test") :
            self.test.name = attrs.get("name")
        elif hasattr(self.test, name):
            self.testAttr = name
        else:
            print 'ERROR: Unrecognized xml attribute: %s' % name
            sys.exit(1)

    def endElement(self, name) :

        if (name == "test"):

            # determine whether we are actually going to run this test
            doit = True
            if self.opts.path is not None:
                pcheck = self.opts.test_dir + self.test.path
                doit = (pcheck and os.path.samefile(pcheck, self.opts.path))

            if doit: # ... yes, we are going to run it
                # Overwrite OS type if specified on command line
                if self.opts.ostype is not None:
                    self.ostype = self.opts.ostype

                # each test may have individual opts so add them in here
                self.test.opts = self.opts

                # overwrite nproc if it was specified on the command line
                if self.opts.nproc is not None:
                  self.test.Nproc = self.opts.nproc

                # Store off the test attributes
                self.testlist.append(self.test)

            self.test = TestData()

        self.testAttr = None

    def characters(self, chars) :
        if self.testAttr is not None:
            v = getattr(self.test, self.testAttr)
            setattr(self.test, self.testAttr, v + chars)
# end of class TestParser

class TestFiles:
    '''
    Computes the full pathnames to all of the test files for a given test.
    '''
    def __init__(self, test, opts):
        self.in_path       = opts.test_dir + test.path
        self.out_path      = opts.dump_dir + test.path
        self.input         = self.in_path + test.input
        self.control       = self.in_path + test.control
        self.baseline      = self.in_path + 'baseline/' + test.inst_baseline()
        self.statbaseline  = self.in_path + 'baseline/' + test.stat_baseline()
        self.globbaseline  = self.in_path + 'baseline/' + test.glob_baseline()
        self.diff          = self.in_path + test.inst_diff()
        self.statdiff      = self.in_path + test.stat_diff()
        self.out_diffs     = self.out_path + test.out_diffs()
        self.out_statdiffs = self.out_path + test.out_statdiffs()
        self.out_conv      = self.out_path + test.out_conv()
        self.out_dump      = self.out_path + test.out_dump()
        self.out_ecntl     = self.out_path + test.out_execntl()
        self.out_exo       = self.out_path + test.out_exo()
        self.out_statexo   = self.out_path + test.out_statexo()
        self.out_glob      = self.out_path + test.out_glob()
        self.out_glob_diff = self.out_path + test.out_glob_diff()
        self.out_out       = self.out_path + test.out_out()
        self.out_tty       = self.out_path + test.out_tty()
        self.out_hist      = self.out_path + test.out_hist()
        self.out_prev      = self.out_path + test.out_prev()
# end of class TestFiles

class Totals:
    '''
    Used for running totals of failures
    '''
    def __init__(self):
        self.numTest = 0 # number of tests run so far
        self.numFail = 0 # number of those tests that failed in some way
# end of class Totals

def threadPrint(s):
    'Prints string from within a thread, ensuring clean output using locks.'
    lock.acquire()
    try:
        sys.stdout.write(s)
        sys.stdout.flush()
    finally:
        lock.release()

def traverseList(tests):
    'Traverse the test list and print the tests to be run'

    for t in tests.testlist:
        print ''
        print 'Test name                    : %s' % t.name
        print 'Test path                    : %s' % t.path
        print 'Input file                   : %s' % t.input
        print 'Control file                 : %s' % t.control
        print 'Output prefix                : %s' % t.output
        print 'Baseline prefix              : %s' % t.baseline
        print 'Exodiff file                 : %s' % t.diff
        print 'No. of proc.                 : %s' % t.Nproc
        print 'Author                       : %s' % t.author
        print 'Restart                      : %s' % t.restart_text()
# end of function traverseList

def sumStatCodes(tests):
    '''
    Returns a vector of length statCodes.count, with the number of tests that
    have each status code.
    '''
    r = [0] * statCodes.count # return value
    for t in tests.testlist:
        r[t.status] += 1
    failures = 0
    for i in range(1, statCodes.count):
        failures += r[i]
    return (failures, r)
# end of function sumStatCodes

def checkTestStatus(tests):
    '''
    Generate test summary and new xml file with failed tests for re-run
    '''
    test_dir = tests.opts.test_dir

    # Sum failures
    ntests  = len(tests.testlist)
    (failures, sums) = sumStatCodes(tests)

    # For exodiff stuff, sum up the results for now
    exoFail = sums[statCodes.exoFail] + sums[statCodes.statFail]

    # Sum total wall clock time
    ttime=0
    for t in tests.testlist:
      ttime+=t.time

    print ''
    print '*** Test Summary ***'
    print 'Total tests executed : ', ntests
    print 'Total tests passed   : ', sums[statCodes.success]
    print 'Hydra failures       : ', sums[statCodes.hydraFail]
    print 'Exodiff failures     : ', exoFail
    print 'Glob failures        : ', sums[statCodes.globFail]
    print 'Total test time (s)  :  %.4f' % (ttime)

    # ".format" will not work with Python 2.5
    #  print 'Percentage failures : {0:.2f}'.format(100.0*float(failures)/float(ntests))
    print 'Percentage failures  : ', (100.0*float(failures)/float(ntests))

    # Write the xml file for failed tests
    if failures != 0 :
        print ''
        ft = tests.opts.dump_dir + '/failed_tests.xml'
        print 'Re-run failed tests using ' + ft
        fp = open(ft, 'w')
        fp.write('<regression>\n')
        # Write the failed test
        for t in tests.testlist:
            if t.status != 0:
                fp.write('  <test name="%s">\n' % t.name )
                fp.write('    <path>%s</path>\n' % t.path )
                fp.write('    <input>%s</input>\n' % t.input )
                fp.write('    <output>%s</output>\n' % t.output )
                fp.write('    <control>%s</control>\n' % t.control )
                fp.write('    <baseline>%s</baseline>\n' % t.baseline )
                fp.write('    <diff>%s</diff>\n' % t.diff )
                fp.write('    <Nproc>%s</Nproc>\n' % t.Nproc )
                fp.write('    <author>%s</author>\n' % t.author )
                fp.write('    <compare>%s</compare>\n' % t.compare )
                if t.restart != '':
                    fp.write('    <restart>%s</restart>\n' % t.restart )
                fp.write('  </test>\n' )

        fp.write('</regression>\n')
        fp.close()
# end of function checkTestStatus

def writeResults(tests):
    'Write an XML file of the test results'

    # Sum failures
    ntests  = len(tests.testlist)
    (failures, sums) = sumStatCodes(tests)

    # For exodiff stuff, sum up the results for now
    exoFail = sums[statCodes.exoFail] + sums[statCodes.statFail]

    hostname = socket.gethostname()
    now      = datetime.datetime.now()
    username = pwd.getpwuid(os.getuid())[0]
    #TODO: compiler = hydra_dir + '/hydra --compiler'
    compiler     = "gnu"
    #hydraVersion = hydra_dir + '/hydra --version'
    hydraVersion = "2.0-dev"

    # Write to file
    test_results = tests.opts.dump_dir + '/' + tests.opts.dashboardFile
    fp = open(test_results, 'w')
    fp.write('<regression name="%s" date="%s">\n' % (tests.regressionName, now.strftime("%Y-%m-%d %H:%M")) )
    fp.write('    <system>\n' )
    fp.write('        <machineName>%s</machineName>\n' % platform.node() )
    fp.write('        <compiler>%s</compiler>\n' % compiler )
    fp.write('        <operatingSystem>%s</operatingSystem>\n' % platform.platform() )
    fp.write('        <OSRelease>%s</OSRelease>\n' % platform.release() )
    fp.write('        <processor>%s</processor>\n' % platform.processor() )
    fp.write('        <hydraVersion>%s</hydraVersion>\n' % hydraVersion )
    fp.write('    </system>\n')
    fp.write('    <summary>\n')
    fp.write('        <username>%s</username>\n' % username )
    fp.write('        <numTests>%s</numTests>\n' % ntests )
    fp.write('        <numPassed>%s</numPassed>\n' % sums[statCodes.success] )
    fp.write('        <numHydraFails>%s</numHydraFails>\n' % sums[statCodes.hydraFail] )
    fp.write('        <numHydraCrashes>0</numHydraCrashes>\n')
    fp.write('        <numExodusCompFails>%s</numExodusCompFails>\n' % exoFail )
    fp.write('        <numExodiffCrashes>0</numExodiffCrashes>\n')
    fp.write('        <numGlobFails>%s</numGlobFails>\n' % sums[statCodes.globFail] )
    fp.write('        <percentFailed>%s</percentFailed>\n' %  (100.0*float(failures)/float(ntests)) )
    fp.write('    </summary>\n')
    # Loop through each test
    for t in tests.testlist:
        # Get the solution times from the hydra ascii output file
        elementCycleTime = "N/A"
        solutionTime = "N/A"
        if hasattr(t, "metapath"):
            filename = tests.opts.dump_dir + '/' + t.metaname + '/' + t.out_tty()
        else:
            filename = tests.opts.dump_dir + t.path + '/' + t.out_tty()
        for line in open(filename):
            if "Element Cycle Time" in line:
                elementCycleTime = line.split(" ")[-1]
            elif "Solution Time" in line:
                solutionTime = line.split(" ")[-1]
        # Print out the xml for this test
        fp.write('    <test name="%s">\n' % t.name )
        fp.write('        <path>%s</path>\n' % t.path )
        fp.write('        <input>%s</input>\n' % t.input )
        fp.write('        <output>%s</output>\n' % t.output )
        fp.write('        <control>%s</control>\n' % t.control )
        fp.write('        <baseline>%s</baseline>\n' % t.baseline )
        fp.write('        <diff>%s</diff>\n' % t.diff )
        fp.write('        <Nproc>%s</Nproc>\n' % t.Nproc )
        fp.write('        <author>%s</author>\n' % t.author )
        fp.write('        <status>%s</status>\n' % statCodes.mesg[t.status] )
        fp.write('        <elementCycleTime>%s</elementCycleTime>\n' % elementCycleTime )
        fp.write('        <solutionTime>%s</solutionTime>\n' % solutionTime )
        fp.write('    </test>\n' )
    fp.write('</regression>\n')
    fp.close()

    # Register the streaming http handlers with urllib2
    if tests.opts.sendToServer:
        print ''
        print 'Updating the dashboard'
        print 'Sending the results file to: ', tests.opts.dashboardURL
        register_openers()
        datagen, headers = multipart_encode( {"regressionXmlFile": open(test_results)} )
        request = urllib2.Request( tests.opts.dashboardURL, datagen, headers )
        try:
          urllib2.urlopen( request )
        except urllib2.HTTPError, e:
          print 'Error sending to Hydra Dashboard'
          print request
          print e.code
          print e.msg
          print e.headers
          print e.fp.read()
# end of function writeResults

def writeRestartCntl(incntl, outcntl, nsteps, restartCall=True):
    '''
    Reads the control file incntl and writes a new control file, with nsteps
    reset.  Returns the number of remaining steps.
    '''
    fin = open(incntl, 'r')
    fout = open(outcntl, 'w')
    nLeft = None
    plti = None
    dump = None
    while 1:
        line = fin.readline()
        if not line:
            break
        s = line.split()
        if len(s) > 0:
            if s[0] == 'nsteps':
                if nLeft is not None:
                    m = "In control file '%s'" % incntl
                    m += "\n       nsteps specified more than once."
                    raise RuntimeError(m)
                n = int(s[1])
                line = '  nsteps %d\n' % (nsteps)
                nLeft = n - nsteps
                if nLeft < 0 and restartCall:
                    m = "In processing control file '%s'" % incntl
                    m += "\n   parameter 'nsteps = %d' which is less than restart = %d" % (n, nsteps)
                    raise RuntimeError(m)
            elif s[0] == 'plti':
                plti = int(s[1])
                if restartCall and nsteps % plti != 0:
                    m = "In control file '%s'" % incntl
                    m += "\n       plti = %d, but restart = %d needs to be a multiple of plti" % (plti, nsteps)
                    raise RuntimeError(m)
            elif s[0] == 'dump':
                dump = int(s[1])
                if dump == 0:
                    # set dump to something so that we get a dump file
                    line = '  dump 1000\n'
        fout.write(line)
    fin.close()
    fout.close()
    # check for various errors
    if nLeft is None:
        m = "Control file '%s' has no parameter 'nsteps' for a restart test!" % incntl
        raise RuntimeError(m)
    if plti is None:
        m = "Control file '%s' has no parameter 'plti' for restart test!" % incntl
        raise RuntimeError(m)
    if dump is None:
        m = "Control file '%s' has no parameter 'dump' for restart test!" % incntl
        raise RuntimeError(m)
    return nLeft
# end of function writeRestartCntl

def updateBaseline(newfile, baseline):
    '''
    Prompts the user to see if baseline file should be updated with newfile.
    '''
    if updateBaselinePrompt:
        lock.acquire()
        try:
            prompt = 'Update baseline "%s" with file "%s"? (y, [n]): ' \
                % (baseline, newfile)
            answer = '*'
            while answer not in ('y','n',''):
                answer = raw_input(prompt)
        finally:
            lock.release()
        if answer == 'y':
            shutil.copy(newfile, baseline)
    else:
        print 'Updating baseline', baseline
        shutil.copy(newfile, baseline)
# end of function updateBaseline

def runHydra(t, f, mpi_command, opts):
    'Runs Hydra'
    restart_flag = '' # this will be set for restart cases
    mesg = None

    if t.restart != '': # then it's a restart case
        # run hydra the first time, taking t.restart steps
        steps = int(t.restart)
        # write a new cntl file for this run
        f_control_r = f.out_path + t.control
        stepsLeft = writeRestartCntl(f.control, f_control_r, steps)
        # creat different tty and dump files; everything else the same
        f_out_tty_r = f.out_tty + '_run1'
        f_out_dump_r = f.out_dump + '_run1'
        h_opts = ' -i ' + f.input + \
                 ' -c ' + f_control_r + \
                 ' -d ' + f_out_dump_r + \
                 ' -p ' + f.out_exo + \
                 ' -g ' + f.out_glob + \
                 ' -o ' + f.out_out + \
                 ' -h ' + f.out_hist + \
                 ' -n ' + f.out_conv
        h = mpi_command.build_command(t.Nproc,opts.hydra_exe,h_opts) + \
            ' > ' + f_out_tty_r

        try:
            if system(h) != 0 : t.status = statCodes.hydraFail
        except Exception, e:
            t.status = statCodes.hydraFail
            mesg = str(e)

        # write the control file for the second run, taking the remaining
        # steps, and set up for the second run of hydra
        writeRestartCntl(f.control, f_control_r, stepsLeft, False)
        f.control = f_control_r
        restart_flag = ' -r ' + f_out_dump_r


    # Run hydra (a second time for restart cases)
    if t.status == statCodes.success:
        # form the overall command
        h_opts = ' -i ' + f.input + \
                 ' -c ' + f.control + \
                 ' -d ' + f.out_dump + \
                 ' -e ' + f.out_ecntl + \
                 ' -p ' + f.out_exo + \
                 ' -g ' + f.out_glob + \
                 ' -o ' + f.out_out + restart_flag + \
                 ' -h ' + f.out_hist + \
                 ' -n ' + f.out_conv
        h = mpi_command.build_command(t.Nproc,opts.hydra_exe,h_opts) + \
            ' > ' + f.out_tty

        try:
            if system(h) == 0:
                fprev = open(f.out_prev, 'w')
                fprev.write('%d\n' % int(t.Nproc))
                fprev.write('%s\n' % opts.hydra_exe)
                fprev.close()
            else:
                t.status = statCodes.hydraFail
        except Exception, e:
            t.status = statCodes.hydraFail
            mesg = str(e)
    return mesg
# end of function runHydra

def runExodiff(t, f, mpi_command, opts):
    'Runs exodiff for exodus output files'
    ediff_opts='-m -stat'
    if os.path.isfile(f.diff): ediff_opts += ' -f ' + f.diff
    ediff_opts += ' ' + f.baseline + ' ' + f.out_exo
    ediff = mpi_command.build_command(1, opts.exodiff_exe, ediff_opts) + \
            ' > ' + f.out_diffs
    mesg = None
    try:
        if system(ediff) != 0 : t.status = statCodes.exoFail
    except Exception, e:
        t.status = statCodes.exoFail
        mesg = str(e)
    return mesg
# end of function runExodiff

def runStatsExodiff(t, f, mpi_command, opts):
    'Runs exodiff for stats file'
    ediff_opt='-m -stat'
    if os.path.isfile(f.statdiff): ediff_opt += ' -f ' + f.statdiff
    ediff_opt += ' ' + f.statbaseline + ' ' + f.out_statexo
    ediff = mpi_command.build_command(1,opts.exodiff_exe,ediff_opt) + \
            ' > ' + f.out_statdiffs
    mesg = None
    try:
        if system(ediff) != 0 : t.status = statCodes.statFail
    except Exception, e:
        t.status = statCodes.statFail
        mesg = str(e)
    return mesg
# end of function runStatsExodiff

def runGlobDiff(t, f, mpi_command, opts):
    'Runs numdiff for glob file'
    mesg = None
    try:
        numdiff = Numdiff()
        numdiff.out = open(f.out_glob_diff, 'w')
        numdiff.verbosity = 2
        numdiff.tolerances.absolute = None
        numdiff.tolerances.relative = 1.0e-6
        numdiff.tolerances.epsilon  = numdiff.tolerances.relative
        (globDiffer, globStats) = numdiff.diff(f.out_glob, f.globbaseline)
        if globDiffer: t.status = statCodes.globFail
        numdiff.out.close()
    except Exception, e:
        t.status = statCodes.globFail
        mesg = str(e)
    return mesg
# end of function runGlobDiff

def runTestWrap(t, num, opts, totals):
    'Wrapper for runTest, so that we can trap any uncaught exceptions'

    # Get the paths to needed files.
    f = TestFiles(t, opts)

    # Create output directory, if needed. We found that sometimes 2 or more
    # threads were trying to create the same dir (they both pass the if-check
    # at the same time, with the same out_path), so do a lock.
    lock.acquire()
    try:
        if not os.path.isdir(f.out_path):
            os.makedirs(f.out_path)
    finally:
        lock.release()

    # run the test
    ranHydra = False
    try:
        with TestTimer() as testtime:
            (mesg, ranHydra) = runTest(t, f, num, opts, totals)
    except Exception, err:
        mesg = str(err)
        # for now, we rack this up as a hydra failure
        t.status = statCodes.hydraFail
    finally:
        t.time=testtime.interval

    # Collect all output into a string to ensure that thread output is not
    # interleaved, without having to acquire a thread lock quite yet
    p  = 80 * '=' + '\n'
    p += 'Completed test    : %d\n' % num
    p += 'Test name         : %s\n' % t.name
    p += 'Test run time     : %.4f\n' % t.time
    if hasattr(t, "metapath"):
        p += 'Test path         : %s\n' % t.metapath
    else:
        p += 'Test path         : %s\n' % t.path
    p += 'Input file        : %s\n' % t.input
    p += 'Output prefix     : %s\n' % t.output
    p += 'Control file      : %s\n' % t.control
    p += 'Baseline file     : %s\n' % t.baseline
    p += 'Diff file         : %s\n' % t.diff
    p += 'Restart           : %s\n' % t.restart_text()
    p += 'Num. of proc.     : %s\n' % t.Nproc

    if mesg is not None:
        p += bcolors.FAIL + 'rtest ERROR: ' + mesg + '\n' + bcolors.ENDC

    if t.status == statCodes.hydraFail:
        p +=  bcolors.FAIL + 'FAILED:'+bcolors.ENDC+bcolors.MESSAGE+' Hydra failed to launch, or crashed.  Output comparisons skipped.\n' + bcolors.ENDC
    else:
        if ranHydra:
            p +=  bcolors.OKBOLDGREEN + 'PASSED:'+bcolors.ENDC+bcolors.MESSAGE+' Hydra completed\n' + bcolors.ENDC
        else:
            p +=  bcolors.OKBOLDGREEN + 'PASSED:'+bcolors.ENDC+bcolors.MESSAGE+' Hydra output up to date, hydra run skipped\n' + bcolors.ENDC

        if os.path.isfile(f.baseline) :
             if t.status == statCodes.exoFail:
                 p +=  bcolors.FAIL + 'FAILED:'+bcolors.ENDC+bcolors.MESSAGE+' Exodiff comparison. Remaining comparisons skipped.\n' + bcolors.ENDC
             else:
                 p +=  bcolors.OKBOLDGREEN + 'PASSED:'+bcolors.ENDC+bcolors.MESSAGE+' Exodiff comparison\n' + bcolors.ENDC
        elif os.path.isfile(f.out_exo):
            p += bcolors.WARNING + \
                'WARNING:'+bcolors.ENDC+bcolors.MESSAGE+' Exodiff has no baseline, but there is exo output...   \n' + \
                '               ...assuming this is intentional...              \n' + bcolors.ENDC

        if os.path.isfile(f.statbaseline) :
             if t.status == statCodes.statFail:
                 p +=  bcolors.FAIL + 'FAILED:'+bcolors.ENDC+bcolors.MESSAGE+' Exodiff-statistics comparison. Remaining comparisons skipped.\n' + bcolors.ENDC
             else:
                 p +=  bcolors.OKBOLDGREEN + 'PASSED:'+bcolors.ENDC+bcolors.MESSAGE+' Exodiff-statistics comparison\n' + bcolors.ENDC
        elif os.path.isfile(f.out_statexo):
            p += bcolors.WARNING + \
                'WARNING:'+bcolors.ENDC+bcolors.MESSAGE+' Exodiff-statistics has no baseline, but there is stats-exo output...   \n' + \
                '                         ...assuming this is intentional...                     \n' + bcolors.ENDC

        if os.path.isfile(f.globbaseline) :
             if t.status == statCodes.globFail:
                 p +=  bcolors.FAIL + 'FAILED:'+bcolors.ENDC+bcolors.MESSAGE+' Glob comparison. Remaining comparisons skipped.\n' + bcolors.ENDC
             else:
                 p +=  bcolors.OKBOLDGREEN + 'PASSED:'+bcolors.ENDC+bcolors.MESSAGE+' Glob comparison\n' + bcolors.ENDC
        elif os.path.isfile(f.out_glob):
            p += bcolors.WARNING + \
                'WARNING:'+bcolors.ENDC+bcolors.MESSAGE+' Glob has no baseline, but there is glob output...   \n' + \
                '                ...assuming this is intentional...           \n' + bcolors.ENDC

    p += 80 * '=' + '\n'
    # Update the running totals.  We acquire a lock here because totals is
    # shared among threads.
    lock.acquire()
    try:
        totals.numTest += 1
        if t.status != statCodes.success: totals.numFail += 1
        p += '=== Current status: %d total tests complete, %d failures\n' % (totals.numTest, totals.numFail)
        p += 80 * '='
        print p
        sys.stdout.flush()
    finally:
        lock.release()
# end of function runTestWrap

def runTest(t, f, num, opts, totals):
    '''
    Runs the various executables for a single test.  Returns True if Hydra was run, false otherwise.
    '''

    # Determine the mpi command to run
#   mpi_command = 'mpirun -mca opal_paffinity_alone 0 --map-by node -np '
#   if opts.ostype == "cray":
#       mpi_command = 'aprun -n '
    my_mpi_command=SelectMpiInterface(opts.mpitype)

    t.status = statCodes.success # hope for the best
    mesg = None # error message from any of the runs below.

    # If the hydra executable, input exo, control file, and number of
    # processors have not changed since the previous run, then we don't run
    # hydra again.
    timeDiff = 1 # ultimately, max diff in file timestamps
    ranHydra = False
    if os.path.isfile(f.out_prev):
        # check whether the last run used the same Nproc, and ensure the
        # executable is the same
        fprev = open(f.out_prev, 'r')
        nproc = int(fprev.readline())
        exe   = fprev.readline()[:-1]
        fprev.close()
        if exe == opts.hydra_exe and nproc == int(t.Nproc):
            # now check all the timestamps. if any time
            # difference is positive, then we're going to re-run hydra, but we
            # accumulate the max just to make the checking a bit easier.
            timePrev = os.path.getmtime(f.out_prev)
            timeDiff = os.path.getmtime(opts.hydra_exe) - timePrev
            timeDiff = max(timeDiff,
                           os.path.getmtime(f.input) - timePrev)
            timeDiff = max(timeDiff,
                           os.path.getmtime(f.control) - timePrev)
    if timeDiff >= 0:
        # then some file is newer than the nproc file, so run hydra
        mesg = runHydra(t, f, my_mpi_command, opts)
        ranHydra = True

    # Run exodiff if Hydra was OK
    if os.path.isfile(f.baseline):
        if t.status == statCodes.success:
            mesg = runExodiff(t, f, my_mpi_command, opts)
    elif opts.updateBaseline and os.path.isfile(f.out_exo):
        print 'Baseline exodus file %s does not exist' % f.baseline
        updateBaseline(f.out_exo, f.baseline)

    # Run exodiff for .stat.exo if exists
    if os.path.isfile(f.statbaseline):
        if t.status == statCodes.success:
            mesg = runStatsExodiff(t, f, my_mpi_command, opts)
    elif opts.updateBaseline and os.path.isfile(f.out_statexo):
        print 'Baseline stats file %s does not exist' % f.statbaseline
        updateBaseline(f.out_statexo, f.statbaseline)

    # Run numdiff for glob files, if they exist
    if os.path.isfile(f.globbaseline):
        if t.status == statCodes.success:
            mesg = runGlobDiff(t, f, my_mpi_command, opts)
    elif opts.updateBaseline and os.path.isfile(f.out_glob):
        print 'Baseline glob file %s does not exist' % f.globbaseline
        updateBaseline(f.out_glob, f.globbaseline)

#####DEBUG
#   if os.path.isfile(f.baseline):
#       if t.status == statCodes.success:
#           mesg = runExodiff(t, f, my_mpi_command, opts)
#       if opts.updateBaseline:
#           print 'Baseline exodus file %s does not exist' % f.baseline
#           updateBaseline(f.out_exo, f.baseline)
#   # Run exodiff for .stat.exo if exists
#   if os.path.isfile(f.statbaseline):
#       if t.status == statCodes.success:
#           mesg = runStatsExodiff(t, f, my_mpi_command, opts)
#       if opts.updateBaseline:
#           print 'Baseline stats file %s does not exist' % f.statbaseline
#           updateBaseline(f.out_statexo, f.statbaseline)
#   # Run numdiff for glob files, if they exist
#   if os.path.isfile(f.globbaseline):
#       if t.status == statCodes.success:
#           mesg = runGlobDiff(t, f, my_mpi_command, opts)
#       if opts.updateBaseline:
#           print 'Baseline glob file %s does not exist' % f.globbaseline
#           updateBaseline(f.out_glob, f.globbaseline)
#####DEBUG

    return (mesg, ranHydra)
# end of function runTest

class Threader(threading.Thread):
    'This is the threading object to run function runTest()'
    def __init__(self, t, num, opts, totals):
        threading.Thread.__init__(self, target=runTestWrap, args=(t, num, opts, totals))
        self.test = t
# end of class Threader

def runAllTests(tests):
    '''
    Runs all of the tests in the test suite
    '''

    print ''
    print 'Running all prescribed tests ...'

    testlist = tests.testlist
    numTests = len(testlist) # total number of tests
    totals = Totals()

    # We assume for now that all comparisons are exodus
    for t in testlist:
        if t.compare != 'exodus':
            print "ERROR: compare flag '%s' not supported in test: %s" % (t.compare, t.name)
            sys.exit(1)

    # Set the test IDs
    for i in range(numTests):
        testlist[i].id = i+1

    n = 0 # current test number
    numcpu = 0
    threads = [] # the list of running threads
    sleep_time = 2.0 # time to sleep before checking threads

    while n < numTests: # we still have tests to do
        numcpu = 0 # number of cpus running
        running = [] # the threads that are still running
        for th in threads:
            if th.isAlive():
                numcpu += int(th.test.Nproc)
                running.append(th)
        threads = running
        t = testlist[n]
        # keep numpcpu <= #cpus, but run at least one thread
        if numcpu == 0 or numcpu + int(t.Nproc) <= tests.opts.cpus:
            threadPrint('=== Spawning test %d of %d: %s\n' % \
                            (testlist[n].id, numTests, testlist[n].name))
            n += 1
            #newthread = Threader(t, n, tests.opts, totals)
            newthread = Threader(t, n, t.opts, totals)
            newthread.start()
            threads.append(newthread)
        else:
            # we didn't spawn a new thread, so sleep before checking again
            # so we are not constantly checking status
            time.sleep(sleep_time)

    # make sure all threads are done
    nLast = -1 # used to print status only when number of threads running changes
    while 1:
        running = [] # list of test ids still running
        for th in threads:
            if th.isAlive():
                running.append(th.test.id)
        n = len(running)
        if n == 0:
            break
        else:
            if tests.opts.cpus > 1 and n != nLast:
                threadPrint('=== %d test(s) still running, IDs: %s\n' % (n, `running`))
            nLast = n
            time.sleep(sleep_time) # sleep a bit so we are not contantly checking status
# end of function runAllTests

################################################################################
# MAIN

# Parse the command line

cl = CL_Options(_usage)
args = cl.parse()

# We check args here so that CL_Options may be inherited and
# used in another script.
if len(args) > 0:
    cl.parser.error('Too many arguments')

################################################################################
# Parse the input suite file

# Test suite parser
tests = TestParser(cl.opts)

# Create the sax parser & parse
saxparser = make_parser()
saxparser.setContentHandler(tests)
datasource = open(cl.opts.suite, "r")
saxparser.parse(datasource)

print '\n*** Hydra Regression Tests ***\n'
print '\nOptions:' + cl.doc_string() + '\n'
print '\nMPI launch command:\n' + \
      SelectMpiInterface(cl.opts.mpitype).build_command('nprocs','hydra','[hydra opts]') + \
      '\n'
print  len(tests.testlist), 'Tests found in ', cl.opts.suite

# Traverse the test suite & echo the list
# traverseList(tests)

# Run the tests
with TestTimer() as alltime:
    runAllTests(tests)

# Check & report the status of the tests
checkTestStatus(tests)
writeResults(tests)

print '\nElapsed wall time (s)    : %.4f' % alltime.interval
