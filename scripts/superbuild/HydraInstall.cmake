set(binaries
  # netCDF
  "${TPL_INSTALL_DIR}/bin/nc-config"
  "${TPL_INSTALL_DIR}/bin/nccopy"
  "${TPL_INSTALL_DIR}/bin/ncdump"
  "${TPL_INSTALL_DIR}/bin/ncgen"
  "${TPL_INSTALL_DIR}/bin/ncgen3"

  # Hydra and Utilities
  "${code_install_prefix}/bin/channel_mesh"
  "${code_install_prefix}/bin/divset"
  "${code_install_prefix}/bin/ascii2exo"
  "${code_install_prefix}/bin/exo2msh"
  "${code_install_prefix}/bin/exodiff"
  "${code_install_prefix}/bin/forcenorm"
  "${code_install_prefix}/bin/hydra"
  "${code_install_prefix}/bin/lo"
  "${code_install_prefix}/bin/msh2exo"
  "${code_install_prefix}/bin/neu2exo"
  "${code_install_prefix}/bin/norm"

)

install(PROGRAMS    ${binaries}
        DESTINATION "${CMAKE_INSTALL_PREFIX}/bin")

foreach (subdir conf lib share)
  set(dirs)
  if (EXISTS "${TPL_INSTALL_DIR}/${subdir}/")
    list(APPEND dirs "${TPL_INSTALL_DIR}/${subdir}/")
  endif ()
  if (EXISTS "${code_install_prefix}/${subdir}/")
    list(APPEND dirs "${code_install_prefix}/${subdir}/")
  endif ()
  install(
    DIRECTORY   ${dirs}
    DESTINATION "${CMAKE_INSTALL_PREFIX}/${subdir}"
    USE_SOURCE_PERMISSIONS)
endforeach ()

file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/use-hydra"
"[ -n \"$LD_LIBRARY_PATH\" ] && \\
  LD_LIBRARY_PATH=\"${CMAKE_INSTALL_PREFIX}/lib:$LD_LIBRARY_PATH\" || \\
  LD_LIBRARY_PATH=\"${CMAKE_INSTALL_PREFIX}/lib\"
PATH=\"${CMAKE_INSTALL_PREFIX}/bin:$PATH\"
")
install(
  PROGRAMS    "${CMAKE_CURRENT_BINARY_DIR}/use-hydra"
  DESTINATION "${CMAKE_INSTALL_PREFIX}")
