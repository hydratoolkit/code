# Setup CPack packaging

include(InstallRequiredSystemLibraries)

set(CPACK_PACKAGE_VENDOR "Computational Sciences International")

set(CPACK_PACKAGE_CONTACT "machriston@c-sciences.com")

set(CPACK_DEBIAN_PACKAGE_MAINTAINER "machriston@c-sciences.com")

#####DEBUG
set(HYDRA_MAJOR_VERSION 1 )
set(HYDRA_MINOR_VERSION 01)
#####DEBUG

set(CPACK_PACKAGE_VERSION ${HYDRA_MAJOR_VERSION}.${HYDRA_MINOR_VERSION})

set(CPACK_BUNDLE_NAME "Hydra Toolkit")

set(CPACK_INCLUDE_TOPLEVEL_DIRECTORY 1)

set(CPACK_RESOURCE_FILE_LICENSE "${CODE_SOURCE}/COPYRIGHT_and_LICENSE.md")

set(CPACK_RESOURCE_FILE_README "${CODE_SOURCE}/README.md")

#####DEBUG
# set(CPACK_INSTALL_CMAKE_PROJECTS
#   "${CPACK_INSTALL_CMAKE_PROJECTS};${CODE_BINARY}/src;Hydra;RuntimeLibraries;/"
# )
#####DEBUG

IF(APPLE)
  message(STATUS, "APPLE true")
  SET(CPACK_BINARY_TBZ2 OFF)
  SET(CPACK_BINARY_DRAGNDROP ON)
  SET(CPACK_BINARY_PACKAGEMAKER OFF)
  SET(CPACK_BINARY_STGZ OFF)
ENDIF()

#####DEBUG
# message(STATUS, "HYDRA_BINARY_DIR: " ${HYDRA_BINARY_DIR})
# set(CPACK_GENERATOR "DEB")
set(CPACK_GENERATOR "TGZ")
# set(CPACK_GENERATOR "STGZ")
#####DEBUG

include(CPack)
