#!/bin/tcsh
#
# Script to build Hydra, it's supporting lib's for packaging/distribution
#
# Build from a clean checkout of code + TPL's
#
# 0. Clone bootstrap, tpl, hydra
# 1. Build MPI and install in distribution package
# 2. Build TPL's using new MPI build
# 3. Add lib's for a given OS, e.g., ubuntu, macos, etc.,
# 4. Build Hydra
# 5. Now patch the runpath's to make relocatable
# 6. Add license, export control, changelog
# 7. Generate distribution packages
# 8. Generate source distribution tarball
#
# Naming: Hydra-x.x.x-OS-MPIversion-64bit
#
# Example:
#   Linux: Hydra-1.0.1-Linux-OpenMPI-64bit
#   MacOS: Hydra-1.0.1-MacOS-OpenMPI-64bit.app
#
# Usage:
# package_build /mnt/scratch/package-build /mnt/scratch Hydra-1.0.1-MacOS-...

if ($#argv < 3) then
    echo ""
    echo "Usage: package_build build-dir install-path install-name"
    echo ""
    exit(1)
else
    echo ""
    echo "Executing: package_build" $argv[1] $argv[2] $argv[3]
    echo ""
endif

# Version to be built -- blank means use the head, otherwise the repo version
# set versionTag = "v1.0.3-Release-p1"
set versionTag = ""

set OSName = `uname`

unsetenv OPAL_PREFIX

set COMPILER_DIR = " "
if ($OSName == "Linux") then
    echo "OS: Linux"
    set COMPILER_DIR = /usr/local/
    set LIB_DIR = /lib/x86_64-linux-gnu
else if ($OSName == "Darwin") then
    echo "OS: Darwin"
    set COMPILER_DIR = /usr/local/Cellar/gcc@7/7.3.0/lib/gcc/7
endif

set BUILD_DIR = $argv[1]

set INSTALL_DIR = $argv[2]/$argv[3]

set INSTALL_NAME = $argv[3]

# For ubuntu
set MPI_PACKAGE = openmpi-4.0.2
# For macos
#set MPI_PACKAGE = openmpi-4.1.1

# Currently unused
set TEST_DIR = /mnt/scratch/package-test

echo "Check build and install directories"
if (-d $BUILD_DIR) then
    echo "Build directory already exists ..."
    echo "Terminating package build"
    exit(1)
else
    echo "Creating build directory"
    mkdir $BUILD_DIR
endif

if (-d $INSTALL_DIR) then
    echo "Existing install directory in place..."
    echo "Terminating package build"
    exit(1)
else
    echo "Creating install directory"
    mkdir $INSTALL_DIR
endif

#===============================================================================

# 1. Build OpenMPI -- in place for openmpi for now
#    Note that cuda and opencl, if installed, force the -lOpenCL and -lcudart
#    on the link, and require including libOpenCL*, libibverbs*, libcudart*
#    in the distribution tarball.
#
#    v1.2.0 built on panther virtual machine w. ubuntu 15.10 and gcc 5.2.1
#    v1.1.0 built on panther virtual machine w. ubuntu 15.10 and gcc 5.2.1
#    v1.0.3 built on panther virtual machine w. ubuntu 15.10 and gcc 5.2.1
#
echo "Bootstrap builds: MPI"
cd $BUILD_DIR
git clone git@gitlab.com:hydratoolkit/bootstrap.git
cd bootstrap
if ($versionTag != "") then
    echo "Checking out version: ", $versionTag
    git checkout $versionTag
endif
tar xvzf $MPI_PACKAGE.tar.gz
cd $MPI_PACKAGE
./configure --prefix=$INSTALL_DIR --enable-mpi1-compatibility
make -j 12
if ($? == 0) then
    echo "...MPI compile successful: " $?
else
    echo "... MPI compile failed: " $?
    exit
endif

make check
if ($? == 0) then
    echo "... MPI testing successful: " $?
else
    echo "... MPI testing failed: " $?
    exit
endif

make install
if ($? == 0) then
    echo "... MPI install successful: " $?
else
    echo "... MPI install failed: " $?
    exit
endif

# 2. Build TPL's
echo "Build TPL's using fresh MPI"
cd $BUILD_DIR
git clone git@gitlab.com:hydratoolkit/tpl.git
if ($versionTag != "") then
    echo "Checking out version: ", $versionTag
    cd tpl
    git checkout $versionTag
    cd ..
endif
mkdir tpl-build
cd tpl-build
cmake \
    -D HYDRA_LAPACK=ON \
    -DTPL_INSTALL_DIR=$INSTALL_DIR \
    -D MPIEXEC:PATH=$INSTALL_DIR/bin/mpiexec \
    -DMPI_CXX_COMPILER=$INSTALL_DIR/bin/mpic++ \
    -DMPI_C_COMPILER=$INSTALL_DIR/bin/mpicc \
    -DMPI_Fortran_COMPILER=$INSTALL_DIR/bin/mpif90 \
    ../tpl
if ($? == 0) then
    echo "... CMake TPL configuration successful: " $?
else
    echo "... CMake TPL configuration failed: " $?
    exit
endif

make -j 12
if ($? == 0) then
    echo "... TPL build successful: " $?
else
    echo "... TPL build failed: " $?
    exit
endif

# 3. Add lib's for a given OS, e.g., ubuntu, macos, etc.,
#    cleanup runpath's
echo "Place lib's for a given OS as required"
cd $INSTALL_DIR/lib
if ($OSName == "Linux") then
    # Linux -- specific to bigbox build on Ubuntu 18.04LTS
    echo "-- Linux libraries being added manually --"
    cp -v $LIB_DIR/libm-2.27.so .
    ln -s libm-2.27.so libm.so.6
    cp -v $LIB_DIR/libz.so.1.2.11 .
    ln -s libz.so.1.2.11 libz.so.1
    echo "... done adding libraries ..."
else if ($OSName == "Darwin") then
    # MacOS
    echo "-- MacOS libraries being added manually --"
    cp -v $COMPILER_DIR/libgfortran.4.dylib .
    cp -v $COMPILER_DIR/libgfortran.a .
    cp -v $COMPILER_DIR/libgfortran.spec .
    ln -s libgfortran.4.dylib libgfortran.dylib
    cp -v $COMPILER_DIR/libquadmath.0.dylib .
    cp -v $COMPILER_DIR/libquadmath.a .
    ln -s libquadmath.0.dylib libquadmath.dylib
    cp -v $COMPILER_DIR/libgcc_s.1.dylib .
    echo "... done adding libraries ..."
endif

# 4. Build Hydra
# Force build of lapack and blas to avoid additional system files
echo "Build Hydra"
cd $BUILD_DIR
git clone git@gitlab.com:hydratoolkit/code.git
if ($versionTag != "") then
    echo "Checking out version: ", $versionTag
    cd code
    git checkout $versionTag
    cd ..
endif
mkdir code-build
cd code-build

cmake \
    -D CMAKE_BUILD_TYPE=RELEASE \
    -D MPIEXEC:PATH=$INSTALL_DIR/bin/mpiexec \
    -D CMAKE_Fortran_COMPILER:PATH=$INSTALL_DIR/bin/mpif90 \
    -D CMAKE_C_COMPILER:PATH=$INSTALL_DIR/bin/mpicc \
    -D CMAKE_CXX_COMPILER:PATH=$INSTALL_DIR/bin/mpic++ \
    -D MPI_Fortran_COMPILER:PATH=$INSTALL_DIR/bin/mpif90 \
    -D MPI_Fortran_INCLUDE_PATH:PATH=$INSTALL_DIR/include \
    -D MPI_C_COMPILER:PATH=$INSTALL_DIR/bin/mpicc \
    -D MPI_C_INCLUDE_PATH:PATH=$INSTALL_DIR/include \
    -D MPI_CXX_COMPILER:PATH=$INSTALL_DIR/bin/mpic++ \
    -D MPI_CXX_INCLUDE_PATH:PATH=$INSTALL_DIR/include \
    -D MPI_LIBRARY:PATH=$INSTALL_DIR/lib \
    -D THIRD_PARTY_PREFIX=$INSTALL_DIR \
    -D CMAKE_INSTALL_PREFIX=$INSTALL_DIR \
    -D CMAKE_EXE_LINKER_FLAGS="-static-libgcc -static-libstdc++" \
    ../code/src
if ($? == 0) then
    echo "... CMake code configuration successful: " $?
else
    echo "... CMake code configuration failed: " $?
    exit
endif

make -j 12
if ($? == 0) then
    echo "... code build successful: " $?
else
    echo "... code build failed: " $?
    exit
endif

make install
if ($? == 0) then
    echo "... code install successful: " $?
else
    echo "... code install failed: " $?
    exit
endif

# 5. Now patch the runpath's to make relocatable
echo "Patch runpath's"
cd $INSTALL_DIR/lib
if ($OSName == "Linux") then
    echo "Patching rpath in OpenMPI binaries"
    cd $INSTALL_DIR/bin

    # Remove unused executables
    /bin/rm TOPSGenerator.py TOPSInstaller.py
    /bin/rm adiforfix.py adprocess.py
    /bin/rm configVars.py
    /bin/rm -rf julia
    /bin/rm -rf matlab
    /bin/rm mpif77 mpif90 mpicc mpicxx mpic++ mpiCC mpif90 mpifort
    /bin/rm parseargs.py
    /bin/rm petsc_libtool
    /bin/rm petscmpiexec
    /bin/rm petscnagupgrade.py
    /bin/rm popup
    /bin/rm portabilitycheck.py
    /bin/rm processSummary.py
    /bin/rm -rf pythonscripts
    /bin/rm taucc.py
    /bin/rm update.py
    /bin/rm urlget
    /bin/rm urlget.py
    /bin/rm -rf win32fe

    echo "Patching rpath in OpenMPI executables"
    patchelf --set-rpath \$ORIGIN/../lib ompi_info
    patchelf --set-rpath \$ORIGIN/../lib opal_wrapper
    patchelf --set-rpath \$ORIGIN/../lib orte-clean
    patchelf --set-rpath \$ORIGIN/../lib orted
    patchelf --set-rpath \$ORIGIN/../lib orte-dvm
    patchelf --set-rpath \$ORIGIN/../lib orte-info
    patchelf --set-rpath \$ORIGIN/../lib orte-ps
    patchelf --set-rpath \$ORIGIN/../lib orterun
    patchelf --set-rpath \$ORIGIN/../lib orte-server
    patchelf --set-rpath \$ORIGIN/../lib orte-submit
    patchelf --set-rpath \$ORIGIN/../lib orte-top
    patchelf --set-rpath \$ORIGIN/../lib oshmem_info

    echo "Patching rpath in OpenMPI shared libraries"
    cd $INSTALL_DIR/lib
    patchelf --set-rpath \$ORIGIN libmpi_mpifh.so.40.10.0
    patchelf --set-rpath \$ORIGIN libmpi.so.40.10.0
    patchelf --set-rpath \$ORIGIN libmpi_usempif08.so.40.10.0
    patchelf --set-rpath \$ORIGIN libmpi_usempi_ignore_tkr.so.40.10.0
    patchelf --set-rpath \$ORIGIN libopen-rte.so.40.10.0
    patchelf --set-rpath \$ORIGIN liboshmem.so.40.10.0

    echo "Patching rpath in low-level OpenMPI libraries"
    cd $INSTALL_DIR/lib/openmpi
    patchelf --set-rpath \$ORIGIN/.. mca_btl_sm.so
    patchelf --set-rpath \$ORIGIN/.. mca_coll_sm.so
    patchelf --set-rpath \$ORIGIN/.. mca_coll_tuned.so
    patchelf --set-rpath \$ORIGIN/.. mca_mpool_sm.so

else if ($OSName == "Darwin") then

    echo "Patching loader_path, rpath in OpenMPI binaries"
    cd $INSTALL_DIR/bin

    # Remove unused executables
    /bin/rm TOPSGenerator.py TOPSInstaller.py
    /bin/rm adiforfix.py adprocess.py
    /bin/rm configVars.py
    /bin/rm -rf julia
    /bin/rm -rf matlab
    /bin/rm mpif77 mpif90 mpicc mpicxx mpic++ mpif90 mpifort
    /bin/rm parseargs.py
    /bin/rm petsc_libtool
    /bin/rm petscmpiexec
    /bin/rm petscnagupgrade.py
    /bin/rm popup
    /bin/rm portabilitycheck.py
    /bin/rm processSummary.py
    /bin/rm -rf pythonscripts
    /bin/rm taucc.py
    /bin/rm update.py
    /bin/rm urlget
    /bin/rm urlget.py
    /bin/rm -rf win32fe

    # orterun: mpirun, mpiexec
    install_name_tool -add_rpath @loader_path/../lib orterun
    install_name_tool -change $INSTALL_DIR/lib/libopen-rte.40.dylib @rpath/libopen-rte.40.dylib orterun
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib @rpath/libopen-pal.40.dylib orterun
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib        @rpath/libgcc_s.1.dylib     orterun

    # ompi-clean, orte-clean:
    install_name_tool -add_rpath @loader_path/../lib orte-clean
    install_name_tool -change $INSTALL_DIR/lib/libopen-rte.40.dylib @rpath/libopen-rte.40.dylib orte-clean
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib @rpath/libopen-pal.40.dylib orte-clean
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib        @rpath/libgcc_s.1.dylib     orte-clean

    # ompi-ps, orte-ps:
    install_name_tool -add_rpath @loader_path/../lib orte-ps
    install_name_tool -change $INSTALL_DIR/lib/libopen-rte.40.dylib @rpath/libopen-rte.40.dylib orte-ps
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib @rpath/libopen-pal.40.dylib orte-ps
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib        @rpath/libgcc_s.1.dylib     orte-ps

    # ompi-server, orte-server:
    install_name_tool -add_rpath @loader_path/../lib orte-server
    install_name_tool -change $INSTALL_DIR/lib/libopen-rte.40.dylib @rpath/libopen-rte.40.dylib orte-server
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib @rpath/libopen-pal.40.dylib orte-server
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib        @rpath/libgcc_s.1.dylib     orte-server

    # ompi-top, orte-top:
    install_name_tool -add_rpath @loader_path/../lib orte-top
    install_name_tool -change $INSTALL_DIR/lib/libopen-rte.40.dylib @rpath/libopen-rte.40.dylib orte-top
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib @rpath/libopen-pal.40.dylib orte-top
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib        @rpath/libgcc_s.1.dylib     orte-top

    # ompi_info
    install_name_tool -add_rpath @loader_path/../lib ompi_info
    install_name_tool -change $INSTALL_DIR/lib/libopen-rte.40.dylib @rpath/libopen-rte.40.dylib ompi_info
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib @rpath/libopen-pal.40.dylib ompi_info
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib      @rpath/libmpi.40.dylib      ompi_info
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib        @rpath/libgcc_s.1.dylib     ompi_info

    # opal_wrapper
    install_name_tool -add_rpath @loader_path/../lib opal_wrapper
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib @rpath/libopen-pal.40.dylib opal_wrapper
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib        @rpath/libgcc_s.1.dylib     opal_wrapper

    # orte-dvm
    install_name_tool -add_rpath @loader_path/../lib orte-dvm
    install_name_tool -change $INSTALL_DIR/lib/libopen-rte.40.dylib @rpath/libopen-rte.40.dylib orte-dvm
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib @rpath/libopen-pal.40.dylib orte-dvm
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib        @rpath/libgcc_s.1.dylib     orte-dvm

    # orte-info
    install_name_tool -add_rpath @loader_path/../lib orte-info
    install_name_tool -change $INSTALL_DIR/lib/libopen-rte.40.dylib @rpath/libopen-rte.40.dylib orte-info
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib @rpath/libopen-pal.40.dylib orte-info
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib        @rpath/libgcc_s.1.dylib     orte-info

    # orte-submit
    install_name_tool -add_rpath @loader_path/../lib orte-submit
    install_name_tool -change $INSTALL_DIR/lib/libopen-rte.40.dylib @rpath/libopen-rte.40.dylib orte-submit
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib @rpath/libopen-pal.40.dylib orte-submit
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib        @rpath/libgcc_s.1.dylib     orte-submit

    # ortecc
    install_name_tool -add_rpath @loader_path/../lib ortecc
    install_name_tool -change $INSTALL_DIR/lib/libopen-rte.40.dylib @rpath/libopen-rte.40.dylib ortecc
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib @rpath/libopen-pal.40.dylib ortecc
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib        @rpath/libgcc_s.1.dylib     ortecc

    # orted
    install_name_tool -add_rpath @loader_path/../lib orted
    install_name_tool -change $INSTALL_DIR/lib/libopen-rte.40.dylib @rpath/libopen-rte.40.dylib orted
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib @rpath/libopen-pal.40.dylib orted
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib        @rpath/libgcc_s.1.dylib     orted

# Note: libmpi_cxx dependency seems to be removed for OpenMPI 3.1.0 w. gcc 7.3.0.

    # ascii2exo
#   install_name_tool -change $INSTALL_DIR/lib/libmpi_cxx.1.dylib @rpath/libmpi_cxx.1.dylib ascii2exo
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib    @rpath/libmpi.40.dylib    ascii2exo

    # channel_mesh
#   install_name_tool -change $INSTALL_DIR/lib/libmpi_cxx.1.dylib @rpath/libmpi_cxx.1.dylib channel_mesh
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib    @rpath/libmpi.40.dylib    channel_mesh

    # divset
#   install_name_tool -change $INSTALL_DIR/lib/libmpi_cxx.1.dylib @rpath/libmpi_cxx.1.dylib divset
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib    @rpath/libmpi.40.dylib    divset

    # exo2fluent
#   install_name_tool -change $INSTALL_DIR/lib/libmpi_cxx.1.dylib @rpath/libmpi_cxx.1.dylib exo2fluent
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib    @rpath/libmpi.40.dylib    exo2fluent

    # exo2msh
#   install_name_tool -change $INSTALL_DIR/lib/libmpi_cxx.1.dylib @rpath/libmpi_cxx.1.dylib exo2msh
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib    @rpath/libmpi.40.dylib    exo2msh

    # exodiff
#   install_name_tool -change $INSTALL_DIR/lib/libmpi_cxx.1.dylib @rpath/libmpi_cxx.1.dylib exodiff
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib    @rpath/libmpi.40.dylib    exodiff

    # fluent2exo
#   install_name_tool -change $INSTALL_DIR/lib/libmpi_cxx.1.dylib @rpath/libmpi_cxx.1.dylib fluent2exo
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib    @rpath/libmpi.40.dylib    fluent2exo

    # forcenorm
#   install_name_tool -change $INSTALL_DIR/lib/libmpi_cxx.1.dylib @rpath/libmpi_cxx.1.dylib forcenorm
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib    @rpath/libmpi.40.dylib    forcenorm

    # hydra
    install_name_tool -change $INSTALL_DIR/lib/libmpi_usempif08.11.dylib @rpath/libmpi_usempif08.11.dylib hydra
    install_name_tool -change $INSTALL_DIR/lib/libmpi_mpifh.40.dylib     @rpath/libmpi_mpifh.40.dylib     hydra
#   install_name_tool -change $INSTALL_DIR/lib/libmpi_cxx.1.dylib        @rpath/libmpi_cxx.1.dylib        hydra
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib           @rpath/libmpi.40.dylib           hydra

    # lo
#   install_name_tool -change $INSTALL_DIR/lib/libmpi_cxx.1.dylib @rpath/libmpi_cxx.1.dylib lo
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib    @rpath/libmpi.40.dylib    lo

    # msh2exo
#   install_name_tool -change $INSTALL_DIR/lib/libmpi_cxx.1.dylib @rpath/libmpi_cxx.1.dylib msh2exo
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib    @rpath/libmpi.40.dylib    msh2exo

    # nccopy, ncdump, ncgen, ncgen3
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib  @rpath/libgcc_s.1.dylib   nccopy
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib  @rpath/libgcc_s.1.dylib   ncdump
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib  @rpath/libgcc_s.1.dylib   ncgen
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib  @rpath/libgcc_s.1.dylib   ncgen3

    # neu2exo
#   install_name_tool -change $INSTALL_DIR/lib/libmpi_cxx.1.dylib @rpath/libmpi_cxx.1.dylib neu2exo
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib    @rpath/libmpi.40.dylib    neu2exo

    # norm
#   install_name_tool -change $INSTALL_DIR/lib/libmpi_cxx.1.dylib @rpath/libmpi_cxx.1.dylib norm
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib    @rpath/libmpi.40.dylib    norm

    echo "Patching rpath in OpenMPI shared libraries"
    cd $INSTALL_DIR/lib

    # libgfortran.dylib
    install_name_tool -change $COMPILER_DIR/libquadmath.0.dylib   @rpath/libquadmath.0.dylib libgfortran.4.dylib
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib      @rpath/libgcc_s.1.dylib    libgfortran.4.dylib

    # libopen-rte.dylib, libopen-pal.dylib
    install_name_tool -change $INSTALL_DIR/libopen-pal.40.dylib  @rpath/libopen-pal.40.dylib  libopen-rte.40.dylib
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib     @rpath/libgcc_s.1.dylib      libopen-rte.40.dylib
    install_name_tool -change $INSTALL_DIR/libopen-pal.40.dylib  @rpath/libopen-pal.40.dylib  libopen-pal.40.dylib
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib     @rpath/libgcc_s.1.dylib      libopen-pal.40.dylib

    # libmca_common_sm.dylib
    install_name_tool -change $COMPILER_DIR/lib/libgcc_s.1.dylib     @rpath/libgcc_s.1.dylib      libmca_common_sm.4.dylib

    # libmpifh.dylib
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib       @rpath/libmpi.40.dylib       libmpi_mpifh.40.dylib
    install_name_tool -change $INSTALL_DIR/lib/libopen-rte.40.dylib  @rpath/libopen-rte.40.dylib  libmpi_mpifh.40.dylib
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib  @rpath/libopen-pal.40.dylib  libmpi_mpifh.40.dylib
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib         @rpath/libgcc_s.1.dylib      libmpi_mpifh.40.dylib

    # libmpi.dylib
    install_name_tool -change $INSTALL_DIR/lib/libopen-rte.40.dylib  @rpath/libopen-rte.40.dylib  libmpi.40.dylib
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib  @rpath/libopen-pal.40.dylib  libmpi.40.dylib
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib         @rpath/libgcc_s.1.dylib      libmpi.40.dylib

    # libmpi_cxx.dylib
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib       @rpath/libmpi.40.dylib       libmpi_cxx.1.dylib
    install_name_tool -change $INSTALL_DIR/lib/libopen-rte.40.dylib  @rpath/libopen-rte.40.dylib  libmpi_cxx.1.dylib
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib  @rpath/libopen-pal.40.dylib  libmpi_cxx.1.dylib
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib         @rpath/libgcc_s.1.dylib      libmpi_cxx.1.dylib

    # libmpi_usempif08.dylib
    install_name_tool -change $INSTALL_DIR/lib/libmpi_mpifh.40.dylib  @rpath/libmpi_mpifh.40.dylib libmpi_usempif08.11.dylib
    install_name_tool -change $INSTALL_DIR/lib/libmpi.40.dylib        @rpath/libmpi.40.dylib       libmpi_usempif08.11.dylib
    install_name_tool -change $INSTALL_DIR/lib/libopen-rte.40.dylib   @rpath/libopen-rte.40.dylib  libmpi_usempif08.11.dylib
    install_name_tool -change $INSTALL_DIR/lib/libopen-pal.40.dylib   @rpath/libopen-pal.40.dylib  libmpi_usempif08.11.dylib
    install_name_tool -change $COMPILER_DIR/lib/libgfortran.4.dylib   @rpath/libgfortran.4.dylib   libmpi_usempif08.11.dylib
    install_name_tool -change $COMPILER_DIR/lib/libquadmath.0.dylib   @rpath/libquadmath.0.dylib   libmpi_usempif08.11.dylib
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib          @rpath/libgcc_s.1.dylib      libmpi_upsempf08.11.dylib

    # libompitrace.dylib
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib      @rpath/libgcc_s.1.dylib      libompitrace.0.dylib

    # libquadmath.dylib
    install_name_tool -change $COMPILER_DIR/libgcc_s.1.dylib      @rpath/libgcc_s.1.dylib      libquadmath.0.dylib

    echo "Patching rpath in low-level OpenMPI libraries"
    cd $INSTALL_DIR/lib/openmpi
    install_name_tool -change $INSTALL_DIR/lib/libmca_common_sm.4.dylib @rpath/libmca_common_sm.4.dylib mca_btl_sm.so
    install_name_tool -change $INSTALL_DIR/lib/libmca_common_sm.4.dylib @rpath/libmca_common_sm.4.dylib mca_coll_sm.so
    install_name_tool -change $INSTALL_DIR/lib/libmca_common_sm.4.dylib @rpath/libmca_common_sm.4.dylib mca_mpool_sm.so

endif

# 6. Add license, export control, changelog
echo "Add licenses, export control, changelog"
cd $INSTALL_DIR
cp $BUILD_DIR/code/COPYRIGHT_and_LICENSE.md .
cp $BUILD_DIR/code/EXPORT_CONTROL.md .
cp $BUILD_DIR/code/CHANGELOG.md .

# 7. Generate distribution packages
echo "Generate distribution packages"
cd $INSTALL_DIR/..

tar cvzf $INSTALL_NAME.tar.gz $INSTALL_NAME

tar cvZf $INSTALL_NAME.tar.Z $INSTALL_NAME

# 8. Generate source distribution tarball
echo "Generate source distribution tarball"
if ($OSName == "Linux") then

    echo "Create the source tarball"

    # Setup the source files for the tarball
    cd $BUILD_DIR
    mkdir ../$INSTALL_NAME-src
    cp -r code ../$INSTALL_NAME-src
    cp -r tpl  ../$INSTALL_NAME-src
    cd ..

    tar cvzf $INSTALL_NAME-src.tar.gz \
        --exclude=.git \
        --exclude=.gitignore \
        --exclude=archive \
        --exclude=code/scripts/packagebuild \
        --exclude=code/scripts/nightly \
        --exclude="tpl/tarballs/parmetis*" \
        $INSTALL_NAME-src

endif
