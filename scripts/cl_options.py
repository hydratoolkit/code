#!/usr/bin/env python

'''
This module wraps optparse, to form a way of combining command-line options,
along with setting those same options in a config file.
'''

import optparse, os

# FUNCTIONS AND CLASSES ##############################################

def read_config_file(f, v):
    '''
    Reads config file f and sets parameters in p.values
    '''
    input_dict = {}
    # using execfile is a security hole, but we assume users who
    # know their config files
    execfile(f, input_dict)
    # check cl_opts_set so that command-line options override what is
    # in the config file.
    for k in input_dict.keys():
        if k in v.keys():
            v[k] = input_dict[k]

def cb_config_file(option, opt_str, value, p):
    '''
    This is a optparse callback function for the config file.  By using this,
    options specified before the config file are overwritten by values in the
    config file, while options specified after the config file overwrite those
    given in the config file.
    '''
    setattr(p.values, option.dest, value)
    # now read the config file, if it is there
    f = p.values.config_file
    if f is not None:
        if not os.path.exists(f):
            m = 'Unable to find config file: %s' % f
            p.error(m)
        read_config_file(f, p.values.__dict__)


# append this usage string in CL_Options
_my_usage = '''If the file %s is in the current working directory, this file
is read as a config file.  Command-line options override options set in %s.
After all options are parsed, %s_last is written, with all the options set for
the last invocation.
'''

class CL_Options:
    '''
    Parses and stores command line options and options set in a config file.

    Attributes:
    parser = a optparse.OptionParser instance
    opts = option values, set after invocation of parse()
    '''
    def __init__(self, usage, dotfile=None):
        fusage = usage
        if dotfile is not None:
            fusage += _my_usage % (dotfile, dotfile, dotfile)
        p = optparse.OptionParser(fusage)
        if dotfile is None: 
            # then support a config file.  Having both a dotfile and a config
            # file may cause inconsistencies, so we don't allow if for now.
            p.set_defaults(config_file=None)
            p.add_option('-c','--config_file', dest='config_file',
                         action='callback', callback=cb_config_file, nargs=1, type='string', 
                         help='Config file name. Options given on the command line after '
                         '--config-file overwrite those values set in the config file, '
                         'while optrions before --config-file are overwritten by values '
                         'specified in the config file [%default]')
        self.parser = p
        self.opts = None
        self.dotfile = dotfile
    def parse(self):
        '''
        Parses the command-line options ane returns a list of the remaining
        command-line arguments.
        '''
        # if available, read the dotfile to overwrite the default values
        if self.dotfile is not None and os.path.isfile(self.dotfile):
            read_config_file(self.dotfile, self.parser.defaults)
        # parse command-line options
        (self.opts, args) = self.parser.parse_args()
        # write the new dotfile
        if self.dotfile is not None:
            newdot = self.dotfile + '_last'
            fw = open(newdot, 'w')
            for e in self.opts.__dict__.keys():
                v = getattr(self.opts, e)
                fw.write('%s = %s\n' % (e, `v`))
        return args
    def add_store_true(self, short_opt='', long_opt='', dest='', help=''):
        '''
        This is a convience wrapper on add_option() with what would otherwise
        use action=store_true, to hide the callback syntax.
        '''
        self.parser.add_option(short_opt, long_opt, dest=dest, help=help, default=False,
                               action='store_true')
    def doc_string(self):
        '''
        Returns the documentation string for the options that are used.  This
        is useful for reporting back what the final values are for the
        options.
        '''
        nc = 1 # maximum variable length
        for e in self.opts.__dict__.keys():
            nc = max(nc, len(e))
        format = '\n%%%ds = %%s' % nc
        s = ''
        for e in self.opts.__dict__.keys():
            v = getattr(self.opts, e)
            s += format % (e, v)
        return s
    
# MAIN ###############################################################

# Test as:
#
#  ./cl_options -h
#  ./cl_options arg1 arg2
#  ./cl_options --cxx='c++' --config_file=config.darwin
#  ./cl_options --config_file=config.darwin --cxx='c++'
#  ./cl_options --config_file=nonexistant_file

if __name__ == '__main__':
    cl = CL_Options('Test of CL_Options')
    cl.parser.add_option('','--cxx', dest='cxx', help='c++ compiler [%default]', action='store',
                         default='g++')
    cl.parser.add_option('-v','--verbose', dest='verbose', help='verbose output [%default]',
                         action='store_true')
    args = cl.parse()
    print 'found args', args
    print 'Options for this run:'
    print cl.doc_string()
