The Hydra toolkit is written in C++ and provides a rich suite of
components that permits rapid application development, supports
multiple discretization techniques, provides I/O interfaces to permit
reading/writing multiple file formats for meshes, plot data,
time-history, surface-based and restart output.  Data registration is
used to provide the ability to register variables at appropriate
locations (e.g., node, element, dual-edge, etc.), and provides
integrated and automatic output and restart capabilities along with
memory management. The toolkit also provides run-time parallel domain
decomposition with data-migration for both static and dynamic
load-balancing.  Linear algebra is handled through an abstract virtual
interface that makes it possible to use popular libraries such as
PETSc (http://www.mcs.anl.gov/petsc/) and Trilinos
(http://trilinos.org/).  The use of output delegates provides the
ability to develop lightweight physics-specific output kernels with
minimal memory overhead that can be tailored to a specific physics,
e.g., computation of vorticity, helicity, enstrophy for large-eddy
simulation.
