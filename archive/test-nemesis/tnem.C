#include <iostream>
#include <stdlib.h>

#include <string.h>

// Required for nemesis/exodus
#include "exodusII.h"
#include "ne_nemesisI.h"

#define DBL

// 3-Node tri element
#define NNPE_TRI3        3  //!< No. of nodes per element
#define NUM_TRI3_EDGES   3  //!< No. of edges per element
#define NNPE_TRI3_EDGES  2  //!< No. of nodes per tri3 edge
#define NUM_TRI3_FACES   1  //!< No. of faces per element
#define NNPE_TRI3_FACES  3  //!< No. of nodes per tri3 face

// 4-Node quad element
#define NNPE_QUAD4       4  //!< No. of nodes per element
#define NUM_QUAD4_EDGES  4  //!< No. of edges per element
#define NNPE_QUAD4_EDGES 2  //!< No. of nodes per quad4 edge
#define NUM_QUAD4_FACES  1  //!< No. of faces per element
#define NNPE_QUAD4_FACES 4  //!< No. of nodes per quad4 face

// 4-Node tet element
#define NNPE_TET4        4   //!< No. of nodes per element
#define NUM_TET4_EDGES   6   //!< No. of edges per element
#define NNPE_TET4_EDGES  2   //!< No. of nodes per tet4 edge
#define NUM_TET4_FACES   4   //!< No. of faces per element
#define NNPE_TET4_FACES  3   //!< No. of nodes per tet4 face

// 8-Node hex element
#define NNPE_HEX8        8   //!< No. of nodes per element
#define NUM_HEX8_EDGES   12  //!< No. of edges per element
#define NNPE_HEX8_EDGES  2   //!< No. of nodes per hex8 edge
#define NUM_HEX8_FACES   6   //!< No. of faces per element
#define NNPE_HEX8_FACES  4   //!< No. of nodes per hex8 face


using namespace std;

int main(int narg, char* argv[]) {

#ifdef DBL
  int CPU_word_size = 8;
#else
  int CPU_word_size = 4;
#endif
  int IO_word_size  = 0;
  float version;
  char ftype[MAX_STR_LENGTH+1];
  int neid;

  if (narg < 3) {
    cout << "Usage: tnem exo_name nem_name" << endl;
    exit(0);
  }

  cout << "Exodus  file: " << argv[1] << endl;
  cout << "Nemesis file: " << argv[2] << endl;

  // Open the exodus & nemesis files using Exodus-II (gag & choke)
  cout << "Opening exodus file ..." << endl;
  IO_word_size = 0;
  int exid = ex_open(argv[1],EX_READ,&CPU_word_size,&IO_word_size,&version);
  if (exid < 0) {
    cout << "Error opening exodus file ..." << endl;
    exit(0);
  }
  IO_word_size = 0;
  neid = ex_open(argv[2],EX_READ,&CPU_word_size,&IO_word_size,&version);

  // Read the global data
  int nnp_global, nel_global, nblk_global, nnsets_global, nssets_global;
  int err = ne_get_init_global(neid, &nnp_global, &nel_global, &nblk_global,
			       &nnsets_global, &nssets_global);
  cout << "Nnp_global  = " << nnp_global << endl;
  cout << "Nel_global  = " << nel_global << endl;
  cout << "Nblk_global = " << nblk_global << endl;
  cout << "Nnsets_glob = " << nnsets_global << endl;
  cout << "Nssets_glob = " << nssets_global << endl;


//------------------------------------------------------------------------------
  //  Read element block parameters only for purpose of establishing the
  //  number of nodes in the element, Nnpe. Do this the lazy way, discarding
  //  most the information instead of saving it for later. 
  
  char elmtype[MAX_STR_LENGTH+1];
  int *ids = new int[nblk_global];
  int *num_elem_in_block  = new int[nblk_global];
  int *num_nodes_per_elem = new int[nblk_global];
  int num_attr;

  int Nel_tri3, Nel_quad4, Nel_tet4, Nel_hex8;
  Nel_tri3  = 0;
  Nel_quad4 = 0;
  Nel_tet4  = 0;
  Nel_hex8  = 0;

  err = ex_get_elem_blk_ids(exid, ids);

  if (err) {
    cout << "ERROR: EX_get_elem_blk_ids = " << err << endl;
    exit(0);
  }

#ifdef TESTOLA
// Read arrays: ids[], Nel_in_block[], Nnpe[]
// Move to struct
// Hold in struct until done ...
  Elblock* eb = new Eblock [];
struct Elblock {
  int Id;
  char [MAX_STR_LENGTH+1];
  Nel;
  Nnpe;
  Nattr;
};
#endif

  // using the element block parameters read the element block info
  for (int i=0; i<nblk_global; i++) {
    err = ex_get_elem_block(exid, 
			    ids[i], 
			    elmtype,
                            &num_elem_in_block[i], 
                            &num_nodes_per_elem[i], 
			    &num_attr);
    if (err) {
      cout << "ERROR: EX_get_elem_block = " << err << endl;
      exit(0);
    }

    if (!strncasecmp(elmtype,"TRI",  3) && num_nodes_per_elem[i] == NNPE_TRI3 ){
      Nel_tri3 += num_elem_in_block[i];
    }
    if (!strncasecmp(elmtype,"QUAD", 4) && num_nodes_per_elem[i] == NNPE_QUAD4){
      Nel_quad4 += num_elem_in_block[i];
    }
    // Hack for weird exodus files
    if (!strncasecmp(elmtype,"SHELL",5) && num_nodes_per_elem[i] == NNPE_TET4 ){
      Nel_quad4 += num_elem_in_block[i];
    }
    if (!strncasecmp(elmtype,"TET",  3) && num_nodes_per_elem[i] == NNPE_TET4 ){
      Nel_tet4 += num_elem_in_block[i];
    }
    if (!strncasecmp(elmtype,"HEX",  3) && num_nodes_per_elem[i] == NNPE_HEX8 ){
      Nel_hex8 += num_elem_in_block[i];
    }
  }

  cout << "Global element counts by type:" << endl;
  cout << "Nel_tri3  = " << Nel_tri3  << endl;
  cout << "Nel_quad4 = " << Nel_quad4 << endl;
  cout << "Nel_tet4  = " << Nel_tet4  << endl;
  cout << "Nel_hex8  = " << Nel_hex8  << endl;

//------------------------------------------------------------------------------

  // Read the processor data -- use to check with #of proc's executing
  int num_proc, num_proc_in_file;
  err = ne_get_init_info(neid, &num_proc, &num_proc_in_file, ftype);
  cout << "Num. processors    = " << num_proc << endl;
  cout << "Num. proc. in file = " << num_proc_in_file << endl;
  cout << "File type: " << ftype << endl;

  // Read the processor-local data
  int t3_total = 0;
  int q4_total = 0;
  for (int i=0; i<num_proc; i++) {
    cout << endl;
    cout << "Proc id = " << i << endl;
    int nnp_int, nnp_brdr, nnp_ext, nel_int, nel_brdr, n_ncmap, n_ecmap;
    int err = ne_get_loadbal_param(neid, 
				   &nnp_int, &nnp_brdr, &nnp_ext,
				   &nel_int, &nel_brdr, &n_ncmap, &n_ecmap,
				   i);

    cout << "Processor " << i << " total counts" << endl;
    cout << "Nel total = " << nel_int + nel_brdr << endl;
    cout << "nnp_int  = " << nnp_int  << endl;
    cout << "nnp_brdr = " << nnp_brdr << endl;
    cout << "nnp_ext  = " << nnp_ext  << endl;
    cout << "nel_int  = " << nel_int  << endl;
    cout << "nel_brdr = " << nel_brdr << endl;
    cout << "n_ncmap  = " << n_ncmap  << endl;
    cout << "n_ecmap  = " << n_ecmap  << endl;

    // Read local-to-global element maps  
    // The maps contain global element numbers that are 0-based
    cout << endl;
    cout << "Element Local-to-global mapping ..." << endl;
    int* elem_mapi = new int [nel_int];
    int* elem_mapb = new int [nel_brdr];
    err = ne_get_elem_map(neid, elem_mapi, elem_mapb, i);

#ifdef DONT
    cout << "Processor i " << i << endl;
    int el = 0;
    for (int j=0; j<nel_int; j++) {
      cout << "el, elem_mapi[j] = " << el << ", " << elem_mapi[j] << endl;
      el++;
    }
    for (int j=0; j<nel_brdr; j++) {
      cout << "el, elem_mapb[j] = " << el << ", " << elem_mapb[j] << endl;
      el++;
    }
#endif

    int* node_map = new int [nnp_int+nnp_brdr];
    int* node_map_ext = NULL;
    if (nnp_ext > 0) node_map_ext = new int [nnp_ext];
      err = ne_get_node_map(neid, node_map, &node_map[nnp_int], 
			    node_map_ext, i);
#ifdef DONT
    cout << "Processor Id = " << i << endl;
    for (int j=0; j<(nnp_int+nnp_brdr); j++) {
      cout << "j, node_map[j] = " << j << ", " << node_map[j] << endl;
    }
#endif

//------------------------------------------------------------------------------
    // Scan the element blocks for elements on processor?
    int iel = 0; // Global element counter
    int nt3 = 0; // initialize element counter
    int nq4 = 0; // initialize element counter
    int nt4 = 0; // initialize element counter
    int nh8 = 0; // initialize element counter
    for (int k=0; k<nblk_global; k++) {

      err = ex_get_elem_block(exid, 
			      ids[k], 
			      elmtype,
			      &num_elem_in_block[k],
			      &num_nodes_per_elem[k],
			      &num_attr);

      // Now scan to see if the elements in this block are on processor-i
      if (!strncasecmp(elmtype, "TRI",   3) && 
          num_nodes_per_elem[k] == NNPE_TRI3 ) {
        cout << "Proc, block = " << i << ", " << k << endl;
        cout << "Scanning tri's ..." << endl;
        int iel_start = iel;
        int iel_end   = iel + num_elem_in_block[k] - 1;
        cout << "Block = " << k << endl;
        cout << "start/end = " << iel_start << ", " << iel_end << endl;
	for (int l=0; l<nel_int; l++) {
          if (elem_mapi[l] >= iel_start && elem_mapi[l] <= iel_end) nt3++;
	}
	for (int l=0; l<nel_brdr; l++) {
          if (elem_mapb[l] >= iel_start && elem_mapb[l] <= iel_end) nt3++;
	}
      }

      if (!strncasecmp(elmtype, "QUAD", 4) && 
          num_nodes_per_elem[k] == NNPE_QUAD4 ) {
        cout << "Proc, block = " << i << ", " << k << endl;
        cout << "Scanning quad's ..." << endl;
        int iel_start = iel;
        int iel_end   = iel + num_elem_in_block[k] - 1;
        cout << "Block = " << k << endl;
        cout << "start/end = " << iel_start << ", " << iel_end << endl;
	for (int l=0; l<nel_int; l++) {
          if (elem_mapi[l] >= iel_start && elem_mapi[l] <= iel_end) nq4++;
	}
	for (int l=0; l<nel_brdr; l++) {
          if (elem_mapb[l] >= iel_start && elem_mapb[l] <= iel_end) nq4++;
	}
      }

      iel += num_elem_in_block[k];
    } // End of global block loop

    cout << "Processor-" << i << " Total Nel    = " << nt3+nq4+nt4+nh8 << endl;
    cout << "Processor-" << i << " No. of TRI3  = " << nt3 << endl;
    cout << "Processor-" << i << " No. of QUAD4 = " << nq4 << endl;
    cout << "Processor-" << i << " No. of TET4  = " << nt4 << endl;
    cout << "Processor-" << i << " No. of HEX8  = " << nh8 << endl;

    t3_total += nt3;
    q4_total += nq4;

    delete [] elem_mapi;
    delete [] elem_mapb;

  } // End of processor loop

  cout << endl;
  cout << "!!!!! GLOBAL ELEMENT COUNT !!!!!" << endl;
  cout << "TRI3  = " << t3_total << endl;
  cout << "QUAD4 = " << q4_total << endl;
  cout << endl;

//------------------------------------------------------------------------------

  for (int i=0; i<num_proc; i++) {

    int nnp_int, nnp_brdr, nnp_ext, nel_int, nel_brdr, n_ncmap, n_ecmap;
    int err = ne_get_loadbal_param(neid, 
				   &nnp_int, &nnp_brdr, &nnp_ext,
				   &nel_int, &nel_brdr, &n_ncmap, &n_ecmap,
				   i);

    cout << "Number of node comm maps = " << n_ncmap << endl;
    cout << "Number of elem comm maps = " << n_ecmap << endl;

    // Read processor map data: id's, counts, element lists
    int* node_cmap_ids       = new int [n_ncmap];
    int* node_cmap_node_cnts = new int [n_ncmap];
    int* elem_cmap_ids       = new int [n_ecmap];
    int* elem_cmap_elem_cnts = new int [n_ecmap];
    err = ne_get_cmap_params(neid, 
			     node_cmap_ids, 
			     node_cmap_node_cnts,
			     elem_cmap_ids, 
			     elem_cmap_elem_cnts, 
			     i);

    cout << "Processor Id = " << i << endl;
    cout << "No. of comm maps for nodes: " << n_ncmap << endl;
    for (int j=0; j<n_ncmap; j++) {
      cout << "j = " << j  << endl
	   << "node_cmap_ids[j] = " << node_cmap_ids[j]  << endl
	   << "node_cmap_cnts[j] = " << node_cmap_node_cnts[j] 
	   << endl;
    }

    cout << "No. of nodes in cmap = " << node_cmap_node_cnts[0] << endl;
    int* node_ids = new int [node_cmap_node_cnts[0]];
    int* node_proc_ids = new int [node_cmap_node_cnts[0]]; // TOO BIG
    err = ne_get_node_cmap(neid, 
			   node_cmap_ids[0], 
			   node_ids,
			   node_proc_ids,
			   i);

    for (int j=0; j<node_cmap_node_cnts[0]; j++) {
      cout << "j, node, proc = " 
	   << j << ", " 
	   << node_ids[j] << ", " 
	   << node_proc_ids[j] << endl;
    }

    cout << "Processor Id = " << i << endl;
    cout << "No. of comm maps for elements: " << n_ecmap << endl;
    for (int j=0; j<n_ecmap; j++) {
      cout << "j = " << j << endl
	   << "elem_cmap_ids[j] = " << elem_cmap_ids[j]  << endl
	   << "elem_cmap_elem_cnts[j] = " << elem_cmap_elem_cnts[j] 
	   << endl;
    }

    // Read element map data -- need to loop over element counts
    // Global element numbers, side id's (1-based?) and proc. id's
    // Need to munge this into gila-style comm data
    cout << "No. of elements in cmap = " << elem_cmap_elem_cnts[0] << endl;
    int* elem_ids = new int [elem_cmap_elem_cnts[0]];
    int* side_ids = new int [elem_cmap_elem_cnts[0]];
    int* proc_ids = new int [elem_cmap_elem_cnts[0]];
    err = ne_get_elem_cmap(neid, 
			   elem_cmap_ids[0], 
			   elem_ids, 
			   side_ids, 
			   proc_ids, 
			   i);

    int* proclist = new int [num_proc];
    for (int k=0; k<num_proc; k++) proclist[k] = -1;
    // Count up number of processors to communicate with by traversing the 
    // list of elements in the communication map
    for (int j=0; j<elem_cmap_elem_cnts[0]; j++) {
         cout << "j, elem, side, proc = " 
   	   << j           << ", " 
   	   << elem_ids[j] << ", " 
   	   << side_ids[j] << ", " 
   	   << proc_ids[j] << endl;
      proclist[proc_ids[j]] = proc_ids[j];
    }
    cout << "Processor communication list" << endl;
    int npcm = 0;
    for (int k=0; k<num_proc; k++) {
      if (proclist[k] >= 0) {
	cout << "Proc " << i << " talks to " << proclist[k] << endl;
        npcm++;
      }
    }
    cout << "Proc " << i << " talks to " << npcm << " processors" << endl;

    // Cleanup comm-map junk
    delete [] node_cmap_ids;
    delete [] node_cmap_node_cnts;
    delete [] elem_cmap_ids;
    delete [] elem_cmap_elem_cnts;
    delete [] node_ids;
    delete [] node_proc_ids;
    delete [] elem_ids;
    delete [] side_ids;
    delete [] proc_ids;
  } // End of loop over processors

//******************************************************************************
  // Read nodes w. a stripmine
  int nbuf = 512;
  double* x = new double [nbuf];
  double* y = new double [nbuf];
  double* z = 0;

  int nstrip = nnp_global/nbuf + 1;
  int is = 1; // Starting node number is 1-based!!!
  int ie = nbuf;
  for (int i=0; i<nstrip; i++) {
    if (i == nstrip - 1) ie = nnp_global - (nstrip-1)*nbuf;
    // Starting node number is 1-based!!!
    err = ne_get_n_coord(exid, is, ie, x, y, z);
    if (err < 0) {
      cout << "Error reading block of coordinates ..." << endl;
      exit(0);
    }

    cout << "Stripmine step = " << i << endl;
//  cout << "node  x_coor y_coor" << endl;
//  for (int i=0; i<ie; i++) {
//    cout << is+i << "  " << x[i] << "  " << y[i] << endl;
//  }
    is += ie;
  }
  delete [] x;
  delete [] y;

//******************************************************************************

  err = ex_close(exid);
  err = ex_close(neid);
}
