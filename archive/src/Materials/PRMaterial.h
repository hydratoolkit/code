//******************************************************************************
//! \file    src/Materials/PrandtlReussStrength.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Prandtl-Reuss Material Strength
//******************************************************************************

#ifndef PrandtlReussStrength_h
#define PrandtlReussStrength_h

#include <DataContainer.h>
#include <Material.h>

namespace Hydra {

class PRMaterial: public Material {

  public:

    enum PRMatParm {PR_A = 0,        //!< Yield Stress
                    PR_B,            //!< Hardening parameter
                    PR_n,            //!< Exponential factor
                    PR_SHEARMOD,     //!< Shear modulus for the material
                    PRNUM_MAT_PARMS
    };

    enum MGMatParm {MG_RHO = 0, //!< Reference Density (copied from mat_parm)
                    MG_GAMMA,   //!< Reference Mie-Gruniesen (copied from mat_parm)
                    MG_CREF,    //!< Shock speed (Mie Gruneisen Parameter)
                    MG_SREF,    //!< linear Us-Up slope (Mie Gruneisen Parameter)
                    MG_CV,      //!< Specific heat at constant volume
                    MG_CP,      //!< Specific heat at constant pressure
                    MGNUM_MAT_PARMS
    };

    //! \name Constructor/Destructor
    //@{
             PRMaterial(DataContainer& mesh,
                        int   matid,
                        Real* JCdata,
                        Real* MGdata);
    virtual ~PRMaterial() {}
    //@}

    //! \name Material model interface
    //@{
    // This returns constant shear modulus
    Real PRParam_A()           {return PRmat_parm[PR_A];}
    Real PRParam_B()           {return PRmat_parm[PR_B];}
    Real PRParam_n()           {return PRmat_parm[PR_n];}

    Real MGParm_Rho()          {return MGmat_parm[MG_RHO];}
    Real MGParm_Gamma()        {return MGmat_parm[MG_GAMMA];}
    Real MGParm_Cref()         {return MGmat_parm[MG_CREF];}
    Real MGParm_Sref()         {return MGmat_parm[MG_SREF];}
    //@}

    //! virtual functions
    //@{
    // EOS
    virtual bool hasMaterialStrength(){return true;}
    virtual Real SoundSpeed(Real& e, Real& rho, Real& press);
    virtual Real Pressure(Real &e, Real& rho);
    virtual Real InternalEnergy(Real &press, Real& rho);
    virtual Real slopeShockParticleVel(){return MGmat_parm[MG_SREF];}

    // Strength
    virtual Real yieldStress(Real& e, Real& epsbar, Real& epsdot);
    virtual Real slopeStressStrainCurve(Real& e, Real& epsbar, Real& epsdot);
    virtual Real shearModulus(){return PRmat_parm[PR_SHEARMOD];}
    virtual void echoStrengthParam(ostream& ofs);
    //@}

    void setData(Real* PRdata, Real* MGdata);

  private:

    //! Don't permit copy or assignment operators
    //@{
    PRMaterial(const Material&);
    PRMaterial& operator=(const Material&);
    //@}

    Real PRmat_parm[PRNUM_MAT_PARMS];  //!< PR Material parameters
    Real MGmat_parm[MGNUM_MAT_PARMS];  //!< Mie Gruneisen Material Parameters
};

}

#endif // PrandtlReussStrength_h
