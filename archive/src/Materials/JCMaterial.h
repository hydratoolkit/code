//******************************************************************************
//! \file    src/Materials/JohnsonCookStrength.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Johnshon-Cook material strength model
//******************************************************************************

#ifndef JCMaterial_h
#define JCMaterial_h

#include <DataContainer.h>
#include <Material.h>

namespace Hydra {

const int JC_PARAMS_LIMIT = 50;
const int MG_PARAMS_LIMIT = 50;

class JCMaterial: public Material {

  public:

    enum JCMatParm {JC_A = 0,       //!< Yield Stress
                    JC_B,       //!< Hardening parameter
                    JC_C,       //!< Lograthmic Constant
                    JC_n,       //!< Exponential on lograthmic strain-rate
                    JC_m,       //!< Melt exponent
                    JC_TMELT,   //!< Melting Temperature
                    JC_TREF,    //!< Reference Temperature
                    JC_SHEARMOD,//!< Shear Modulus for the material
                    JCNUM_MAT_PARMS};
    enum MGMatParm {MG_RHO = 0,     //!< Reference Density (copied from mat_parm)
                    MG_GAMMA,   //!< Reference Mie-Gruniesen (copied from mat_parm)
                    MG_CREF,    //!< Shock speed (Mie Gruneisen Parameter)
                    MG_SREF,    //!< linear Us-Up slope (Mie Gruneisen Parameter)
                    MG_CV,      //!< Specific heat at constant volume
                    MG_CP,      //!< Specific heat at constant pressure
                    MGNUM_MAT_PARMS};

    //! \name Constructor/Destructor
    //@{
             JCMaterial(DataContainer& mesh,
                        int   matid,
                        Real* JCdata,
                        Real* MGdata);
    virtual ~JCMaterial() {}
    //@}

    //! \name Material model interface
    //@{
    // This returns constant shear modulus
    Real JCParam_A()           {return JCmat_parm[JC_A];}
    Real JCParam_B()           {return JCmat_parm[JC_B];}
    Real JCParam_C()           {return JCmat_parm[JC_C];}
    Real JCParam_n()           {return JCmat_parm[JC_n];}
    Real JCParam_m()           {return JCmat_parm[JC_m];}
    Real JCParam_Tmelt()       {return JCmat_parm[JC_TMELT];}

    Real MGParm_Rho()          {return MGmat_parm[MG_RHO];}
    Real MGParm_Gamma()        {return MGmat_parm[MG_GAMMA];}
    Real MGParm_Cref()         {return MGmat_parm[MG_CREF];}
    Real MGParm_Sref()         {return MGmat_parm[MG_SREF];}
    //@}

    //! virtual functions
    //@{
    virtual bool hasMaterialStrength(){return true;}
    virtual Real slopeShockParticleVel(){return MGmat_parm[MG_SREF];}
    virtual Real SoundSpeed(Real& e, Real& rho, Real& press);
    virtual Real Pressure(Real &e, Real& rho);
    virtual Real InternalEnergy(Real &press, Real& rho);
    virtual Real yieldStress(Real& e, Real& epsbar, Real& epsdot);
    virtual Real slopeStressStrainCurve(Real& e, Real& epsbar, Real& epsdot);
    virtual Real shearModulus(){return JCmat_parm[JC_SHEARMOD];}
    virtual Real meltingTemp(){return JCmat_parm[JC_TMELT];}
    //@}

    void setData(Real* JCdata, Real* MGdata);

  private:

    //! Don't permit copy or assignment operators
    //@{
    JCMaterial(const Material&);
    JCMaterial& operator=(const Material&);
    //@}

    Real JCmat_parm[JC_PARAMS_LIMIT]; //!< JC Material parameters
    Real MGmat_parm[MG_PARAMS_LIMIT]; //!< Mie Gruneisen Material Parameters
};

}

#endif // JCMaterial_h
