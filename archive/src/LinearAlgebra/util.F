C******************************************************************************
C \file    src/LinearAlgebra/Util.F
C \author  Mark A. Christon
C \date    Thu Jul 14 11:50:35 2011
C \brief   Utility functions for linear algebra
C******************************************************************************
C
C HYDRA: an extensible multiphysics code.
C
C******************************************************************************/
C
      subroutine accumv(     v,    dv,    nw)
C-----------------------------------------------------------------------
C
C* Routine : accumv - accumulate vectors
C
C Argument |    Meaning
C =========|============================================================
C v        | vector to be accumulated into
C dv       | increment for v
C nw       | # of words
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension v(nw), dv(nw)

C ... Add in the advective flux
      do i = 1, nw
         v(i) = v(i) + dv(i)
      end do

      return
      end

      subroutine blkcpy(   src,   dst,     n)
C-----------------------------------------------------------------------
C
C* Routine : blkcpy - copy a piece of memory
C
C Argument |    Meaning
C =========|============================================================
C src      | source
C dst      | destination
C n        | # of words to copy
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension src(n), dst(n)
      do 100 i = 1, n
         dst(i) = src(i)
  100 continue
      return
      end

      subroutine ibkcpy(  isrc,  idst,     n)
C-----------------------------------------------------------------------
C
C* Routine : ibkcpy - copy a piece of memory
C
C Argument |    Meaning
C =========|============================================================
C isrc     | source
C idst     | destination
C n        | # of words to copy
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
      dimension isrc(n), idst(n)

      do 100 i = 1, n
         idst(i) = isrc(i)
  100 continue
      return
      end

      subroutine irenum(    ia,    ib,    na,    nb)
C-----------------------------------------------------------------------
C
C* Routine: irenum - integer runumber of ia according to ib(ia(i))
C
C Argument |    Meaning
C =========|============================================================
C ia       | input vector to be renumbered
C ib       | new numbering scheme
C na       | sizeof ia
C nb       | sizeof ib
C
C* Author :  Mark A. Christon
C
C-----------------------------------------------------------------------
      dimension ia(na), ib(nb)

      do 100 i = 1, na
         ii    = ia(i)
         ia(i) = ib(ii)
  100 continue

      return
      end

      subroutine copyfi(   src,   dst,    nw,  ndim,  icol)
C-----------------------------------------------------------------------
C
C* Routine : copyfi - copy from ith column of src to dst
C
C Argument |    Meaning
C =========|============================================================
C          |
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension src(nw,ndim), dst(nw)

      do 100 i = 1, nw
         dst(i) = src(i,icol)
  100 continue

      return
      end

      subroutine copyti(   src,   dst,    nw,  ndim,  icol)
C-----------------------------------------------------------------------
C
C* Routine : copyti - copy src to ith column of dst
C
C Argument |    Meaning
C =========|============================================================
C          |
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension src(nw), dst(nw,ndim)

      do 100 i = 1, nw
         dst(i,icol) = src(i)
  100 continue

      return
      end

      subroutine getdel(     a,     b,     nw,  ityp)
C-----------------------------------------------------------------------
C
C* Routine : getdel - compute the increment in a vector
C
C Argument |    Meaning
C =========|============================================================
C          |
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension a(nw), b(nw)

      if( ityp .eq. -1 ) s = -1.0
      if( ityp .eq.  1 ) s =  1.0
      do 100 i = 1, nw
         a(i) = a(i) + s*b(i)
  100 continue

      return
      end

      subroutine invvec(     a,    ai,    nw)
C-----------------------------------------------------------------------
C
C* Routine : invvec - invert a vector
C
C Argument |    Meaning
C =========|============================================================
C a        | source vector
C ai       | dest.  vector
C nw       | # of words
C
C* Author:  Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension a(nw), ai(nw)

      do 100 i = 1, nw
         ai(i) = 1.0/a(i)
  100 continue

      return
      end

      subroutine izero (    ia,     n )
C-----------------------------------------------------------------------
C
C* Routine : izero - zero an integer 2-D array
C
C Argument |    Meaning
C =========|============================================================
C ia       | array
C n        | dim. of ia
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension ia(n)
      do 100 i = 1, n
         ia(i) = 0
  100 continue
      return
      end

      subroutine izero2(    ia,     n,      m )
C-----------------------------------------------------------------------
C
C* Routine : izero2 - zero a 2-D integer array
C
C Argument |    Meaning
C =========|============================================================
C ia       | array
C n        | 1rst dim. of ia
C m        | 2nd  dim. of ia
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension ia(n,m)
      do 100 j = 1, m
         do 100 i = 1, n
            ia(i,j) = 0
  100 continue
      return
      end

      subroutine r_one (     a,     n)
C-----------------------------------------------------------------------
C
C* Routine : r_one - initialize a real array with 1.0
C
C Argument |    Meaning
C =========|============================================================
C a        | array
C n        | # of words
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension a(n)
      do 100 i = 1, n
         a(i) = 1.0
  100 continue
      return
      end

      subroutine rzero (     a,     n)
C-----------------------------------------------------------------------
C
C* Routine : rzero - zero a real array
C
C Argument |    Meaning
C =========|============================================================
C a        | array
C n        | # of words
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension a(n)
      do i = 1, n
         a(i) = 0.0
      end do
      return
      end

      subroutine rzero2(     a,     n,     m)
C-----------------------------------------------------------------------
C
C* Routine : rzero2 - zero a real 2-D array
C
C Argument |    Meaning
C =========|============================================================
C a        | array
C n        | 1rst dim. of a
C m        | 2nd  dim. of a
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension a(n,m)
      do j = 1, m
         do i = 1, n
            a(i,j) = 0.0
         end do
      end do
      return
      end

      subroutine rzeroi(     a,  indx,   nwa,   nwi)
C-----------------------------------------------------------------------
C
C* Routine : rzeroi - zero a real array according to and index
C
C Argument |    Meaning
C =========|============================================================
C a        | array
C indx     | index array
C nwa      | # of words in a
C nwi      | # of words in indx
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension a(nwa), indx(nwi)
      do 100 i = 1, nwi
         a(indx(i)) = 0.0
  100 continue
      return
      end


#ifdef HYDRAF77_BLAS
#ifdef DBL
      subroutine daxpy (     n,     s,     x,   isx,     y,   isy)
#else
      subroutine saxpy (     n,     s,     x,   isx,     y,   isy)
#endif
C-----------------------------------------------------------------------
C
C* Routine : saxpy - perform a saxpy operation (BLAS compatible)
C
C Argument |    Meaning
C =========|============================================================
C n        | # of words
C s        | scalar
C x        | x-vector
C isx      | x-stride
C y        | y-vector
C isx      | y-stride
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension x(n), y(n)
      ix = 1
      iy = 1
      do 100 i = 1, n
         y(iy) = y(iy) + s*x(ix)
         ix = ix + isx
         iy = iy + isy
  100 continue
      return
      end

#ifdef DBL
      subroutine dcopy (     n,   src,   is1,   dst,   is2)
#else
      subroutine scopy (     n,   src,   is1,   dst,   is2)
#endif
C-----------------------------------------------------------------------
C
C* Routine : scopy - copy a piece of memory (SCILIB compatible)
C
C Argument |    Meaning
C =========|============================================================
C n        | # of words
C src      | source vector
C is1      | stride of source
C dst      | destination vector
C is2      | stride of destination
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension src(n), dst(n)
      i1 = 1
      i2 = 1
      do 100 i = 1, n
         dst(i2) = src(i1)
         i1 = i1 + is1
         i2 = i2 + is2
  100 continue
      return
      end

      function   sdot  (     n,    v1,   is1,    v2,   is2)
C-----------------------------------------------------------------------
C
C* Function: sdot - perform a dot-product operation (SCILIB compatible)
C
C Argument |    Meaning
C =========|============================================================
C n        | # of words
C v1       | vector #1
C is1      | stride of vector #1
C v2       | vector #2
C is2      | stride of vector #2
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension v1(n), v2(n)
      sum = 0.0
      i1 = 1
      i2 = 1
      do 100 i = 1, n
         sum = sum + v1(i1)*v2(i2)
         i1 = i1 + is1
         i2 = i2 + is2
  100 continue
C ... Handle sum across multiple processors
      call mpdsum( sum, work, 1 )
      sdot = sum
      return
      end

#endif

      subroutine scattr(   dst,   src,  indx,    nd,    ns)
C-----------------------------------------------------------------------
C
C* Routine : scattr - scatter src into dst according to indx
C
C Argument |    Meaning
C =========|============================================================
C dst      | destination array
C src      | source array
C indx     | index vector
C nd       | size of dst
C ns       | size of src
C
C* Author:  Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension dst(nd), src(ns), indx(ns)

      do 100 i = 1, ns
         dst(indx(i)) = src(i)
  100 continue

      return
      end

      subroutine stindx(  indx,     n)
C-----------------------------------------------------------------------
C
C* Routine : stindx - setup a sequential index, e.g., proc. list
C
C Argument |    Meaning
C =========|============================================================
C indx     | integer index
C n        | size of indx
C
C* Author  :Mark A. Christon
C
C-----------------------------------------------------------------------
      dimension indx(n)

      do 100 i = 1, n
         indx(i) = i
  100 continue

      return
      end

      subroutine sumvec(     v,   sum,    n)
C-----------------------------------------------------------------------
C
C* Routine : sumvec - sum a vector
C
C Argument |    Meaning
C =========|============================================================
C v        | input vector
C sum      | sum over v
C n        | # of elements in v
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension v(n)
      sum = 0.0
      do i = 1, n
         sum = sum + v(i)
      end do

      call mpdsum( sum, work, 1 )

      return
      end

      subroutine vrepet(    vi,    vo,    nw,  nrep)
C-----------------------------------------------------------------------
C
C* Routine : vrepet - replicate input vector nrep times in output vector
C
C Argument |    Meaning
C =========|============================================================
C vi       | input vector
C vo       | output vector
C nw       | # of words
C nrep     | # of repeats
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension vi(nw)
      dimension vo(nw,nrep)

      do i = 1, nw
         do j = 1, nrep
            vo(i,j) = vi(i)
         end do
      end do

      return
      end

      function   pdot  (     n,    v1,   is1,    v2,   is2)
C-----------------------------------------------------------------------
C
C* Function: pdot  - perform a dot-product operation (BLAS compatible)
C
C Argument |    Meaning
C =========|============================================================
C n        | # of words
C v1       | vector #1
C is1      | stride of vector #1
C v2       | vector #2
C is2      | stride of vector #2
C
C* Author  : Mark A. Christon
C
C-----------------------------------------------------------------------
#ifdef DBL
      implicit double precision (a-h,o-z)
#endif
      dimension v1(n), v2(n)

#ifdef HYDRAF77_BLAS
      sum  = 0.0
      i1 = 1
      i2 = 1
      do i = 1,n
        sum = sum + v1(i1)*v2(i2)
        i1 = i1 + is1
        i2 = i2 + is2
      end do

c     sum  = 0.0
c     sum1 = 0.0
c     sum2 = 0.0
c     sum3 = 0.0
c     sum4 = 0.0
c     i1 = 1
c     i2 = 1
c     ie = mod(n,4)
c     is = ie+1
c     do i = 1,ie
c       sum = sum + v1(i1)*v2(i2)
c       i1 = i1 + is1
c       i2 = i2 + is2
c     end do
c     do i = is,n,4
c       i11 = i1  + is1
c       i12 = i11 + is1
c       i13 = i12 + is1
c       i21 = i2  + is2
c       i22 = i21 + is2
c       i23 = i22 + is2
c       sum1 = sum1 + v1(i1 )*v2(i2 )
c       sum2 = sum2 + v1(i11)*v2(i21)
c       sum3 = sum3 + v1(i12)*v2(i22)
c       sum4 = sum4 + v1(i13)*v2(i23)
c       i1 = i1 + 4*is1
c       i2 = i2 + 4*is2
c     end do
c     sum = sum + sum1 + sum2 + sum3 + sum4
#else
#ifdef DBL
      sum = ddot (     n,    v1,   is1,    v2,   is2)
#else
      sum = sdot (     n,    v1,   is1,    v2,   is2)
#endif
#endif
C ... Handle sum across multiple processors
      call mpdsum( sum, work, 1 )
      pdot = sum

      return
      end
