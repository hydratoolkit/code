#ifndef HYDRA_OPTIONS_H
#define HYDRA_OPTIONS_H

//******************************************************************************
/*!
  \file    src/Base/options.h
  \author  mark
  \date    Thu Jul 14 11:50:35 2011
  \brief   Preformatted list of analysis options for data-echo purposes.
 */
//******************************************************************************

namespace Hydra {

// DOF names
const char* dof_name[] = {
  "X-displacement: DOF ID", 
  "Y-displacement: DOF ID", 
  "Z-displacement: DOF ID", 
  "X-velocity: DOF ID", 
  "Y-velocity: DOF ID", 
  "Z-velocity: DOF ID", 
  "Temperature: DOF ID"
};

}

#endif
