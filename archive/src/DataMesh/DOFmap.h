//******************************************************************************
/*!
  \file    src/DataMesh/DOFmap.h
  \author  mark
  \date    Thu Jul 14 12:32:02 2011
  \brief   DOF map -- old style for BC's
 */
//******************************************************************************
#ifndef DOFmap_h
#define DOFmap_h

namespace Hydra {

// Mapping of DOF for Nodal BC's
enum DOFmap {XDISPLACEMENT=0,
             YDISPLACEMENT,
             ZDISPLACEMENT,
             XVELOCITY,
             YVELOCITY,
             ZVELOCITY,
             TEMP,
             MAXDOF};

}

#endif // DOFmap_h
