//******************************************************************************
/*!
  \file    src/ParseControl/NavierStokesParse.h
  \author  mark
  \date    Thu Jul 14 12:58:46 2011
  \brief   
 */
//******************************************************************************

#ifndef NavierStokesParse_h
#define NavierStokesParse_h

namespace Hydra {

// Lagrangian Dynamics Analysis keywords
typedef bool 
(*NavStokes_Keyword_Function)(TokenStream*, UnsMesh*, Control*, physicsManager&);

typedef struct {
  const char* name;
  NavStokes_Keyword_Function func;
} NavStokes_Keyword;

// Lagrangian Dynamics Keywords
bool parseNavierStokes(TokenStream*, fileIO*, UnsMesh*, Control*, physicsManager&);

bool parseDIVU(TokenStream*, UnsMesh*, Control*, physicsManager&);
bool parseElSet(TokenStream*, UnsMesh*, Control*, physicsManager&);

void setNavierStokesDefaults(Control* control);

}

#endif // NavierStokesParse_h
