//******************************************************************************
/*!
  \file    src/Utilities/ex2msh/my_malloc.h
  \author  mark
  \date    Thu Jul 14 12:59:30 2011
  \brief   
 */
//******************************************************************************
#ifndef MY_MALLOC_H
#define MY_MALLOC_H

char *my_malloc(int n);
void my_free(void *ptr);
char *my_realloc(char *ptr, int n);

#endif  /* MY_MALLOC_H */
