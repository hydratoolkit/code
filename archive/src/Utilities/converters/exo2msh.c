/******************************************************************************/
/*!
  \file    src/Utilities/exo2msh/exo2msh.c
  \author  mark
  \date    Thu Jul 14 12:59:30 2011
  \brief   Simple Exodus to ASCII msh converter
 */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netcdf.h>
#include <exodusII.h>

#include "exo2msh.h"

struct MeshParam mesh;

#define TWOD   2
#define THREED 3

#define NNPE_TRI3        3
#define NUM_TRI3_FACES   3
#define NNPE_TRI3_FACES  2

#define NNPE_QUAD4       4
#define NUM_QUAD4_FACES  4
#define NNPE_QUAD4_FACES 2

#define NNPE_TET4        4
#define NUM_TET4_EDGES   6
#define NNPE_TET4_EDGES  2
#define NUM_TET4_FACES   4
#define NNPE_TET4_FACES  3

#define NNPE_HEX8        8
#define NUM_HEX8_FACES   6
#define NNPE_HEX8_FACES  4

#define NEL_TYPE         3


/* Setup simple face-element and edge-element mapping */
int
tri3_faces[NUM_TRI3_FACES][NNPE_TRI3_FACES] =
  { {0, 1},
    {1, 2},
    {2, 1} };

int
quad4_faces[NUM_QUAD4_FACES][NNPE_QUAD4_FACES] =
  { {0, 1},
    {1, 2},
    {2, 3},
    {3, 0} };

int
tet4_faces[NUM_TET4_FACES][NNPE_TET4_FACES] =
  { {0,1,3},
    {1,2,3},
    {0,3,2},
    {0,2,1} };

int
hex8_faces[NUM_HEX8_FACES][NNPE_HEX8_FACES] =
  { {1, 2, 6, 5},
    {2, 3, 7, 6},
    {0, 4, 7, 3},
    {1, 5, 4, 0},
    {4, 5, 6, 7},
    {0, 3, 2, 1} };

int el_nnpe[NEL_TYPE] = {3, 4, 8};

int
read_coord( FILE* fpm, int nnp, int ndim, int exoid );

int
read_conn(FILE *fpm, int ndim, int nel_blk, int exoid, int* blk_ids);

int
read_node_set( FILE* fpss, int num_nd_sets, int exoid );

int read_side_set( FILE* fpss, int num_sd_sets, int ndim, int exoid );

int
count_ebcs( int num_nd_sets, int ndim, int exoid );

void
write_cntl( char *title , char *cntl_name, int exoid );

int
side2node_set( FILE* fpss, int Nnp, int num_sd_sets, int ndim,
               int exoid );

int
main( int argc, char *argv[] )
/***********************************************************************

Program: exo2msh - convert an exodus file to a '.msh' file

Author : Mark A. Christon

Date   : 12/02/95

Date     |   Modification
=========|=============================================================
09/10/96 | Added extraction of side-sets in element-side format
07/23/97 | Added in code to write updated .msh & .cntl format files
08/28/00 | tweaked code to convert all sidesets to nodesets
07/05/03 | setup to handle heterogeneous meshes

***********************************************************************/
{
  int   exoid, cpu_word_size, io_word_size, ex_err;
  int   nel_per_blk, num_attr, blk_nnpe;
  int   i;
  int*  blk_ids;
  float version;
  char  title[MAX_LINE_LENGTH+1];
  char  mesh_name[MAX_LINE_LENGTH+1];
  char  elem_type[MAX_STR_LENGTH+1];
  FILE  *fpm, *fopen();

  /* Trap on command line arguments */
  if( argc < 3 ) {
    printf( "\n\tUsage: exo2msh exodus mesh\n\n" );
    exit( 1 );
  }
  strcpy( mesh_name, argv[2] );

  /* Get the dataset parameters */
  cpu_word_size = 0;
  io_word_size  = 0;
  exoid = ex_open( argv[1], EX_READ, &cpu_word_size, &io_word_size, &version );
  if( exoid < 0 ) {
    printf("exoid = %d\n", exoid );
    exit( 1 );
  }

  fprintf(stdout,"CPU Word size = %d\n", cpu_word_size);
  fprintf(stdout,"I/O Word size = %d\n", io_word_size);

  /* Read in the primary mesh parameters */
  mesh.Ndim      = 0;
  mesh.Nnp       = 0;
  mesh.Nel       = 0;
  mesh.Nel_tri3  = 0;
  mesh.Nel_quad4 = 0;
  mesh.Nel_hex8  = 0;
  mesh.Nel_blk   = 0;
  mesh.Nnd_sets  = 0;
  mesh.Nsd_sets  = 0;
  ex_err = ex_get_init( exoid, title, &mesh.Ndim, &mesh.Nnp,
                        &mesh.Nel, &mesh.Nel_blk,
                        &mesh.Nnd_sets, &mesh.Nsd_sets );

  /* Get the number of elements on an element type basis */
  blk_ids = (int *) calloc( mesh.Nel_blk, sizeof( int ) );
  ex_err  = ex_get_elem_blk_ids( exoid, blk_ids );

  for( i = 0; i < mesh.Nel_blk; i ++ ) {
     ex_err = ex_get_elem_block(exoid, blk_ids[i], elem_type,
                                &nel_per_blk, &blk_nnpe, &num_attr );

     fprintf(stdout,"Element type = %s\n", elem_type);
     if( ex_err < 0 ) {
       fprintf(stdout,"\n\t!!! Error reading element block parameters !!!\n");
       exit(0);
     }

     switch(blk_nnpe) {
        case NNPE_TRI3:
          mesh.Nel_tri3  += nel_per_blk;
          break;
        case NNPE_QUAD4:
          mesh.Nel_quad4 += nel_per_blk;
          break;
        case NNPE_HEX8:
          mesh.Nel_hex8  += nel_per_blk;
          break;
     }
  }

  /* Bogus way to set up Ndim for now */
  mesh.Ndim = 2;
  if (mesh.Nel_hex8 > 0) mesh.Ndim = 3;

  /* Write a mini-header in the .msh file*/
  fpm = fopen( mesh_name, "w" );
  fprintf( fpm, "%s", title );
  fprintf( fpm, "#\n");
  fprintf( fpm, "Nnp       %d\n", mesh.Nnp      );
  fprintf( fpm, "Nel       %d\n", mesh.Nel      );
  fprintf( fpm, "Nel_tri3  %d\n", mesh.Nel_tri3 );
  fprintf( fpm, "Nel_quad4 %d\n", mesh.Nel_quad4);
  fprintf( fpm, "Nel_hex8  %d\n", mesh.Nel_hex8 );
  fprintf( fpm, "Ndim      %d\n", mesh.Ndim     );
  fprintf( fpm, "Neblock   %d\n", mesh.Nel_blk  );
  fprintf( fpm, "Nnd_sets  %d\n", mesh.Nnd_sets );
  fprintf( fpm, "Nsd_sets  %d\n", mesh.Nsd_sets );
  fprintf( fpm, "end\n" );
  fprintf( fpm, "#\n" );


  /* Read & write the element connectivity */
  ex_err = read_conn(fpm, mesh.Ndim, mesh.Nel_blk, exoid, blk_ids);
  if( ex_err < 0 ) {
    printf("Reading connectivity: ex_err = %d\n", ex_err );
    exit( 1 );
  }

  /* Read & write the nodal coordinates */
  ex_err = read_coord( fpm, mesh.Nnp, mesh.Ndim, exoid );
  if( ex_err < 0 ) {
    printf("Reading coordinates : ex_err = %d\n", ex_err );
    exit( 1 );
  }

  /* Read & write the node-set data */
  read_node_set( fpm, mesh.Nnd_sets, exoid );

  /* Read & write the side-set data */
  read_side_set( fpm, mesh.Nsd_sets, mesh.Ndim, exoid );

  fclose( fpm );

  fprintf(stdout,"\n\texo2msh summary\n");
  fprintf(stdout,"\tInput exodus file       : %s\n", argv[1]        );
  fprintf(stdout,"\tOutput ASCII file       : %s\n", argv[2]        );
  fprintf(stdout,"\tDimension of Mesh       : %d\n", mesh.Ndim      );
  fprintf(stdout,"\tNo. of Nodes            : %d\n", mesh.Nnp       );
  fprintf(stdout,"\tNo. of Elements         : %d\n", mesh.Nel       );
  fprintf(stdout,"\tNo. of TRI3  Elements   : %d\n", mesh.Nel_tri3  );
  fprintf(stdout,"\tNo. of QUAD4 Elements   : %d\n", mesh.Nel_quad4 );
  fprintf(stdout,"\tNo. of HEX8  Elements   : %d\n", mesh.Nel_hex8  );
  fprintf(stdout,"\tNo. of materials        : %d\n", mesh.Nel_blk   );
  for (i=0; i<mesh.Nel_blk; i++) {
    fprintf(stdout,"\t  Block = %d, ID = %d\n", i, blk_ids[i]);
  }
  fprintf(stdout,"\tNo. of Node Sets        : %d\n", mesh.Nnd_sets );
  fprintf(stdout,"\tNo. of Side Sets        : %d\n", mesh.Nsd_sets );

  free(blk_ids);
}

int
read_coord( FILE* fpm, int nnp, int ndim, int exoid )
/***********************************************************************

Routine: read_coord - read nodal coordinates from an Exodus-II file

Argument   |    Meaning
===========|===========================================================
fpm        | mesh file pointer
nnp        | # of nodes
ndim       | # of dimensions
exoid      | Exodus file pointer


Author : Mark A. Christon

Date   : 12/02/95

Date     |   Modification
=========|=============================================================
xx/xx/xx |

***********************************************************************/
{
  int   i, ex_err;
  float *x, *y, *z;

  /* Allocate for coordinates */
  x = (float *) calloc( nnp, sizeof( float ) ) ;
  y = (float *) calloc( nnp, sizeof( float ) ) ;
/*   if( ndim == 3 ) z = (float *) calloc( nnp, sizeof( float ) ) ; */
  z = (float *) calloc( nnp, sizeof( float ) ) ;

  /* Read in coordinates */
  ex_err = ex_get_coord( exoid, x, y, z );
  if( ex_err < 0 ) return( ex_err );

  /* Output coordinates */
  fprintf( fpm, "# ===== Nodal Coordinates =====\n" );
  fprintf( fpm, "#\n" );
  if( ndim == 2 ) {
    for( i = 0; i < nnp; i++ )
         fprintf( fpm, "%8d     %20.13e%20.13e\n", i+1, x[i], y[i] );
      free( x );
      free( y );
  }
  else {
    for( i = 0; i < nnp; i++ )
      fprintf(fpm, "%8d     %20.13e%20.13e%20.13e\n", i+1, x[i],y[i], z[i]);
    free( x );
    free( y );
    free( z );
  }
  return( 0 );
}

int
read_conn( FILE *fpm, int ndim, int nel_blk, int exoid, int* blk_ids )
/*******************************************************************************

*Routine: read_conn - read element connectivity from an Exodus file

Argument   |    Meaning
===========|====================================================================
fpm        | mesh file pointer
ndim       | # of dimensions
nel_blk    | # of element blocks
exoid      | Exodus file pointer


Author : Mark A. Christon

Date   : 12/02/95

Date     |   Modification
=========|======================================================================
07/25/97 | Cleaned up for new .msh format
07/05/03 | Cleaned up for revised .msh format -- multi-element version

*******************************************************************************/
{
   int    i, j, k, m, el_num, ex_err, glob_num, nnpe;
   int   *nel_per_blk, *blk_nnpe, *num_attr;
   int   *el_conn, *el_loc;
   char  elem_type[MAX_STR_LENGTH+1];

   /* Allocate for & Read the number of elements per block, etc. */
   nel_per_blk = (int *) calloc( nel_blk, sizeof( int ) );
   blk_nnpe    = (int *) calloc( nel_blk, sizeof( int ) );
   num_attr    = (int *) calloc( nel_blk, sizeof( int ) );
   ex_err      = ex_get_elem_blk_ids( exoid, blk_ids );

   for( i = 0; i < nel_blk; i ++ ) {
     ex_err = ex_get_elem_block(exoid, blk_ids[i], elem_type,
                                &(nel_per_blk[i]), &(blk_nnpe[i]),
                                &(num_attr[i]) );
     if( ex_err < 0 ) return( ex_err );
   }

   /* Read/write the element connectivity */
   fprintf( fpm, "#\n" );
   fprintf( fpm, "# ===== Element Connectivity =====\n" );
   fprintf( fpm, "#\n" );

   el_num = 0;
   for (m=0; m<NEL_TYPE; m++) {
     nnpe = el_nnpe[m];
     fprintf(fpm, "#\n" );
     fprintf(fpm, "# ===== %d-Node Elements =====\n", nnpe);
     fprintf(fpm, "#\n" );
     glob_num = 0;
     for (i=0; i<nel_blk; i++) {
       if (blk_nnpe[i] == nnpe) {
         el_conn = (int *)calloc((blk_nnpe[i]*nel_per_blk[i]), sizeof( int ));
         ex_err  = ex_get_elem_conn( exoid, blk_ids[i], el_conn );
         if (ex_err < 0) return( ex_err );
         /* Write out the element-block connectivity */
         el_loc = el_conn;
         for( j = 0; j < nel_per_blk[i]; j ++ ) {
           fprintf(fpm,"%8d%5d", glob_num+j+1, blk_ids[i]);
           for( k = 0; k < blk_nnpe[i]; k++ ) {
             fprintf(fpm,"%8d",*el_loc );
             if( *el_loc == 0 ) {
               fprintf(stderr,"Element # %d\n", el_num);
               fprintf(stderr,"Local Node %d\n", k+1);
               fprintf(stderr,"Node # %d\n", *el_loc);
             }
             el_loc++;
           }
           fprintf(fpm, "\n");
           el_num++ ;
         }
         free( el_conn );
       }
       glob_num += nel_per_blk[i];
     }
   }

   /*
   fprintf( stdout, "No. elements read/written: %d\n", el_num );
   */

   free( nel_per_blk );
   free( blk_nnpe    );
   free( num_attr    );

   return( 0 );
}

int
count_ebcs( int num_nd_sets, int ndim, int exoid )
/***********************************************************************

 *Routine: count_ebcs - count up nodal essential bc's from node-sets

 Argument   |    Meaning
 ===========|===========================================================
 num_nd_sets| # of nodal sets in database
 ndim       | # of dimensions
 exoid      | Exodus-II file pointer

 Author : Mark A. Christon

 Date   : 12/02/95

 Date     |   Modification
 =========|=============================================================
 03/14/96 | Patched out output node set

***********************************************************************/
{
   int   i, j, nebc, nnp_in_set, num_df_in_set, ex_err;
   int   *nd_ids, *nd_set;
   FILE  *fpu, *fpv, *fpw, *fopen();

   /* Open files for ebc's  */
   fpu = fopen( "ubcs", "w" );
   fpv = fopen( "vbcs", "w" );
   fprintf( fpu, "# U-BC's\n" );
   fprintf( fpu, "#\n" );
   fprintf( fpv, "# V-BC's\n" );
   fprintf( fpv, "#\n" );
   if( ndim == 3 )
   {  fpw = fopen( "wbcs", "w" );
      fprintf( fpw, "# W-BC's\n" );
      fprintf( fpw, "#\n" );
   }

   nd_ids = (int *) calloc( num_nd_sets, sizeof( int ) );
   ex_err = ex_get_node_set_ids( exoid, nd_ids );

   /* Simple count of EBC's for now */
   nebc = 0;
   for( i = 0; i < num_nd_sets; i++ )
   {  ex_err = ex_get_node_set_param( exoid, nd_ids[i], &nnp_in_set,
                     &num_df_in_set );
      nd_set = (int *) calloc( nnp_in_set, sizeof( int ) );
      ex_err = ex_get_node_set( exoid, nd_ids[i], nd_set );

      /* Write out node set */
      fprintf( fpu,
        "# Set: %d \tNnp in Set: %d\n", nd_ids[i], nnp_in_set );
      fprintf( fpu, "#\n" );
      fprintf( fpv,
        "# Set: %d \tNnp in Set: %d\n", nd_ids[i], nnp_in_set );
      fprintf( fpv, "#\n" );
      if( ndim == 3 )
      {  fprintf( fpw,
           "# Set: %d \tNnp in Set: %d\n", nd_ids[i], nnp_in_set );
         fprintf( fpw, "#\n" );
      }
      for( j = 0; j < nnp_in_set; j++ )
      {  fprintf( fpu, "%8d 0.00000e+00\n", *nd_set );
         fprintf( fpv, "%8d 0.00000e+00\n", *nd_set );
         if( ndim == 3 )
            fprintf( fpw, "%8d 0.00000e+00\n", *nd_set );
         nd_set++ ;
      }

      /* Accumulate ebc count */
      nebc += nnp_in_set;
   }

   free( nd_ids );

   fclose( fpu );
   fclose( fpv );
   if( ndim == 3 ) fclose( fpw );

   return( nebc );
}

void
write_cntl( char *title , char *cntl_name, int exoid )
/***********************************************************************

 *Routine: write_cntl - write a gila control file

 Argument   |    Meaning
 ===========|===========================================================
 title      | 1-line analysis title
 cntl_name  | control file name
 exoid      | exodus file id

 Author : Mark A. Christon

 Date   : 12/02/95

 Date     |   Modification
 =========|=============================================================
 07/25/97 | Cleaned up for node-set/side-set format

***********************************************************************/
{
   int *ids;
   int i, error;
   FILE *fp, *fopen();

   fp = fopen( cntl_name, "w" );

   fprintf( fp, "title\n" );
   fprintf( fp, "%s\n", title );
   fprintf( fp, "\n" );

   /* Analysis options */
   fprintf( fp, "{ Analysis parameters: }\n" );
   fprintf( fp, "analyze\n" );
   fprintf( fp, "   solve  3\n" );
   fprintf( fp, "   refine 1\n" );
   fprintf( fp, "   nstep  10\n" );
   fprintf( fp, "   plti    2\n" );
   fprintf( fp, "   prti   10\n" );
   fprintf( fp, "   pltype  1\n" );
   fprintf( fp, "   icset    \n" );
   fprintf( fp, "      set_u 1.0\n" );
   fprintf( fp, "      set_v 0.0\n" );
   if( mesh.Ndim == 3 )
      fprintf( fp, "      set_w 0.0\n" );
   fprintf( fp, "               ;\n" );
   fprintf( fp, "   prtlev  0\n" );
   fprintf( fp, "   ttyi    1\n" );
   fprintf( fp, "   btd     1\n" );
   fprintf( fp, "   fct     1\n" );
   fprintf( fp, "   divu    1.0e-8\n" );
   fprintf( fp, "   deltat  0.250\n" );
   fprintf( fp, "   dtchk  -1\n" );
   fprintf( fp, "   dtscal  1\n" );
   fprintf( fp, "   thetak  0.5\n" );
   fprintf( fp, "   thetab  0.5\n" );
   fprintf( fp, "   term    10.00\n" );
   fprintf( fp, "end\n" );
   fprintf( fp, "\n" );

   fprintf( fp, "material   1  \n" );
   fprintf( fp, "   rho     1.0\n" );
   fprintf( fp, "   nu      1.0\n" );
   fprintf( fp, "end\n" );
   fprintf( fp, "\n" );

   /* Nodal BC template */

   /* Read in the list of node set numbers */
   ids = (int *) calloc( mesh.Nnd_sets, sizeof( int ) );
   error = ex_get_node_set_ids( exoid, ids );

   /* Print the header information */
   fprintf( fp, "{ Node set BC Template: %d node sets available }\n\n",
            mesh.Nnd_sets );
   fprintf( fp, "{ Velocity BC's: }\n" );
   fprintf( fp, "ubc\n" );
   for( i = 0; i < mesh.Nnd_sets; i++ ) {
     fprintf( fp, "  nodeset %5d 0.0 0\n", ids[i] );
   }
   fprintf( fp, "end\n" );
   fprintf( fp, "\n" );

   free( ids );

   /* Nodal BC template */


   /* Surface BC template */

   /* Read in the list of side set numbers */
   ids = (int *) calloc( mesh.Nsd_sets, sizeof( int ) );
   error = ex_get_side_set_ids( exoid, ids );

   /* Print the header information */
   fprintf( fp, "{ Side set BC Template: %d side sets available }\n\n",
            mesh.Nsd_sets );
   fprintf( fp, "{ Outflow BC's: }\n" );
   fprintf( fp, "obc\n" );
   for( i = 0; i < mesh.Nsd_sets; i++ ) {
     fprintf( fp, "  sideset %5d\n", ids[i] );
   }
   fprintf( fp, "end\n" );
   fprintf( fp, "\n" );

   free( ids );

   /* Surface BC template */


   /* PPE solver options */
   fprintf( fp, "{ PPE solver parameters: }\n" );
   fprintf( fp, "ppesol     64\n" );
   fprintf( fp, "   itmax  100\n" );
   fprintf( fp, "   itchk    2\n" );
   fprintf( fp, "   novec    5\n" );
   fprintf( fp, "   wrt      0\n" );
   fprintf( fp, "   eps    1.0e-5\n" );
   fprintf( fp, "end\n" );
   fprintf( fp, "\n" );

   /* Momentum solver options */
   fprintf( fp, "{ Momentum solver parameters: }\n" );
   fprintf( fp, "momsol      1\n" );
   fprintf( fp, "   itmax   50\n" );
   fprintf( fp, "   itchk    2\n" );
   fprintf( fp, "   wrt      0\n" );
   fprintf( fp, "   eps     1.0e-5\n" );
   fprintf( fp, "end\n" );
   fprintf( fp, "\n" );

   /* End of control file */
   fprintf( fp, "{ End-of-control file }\n" );
   fprintf( fp, "exit\n" );
   fprintf( fp, "\n" );

   /* Time-history data */
   fprintf( fp, "{ Time-history parameters: }\n" );
   fprintf( fp, "ndhist  2\n" );
   fprintf( fp, "   nstep   1\n" );
   fprintf( fp, "   st 1   en 2\n" );
   fprintf( fp, "   st 1   en 2\n" );
   fprintf( fp, "end\n" );

   fclose( fp );

}

int
read_side_set( FILE* fpss, int num_sd_sets, int ndim, int exoid )
/***********************************************************************

 *Routine: read_side_set - read side sets from an Exodus-II file

 Argument   |    Meaning
 ===========|===========================================================
 fpss       | file pointer for mesh
 fpref      | file pointer for reference surface
 num_sd_sets| # of side sets
 exoid      | Exodus file pointer


 Author : Mark A. Christon

 Date   : 09/10/96

 Date     |   Modification
 =========|=============================================================
 11/25/97 | Hacked for output of sideset list in 3-D

***********************************************************************/
{
   int error, num_sides_in_set,  num_df_in_set;
   int *ids, *elem_list, *side_list;
   int i, j;

   if( num_sd_sets == 0 ) return( 0 );

   /* Write header for side-sets  */
   fprintf( fpss , "#\n" );
   fprintf( fpss , "# %d Side-sets\n", num_sd_sets );


   /* Allocate for the number of side sets */
   ids = (int *) calloc( num_sd_sets, sizeof( int ) );


   /* Read in the list of side set numbers */
   error = ex_get_side_set_ids( exoid, ids );

   /* Print the header information */
   fprintf( fpss , "%10d\n", num_sd_sets );
   fprintf( fpss , "# Side Set  Number of Sides\n" );
/*    fprintf( stdout, "Number of Side Sets: %d\n", num_sd_sets ); */

   for( i = 0; i < num_sd_sets; i++ ) {
     error = ex_get_side_set_param( exoid, ids[i], &num_sides_in_set,
                                    &num_df_in_set );
     fprintf( fpss, "%10d%10d\n", ids[i], num_sides_in_set );
   }

   for( i = 0; i < num_sd_sets; i++ ) {
     error = ex_get_side_set_param( exoid, ids[i], &num_sides_in_set,
                                    &num_df_in_set );
     fprintf( fpss , "#\n" );
     fprintf( fpss , "# Side Set Number : %d\n", ids[i]           );
     fprintf( fpss , "# No. of Segments : %d\n", num_sides_in_set );
     fprintf( fpss , "#\n" );

     elem_list = (int *) calloc( num_sides_in_set, sizeof( int ) );
     side_list = (int *) calloc( num_sides_in_set, sizeof( int ) );

     error = ex_get_side_set( exoid, ids[i], elem_list, side_list );

     for( j = 0; j < num_sides_in_set; j++ ) {
       fprintf(fpss, "%10d%10d\n", elem_list[j], side_list[j] );
     }

     free( elem_list );
     free( side_list );
   }

   return( 0 );
}

int
read_node_set( FILE* fpss, int num_nd_sets, int exoid )
/***********************************************************************

 *Routine: read_node_set - read node sets from an Exodus-II file

 Argument   |    Meaning
 ===========|===========================================================
 fpss       | file pointer for mesh
 num_nd_sets| # of side sets
 exoid      | Exodus file pointer


 Author : Mark A. Christon

 Date   : 07/23/97

 Date     |   Modification
 =========|=============================================================
 07/23/97 | Hacked up from read_side_set() routine

***********************************************************************/
{
   int error, num_nodes_in_set,  num_df_in_set;
   int *ids, *node_list;
   int i, j;

   if( num_nd_sets == 0 ) return( 0 );

   /* Write header for node-sets  */
   fprintf( fpss, "#\n" );
   fprintf( fpss, "# %d Node-sets\n", num_nd_sets );


   /* Allocate for the number of node sets */
   ids = (int *) calloc( num_nd_sets, sizeof( int ) );


   /* Read in the list of node set numbers */
   error = ex_get_node_set_ids( exoid, ids );

   /* Print the header information */
   fprintf( fpss, "%10d\n", num_nd_sets );
   fprintf( fpss, "# Node Set  Number of Nodes\n" );

   for( i = 0; i < num_nd_sets; i++ ) {
     error = ex_get_node_set_param( exoid, ids[i], &num_nodes_in_set,
               &num_df_in_set );
     fprintf( fpss, "%10d%10d\n", ids[i], num_nodes_in_set );
   }

   for( i = 0; i < num_nd_sets; i++ ) {
     error = ex_get_node_set_param( exoid, ids[i], &num_nodes_in_set,
               &num_df_in_set );

     fprintf( fpss, "#\n" );
     fprintf( fpss, "# Node Set Number : %d\n", ids[i]           );
     fprintf( fpss, "# No. of Nodes    : %d\n", num_nodes_in_set );
     fprintf( fpss, "#\n" );

     node_list = (int *) calloc( num_nodes_in_set, sizeof( int ) );

     error = ex_get_node_set( exoid, ids[i], node_list );

     for( j = 0; j < num_nodes_in_set; j++ ) {
        fprintf(fpss, "%10d%10d\n", j+1, node_list[j] );
     }

     free( node_list );

   }

   return( 0 );
}

int
side2node_set( FILE* fpss, int Nnp, int num_sd_sets, int ndim,
               int exoid )
/***********************************************************************

 *Routine: side2node_est - convert side to node sets

 Argument   |    Meaning
 ===========|===========================================================
 fpss       | file pointer for mesh
 Nnp        | # of nodes in mesh
 num_sd_sets| # of side sets
 exoid      | Exodus file pointer

 Author : Mark A. Christon

 Date   : 05/28/1998

 Date     |   Modification
 =========|=============================================================
 11/25/97 | Hacked for output of sideset list in 3-D

***********************************************************************/
{
   int error, num_sides_in_set,  num_df_in_set;
   int *ids;
   int *side_set_cnt, *side_set_node, *num_node_set;
   int num_nodes_in_set;
   int *cnt;
   int nnpe, Nnp_wrt;
   int i, j, jj, k;

   if( num_sd_sets == 0 ) return( 0 );

   cnt = (int *) malloc( Nnp*sizeof( int ) );

   /* Write header for side-sets  */
   fprintf( fpss , "#\n" );
   fprintf( fpss , "# %d Node-sets\n", num_sd_sets );

   /* Allocate for the number of side sets */
   ids = (int *) calloc( num_sd_sets, sizeof( int ) );
   num_node_set = (int *) malloc( num_sd_sets*sizeof( int ) );

   /* Read in the list of side set numbers */
   error = ex_get_side_set_ids( exoid, ids );

   /* Print the header information */
   fprintf( fpss , "%10d\n", num_sd_sets );
   fprintf( fpss , "# Node Set  Number of Nodes\n" );

   /* Count & print out number of nodes per node set */
   for( i = 0; i < num_sd_sets; i++ ) {
     error = ex_get_side_set_param( exoid, ids[i], &num_nodes_in_set,
               &num_df_in_set );
     if( num_df_in_set > 0 ) {
        nnpe = 4;
        if( ndim == 2 ) nnpe = 2;
        side_set_cnt  =
           (int *) calloc( num_sides_in_set, sizeof( int ) );
        side_set_node =
           (int *) calloc( nnpe*num_sides_in_set, sizeof( int ) );
        error = ex_get_side_set_node_list( exoid, ids[i], side_set_cnt,
                side_set_node );
        if( error < 0 )
           fprintf( stderr,
                    "Error for ex_get_side_set_node_list : %d\n",
                    error );

        /* Mark and count the nodes that are in a side-set */
        memset( cnt, 0, sizeof( int )*Nnp );
        jj = 0;
        for( k = 0; k < nnpe; k++ ) {
           for( j = 0; j < num_sides_in_set; j++ ) {
              if( side_set_node[jj] > Nnp )
                 fprintf( stdout,
                  "Node error: local = %d, side = %d node = %d\n",
                   k, j, side_set_node[jj] );
              cnt[side_set_node[jj]-1] = 1;
              jj++;
           }
        }
        num_node_set[i] = 0;
        for( j = 0; j < Nnp; j++ ) {
           if( cnt[j] == 1 ) num_node_set[i]++;
        }
        free( side_set_cnt );
        free( side_set_node );
     }
     fprintf( fpss, "%10d%10d\n", ids[i], num_node_set[i] );
   }
   /* End of count on nodes */

   /* Write out the converted node-set data */
   for( i = 0; i < num_sd_sets; i++ ) {
     error = ex_get_side_set_param( exoid, ids[i], &num_sides_in_set,
                                    &num_df_in_set );
     fprintf( fpss , "#\n" );
     fprintf( fpss , "# Node Set Number : %d\n", ids[i]             );
     fprintf( fpss , "# No. of Nodes    : %d\n", num_node_set[i]    );
     fprintf( fpss , "#\n" );

     if( num_df_in_set > 0 ) {
        nnpe = 4;
        if( ndim == 2 ) nnpe = 2;
        side_set_cnt  =
           (int *) calloc( num_sides_in_set, sizeof( int ) );
        side_set_node =
           (int *) calloc( nnpe*num_sides_in_set, sizeof( int ) );
        error = ex_get_side_set_node_list( exoid, ids[i], side_set_cnt,
                side_set_node );
        if( error < 0 )
           fprintf( stderr,
                    "Error for ex_get_side_set_node_list : %d\n",
                     error );
        /* Mark the nodes that are in a side-set */
        memset( cnt, 0, sizeof( int )*Nnp );
        for( j = 0; j < nnpe*num_sides_in_set; j++ ) {
           cnt[side_set_node[j]-1] = 1;
        }
        k = 0;
        Nnp_wrt = 0;
        for( j = 0; j < Nnp; j++ ) {
           if( cnt[j] == 1 ) {
              k++;
              fprintf( fpss, "%10d%10d\n", k, j+1 );
              Nnp_wrt++;
           }
        }
        fprintf( stdout,
                 "Node set #%d -- Nodes found   = %d\n",
                  i+1, num_node_set[i] );
        fprintf( stdout, "Node set #%d -- Nodes written = %d\n",
                 i+1, Nnp_wrt );
        free( side_set_cnt );
        free( side_set_node );
     }
   }
   free( cnt );
   return( 0 );
}
