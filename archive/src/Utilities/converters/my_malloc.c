/******************************************************************************/
/*!
  \file    src/Utilities/ex2msh/my_malloc.c
  \author  mark
  \date    Thu Jul 14 12:59:30 2011
  \brief   
 */
/******************************************************************************/

/* Robust versions of malloc, free, realloc
   allow for 0-length requests and NULL ptrs */

#include <stdio.h>
#include <stdlib.h>

/* ------------------------------------------------------------------------ */
/* Malloc with error checking */

char *my_malloc(int n)

{
  char *ptr;

  if (n == 0) return NULL;

  ptr = (char *) malloc(n);

  if (ptr == NULL) {
    printf("ERROR: Could not malloc %d bytes\n",n);
    exit(0);
  }

  return ptr;
}

/* ------------------------------------------------------------------------ */
/* Free with error checking */

void my_free(void *ptr)

{
  if (ptr == NULL) return;

  free(ptr);
}

/* ------------------------------------------------------------------------ */
/* Realloc with error checking */

char *my_realloc(char *ptr, int n)

{
  if (n == 0) {
    if (ptr != NULL) free(ptr);
    return NULL;
  }

  if (ptr == NULL)
    ptr = (char *) malloc(n);
  else
    ptr = (char *) realloc(ptr,n);

  if (ptr == NULL) {
    printf("ERROR: Could not realloc %d bytes\n",n);
    exit(0);
  }

  return ptr;
}
