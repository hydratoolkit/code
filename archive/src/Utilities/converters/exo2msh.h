//******************************************************************************
/*!
  \file    src/Utilities/exo2msh/exo2msh.h
  \author  mark
  \date    Thu Jul 14 12:59:30 2011
  \brief   Simple converter from exodus-ii to ascii mesh format
 */
//******************************************************************************

#ifdef DBL
typedef double Real;
#else
typedef float  Real;
#endif

struct MeshParam {
  int Ndim;
  int Nnp;
  int Nel;
  int Nel_tri3;
  int Nel_quad4;
  int Nel_hex8;
  int Nel_blk;
  int Nnd_sets;
  int Nsd_sets;
};
