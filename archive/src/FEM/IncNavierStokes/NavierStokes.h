//******************************************************************************
/*!
  \file    src/FEM/IncNavierStokes/NavierStokes.h
  \author  mark
  \date    Thu Jul 14 12:34:35 2011
  \brief   
 */
//******************************************************************************

#ifndef NavierStokes_h
#define NavierStokes_h

#include <FlowPhysics.h>
#include <DOFmap.h>

/*! \file NavierStokes.h
    \brief 
    The NavierStokes class is used for solving the time-dependent 
    incompressible Navier-Stokes equations.
 */

namespace Hydra {

extern "C" {
void
FORTRAN(elmkj) (int* iecn, int* mkj, int* icnt, int* ielst, 
		int& nnp, int& nel, int& npe, int& mxe, int& ndw,  
		int& iebw, int& luo);

void
FORTRAN(rzpmkj) (int* mkj, int* icon, int& nel, int& ndw, int& na);

void
FORTRAN(gpskca) (int& N, int* DEGREE, int* RSTART, int* CONNEC, int& OPT, 
		 int& WRKLEN, int* PERMUT, int* WORK, int& BANDWD, 
		 int& PROFIL, int& ERROR, int& SPACE);

void
FORTRAN(pvsrow) (int* ierv, int* mkj, int* irowl, int* icolh, int* maxa, 
                 int& neq, int&  ndw, int& lev, int& na);

void
FORTRAN(pvsif) ( Real* A, Real* B, int* MAXA, int* IROWL, int* ICOLH, int&  na, 
		 int& NEQ, int& IFLAG);

}

struct ElSet {
  /*! \name Element set -- used for setting hydrostatic pressures
   The ElSet structure holds the list of elements where the pressure is fixed.
  */
  //@{
  int Nel_set;         //!< No. of elements in the element set
  DataIndex EL_LIST;   //!< List of elements
  DataIndex EL_VAL;    //!< List of values associated with elements
  //@}
};

/*! The NavierStokes class is used for solvint the time-dependent 
    incompressible Navier-Stokes equations.
*/
class NavierStokes : public FlowPhysics {

  public:
    /*! \name Constructor/Destructor
     */
    //@{
             NavierStokes();
    virtual ~NavierStokes() {}
    //@}

    /*! \name Virtual Data Registration
        Each physics that is implemented will require its own
        storage and specific variables.  The data registration
        function is implemented for each physics class to enable
        custom definition of variables for each algorithm that's
        implemented.
     */
    //@{
    virtual void registerData();
    //@}

    /*! \name Virtual Physics Functions
        These functions are pure virtual functions that need to 
        be implemented for each specific type of physics being solved
        with the framework.
     */
    //@{
    virtual void setup();

    virtual void initialize();

    virtual void solve();

    virtual void finalize() {}
    //@}

    //! \name Pressure-Poisson Equation (PPE)
    //@{
    void setupPPE();      //!< register variables, compute bandwidth, etc.
   
    void genElCon();      //!< calculate the element-to-element connectivity

    void formPPE();          //!< Form the PPE operator

    void formPPE(Quad4& ec); //!< Form the PPE operator

    void solvePPE();      //!< driver to solve the PPE problem

    void reorderP();      //!< reorder the pressure for minimum bandwidth

    void reorderRHS();    //!< reorder the PPE rhs for minimum bandwidth

    void nodalPressure(); //!< compute nodal pressures for graphics output
    //@}

    //! \name Basic operators and forces
    //@{
    void formMpK();    //!< Form the M+K and M-K operators

    void formMpK(Real alpha, Real wtmpk, Real wtmmk, Real dto2, Quad4& ec);

    void formCMK();    //!< Form the C, M and K operators for startup

    void formCMK(Real alpha, Quad4& ec);

    void calcForces(); //!< Compute the force terms for the momentum equations
    //@}

    //! \name Mass matrix & BC operations for the Projection algorithm
    //@{
    //! setup inverse mass + EBC's
    void setupInverseMass(); 

    //! set zero's in inverse mass according to EBC's
    void applyInverseMassBCs(DOFmap dof, DataIndex index);
    //@}

    //! \name Projection machinery & initial pressure calculation
    //@{
    //! perform the startup projection
    void divFree();

    //! compute the div for a given velocity field
    void divU(DataIndex U, DataIndex V);

    //! calculate the current RMS divergence
    Real rmsDiv();

    //! L2 projection onto div-free subspace
    void projectVelocity(Real dt);

    //! initialize the pressure field at t=0
    void initPressure();
    //@}

    //! \name Global utility functions
    //@{
    //! Compute the global kinetic energy
    Real calcKE(DataIndex U, DataIndex V, DataIndex ML);
    //@}

    /*! \name Virtual Functions for I/O
        These functions are implemented for each specific physics
        to permit custom options to be coded for reading and writing
        restart files, time-history data, bc's, etc.
     */
    //@{
    virtual void writeSolving();

    virtual void echoOptions(ostream& ofs);

    void echoBCs(ostream& ofs);
    //@}

    //! \name I/O functions specific to this flow solver
    //@{
    //! Write out any element sets used to peg pressure hydrostats
    void echoElSet(ostream& ofs);

    //! Write out the PPE statistics: bandwidth, etc.
    void echoPPEStats(ostream& ofs);

    // Echo time integrator options
    void echoTimeInt(ostream& ofs);

    //! Write the header for the global time-history data file
    void globHeader(ofstream& globf);

    //! Write the global time history data at each step
    void writeGlobal(ofstream& globf, Real time, Real divu, Real ke);
    //@}

    //! Element list for setting the hydrostatic pressure
    ElSet*  getElSet() {return &eset;}

    //! Set methods for parameters here -- likely to move
    void setDivEps(Real eps) {diveps = eps;}

    //! Time weight
    void setTheta(Real parm) {thetak = parm;}

    //! Mass type
    void setMass(int mass) {masstype = mass;}

  protected:
    //! \brief Data registered for Navier-Stokes

    //! \name Data required for addition of advection-diffusion -- not used
    //@{
    DataIndex TEMPERATURE;
    DataIndex Q;
    //@}

    //! \name Data required for linear algegra
    //@{
    DataIndex MPK; //!< DataIndex for the M+K operator
    DataIndex MMK; //!< DataIndex for the M-K operator
    DataIndex MKJ; //!< DataIndex for the compressed row storage column pointers
    //@}

    /*! \name Temporary storage for required for the C-G algorithm 
        These DataIndices are used for the temporary storage required
        for the C-G solution algorithm for nodal variables.
        Note that TMP is simply a 'temporary' variable.
     */
    //@{
    DataIndex PP;  //!< Search direction p
    DataIndex AP;  //!< For the Ap product
    DataIndex W;   //!< The W scaling matrix -- really the diagonal of A
    DataIndex R;   //!< The residucal vector r
    DataIndex Z;   //!< The pseudo-residucal in PCCG
    DataIndex TMP; //!< Just a temporary vector
    //@}


    // ===== Required for P2 Solver =====
    /*! \name Nodal data for P2 solver stored in the datamesh.
              Allocate vectors as [Nnp]
    */
    //@{
    DataIndex VELXN;        //!< X-velocity at n+1
    DataIndex VELYN;        //!< Y-velocity at n+1
    DataIndex FX;           //!< X-force at n
    DataIndex FY;           //!< Y-force at n
    DataIndex FXN;          //!< X-force at n+1  (not used)
    DataIndex FYN;          //!< X-force at n+1  (not used)
    DataIndex RIMX;         //!< X-inverse lumped mass w. EBC's imposed
    DataIndex RIMY;         //!< Y-inverse lumped mass w. EBC's imposed
    DataIndex CONST_MASS;   //!< Consistent mass matrix
    DataIndex LUMPED_MASS;  //!< Lumped mass matrix
    DataIndex DM;           //!< Temporary to hold diagonal of M+K
    DataIndex V1;           //!< Scratch vector
    DataIndex V2;           //!< Scratch vector
    //@}

    /*! \name Element operators for P2 solver stored in the datamesh.
              Allocate as [Nel]
     */
    //@{
    DataIndex IEN;          //!< New element number in terms of old (not used)
    DataIndex IEO;          //!< Old element number in terms of new
    DataIndex P;            //!< Pressure at n
    DataIndex PN;           //!< Pressure at n+1
    DataIndex RHS;          //!< PPE right-hand-side
    DataIndex CX;           //!< X-gradient operator
    DataIndex CY;           //!< Y-gradient operator
    //@}

    /*! \name PPE storage for the PVS solver & bandwidth minimization.
     */
    //@{
    DataIndex ROWL;         //!< Row lengths
    DataIndex COLH;         //!< Column lengths
    DataIndex MAXA;         //!< Maximum row in the matrix
    DataIndex PPE;          //!< Coefficient matrix for the PPE
    DataIndex IDEG;         //!< Degree array for bandwidth minimization
    DataIndex IRST;         //!< Working storage for bandwidth minimization
    DataIndex IERV;         //!< Reorder vector for bandwidth minimzation
    DataIndex IECN;         //!< Element-to-element connectivity
    DataIndex MKJE;         //!< Column pointers for PPE operator
    //@}

    // ===== Required for P2 Solver =====


  private:
    //! Don't permit copy or assignment operators
    //@{
    NavierStokes(const NavierStokes&);
    NavierStokes& operator=(const NavierStokes&);
    //@}

    int Npe;        //!< No. of pressure elements
    int Elbwi;      //!< initial 1/2-bandwidth
    int Elbwf;      //!< final   1/2-bandwidth
    int Ndwe;       //!< No. of non-zero row entries for PPE
    int Npre;       //!< profile
    int ppe_na;     //!< size of ppe for PVS

    int luflag;     //!< LU-flag for PVS

    //! \name Element operators
    //@{
    Real Me[4][4];  //!< Element consistent mass matrix
    Real Mle[4];    //!< Element lumped mass matrix
    Real Ke[4][4];  //!< Element conductivity matrix
    Real C[2][4];   //!< Element gradient (Cx,Cy) operators
    //@}

    //! Element set used for hydrostatic pressure
    ElSet eset;

    //! Divergence tolerance
    Real diveps;

    //! Time weight
    Real thetak;

    //! Mass type
    int masstype;
};

}
#endif // NavierStokes_h
