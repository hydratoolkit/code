<?php
// This page provides the data for the dashboard
	// Pull in the MySQL info
	require_once("db.conf");
	require_once("class.MySqlDb.php");

	// Setup the columns that are in the table/database (server and client side names must agree)
	$aColumns = 
	$regressionColumns = array('id', 'name', 'date');
	$systemColumns = array('regression_id', 'machineName', 'compiler', 'operatingSystem', 'OSRelease', 'processor'); 
	$summaryColumns = array('regression_id', 'username', 'numTests', 'numPassed', 'numHydraFails', 'numHydraCrashes', 'numExodusCompFails', 'numExodiffCrashes', 'percentFailed'); 
	$testColumns = array('regression_id', 'name', 'path', 'input', 'output', 'control', 'baseline', 'diff', 'Nproc', 'author', 'status', 'elementCycleTime', 'solutionTime');

	if ($_GET['table']=='summary')
		$aColumns = array( 'numTests', 'numPassed', 'numHydraFails', 'numHydraCrashes', 'numExodusCompFails', 'numExodiffCrashes', 'percentFailed' );
	else if ($_GET['table']=='regression')
		$aColumns = array('id', 'username', 'machineName', 'operatingSystem', 'name', 'date', 'numTests', 'numPassed');
	else if ($_GET['table']=='test')
		$aColumns = array('name', 'path', 'input', 'output', 'control', 'baseline', 'diff', 'Nproc', 'author', 'status', 'elementCycleTime', 'solutionTime');

		
	/* 
	 * MySQL connection
	 */
	$db = new MySqlDb($db_host, $db_username, $db_password, $db_name);
	$db->connect();
	
	/* 
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".
			mysql_real_escape_string( $_GET['iDisplayLength'] );
	}
	
	
	/*
	 * Ordering
	 */
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
		{
			if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
			{
				$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
				 	".mysql_real_escape_string( $_GET['sSortDir_'.$i] ) .", ";
			}
		}
		
		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}
	}
	
	
	/* 
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
/*	$sWhere = "";
	if ( $_GET['sSearch'] != "" )
	{
		$sWhere = "WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
	}
*/	
	/* Individual column filtering */
/*	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
		{
			if ( $sWhere == "" )
			{
				$sWhere = "WHERE ";
			}
			else
			{
				$sWhere .= " AND ";
			}
			$sWhere .= $aColumns[$i]." LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
		}
	}
*/	
	
	/*
	 * SQL queries
	 * Get data to display
	 */
        if ($_GET['table']=='regression')
	{
//		$sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $regressionColumns)) .
//			" FROM regression ";
		$sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns)) .
			" FROM regression JOIN" .
			" summary JOIN system WHERE regression.id=summary.regression_id AND regression.id=system.regression_id" .
			" $sWhere" .
			" $sOrder" .
			" $sLimit";
	}
        else if ($_GET['table']=='test')
	{
		$regression_id = $_GET['regression_id'];
		$sQuery = "SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $testColumns)) .
                        " FROM test" .
                        " WHERE regression_id=$regression_id" .
                        " $sWhere" .
                        " $sOrder" .
                        " $sLimit";
	}
	else if ($_GET['table']=='summary')
	{
		$regression_id = $_GET['regression_id'];
                $sQuery = "SELECT *" .
                        " FROM summary" .
                        " WHERE regression_id=$regression_id";
	}

	$rResult = $db->getResults($sQuery);
	
	// If we are doing the summary, we will wrap it up here
	if ($_GET['table']=='summary')
	{
		$aRow = mysql_fetch_array( $rResult );
		   echo "       <dt>Number of Tests</dt><dd>" . $aRow['numTests'] . "</dd>" .
			"	<dt>Number Passed</dt><dd>" . $aRow['numPassed'] . "</dd>" .
        		"	<dt>Hydra Failures</dt><dd>" . $aRow['numHydraFails'] . "</dd>" .
        		"	<dt>Hydra Crashes</dt><dd>" . $aRow['numHydraCrashes'] . "</dd>" .
        		"	<dt>Exodus Comparison Failures</dt><dd>" . $aRow['numExodusCompFails'] . "</dd>" .
        		"	<dt>Exodiff Crashes</dt><dd>" . $aRow['numExodiffCrashes'] . "</dd>" .
        		"	<dt>Percent Failed</dt><dd class='percentage'>" . number_format($aRow['percentFailed'], 1) . "</dd>";
		return;
	}

	/* Data set length after filtering */
	$sQuery = "
		SELECT FOUND_ROWS()
	";
	$rResultFilterTotal = $db->getResults($sQuery);
	$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];
	
	/* Total data set length */
	if ($_GET['table']=='regression')
		$sQuery = "SELECT COUNT(id) FROM regression";
	else if ($_GET['table']=='test')
		$sQuery = "SELECT COUNT(name) FROM test WHERE regression_id=$regression_id";

	$rResultTotal = $db->getResults($sQuery);
	$aResultTotal = mysql_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];
	
	
	/*
	 * Output
	 */
	$output = array(
		"sEcho" => intval($_GET['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);
	
	while ( $aRow = mysql_fetch_array( $rResult ) )
	{
		$row = array();
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( $aColumns[$i] == "version" )
			{
				/* Special output formatting for 'version' column */
				$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
			}
			else if ( $aColumns[$i] != ' ' )
			{
				/* General output */
				$row[] = $aRow[ $aColumns[$i] ];
			}
		}
		$output['aaData'][] = $row;
	}
	$db->close();	
	echo json_encode( $output );
?>

