<?php
if ( !isset($_FILES['regressionXmlFile']['name']) )
{
?>
<form enctype="multipart/form-data" action="updateDashboard.php" method="POST">
<input type="hidden" name="MAX_FILE_SIZE" value="100000" />
Choose a file to upload: <input name="regressionXmlFile" type="file" /><br />
<input type="submit" value="Upload File" />
</form>
<br /><a href='http://hydra.lanl.gov/hydra/dashboard.php'>Head back to the dashboard</a><br />
<?php
}
else
{
require("class.Regression.php");
require("class.MySqlDb.php");
require("db.conf");

// Grab the uploaded file and open it
$tmp_file = $_FILES['regressionXmlFile']['tmp_name'];

// Declare a SimpleXMLElement iterator and a regression storage class
$xmlElement = new SimpleXMLElement($tmp_file, null, true);
$regression = new Regression();

// Open a connection to the db - we need this to make the strings safe
$db = new MySqlDb($db_host, $db_username, $db_password, $db_name);
$db->connect();

// First, grab the attributes of the regression
foreach($xmlElement->attributes() as $attr)
{
	if ( strcmp($attr->getName(), "name") == 0 )
		$regression->name = mysql_real_escape_string("$attr");
	else if ( strcmp($attr->getName(), "date") == 0 )
		$regression->date = mysql_real_escape_string("$attr");
}

// We will use this for iterating through the regression test array
$testNum = 0;

// Now we grab the child elements which are the summary and the tests
foreach($xmlElement->children() as $group)
{
	if ( strcmp($group->getName(), "summary") == 0 )
	{
		foreach($group->children() as $summaryElement)
		{
                        if ( strcmp($summaryElement->getName(), "username") == 0 )
                        {
                                $regression->summary->username = mysql_real_escape_string("$summaryElement");
                        }
			else if ( strcmp($summaryElement->getName(), "numTests") == 0 )
			{
				$regression->summary->numTests = mysql_real_escape_string("$summaryElement");
			}
			else if ( strcmp($summaryElement->getName(), "numPassed") == 0 )
			{
				$regression->summary->numPassed = mysql_real_escape_string("$summaryElement");
			}
                        else if ( strcmp($summaryElement->getName(), "numHydraFails") == 0 )
			{
				$regression->summary->numHydraFails = mysql_real_escape_string("$summaryElement");
			}
                        else if ( strcmp($summaryElement->getName(), "numHydraCrashes") == 0 )
			{
				$regression->summary->numHydraCrashes = mysql_real_escape_string("$summaryElement");
			}
                        else if ( strcmp($summaryElement->getName(), "numExodusCompFails") == 0 )
			{
				$regression->summary->numExodusCompFails = mysql_real_escape_string("$summaryElement");
			}
                        else if ( strcmp($summaryElement->getName(), "numExodiffCrashes") == 0 )
			{
				$regression->summary->numExodiffCrashes = mysql_real_escape_string("$summaryElement");
			}
                        else if ( strcmp($summaryElement->getName(), "percentFailed") == 0 )
			{
				$regression->summary->percentFailed = mysql_real_escape_string("$summaryElement");
			}
		}
	}
	else if ( strcmp($group->getName(), "system") == 0 )
	{
		foreach($group->children() as $systemElement)
		{
			if ( strcmp($systemElement->getName(), "machineName") == 0 )
			{
				$regression->system->machineName = mysql_real_escape_string("$systemElement");
			}
			else if ( strcmp($systemElement->getName(), "compiler") == 0 )
			{
				$regression->system->compiler = mysql_real_escape_string("$systemElement");
			}
                        else if ( strcmp($systemElement->getName(), "operatingSystem") == 0 )
			{
				$regression->system->operatingSystem = mysql_real_escape_string("$systemElement");
			}
                        else if ( strcmp($systemElement->getName(), "OSRelease") == 0 )
			{
				$regression->system->OSRelease = mysql_real_escape_string("$systemElement");
			}
                        else if ( strcmp($systemElement->getName(), "processor") == 0 )
			{
				$regression->system->processor = mysql_real_escape_string("$systemElement");
			}
		}
	}
	else if ( strcmp($group->getName(), "test") == 0 )
	{
		$regression->test[$testNum] = new Test();
		
		foreach($group->attributes() as $attr)
		{
			if ( strcmp($attr->getName(), "name") == 0 )
				$regression->test[$testNum]->name = mysql_real_escape_string("$attr");
		}
		
		foreach($group->children() as $testElement)
		{
			if ( strcmp($testElement->getName(), "path") == 0 )
			{
				$regression->test[$testNum]->path = mysql_real_escape_string("$testElement");
			}
			else if ( strcmp($testElement->getName(), "input") == 0 )
			{
				$regression->test[$testNum]->input = mysql_real_escape_string("$testElement");
			}
			else if ( strcmp($testElement->getName(), "output") == 0 )
			{
				$regression->test[$testNum]->output = mysql_real_escape_string("$testElement");
			}
			else if ( strcmp($testElement->getName(), "control") == 0 )
			{
				$regression->test[$testNum]->control = mysql_real_escape_string("$testElement");
			}
			else if ( strcmp($testElement->getName(), "baseline") == 0 )
			{
				$regression->test[$testNum]->baseline = mysql_real_escape_string("$testElement");
			}
			else if ( strcmp($testElement->getName(), "diff") == 0 )
			{
				$regression->test[$testNum]->diff = mysql_real_escape_string("$testElement");
			}
			else if ( strcmp($testElement->getName(), "Nproc") == 0 )
			{
				$regression->test[$testNum]->Nproc = mysql_real_escape_string("$testElement");
			}
			else if ( strcmp($testElement->getName(), "author") == 0 )
			{
				$regression->test[$testNum]->author = mysql_real_escape_string("$testElement");
			}
			else if ( strcmp($testElement->getName(), "status") == 0 )
			{
				$regression->test[$testNum]->status = mysql_real_escape_string("$testElement");
			}
                        else if ( strcmp($testElement->getName(), "elementCycleTime") == 0 )
                        {
                                $regression->test[$testNum]->elementCycleTime = mysql_real_escape_string("$testElement");
                        }
                        else if ( strcmp($testElement->getName(), "solutionTime") == 0 )
                        {
                                $regression->test[$testNum]->solutionTime = mysql_real_escape_string("$testElement");
                        }
		}
		$testNum++;
	}
}

// We are finished reading the file, so we should have read the number 
// of tests that the summary declared
assert('$testNum==$regression->summary->numTests');

// Stuff the object into the mysql database

// Insert Regression
$db->execute("INSERT INTO regression (name, date) VALUES ('" . $regression->name . "', '" . $regression->date . "')");

$regression_id = $db->getLastInsertId();

// Insert Summary
$query = "INSERT INTO summary (regression_id, username, numTests, numPassed, numHydraFails, numHydraCrashes, numExodusCompFails, numExodiffCrashes, percentFailed) " .
	 "VALUES ('$regression_id', '" . $regression->summary->username . "', '" . $regression->summary->numTests . "', '" . $regression->summary->numPassed . "', '" . $regression->summary->numHydraFails . "', " .
	 "'" . $regression->summary->numHydraCrashes . "', '" . $regression->summary->numExodusCompFails . "', '" . $regression->summary->numExodiffCrashes . "', " .
	 "'" . $regression->summary->percentFailed . "')";
$db->execute( $query );

// Insert System
$query = "INSERT INTO system (regression_id, machineName, compiler, operatingSystem, OSRelease, processor) " .
	 "VALUES ('" . $regression_id . "', '" . $regression->system->machineName . "', '" . $regression->system->compiler . "', " .
	 "'" . $regression->system->operatingSystem . "', '" . $regression->system->OSRelease . "', '" . $regression->system->processor . "' )";
$db->execute( $query );

// Insert Tests
for ($i=0; $i<$regression->summary->numTests; $i++)
{
	$query = "INSERT INTO test (regression_id, name, path, input, output, control, baseline, diff, Nproc, author, status, elementCycleTime, solutionTime) " .
		"VALUES ('" . $regression_id . "', '" . $regression->test[$i]->name . "', '" . $regression->test[$i]->path . "', " .
		"'" . $regression->test[$i]->input . "', '" . $regression->test[$i]->output . "', '" . $regression->test[$i]->control . "', " .
		"'" . $regression->test[$i]->baseline . "', '" . $regression->test[$i]->diff . "', '" . $regression->test[$i]->Nproc . "', " .
		"'" . $regression->test[$i]->author . "', '" . $regression->test[$i]->status . "', '" . $regression->test[$i]->elementCycleTime . "', " .
		"'" . $regression->test[$i]->solutionTime . "')";
	$db->execute( $query );
}

$db->close();

echo "The regression was added to the database successfully\n<br /><br />";
echo "<a href='http://hydra.lanl.gov/hydra/updateDashboard.php'>Add another regression to the database</a>\n<br />\n";
echo "<a href='http://hydra.lanl.gov/hydra/dashboard.php'>Head back to the dashboard</a>\n<br />\n";
echo "<a href='http://hydra.lanl.gov'>Head back to the Hydra Homepage</a>";
}
?>
