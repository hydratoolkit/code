<?php
/**
 * \file		class.MySqlDb.php
 * \brief		Provides a wrapper for accessing a MySQL database
 * \author		Nathan Barnett
 * \date		May 2011
 */

/**
 * \class		MySqlDb
 * \brief		Provides a wrapper for accessing a MySQL database
 */
class MySqlDb
{
	public		$host;			/*< The host									*/
	public		$username;		/*< The database username						*/
	public		$password;		/*< The database password						*/
	public		$dbname;		/*< The name of the database					*/	
	public		$con;			/*< Connection object (used to open and close)	*/
	public		$results;		/*< Store the results of a query				*/


	/**
	 * \fn			Constructor
	 * \brief		Constructs an object holding the necessary data to connect to a MySQL database
	 * \param[in]	server		Name of the server or ip address
	 * \param[in]	username	Username needed to connect to the db
	 * \param[in]	password	Password corresponding to the username
	 * \param[in]	dbname		Name of the database to which to connect
	 */
	public function __construct( $server, $username, $password, $dbname )
	{
		$this->host			= $server;
		$this->username		= $username;
		$this->password		= $password;
		$this->dbname		= $dbname;
	}

	/**
	 * \fn			connect
	 * \brief		Connects to the MySQL database via the member variables
	 */
	public function connect()
	{
		$this->con = mysql_connect($this->host,$this->username,$this->password);
		if (!$this->con)
			die('Could not connect to the MySQL database: ' . mysql_error());
		$this->select_db();
	}

	/**
	 * \fn			select_db
	 * \brief		Selects the MySQL database to use.
	 *				The main purpose of this function is to switch the database of an already
	 *				connected object.
	 * \param[in]	db		Name of the database to use
	 */
	public function select_db($db=NULL)
	{ 
		if ($db!=NULL)
			$this->dbname = $db;
		mysql_select_db($this->dbname, $this->con);
	}

	/**
	 * \fn			exec
	 * \brief		Executes a query
	 *				Queries the MySQL database with a command that does not
	 *				expect anything to be returned. Calls the member function 'execute'.
	 * \param[in]	query		String to send to the MySQL db
	 */
	public function exec($query)
	{
		execute($query);
	}

	/**
	 * \fn			execute
	 * \brief		Executes a query
	 *				Queries the MySQL database with a command that does not
	 *				expect anything to be returned.
	 * \param[in]	query		String to send to the MySQL db
	 */
	public function execute($query)
	{
		if (!mysql_query($query,$this->con))
			die("Error creating database: " . mysql_error());
	}

	/**
	 * \fn			getResults
	 * \brief		Queries the db, saves the results to the results member
	 *				variable and also returns them for immediate use.
	 * \param[in]	query		String to send to the MySQL db
	 */
	public function getResults($query)
	{
		$this->results = mysql_query($query,$this->con);
		if (!$this->results)
			die("Error querying database: " . mysql_error());
		return $this->results;
	}

	/**
	 * \fn			getLastInsertId
	 * \brief		Retrieves the last id that the MySQL db created
	 * \returns		Integer ID number
	 */
	public function getLastInsertId()
	{
		return mysql_insert_id($this->con);
	}

	/**
	 * \fn			numRows
	 * \brief		Gets the number of rows in the result of the current or last query.
	 *				If the input variable is not used, the it will use the last results
	 *				received and stored as a member variable. Otherwise, queries the db
	 *				and returns the number of rows.
	 * \param[in]	query		String to send to the MySQL db
	 * \returns		Integer number of results from the db
	 */
	public function numRows($query=NULL)
	{
		if ( $query!=NULL ) 
			$this->getResults($query);
		return mysql_numrows($this->results);
	}

	/**
	 * \fn			close
	 * \brief		Closes the connection to the db
	 */
	public function close()
	{
		mysql_close($this->con);
	}

	/**
	 * \fn			__sleep
	 * \brief		Serialization function for passing the db throughout a php session.
	 *				Serializes and saves all of the member variables.
	 */
	public function __sleep()
	{
		return array('host', 'username', 'password', 'dbname');
	}
}
?>
