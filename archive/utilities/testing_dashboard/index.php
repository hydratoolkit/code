<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Hydra</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <div class="logo">
        <h1><a href="index.php"><span>Hydra</span>-TH <small>Advanced Thermal Hydraulics</small></a></h1>
      </div>
      <div class="menu">
        <ul>
          <li><a href="http://hydra.lanl.gov/hydra/index.php" class="active">Home</a></li>
          <li><a href="http://hydra.lanl.gov/hydra/cgi-bin/viewvc.cgi/hydra/">Code Repository</a></li>
          <li><a href="http://hydra.lanl.gov/hydra/wiki">Wiki</a></li>
          <li><a href="http://hydra.lanl.gov/hydra/redmine/projects/hydra-th">Project Management</a></li>
          <li><a href="http://hydra.lanl.gov/hydra/dashboard.php">Dashboard</a></li>
          <li><a href="http://hydra.lanl.gov/hydra/documentation/">Documentation</a></li>
        </ul>
      </div>
      <div class="clr"></div>
    </div>
    <div class="headert_text_resize">
      <div class="headert_text">
        <h2>Hydra</h2>
        <p></p>
      </div>
      <img src="images/hydra_logo.png" alt="" width="384" height="198" />
      <div class="clr"></div>
    </div>
  </div>
  <div class="body">
    <div class="body_resize">
      <div class="left">
        <div class="resize_bg">
          <h2> Welcome</h2>
        <p>HYDRA is an extensible multiphysics code that includes finite-element
 based solvers for 2D/3D time-dependent heat conduction, 2D
 time-dependent advection- diffusion, 2D time-dependent incompressible
 flow, 2D Lagrangian hydrodynamics.  In addition, 2D/3D
 unstructured-grid finite-volume solvers are being developed for
 solving time-dependent advection-diffusion, Burgers' equation, the
 compressible Euler equationa, and incompressible Navier-Stokes
 equations.  HYDRA uses a toolkit model for development and provides
 lightweight, high-performance and reusable code modules for agile
 development. </p>  
	<p>&nbsp;</p>

          <h2>&nbsp;</h2>
        </div>
      </div>
      <div class="right">
		<?php include_once("sidebar.html.inc"); ?>
        <div class="resize_bg">
          <h2>Relevant Links</h2>
          <ul class="sponsors">
            <li class="sponsors"><a href="http://hydra.lanl.gov/hydra/gallery/">Results Gallery</a><br />
              Some images of cool Hydra-produced simulations</li>
            <li class="sponsors"><a href="http://hydra.lanl.gov/hydra/dashboard.php">Dashboard</a><br />
              Test results for Hydra Daily and Nightly grinds</li>
            <li class="sponsors"><a href="http://hydra.lanl.gov/hydra/redmine">Project Management</a><br />
              Redmine project management system</li>
            <li class="sponsors"><a href="http://hydra.lanl.gov/hydra/repo/">View the Code</a><br />
              Browse the latest code in the Git repository</li>
          </ul>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="FBG">
    <div class="FBG_resize">
      <div class="blok">
        <a href="http://hydra.lanl.gov/hydra/gallery/"><h2>Results <span>Gallery</span></h2>
        <img src="images/gallery1.jpg" alt="" width="95" height="94" /><img src="images/gallery2.jpg" alt="" width="95" height="94" /><img src="images/gallery3.png" alt="" width="95" height="94" /> <img src="images/gallery4.jpg" alt="" width="95" height="94" /></a>
        <div class="clr"></div>
      </div>
      <div class="blok">
        <h2>About <small>the</small> <span>Staff</span></h2>
        <img src="images/stickpeople.jpg" alt="" width="96" height="96" />
        <p>The Hydra-th staff consists of LANL scientists whose backgrounds lie in fluids and computer science.<!-- <a href="#">Learn more...</a> --><br />
        </p>
      </div>
      <div class="blok">
        <h2>Contact</h2>
        <p>The following people can be reached for questions or comments:<br />
          <br />
		  <b>Mark Christon (Project Lead)</b> <br />
          E-mail: <a href="mailto:christon@lanl.gov.com">christon@lanl.gov</a><br />
          Telephone : (505) 695-5649<br /><br />

		  <b>Rob Lowrie (CASL Czar)</b> <br />
          E-mail: <a href="mailto:lowrie@lanl.gov">lowrie@lanl.gov</a><br />
          Telephone : (505) 667-2121<br /><br />

		  <b>Nathan Barnett (Programming)</b> <br />
          E-mail: <a href="mailto:nbarnett@lanl.gov">nbarnett@lanl.gov</a><br />
          Telephone : (505) 606-2111<br /><br />

		<b>Jozsef Bakosi</b> <br />
          E-mail: <a href="mailto:jbakosi@lanl.gov">jbakosi@lanl.gov</a><br />
          Telephone : (505) 663-5607<br /><br />

                <b>Marianne Francois</b> <br />
          E-mail: <a href="mailto:mmfran@lanl.gov">mmfran@lanl.gov</a><br />
          Telephone : (505) 667-4237<br /></p><br />
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="footer">
    <div class="footer_resize">
      <div class="clr"></div>
    </div>
    <div class="clr"></div>
  </div>
</div>
</body>
</html>

