<?php
function make_thumbnail($src, $dest, $desired_width)
{
  // Get the extension of the filename
  $filename = stripslashes($src);
  $i = strrpos($filename,".");
  $l = strlen($filename) - $i;
  $ext = substr($filename,$i+1,$l);
  $ext = strtolower($ext);

  $source_image;

  if (( strcmp($ext,"jpg") == 0 ) || ( strcmp($ext,"jpeg") == 0 ))
  {
    $source_image = imageCreateFromJPEG($src);
  }
  else if ( strcmp($ext,"png") == 0 )
  {
    $source_image = imageCreateFromPNG($src);
  }
  else if ( strcmp($ext,"gif") == 0 )
  {
    $source_image = imageCreateFromGIF($src);
  }

  $width = imagesx($source_image);
  $height = imagesy($source_image);
 
  /* find the "desired height" of this thumbnail, relative to the desired width  */
  $desired_height = floor($height*($desired_width/$width));

  /* create a new, "virtual" image */
  $virtual_image = imagecreatetruecolor($desired_width,$desired_height);

  /* copy source image at a resized size */
  imagecopyresampled($virtual_image,$source_image,0,0,0,0,$desired_width,$desired_height,$width,$height);

  /* create the physical thumbnail image to its destination */
  if (( strcmp($ext,"jpg") == 0 ) || ( strcmp("$ext","jpeg") == 0 ))
  {
    imagejpeg($virtual_image, $dest);
  }
  else if ( strcmp($ext,"png") == 0 )
  {
    imagepng($virtual_image, $dest);
  }
  else if ( strcmp($ext,"gif") == 0 )
  {
    imagegif($virtual_image, $dest);
  }
}
?>
