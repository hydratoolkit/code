<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Hydra</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="../css/style.css" rel="stylesheet" type="text/css" /> 
    <script type="text/javascript" src="../js/jquery-1.6.4.min.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="../js/jquery.opacityrollover.js"></script>
    <style>
        ul.thumbs { list-style-type: none; }
    </style>
</head>

<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <div class="logo">
        <h1><a href="index.php"><span>Hydra</span>-TH <small>Advanced Thermal Hydraulics</small></a></h1>
      </div>
      <div class="menu">
        <ul>
          <li><a href="http://hydra.lanl.gov/index.php" class="active">Home</a></li>
          <li><a href="http://hydra.lanl.gov/repo/">Code Repository</a></li>
          <li><a href="http://hydra.lanl.gov/wiki">Wiki</a></li>
          <li><a href="http://hydra.lanl.gov/redmine/projects/hydra-th">Project Management</a></li>
          <li><a href="http://hydra.lanl.gov/dashboard">Dashboard</a></li>
        </ul>
      </div>
      <div class="clr"></div>
    </div>
    <div class="headert_text_resize">
      <div class="headert_text">
        <h2>Hydra</h2>
        <p>Results Gallery</p>
      </div>
      <img src="../images/hydra_logo.png" alt="" width="384" height="198" />
      <div class="clr"></div>
    </div>
  </div>
  <div class="body">
    <div class="body_resize">
    <!--  <div class="left"> -->
        <div class="resize_bg">

<a href='http://hydra.lanl.gov/gallery/upload.php' style='float:right;'>Upload a new image of Hydra results</a>
    <div id="thumbs">
        <ul class="thumbs noscript">
<?php
//
// Grab the MySQL db pull all of the images and create the html that jquery galleriffic needs.
//
    require("../class.MySqlDb.php");
    require("db.conf");

    // Open a connection to the db
    $db = new MySqlDb($db_host, $db_username, $db_password, $db_name);
    $db->connect();

    $query = "SELECT * FROM galleryimage";
    $results = $db->getResults( $query );

    while ( $row = mysql_fetch_array($results) )
    {
        echo 
            "<li>\n" .
            "    <a class='thumb' name='imageid-" . $row['id'] . "' href='images/" . $row['filename'] . "' title='" . $row['title'] ."'>\n" .
            "        <img src='thumbnails/" . $row['filename'] . "' alt='" . $row['title'] . "' />\n" .
            "    </a>\n" .
            "    <div class='caption'>\n" .
            "        <div class='download'>\n" .
            "            <a href='images/" . $row['filename'] . "'>Download Original</a>\n" .
            "        </div>\n" .
            "        <div class='image-title'>" . $row['title'] . "</div>\n" .
            "        <div class='image-desc'>" . $row['description'] . "</div>\n" .
            "    </div>\n" .
            "</li>\n";
    }
?>
    </ul>
    </div><!-- thumbs -->
  </div>
</div>
</div>
  <div class="footer">
    <div class="footer_resize">
      <div class="clr"></div>
    </div>
    <div class="clr"></div>
  </div>
</div>
</body>
<script type="text/javascript">
jQuery(document).ready(function($) {
				// We only want these styles applied when javascript is enabled
				$('div.navigation').css({'width' : '300px', 'float' : 'left'});
				$('div.content').css('display', 'block');

				// Initially set opacity on thumbs and add
				// additional styling for hover effect on thumbs
				var onMouseOutOpacity = 0.67;
				$('#thumbs ul.thumbs li').opacityrollover({
					mouseOutOpacity:   onMouseOutOpacity,
					mouseOverOpacity:  1.0,
					fadeSpeed:         'fast',
					exemptionSelector: '.selected'
				});
});
</script>
</html>
