<?php
if ( array_key_exists('submit', $_POST) )
{
    require("../class.MySqlDb.php");
    require("db.conf");
    require("fn.make_thumbnail.php");

    // Get the name of the file passed in
    $image = $_FILES['image']['name'];
    // Get the name of the tmp file that php stored it as
    $tmp_file = $_FILES['image']['tmp_name'];

    // Get the extension of the filename
    $filename = stripslashes($image);
    $i = strrpos($filename,".");
    $l = strlen($filename) - $i;
    $ext = substr($filename,$i+1,$l);
    $ext = strtolower($ext);

    // Check the extension and make sure it is an image
    if (($ext != "jpg") && ($ext != "jpeg") && ($ext != "png") && ($ext != "gif"))
    {
        echo "The extension (" . $ext . ") is unknown. The transfer has been terminated.";
        exit();
    }

    // Get a unique new name with the correct extension
    $new_name = time() . '.' . $ext;

    // Copy the tmp file into the correct location
    $move_success = move_uploaded_file($tmp_file, "images/" . $new_name);

    // Make sure the copy happened successfully
    if (!$move_success)
    {
        echo "For some reason the file could not be moved into the correct dir.";
        exit();
    }

    // Create a thumbnail
    make_thumbnail("images/" . $new_name, "thumbnails/" . $new_name, 75);

    // Open a connection to the db
    $db = new MySqlDb($db_host, $db_username, $db_password, $db_name);
    $db->connect();

    // Grab the other info about the image that was POSTed
    $title = mysql_real_escape_string( $_POST['title'] );
    $description = mysql_real_escape_string( $_POST['description'] );

    // Shove this information into the database
    $query = "INSERT INTO galleryimage (title, description, filename, date) " .
             "VALUES('" . $title . "','" . $description . "','" . $new_name . "',NOW())";
    $db->execute( $query );

    $db->close();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Upload Images of Hydra Results</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="../css/style.css" rel="stylesheet" type="text/css" /> 
    <link rel="stylesheet" href="../css/south-street/jquery-ui-1.8.16.custom.css" id="theme">
<style>
  dl {
    padding: 0.5em;
  }
  dt {
    float: left;
    clear: left;
    width: 275px;
    text-align: left;
    font-weight: bold;
    color: green;
  }
  dt:after {
    content: ":";
  }
  dd {
    margin: 0 0 0 300px;
    padding: 0 0 0.5em 0;
  }
  textarea {
    width: 350px;
    height: 120px;
  }
</style>
</head>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <div class="logo">
        <h1><a href="index.php"><span>Hydra</span>-TH <small>Advanced Thermal Hydraulics</small></a></h1>
      </div>
      <div class="menu">
        <ul>
          <li><a href="http://hydra.lanl.gov/index.php" class="active">Home</a></li>
          <li><a href="http://hydra.lanl.gov/repo/">Code Repository</a></li>
          <li><a href="http://hydra.lanl.gov/wiki">Wiki</a></li>
          <li><a href="http://hydra.lanl.gov/redmine/projects/hydra-th">Project Management</a></li>
          <li><a href="http://hydra.lanl.gov/dashboard">Dashboard</a></li>
        </ul>
      </div>
      <div class="clr"></div>
    </div>
    <div class="headert_text_resize">
      <div class="headert_text">
        <h2>Hydra</h2>
        <p>Upload Results to Gallery</p>
      </div>
      <img src="../images/hydra_logo.png" alt="" width="384" height="198" />
      <div class="clr"></div>
    </div>
  </div>
  <div class="body">
    <div class="body_resize">
      <div class="resize_bg">
<?php
// If we just uploaded a file, inform the user that it was successful 
if ( array_key_exists('submit', $_POST) )
{
    echo "<div id='upload-history' class='ui-widget ui-widget-content ui-corner-all'>\n";
    echo "The image: " . $image . " was uploaded successfully.\n";
    echo "</div>\n";
}
?>
        <div id="input-form" class="ui-widget ui-widget-content ui-corner-all">
        <form enctype="multipart/form-data" action="upload.php" method="POST">
            <dl>
                <dt>Choose a file to upload</dt> <dd><input name="image" type="file" /></dd>
                <dt>Title</dt> <dd><input name="title" type="text" style="width:350px;"></dd>
                <dt>Description</dt> <dd><textarea name="description" type="text" ></textarea></dd>
                <input type="submit" value="Upload File" name="submit" class="fg-button ui-state-default ui-corner-all" />
            </dl>
        </form>
        </div> <!-- "input-form" -->
      </div> <!-- resize_bg -->
    </div> <!-- body_resize -->
  </div> <!-- body -->
</div> <!-- main -->
<script src="../js/jquery-1.6.4.min.js"></script>
<script src="../js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" charset="utf-8"> 

$(document).ready(function() {
 
    // On clicking the submit button:
    //   1. Add a new horizontal box with
    //      a. File Name
    //      b. Progress
    //      c. Any error or success message
    //   2. Submit ajax post
});

</script>
</body> 
</html>
