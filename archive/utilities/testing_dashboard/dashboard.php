<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Hydra Dashboard</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css" title="currentStyle"> 
	@import "css/demo_table.css"; 
  dl {
    border: 3px solid #ccc;
    padding: 0.5em;
  }
  dt {
    float: left;
    clear: left;
    width: 300px;
    text-align: left;
    font-weight: bold;
    color: green;
  }
  dt:after {
    content: ":";
  }
  dd {
    margin: 0 0 0 325px;
    padding: 0 0 0.5em 0;
  }
  dd.percentage:after {
    content: " %";
  }
</style> 
<link type="text/css" href="css/south-street/jquery-ui-1.8.16.custom.css" rel="stylesheet" />   
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>

<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script> 
<script type="text/javascript" charset="utf-8"> 

var regressionTable;
var selectedTable;
var selectedRegressionId = -1; // Start off at an error state

$(document).ready(function() {

	regressionTable = $('#example').dataTable( {
		"bJQueryUI": true,
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "getDashboardData.php",
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "table", "value": "regression" } );
			$.getJSON( sSource, aoData, function (json) {
				fnCallback(json);
			});
		},
                "aaSorting": [[5,'desc']],
                "aoColumnDefs": [
                        /* ID */ 	{ "bSearchable": true, "bVisible": false, "aTargets": [ 0 ] },
                        /* username */  { "bSearchable": true, "bVisible": true,  "aTargets": [ 1 ] },
                        /* machine */   { "bSearchable": true, "bVisible": true,  "aTargets": [ 2 ] },
                        /* OS */   	{ "bSearchable": true, "bVisible": true,  "aTargets": [ 3 ] },
                        /* name */      { "bSearchable": true, "bVisible": true,  "aTargets": [ 4 ] },
                        /* date */      { "bSearchable": true, "bVisible": true,  "aTargets": [ 5 ] },
                        /* num_tests */ { "bSearchable": true, "bVisible": true,  "aTargets": [ 6 ] },
                        /* num_passed */{ "bSearchable": true, "bVisible": true,  "aTargets": [ 7 ] }
                ]
	} );

	selectedTable = $('#selected').dataTable( {
		"bJQueryUI": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "getDashboardData.php",
                "fnServerData": function ( sSource, aoData, fnCallback ) {
                        aoData.push( { "name": "table", "value": "test" } );
                        aoData.push( { "name": "regression_id", "value": selectedRegressionId } );
                        $.getJSON( sSource, aoData, function (json) {
                                fnCallback(json);
                        });
                },
		"aoColumnDefs": [
                        /* Test Name */     { "bSearchable": true,  "bVisible": true,   "aTargets": [ 0 ] },
			/* path */ 	    { "bSearchable": true,  "bVisible": false,  "aTargets": [ 1 ] },
                        /* input */ 	    { "bSearchable": true,  "bVisible": false,  "aTargets": [ 2 ] },
                        /* output */ 	    { "bSearchable": true,  "bVisible": false,  "aTargets": [ 3 ] },
                        /* control */ 	    { "bSearchable": true,  "bVisible": false,  "aTargets": [ 4 ] },
                        /* baseline */ 	    { "bSearchable": true,  "bVisible": false,  "aTargets": [ 5 ] },
			/* diff */ 	    { "bSearchable": true,  "bVisible": false,  "aTargets": [ 6 ] },
                        /* NProc */  	    { "bSearchable": true,  "bVisible": true,   "aTargets": [ 7 ] },
                        /* author */  	    { "bSearchable": true,  "bVisible": false,  "aTargets": [ 8 ] },
                        /* status */  	    { "bSearchable": true,  "bVisible": true,   "aTargets": [ 9 ] },
                        /* el cycle time */ { "bSearchable": false, "bVisible": true,   "aTargets": [ 10 ] },
                        /* solution time */ { "bSearchable": false, "bVisible": false,  "aTargets": [ 11 ] }
		]
	} );

	/* Add a click handler to the rows - this could be used as a callback */
	$('#example tbody').click(function(event) {
		// Remove all highlights from rows
		$(regressionTable.fnSettings().aoData).each(function (){
		    $(this.nTr).removeClass('row_selected');
		});

		// Highlight clicked row
		$(event.target.parentNode).addClass('row_selected');

		/* Get the position of the current data from the node */
                var aPos = regressionTable.fnGetPosition( event.target );
                /* Get the data array for this row */
                var aData = regressionTable.fnGetData( aPos[0] );

		// Fill in the summary table using aData[i]
		selectedRegressionId = aData[0];
		// Load selected table
		selectedTable.fnDraw();

		// Now update the summary table
		var newSummaryHTML = $.get(
        		"getDashboardData.php",
        		[ { 'name' : 'regression_id', 'value' : selectedRegressionId },
                	  { 'name' : 'table', 'value' : 'summary' } ],
			function(data)
			{
				$('dl.summary').html(data);
			}
		);
	});

	/* Add a click handler to the rows - create a popup box */
	$("#selected tbody").click(function(event) {
		// Remove all highlights from rows
		$(selectedTable.fnSettings().aoData).each(function (){
		    $(this.nTr).removeClass('row_selected');
		});
		// Highlight clicked row
		$(event.target.parentNode).addClass('row_selected');

		/* Get the position of the current data from the node */
		var aPos = selectedTable.fnGetPosition( event.target );
		/* Get the data array for this row */
		var aData = selectedTable.fnGetData( event.target.parentNode );
		// Load a popup box
		$("#test-popup").html(
						"<dl>" +
        					"<dt>Test Name</dt><dd>" + aData[0] + "</dd>" +
        					"<dt>Path</dt><dd>" + aData[1] + "</dd>" +
        					"<dt>Input File</dt><dd>" + aData[2] + "</dd>" +
        					"<dt>Output File</dt><dd>" + aData[3] + "</dd>" +
        					"<dt>Control File</dt><dd>" + aData[4] + "</dd>" +
        					"<dt>Baseline File</dt><dd>" + aData[5] + "</dd>" +
        					"<dt>Diff File</dt><dd>" + aData[6] + "</dd>" +
                            			"<dt>Number of Processors</dt><dd>" + aData[7] + "</dd>" +
                            			"<dt>Author</dt><dd>" + aData[8] + "</dd>" +
                            			"<dt>Test Status</dt><dd>" + aData[9] + "</dd>" +
						"<dt>Element Cycle Time (s)</dt><dd>" + aData[10] + "</dd>" +
                                                "<dt>Solution Time (s)</dt><dd>" + aData[11] + "</dd>" +
						"</dl>");
		$("#test-popup").dialog({ minWidth: 750 });
	});
} );

</script>

</head>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <div class="logo">
        <h1><a href="index.php"><span>Hydra</span>-TH <small>Advanced Thermal Hydraulics</small></a></h1>
      </div>
    <div class="headert_text_resize">
      <div class="headert_text">
        <h2>Hydra<br /></h2>
        <p>Regression Testing Dashboard</p>
      </div>
      <img src="images/hydra_logo.png" alt="" width="384" height="198" />
      <div class="clr"></div>
    </div>
  </div>

<div class="body"> <!-- restricts width -->
<div class="body_resize">
<!-- Table of regression tests -->
<div class="resize_bg"><span style='clear: both; float: right;'><a href='http://hydra.lanl.gov/hydra/updateDashboard.php'>Upload Regression Results</a></span>
<h2>Regression Runs</h2>
<hr/>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="example"> 
	<thead> 
		<tr> 
			<th width="0%">Record ID</th>
                        <th width="5%">Username</th> 
			<th width="10%">Machine</th> 
                        <th width="30%">Operating System</th>
			<th width="30%">Regression Name</th> 
			<th width="20%">Date</th> 
			<th width="2%">Tests</th>
			<th width="3%">Passed</th> 
		</tr> 
	</thead> 
	<tbody> 
		<tr> 
			<td colspan="5" class="dataTables_empty">Loading data from server</td> 
		</tr> 
	</tbody> 
	<tfoot> 
		<tr> 
			<th>Record ID</th>
                        <th>Username</th>
			<th>Machine</th>
                        <th>Operating System</th> 
			<th>Regression Name</th> 
			<th>Date</th> 
			<th>Tests</th>
			<th>Passed</th> 
		</tr> 
	</tfoot> 
</table> 
</div>
<!-- Selected regression test -->
<div class="resize_bg">
<h2>Selected Regression</h2>
<hr/>
<h3>Summary</h3>
<dl class="summary">
	<dt>Number of Tests</dt><dd>-</dd>
	<dt>Number Passed</dt><dd>-</dd>
	<dt>Hydra Failures</dt><dd>-</dd>
	<dt>Hydra Crashes</dt><dd>-</dd>
	<dt>Exodus Comparison Failures</dt><dd>-</dd>
	<dt>Exodiff Crashes</dt><dd>-</dd>
	<dt>Percent Failed</dt><dd class="percentage">-</dd>
</dl>

<h3>Individual Tests</h3>
<table style="" cellpadding="0" cellspacing="0" border="0" class="display" id="selected">
        <thead>
                <tr>
                        <th width="65%">Test Name</th>
                        <th width="0%">Path</th>
                        <th width="0%">Input</th>
                        <th width="0%">Output</th>
                        <th width="0%">Control</th>
                        <th width="0%">Baseline</th>
                        <th width="0%">Diff File</th>
                        <th width="5%">Processors</th>
                        <th width="0%">Author</th>
                        <th width="20%">Status</th>
                        <th width="10%">Element Cycle Time (s)</th>
                        <th width="0%">Solution Time (s)</th>
                </tr>
        </thead>
        <tbody>
                <tr>
                        <td colspan="5" class="dataTables_empty">Loading data from server</td>
                </tr>
        </tbody>
        <tfoot>
                <tr>
                        <th>Test Name</th>
                        <th>Path</th>
                        <th>Input</th>
                        <th>Output</th>
                        <th>Control</th>
                        <th>Baseline</th>
                        <th>Diff File</th>
                        <th>Processors</th>
                        <th>Author</th>
                        <th>Status</th>
                        <th>Element Cycle Time (s)</th>
                        <th>Solution Time (s)</th>
                </tr>
        </tfoot>

</table>
</div>
</div>
</div>
</div>
<div id="test-popup"></div>
</div>
</body>
</html>

