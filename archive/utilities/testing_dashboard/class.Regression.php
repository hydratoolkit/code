<?php

class Test
{
	///////////////////////////////
	// Member Variables
	///////////////////////////////

	public	$name;
	public	$path;
	public	$input;
	public	$output;
	public	$control;
	public	$baseline;
	public	$diff;
	public	$Nproc;
	public	$author;
        public  $elementCycleTime;
        public  $solutionTime;
};

class SystemInformation
{
	///////////////////////////////
	// Member Variables
	///////////////////////////////

	public	$machineName;
	public	$compiler;
	public	$operatingSystem;
	public	$OSRelease;
	public	$processor;
        public  $hydraVersion;
};

class RegressionSummary
{
        public  $username;
	public	$numTests;
	public	$numPassed;
	public	$numHydraFails;
	public	$numHydraCrashes;
	public	$numExodusComp;
	public	$numExodiffCrashes;
	public	$percentFailed;
};

class Regression
{
	///////////////////////////////
	// Member Variables
	///////////////////////////////
	
	public	$name;
	public	$date;
	public	$test;
	public	$summary;
	public	$system;
	
    ///////////////////////////////
    // Constructors
    ///////////////////////////////

    /** 
     * \fn          Contructor
     * \brief       Provides a constructor that initializes the
     *				testSuite member variable as an array.
     */
    public function __construct()
    {
    	$this->test 	= array();
    	$this->summary 	= new RegressionSummary();
    	$this->system 	= new SystemInformation();
    }
};

?>
