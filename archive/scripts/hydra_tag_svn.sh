#!/bin/bash -u
#
# Tag the last commit in the Hydra SVN repository

# Require the following arguments: <tagname> <commit_message>
die () { echo >&2 "$@"; exit 1; }
[ "$#" -eq 2 ] || \
die "Exactly two arguments required: <tagname> <commit_message>"

# Set repository hostname and path
REPOSITORY_HOSTNAME="ccscs8.lanl.gov"
REPOSITORY_PATH="/codes/casl/hydra"

# Get last revision
LAST_REVISION=`svn log -l 1 -q \
               svn+ssh://$REPOSITORY_HOSTNAME$REPOSITORY_PATH/trunk | \
               grep ^r | awk '{print $1}' | cut -c2-`

# Create tag
svn copy -r $LAST_REVISION \
  svn+ssh://$REPOSITORY_HOSTNAME$REPOSITORY_PATH/trunk \
  svn+ssh://$REPOSITORY_HOSTNAME$REPOSITORY_PATH/tags/$1 \
  -m "$2"
