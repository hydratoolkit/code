#!/bin/bash
#
# Platform and compiler dependent configuration for Hydra & ThirdParty libs build.
# Anything in this file will overrule the defaults in the build script.
#
# PLATFORM is a combination of:
#   (1) OS: titan
#   (2) COMPILER: intel
# Required modules on Titan:
# module swap PrgEnv-pgi PrgEnv-intel
# module load hdf5 netcdf cray-petsc

# This is platform titan-intel
PLATFORM=titan-intel

# Path to hydra source
HYDRA_SRC_DIR=${HYDRA}/src

# Path where the ThirdParty library tarballs reside
TARBALLS_DIR=${HYDRA_ROOT}/tpl/tarballs

# Path where the ThirdParty (tpl) libraries will be installed
#   - Must be absolute
#   - TPLs in project dir on conejo/mapache:
#       * GNU:   /usr/projects/casl/hydra-th/tpl/gnu
#       * INTEL: /usr/projects/casl/hydra-th/tpl/intel
#   - TPLs in project dir on titan:
#       * GNU:   /ccs/proj/nfi007/hydra-th/tpl/gnu
#       * INTEL: /ccs/proj/nfi007/hydra-th/tpl/intel
TPL_INSTALL_DIR=${HYDRA_ROOT}/tpl/intel

# Path to hydra build
HYDRA_BUILD_DIR=${HYDRA_ROOT}/build/intel

# Path to hydra install
HYDRA_INSTALL_DIR=${HYDRA_ROOT}/install/intel

# Filenames of library tarballs:
#  - They must all reside in ${TARBALLS_DIR} relative to the current dir
#  - They must all end in tar.gz
#  - They must all create a directory with the same name w/o "tar.gz"
OPENMPI_FILENAME=openmpi-1.6.5.tar.gz
TRILINOS_FILENAME=trilinos-10.6.4-Source.tar.gz
ZLIB_FILENAME=zlib-1.2.5-patched.tar.gz
HDF5_FILENAME=hdf5-1.8.7-patched.tar.gz
NETCDF_FILENAME=netcdf-4.1.3.tar.gz
EXODUS_FILENAME=exodusii-5.14-patched.tar.gz
PARMETIS_FILENAME=parmetis-4.0.2-patched.tar.gz
PETSC_FILENAME=petsc-3.3-p6-patched.tar.gz
ML_FILENAME=ml-6.2.tar.gz
HYPRE_FILENAME=hypre-2.8.0b.tar.gz

# Bools that show whether the lib is to be built
OPENMPI=false
TRILINOS=true
ZLIB=true
HDF5=false
NETCDF=false
EXODUS=true
PARMETIS=true
PETSC=false
HYDRA=true

# Source directory names (used after untar of each)
OPENMPI_DIR=`echo $OPENMPI_FILENAME | sed 's/[ tar.gz]*$//'`
TRILINOS_DIR=`echo $TRILINOS_FILENAME | sed 's/[ tar.gz]*$//'`
ZLIB_DIR=`echo $ZLIB_FILENAME | sed 's/[ tar.gz]*$//'`
HDF5_DIR=`echo $HDF5_FILENAME | sed 's/[ tar.gz]*$//'`
NETCDF4_DIR=`echo $NETCDF_FILENAME | sed 's/[ tar.gz]*$//'`
EXODUS_DIR=`echo $EXODUS_FILENAME | sed 's/[ tar.gz]*$//'`
PARMETIS_DIR=`echo $PARMETIS_FILENAME | sed 's/[ tar.gz]*$//'`
PETSC_DIR=`echo $PETSC_FILENAME | sed 's/[ tar.gz]*$//'`

# Configure commands
FARG="-O3 -ffree-line-length-none"
TRILINOS_CONF_CMD=" \
  cmake \
      -D CMAKE_INSTALL_PREFIX:PATH=${TPL_INSTALL_DIR} \
      -D CMAKE_Fortran_FLAGS:STRING=${FARG} \
      -D CMAKE_Fortran_COMPILER:PATH=ftn \
      -D CMAKE_C_COMPILER:PATH=cc \
      -D CMAKE_CXX_COMPILER:PATH=CC \
      -D TPL_ENABLE_MPI:BOOL=ON \
      -D Trilinos_ENABLE_Zoltan:BOOL=ON \
      ../${TRILINOS_DIR}"

ZLIB_CONF_CMD=" \
  cmake \
      -D BUILD_SHARED_LIBS=off \
      -D CMAKE_C_FLAGS=-fPIC \
      -D CMAKE_C_COMPILER:PATH=cc \
      -D CMAKE_INSTALL_PREFIX:PATH=${TPL_INSTALL_DIR} \
      ."

HDF5_CONF_CMD=" \
  cmake \
      -D CMAKE_C_COMPILER:PATH=cc \
      -D CMAKE_CXX_COMPILER:PATH=CC \
      -D CMAKE_INSTALL_PREFIX:PATH=${TPL_INSTALL_DIR} \
      -D HDF5_BUILD_HL_LIB:BOOL=ON \
      ."

NETCDF_CONF_CMD=" \
  ./configure \
      CC=cc CXX=CC F77=ftn FC=ftn \
      --prefix=${TPL_INSTALL_DIR} \
      --disable-examples \
      --enable-netcdf4 \
      --disable-dap \
      --disable-shared \
      CPPFLAGS=-I${TPL_INSTALL_DIR}/include \
      LDFLAGS=-L${TPL_INSTALL_DIR}/lib"

EXODUS_CONF_CMD=" \
  cmake \
      -D CMAKE_INSTALL_PREFIX:PATH=${TPL_INSTALL_DIR} \
      -D HDF5HL_LIBRARY:PATH=${TPL_INSTALL_DIR}/lib/libhdf5_hl.a \
      -D HDF5_LIBRARY:PATH=${TPL_INSTALL_DIR}/lib/libhdf5.a \
      -D NETCDF_INCLUDE_DIR:PATH=${TPL_INSTALL_DIR}/include \
      -D NETCDF_LIBRARY:PATH=${TPL_INSTALL_DIR}/lib/libnetcdf.a \
      -D NETCDF_NCDUMP:PATH=${TPL_INSTALL_DIR}/bin/ncdump \
      -D Z_LIBRARY:PATH=${TPL_INSTALL_DIR}/lib/libz.a \
      -D CMAKE_C_COMPILER:PATH=cc \
      -D CMAKE_CXX_COMPILER:PATH=CC \
      ../${EXODUS_DIR}/exodus"

NEMESIS_CONF_CMD=" \
  cmake \
      -D CMAKE_INSTALL_PREFIX:PATH=${TPL_INSTALL_DIR} \
      -D EXODUS_LIBRARY:PATH=${TPL_INSTALL_DIR}/lib/libexoIIv2c.a \
      -D HDF5HL_LIBRARY:PATH=/opt/cray/hdf5/1.8.8/gnu/47/lib/libhdf5_hl.a \
      -D HDF5_LIBRARY:PATH=/opt/cray/hdf5/1.8.8/gnu/47/lib/libhdf5.a \
      -D NETCDF_INCLUDE_DIR:PATH=${TPL_INSTALL_DIR}/include \
      -D NETCDF_LIBRARY:PATH=${TPL_INSTALL_DIR}/lib/libnetcdf.a \
      -D NETCDF_NCDUMP:PATH=${TPL_INSTALL_DIR}/bin/ncdump \
      -D Z_LIBRARY:PATH=${TPL_INSTALL_DIR}/lib/libz.a \
      -D CMAKE_C_COMPILER:PATH=cc \
      -D CMAKE_CXX_COMPILER:PATH=CC \
      ../${EXODUS_DIR}/nemesis"

PARMETIS_CONF_CMD="make config cc=cc cxx=CC prefix=${TPL_INSTALL_DIR}"

PETSC_CONF_CMD=" \
  ./configure \
      PETSC_DIR=${TPL_BUILD_DIR}/${PETSC_DIR} \
      PETSC_ARCH=${PLATFORM}-opt \
      --prefix=${TPL_INSTALL_DIR} \
      --with-cc=cc \
      --with-cxx=CC \
      --with-fc=ftn \
      --with-mpiexec=/bin/false \
      --with-blas-lapack-lib=-lsci_intel_mp \
      --with-shared-libraries=0 \
      --with-scalar-type=real \
      --with-clib-autodetect=0 \
      --with-cxxlib-autodetect=0 \
      --with-fortranlib-autodetect=0 \
      --with-fortran-kernels=1 \
      --with-fortran-interfaces=1 \
      --with-clanguage=c++ \
      --with-x=false \
      --with-x11=false \
      --download-ml=${TARBALLS_DIR}/${ML_FILENAME} \
      --download-hypre=${TARBALLS_DIR}/${HYPRE_FILENAME} \
      --with-debugging=0 \
      -COPTFLAGS=-O3 \
      -CXXOPTFLAGS=-O3 \
      -FOPTFLAGS=-O3 \
      -FFLAGS=-ffree-line-length-none"

HYDRA_CONF_CMD=" \
    cmake \
      -D PLATFORM:STRING=$PLATFORM \
      -D THIRD_PARTY_PREFIX:PATH=${TPL_INSTALL_DIR} \
      -D CMAKE_BUILD_TYPE:STRING=RELEASE \
      -D CMAKE_INSTALL_PREFIX:PATH=${HYDRA_INSTALL_DIR} \
      -D CMAKE_Fortran_COMPILER:PATH=ftn \
      -D CMAKE_C_COMPILER:PATH=cc \
      -D CMAKE_CXX_COMPILER:PATH=CC \
      -D BLAS_LIB_PATH:PATH=/opt/xt-libsci/default/intel/120/interlagos/lib \
      -D LAPACK_LIB_PATH:PATH=/opt/xt-libsci/default/intel/120/interlagos/lib \
      -D PMI_LIB_PATH:PATH=/opt/cray/pmi/3.0.0-1.0000.8661.28.2807.gem/lib64 \
      -D CMAKE_NO_BUILTIN_CHRPATH:BOOL=ON \
     ${HYDRA_SRC_DIR}"
