#!/bin/bash -u
#
# Update hydra git repository -- this script runs on navier
#
# This script pulls in changes from the main hydra (svn) repository on ccscs8
# to the hydra git repository on navier and will push/pull changes to
# ccs-green - this last step to ccs-green is not enabled at this time.
#
# The three repositories involved are:
# (1) hydra:          ccscs8:/ccs/codes/casl/hydra      - master, svn
# (2) hydra.git:      navier:~jbakosi/codes/hydra.git   - hydra, git
# (3) hydra-all:      ccs-green:/var/hydra/hydra-all    - hydra, bare git
#
# Arguments to bash:
# -u: will exit if we try to use an uninitialised variable

##############################################################################
# git configurations for all repositories involved,
# the longest is the mediator, where everything happens
##############################################################################
#
# navier:~jbakosi/code/hydra.git/.git/config:
#[core]
#        repositoryformatversion = 0
#        filemode = true
#        bare = false
#        logallrefupdates = true
#[svn-remote "svn"]
#        url = svn+ssh://ccscs8.lanl.gov/ccs/codes/casl/hydra
#        fetch = trunk:refs/remotes/trunk
#        branches = branches/*:refs/remotes/*
#        tags = tags/*:refs/remotes/tags/*
#[remote "green"]
#        url = ssh://jbakosi@ccs-green/var/hydra/hydra-all
#        fetch = +refs/heads/master:refs/remotes/green/master
#
##############################################################################
#
# ccs-green:/var/hydra/hydra-all/config:
#[core]  
#        repositoryformatversion = 0
#        filemode = true
#        bare = true
#[remote "origin"]
#        url = /home/jbakosi/code/hydra.git
#
##############################################################################

# Debug mode with output to screen, otherwise the output is mailed for each
# command if an error is encountered
DEBUG=false

# Email to
EMAIL="jbakosi@lanl.gov christon@lanl.gov"

# git executable
GIT="git"

# Definition of commands

# Pull in changes from master svn
SVN_FETCHCMD="$GIT svn fetch"
SVN_MERGECMD="$GIT merge remotes/trunk"

# Push out changes to hydra on ccs-green
GIT_FETCHCMD_CCS_GREEN="$GIT fetch green"
GIT_MERGECMD_CCS_GREEN="$GIT merge remotes/green/master"
GIT_PUSHCMD_CCS_GREEN="$GIT push green"

# All steps happen in hydra.git
cd /home/jbakosi/code/hydra.git

# Do a "git svn rebase" in two steps:
# (1) fetch (always safe)
# (2) merge (may result in conflicts)

if [[ $DEBUG == false ]]; then
  SVN_FETCHCMD_OUT=$($SVN_FETCHCMD 2>&1)
else
  echo -e "(1) $SVN_FETCHCMD ...\n"
  SVN_FETCHCMD_OUT=$($SVN_FETCHCMD)
fi

if [ "$?" -ne "0" ]; then
  echo -e "There was an error executing '$SVN_FETCHCMD' in hydra-th.trans:\n\n$SVN_FETCHCMD_OUT" \
    | mailx -s "hydra-th.trans svn fetch error" $EMAIL
fi

if [[ $DEBUG == false ]]; then
  SVN_MERGECMD_OUT=$($SVN_MERGECMD 2>&1)
else
  echo -e "(2) $SVN_MERGECMD ...\n"
  SVN_MERGECMD_OUT=$($SVN_MERGECMD)
fi

if [ "$?" -ne "0" ]; then
  echo -e "There was an error executing '$SVN_MERGECMD' in hydra-th.trans:\n\n$SVN_MERGECMD_OUT" \
    | mailx -s "hydra-th.trans svn merge error" $EMAIL
fi

#
# NOT ENABLED AT THIS TIME
#
# Do a "git push" in three steps to ccs-green
# (1) fetch (always safe)
# (2) merge (may result in conflicts)
# (3) push  (should be safe after fetch & merge)
#
#if [[ $DEBUG == false ]]; then
#  GIT_FETCHCMD_OUT=$($GIT_FETCHCMD_CCS_GREEN 2>&1)
#else
#  echo -e "(3) $GIT_FETCHCMD_CCS_GREEN ...\n"
#  GIT_FETCHCMD_OUT=$($GIT_FETCHCMD_CCS_GREEN)
#fi
#
#if [ "$?" -ne "0" ]; then
#  echo -e "There was an error executing '$GIT_FETCHCMD_CCS_GREEN' in hydra-th.trans:\n\n$GIT_FETCHCMD_OUT" \
#    | mailx -s "hydra-th.trans git fetch error" $EMAIL
#fi
#
#if [[ $DEBUG == false ]]; then
#  GIT_MERGECMD_OUT=$($GIT_MERGECMD_CCS_GREEN 2>&1)
#else
#  echo -e "(4) $GIT_MERGECMD_CCS_GREEN ...\n"
#  GIT_MERGECMD_OUT=$($GIT_MERGECMD_CCS_GREEN)
#fi
#
#if [ "$?" -ne "0" ]; then
#  echo -e "There was an error executing '$GIT_MERGECMD_CCS_GREEN' in hydra-th.trans:\n\n$GIT_MERGECMD_OUT" \
#    | mailx -s "hydra-th.trans git merge error" $EMAIL
#fi
#
#if [[ $DEBUG == false ]]; then
#  GIT_PUSHCMD_OUT=$($GIT_PUSHCMD_CCS_GREEN 2>&1)
#else
#  echo -e "(5) $GIT_PUSHCMD_CCS_GREEN ...\n"
#  GIT_PUSHCMD_OUT=$($GIT_PUSHCMD_CCS_GREEN)
#fi
#
#if [ "$?" -ne "0" ]; then
#  echo -e "There was an error executing '$GIT_PUSHCMD_CCS_GREEN' in hydra-th.trans:\n\n$GIT_PUSHCMD_OUT" \
#    | mailx -s "hydra-th.trans git push error" $EMAIL
#fi
