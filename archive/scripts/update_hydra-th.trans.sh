#!/bin/bash -u
#
# Update hydra repositories
#
# This script pulls in changes from the main hydra (svn) repository
# to hydra-th.trans and pushes the changes to the hydra-th
# repository on ccscs8 and to the hydra-th repository on ccs-green.
#
# The four repositories involved are:
# (1) hydra:          ccscs8:/codes/casl/hydra          - master, svn
# (2) hydra-th.trans: ccscs8:/codes/casl/hydra-th.trans - trans, git
# (3) hydra-th:       ccscs8:/codes/casl/hydra-th       - hydra-th, bare git
# (4) hydra-th:       ccs-green:/var/hydra/hydra-th     - hydra-th, bare git
#
# For more information see doc/design/repository-setup.*
#
# Arguments to bash:
# -u: will exit if we try to use an uninitialised variable

# Debug mode with output to screen, otherwise the output is mailed for each command
# if an error is encountered
DEBUG=false

# Email to
EMAIL="jbakosi@lanl.gov christon@lanl.gov"

# git executable
GIT="/ccs/opt/x86_64/git/bin/git"

# Definition of commands

# Pull in changes from master svn: ALWAYS UNIDIRECTIONAL: SVN -> TRANS
SVN_FETCHCMD="$GIT svn fetch"
SVN_MERGECMD="$GIT merge remotes/trunk"

# Push out changes to hydra-th on ccscs8 and ccs-green
GIT_FETCHCMD_CCSCS8="$GIT fetch origin master"
GIT_MERGECMD_CCSCS8="$GIT merge remotes/origin/master"
GIT_PUSHCMD_CCSCS8="$GIT push origin master"

GIT_FETCHCMD_CCS_GREEN="$GIT fetch green"
GIT_MERGECMD_CCS_GREEN="$GIT merge remotes/green/master"
GIT_PUSHCMD_CCS_GREEN="$GIT push green"

# All steps happen in hydra-th.trans, which facilitates filtering and distribution
cd /codes/casl/hydra-th.trans

# Set umask rwx groups
umask u=rwx,g=rwx,o=

# Do a "git svn rebase" in two steps:
# (1) fetch (always safe)
# (2) merge (may result in conflicts)

if [[ $DEBUG == false ]]; then
  SVN_FETCHCMD_OUT=$($SVN_FETCHCMD 2>&1)
else
  echo -e "(1) $SVN_FETCHCMD ...\n"
  SVN_FETCHCMD_OUT=$($SVN_FETCHCMD)
fi

if [ "$?" -ne "0" ]; then
  echo -e "There was an error executing '$SVN_FETCHCMD' in hydra-th.trans:\n\n$SVN_FETCHCMD_OUT" \
    | mailx -s "hydra-th.trans svn fetch error" $EMAIL
fi

if [[ $DEBUG == false ]]; then
  SVN_MERGECMD_OUT=$($SVN_MERGECMD 2>&1)
else
  echo -e "(2) $SVN_MERGECMD ...\n"
  SVN_MERGECMD_OUT=$($SVN_MERGECMD)
fi

if [ "$?" -ne "0" ]; then
  echo -e "There was an error executing '$SVN_MERGECMD' in hydra-th.trans:\n\n$SVN_MERGECMD_OUT" \
    | mailx -s "hydra-th.trans svn merge error" $EMAIL
fi

# Do a "git push" in three steps to ccscs8
# (1) fetch (always safe)
# (2) merge (may result in conflicts)
# (3) push  (should be safe after fetch & merge)

if [[ $DEBUG == false ]]; then
  GIT_FETCHCMD_OUT=$($GIT_FETCHCMD_CCSCS8 2>&1)
else
  echo -e "(3) $GIT_FETCHCMD_CCSCS8 ...\n"
  GIT_FETCHCMD_OUT=$($GIT_FETCHCMD_CCSCS8)
fi

if [ "$?" -ne "0" ]; then
  echo -e "There was an error executing '$GIT_FETCHCMD_CCSCS8' in hydra-th.trans:\n\n$GIT_FETCHCMD_OUT" \
    | mailx -s "hydra-th.trans git fetch error" $EMAIL
fi

if [[ $DEBUG == false ]]; then
  GIT_MERGECMD_OUT=$($GIT_MERGECMD_CCSCS8 2>&1)
else
  echo -e "(4) $GIT_MERGECMD_CCSCS8 ...\n"
  GIT_MERGECMD_OUT=$($GIT_MERGECMD_CCSCS8)
fi

if [ "$?" -ne "0" ]; then
  echo -e "There was an error executing '$GIT_MERGECMD_CCSCS8' in hydra-th.trans:\n\n$GIT_MERGECMD_OUT" \
    | mailx -s "hydra-th.trans git merge error" $EMAIL
fi

if [[ $DEBUG == false ]]; then
  GIT_PUSHCMD_OUT=$($GIT_PUSHCMD_CCSCS8 2>&1)
else
  echo -e "(5) $GIT_PUSHCMD_CCSCS8 ...\n"
  GIT_PUSHCMD_OUT=$($GIT_PUSHCMD_CCSCS8)
fi

if [ "$?" -ne "0" ]; then
  echo -e "There was an error executing '$GIT_PUSHCMD_CCSCS8' in hydra-th.trans:\n\n$GIT_PUSHCMD_OUT" \
    | mailx -s "hydra-th.trans git push error" $EMAIL
fi

# Do a "git push" in three steps to ccs-green
# (1) fetch (always safe)
# (2) merge (may result in conflicts)
# (3) push  (should be safe after fetch & merge)

if [[ $DEBUG == false ]]; then
  GIT_FETCHCMD_OUT=$($GIT_FETCHCMD_CCS_GREEN 2>&1)
else
  echo -e "(3) $GIT_FETCHCMD_CCS_GREEN ...\n"
  GIT_FETCHCMD_OUT=$($GIT_FETCHCMD_CCS_GREEN)
fi

if [ "$?" -ne "0" ]; then
  echo -e "There was an error executing '$GIT_FETCHCMD_CCS_GREEN' in hydra-th.trans:\n\n$GIT_FETCHCMD_OUT" \
    | mailx -s "hydra-th.trans git fetch error" $EMAIL
fi

if [[ $DEBUG == false ]]; then
  GIT_MERGECMD_OUT=$($GIT_MERGECMD_CCS_GREEN 2>&1)
else
  echo -e "(4) $GIT_MERGECMD_CCS_GREEN ...\n"
  GIT_MERGECMD_OUT=$($GIT_MERGECMD_CCS_GREEN)
fi

if [ "$?" -ne "0" ]; then
  echo -e "There was an error executing '$GIT_MERGECMD_CCS_GREEN' in hydra-th.trans:\n\n$GIT_MERGECMD_OUT" \
    | mailx -s "hydra-th.trans git merge error" $EMAIL
fi

if [[ $DEBUG == false ]]; then
  GIT_PUSHCMD_OUT=$($GIT_PUSHCMD_CCS_GREEN 2>&1)
else
  echo -e "(5) $GIT_PUSHCMD_CCS_GREEN ...\n"
  GIT_PUSHCMD_OUT=$($GIT_PUSHCMD_CCS_GREEN)
fi

if [ "$?" -ne "0" ]; then
  echo -e "There was an error executing '$GIT_PUSHCMD_CCS_GREEN' in hydra-th.trans:\n\n$GIT_PUSHCMD_OUT" \
    | mailx -s "hydra-th.trans git push error" $EMAIL
fi
