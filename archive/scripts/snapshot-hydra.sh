#!/bin/sh
#
#
#
#

#
# Initialize the START_TIME and LOG_FILE variables. If
# log file name is not passed in, the default is
# ${0}-yymmddHHMM.log where $0 is the calling script name.
#
START_TIME=`date +%s`
if [ $# -eq "1" ] ; then 
  LOG_FILE=$1
else
  now=`date +%y%m%d%H%M`
  LOG_FILE=$0-${now}.log
fi

# Delete the log file if it exists
if [ -e ${LOG_FILE} ] ; then
  echo "Previous log file ${LOG_FILE} will be removed"
  rm ${LOG_FILE}
fi

# 
# Usage:
#   log_echo [msg]
#
# Purpose: Echo output msg to log file. If no message is passed in then
#          adds a blank line to the log file.   
log_echo ()
{
   if [ $# -eq "1" ] ; then 
     echo "$1" | tee -a ${LOG_FILE}
   else
     echo | tee -a ${LOG_FILE}
   fi
}
 
#
# Usage:
#   log_header
#
# Purpose: Add a useful header at the top of the log file. Routine will
#          overwrite any existing file named ${LOG_FILE}
log_header ()
{
  host=`hostname`
  os=`uname -m`
  user=`whoami`
  date_now=`date`
  log_echo "================================================================================" 
  log_echo "Machine: $host" 
  log_echo "OS:      $os"
  log_echo "User:    ${user}" 
  log_echo "Date:    $date_now" 
  log_echo "================================================================================"

}

log_elapsed_time () 
{
  now=`date +%s`
  elap_secs=`echo ${now}-${START_TIME} | bc`
  elap_hrs=`echo $elap_secs/3600 | bc`
  elap_mins=`echo \(${elap_secs}-3600*${elap_hrs}\)/60 | bc`
  remain=`echo \(${elap_secs}-3600*${elap_hrs}-${elap_mins}*60\) | bc`
  log_echo 
  log_echo "ELAPSED TIME (h:m:s) ${elap_hrs}:${elap_mins}:${remain}"
  log_echo

}

log_footer ()
{
  log_echo "================================================================================" 
  log_echo "${0} complete"
  log_elapsed_time
  log_echo "================================================================================"
}


exit_now ()
{
  if [[ $# -eq "1" ]] ; then
    log_echo "EXIT NOW: $1"
  else
    log_echo "ERROR: '${command}' failed ($status)"
  fi
  log_elapsed_time
  exit 1
}

do_it ()
{
  log_echo 
  log_echo "----> $command" 
  status_file=".pipestatus.$$"
  { $command 2>&1 ; echo $?>${status_file} ; } | tee -a ${LOG_FILE}
  status=`cat ${status_file}`
  rm ${status_file}
  if [ "${status}" -ne 0 ] ; then
    exit_now
  fi
  log_echo 
  command=
}

dump_env ()
{
  command="printenv"
  do_it
}

dump_variable ()
{
  a=$1
  log_echo "" 
  log_echo "----> $a=${!a}"
  log_echo "" 
}

set_env_variable ()
{
  log_echo "----> export $1=$2" 
  export $1=$2

}

change_directory ()
{
  if [[ $# -ne "1" ]] ; then
    exit_now "Invalid arguments to ${FUNCNAME}"
  fi
  if [[ ! -e "$1" ]] ; then
    exit_now "Can not change to directory ${1}, does not exist"
  fi
  log_echo "Change to directory $1"
  cd "$1" || ( command="cd ${1}" ; exit_now ; )
}  



# --- Begin here

# Log file header
log_header

# User name
USER=`whoami`

# Define the script directory
SCRIPT_DIRECTORY="$( cd "$( dirname "$0" )" && pwd )"

# Define the repository location and branch
CASL_REPO_MACHINE=ccscs8
CASL_REPO_DIRECTORY=/ccs/codes/casl/hydra
CASL_REPO_BRANCH=trunk

# Define local export directory
CASL_EXPORT_DIRECTORY=hydra_snapshot

# --- Checkout the code

# Define the svn executable
command='which svn'
do_it
SVN=svn

# Export the repo
command="${SVN} export svn+ssh://${USER}@${CASL_REPO_MACHINE}/${CASL_REPO_DIRECTORY}/${CASL_REPO_BRANCH} ${CASL_EXPORT_DIRECTORY}"
do_it

# --- Pack it up

# Everything but tests
HYDRA_NOTESTS_TARFILE=hydra_snapshot_sanstest.tar.gz
command="tar zcf ${HYDRA_NOTESTS_TARFILE} --exclude=test ${CASL_EXPORT_DIRECTORY}"
do_it

# The test directory
HYDRA_TESTS_TARFILE=hydra_snapshot_test.tar.gz
command="tar zcf ${HYDRA_TESTS_TARFILE} ${CASL_EXPORT_DIRECTORY}/test"
do_it

# --- Move tarfiles out to ccs-green
CASL_WEB_MACHINE=ccs-green
CASL_WEB_DIRECTORY=/var/www/virtual/get-hydra.lanl.gov/html/downloads/hydra

# The no test tarfile
command="scp ${HYDRA_NOTESTS_TARFILE} ${CASL_WEB_MACHINE}://${CASL_WEB_DIRECTORY}/."
do_it

command="ssh ${CASL_WEB_MACHINE} chmod 0664 ${CASL_WEB_DIRECTORY}/${HYDRA_NOTESTS_TARFILE}"
do_it

# And the test tarfile
command="scp ${HYDRA_TESTS_TARFILE} ${CASL_WEB_MACHINE}://${CASL_WEB_DIRECTORY}/."
do_it

command="ssh ${CASL_WEB_MACHINE} chmod 0664 ${CASL_WEB_DIRECTORY}/${HYDRA_TESTS_TARFILE}"
do_it


# --- Clean up
command="rm -rf ${CASL_EXPORT_DIRECTORY}"
do_it

command="rm ${HYDRA_TESTS_TARFILE}"
do_it

command="rm ${HYDRA_NOTESTS_TARFILE}"
do_it

# --- Finalize the script
log_footer
