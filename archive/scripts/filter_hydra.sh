#!/bin/bash -u
#
# Hydra -> Hydra-TH filter script
#
# This script pulls in changes from the main Hydra repositories to a working
# directory (hydra.trans) and after filtering everything that's not part of
# Hydra-TH, pushes them to the hydra-th (bare) repository on prandtl and to the
# hydra-th (bare) repository on ccs-green.
#
# This is accomplished by the following steps:
#   1.  Checkout the latest updates from the master Hydra repositories
#   2.  Remove everything that is not part of Hydra-TH
#   3.  Rsync the updated directory onto a working clone of a bare Hydra-TH repo
#   4.  Commit all changes in the working clone
#   5.  Push new commit to bare Hydra-TH repository on prandtl
#   6.  Push new commit to bare Hydra-TH repository on ccs-green
#
# The repositories/directories involved are:
# (1) hydra:          ccs-green:/var/www/virtual/hydra.lanl.gov/repos - master
# (2) hydra.trans:    prandtl:/mnt/scratch2/filter/hydra.trans    - for filtering
# (4) hydra-th.clone: prandtl:/mnt/scratch2/filter/hydra-th.clone - hydra-th, working
# (5) hydra-th:       prandtl:/mnt/scratch2/filter/hydra-th       - hydra-th, bare
# (6) hydra-th:       ccs-green:/var/hydra/hydra-th         - hydra-th, bare
#
# Arguments to bash:
# -u: will exit if we try to use an uninitialised variable
#
# This script runs as a cron-job on prandtl and sends emails on error, see EMAIL
# variable below.
#

# set location variables
HYDRA_TRANS=/mnt/scratch2/filter/hydra.trans
HYDRA_TH_CLONE=/mnt/scratch2/filter/hydra-th.clone

# Email these folks in case of error
EMAIL="jbakosi@lanl.gov christon@lanl.gov berndt@lanl.gov"

# Generic part of the error message
ERR="An error occurred while executing the script '$0'\non $HOSTNAME, command and output below:"

# Simple error handler: send email
function error () {
  echo -e "$ERR \n\n\$ $1" | bsd-mailx -s "filter_hydra.sh error" $EMAIL
  exit 1
}

# git executable
GIT=git

# Set umask rwx groups, --- world
umask u=rwx,g=rwx,o=

# Change directory to a working directory (hydra.trans), a staging directory
# for filtering hydra -> hydra-th
cd $HYDRA_TRANS
[[ "$?" != "0" ]] && error "cd $HYDRA_TRANS";

echo "Automated Hydra -> Hydra-TH filter commit" > /tmp/filter_log
echo >> /tmp/filter_log
echo >> /tmp/filter_log

# Pull changes from master hydra repositories
# and record the current hash for each
OUT=`git pull 2>&1`
[[ "$?" != "0" ]] && error "git pull (code)\n$OUT";
echo "---" >> /tmp/filter_log
echo "origin: https://hydra.lanl.gov/repos/code" >> /tmp/filter_log
echo "destination: ." >> /tmp/filter_log
echo >> /tmp/filter_log
git log --pretty=format:'%h "%s"%nAuthor: %an <%ae>%nDate:   %ad (%cr)%n' -1 HEAD >> /tmp/filter_log
echo "---" >> /tmp/filter_log
echo >> /tmp/filter_log
echo >> /tmp/filter_log

OUT=`cd examples; git pull; cd .. 2>&1`
[[ "$?" != "0" ]] && error "git pull (examples)\n$OUT";
cd examples
echo "---" >> /tmp/filter_log
echo "origin: https://hydra.lanl.gov/repos/examples" >> /tmp/filter_log
echo "destination: ./examples" >> /tmp/filter_log
echo >> /tmp/filter_log
git log --pretty=format:'%h "%s"%nAuthor: %an <%ae>%nDate:   %ad (%cr)%n' -1 HEAD >> /tmp/filter_log
echo "---" >> /tmp/filter_log
echo >> /tmp/filter_log
echo >> /tmp/filter_log
cd ..

OUT=`cd tpl; git pull; cd .. 2>&1`
[[ "$?" != "0" ]] && error "git pull (tpl)\n$OUT";
cd tpl
echo "---" >> /tmp/filter_log
echo "origin: https://hydra.lanl.gov/repos/tpl" >> /tmp/filter_log
echo "destination: ./tpl" >> /tmp/filter_log
echo >> /tmp/filter_log
git log --pretty=format:'%h "%s"%nAuthor: %an <%ae>%nDate:   %ad (%cr)%n' -1 HEAD >> /tmp/filter_log
echo "---" >> /tmp/filter_log
echo >> /tmp/filter_log
echo >> /tmp/filter_log
cd ..


OUT=`cd test/regression; git pull; cd ../.. 2>&1`
[[ "$?" != "0" ]] && error "git pull (regression)\n$OUT";
cd test/regression
echo "---" >> /tmp/filter_log
echo "origin: https://hydra.lanl.gov/repos/regression" >> /tmp/filter_log
echo "destination: ./test/regression" >> /tmp/filter_log
echo >> /tmp/filter_log
git log --pretty=format:'%h "%s"%nAuthor: %an <%ae>%nDate:   %ad (%cr)%n' -1 HEAD >> /tmp/filter_log
echo "---" >> /tmp/filter_log
echo >> /tmp/filter_log
echo >> /tmp/filter_log
cd ../..


OUT=`cd test/long_regression; git pull; cd ../.. 2>&1`
[[ "$?" != "0" ]] && error "git pull (long_regression)\n$OUT";
cd test/long_regression
echo "---" >> /tmp/filter_log
echo "origin: https://hydra.lanl.gov/repos/long_regression" >> /tmp/filter_log
echo "destination: ./test/long_regression" >> /tmp/filter_log
echo >> /tmp/filter_log
git log --pretty=format:'%h "%s"%nAuthor: %an <%ae>%nDate:   %ad (%cr)%n' -1 HEAD >> /tmp/filter_log
echo "---" >> /tmp/filter_log
echo >> /tmp/filter_log
echo >> /tmp/filter_log
cd ../..


OUT=`cd test/verification; git pull; cd ../.. 2>&1`
[[ "$?" != "0" ]] && error "git pull (verification)\n$OUT";
cd test/verification
echo "---" >> /tmp/filter_log
echo "origin: https://hydra.lanl.gov/repos/verification" >> /tmp/filter_log
echo "destination: ./test/verification" >> /tmp/filter_log
echo >> /tmp/filter_log
git log --pretty=format:'%h "%s"%nAuthor: %an <%ae>%nDate:   %ad (%cr)%n' -1 HEAD >> /tmp/filter_log
echo "---" >> /tmp/filter_log
echo >> /tmp/filter_log
echo >> /tmp/filter_log
cd ../..


# Delete everything that's not part of hydra-th: this is the filtering step,
# add/remove everything as future-changes necessary
OUT=`rm -rf HYDRA_LICENSE \
            src/FEM \
            src/FVM/CCAdvection \
            src/FVM/CCConduction \
            src/FVM/CCDummy \
            src/FVM/CCBurgers \
            src/FVM/CCEuler \
            src/FVM/CCFront \
            src/FVM/CCLagrangian \
            src/FVM/CCLset \
            test/regression/FEM \
            test/regression/FVM/CCAdvection \
            test/regression/FVM/CCConduction \
            test/regression/FVM/CCEuler \
            test/regression/FVM/CCLagrangian \
            tpl/tarballs/Chaco* 2>&1`
[[ "$?" != "0" ]] && error "rm -rf ... \n$OUT";

# Copy over everyting (only the changes, including deletes) to a clone of the
# hydra-th (bare) repository
#  * Bare: $HYDRA_TH (this gets discributed to ccs-green and
#  eventually to VERA at ORNL)
#  * Clone of bare: $HYDRA_TH_CLONE (where updates happen)
OUT=`rsync -av --delete --exclude=.git/ --exclude=.gitignore $HYDRA_TRANS/ $HYDRA_TH_CLONE 2>&1`
[[ "$?" != "0" ]] && error "rsync ...\n$OUT";

# Change directory to working hydra-th clone
cd $HYDRA_TH_CLONE
[[ "$?" != "0" ]] && error "cd $HYDRA_TH_CLONE";

# Commit all changes to clone (if any) and push
if [ -z "`$GIT status --porcelain`" ];
then
  echo "no changes" > /dev/null
else
  # Add new files git does not yet know about
  OUT=`$GIT add . 2>&1`
  [[ "$?" != "0" ]] && error "$GIT add .\n$OUT";

  # Commit changes
  OUT=`$GIT commit -a -F/tmp/filter_log 2>&1`
  [[ "$?" != "0" ]] && error "$GIT commit ...\n$OUT";

  # Push new commit to bare hydra-th on prandtl
  OUT=`$GIT push 2>&1`
  [[ "$?" != "0" ]] && error "$GIT push\n$OUT";

  # Push new commit to bare hydra-th on ccs-green
  OUT=`$GIT push green 2>&1`
  [[ "$?" != "0" ]] && error "$GIT push green\n$OUT";
fi
