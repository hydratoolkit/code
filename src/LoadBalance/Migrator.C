//******************************************************************************
//! \file    src/LoadBalance/Migrator.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 17:20:12 2011
//! \brief   Virtual base class for derived node, element, set migrators
//******************************************************************************
#include <stdio.h>
#include <cstdlib>
#include <mpi.h>

#include <set>

using namespace std;

#include <HydraTypes.h>
#include <Migrator.h>
#include <UnsMesh.h>
#include <MPIWrapper.h>

using namespace Hydra;

Migrator::Migrator(UnsMesh& mesh):
  m_max_send(0),
  m_max_recv(0),
  SEND_BUF(UNREGISTERED),
  RECV_BUF(UNREGISTERED),
  SENDID_LIST(UNREGISTERED),
  m_mesh(mesh)
/*******************************************************************************
Routine: Migrator - constructor
Author : Mark A. Christon
*******************************************************************************/
{}

Migrator::~Migrator()
/*******************************************************************************
Routine: ~Migrator - destructor
Author : Mark A. Christon
*******************************************************************************/
{}

void
Migrator::finalize()
/*******************************************************************************
Routine: finalize - free all registered data
Author : Mark A. Christon
*******************************************************************************/
{
  if (m_max_send > 0) {
    m_mesh.freeVariable(SEND_BUF);
    m_mesh.freeVariable(SENDID_LIST);
  }
  if (m_max_recv > 0) m_mesh.freeVariable(RECV_BUF);
}

char*
Migrator::getRecvBuffer()
/*******************************************************************************
Routine: getRecvBuffer - return the RECV buffer for basic receive
Author : Mark A. Christon
*******************************************************************************/
{
  return m_mesh.getVariable<char>(RECV_BUF);
}

void
Migrator::migrate(int* send_list, int* recv_list, int* send_offset,
                  DataIndex OLD_VAR, DataIndex NEW_VAR)
/*******************************************************************************
Routine: migrate - generalized migration function for sets and fields
Author : Mark A. Christon
*******************************************************************************/
{
  // Migrate data in send-receive pairs for each processor

  // Pack the data for sending
  CommRequest* reqs =
    static_cast<CommRequest*>(m_mesh.allocTempData(Nproc*sizeof(CommRequest)));
  CommStatus* status =
    static_cast<CommStatus*>(m_mesh.allocTempData(sizeof(CommStatus)));

  if( m_max_send > 0) {
    char* send_buf = pack(OLD_VAR, send_list, send_offset);
    for (int iproc=0; iproc<Nproc; iproc++) {
      if (pid != iproc && send_list[iproc] > 0) {
        int mtype = pid+1;
        int nsend = send_list[iproc];
        int size  = getDataSize();
        int offset = send_offset[iproc]*size;
        char* send_tmp = &send_buf[offset];
        reqs[iproc] = g_comm->nonblockingSend(mtype, send_tmp, nsend*size, iproc);
      }
    }
  }

  if(m_max_recv > 0) {
    for (int iproc=0; iproc<Nproc; iproc++) {
      if (pid != iproc && recv_list[iproc] > 0) {
        int mtype = iproc+1;
        memset(status, 0, sizeof(CommStatus));
        // waiting matched message arrives
        g_comm->blockingProbe(iproc, mtype, status);
        int size  = getDataSize();
        int nrecv = recv_list[iproc];
        char* recv_buf = getRecvBuffer();
        g_comm->receive(recv_buf, nrecv*size, iproc, mtype);
        unpack(nrecv, NEW_VAR);
      }
    } // End of loop over processor receive lists
  }

  // waiting for nonblocking sending to complete
  for (int iproc=0; iproc<Nproc; iproc++) {
    if (pid != iproc && send_list[iproc] > 0) {
      g_comm->wait(&reqs[iproc]);
    }
  }
  m_mesh.freeTempData(status);
  m_mesh.freeTempData(reqs);
}
