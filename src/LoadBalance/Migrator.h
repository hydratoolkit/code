//******************************************************************************
//! \file    src/LoadBalance/Migrator.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 17:20:12 2011
//! \brief   Abstract base class for derived node, element, set migrators
//******************************************************************************
#ifndef Migrator_h
#define Migrator_h

#include <iostream>
#include <string>
#include <vector>
#include <set>

#include <HydraTypes.h>
#include <DataContainer.h>
#include <DataShapes.h>

namespace Hydra {

class UnsMesh;

//! Abstract base class for derived migrators
class Migrator {

  public:

    //! \name Constructor/Destructor
    //@{
             Migrator(UnsMesh& mesh);
    virtual ~Migrator();
    //@}

    virtual void compress(set<int>& unique_memb,
                          DataIndex OLD_VAR,
                          DataIndex NEW_VAR) = 0;

    virtual void finalize();

    virtual void initialize(int* elem_pid, int* send_list, int* recv_list) = 0;

    void migrate(int* send_list,
                 int* recv_list,
                 int* send_offset,
                 DataIndex OLD_VAR,
                 DataIndex NEW_VAR);

    virtual void setupCompress(int* elem_pid, set<int>& unique_memb) = 0;

    virtual void setupPack(int* send_list, int* send_offset, int* elem_pid) = 0;

    int getMaxSend() {return m_max_send;}

    int getMaxRecv() {return m_max_recv;}

    DataIndex getSendIdIndex() {return SENDID_LIST;}


  protected:

    //! Compute the distribution of objects across all processors
    virtual void calcDist(int* elem_pid, int* node_send, int* node_recv) = 0;

    virtual char* pack(DataIndex OLD_VAR, int* send_list, int* send_offset) = 0;

    virtual void unpack(int nrecv, DataIndex NEW_VAR) = 0;

    virtual int getDataSize() = 0;

    char* getRecvBuffer();

    int Nproc;
    int pid;

    int Nel;

    int m_max_send;
    int m_max_recv;

    DataIndex SEND_BUF;
    DataIndex RECV_BUF;
    DataIndex SENDID_LIST;

    //! Primary object used by the migrator
    UnsMesh& m_mesh;

  private:

    //! Don't permit copy or assignment operators
    //@{
    Migrator(const Migrator&);
    Migrator& operator=(const Migrator&);
    //@}

};

}

#endif // Migrator.h
