//******************************************************************************
//! \file    src/LoadBalance/FieldMigrator.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 17:20:12 2011
//! \brief   Abstract base class for derived node, element, set migrators
//******************************************************************************
#ifndef FieldMigrator_h
#define FieldMigrator_h

#include <set>

#include <HydraTypes.h>

namespace Hydra {

class UnsMesh;


//! Abstract base class for derived migrators
class FieldMigrator {

  public:

    //! \name Constructor/Destructor
    //@{
             FieldMigrator(UnsMesh& mesh);
    virtual ~FieldMigrator();
    //@}

    //! Finalize the field migration
    void finalize();

    //! Get the number of the fields
    int  getNumSetFd() {return m_nsetfd;}

    //! Migrate all fields
    virtual void migrateFields(int* send_list, int* recv_list,
                               int* send_offset, DataIndex SENDID_LIST);

  protected:

    //! Compress the node/element stay on processor
    void compress(DataIndex OLD_FIELD, DataIndex NEW_FIELD);

    //! Initialize the field variable migration
    void initialize();

    //! Migrate the field variables
    void migrate(int* send_list, int* recv_list,
                 int* send_offset, DataIndex SENDID_LIST,
                 DataIndex OLD_FIELD, DataIndex NEW_FIELD);

    //! Pack the field variables to be sent
    virtual char* pack(DataIndex OLD_FIELD, DataIndex SENDID_LIST,
                       int* send_list, int* send_offset);

    //! Unpack the field variables after receiving
    void unpack(int nrecv, DataIndex NEW_FIELD);

    //! Data size in sending and receiving
    int getDataSize() {return sizeof(Real);}

    //! Get the receiving buffer
    char* getRecvBuffer();

    int Nproc;
    int pid;

    int m_max_send;
    int m_max_recv;

    DataIndex SEND_BUF;
    DataIndex RECV_BUF;
    DataIndex FIELD_LIST;
    DataIndex STAY_LIST;

    //! Primary object used by the migrator
    UnsMesh& m_mesh;

    //! Initial number of field variables in the set
    int m_nfd;

    //! New number of field variables after migration
    int m_nfd_new;

    //! Field variable counter for migration
    int m_cur_fd;

    //! field variables counter that stay on processor
    int m_nfd_stay;

    //! number of fields for particular set
    int m_nsetfd;

  private:

    //! Don't permit copy or assignment operators
    //@{
    FieldMigrator(const FieldMigrator&);
    FieldMigrator& operator=(const FieldMigrator&);
    //@}
};

}

#endif // FieldMigrator.h
