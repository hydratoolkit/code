//******************************************************************************
//! \file    src/LoadBalance/NodeSetMigrator.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 17:20:12 2011
//! \brief   Migrator for node sets
//******************************************************************************
#ifndef NodeSetMigrator_h
#define NodeSetMigrator_h

/*! \name NodeSetMigrator.h
    \brief
*/

#include <iostream>
#include <map>
#include <set>

#include <Migrator.h>

namespace Hydra {


//! NodeSet Migratory
class NodeSetMigrator: public Migrator {

  public:

    //! \name Constructor/Destructor
    //@{
             NodeSetMigrator(UnsMesh& mesh,
                             map<int,int>& node2local,
                             map<int,int>& new_node2local);
    virtual ~NodeSetMigrator();
    //@}

    virtual void initialize(int* elem_pid, int* send_list, int* recv_list);

    void initialize(int* elem_pid, int* send_list, int* recv_list,
                    int* sendall_list);

    virtual void finalize();

    virtual void compress(set<int>& unique_memb,
                          DataIndex OLD_VAR, DataIndex NEW_VAR);

    virtual void setupCompress(int* elem_pid, set<int>& unique_memb);

    void setupCompress(int* elem_pid, int* sendall_list, set<int>& unique_memb);

    virtual void setupPack(int* send_list, int* send_offset, int* elem_pid);

    void setupPack(int* send_list, int* send_offset,
                   int* sendall_list, int* sendallid_offset, int* elem_pid);

    int* setupAllPack(int* elem_pid, int* sendall_list, int* sendallid_offset);

    void setId(int id) {m_id = id;}

    int getNewCount() {return m_nnp_new;}


  protected:

    //! Compute the distribution of objects across all processors
    virtual void calcDist(int* elem_pid, int* node_send, int* node_recv);

    void calcDist(int* elem_pid, int* node_send, int* node_recv,
                  int* sendall_list);

    virtual char* pack(DataIndex OLD_VAR, int* send_list, int* send_offset);

    virtual void unpack(int nrecv, DataIndex NEW_VAR);

    int getDataSize() {return sizeof(int);}

  private:

    //! Don't permit copy or assignment operators
    //@{
    NodeSetMigrator(const NodeSetMigrator&);
    NodeSetMigrator& operator=(const NodeSetMigrator&);
    //@}

    //! Total number of node set
    int m_Nndsets;

    //! Set Id being operated on
    int m_id;

    //! Initial number of nodes in the set
    int m_nnp;

    //! New number of nodes after migration
    int m_nnp_new;

    //! Node counter for migration
    int m_cur_nd;

    //! Global-to-local maps
    map<int,int>& m_node2local;
    map<int,int>& m_new_node2local;

};

}

#endif // NodeMigrator_h
