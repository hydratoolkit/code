//******************************************************************************
//! \file    src/LoadBalance/NodeFieldMigrator.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 17:20:12 2011
//! \brief   Migrator for node field information
//******************************************************************************
#ifndef NodeFieldMigrator_h
#define NodeFieldMigrator_h

#include <map>
#include <set>

#include <FieldMigrator.h>

namespace Hydra {


//! Node Field Migrator
class NodeFieldMigrator: public FieldMigrator
{

  public:

    //! \name Constructor/Destructor
    //@{
             NodeFieldMigrator(UnsMesh& mesh);
    virtual ~NodeFieldMigrator();
    //@}

    //! Finalize the field migration for node sets
    void finalize(int setId);

    //! Pack and map the field variables to be sent
    virtual char* pack(DataIndex OLD_FIELD, DataIndex SENDID_LIST,
                       int* send_list, int* send_offset);

    //! Setup the list of nodes on processor
    void setupCompress(set<int>& uniqueNodes, DataIndex OLD_VAR);

    //! Setup the list of field variables with the same node set ID
    void setupNodeSetField(int setId, int nfdOld, int nfdNew,
                           int numsend, int numrecv);

  private:

    //! Don't permit copy or assignment operators
    //@{
    NodeFieldMigrator(const NodeFieldMigrator&);
    NodeFieldMigrator& operator=(const NodeFieldMigrator&);
    //@}

    //! Mapping for ordinals
    map<int,int> m_nsetMap;
};

}

#endif // NodeFieldMigrator.h
