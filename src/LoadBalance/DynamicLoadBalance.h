//******************************************************************************
//! \file    src/LoadBalance/DynamicLoadBalance.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 17:20:11 2011
//! \brief   Dynamic Load Balancer
//******************************************************************************
#ifndef DynamicLoadBalance_h
#define DynamicLoadBalance_h

#include <StaticLoadBalance.h>

namespace Hydra {

class UnsMesh;
class Physics;
class Timer;
class Element;
class Control;

//! Dynamic Load Balancer
class DynamicLoadBalance : public StaticLoadBalance {

  public:

    // Ultimately we will need more general constructor.
    // This will require the reader classes to have a general
    // constructor as well... at least for now.  As we go forward,
    // The load-balancing step can be moved out of the reader.

    //! \name Constructor/Destructor
    //@{
             DynamicLoadBalance(UnsMesh&    mesh,
                                Physics& physics,
                                Control& control,
                                Timer&   timer   );
    virtual ~DynamicLoadBalance();
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    DynamicLoadBalance(const DynamicLoadBalance&);
    DynamicLoadBalance& operator=(const DynamicLoadBalance&);
    //@}

    //! \name Virtual interface for load-balancing
    //@{
    virtual void initialize();

    virtual void registerVariables();
    //@}

    // \name Primary objects for load balancing
    //@{
    //! The currently selected physics
    Physics& m_physics;
    //@}

};

}

#endif // LoadBalance_h
