//******************************************************************************
//! \file    src/LoadBalance/SideSetFieldMigrator.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 17:20:12 2011
//! \brief   SideSet field migrator
//******************************************************************************
#ifndef SideSetFieldMigrator_h
#define SideSetFieldMigrator_h

#include <ElementFieldMigrator.h>

namespace Hydra {

//! SideSet Field Migrator
class SideSetFieldMigrator: public ElementFieldMigrator
{

  public:

    //! \name Constructor/Destructor
    //@{
             SideSetFieldMigrator(UnsMesh& mesh);
    virtual ~SideSetFieldMigrator();
    //@}

    //! Setup the list of field variables with the same surface set ID
    void setupSurfSetField(int setId, int nfdOld, int nfdNew,
                           int numsend, int numrecv);

  private:

    //! Don't permit copy or assignment operators
    //@{
    SideSetFieldMigrator(const SideSetFieldMigrator&);
    SideSetFieldMigrator& operator=(const SideSetFieldMigrator&);
    //@}
};

}

#endif // SideSetFieldMigrator.h
