//******************************************************************************
//! \file    src/LoadBalance/ElementSetMigrator.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 17:20:11 2011
//! \brief   Migrator for element sets
//******************************************************************************
#ifndef ElementSetMigrator_h
#define ElementSetMigrator_h

#include <iostream>
#include <map>
#include <set>

#include <Migrator.h>

namespace Hydra {

//! Element Set Migrator
class ElementSetMigrator: public Migrator {

  public:

    //! \name Constructor/Destructor
    //@{
             ElementSetMigrator(UnsMesh& mesh, map<int,int>& elem2local);
    virtual ~ElementSetMigrator();
    //@}

    virtual void initialize(int* elem_pid, int* send_list, int* recv_list);

    virtual void compress(set<int>& unique_memb,
                          DataIndex OLD_VAR, DataIndex NEW_VAR);

    virtual void setupCompress(int* elem_pid, set<int>& unique_memb);

    virtual void setupPack(int* send_list, int* send_offset, int* elem_pid);

    void setId(int id) {m_id = id;}

    int getNewCount() {return m_nel_new;}

  protected:

    //! Compute the distribution of objects across all processors
    virtual void calcDist(int* elem_pid, int* node_send, int* node_recv);

    virtual char* pack(DataIndex OLD_VAR, int* send_list, int* send_offset);

    virtual void unpack(int nrecv, DataIndex NEW_VAR);

    virtual int getDataSize() {return sizeof(int);}

  private:

    //! Don't permit copy or assignment operators
    //@{
    ElementSetMigrator(const ElementSetMigrator&);
    ElementSetMigrator& operator=(const ElementSetMigrator&);
    //@}

    //! Set Id being operated on
    int m_id;

    //! Element counter for migration
    int m_cur_el;

    //! Initial number of elements in the set
    int m_nel;

    //! New number of elements after migration
    int m_nel_new;

    //! Global-to-local maps
    map<int,int>& m_elem2local;
};

}

#endif // ElementSetMigrator.h
