//******************************************************************************
//! \file    src/LoadBalance/ElementVarMigrator.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 17:20:11 2011
//! \brief   Migrator for element variables
//******************************************************************************
#ifndef ElementVarMigrator_h
#define ElementVarMigrator_h

#include <set>
#include <vector>

#include <Migrator.h>

namespace Hydra {

//! Element Variable Migrator
class ElementVarMigrator: public Migrator {

  public:

    //! \name Constructor/Destructor
    //@{
             ElementVarMigrator(UnsMesh& mesh);
    virtual ~ElementVarMigrator();
    //@}

    void migrate(int* send_list, int* recv_list,
                 int* send_offset, DataIndex OLD_VAR, DataIndex NEW_VAR);

    virtual void initialize(int* elem_pid, int* send_list, int* recv_list);

    virtual void compress(set<int>& unique_memb,
                          DataIndex OLD_VAR, DataIndex NEW_VAR);

    virtual void setupCompress(int* elem_pid, set<int>& unique_memb);

    virtual void setupPack(int* send_list, int* send_offset, int* elem_pid);

    //! setup maximum dimensions
    void setupMaxDimension(vector<DataIndex>& elem_var);

    //! setup stride
    void setupStride(int nDim, int strideOld, int strideNew);

  protected:

    //! Compute the distribution of objects across all processors
    virtual void calcDist(int* elem_pid, int* node_send, int* node_recv);

    virtual char* pack(DataIndex OLD_VAR, int* send_list, int* send_offset);

    virtual void unpack(int nrecv, DataIndex NEW_VAR);

    virtual int getDataSize() {return sizeof(Real);}

  private:

    //! Don't permit copy or assignment operators
    //@{
    ElementVarMigrator(const ElementVarMigrator&);
    ElementVarMigrator& operator=(const ElementVarMigrator&);
    //@}

    //! Element counter for migration
    int m_cur_el;

    //! New number of elements after migration
    int m_nel_new;

    //! maximum dimensions
    int m_max_Dim;
    int m_nDim;
    int m_strideOld;
    int m_strideNew;

};

}

#endif // ElemMigrator.h
