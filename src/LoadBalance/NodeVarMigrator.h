//******************************************************************************
//! \file    src/LoadBalance/NodeVarMigrator.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 17:20:12 2011
//! \brief   Migrator for nodal variables
//******************************************************************************
#ifndef NodeVarMigrator_h
#define NodeVarMigrator_h

#include <ostream>
#include <vector>
#include <map>
#include <set>

#include <Migrator.h>

namespace Hydra {

//! Structure for packing/unpacking messages
struct NodeVar {
  int  ordinal_id;
  Real value;
};

//! Node Variable Migrator
class NodeVarMigrator : public Migrator {

  public:

    //! \name Constructor/Destructor
    //@{
             NodeVarMigrator(UnsMesh& mesh, map<int,int>& node2local,
                             map<int,int>& new_node2local);
    virtual ~NodeVarMigrator();
    //@}

    void migrate(int* send_list, int* recv_list,
                 int* send_offset, DataIndex OLD_VAR, DataIndex NEW_VAR);

    void migrateOrdinalID(int* send_list, int* recv_list);

    void finalize();

    virtual void initialize(int* elem_pid,
                            int* send_list, int* recv_list);

    virtual void compress(set<int>& unique_memb,
                          DataIndex OLD_VAR, DataIndex NEW_VAR);

    virtual void setupCompress(int* elem_pid, set<int>& unique_memb);

    virtual void setupPack(int* send_list, int* send_offset, int* elem_pid);

    //! setup maximum dimensions
    void setupMaxDimension(vector<DataIndex>& elem_var);

    //! setup stride
    void setupStride(int nDim, int strideOld, int strideNew);


  protected:

    //! Compute the distribution of objects across all processors
    virtual void calcDist(int* elem_pid, int* node_send, int* node_recv);

    virtual char* pack(DataIndex OLD_VAR, int* send_list, int* send_offset);

    virtual void unpack(int nrecv, DataIndex NEW_VAR);

    void unpack(int nrecv, int offset, DataIndex NEW_VAR);

    void recvOrdinalId(int offset, int nrecv);

    virtual int getDataSize() {return sizeof(Real);}

    DataIndex RECVID_LIST;
    DataIndex RECV_OFFSET;

  private:

    //! Don't permit copy or assignment operators
    //@{
    NodeVarMigrator(const NodeVarMigrator&);
    NodeVarMigrator& operator=(const NodeVarMigrator&);
    //@}

    //! Global-to-local maps
    map<int,int>& m_node2local;
    map<int,int>& m_new_node2local;

    //! maximum dimensions
    int m_max_Dim;

    int m_nDim;
    int m_strideOld;
    int m_strideNew;

};

}

#endif // NodeMigrator.h
