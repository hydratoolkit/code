//******************************************************************************
//! \file    src/LoadBalance/SideSetMigrator.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 17:20:12 2011
//! \brief   Migrator for surface (side) sets
//******************************************************************************
#ifndef SideSetMigrator_h
#define SideSetMigrator_h

#include <iostream>
#include <map>
#include <set>

#include <Migrator.h>

namespace Hydra {

//! Structure for packing/unpacking messages
struct Surf {
    int elem;
    int side;
};

//! SideSet Migrator
class SideSetMigrator: public Migrator {

  public:

    //! \name Constructor/Destructor
    //@{
             SideSetMigrator(UnsMesh& mesh, map<int,int>& elem2local);
    virtual ~SideSetMigrator();
    //@}

    void migrate(int* send_list, int* recv_list, int* send_offset,
                 DataIndex OLD_EL_LIST, DataIndex OLD_SD_LIST,
                 DataIndex NEW_EL_LIST, DataIndex NEW_SD_LIST);

    virtual void initialize(int* elem_pid, int* send_list, int* recv_list);

    virtual void compress(set<int>& unique_memb,
                          DataIndex OLD_VAR, DataIndex NEW_VAR);

    virtual void setupCompress(int* elem_pid, set<int>& unique_memb);

    virtual void setupPack(int* send_list, int* send_offset, int* elem_pid);

    void compress(set<int>& unique_memb,
                  DataIndex OLD_EL_LIST, DataIndex NEW_EL_LIST,
                  DataIndex OLD_SD_LIST, DataIndex NEW_SD_LIST);

    void setId(int id) {m_id = id;}

    int getNewCount() {return m_nel_new;}

  protected:

    //! Compute the distribution of objects across all processors
    virtual void calcDist(int* elem_pid, int* node_send, int* node_recv);

    virtual char* pack(DataIndex OLD_VAR, int* send_list, int* send_offset);

    char* pack(DataIndex OLD_EL_LIST, DataIndex OLD_SD_LIST,
               int* elem_pid, int* send_offset);

    virtual void unpack(int nrecv, DataIndex NEW_VAR);

    void unpack(int nrecv, DataIndex NEW_EL_LIST, DataIndex NEW_SD_LIST);

    virtual int getDataSize() {return sizeof(Surf);}

  private:

    //! Don't permit copy or assignment operators
    //@{
    SideSetMigrator(const SideSetMigrator&);
    SideSetMigrator& operator=(const SideSetMigrator&);
    //@}

    //! Set Id being operated on
    int m_id;

    //! Element counter for migration
    int m_cur_el;

    //! Initial number of elements in the set
    int m_nel;

    //! New number of elements after migration
    int m_nel_new;

    //! Global-to-local maps
    map<int,int>& m_elem2local;

};

}

#endif // SideSetMigrator.h
