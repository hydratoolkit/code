//******************************************************************************
//! \file    src/LoadBalance/DynamicLoadBalance.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 17:20:12 2011
//! \brief Dynamic load balancer
//******************************************************************************
#include <cassert>
#include <stdio.h>
#include <cstdlib>

#include <map>
#include <set>

using namespace std;

#include <Tri3.h>
#include <Tet4.h>
#include <Quad4.h>
#include <Hex8.h>
#include <DynamicLoadBalance.h>
#include <Control.h>
#include <OutputControl.h>
#include <fileIO.h>
#include <UnsMesh.h>
#include <Timer.h>
#include <MPIWrapper.h>
#include <Physics.h>

using namespace Hydra;


DynamicLoadBalance::DynamicLoadBalance(UnsMesh&    mesh,
                                       Physics& physics,
                                       Control& control,
                                       Timer&   timer    ) :
  StaticLoadBalance(mesh, control, timer),
  m_physics(physics)
/*******************************************************************************
Routine: DynamicLoadBalance - constructor
Author : Mark A. Christon
*******************************************************************************/
{}

DynamicLoadBalance::~DynamicLoadBalance()
/*******************************************************************************
Routine: ~DynamicLoadBalance - destructor
Author : Mark A. Christon
*******************************************************************************/
{}

void
DynamicLoadBalance::initialize()
/*******************************************************************************
Routine: initialize - initialize the load balance object
Author : Mark A. Christon
*******************************************************************************/
{
  // Setup basic data used in the class -- number of nodes and elements
  // is for the current distribution of elements across processors
  pid   = g_comm->getPid();
  Nproc = g_comm->getNproc();
  Ndim  = m_mesh.getNdim();
  Nel   = m_mesh.getNel();
  Nnp   = m_mesh.getNnp();

#ifdef USE_PARMETIS
  // Setup defaults for ParMetis
  // Use 0-based numbering
  numflag = 0;
  // int ncommonnodes = Ndim;
  // This gives somewhat smoother cuts
  ncommonnodes = 1;
  wgtflag = 0;
  ncon    = 1;
  edgecut = 0;
  // ubvec      = 1.05;
  ubvec      = 1.01;
  itr        = 1000.0;

  // Set the basic controls for ParMETIS
  options[0] = 0;
  options[1] = 1;
  options[2] = 15;
  options[3] = 1;
  Category* ocat = m_control.getCategory(OUTPUT_CATEGORY);
  if (ocat->getOptionVal(OutputControl::PRTLEV) == VERBOSE) {
    options[0] = 1;
    options[1] = 3;
  }

  partition_type = ADAPTIVE_REPARTITION;
#else
  assert("Dynamic Load Balancing currently incomplete" == 0);
#endif

  // register variables use for the load-balancing operation
  registerVariables();

  // Setup the ordinal-to-local map for the current distribution of
  // elements over processors
  int* node_map = m_mesh.getNodeMap();
  m_node2local.clear();
  for (int i=0; i<Nnp; i++)
    m_node2local[node_map[i]] = i;

  // Now setup the global-to-local map
  int* elem_map = m_mesh.getElemMap();
  m_elem2local.clear();
  for (int i=0; i<Nel; i++)
    m_elem2local[elem_map[i]] = i;

  // Setup the lists of variables to be moved in the data migration phase
  m_mesh.setupMigrationVars(m_elem_var, m_node_var);
  m_physics.setupMigrationVars(m_elem_var, m_node_var);

}

void
DynamicLoadBalance::registerVariables()
/*******************************************************************************
Routine: registerVariables - register all variables for load balancing
Author : Mark A. Christon
*******************************************************************************/
{
  // Register the variables required for the static load-balancing operation
  StaticLoadBalance::registerVariables();

  // Register any additional variable for the dynamic load-balancer.
  // For now, there's none!!!
}
