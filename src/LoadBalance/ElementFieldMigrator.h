//******************************************************************************
//! \file    src/LoadBalance/ElementFieldMigrator.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 17:20:11 2011
//! \brief   Element field migrator
//******************************************************************************
#ifndef ElementFieldMigrator_h
#define ElementFieldMigrator_h

#include <FieldMigrator.h>

namespace Hydra {

//! Element Field Migrator
class ElementFieldMigrator: public FieldMigrator
{
  public:

    //! \name Constructor/Destructor
    //@{
             ElementFieldMigrator(UnsMesh& mesh);
    virtual ~ElementFieldMigrator();
    //@}

    //! Setup the list of elements/surface on processor
    //  Unique elements stay on processor
    void setupCompress(set<int>& uniqueElem);

    //! Setup the list of field variables with the same element set ID
    void setupElemSetField(int setId, int nfdOld, int nfdNew,
                           int numsend, int numrecv);

  private:

    //! Don't permit copy or assignment operators
    //@{
    ElementFieldMigrator(const ElementFieldMigrator&);
    ElementFieldMigrator& operator=(const ElementFieldMigrator&);
    //@}

};

}

#endif // ElementFieldMigrator.h
