//******************************************************************************
//! \file    src/LoadBalance/FieldMigrator.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 17:20:12 2011
//! \brief   Virtual base class for derived node, element, set migrators
//******************************************************************************
#include <cassert>
#include <stdio.h>
#include <cstdlib>

#include <map>
#include <set>
#include <string>
#include <sstream>

using namespace std;

#include <UnsMesh.h>
#include <MPIWrapper.h>
#include <FieldMigrator.h>

using namespace Hydra;

FieldMigrator::FieldMigrator(UnsMesh& mesh):
  m_max_send(0),
  m_max_recv(0),
  SEND_BUF(UNREGISTERED),
  RECV_BUF(UNREGISTERED),
  FIELD_LIST(UNREGISTERED),
  m_mesh(mesh),
  m_nsetfd(0)
/*******************************************************************************
Routine: FieldMigrator - constructor
Author : Mark A. Christon
*******************************************************************************/
{}

FieldMigrator::~FieldMigrator()
/*******************************************************************************
Routine: ~FieldMigrator - destructor
Author : Mark A. Christon
*******************************************************************************/
{}

void
FieldMigrator::compress(DataIndex OLD_FIELD, DataIndex NEW_FIELD)
/*******************************************************************************
Routine: compress - compress the field variable that stays on processor.
Author : Mark A. Christon
*******************************************************************************/
{
  if( m_nfd == 0 ) return;
  assert(OLD_FIELD != UNREGISTERED);
  assert(NEW_FIELD != UNREGISTERED);

  int* stay_list = m_mesh.getVariable<int>(STAY_LIST);
  Real* old_field = m_mesh.getVariable<Real>(OLD_FIELD);
  Real* new_field = m_mesh.getVariable<Real>(NEW_FIELD);

  // Compress from old into new
  for ( int i=0; i < m_nfd_stay; i++) {
    int id = stay_list[i];
    new_field[i] = old_field[id];
  }
}

void
FieldMigrator::finalize()
/*******************************************************************************
Routine: finalize - free all registered data
Author : Mark A. Christon
*******************************************************************************/
{
  m_mesh.freeVariable(FIELD_LIST);
  if (m_nfd > 0)      m_mesh.freeVariable(STAY_LIST);
  if (m_max_send > 0) m_mesh.freeVariable(SEND_BUF);
  if (m_max_recv > 0) m_mesh.freeVariable(RECV_BUF);
}

char*
FieldMigrator::getRecvBuffer()
/*******************************************************************************
Routine: getRecvBuffer - return the RECV buffer for basic receive
Author : Mark A. Christon
*******************************************************************************/
{
  return m_mesh.getVariable<char>(RECV_BUF);
}

void
FieldMigrator::initialize()
/*******************************************************************************
Routine: initialize - initialize the basic data structures for field migration
Author : Mark A. Christon
*******************************************************************************/
{
  pid   = g_comm->getPid();
  Nproc = g_comm->getNproc();
  m_nfd_stay = 0;

  if( m_nfd > 0)
    STAY_LIST = m_mesh.registerVariable(m_nfd,
                                        sizeof(int),
                                        "STAY_LIST");

  if (m_max_send > 0)
    SEND_BUF = m_mesh.registerVariable(m_max_send,
                                       sizeof(Real),
                                       "FIELD_SEND_BUF");


  if (m_max_recv > 0)
    RECV_BUF = m_mesh.registerVariable(m_max_recv,
                                       sizeof(Real),
                                       "FIELD_RECV_BUF");
}

void
FieldMigrator::migrate(int* send_list, int* recv_list,
                       int* send_offset,  DataIndex SENDID_LIST,
                       DataIndex OLD_FIELD, DataIndex NEW_FIELD)
/*******************************************************************************
Routine: migrate - generalized migration function for fields
Author : Mark A. Christon
*******************************************************************************/
{
  // Migrate data in send-receive pairs for each processor
  // Pack the data for sending
  CommRequest* reqs =
    static_cast<CommRequest*>(m_mesh.allocTempData(Nproc*sizeof(CommRequest)));
  CommStatus* status =
    static_cast<CommStatus*>(m_mesh.allocTempData(sizeof(CommStatus)));

  if( m_max_send > 0) {
    char* send_buf = pack(OLD_FIELD, SENDID_LIST, send_list, send_offset);
    for (int iproc=0; iproc<Nproc; iproc++) {
      if (pid != iproc && send_list[iproc] > 0) {
        int mtype = pid+1;
        int nsend = send_list[iproc];
        int size = getDataSize();
        int offset = send_offset[iproc]*size;
        char* send_tmp = &send_buf[offset];
        reqs[iproc] = g_comm->nonblockingSend(mtype, send_tmp, nsend*size, iproc);
      }
    }
  }

  if(m_max_recv > 0) {
    for (int iproc=0; iproc<Nproc; iproc++) {
      if (pid != iproc && recv_list[iproc] > 0) {
        int mtype = iproc+1;
        memset(status, 0, sizeof(CommStatus));
        // waiting matched message arrives
        g_comm->blockingProbe(iproc, mtype, status);
        int size = getDataSize();
        int nrecv = recv_list[iproc];
        char* recv_buf = getRecvBuffer();
        g_comm->receive(recv_buf, nrecv*size, iproc, mtype);
        unpack(nrecv, NEW_FIELD);
      }
    } // End of loop over processor receive lists
  }

  // waiting for nonblocking sending to complete
  for (int iproc=0; iproc<Nproc; iproc++) {
    if (pid != iproc && send_list[iproc] > 0) {
      g_comm->wait(&reqs[iproc]);
    }
  }
  m_mesh.freeTempData(status);
  m_mesh.freeTempData(reqs);
}

void
FieldMigrator::migrateFields(int* send_list, int* recv_list,
                             int* send_offset, DataIndex SENDID_LIST)
/*******************************************************************************
Routine: migrateFields - migrate fields
Author : Mark A. Christon
*******************************************************************************/
{
  DataIndex OLD_FIELD = UNREGISTERED;
  DataIndex NEW_FIELD = UNREGISTERED;

  int* field_list = m_mesh.getVariable<int>(FIELD_LIST);
  for ( int ii =0; ii < m_nsetfd; ii++) {
    int i = field_list[ii];
    Field& field = m_mesh.getField(i);
    OLD_FIELD = field.FIELD;
    NEW_FIELD = UNREGISTERED;
    m_cur_fd = m_nfd_stay;   // set current fd
    if (m_nfd_new > 0 || (m_nfd > 0 && m_nfd_new==0) ) {
      if( m_nfd_new > 0) {
        string fname("Field-");
        stringstream sout;
        sout << i;
        string id = sout.str();
        fname.append(id);
        NEW_FIELD  = m_mesh.registerVariable(m_nfd_new,
                                             sizeof(Real),
                                             fname.c_str(),
                                             field.centering,
                                             field.fieldType);
      }
      if( m_nfd_stay > 0) compress(OLD_FIELD, NEW_FIELD);
      migrate(send_list, recv_list, send_offset, SENDID_LIST,
              OLD_FIELD, NEW_FIELD);
    }
    // Swap memory for the registered variables to keep the new data,
    // and set the new count in the set.
    // 4 cases:
    // 1. nfd == 0, nfd_new == 0 : do nothing
    // 2. nfd == 0, nfd_new  > 0 : keep NEW_FIELD (no OLD_FIELD)
    // 3. nfd  > 0, nfd_new  > 0 : do a memory swap, keep OLD_FIELD
    // 4. nfd  > 0, nfd_new == 0 : Keep OLD_VAR, do nothing

    // case-2: keep the NEW_LIST's
    if (m_nfd == 0 && m_nfd_new > 0) {
      field.FIELD   = NEW_FIELD;
    }

    // case-3: memory swap, keep the OLD_LIST's
    if (m_nfd > 0 && m_nfd_new >0) {
      m_mesh.swapMemory(NEW_FIELD, OLD_FIELD);
      m_mesh.freeVariable(NEW_FIELD);
    }

    // case-4: All elements migrate off-processor
    if (m_nfd > 0 && m_nfd_new == 0) {
      m_mesh.freeVariable(field.FIELD  );
      field.FIELD   = UNREGISTERED;
    }
  }

}

char*
FieldMigrator::pack(DataIndex OLD_FIELD, DataIndex SENDID_LIST,
                    int* send_list, int* send_offset)
/*******************************************************************************
Routine: pack - pack up nodal variables for migration
Author : Mark A. Christon
*******************************************************************************/
{
  assert(OLD_FIELD   != UNREGISTERED);
  assert(SENDID_LIST != UNREGISTERED);

  int* sendid_list = m_mesh.getVariable<int>(SENDID_LIST);
  Real* old_field  = m_mesh.getVariable<Real>(OLD_FIELD);
  Real* send_buf   = m_mesh.getVariable<Real>(SEND_BUF);

  // Pack up the variable to be sent to each processor

  for (int iproc = 0; iproc < Nproc; iproc++) {
    if( iproc != pid && send_list[iproc] > 0) {
      for ( int i =0; i < send_list[iproc]; i++) {
        int offset = send_offset[iproc];
        int id = sendid_list[offset+i];
        send_buf[offset+i] = old_field[id];
      }
    }
  }

  // Cast to a char* for simple byte-stream messaging
  return m_mesh.getVariable<char>(SEND_BUF);
}

void
FieldMigrator::unpack(int nrecv, DataIndex NEW_FIELD)
/*******************************************************************************
Routine: unpack - unpack the field migration variables
Author : Mark A. Christon
*******************************************************************************/
{
  assert(NEW_FIELD != UNREGISTERED);

  Real* new_field  = m_mesh.getVariable<Real>(NEW_FIELD);
  Real* recvfd_buf = m_mesh.getVariable<Real>(RECV_BUF);

  for (int i=0; i<nrecv; i++) {
    new_field[m_cur_fd] = recvfd_buf[i];
    m_cur_fd++;
  }
}
