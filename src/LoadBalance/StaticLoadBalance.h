//******************************************************************************
//! \file    src/LoadBalance/StaticLoadBalance.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 17:20:12 2011
//! \brief   Static load balancer
//******************************************************************************
#ifndef StaticLoadBalance_h
#define StaticLoadBalance_h

#include <vector>
#include <map>

#include <LoadBalance.h>
#include <MPIWrapper.h>

namespace Hydra {

class UnsMesh;
class Timer;
class Element;
class Control;

//! Static Load Balancer
class StaticLoadBalance : public LoadBalance {

  public:

    //! \name Constructor/Destructor
    //@{
             StaticLoadBalance(UnsMesh& mesh, Control& control, Timer& timer);
    virtual ~StaticLoadBalance();
    //@}

    //! Static Load balancer
    void loadBalance();

  protected:

    //! Virtual interface for load-balancing
    //@{
    virtual void initialize();

    virtual void finalize();

    virtual void registerVariables();

    virtual void partitionMesh();
    //@}

    void zoltanGeometricPartitioner(string partitionType);

    void calcCentroids(float* xyz);

    void setupPartWeights(float* procwts);

    //! Partition the dual-graph
    void partitionGraph();

    //! \name Data Migration
    //! Using the assignment of elements-to-processors from
    //! the graph partitioner, migrate element data to its
    //! assigned processor Id (aka, MPI rank).
    //@{
    void calcElemDist();

    void calcNodeDist();

    void migrateElemClasses();

    void migrateElements(Element& eclass, int* elem_pid,
                         int* send_list, int* elem_recv, int* send_offset);

    void migrateElemSets();

    void migrateNodeSets();

    void migrateSideSets();

    void migrateElemVars();

    void migrateNodeVars();

    void setupNewMaps();
    //@}

    //! \name Primary objects for load balancing
    //@{
    UnsMesh& m_mesh;    // The unstructured data-mesh
    Control& m_control; // Holds all of the control info
    Timer&   m_timer;   // The Timer object for all timing data
    //@}

    //! \name Data used for load balancing operations.
    //! All data is registered on the mesh.
    //@{
    DataIndex ELEM_DIST;     // Element/vertex distribution on processors
    DataIndex ELEM_PID;      // Processor id's for elements
    DataIndex NET_ELEM_DIST; // NET Element distribution after load-balancing
    DataIndex SEND_LIST;     // Number of items per processor to send
    DataIndex RECV_LIST;     // Number of items per processor to receive
    DataIndex SEND_OFFSET;   // offset in sending array for each processor
    DataIndex NEW_NODE_MAP;  // New local-to-ordinal node mapping
    //@}

    int pid;
    int Nproc;
    int Ndim;
    int Nel;
    int Nnp;

    int Nel_new;
    int Nnp_new;
    int Nnp_stay; // Length of compressed list of nodes that stay on-processor

#ifdef USE_PARMETIS
    //! \name ParMetis defaults & options
    //@{
    int numflag;
    int ncommonnodes;
    int wgtflag;
    int ncon;
    int edgecut;
    int options[4];

    float ubvec;
    float itr;
#endif

    PartType partition_type;
    //@}

    // \name Migration variables
    //@{
    vector<DataIndex> m_elem_var;
    vector<DataIndex> m_node_var;
    //@}

    //! \name Global-to-local maps
    //! These maps are in terms of the global ordinal Id's for nodes
    //! and elements and the local on-processor Id's for each
    //! entity.
    //@{
    map<int,int> m_elem2local;     // Ordinal elem Id to local elem Id
    map<int,int> m_node2local;     // Ordinal node Id to local node Id
    map<int,int> m_new_node2local; // Ordinal node Id to NEW local node Id
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    StaticLoadBalance(const StaticLoadBalance&);
    StaticLoadBalance& operator=(const StaticLoadBalance&);
    //@}

    //! \name Zoltan mesh partitioners
    //@{
    //! Mesh partitioning with Zoltan: RCB
    void zoltanRCB();
    //! Mesh partitioning with Zoltan: RIB
    void zoltanRIB();
    //@}

#ifdef USE_PARMETIS
    //! \name ParMetis mesh partitioners
    //@{
    int* m_elemDist;
    int* m_elemPid;
    int* m_eInd;
    int* m_ePtr;
    float* m_xyz;
    CommType m_comm;

    //! Setup ParMetis
    bool setupParMetis();
    //! Free variables allocated in setupParMetis
    void freeParMetis();

    float* m_tpwgts;
    int* m_xadj;
    int* m_adjncy;

    //! Setup common for ParMetis KWay partitioning
    void setupParMetisKWay();
    //! Free variables allocated in setupParMetisKWay
    void freeParMetisKWay();

    //! Mesh partitioning with ParMetis: space-filling curve
    void parmetisGeom();
    //! Mesh partitioning with ParMetis: multilevel k-way multi-constraint alg.
    void parmetisKWay();
    //! Mesh partitioning with ParMetis: combine GEOM & KWAY
    void parmetisGeomKWay();
    //! Mesh partitioning with ParMetis: adaptive repartitioning
    void parmetisAdaptiveRepart();
    //@}
#endif
};

}

#endif // StaticLoadBalance_h
