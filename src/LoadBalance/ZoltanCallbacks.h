//******************************************************************************
//! \file    src/LoadBalance/ZoltanCallbacks.h
//! \author  Nathan Barnett, Mark A. Christon
//! \date    Fri Dec 9, 2011
//! \brief   Provides callbacks needed for the Zoltan calls in StaticLoadBalance
//******************************************************************************
#ifndef ZoltanCallbacks_h
#define ZoltanCallbacks_h

#include <zoltan_cpp.h>

namespace Hydra {

class UnsMesh;

//! \brief ZoltanInterface provides basic Zoltan callback functions

namespace ZoltanInterface {

// Not a callback, but needed to setup data for Zoltan
void calcCentroids(Real* xyz, UnsMesh* mesh);

int getNumObjects(void* pmesh, int *ierr);

void getObjectList(void* pmesh, int sizeGID, int sizeLID,
                   ZOLTAN_ID_PTR globalID, ZOLTAN_ID_PTR localID,
                   int wgt_dim, float *obj_wgts, int *ierr);

int getNumGeometry(void* pmesh, int *ierr);

void getGeometryList(void *pmesh, int sizeGID, int sizeLID, int numObj,
                     ZOLTAN_ID_PTR globalID, ZOLTAN_ID_PTR localID,
                     int numDim, double *geomVec, int *ierr);

}
}

#endif // ZoltanCallbacks_h
