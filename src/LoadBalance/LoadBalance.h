//******************************************************************************
//! \file    src/LoadBalance/LoadBalance.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 13:09:56 2011
//! \brief   Virtual base class for static and dynamics load balancing
//******************************************************************************
#ifndef LoadBalance_h
#define LoadBalance_h

#include <iostream>

#include <HydraTypes.h>

namespace Hydra {

enum PartType {
#ifdef USE_PARMETIS
  GEOM_PARTITION,
  KWAY_PARTITION,
  GEOM_KWAY_PARTITION,
  ADAPTIVE_REPARTITION,
#endif
  RCB_PARTITION,
  RIB_PARTITION
};

//! Abstract base class for load-balancers
class LoadBalance {

  public:

    //! \name Constructor/Destructor
    //@{
             LoadBalance();
    virtual ~LoadBalance();
    //@}

    virtual void loadBalance() = 0;

  private:

    //! Don't permit copy or assignment operators
    //@{
    LoadBalance(const LoadBalance&);
    LoadBalance& operator=(const LoadBalance&);
    //@}

    //! Virtual interface for load-balancing
    //@{
    virtual void initialize() = 0;

    virtual void finalize() = 0;

    virtual void registerVariables() = 0;

    virtual void partitionMesh() = 0;
    //@}

};

}

#endif // LoadBalance_h
