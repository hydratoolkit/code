//******************************************************************************
//! \file    src/IO/asciiReader.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:33 2011
//! \brief   Reader for ASCII Hydra mesh, plain ASCII files
//******************************************************************************
#ifndef asciiReader_h
#define asciiReader_h

#include <fstream>

#include <Control.h>
#include <Element.h>
#include <UnsMesh.h>
#include <TokenStream.h>
#include <meshReader.h>

namespace Hydra {

//! ASCII Mesh Reader
class asciiReader: public meshReader {

  public:

    //! \name Constructor/Destructor
    //@{
             asciiReader(string meshname);
    virtual ~asciiReader() {}
    //@}

    virtual void open();

    virtual void close();

    virtual void readMesh(UnsMesh* mesh);

    void readPlainMesh(UnsMesh *mesh);

  protected:

    ifstream  meshf;   //<! Mesh file
    string m_meshname; //<! Mesh file name

  private:

    //! Don't permit copy or assignment operators
    //@{
    asciiReader(const asciiReader&);
    asciiReader& operator=(const asciiReader&);
    //@}

    //! \name ASCII Mesh read operations
    //@{
    void readHeader(UnsMesh* mesh);

    void readConnectivity(UnsMesh* mesh);

    void readPlainConnectivity(UnsMesh* mesh);

    void readCoordinates(UnsMesh* mesh);

    void readPlainCoordinates(UnsMesh* mesh);

    void readNodeset(UnsMesh* mesh);

    void readSideset(UnsMesh* mesh);

    void setupMaps(UnsMesh* mesh);

    int iscomment(char *cdat);
    //@}

};

}
#endif // asciiReader_h
