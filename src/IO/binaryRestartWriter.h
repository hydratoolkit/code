//******************************************************************************
//! \file    src/IO/binaryRestartWriter.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:33 2011
//! \brief   Binary restart writer
//******************************************************************************
#ifndef binaryRestartWriter_h
#define binaryRestartWriter_h

#include <fstream>

#include <restartWriter.h>

namespace Hydra {

//! Binary restart writer
class binaryRestartWriter : public restartWriter {

  public:

    //! \name Constructor/Destructor
    //@{
             binaryRestartWriter(string fname) {m_fname = fname;}
    virtual ~binaryRestartWriter() {}
    //@}

    virtual void open();

    virtual void close();

    virtual void writeDump(Control* control, UnsMesh* mesh);

  protected:

    string m_fname;

  private:

    //! Don't permit copy or assignment operators
    //@{
    binaryRestartWriter(const binaryRestartWriter&);
    binaryRestartWriter& operator=(const binaryRestartWriter&);
    //@}

    ofstream dumpf;

};

}
#endif // binaryRestartWriter_h
