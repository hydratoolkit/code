//******************************************************************************
//! \file    src/IO/restartWriter.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   Abstract base class for deriver restart writers
//******************************************************************************
#ifndef restartWriter_h
#define restartWriter_h

#include <string>

namespace Hydra {

//! Abstract base class for restart writers
class restartWriter {

  public:

    //! \name Constructor/Destructor
    //@{
             restartWriter() {}
    virtual ~restartWriter() {}
    //@}

    virtual void open()  = 0;

    virtual void close() = 0;

    virtual void writeDump(Control* control, UnsMesh* mesh) = 0;

  protected:

    string m_fname;

  private:

    //! Don't permit copy or assignment operators
    //@{
    restartWriter(const restartWriter&);
    restartWriter& operator=(const restartWriter&);
    //@}
};

}

#endif // restartWriter_h
