//******************************************************************************
//! \file    src/IO/exodusWriter.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   Derived plot writer for Exodus-II output
//******************************************************************************
#ifndef exodusWriter_h
#define exodusWriter_h

#include <Category.h>
#include <plotWriter.h>

namespace Hydra {

//! Serial Exodus-II Writer
class exodusWriter : public plotWriter {

  public:

  enum FileMode {OUTPUT = 0,     //<! Open in CLOBBER mode
                 OUTPUT_RESTART  //<! Open for restart run
  };


    //! \name Constructor/Destructor
    //@{
             exodusWriter(string fname, FileMode mode, int format=EXODUSII);
    virtual ~exodusWriter() {}
    //@}

    virtual void open();

    virtual void close();

    // Pure serial writer
    virtual void writeFieldHeader() {}

    virtual void writeFieldFooter() {}

    virtual void writeMesh(UnsMesh* mesh);

    virtual void writeMesh(UnsMesh* mesh, Control* control);

    virtual void writeTimeStamp(int plnum, int step, Real time,
                                Control* control, UnsMesh* mesh);

    virtual void writeFieldNames(Control* control,
                                 vector<string> elemFieldName,
                                 vector<string> nodeFieldName,
                                 vector<string> surfFieldName);

    virtual void writeElemScalarField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      Real* evar);

    virtual void writeElemVectorField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      CVector evec);

    virtual void writeElemSymTensorField(UnsMesh* mesh,
                                         int plnum,
                                         int varId,
                                         string& name,
                                         CSymTensor symtensor);

    virtual void writeNodeScalarField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      Real* nvar);

    virtual void writeNodeVectorField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      CVector nvec);

    virtual void writeNodeSymTensorField(UnsMesh* mesh,
                                         int plnum,
                                         int varId,
                                         string& name,
                                         CSymTensor symtensor);

    virtual void writeSurfScalarField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      Sideset& surf,
                                      string& name,
                                      Real* svar);

    virtual void writeSurfVectorField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      Sideset& surf,
                                      string& name,
                                      CVector svec);

    //! Accessor for file handle
    int getFileHandle() {return m_exofh;}

    //! \name Exodus-II write functions - public for use in utilities
    //@{
    void writeConnectivity(UnsMesh* mesh);

    void writeCoordinateNames(UnsMesh* mesh);

    void writeCoordinates(UnsMesh* mesh);

    void writeMaps(UnsMesh* mesh);

    void writeHeader(UnsMesh* mesh);

    void writeHeader(UnsMesh* mesh, Control *control);

    void writeNodeset(UnsMesh* mesh);

    void writeSideset(UnsMesh* mesh);
    //@}

  protected:

  private:

    //! Don't permit copy or assignment operators
    //@{
    exodusWriter(const exodusWriter&);
    exodusWriter& operator=(const exodusWriter&);
    //@}

    int m_pid;           //!< processor rank
    int m_Nproc;         //!< No. of processors
    int m_exofh;         //!< Exodus file handle for the output plot file
    int m_format;        //!< File format: EXODUSII or EXODUSII_HDF5
    FileMode m_filemode; //!< Mode to open the file
    string m_fname;      //!< File name

};

}
#endif // exodusWriter_h
