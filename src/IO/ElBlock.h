//******************************************************************************
//! \file    src/IO/ElBlock.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:33 2011
//! \brief   Basic element block information for IO
//******************************************************************************
#ifndef ElBlock_h
#define ElBlock_h

namespace Hydra {

struct ElBlock {
  //! \name Element Block -- used for holding basic block information.
  //! The ElBlock structure is currently used only for input processing.
  //@{
  int Id;         //!< Id for the block
  Eltype type;    //!< Element type
  int Nel;        //!< No. of elements in the block
  int Nnpe;       //!< No. of nodes per element
  int Nattr;      //!< No. of attributes in the block
  //@}
};

}
#endif // ElBlock_h
