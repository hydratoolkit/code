# ############################################################################ #
#
# Library: IO
# File Definition File
#
# ############################################################################ #

# Source Files

set(IO_SOURCE_FILES
  fileIO.C
  OutputDelegate.C
  asciiReader.C
  asciiHistoryWriter.C
  asciiWriter.C
  binaryRestartReader.C
  binaryRestartWriter.C
  exodusReader.C
  fanoutExodusReader.C
  fluentReader.C
  fluentWriter.C
  netgenReader.C
  gmvWriter.C
  exodusWriter.C
  faninExodusWriter.C
  vtkWriter.C
)

if(USE_CATALYST)
  list(APPEND IO_SOURCE_FILES catalystAdaptor.C)
else()
  # add in the header file so that it shows up in IDEs
  list(APPEND IO_SOURCE_FILES catalystAdaptor.h)
endif()


