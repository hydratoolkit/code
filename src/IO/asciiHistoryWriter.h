//******************************************************************************
//! \file    src/IO/asciiHistoryWriter.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   ASCII time-history writer
//******************************************************************************
#ifndef asciiHistoryWriter_h
#define asciiHistoryWriter_h

#include <map>

#include <Control.h>
#include <UnsMesh.h>
#include <OutputDelegate.h>
#include <historyWriter.h>

namespace Hydra {

//! ASCII time-history writer
class asciiHistoryWriter : public historyWriter {

  public:

    //! \name Constructor/Destructor
    //@{
             asciiHistoryWriter(string fname);
    virtual ~asciiHistoryWriter() {}
    //@}

    //! \name Open all surface, node and element time-history files
    virtual void open(UnsMesh* mesh, Control* control,
                      const OutputDelegateKey key);

    virtual void close();

    virtual void writeHistoryData(const OutputDelegateKey& key,
                                  int Nvar, Real* var);

    virtual void writeHistoryData(const OutputDelegateKey& key,
                                  int Nvar, Vector var);

    virtual void writeSurfaceHistoryData(const OutputDelegateKey& key,
                                         int Nvar, Real* var);

  private:

    //! Don't permit copy or assignment operators
    //@{
    asciiHistoryWriter(const asciiHistoryWriter&);
    asciiHistoryWriter& operator=(const asciiHistoryWriter&);
    //@}

    void concatFname(string& fname, string vname, int setId, bool addpid);

    int m_pid;           //!< processor rank
    int m_Nproc;         //!< No. of processors

    string m_fname;

    //! \name File pointer map for all history delegates
    typedef map<OutputDelegateKey, ofstream*> DelegateStreamMap;
    DelegateStreamMap m_streams;

};

}
#endif // historyWriter_h
