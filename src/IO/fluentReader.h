//******************************************************************************
//! \file    src/IO/fluentReader.h
//! \author  Mark A. Christon
//! \date    Thu Oct 05 2016
//! \brief   Fluent msh reader
//******************************************************************************
#ifndef asciiReader_h
#define asciiReader_h

#include <fstream>
#include <set>

#include <Control.h>
#include <Element.h>
#include <UnsMesh.h>
#include <TokenStream.h>
#include <meshReader.h>

namespace Hydra {

struct Face {
  //! \name Face data structure for Fluent faces
  //@{
  int Id;            //!< Zone Id used for node-set/side-set construction
  int type;          //!< Fluent face type, e.g., "bc name": 2 - 37
  int gid1;          //!< Element Id's that own the face
  int gid2;          //!< Element Id's that share the face, or -1 for external
  vector<int> nodes; //!< Node Id's for the face -- size determines topo
  //@}
 };

struct Felement {
  //! \name Fluent element for reading, mesh (re)construction
  //@{
  Eltype type;         //!< Element type: HEX8, WEDGE6, etc.
  vector<bool> flip;   //!< Indicates a gid2 face, connectivity to be flipped
  vector<int> faceIds; //!< List of faces attached to the element
  //@}
};

struct ZoneId {
  //! \name Fluent element for reading, mesh (re)construction
  //@{
  bool isSet;    //!< Indicates zone should generate a node-set and a side-set
  int Id;        //!< Zone Id used for node-set/side-set construction
  int ztype;     //!< Zone type, e.g., interior, wall, e.g., "bc name": 2-37
  string bctype; //!< Type, e.g., fluid, wall, symmetry
  string bcname; //!< Name for the zone
  //@}
};

struct BlockParam {
  //! \name Blocks parameters to be filled in from the Fluent mesh
  //@{
  Eltype type;  //!< Element type: HEX8, WEDGE6, etc.
  int Nel;      //!< Number of elements of this type
  BlockParam(Eltype etype, int nel) {
    type = etype;
    Nel = nel;
  }
  //@}
};

//! Fluent msh Reader
class fluentReader: public meshReader {

  public:

    //! \name Constructor/Destructor
    //@{
             fluentReader(string meshname);
    virtual ~fluentReader();
    //@}

    virtual void open();

    virtual void close();

    virtual void readMesh(UnsMesh* mesh);

  protected:

    ifstream  meshf;   //<! Mesh file
    string m_meshname; //<! Mesh file name

  private:

    //! Don't permit copy or assignment operators
    //@{
    fluentReader(const fluentReader&);
    fluentReader& operator=(const fluentReader&);
    //@}

    //! \name ASCII Mesh read operations
    //@{
    void addElemClasses(UnsMesh* mesh);

    void buildConnectivity(UnsMesh* mesh);

    void buildHex8Connectivity(vector<int>& ix, int gid);

    void buildTet4Connectivity(vector<int>& ix, int gid);

    void buildPyr5Connectivity(vector<int>& ix, int gid);

    void buildWedge6Connectivity(vector<int>& ix, int gid);

    void countFaces();

    void countZones();

    void echoFluentParms();

    void genNodesets(UnsMesh* mesh);

    void genSidesets(UnsMesh* mesh);

    void readCoordinates(UnsMesh* mesh, int start, int end);

    void readElemTypes();

    void readMixedFaces(int Nfaces, int Id, int type);

    void readPeriodicFaces(int Nfaces);

    void readQuadFaces(int Nfaces, int Id, int type);

    void readTriFaces(int Nfaces, int Id, int type);

    void readLine(string& sbuf);

    void setupElements();

    void setupMaps(UnsMesh* mesh);

    void setupSets(UnsMesh* mesh);
    //@}

    char m_cbuf[MAXCHR];

    int Nbc;

    vector<Felement*> m_felements;

    vector<Face*> m_facelist;

    vector<ZoneId*> m_zones;

    vector<BlockParam*> m_blocklist;

};

}
#endif // fluentReader_h
