//******************************************************************************
//! \file    src/IO/catalystAdaptor.h
//! \author  Andy Bauer, Mark A. Christon
//! \date    Mon April 21 2014
//! \brief   Class for ParaView Catalyst in situ output
//******************************************************************************
#ifndef catalystAdaptor_h
#define catalystAdaptor_h

#include <string>
#include <utility>

#include <fileIO.h> // for DelegateMap definition

struct CSymTensor;
struct CVector;
class UnsMesh;
#ifdef USE_CATALYST
class vtkCPProcessor;
class vtkIdList;
class vtkMultiBlockDataSet;
class vtkUnstructuredGrid;
#endif

namespace Hydra {

  class catalystAdaptor {

  public:


    //! \name Constructor/Destructor
    //@{
             catalystAdaptor(fileIO* fileio);
    virtual ~catalystAdaptor();
    //@}

    //! \name add node- or cell-centered fields to the grid
    //@{
    void addField(const string& name, const bool isCellData,
                  Real* inArray);
    void addField(const string& name, const bool isCellData,
                  CVector& inVec);
    void addField(const string& name, const bool isCellData,
                  CSymTensor& inSymTensor);
    //@}

    //! \name add a pipeline for Catalyst to process
    //! Add in a Python script pipeline and the fields that
    //! are required in order to execute it properly
    //@{
    void addPipeline(string& fileName, vector<string>& nodeVars,
                     vector<string>& elemVars,
                     vector<OutputId*>& sideVars);
    //@}

    //! \name Add surface-centered fields to the grid
    //@{
    void addSurfField(const string& name, Sideset& surf, Real* inArray);
    void addSurfField(const string& name, Sideset& surf, CVector& inVec);
    //@}

    //! \name check if coprocessing needs to be done and do it as required
    //@{
    void coProcess(double time, int timeStep, bool forceOutput,
                   UnsMesh* mesh, fileIO::DelegateMap& delegates,
                   set<OutputDelegateKey>& elemKeys,
                   set<OutputDelegateKey>& nodeKeys,
                   set<OutputDelegateKey>& surfKeys);
    //@}

  protected:

#ifdef USE_CATALYST
    //! \name Create the VTK grid representation of Hydra's unstructured mesh
    //@{
    void createVTKGrid(UnsMesh* mesh, vtkMultiBlockDataSet* grid);
    //@}

    //! \name Fill in the cell data from the volumetric fields for side sets
    //@{
    void fillSideSetFields(UnsMesh* mesh, vtkMultiBlockDataSet* grid);
    //@}

    //! \name return the point ids to a side for specifying side sets.
    //! Note that side ordering for Hydra/ExodusII is different than VTK.
    //@{
    vtkIdList* getCellSidePointIds(vtkUnstructuredGrid* grid, int cellId,
                                   int side);
    //@}

    //! \name get the requested delegate given variable name, centering
    //! and a set of available delegates.
    //@{
    OutputDelegate* getDelegate(
      const string& variable, const VariableCentering centering,
      const fileIO::DelegateMap& delegates,
      const set<OutputDelegateKey>& keys, OutputDelegateKey& key);
    //@}
#endif

  private:

    //! Don't permit copy or assignment operators
    //@{
    catalystAdaptor(const catalystAdaptor&);
    catalystAdaptor& operator=(const catalystAdaptor&);
    //@}

#ifdef USE_CATALYST
    //! \name member variables for Catalyst objects
    //! m_surfIdToGrid map is from Sideset.id to VTK grid
    //@{
    vtkCPProcessor* m_processor;
    vtkMultiBlockDataSet* m_grid;
    map<int,vtkUnstructuredGrid*> m_surfIdToGrid;
    //@}
#endif

    //! \name pointer to the fileIO object that created this object.
    //! This is for writing out warning and error information.
    //@{
    fileIO* const m_io;
    //@}

    /*! \name vectors of required variables for each pipeline.
     */
    vector<vector<string> > m_elemVariables;
    vector<vector<string> > m_nodeVariables;
    vector<vector<OutputId*> > m_sideVariables;
    //@}

};

#ifndef USE_CATALYST
  // If we aren't using catalyst we won't build catalystAdaptor.C.
  // To avoid having a bunch of #ifdef's scattered through the code
  // we implement the missing methods here with warnings to indicate that
  // Catalyst isn't being used.
  inline void outputCatalystWarning(fileIO* io)
  {
    static bool outputWarning = false;
    if(outputWarning == false) {
      io->writeError("Hydra not built with Catalyst");
      outputWarning = true;
    }
  }

  catalystAdaptor::catalystAdaptor(fileIO* fileio) :m_io(fileio) {}
  catalystAdaptor::~catalystAdaptor() {}
  void catalystAdaptor::addField(const string&, const bool, Real*)
  {
    outputCatalystWarning(m_io);
  }
  void catalystAdaptor::addField(const string&, const bool, CVector&)
  {
    outputCatalystWarning(m_io);
  }
  void catalystAdaptor::addField(const string&, const bool, CSymTensor&)
  {
    outputCatalystWarning(m_io);
  }
  void catalystAdaptor::addPipeline(string&, vector<string>&,
                                    vector<string>&, vector<OutputId*>&)
  {
    outputCatalystWarning(m_io);
  }
  void catalystAdaptor::addSurfField(const string&, Sideset&, Real*)
  {
    outputCatalystWarning(m_io);
  }
  void catalystAdaptor::addSurfField(const string&, Sideset&, CVector&)
  {
    outputCatalystWarning(m_io);
  }
  void catalystAdaptor::coProcess(double, int, bool, UnsMesh*,
                                  fileIO::DelegateMap&,
                                  set<OutputDelegateKey>&,
                                  set<OutputDelegateKey>&,
                                  set<OutputDelegateKey>&) {}
#endif

}
#endif // catalystAdaptor_h
