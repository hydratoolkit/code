//******************************************************************************
//! \file    src/IO/faninExodusWriter.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   Derived plot writer for Exodus-II output
//******************************************************************************
#ifndef faninExodusWriter_h
#define faninExodusWriter_h

#include <Category.h>
#include <plotWriter.h>

namespace Hydra {

#define EXODUS_FANINSIZE 4096 //<! Define the strip-mine size for fanin

//! Fanin Exodus-II Writer
class faninExodusWriter : public plotWriter {

  public:

    enum FileMode {OUTPUT = 0,     //<! Open in CLOBBER mode
                   OUTPUT_RESTART  //<! Open for restart run
    };

    enum CommTags {ELEM_COLLATE_SIZE = 10000,  //!< Start for message tags
                   ELEM_COLLATE_IDS,
                   ELEM_COLLATE_VAL,
                   NODE_SCALAR_SIZE,
                   NODE_SCALAR_IDS,
                   NODE_SCALAR_VAL,
                   NODE_VECTOR_SIZE,
                   NODE_VECTOR_IDS,
                   NODE_VECTOR_VAL,
                   SURF_SCALAR_SIZE,
                   SURF_SCALAR_IDS,
                   SURF_SCALAR_VAL,
                   SURF_VECTOR_SIZE,
                   SURF_VECTOR_IDS,
                   SURF_VECTOR_VAL
    };

    //! \name Constructor/Destructor
    //@{
             faninExodusWriter(string fname, string mname, FileMode mode,
                               int format=EXODUSII);
    virtual ~faninExodusWriter() {}
    //@}

    //! \name Virtual Fanin Exodus Writer Interface
    //@{
    virtual void open();

    virtual void close();

    virtual void writeFieldHeader() {}

    virtual void writeFieldFooter() {}

    virtual void writeMesh(UnsMesh* mesh, Control* control);

    virtual void writeTimeStamp(int plnum, int step, Real time,
                                Control* control, UnsMesh* mesh);

    virtual void writeFieldNames(Control* control,
                                 vector<string> elemFieldName,
                                 vector<string> nodeFieldName,
                                 vector<string> surfFieldName);

    virtual void writeElemScalarField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      Real* evar);

    virtual void writeElemVectorField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      CVector evec);

    virtual void writeElemSymTensorField(UnsMesh* mesh,
                                         int plnum,
                                         int varId,
                                         string& name,
                                         CSymTensor symtensor);

    virtual void writeNodeScalarField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      Real* nvar);

    virtual void writeNodeVectorField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      CVector nvec);

    virtual void writeNodeSymTensorField(UnsMesh* mesh,
                                         int plnum,
                                         int varId,
                                         string& name,
                                         CSymTensor symtensor);

    virtual void writeSurfScalarField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      Sideset& surf,
                                      string& name,
                                      Real* svar);

    virtual void writeSurfVectorField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      Sideset& surf,
                                      string& name,
                                      CVector svec);
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    faninExodusWriter(const faninExodusWriter&);
    faninExodusWriter& operator=(const faninExodusWriter&);
    //@}

    //! \name Exodus-II/Nemesis-I parallel read and fan-out functions
    //@{
    //! Collate element field in serial order and write in blocks
    int collateElemField(UnsMesh *mesh, Real* data, int cid, int plnum,
                         int elnum, int varId);

    //! Collate sideset in serial order and write in blocks
    void collateSideset(UnsMesh* mesh, Sideset& sdset, int Nel_g);

    void readwriteConnectivity(UnsMesh* mesh);

    void readwriteMaps(UnsMesh* mesh);

    void readwriteCoordinateNames(UnsMesh* mesh);

    void readwriteCoordinates(UnsMesh* mesh);

    void readwriteNodeset(UnsMesh* mesh);

    void readwriteSideset(UnsMesh* mesh);

    void writeHeader(UnsMesh* mesh, Control *control);

    void writeSideset(UnsMesh* mesh);
    //@}

    int m_pid;             //!< processor rank
    int m_Nproc;           //!< # of ranks

    int m_exofh;           //!< Exodus file handle for the output plot file
    int m_meshfh;          //!< Exodus file handle for the input  mesh file

    int m_format;          //!< File format: EXODUSII or EXODUSII_HDF5

    FileMode m_filemode;   //!< Mode to open the file

    string m_fname;        //!< Plot file name
    string m_meshfn;       //!< Mesh file name

};

}
#endif // faninExodusWriter_h
