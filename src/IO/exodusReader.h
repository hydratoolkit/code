//******************************************************************************
//! \file    src/IO/exodusReader.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:33 2011
//! \brief   Virtual base class for deriver mesh readers
//******************************************************************************
#ifndef exodusReader_h
#define exodusReader_h

#include <fstream>

#include <Control.h>
#include <Element.h>
#include <ElBlock.h>
#include <UnsMesh.h>
#include <TokenStream.h>
#include <meshReader.h>

namespace Hydra {

//! Exodus-II Serial Reader
class exodusReader: public meshReader {

  public:

    //! \name Constructor/Destructor
    //@{
             exodusReader(string meshname);
    virtual ~exodusReader() {}
    //@}

    virtual void open();

    virtual void close();

    virtual void readMesh(UnsMesh* mesh);

  protected:

  private:

    //! Don't permit copy or assignment operators
    //@{
    exodusReader(const exodusReader&);
    exodusReader& operator=(const exodusReader&);
    //@}

    //! \name Exodus-II serial read functions
    //@{
    void readHeader(UnsMesh* mesh);

    void readCoordinateNames(UnsMesh* mesh);

    void readCoordinates(UnsMesh* mesh);

    void readConnectivity(UnsMesh* mesh);

    void readMaps(UnsMesh* mesh);

    void readElTypes(UnsMesh* mesh);

    void readNodeset(UnsMesh* mesh);

    void readSideset(UnsMesh* mesh);

    void readNodeVar(UnsMesh* mesh, int plnum, Real time,
                     vector<DataIndex> nodevar);

    void readVarNames(UnsMesh* mesh, vector<DataIndex> nodevar);
    //@}

    int m_exofh;       //!< Exodus file handle for the input mesh file
    string m_meshname; //!< Mesh file name
};

}
#endif // exodusReader_h
