//******************************************************************************
//! \file    src/IO/netgenReader.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   Netgen neutral file reader
//******************************************************************************
#ifndef asciiReader_h
#define asciiReader_h

#include <fstream>
#include <set>

#include <Control.h>
#include <Element.h>
#include <UnsMesh.h>
#include <TokenStream.h>
#include <meshReader.h>

namespace Hydra {

struct BC {
  //! \name BC data structure used to hold input nodes from netgen
  //@{
  int  id;      //!< Nodeset id
  int  nd[3];   //!< Node nodes in nodeset
  //@}
};

//! netgen mesh reader
class netgenReader: public meshReader {

  public:

    //! \name Constructor/Destructor
    //@{
             netgenReader(string meshname);
    virtual ~netgenReader();
    //@}

    virtual void open();

    virtual void close();

    virtual void readMesh(UnsMesh* mesh);

  protected:

    ifstream  meshf;   //<! Mesh file
    string m_meshname; //<! Mesh file name

  private:

    //! Don't permit copy or assignment operators
    //@{
    netgenReader(const netgenReader&);
    netgenReader& operator=(const netgenReader&);
    //@}

    //! \name ASCII Mesh read operations
    //@{
    void readConnectivity(UnsMesh* mesh);

    void readCoordinates(UnsMesh* mesh);

    void readBCs(UnsMesh* mesh);

    void setupSets(UnsMesh* mesh);

    void genNodesets(UnsMesh* mesh);

    void genSidesets(UnsMesh* mesh);

    void setupMaps(UnsMesh* mesh);
    //@}

    int Nbc;

    BC* bcs;       // Stack of netgen "bc's"
    set<int> nset; // Nodeset/sideset Id's

};

}
#endif // netgenReader_h
