//******************************************************************************
//! \file    src/IO/fluentWriter.h
//! \author  Mark A. Christon
//! \date    Wed Oct 05 2016
//! \brief   Derived plot writer for ASCII FLUENT output -- really just meshes
//******************************************************************************
#ifndef fluentWriter_h
#define fluentWriter_h

#include <plotWriter.h>

namespace Hydra {

//! Fluent msh writer
class fluentWriter : public plotWriter {

  public:

    //! \name Constructor/Destructor
    //@{
             fluentWriter() {};
             fluentWriter(string fname) {m_fname = fname;}
    virtual ~fluentWriter() {}
    //@}

    virtual void open();

    virtual void close();

    virtual void writeFieldHeader() {}

    virtual void writeFieldFooter() {}

    virtual void writeMesh(UnsMesh* mesh);

    virtual void writeMesh(UnsMesh* /*mesh*/, Control* /*control*/) {}

    virtual void writeTimeStamp(int /*plnum*/, int /*step*/, Real /*time*/,
                                Control* /*control*/, UnsMesh* /*mesh*/) {}

    // open, close, writeFieldNames not used
    virtual void writeFieldNames(Control* /*control*/,
                                 vector<string> /*elemFieldName*/,
                                 vector<string> /*nodeFieldName*/,
                                 vector<string> /*surfFieldName*/) {}

    virtual void writeElemScalarField(UnsMesh* /*mesh*/,
                                      int /*plnum*/,
                                      int /*varId*/,
                                      string& /*name*/,
                                      Real* /*evar*/) {}

     virtual void writeElemVectorField(UnsMesh* /* mesh */,
                                       int /* plnum */,
                                       int /* varId */,
                                       string& /* name */,
                                       CVector /* evec */) {}

     virtual void writeElemSymTensorField(UnsMesh* /* mesh */,
                                          int /* plnum */,
                                          int /* varId */,
                                          string& /* name */,
                                          CSymTensor /* symtensor */) {}

     virtual void writeNodeScalarField(UnsMesh* /* mesh */,
                                       int /* plnum */,
                                       int /* varId */,
                                       string& /* name */,
                                       Real* /* nvar */) {}

     virtual void writeNodeVectorField(UnsMesh* /* mesh */,
                                       int /* plnum */,
                                       int /* varId */,
                                       string& /* name */,
                                       CVector /* nvec */) {}

     virtual void writeNodeSymTensorField(UnsMesh* /* mesh */,
                                          int /* plnum */,
                                          int /* varId */,
                                          string& /* name */,
                                          CSymTensor /* symtensor */) {}

     virtual void writeSurfScalarField(UnsMesh* /* mesh */,
                                       int /* plnum */,
                                       int /* varId */,
                                       Sideset& /* surf */,
                                       string& /* name */,
                                       Real* /* svar */) {}

     virtual void writeSurfVectorField(UnsMesh* /* mesh */,
                                       int /* plnum */,
                                       int /* varId */,
                                       Sideset& /* surf */,
                                       string& /* name */,
                                       CVector /* svec */) {}

    //! \name ASCII msh write functions - public for use in utilities
    //@{
    void writeCells(UnsMesh* mesh);

    void writeCoordinates(UnsMesh* mesh);

    void writeFaces(UnsMesh* mesh);

    void writeMixedFaces(UnsMesh* mesh);

    void writeUniformFaces(UnsMesh* mesh, int Nfpe);

    void writeHeader(UnsMesh* mesh);

    void writeZones(UnsMesh* mesh);
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    fluentWriter(const fluentWriter&);
    fluentWriter& operator=(const fluentWriter&);
    //@}

    ofstream  meshf;  //!< ASCII Mesh file (output)
};

}
#endif // fluentWriter_h
