//******************************************************************************
//! \file    src/IO/historyWriter.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   Time-history writer interface
//******************************************************************************
#ifndef historyWriter_h
#define historyWriter_h

#include <string>

#include <OutputDelegate.h>
#include <DataShapes.h>

namespace Hydra {

//! Abstract base class for time-history writers
class historyWriter {

  public:

    //! \name Constructor/Destructor
    //@{
             historyWriter() {}
    virtual ~historyWriter() {}
    //@}

    virtual void open(UnsMesh* mesh, Control* control,
                      const OutputDelegateKey key) = 0;

    virtual void close() = 0;

    virtual void writeHistoryData(const OutputDelegateKey& key,
                                  int Nvar, Real* var) = 0;

    virtual void writeHistoryData(const OutputDelegateKey& key,
                                  int Nvar, Vector var) = 0;

    virtual void writeSurfaceHistoryData(const OutputDelegateKey& key,
                                         int Nvar, Real* var) = 0;

    void setTime(Real time) {m_time = time;}

  protected:

    Real m_time;

  private:

    //! Don't permit copy or assignment operators
    //@{
    historyWriter(const historyWriter&);
    historyWriter& operator=(const historyWriter&);
    //@}

};

}

#endif // historyWriter_h
