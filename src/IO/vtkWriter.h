//******************************************************************************
//! \file    src/IO/vtkWriter.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   ASCII VTK Writer
//******************************************************************************
#ifndef vtkWriter_h
#define vtkWriter_h

#include <plotWriter.h>

namespace Hydra {

//! VTK plot writer -- serial and ASCII Only
class vtkWriter : public plotWriter {

  public:

    enum VTKEntity {VTK_VERTEX=1,
                    VTK_POLY_VERTEX,
                    VTK_LINE,
                    VTK_POLY_LINE,
                    VTK_TRIANGLE,
                    VTK_TRIANGLE_STRIP,
                    VTK_POLYGON,
                    VTK_PIXEL,
                    VTK_QUAD,
                    VTK_TETRA,
                    VTK_VOXEL,
                    VTK_HEXAHEDRON,
                    VTK_WEDGE,
                    VTK_PYRAMID,
                    VTK_PENTAGONAL_PRISM,
                    VTK_HEXAGONAL_PRISM };

    //! \name Constructor/Destructor
    //@{
             vtkWriter(string fname);
    virtual ~vtkWriter() {}
    //@}

    void open();

    void close();

    virtual void writeElemFieldHeader();

    virtual void writeElemFieldFooter();

    virtual void writeFieldHeader() {}

    virtual void writeFieldFooter();

    virtual void writeNodeFieldHeader();

    virtual void writeNodeFieldFooter();

    virtual void writeMesh(UnsMesh* /*mesh*/) {}

    virtual void writeMesh(UnsMesh* /*mesh*/, Control* /*control*/) {}

    virtual void writeTimeStamp(int plnum, int step, Real time,
                                Control* control, UnsMesh* mesh);

    virtual void writeFieldNames(Control* /*control*/,
                                 vector<string> /*elemFieldName*/,
                                 vector<string> /*nodeFieldName*/,
                                 vector<string> /*surfFieldName*/) {}

    virtual void writeElemScalarField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      Real* evar);

    virtual void writeElemVectorField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      CVector evec);

    virtual void writeElemSymTensorField(UnsMesh* mesh,
                                         int plnum,
                                         int varId,
                                         string& name,
                                         CSymTensor symtensor);

    virtual void writeNodeScalarField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      Real* nvar);

    virtual void writeNodeVectorField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      CVector nvec);

    virtual void writeNodeSymTensorField(UnsMesh* mesh,
                                         int plnum,
                                         int varId,
                                         string& name,
                                         CSymTensor symtensor);

    virtual void writeSurfScalarField(UnsMesh* /*mesh*/,
                                      int /*plnum*/,
                                      int /*varId*/,
                                      Sideset& /*surf*/,
                                      string& /*name*/,
                                      Real* /*svar*/) {}

    virtual void writeSurfVectorField(UnsMesh* /*mesh*/,
                                      int /*plnum*/,
                                      int /*varId*/,
                                      Sideset& /*surf*/,
                                      string& /*name*/,
                                      CVector /*svec*/) {}

    //! \name VTK write functions - public for use in utilities
    //@{
    void writeConnectivity(UnsMesh* mesh);

    void writeCoordinates(UnsMesh* mesh);
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    vtkWriter(const vtkWriter&);
    vtkWriter& operator=(const vtkWriter&);
    //@}

    int m_pid;       //!< processor rank
    int m_Nproc;     //!< No. of processors
    string m_fname;  //!< Name without the absolute path and extension
    ofstream  plotf; //!< plot file
    ofstream pplotf; //!< Parallel plot file, i.e., pvd file
};

}
#endif // vtkWriter_h
