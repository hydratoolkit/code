//******************************************************************************
//! \file    src/IO/meshReader.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   Abstract base class for derived mesh readers
//******************************************************************************
#ifndef meshReader_h
#define meshReader_h

#include <UnsMeshParm.h>

namespace Hydra {

//! Abstract base class for mesh readers
class meshReader {

  public:

    //! \name Constructor/Destructor
    //@{
             meshReader() {}
    virtual ~meshReader() {}
    //@}

    //@{
    //! \name Virtual interface for mesh readers
    virtual void open()  = 0;

    virtual void close() = 0;

    virtual void readMesh(UnsMesh* mesh) = 0;
    //@}

  protected:

    UnsMeshParm m_meshParm;

  private:

    //! Don't permit copy or assignment operators
    //@{
    meshReader(const meshReader&);
    meshReader& operator=(const meshReader&);
    //@}
};

}
#endif // meshReader_h
