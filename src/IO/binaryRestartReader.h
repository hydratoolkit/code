//******************************************************************************
//! \file    src/IO/binaryRestartReader.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:33 2011
//! \brief   Binary restart reader
//******************************************************************************
#ifndef binaryRestartReader_h
#define binaryRestartReader_h

#include <fstream>

#include <restartReader.h>

namespace Hydra {

// Forward declarations
class Control;
class UnsMesh;

//! Binary restart reader
class binaryRestartReader : public restartReader {

  public:

    //! \name Constructor/Destructor
    //@{
             binaryRestartReader(string fname) {m_fname = fname;}
    virtual ~binaryRestartReader() {}
    //@}

    virtual void open();

    virtual void close();

    virtual void readDump(Control* control, UnsMesh* mesh);

  protected:

    string m_fname;

  private:

    //! Don't permit copy or assignment operators
    //@{
    binaryRestartReader(const binaryRestartReader&);
    binaryRestartReader& operator=(const binaryRestartReader&);
    //@}

    ifstream restf;

};

}
#endif // binaryRestartReader_h
