//******************************************************************************
//! \file    src/IO/OutputDelegateKey.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   Output delegate key for managing output requests
//******************************************************************************
#ifndef OutputDelegateKey_h
#define OutputDelegateKey_h

#include <cstring>
#include <string>
#include <iostream>
#include <MPIWrapper.h>
#include <HydraTypes.h>
#include <DataContainerEntry.h>
#include <DataShapes.h>

namespace Hydra {

struct OutputDelegateKey {
  //! \name Structure used as the key to lookup delegates
  //@{
  bool onProc;         //!< Variable is on-processor
  bool hasCoord;       //!< Carrying a valid coordinate point (pt)
  VariableCentering centering; //!< Mesh centering
  int outputType;      //!< Type of output requested
  VariableType vtype;  //!< Variable type: SCALAR_VAR, VECTOR_VAR, etc.
  bool registeredVar;  //!< Use registered variable for output
  int Id;              //!< Local (on-proc.) node, element or internal set Id
  int userId;          //!< User node, element or set Id
  DataIndex vid;       //!< Variable data index
  string variable;     //!< Name for the requested output
  Vector pt;           //!< Point for coordinate time-history data echo
  //@}

  //! \name OutputDelegateKey constructor
  //! Construct the key with vid initialized to UNREGISTERED to make it
  //! clear what it's state is.
  //@{
  OutputDelegateKey() : vid(UNREGISTERED) {}
  //@}

  //! Define < operator for comparator in delegate map
  bool operator < (const OutputDelegateKey& key) const {
    int vardiff = strcasecmp(variable.c_str(), key.variable.c_str());
    if (vardiff < 0) {
      return true;
    }
    else if (vardiff == 0 && centering < key.centering) {
      return true;
    }
    else if (vardiff == 0 && centering == key.centering &&
             outputType < key.outputType) {
      return true;
    }
    else if (vardiff == 0 && centering == key.centering &&
             outputType == key.outputType && Id < key.Id) {
      return true;
    }
    else {
      return false;
    }
  }
};

}

#endif
