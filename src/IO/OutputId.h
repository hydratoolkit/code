//******************************************************************************
//! \file    src/IO/OutputId.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   Simple struct to hold parsed output request information
//******************************************************************************
#ifndef OutputId_h
#define OutputId_h

#include <cstring>
#include <string>

#include <DataShapes.h>

namespace Hydra {

struct OutputId {
  //! \name Output Id struct for surface and time-history output requests
  //@{
  bool useCoord; //!< Use coordinate to locate history node/element
  bool onProc;   //!< Variable lives on processor
  int Id;        //!< Local (on-proc) node, element or sideset Id
  int userId;    //!< User node, element or sideset Id
  Vector pt;     //!< Coordinate for time-history output
  string name;   //!< Variable name for the surface output
  //@}

  OutputId(): useCoord(false), onProc(true), Id(-1), userId(-1) {
    pt.X = 0.0;
    pt.Y = 0.0;
    pt.Z = 0.0;
  }

};

}

#endif

