//******************************************************************************
//! \file    src/IO/plotWriter.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   Abstract base class for derived plot writers
//******************************************************************************
#ifndef plotWriter_h
#define plotWriter_h

#include <string>

namespace Hydra {

//! Abstract base class for plot writers
class plotWriter {

  public:

    //! \name Constructor/Destructor
    //@{
             plotWriter() {}
    virtual ~plotWriter() {}
    //@}

    virtual void open()  = 0;

    virtual void close() = 0;

    virtual void writeElemFieldHeader() {};

    virtual void writeElemFieldFooter() {};

    virtual void writeFieldHeader() = 0;

    virtual void writeFieldFooter() = 0;

    virtual void writeNodeFieldHeader() {};

    virtual void writeNodeFieldFooter() {};

    virtual void writeMesh(UnsMesh* mesh, Control* control) = 0;

    virtual void writeTimeStamp(int plnum, int step, Real time,
                                Control* control, UnsMesh* mesh) = 0;

    virtual void writeFieldNames(Control* control,
                                 vector<string> elemFieldName,
                                 vector<string> nodeFieldName,
                                 vector<string> surfFieldName) = 0;

    virtual void writeElemScalarField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      Real* evar) = 0;

    virtual void writeElemVectorField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      CVector evec) = 0;

    virtual void writeElemSymTensorField(UnsMesh* mesh,
                                         int plnum,
                                         int varId,
                                         string& name,
                                         CSymTensor symtensor) = 0;

    virtual void writeNodeScalarField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      Real* evar) = 0;

    virtual void writeNodeVectorField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      string& name,
                                      CVector evec) = 0;

    virtual void writeNodeSymTensorField(UnsMesh* mesh,
                                         int plnum,
                                         int varId,
                                         string& name,
                                         CSymTensor symtensor) = 0;

    virtual void writeSurfScalarField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      Sideset& surf,
                                      string& name,
                                      Real* svar) = 0;

    virtual void writeSurfVectorField(UnsMesh* mesh,
                                      int plnum,
                                      int varId,
                                      Sideset& surf,
                                      string& name,
                                      CVector svec) = 0;

  protected:

    string m_fname;

  private:

    //! Don't permit copy or assignment operators
    //@{
    plotWriter(const plotWriter&);
    plotWriter& operator=(const plotWriter&);
    //@}
};

}

#endif // plotWriter_h
