//******************************************************************************
//! \file    src/IO/gmvWriter.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   GMV Plot Writer
//******************************************************************************
#ifndef gmvWriter_h
#define gmvWriter_h

#include <plotWriter.h>

namespace Hydra {

class gmvWriter : public plotWriter {

  public:

    //! \name Constructor/Destructor
    //@{
             gmvWriter(string fname) {m_fname = fname;}
    virtual ~gmvWriter() {}
    //@}

    // open, close, writeMesh are not used for GMV files
    virtual void open() {}

    virtual void close() {}

    virtual void writeFieldHeader();

    virtual void writeFieldFooter();

    virtual void writeMesh(UnsMesh* /*mesh*/, Control* /*control*/) {}

    virtual void writeTimeStamp(int plnum, int step, Real time,
                                Control* control, UnsMesh* mesh);

    // open, close, writeFieldNames not used for GMV files
    virtual void writeFieldNames(Control* /*control*/,
                                 vector<string> /*elemFieldName*/,
                                 vector<string> /*nodeFieldName*/,
                                 vector<string> /*surfFieldName*/) {}

     virtual void writeElemScalarField(UnsMesh* mesh,
                                       int plnum,
                                       int varId,
                                       string& name,
                                       Real* evar);

     virtual void writeElemVectorField(UnsMesh* mesh,
                                       int plnum,
                                       int varId,
                                       string& name,
                                       CVector evec);

     virtual void writeElemSymTensorField(UnsMesh* mesh,
                                          int plnum,
                                          int varId,
                                          string& name,
                                          CSymTensor symtensor);

     virtual void writeNodeScalarField(UnsMesh* mesh,
                                       int plnum,
                                       int varId,
                                       string& name,
                                       Real* nvar);

     virtual void writeNodeVectorField(UnsMesh* mesh,
                                       int plnum,
                                       int varId,
                                       string& name,
                                       CVector nvec);

     virtual void writeNodeSymTensorField(UnsMesh* mesh,
                                          int plnum,
                                          int varId,
                                          string& name,
                                          CSymTensor symtensor);

     virtual void writeSurfScalarField(UnsMesh* mesh,
                                       int plnum,
                                       int varId,
                                       Sideset& surf,
                                       string& name,
                                       Real* svar);

     virtual void writeSurfVectorField(UnsMesh* mesh,
                                       int plnum,
                                       int varId,
                                       Sideset& surf,
                                       string& name,
                                       CVector svec);

  private:

    //! Don't permit copy or assignment operators
    //@{
    gmvWriter(const gmvWriter&);
    gmvWriter& operator=(const gmvWriter&);
    //@}

    //! \name GMV ASCII plot file output methods
    //@{
    void writeVars(bool head);

    void writeVelocity(UnsMesh* mesh, DataIndex VELX, DataIndex VELY);

    ofstream  plotf;  //!< State plot file
};

}
#endif // gmvWriter_h
