//******************************************************************************
//! \file    src/IO/restartReader.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   Abstract base class for restart readers
//******************************************************************************
#ifndef restartReader_h
#define restartReader_h

#include <string>

namespace Hydra {

// Forward declarations

class Control;
class UnsMesh;

//! Abstract base class for restart readers
class restartReader {

  public:

    //! \name Constructor/Destructor
    //@{
             restartReader() {}
    virtual ~restartReader() {}
    //@}

    virtual void open()  = 0;

    virtual void close() = 0;

    virtual void readDump(Control* control, UnsMesh* mesh) = 0;

  protected:

    string m_fname;

  private:

    //! Don't permit copy or assignment operators
    //@{
    restartReader(const restartReader&);
    restartReader& operator=(const restartReader&);
    //@}
};

}

#endif // restartReader_h
