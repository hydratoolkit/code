//******************************************************************************
//! \file    src/IO/partExodusReader.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   Partitioned Exodus-II reader
//******************************************************************************
#ifndef partExodusReader_h
#define partExodusReader_h

#include <fstream>

#include <Control.h>
#include <Element.h>
#include <ElBlock.h>
#include <UnsMesh.h>
#include <TokenStream.h>
#include <meshReader.h>

namespace Hydra {

// Exodus-II Reader that uses a partitioned file
class partExodusReader: public meshReader {

  public:

    //! \name Constructor/Destructor
    //@{
             partExodusReader(string meshname, string partname);
    virtual ~partExodusReader() {}
    //@}

    virtual void open();

    virtual void close();

    virtual void readMesh(UnsMesh* mesh);

  protected:

  private:

    //! Don't permit copy or assignment operators
    //@{
    partExodusReader(const partExodusReader&);
    partExodusReader& operator=(const partExodusReader&);
    //@}

    //! \name Exodus-II/Nemesis read functions
    //@{
    int  mapElemTypes(ElBlock* eblock, int Neblock, int* elem_map, int Nel,
                    int* Nel_tri3, int* Nel_quad4, int* Nel_tet4,int* Nel_hex8);

    void partitionCheck();

    void readConnectivity(UnsMesh* mesh);

    void readCoordinateNames(UnsMesh* mesh);

    void readCoordinates(UnsMesh* mesh);

    void readElTypes(int ndim, int Neblock, int Nel, ElBlock* eblock,
                        int* Nel_tri3, int* Nel_quad4,
                        int* Nel_tet4, int* Nel_hex8);

    void readHeader(UnsMesh* mesh);

    void readNodeset(UnsMesh* mesh);

    void readNodeVar(UnsMesh* mesh, int plnum, Real time,
                     vector<DataIndex> nodevar);

    void readPartComm(UnsMesh* mesh);

    void readSideset(UnsMesh* mesh);
    //@}

    int m_exofh;       //!< Exodus  file handle for the input mesh file
    int m_nemfh;       //!< Nemesis file handle for the partition  file

    string m_meshname; //<! Mesh file name
    string m_partname; //<! Partition file name

};

}
#endif // partExodusReader_h
