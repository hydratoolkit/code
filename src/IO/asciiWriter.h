//******************************************************************************
//! \file    src/IO/asciiWriter.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   Derived plot writer for ASCII output -- really just meshes
//******************************************************************************
#ifndef asciiWriter_h
#define asciiWriter_h

#include <plotWriter.h>

namespace Hydra {

//! ASCII plot writer -- primarily for mesh output
class asciiWriter : public plotWriter {

  public:

    //! \name Constructor/Destructor
    //@{
             asciiWriter(string fname) {m_fname = fname;}
    virtual ~asciiWriter() {}
    //@}

    virtual void open();

    virtual void close();

    virtual void writeFieldHeader() {}

    virtual void writeFieldFooter() {}

    virtual void writeMesh(UnsMesh* mesh);

    virtual void writeMesh(UnsMesh* /*mesh*/, Control* /*control*/) {}

    virtual void writeTimeStamp(int /*plnum*/, int /*step*/, Real /*time*/,
                                Control* /*control*/, UnsMesh* /*mesh*/) {}

    // open, close, writeFieldNames not used
    virtual void writeFieldNames(Control* /*control*/,
                                 vector<string> /*elemFieldName*/,
                                 vector<string> /*nodeFieldName*/,
                                 vector<string> /*surfFieldName*/) {}

    virtual void writeElemScalarField(UnsMesh* /*mesh*/,
                                      int /*plnum*/,
                                      int /*varId*/,
                                      string& /*name*/,
                                      Real* /*evar*/) {}

     virtual void writeElemVectorField(UnsMesh* /* mesh */,
                                       int /* plnum */,
                                       int /* varId */,
                                       string& /* name */,
                                       CVector /* evec */) {}

     virtual void writeElemSymTensorField(UnsMesh* /* mesh */,
                                          int /* plnum */,
                                          int /* varId */,
                                          string& /* name */,
                                          CSymTensor /* symtensor */) {}

     virtual void writeNodeScalarField(UnsMesh* /* mesh */,
                                       int /* plnum */,
                                       int /* varId */,
                                       string& /* name */,
                                       Real* /* nvar */) {}

     virtual void writeNodeVectorField(UnsMesh* /* mesh */,
                                       int /* plnum */,
                                       int /* varId */,
                                       string& /* name */,
                                       CVector /* nvec */) {}

     virtual void writeNodeSymTensorField(UnsMesh* /* mesh */,
                                          int /* plnum */,
                                          int /* varId */,
                                          string& /* name */,
                                          CSymTensor /* symtensor */) {}

     virtual void writeSurfScalarField(UnsMesh* /* mesh */,
                                       int /* plnum */,
                                       int /* varId */,
                                       Sideset& /* surf */,
                                       string& /* name */,
                                       Real* /* svar */) {}

     virtual void writeSurfVectorField(UnsMesh* /* mesh */,
                                       int /* plnum */,
                                       int /* varId */,
                                       Sideset& /* surf */,
                                       string& /* name */,
                                       CVector /* svec */) {}

    //! \name ASCII msh write functions - public for use in utilities
    //@{
    void writeConnectivity(UnsMesh* mesh);

    void writeElemConnectivity(int& gid, string& name, Element* ec);

    void writePoly2DConnectivity(int& gid, string& name, Element* ec);

    void writePoly3DConnectivity(int& /* gid */,
                                 string& /* name */,
                                 Element* /* ec */);

    void writeCoordinates(UnsMesh* mesh);

    void writeHeader(UnsMesh* mesh);

    void writeNodeset(UnsMesh* mesh);

    void writeSideset(UnsMesh* mesh);
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    asciiWriter(const asciiWriter&);
    asciiWriter& operator=(const asciiWriter&);
    //@}

    ofstream  meshf;  //!< ASCII Mesh file (output)
};

}
#endif // asciiWriter_h
