//******************************************************************************
//! \file    src/IO/OutputDelegate.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   Output delegate for output requests, aka, call-back functions
//******************************************************************************
#ifndef OutputDelegate_h
#define OutputDelegate_h

#include <OutputDelegateKey.h>

namespace Hydra {

// Typedef for function used to call a delegate, i.e., a callback
typedef void (*OutputDelegateWrapperCall)(void* obj,
                                          const OutputDelegateKey& key,
                                          int plnum,
                                          int varId);

// Macros used to define delegate member function and alias.
#define OUTPUT_DELEGATE_DECL(CLASS, METHOD, NAME) \
void NAME(void* obj, const OutputDelegateKey& key, int plnum, int varId);

#define OUTPUT_DELEGATE_IMPL(CLASS, METHOD, NAME) \
void NAME(void* obj, const OutputDelegateKey& key, int plnum, int varId)\
{ \
  reinterpret_cast<CLASS*>(obj)->METHOD(key, plnum, varId); \
}

//! \brief Output delegate class
//! This class is an implementation of a delegate specialized for output
//! request processing.  The function that the delegate calls must be a class
//! member function that accepts an output request as its only argument.  The
//! macro DEFINE_OUTPUT_DELEGATE_CALL must be present in the .C file for the
//! class that is having its member function delegated.  The third argument
//! "NAME" is the name of the function to be passed as the "func" argument in the
//! delegate class constructor or the "set" method.
class OutputDelegate
{
  public:

    //! \name Constructor/Destructor
    //@{
             OutputDelegate(): m_ptr(0), m_func(0) {}
             OutputDelegate(void* ptr, OutputDelegateWrapperCall func):
                            m_ptr(ptr), m_func(func) {}
    virtual ~OutputDelegate() {}
    //@}

    void set(void* ptr, OutputDelegateWrapperCall func) {
      m_ptr = ptr; m_func = func;
    }

    bool isNull() const { return m_ptr == 0 || m_func == 0; }

    void exec(const OutputDelegateKey& key, int plnum, int varId);

  private:

    //! Don't permit copy or assignment operators
    //@{
    OutputDelegate(const OutputDelegate&);
    OutputDelegate& operator=(const OutputDelegate&);
    //@}

    void* m_ptr;
    OutputDelegateWrapperCall m_func;

};

}

#endif
