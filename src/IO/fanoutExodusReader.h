//******************************************************************************
//! \file    src/IO/fanoutExodusReader.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   Exodus/Nemesis fanout reader
//******************************************************************************
#ifndef fanoutExodusReader_h
#define fanoutExodusReader_h

#include <fstream>
#include <map>
#include <set>

#include <Control.h>
#include <Element.h>
#include <UnsMesh.h>
#include <TokenStream.h>
#include <meshReader.h>

namespace Hydra {

#define EXODUS_FANOUTSIZE 1024 //<! Define the strip-mine size for fanout

//! Fanout Exodus-II Reader
class fanoutExodusReader: public meshReader {

  public:

    //! \name Constructor/Destructor
    //@{
             fanoutExodusReader(string meshname);
    virtual ~fanoutExodusReader();
    //@}

    virtual void open();

    virtual void close();

    virtual void readMesh(UnsMesh* mesh);

  protected:


  private:

    //! Don't permit copy or assignment operators
    //@{
    fanoutExodusReader(const fanoutExodusReader&);
    fanoutExodusReader& operator=(const fanoutExodusReader&);
    //@}

    //! \name Exodus-II/Nemesis read functions
    //@{
    void readConnectivity(UnsMesh* mesh);

    void setupMaps(UnsMesh* mesh);

    void readCoordinateNames(UnsMesh* mesh);

    void readCoordinates(UnsMesh* mesh);

    void readElTypes(UnsMesh* mesh);

    void readHeader(UnsMesh* mesh);

    void readNodeset(UnsMesh* mesh);

    void readNodeVar(UnsMesh* /*mesh*/, int /*plnum*/, Real /*time*/,
                     vector<DataIndex> /*nodevar*/) {
      cout << "!!! fanoutExodusReader::readNodeVar not implemented !!!" << endl;
    }

    void readSideset(UnsMesh* mesh);
    //@}

    int m_pid;         //!< processor rank
    int m_Nproc;       //!< No. of processors
    int m_exofh;       //!< Exodus/Nemesis file handle for the input mesh file
    string m_meshname; //!< Mesh file name

    ElBlock* eblock;

    map<int,int> m_node2local;
    set<int> uniqueNodes;
};

}
#endif // fanoutExodusReader_h
