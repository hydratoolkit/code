//******************************************************************************
//! \file    src/IO/fileIO.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:34 2011
//! \brief   The fileIO class manages all file-based I/O, e.g., opens files, etc.
//******************************************************************************
#ifndef fileIO_h
#define fileIO_h

#include <fstream>
#include <map>
#include <vector>
#include <stdio.h>

#include <Control.h>
#include <Element.h>
#include <OutputDelegate.h>
#include <UnsMesh.h>
#include <TokenStream.h>

namespace Hydra {

// Forward declarations
class historyWriter;
class plotWriter;
class restartReader;
class restartWriter;
class catalystAdaptor;

enum FileType {EXE=0,         // Executable name
               MESH,          // Input mesh file
               CONTROL,       // Input control file
               ASCII_OUTPUT,  // File for ASCII data echo
               PLOT,          // File for instantaneous plot data
               PLOTSTAT,      // File for statistics plot data
               HISTORY,       // File for nodal instantaneous time-history data
               GLOBAL,        // File for global time-history data
               CONVERGENCE,   // File for convergence-history data
               RESTARTW,      // File to write restarts to
               RESTARTR,      // File to read restarts from
               CATALYST,      // Catalyst output
               NUM_FILETYPES  // Number of file types
};

enum OutputDelegateType {FIELD_DELEGATE = 0,   // Field output
                         HISTORY_DELEGATE,     // Time-history output
                         SURFACE_DELEGATE      // Surface output
};

//! The fileIO is used to handle all I/O functions -- or at least most.
class fileIO {

  public:

    //! \name Constructor/Destructor
    //@{
             fileIO();
    virtual ~fileIO();
    //@}

    //! \name Delegate map -- all registered output delegates
    typedef map<OutputDelegateKey, OutputDelegate*> DelegateMap;

    //! \name File and Filename management
    //@{
    void closeFiles(UnsMesh* mesh, Control* control);

    string* getFnames() {return fnames;}

    const string  getFname(FileType type) {return fnames[type];}

    void setFname(FileType type, char* name) {fnames[type] = name;}

    void setFname(FileType type, string name) {fnames[type] = name.c_str();}

    void concatFname(FileType type);

    void echoFnames(ostream& ofs);

    void setupFiles(Control* control);
    //@}

    //! \name Restart file management (for reading)
    //@{
    void closeRestart();

    void openRestart(Control* control);
    //@}

    //! \name Dump file management (for writing)
    //@{
    void closeDump();

    void openDump(Control* control);
    //@}

    //! \name Global data file  -- where the kinetic energy, div(U) is written
    //@{
      void closeGlob() {globf.close();}
      void closeConv() {m_convf.close();}

      ofstream& getGlob() {return globf;} //!< Get global data file
      ofstream& getConv() {return m_convf;} //!< Get convergence data file

      ofstream& openGlob(int append); //!< Open global data file
      ofstream& openConv(int append); //!< Open convergence data file
    //@}

    //! \name Generic file manipulation
    //@{
    void closeOut() {outf.close();}

    void openOut() {outf.open(fnames[ASCII_OUTPUT].c_str(), ios::app);}

    ofstream& getOut() {return outf;}

    ifstream& inputStream(FileType type);

    ofstream& outputStream(FileType type);
    //@}

    //! \name Mesh file operations
    //@{
    void readMesh(UnsMesh *mesh, Control *control);
    //@}

    //! \name Top level plot variable output driver
    //@{
    void closePlotFiles(Control* control);

    void openPlotFiles(Control* control, FileType type);

    void writeMesh(UnsMesh *mesh, Control *control);

    void writePlot(int& plnum, int step, Real time,
                   Control* control, UnsMesh* mesh);

    void writeHistory(int& thnum, int step, Real time,
                      Control* control, UnsMesh* mesh);

    void writeRestart(Control* control, UnsMesh* mesh);

    void readRestart(Control* control, UnsMesh* mesh);

    //@}

    //! \name Time history file & parsing operations
    //@{
    int iscomment(char *cdat);

    void openHistoryFiles(UnsMesh* mesh, Control* control);

    void closeHistoryFiles(Control* control);
    //@}

    // Write a line with a prefix and break it up over multiple lines
    void writeLine(ostream& output, const char* prefix, const char* msg,
                   int lineWidth);

    // Write error messages
    void writeError(const char* format, ...);

    // Write warning messages
    void writeWarning(const char* format, ...);

    // Test a file to find out what format it is
    int testFile(const char* file_name);

    //! \name Output Delegates
    //@{
    void echoOutputRequests(ostream& ofs, Control* control);

    OutputDelegateKey registerDelegate(const string& variable,
                                       VariableCentering centering,
                                       VariableType vtype,
                                       OutputDelegateType otype,
                                       void* cptr,
                                       OutputDelegateWrapperCall f,
                                       bool regvar = false);

    void setupOutputDelegates(Control* control, UnsMesh* mesh, FileType type);

    void writeElemScalarField(UnsMesh* mesh, int plnum, int varId,
                              string& name, Real* var);

    void writeElemVectorField(UnsMesh* mesh, int plnum, int varId,
                              string& name, CVector vec);

    void writeElemSymTensorField(UnsMesh* mesh, int plnum, int varId,
                                 string& name, CSymTensor symtensor);

    void writeNodeScalarField(UnsMesh* mesh, int plnum, int varId,
                              string& name, Real* var);

    void writeNodeVectorField(UnsMesh* mesh, int plnum, int varId,
                              string& name, CVector vec);

    void writeNodeSymTensorField(UnsMesh* mesh, int plnum, int varId,
                                 string& name, CSymTensor symtensor);

    void writeSurfScalarField(UnsMesh* mesh, int plnum, int varId,
                              Sideset& surf, string& name, Real* var);

    void writeSurfVectorField(UnsMesh* mesh, int plnum, int varId,
                              Sideset& surf, string& name, CVector vec);

    void writeFieldNames(Control* control, UnsMesh* mesh);

    void writeHistoryData(UnsMesh* mesh, int plnum, int varId,
                          const OutputDelegateKey& key, int Nval, Real* var);

    void writeHistoryData(UnsMesh* mesh, int plnum, int varId,
                          const OutputDelegateKey& key, int Nval, Vector vec);

    void writeSurfaceHistoryData(UnsMesh* mesh, int thnum, int varId,
                                 const OutputDelegateKey& key,
                                 int Nvar, Real* var);

    void selectInstPlotOutput();  //<! Switch to instantaneous plot output
    void selectStatPlotOutput();  //<! Switch to statistics plot output

    set<OutputDelegateKey> getElemFieldKey() {return m_elemFieldKey;}
    set<OutputDelegateKey> getNodeFieldKey() {return m_nodeFieldKey;}
    set<OutputDelegateKey> getSurfFieldKey() {return m_surfFieldKey;}
    //@}

    void initializeCatalyst(Control* control);
    // This sets up everything to execute any Catalyst pipelines.
    void executeCatalystPipelines(int step, Real time, UnsMesh* mesh);

  private:

    //! Don't permit copy or assignment operators
    //@{
    fileIO(const fileIO&);
    fileIO& operator=(const fileIO&);
    //@}

    int m_pid;
    int m_Nproc;

    //! \name Private data for file management
    //@{
    string fnames[NUM_FILETYPES];  //<! File names - 1 per file type
    ifstream  cntlf;               //!< Control file
    ofstream  outf;                //!< Output file
    ofstream  globf;               //!< Global data file
    ofstream  m_convf;             //!< Convergence data file
    int m_exeCntrlStep;            //!< Step for checking execution control file
    //@}

    //! \name Generic readers/writers
    //@{
    plotWriter* m_plotInstWriter;       //<! Ptr to instantaneous plotWriter
    plotWriter* m_plotStatWriter;       //<! Ptr to statistics plotWriter
    historyWriter* m_histInstWriter;    //<! Ptr to instantaneous historyWriter

    //! These pointers point to either inst or stat keys at a time
    plotWriter* m_plotWriter;           //<! Ptr to current plotWriter
    historyWriter* m_histWriter;        //<! Ptr to current historyWriter
    FileType    m_plotType;             //<! Enum of the current plot type
    restartWriter* m_restartWriter;     //<! restart writer
    restartReader* m_restartReader;     //<! restart reader
    //@}

    //! \name Delegate map -- all registered output delegates
    DelegateMap m_delegates;

    //! Delegate keys for various output
    set<OutputDelegateKey> m_elemFieldInstKey; //!< Elem-centered inst field key
    set<OutputDelegateKey> m_nodeFieldInstKey; //!< Node-centered inst field key
    set<OutputDelegateKey> m_surfFieldInstKey; //!< Surface inst output key

    set<OutputDelegateKey> m_elemFieldStatKey; //!< Elem-centered stat field key
    set<OutputDelegateKey> m_nodeFieldStatKey; //!< Node-centered stat field key
    set<OutputDelegateKey> m_surfFieldStatKey; //!< Surface stat key

    //! These pointers point to either inst or stat keys at a time
    set<OutputDelegateKey> m_elemFieldKey; //!< Ptr to current elem field key
    set<OutputDelegateKey> m_nodeFieldKey; //!< Ptr to current node field key
    set<OutputDelegateKey> m_surfFieldKey; //!< Ptr to current surf key
    //-------------------------------------------------------------------------

    set<OutputDelegateKey> m_elemHistInstKey; //!< Elem-centered inst hist key
    set<OutputDelegateKey> m_nodeHistInstKey; //!< Node-centered inst hist key
    set<OutputDelegateKey> m_surfHistInstKey; //!< Surface inst history key

    // No HistStat output and no need to switch between inst and stat Hist -JB
    //-------------------------------------------------------------------------

    //! \name ParaView Catalyst adaptor object
    catalystAdaptor* m_catalyst;

    //! Delegate keys for Catalyst output output
    set<OutputDelegateKey> m_elemFieldCatalystKey; //!< Elem-centered keys
    set<OutputDelegateKey> m_nodeFieldCatalystKey; //!< Node-centered keys
    set<OutputDelegateKey> m_surfFieldCatalystKey; //!< Surface keys
};

}

#endif  // fileIO_h
