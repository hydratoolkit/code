//******************************************************************************
//! \file    src/NLSolvers/NonLinearSolver.h
//! \author  Robert N. Nourgaliev
//! \date    Thu Oct 03 12:00:00 2012
//! \brief   Virtual base class for all non-linear solvers
//******************************************************************************
#ifndef NonLinearSolver_h
#define NonLinearSolver_h

#include <NonLinearSolverFlags.h>

namespace Hydra {

class NonLinearSolver {

  public:

             NonLinearSolver();
    virtual ~NonLinearSolver();

    //**************************************************************************
    //! \name Virtual Non-Linear Solver Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of solver being solved
    //! with the framework.
    //@{
    //! Setup -- implemented for each specific non-linear solver
    virtual void setup()=0;

    //! Finalize -- implemented for each specific non-linear solver
    virtual void finalize()=0;

    //! Initialize -- implemented for each specific non-linear solver
    virtual void initialize()=0;

    //! Solve -- implemented for each specific non-linear solver
    virtual void solve()=0;
    //@}

  protected:

  private:

    //! Don't permit copy or assignment operators
    //@{
    NonLinearSolver(const NonLinearSolver&);
    NonLinearSolver& operator=(const NonLinearSolver&);
    //@}

};

}

#endif
