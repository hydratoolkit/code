//******************************************************************************
//! \file    src/NLSolvers/SNESSolver.h
//! \author  Robert N. Nourgaliev
//! \date    Thu Oct 03 12:00:00 2012
//! \brief   Virtual base class for all PETSc SNES based non-linear solvers
//******************************************************************************
#ifndef SNESSolver_h
#define SNESSolver_h

#include <HydraTypes.h>
#include <NonLinearSolverFlags.h>

#include <petsc.h>
#include <petscsnes.h>

namespace Hydra {

class SNESSolver {

  public:

    //! \name Constructor/Destructor
    //@{
             SNESSolver(int &iters, const string &name);
    virtual ~SNESSolver();
    //@}

    //! Execute non-linear solver
    //!  \param[in] X Solution vector
    //!  \return 0 if everything is fine and \f$\neq 0\f$ on failure
    virtual PetscErrorCode nonLinearSolve(Vec& X);

    //! Dump solver data structures to stdout
    virtual PetscErrorCode dump() const;

  protected:

    //**************************************************************************
    //! \name Configure/Setup routines
    //@{
    //! Get Petsc option with prefix
    //!   \param[in] option Petsc option
    //!   \return Petsc option with prefix
    const char* getOptWithPrefix(const string& option) {
      m_petscOption = "-" + m_prefix + option;
      return m_petscOption.c_str();
    }

    //! Configure SNES non-linear solver
    //!   \return 0 if everything is fine and \f$\neq 0\f$ if some
    //!   failure happened
    PetscErrorCode configureSNES();

    //! Set function for residual evaluation
    //!  \return 0 if everything is fine and \f$\neq 0\f$ if some
    //!  failure happened
    virtual PetscErrorCode setResidualFunction()=0;

    //! Setup preconditioner
    //!  \param[in] pc           Reference to PC object
    //!  \return 0 if everything is fine and \f$\neq 0\f$ if some
    //!  failure happened
    virtual PetscErrorCode setupPC(PC& pc)=0;
    //@}

    const string m_prefix; //!< Petsc prefix for options

    SNES m_snes; //!< Petsc's non-linear solver

    unsigned short m_preconditioning;   //!< Preconditioning option
    unsigned short m_snes_type;         //!< Type of SNES
    unsigned short m_mf_type;           //!< Type of matrix-free evaluation
    unsigned short m_orthogonalization; //!< Orthogonalization method
    unsigned short m_refinement;        //!< Refinement of orthogonalization

    Real m_rtol_snes;  //!< Relative tolerance of non-linear iterations
    Real m_atol_snes;  //!< Absolute tolerance of non-linear iterations
    Real m_stol_snes;  //!< Solution tolerance of non-linear iterations
    int  m_itmax_snes; //!< Maximum number of non-linear iterations
    int  m_max_funcs;  //!< Maximum number of non-linear function evaluations
    int  m_max_gmres_rstrt; //!< Maximum number of GMRES iterations before restart

    int  m_itmax_ksp; //!< Maximum number of linear iterations
    Real m_rtol_ksp;  //!< Relative tolerance of linear iterations
    Real m_atol_ksp;  //!< Absolute tolerance of linear iterations
    Real m_dtol_ksp;  //!< Divergence parameter of linear iterations

    bool m_ew;         //!< Flag to activate Eisenstad-Walker technique
    bool m_view;       //!< Flag to activate snes_view
    bool m_monitor;    //!< Flag to activate snes_monitor
    bool m_monitor_tr; //!< Flag to activate ksp_monitor_true_residual

    Real m_Erel;  //!< Relative error parameter for matrix-free methods
    Real m_Umin;  //!< Tuning parameter for matrix-free methods

    int& m_newtonIters; //!< The number of non-linear iterations taken

    int m_ksp_his_store;  //!< The size of array to store the linear iterations
    int m_snes_his_store; //!< The size of array to store the non-linear iterations

  private:

    //! Don't permit copy or assignment operators
    //@{
    SNESSolver(const SNESSolver&);
    SNESSolver& operator=(const SNESSolver&);
    //@}

    string m_petscOption;  //!< String to construct prefix + option

};

}

#endif
