//******************************************************************************
//! \file    src/Utilities/norm/norm_data.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 13:01:18 2011
//! \brief   Basic data for norm utility
//******************************************************************************
#ifndef NORM_DATA_H
#define NORM_DATA_H

#include <iostream>
#include <string>

#include <HydraTypes.h>

namespace Hydra {

// Locations for variables that can be interpolated
enum varLocale {ELEMENT=0, NODE, NUM_LOC};

class NormData {
  public:
             NormData();
    virtual ~NormData();

    void readInput();
    void echoInput();
    void checkInput(Mesh* mesh);
    void setComment(const string ucomment) {comment = ucomment;}

    void getMinMax(Mesh* mesh, int &minloc, Real& minval, int& maxloc, Real& maxval);

    void getNodeValue(Mesh* mesh, Real& val, int& ndloc);

    varLocale getLocale() {return loc;}
    int getVid() {return var_id;}
    int getTid() {return time_plane;}


  private:
    Real *x;
    Real *y;
    Real *z;
    Real *data;

    varLocale loc;
    int       var_id;     // 0-based
    int       time_plane; // 0-based
    int       node;
    string    comment;
    string    var_name;
};

}
#endif // NORM_DATA_H
