//******************************************************************************
//! \file    src/Utilities/norm/exodus_io.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 13:01:18 2011
//! \brief   Exodus-II prototypes
//******************************************************************************
#ifndef EXODUS_IO_H
#define EXODUS_IO_H

#include <vector>

#include "norm_data.h"

using namespace Hydra;

int openExoFile( const string fname, const string file_mode );

void closeExoFile(int exofh);

void readExoHeader(int exofh, Mesh* mesh);

void readExoCoord(int exofh, Mesh* mesh);

void readExoConn(int exofh, Mesh *mesh);

void readExoVnames(int exofh, Mesh* mesh);

void readExoTimes(int exofh, Mesh* mesh);

void readExoVar(int exofh, Mesh* mesh, NormData* nd);

void readExoEvar(int exofh, Mesh* mesh, NormData* nd);

void readExoNvar(int exofh, Mesh* mesh, NormData* nd);

#endif // EXODUS_IO_H
