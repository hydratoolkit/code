//******************************************************************************
//! \file    src/Utilities/channel_mesh/channel_mesh.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:59:30 2011
//! \brief   Simple channel mesh generator for scaling studies
//******************************************************************************
#ifndef CHANNEL_MESH_H
#define CHANNEL_MESH_H

#include <HydraTypes.h>

void finalize();

void initialize(int argc, char **argv);

void parseArgv(int argc, char *argv[], string* fnames, int* numElements);

void setup();

namespace Hydra {

enum MeshFileType {EXE_FILE=0,    // Executable name
                   INPUT_FILE,    // Input file describing mesh
                   EXODUSII_FILE, // Input Exodus-II mesh file
                   NUM_FILES      // Number of files
};

enum Direction {
  X = 0,
  Y = 1,
  Z = 2
};

struct StructuredMesh {
};

// Forward declaration
class UnsMesh;

class ChannelMesh {

  public:

    //! \name Constructor/Destructor
    //@{
             ChannelMesh(Hydra::UnsMesh& mesh, int* numElements);
    virtual ~ChannelMesh() {}
    //@}

    void genCoordinates(int exofh);

    void genConnectivity(int exofh);

    void genSideSets(int exofh);

  private:

    //! Don't permit copy or assignment operators
    //@{
    ChannelMesh(const ChannelMesh&);
    ChannelMesh& operator=(const ChannelMesh&);
    //@}

    UnsMesh& m_mesh;

    int Nel_tot;
    int Nel[3];

    int Nnp_tot;
    int Nnp[3];

    Vector xmin;
    Vector xmax;
};

}

#endif // CHANNEL_MESH_H
