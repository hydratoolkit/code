project(channel_mesh C)

# Basic configuration
add_definitions()

# Include configuration/headers/libraries common to all code
INCLUDE(${HYDRA_SOURCE_DIR}/HydraCommon.cmake)
INCLUDE(${HYDRA_SOURCE_DIR}/HydraDefine.cmake)

include_directories(${HYDRA_SOURCE_DIR}/Base
                    ${HYDRA_SOURCE_DIR}/Control
                    ${HYDRA_SOURCE_DIR}/DataMesh
                    ${HYDRA_SOURCE_DIR}/IO
                    ${HYDRA_SOURCE_DIR}/Main
                    ${HYDRA_SOURCE_DIR}/Materials
                    ${HYDRA_SOURCE_DIR}/Utilities/channel_mesh
)

add_executable(channel_mesh channel_mesh.C)

# Force static link on cray
IF(CRAY)
  set_property(TARGET channel_mesh PROPERTY LINK_SEARCH_START_STATIC 1)
ENDIF()

target_link_libraries(channel_mesh
                      DataMesh
                      IO
                      Materials
                      Base
                      Control
                      ${PETSC_LIBRARY}
                      ${PARMETIS_LIBRARY}
                      ${METIS_LIBRARY}
                      ${EXO_C_LIBRARY}
                      ${NEMESIS_LIBRARY}
                      ${NETCDF_LIBRARY}
                      ${HDF5HL_LIBRARY}
                      ${HDF5_LIBRARY}
                      ${Z_LIBRARY}
                      ${MPI_CXX_LIBRARIES}
                      ${MPI_mpi_LIBRARY}
                      ${MPI_mpi_cxx_LIBRARY}
                      ${MPI_mpi_mpifh_LIBRARY}
                      ${MPI_mpi_usempi_ingore_tkr_LIBRARY}
                      ${MPI_mpi_usempif08_LIBRARY}
                      ${PMI_LIBRARY}
                      ${PORTALS_LIBRARY}
)

INSTALL(TARGETS channel_mesh
        RUNTIME DESTINATION bin COMPONENT Runtime
        LIBRARY DESTINATION lib COMPONENT Runtime
        ARCHIVE DESTINATION lib COMPONENT Development
)

