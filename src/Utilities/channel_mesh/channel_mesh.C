//******************************************************************************
//! \file    src/Utilities/channel_mesh/channel_mesh.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:59:30 2011
//! \brief   Simple channel mesh generator for scaling studies
//******************************************************************************
#include <cstdlib>
#include <cstring>
#include <time.h>

#include <cassert>
#include <string>
#include <iostream>
#include <fstream>
#include <set>
#include <cmath>

using namespace std;

// Required for local block-based write of big meshes
#include <ne_nemesisI.h>
#include <exodusII.h>

#include <fileIO.h>
#include <UnsMesh.h>
#include <MPIWrapper.h>
#include <PrintUtil.h>
#include <asciiReader.h>
#include <exodusWriter.h>
#include <channel_mesh.h>
#include <HydraConfig.h>

using namespace Hydra;

#define CONNECTIVITY_STRIPMINE 4096
#define COORDINATES_STRIPMINE  8192

int main(int argc, char* argv[])
{
  // Number of elements to create in x, y, z
  int numElements[3] = {200, 32, 100};

  // Filenames for IO class
  string fnames[NUM_FILES];

  // Initialize the messaging system
  g_comm = new MPIWrapper(&argc, &argv);

  if (g_comm->getNproc() > 1) {
    cerr << endl
         << " Channel mesh was not designed to take advantage of MPI.\n"
         << " Please restart the application with only one processor."
         << endl;
    g_comm->finalMP();
  }

  // Create an I/O object
  PrintUtil pu(cout);
  pu.printBanner("CHANNEL",
                 HYDRA_VERSION,
                 HYDRA_OSNAME,
                 HYDRA_COMPILER,
                 HYDRA_BUILD_TYPE,
                 HYDRA_BUILD_DATE,
 " ***************************************************************************");

  // Crack the command-line arguments
  parseArgv(argc, argv, fnames, numElements);

  // Construct the mesh object which holds all field variables too
  UnsMesh mesh;

  // For now, hardwire the structured channel mesh
  ChannelMesh cmesh(mesh, numElements);

  // Echo the mesh parameters, analysis options, etc.
  mesh.echoMeshParms(cout);

  // Create the exodus file -- always write an HDF5 file
  exodusWriter mw(fnames[EXODUSII_FILE], exodusWriter::OUTPUT, EXODUSII_HDF5);
  mw.open();

  // Exodus file handle for strip-mined writes
  int exofh = mw.getFileHandle();

  // Write the header
  cout << "\tWriting the header ... " << endl;
  mw.writeHeader(&mesh);
  cout << "\t... done" << endl;

  // Fill the coordinates & write
  cout << "\tWriting the coordinates ... " << endl;
  cmesh.genCoordinates(exofh);
  cout << "\t... done" << endl;

  // Fill the connectivity and write block-by-block
  cout << "\tWriting connectivity ..." << endl;
  cmesh.genConnectivity(exofh);
  cout << "\t... done" << endl;

  // Fill the sidesets
  cout << "\tWriting sidesets ..." << endl;
  cmesh.genSideSets(exofh);
  cout << "\t... done" << endl;

  mw.close();
  cout << "\tExodus file closed" << endl;

  // Get a reading on memory
  mesh.reportMemory(cout);

  // Terminate the message passing system
  g_comm->syncup();
  g_comm->finalMP();

  return 0;
}

void parseArgv(int argc, char *argv[], string* fnames, int* numElements)
/******************************************************************************
Routine: parseArgv - parse command line arguments and setup filenames
Author : Mark A. Christon
*******************************************************************************/
{
  bool usefail = false;
  bool oset    = false;

  fnames[EXE_FILE] = argv[0];

  // Parse the command line the slow, but sure way
  usefail = false;
  for (int i=1; i<argc; i++) {

    // Parse Nx
    if (!strcmp( argv[i], "-x" )) {
      if( i+1 < argc ) {
        numElements[0] = atoi(argv[i+1]);
      } else {
        usefail = true;
      }
    }

    // Parse Ny
    if (!strcmp( argv[i], "-y" )) {
      if( i+1 < argc ) {
        numElements[1] = atoi(argv[i+1]);
      } else {
        usefail = true;
      }
    }

    // Parse Nz
    if (!strcmp( argv[i], "-z" )) {
      if( i+1 < argc ) {
        numElements[2] = atoi(argv[i+1]);
      } else {
        usefail = true;
      }
    }

    // Parse the output exodus-ii file name
    if (!strcmp( argv[i], "-o" )) {
      if( i+1 < argc ) {
        fnames[EXODUSII_FILE] = argv[i+1];
        oset = true;
      } else {
        usefail = true;
      }
    }

  } /* End of the loop over all command line arguments */

  if( !oset || usefail ) {
    cout << endl
         << "  Executable: " << argv[0] << endl;
    cout << endl
         << "  Usage: channel_mesh -x Nx -y Ny -z Nz -o exodus" << endl
         << endl;
    exit(0);
  }

}

ChannelMesh::ChannelMesh(UnsMesh& mesh, int* numElements) :
  m_mesh(mesh)
/*******************************************************************************
Routine: ChannelMesh - constructor for simple channel mesh
Author : Mark A. Christon
*******************************************************************************/
{
  xmin.X = 0.0;
  xmin.Y = 0.0;
  xmin.Z = 0.0;

  // 2*PI x 1 x PI for Kim & Moin geometry
  //xmax[0] = 2.0*PI;
  //xmax[1] =    1.0;
  //xmax[2] =     PI;

  xmax.X = 20.0;
  xmax.Y = 1.0;
  xmax.Z = 0.05;

  m_mesh.setBbox(xmin, xmax);

  //   Nx    Ny    Nz           Nel    Np
  //  200 x  32 x 100 =     640,000     4
  // Nel[0]  =    200;
  // Nel[1]  =     32;
  // Nel[2]  =    100;

  //  563 x  90 x 282 =  14,288,940   128 or  256
  // Nel[0]  =    563;
  // Nel[1]  =     90;
  // Nel[2]  =    282;

  //  704 x 112 x 352 =  27,754,496   256 or  512
  // Nel[0]  =    704;
  // Nel[1]  =    112;
  // Nel[2]  =    352;

  //  880 x 140 x 440 =  54,208,000   512 or 1024
  // Nel[0]  =    880;
  // Nel[1]  =    140;
  // Nel[2]  =    440;

  //  1100 x 176 x 550 = 106,480,000  1024 or 2048
  // Nel[0]  =   1100;
  // Nel[1]  =    176;
  // Nel[2]  =    550;

  Nel[0]  =   numElements[X];
  Nel[1]  =   numElements[Y];
  Nel[2]  =   numElements[Z];

  Nnp[0]  = Nel[0] + 1;
  Nnp[1]  = Nel[1] + 1;
  Nnp[2]  = Nel[2] + 1;

  Nel_tot = Nel[0]*Nel[1]*Nel[2];
  Nnp_tot = (Nel[0] + 1)*(Nel[1] + 1)*(Nel[2] + 1);

  UnsMeshParm mp;
  memset(&mp, 0, sizeof(mp));
  mp.Ndim       = 3;
  mp.Nel        = Nel_tot;
  mp.Nel_g      = Nel_tot;
  mp.Nel_hex8   = Nel_tot;
  mp.Nel_hex8_g = Nel_tot;

  mp.Nnp   = Nnp_tot;
  mp.Nnp_g = Nnp_tot;

  mp.Neblock = 1;

  // Side-set on every face
  mp.Nsd_sets = 6;

  m_mesh.setLocalMeshParm(mp);
  m_mesh.setGlobalMeshParm(mp);

  // Add element class to the mesh
  int id = 1;
  m_mesh.addElementClass(HEX8, id, Nel_tot);

}

void
ChannelMesh::genConnectivity(int exofh)
/*******************************************************************************
Routine: genConnectivity - generate the element connectivity
Author : Mark A. Christon
*******************************************************************************/
{
  // Write the element block information
  int num_attr = 0;
  int nnpe = 8;
  int id = 1;
  string elname = "HEX";
  int err = ex_put_elem_block(exofh, id, elname.c_str(), Nel_tot, nnpe, num_attr);
  if (err) {
    cout << "exodusWriter::writeConnectivity - ex_put_elem_block = "
         << err << endl;
    exit(0);
  }

  // A single element class is used here
  int* ix = m_mesh.allocTempInt(8*CONNECTIVITY_STRIPMINE);

  int nel = 0;
  int is  = 1;  // 1-based for Exodus output!!!
  for (int k=0; k<Nel[2]; ++k ) {
    for (int j=0; j<Nel[1]; ++j ) {
      for (int i=0; i<Nel[0]; ++i ) {
        int el = i + j*Nel[0] + k*Nel[0]*Nel[1];

        int n0 = i +  j   *Nnp[0] +  k   *Nnp[0]*Nnp[1];
        int n3 = i + (j+1)*Nnp[0] + (k  )*Nnp[0]*Nnp[1];
        int n4 = i +  j   *Nnp[0] + (k+1)*Nnp[0]*Nnp[1];
        int n7 = i + (j+1)*Nnp[0] + (k+1)*Nnp[0]*Nnp[1];
        int n1 = n0 + 1;
        int n2 = n3 + 1;
        int n5 = n4 + 1;
        int n6 = n7 + 1;

        // Store 1-based
        int off = nel*8;
        ix[off  ] = n0 + 1;
        ix[off+1] = n1 + 1;
        ix[off+2] = n2 + 1;
        ix[off+3] = n3 + 1;
        ix[off+4] = n4 + 1;
        ix[off+5] = n5 + 1;
        ix[off+6] = n6 + 1;
        ix[off+7] = n7 + 1;

        // Increment, then test so that 1-based count is correct
        nel++;
        if (nel == CONNECTIVITY_STRIPMINE || (el+1) == Nel_tot) {
          err = ne_put_n_elem_conn(exofh, id, is, nel, ix);
          is += nel;
          nel = 0;
        }
      }
    }
  }
  m_mesh.freeTempData(ix);
}

void
ChannelMesh::genCoordinates(int exofh)
/*******************************************************************************
Routine: genCoordinates - generate the nodal coordinates
Author : Mark A. Christon
*******************************************************************************/
{
  // Allocate stripmine buffers first
  Real* x = m_mesh.allocTempReal(COORDINATES_STRIPMINE);
  Real* y = m_mesh.allocTempReal(COORDINATES_STRIPMINE);
  Real* z = m_mesh.allocTempReal(COORDINATES_STRIPMINE);

  Real dx = (xmax.X - xmin.X)/Nel[0];
  Real dy = (xmax.Y - xmin.Y)/Nel[1];
  Real dz = (xmax.Z - xmin.Z)/Nel[2];

  // Trip over all coordinates
  int nnp = 0;
  int is  = 1; // 1-based for Exodus output!!!

  // For Kim & Moin configuration

  Real zl = xmin.Z;
  for (int k=0; k<Nnp[2]; ++k ) {
    Real yl = xmin.Y;
    for (int j=0; j<Nnp[1]; j++ ) {
      Real xl = xmin.X;
      for (int i=0; i<Nnp[0]; ++i ) {
        int nd = i + j*Nnp[0] + k*Nnp[0]*Nnp[1];
        x[nnp] = xl;
        y[nnp] = yl;
        // Grading too severe, but from Kim & Moin, 1987
//      y[nnp] = 0.5*(1.0 - cos((j*PI)/(Nnp[1]-1)));
        z[nnp] = zl;

        // Increment, then test so that 1-based count is correct
        ++nnp;
        if (nnp == COORDINATES_STRIPMINE || (nd+1) == Nnp_tot) {
          int err = ne_put_n_coord(exofh, is, nnp, x, y, z);
          if (err) {
            cout << "!!! Error: ne_put_n_coord error !!!" << endl;
            exit(0);
          }
          is += nnp;
          nnp = 0;
        }

        xl += dx;
      }
      yl += dy;
    }
    zl += dz;
  }

  m_mesh.freeTempData(x);
  m_mesh.freeTempData(y);
  m_mesh.freeTempData(z);
}

void
ChannelMesh::genSideSets(int exofh)
/*******************************************************************************
Routine: genSideSets - generate sidesets
Author : Mark A. Christon
*******************************************************************************/
{
  char cbuf[MAXCHR];

  // Register the sets -- 0 nodesets, 6 sidesets for this special case
  m_mesh.registerSets(0, 6);

  int Nsd_sets = m_mesh.getNsdsets();

  Sideset* sdset = m_mesh.getSidesets();

  // Inlet
  sdset[0].id  = 10;
  sdset[0].Nel = Nel[1]*Nel[2];
  // Outlet
  sdset[1].id  = 20;
  sdset[1].Nel = Nel[1]*Nel[2];
  // Bottom
  sdset[2].id  = 30;
  sdset[2].Nel = Nel[0]*Nel[1];
  // Top
  sdset[3].id  = 40;
  sdset[3].Nel = Nel[0]*Nel[1];
  // Front
  sdset[4].id  = 50;
  sdset[4].Nel = Nel[0]*Nel[2];
  // Back
  sdset[5].id  = 60;
  sdset[5].Nel = Nel[0]*Nel[2];

  // Register the data
  for (int i=0; i<Nsd_sets; ++i) {
    sprintf(cbuf,"EL_LIST_%d",i);
    sdset[i].EL_LIST = m_mesh.registerVariable(sdset[i].Nel,
                                               sizeof(int),
                                               cbuf,
                                               NO_CTR);

    sprintf(cbuf,"SIDE_LIST_%d",i);
    sdset[i].SIDE_LIST = m_mesh.registerVariable(sdset[i].Nel,
                                                 sizeof(int),
                                                 cbuf,
                                                 NO_CTR);
  }

  for (int m=0; m<Nsd_sets; ++m) {
    cout << "\tGenerating sideset # " << m << endl;
    int* el_list = m_mesh.getVariable<int>(sdset[m].EL_LIST);
    int* sd_list = m_mesh.getVariable<int>(sdset[m].SIDE_LIST);

    switch (m) {
    case 0: {
      // Inlet
      int sd = 3; // 0-based
      int i = 0;
      int e = 0;
      for (int k=0; k<Nel[2]; ++k) {
        for (int j=0; j<Nel[1]; ++j) {
          int el = i + j*Nel[0] + k*Nel[0]*Nel[1];
          el_list[e] = el + 1;
          sd_list[e] = sd + 1;
          e++;
        }
      }
    }
      break;
    case 1: {
      // Outlet
      int sd = 1;
      int i = Nel[0] - 1;
      int e = 0;
      for (int k=0; k<Nel[2]; k++) {
        for (int j=0; j<Nel[1]; j++) {
          int el = i + j*Nel[0] + k*Nel[0]*Nel[1];
          el_list[e] = el + 1;
          sd_list[e] = sd + 1;
          e++;
        }
      }
    }
      break;
    case 2: {
      // Bottom
      int sd = 4;
      int k = 0;
      int e = 0;
      for (int j=0; j<Nel[1]; ++j) {
        for (int i=0; i<Nel[0]; ++i) {
          int el = i + j*Nel[0] + k*Nel[0]*Nel[1];
          el_list[e] = el + 1;
          sd_list[e] = sd + 1;
          ++e;
        }
      }
    }
      break;
    case 3: {
      // Top
      int sd = 5;
      int k = Nel[2] - 1;
      int e = 0;
      for (int j=0; j<Nel[1]; ++j) {
        for (int i=0; i<Nel[0]; ++i) {
          int el = i + j*Nel[0] + k*Nel[0]*Nel[1];
          el_list[e] = el + 1;
          sd_list[e] = sd + 1;
          ++e;
        }
      }
    }
      break;
    case 4: {
      // Front
      int sd = 0;
      int j = 0;
      int e = 0;
      for (int k=0; k<Nel[2]; ++k) {
        for (int i=0; i<Nel[0]; ++i) {
          int el = i + j*Nel[0] + k*Nel[0]*Nel[1];
          el_list[e] = el + 1;
          sd_list[e] = sd + 1;
          ++e;
        }
      }
    }
      break;
    case 5: {
      // Front
      int sd = 2;
      int j = Nel[1] - 1;
      int e = 0;
      for (int k=0; k<Nel[2]; ++k) {
        for (int i=0; i<Nel[0]; ++i) {
          int el = i + j*Nel[0] + k*Nel[0]*Nel[1];
          el_list[e] = el + 1;
          sd_list[e] = sd + 1;
          ++e;
        }
      }
      break;
    }
    } // End of switch

    cout << "\tWriting sideset # " << m << endl;
    int num_dist_fact_in_set = 0;
    int err = ex_put_side_set_param(exofh, sdset[m].id, sdset[m].Nel,
                                    num_dist_fact_in_set);
    if (err) {
      cout << "ChannelMesh::genSideSets - ex_put_side_set_param = "
           << err << endl;
      exit(0);
    }
    err = ex_put_side_set(exofh, sdset[m].id, el_list, sd_list);
    if (err) {
      cout << "ChannelMesh::genSideSets - ex_put_side_set = "
           << err << endl;
      exit(0);
    }
    err = ex_update(exofh);
  }
}
