//******************************************************************************
//! \file    src/Utilities/divset/divset.h
//! \author  Jozsef Bakosi
//! \date    Thu May 10 15:06:00 2012
//! \brief   Divide side set into smaller side sets
//******************************************************************************
#ifndef DIVSET_H
#define DIVSET_H

namespace Hydra {

//! \brief DivSet provides a simple utility for sub-dividing side-sets.

namespace DivSet {

const Real PI = 3.14159265358979323846;
const Real RAD2DEG = 180.0/PI;

//! Basic parameters for divset
struct divsetParams {
  string exeFile;
  string inExo;
  string outExo;
  int setId;
  Vector ref;
  int dir;
  Real len;
};

//! Augment mesh with new side sets
void augmentMesh(PrintUtil* pu, UnsMesh* mesh, divsetParams& params,
                 int numNewSets, int* el_list, int* sd_list, int* cidmap,
                 CVector& coord, int sNel);

//! Calculate centroid of surface element
void calcSurfCentroid(Vector& centroid, int el, int sd, Element* ec,
                      CVector& coord);

//! Compute memory allocated
void computeMemory(UnsMesh* mesh, vector<Real>& memory);

//! Divide side set into smaller ones
void divSet(PrintUtil* pu, UnsMesh* mesh, divsetParams& params, bool refset);

//! Generate new side set
void genNewset(int stripe, vector<int>& new_el_list, vector<int>& new_sd_list,
               divsetParams& params, int* el_list, int* sd_list, int* cidmap,
               UnsMesh* mesh, CVector& coord, int sNel);

//! Calculate the coordinate extents of the side set (to be divided)
void getExtents(divsetParams& params, int* el_list, int* sd_list,
                int* cidmap, UnsMesh* mesh, CVector& coord, int sNel,
                Real& min, Real& max);

//! Parse command line arguments for divset
void parseArgv(int argc, char *argv[], divsetParams& params, bool& refset);

//! Print memory allocated in detail
void printMemory(ostream& ofs, vector<Real>& memory);

//! Report memory usage statistics
void reportMemory(UnsMesh* mesh, ostream& ofs);

}
}

#endif // DIVSET_H
