#!/bin/sh

# run forcenorm 4 times for the 4 sidesets to fix:

$HYDRA/install/gnu/bin/forcenorm -i elmahdi-3x3.h5 -o e.exo -id 4 -n 1 0 0
$HYDRA/install/gnu/bin/forcenorm -i e.exo -o e.exo -id 6 -n -1 0 0
$HYDRA/install/gnu/bin/forcenorm -i e.exo -o e.exo -id 5 -n 0 0 1
$HYDRA/install/gnu/bin/forcenorm -i e.exo -o e.exo -id 7 -n 0 0 -1

# run divset to divide the sidesets into stripes so we can extract forces in segments:

$HYDRA/install/gnu/bin/divset -i e.exo -o elmahdi_3x3.exo -id 9 -dir 2 -ref 0 0.00583742850432053 0 -len 0.0254
