//******************************************************************************
//! \file    src/Utilities/forcenorm/forcenorm.h
//! \author  Jozsef Bakosi
//! \date    Thu May 3 13:16:00 2012
//! \brief   Force a normal on a side set
//******************************************************************************
#ifndef FORCENORM_H
#define FORCENORM_H

namespace Hydra {

//! \brief ForceNorm provids a simple utility for constraining sideset normals

namespace ForceNorm {

const Real RAD2DEG = 180.0/PI;

//! Basic parameters for forcenorm
struct forcenormParams {
  string exeFile;
  string inExo;
  string outExo;
  int setId;
  Vector normal;
};

//! Check volumes and return tru on finding a negative one
bool checkVolume(UnsMesh* mesh);

//! Enforce normal on all nodes of one surface element, i.e. move nodes to plane
void enforceNorm(int el, int sd, Element* ec, int* conn, GlobalDirection dir,
                 Vector ref, Vector& normal, CVector& coord, Vector& elnorm,
                 Real& maxphi);

//! Find a good reference point on the sideset used for determining a plane
bool findRef(int sNel, int* el_list, int* sd_list, int* cidmap, UnsMesh* mesh,
             CVector& coord, Vector& normal, Vector& ref);

//! Force normal on a side set
void forceNorm(PrintUtil* pu, UnsMesh* mesh, int userSetId, Vector& norm);

//! Move node c into plane given by point r and normal n
void moveNode(Real& c1, Real& c2, Real& c3, Real& n1, Real& n2, Real& n3,
              Real& r1, Real& r2, Real& r3);

//! Compute normal of element
void norm(int el, int sd, Element* ec, CVector& coord, Vector& elnorm, int* conn);

//! Parse command line arguments for forcenorm
void parseArgv(int argc, char *argv[], forcenormParams& params);

//! Search for a good reference point on the sideset used for determining a plane
bool searchRef(int sNel, int* el_list, int* sd_list, int* cidmap, UnsMesh* mesh,
               CVector& coord, Vector& normal, Vector& ref);

//! Update maximum angle
void updateMaxAngle(Real angle, Real& maxAngle);

}
}

#endif // FORCENORM_H
