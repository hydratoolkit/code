Line-out utility                                                          1/4/04

command line syntax:

lo fn.exo fn.cntl fn.dat

or

lo fn.exo fn.dat < fn.cntl

Input:
  (x1,y1,z1)
  (x2,y2,z2)
  Npts
  Varname -- could have Nvar variables to output
  Time plane

1. Open exo or other file, read header information

2. Read in time planes & variables

3. Check for valid time plane 

4. Check for valid variable names -- set variable centering
   ELEMENT_VAR
   NODE_VAR

5. Allocate for points, line-out data, generate list of points for line-out

6. Allocate & read connectivity and coordinates

7. Allocate & read data to be interpolated

8. Loop over pts. for lineout
   o Loop over element types
   o Use RCB to locate element where point exists
   o Interpolate for nodal or store for element data
     - Nodal requires inverse mapping problem -- solve for xi,eta,zeta...
     - sample all data at once

9. Terminate or sample more data...
