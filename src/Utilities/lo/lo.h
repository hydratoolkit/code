//******************************************************************************
//! \file    src/Utilities/lo/lo.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 13:01:01 2011
//! \brief   Mesh parameters
//******************************************************************************

namespace Hydra {

#ifdef DBL
typedef double Real;
#else
typedef float  Real;
#endif

struct MeshParam {
  int Ndim;
  int Nnp;
  int Nel;
  int Nel_tri3;
  int Nel_quad4;
  int Nel_hex8;
  int Nel_blk;
  int Nnd_sets;
  int Nsd_sets;
};

}
