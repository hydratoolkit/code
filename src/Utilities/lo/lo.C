//******************************************************************************
//! \file    src/Utilities/lo/lo.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 13:01:01 2011
//! \brief   Simple line-out utility for 2-D currently
//******************************************************************************
#include <cassert>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

#include <cstdlib>
#include <cstring>
#include <time.h>

using namespace std;

#include <Banner.h>
#include "mesh.h"
#include <MPIWrapper.h>
#include <HydraTypes.h>
#include "exodus_io.h"
#include "line_data.h"
#include <PrintUtil.h>
#include <HydraConfig.h>

//------------------------------------------------------------------------------
// Functions
void echoFiles();

void echoHeader();

void initialize(int argc, char **argv);

void openFiles();

void parseArgv(int argc, char *argv[], string* fnames, string& comment);

void readMesh();

void setup();
// Functions
//------------------------------------------------------------------------------

using namespace Hydra;

enum FileType {PLOT=0,       //!< File for plot data
               OUTPUT,       //!< File for ASCII line-out data
               NUM_FILENAMES //!< Number of filenames
};

//------------------------------------------------------------------------------
// Objects with file-scope -- I don't like this, but it's OK for now
Mesh*     mesh;
LineData* ld;
string fnames[NUM_FILENAMES];
ofstream outf;   // output file handle
int exo_fh;      // file handle for exodus input
// Objects & data with file-scope -- I don't like this, but it's OK for now
//------------------------------------------------------------------------------

int main(int argc, char* argv[])
{
  // Initialize the messaging system
  g_comm = new MPIWrapper(&argc, &argv);

  // Initialize, setup files, create primary objects, start timer
  initialize(argc, argv);

  // Do the setup tasks
  setup();

  // Data for interpolation is read by this point
  ld->interpData(mesh);

  // Output line data
  ld->lineout(mesh, outf);

  if (ld->getLocale() == ELEMENT) {
    ld->elemDump(mesh);
  }

  // Close all the files
  closeExoFile(exo_fh);
  outf.close();

  // Cleanup the mesh object & line data
  delete mesh;
  delete ld;

  // Terminate the message passing system
  g_comm->syncup();
  g_comm->finalMP();
}

void echoFiles()
/*******************************************************************************
Routine: echoFiles - print the file names
Author : Mark A. Christon
*******************************************************************************/
{
  cout << "\tExodus Plot File : " << fnames[PLOT]   << endl;
  cout << "\tLine Data   File : " << fnames[OUTPUT] << endl;
}

void echoHeader()
/*******************************************************************************
Routine: echoHeader - echo the mesh header information
Author : Mark A. Christon
*******************************************************************************/
{
  string title = mesh->Title();

  cout << endl;
  cout << "\tM E S H   S T A T I S T I C S\n"
       << "\t=============================\n" << endl;

  cout << "\t" << title.c_str() << endl;
  cout << endl;

  cout << "\tProblem dimension ....................................... "
       << mesh->getNdim() << endl << endl;

  cout << "\tNumber of nodes ......................................... "
       << mesh->getNnp() << endl << endl;

  cout << "\tNumber of elements ...................................... "
       << mesh->getNel() << endl << endl;

  cout << "\tNumber of tri3 elements ................................. "
       << mesh->getNel_tri3() << endl << endl;

  cout << "\tNumber of quad4 elements ................................ "
       << mesh->getNel_quad4() << endl << endl;

  cout << "\tNumber of tet4 elements ................................. "
       << mesh->getNel_tet4() << endl << endl;

  cout << "\tNumber of hex8 elements ................................. "
       << mesh->getNel_hex8() << endl << endl;

  cout << "\tNumber of materials ..................................... "
       << mesh->getNmat() << endl << endl;

  cout << "\tNumber of node sets ..................................... "
       << mesh->getNndsets() << endl << endl;

  cout << "\tNumber of side sets ..................................... "
       << mesh->getNsdsets() << endl << endl;

  Real* xmin = mesh->getXmin();
  Real* xmax = mesh->getXmax();
  int Ndim = mesh->getNdim();
  switch(Ndim) {
    case TWOD: {
      cout << "\tX-min. .................................................. "
           << xmin[0] << endl << endl;
      cout << "\tY-min. .................................................. "
           << xmin[1] << endl << endl;
      cout << "\tX-max. .................................................. "
           << xmax[0] << endl << endl;
      cout << "\tY-max. .................................................. "
           << xmax[1] << endl << endl;
    }
    break;
    case THREED: {
    }
    break;
  }

}

void initialize(int argc, char* argv[])
/*******************************************************************************
Routine: initialize - perform code initialization tasks
Author : Mark A. Christon
*******************************************************************************/
{
  // Store the date & time
  time_t qa_time = -1;
  qa_time = time(NULL);
  string date = ctime(&qa_time);

  // Print banner
  PrintUtil pu(cout);
  pu.printBanner("LO",
                 HYDRA_VERSION,
                 HYDRA_OSNAME,
                 HYDRA_COMPILER,
                 HYDRA_BUILD_TYPE,
                 HYDRA_BUILD_DATE,
 " ***************************************************************************");

  string comment = " ";
  parseArgv(argc, argv, fnames, comment);
  echoFiles();

  // Open all the files
  openFiles();

  // Construct the mesh object which holds all field variables too
  mesh = new Mesh;
  ld   = new LineData;

  // Switch the comment symbol as required
  if (comment != " ") {
    ld->setComment(comment);
  }
}

void
openFiles()
/*******************************************************************************
Routine: openFiles - setup the primary files for I/O
         PLOT
         OUTPUT
Author : Mark A. Christon
*******************************************************************************/
{
  // Open the exodus file (for input)
  exo_fh = openExoFile(fnames[PLOT], "input");

  // Open the output file
  outf.open(fnames[OUTPUT ].c_str(), ios::out);
  if (!outf) {
    cout << "File " << fnames[OUTPUT] << " can't be opened ..." << endl;
    exit(0);
  }
}

void
parseArgv(int argc, char *argv[], string* names, string& comment)
/******************************************************************************
Routine: parseArgv - parse command line arguments and setup filenames
Author : Mark A. Christon
*******************************************************************************/
{
  bool usefail;
  bool pset = false;
  bool oset = false;

  // Parse the command line the slow, but sure way
  usefail = false;
  for (int i=1; i<argc; i++) {

    // Parse the plot file name
    if (!strcmp( argv[i], "-i" )) {
      if( i+1 < argc ) {
        names[PLOT] = argv[i+1];
        pset = true;
      } else {
        usefail = true;
      }
    }

    // Parse the control file name
    if (!strcmp( argv[i], "-o" )) {
      if( i+1 < argc ) {
        names[OUTPUT] = argv[i+1];
        oset = true;
      } else {
        usefail = true;
      }
    }

    // Parse the comment string
    if (!strcmp( argv[i], "-c" )) {
      if( i+1 < argc ) {
        comment = argv[i+1];
      } else {
        usefail = true;
      }
    }

  } /* End of the loop over all command line arguments */

  if( (!pset && !oset) || usefail ) {
    cout << endl
         << "  Executable: " << argv[0] << endl;
    cout << endl
         << "  Usage: lo -i plot_file -o out_file -c comment_symbol \n" << endl;
    exit(0);
  }
}

void readMesh()
/*******************************************************************************
Routine: readMesh - driver to allocate & read a mesh file
Author : Mark A. Christon
*******************************************************************************/
{
  // read the header and some element information for
  // determining element type so we can allocate storage
  readExoHeader(exo_fh, mesh);

  // Allocate base storage for mesh
  mesh->allocateData();

  // now read the grid, nodeset and sidesets
  readExoCoord(exo_fh, mesh);
  readExoConn(exo_fh, mesh);

  // Calculate the bounding box -- echo mesh parm's
  mesh->calcBbox();
  echoHeader();

  // Read in the variable names -- stored in the data mesh
  readExoVnames(exo_fh, mesh);
  readExoTimes(exo_fh, mesh);
}

void setup()
/*******************************************************************************
Routine: setup - perform setup: Read the mesh, parse controls , etc.
Author : Mark A. Christon
*******************************************************************************/
{
  // Read the mesh
  readMesh();

  // Parse the line data
  ld->readInput();
  ld->echoInput();

  // Check the variable for line-out and the time-plane
  ld->checkInput(mesh);

  // Setup the line data
  ld->genLine();

  // Allocate & read in the data to interpolate
  readExoVar(exo_fh, mesh, ld);
}
