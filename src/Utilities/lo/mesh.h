//******************************************************************************
//! \file    src/Utilities/lo/mesh.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 13:01:01 2011
//! \brief   Mesh interface for line-out utility
//******************************************************************************
#ifndef MESH_H
#define MESH_H

#include <string>
#include <vector>

#include <HydraTypes.h>

namespace Hydra {

// Element Types
enum Eltype {TRI3,             //!< 3-node triangle
             QUAD4,            //!< 4-node quadrilateral
             TET4,             //!< 4-node tetrahedra
             HEX8,             //!< 8-node hex
             NUM_ELEMENT_TYPES //!< # of element types
};

// Define the topology
// 3-Node tri element
#define NNPE_TRI3        3  //!< No. of nodes per element
#define NUM_TRI3_EDGES   3  //!< No. of edges per element
#define NNPE_TRI3_EDGES  2  //!< No. of nodes per tri3 edge
#define NUM_TRI3_FACES   1  //!< No. of faces per element
#define NNPE_TRI3_FACES  3  //!< No. of nodes per tri3 face

// 4-Node quad element
#define NNPE_QUAD4       4  //!< No. of nodes per element
#define NUM_QUAD4_EDGES  4  //!< No. of edges per element
#define NNPE_QUAD4_EDGES 2  //!< No. of nodes per quad4 edge
#define NUM_QUAD4_FACES  1  //!< No. of faces per element
#define NNPE_QUAD4_FACES 4  //!< No. of nodes per quad4 face

// 4-Node tet element
#define NNPE_TET4        4   //!< No. of nodes per element
#define NUM_TET4_EDGES   6   //!< No. of edges per element
#define NNPE_TET4_EDGES  2   //!< No. of nodes per tet4 edge
#define NUM_TET4_FACES   4   //!< No. of faces per element
#define NNPE_TET4_FACES  3   //!< No. of nodes per tet4 face

// 8-Node hex element
#define NNPE_HEX8        8   //!< No. of nodes per element
#define NUM_HEX8_EDGES   12  //!< No. of edges per element
#define NNPE_HEX8_EDGES  2   //!< No. of nodes per hex8 edge
#define NUM_HEX8_FACES   6   //!< No. of faces per element
#define NNPE_HEX8_FACES  4   //!< No. of nodes per hex8 face

// 3-Node tri element
struct Tri3{
  //! \name  3-node triangular element
  //! This structure is used to hold all the book-keeping data for the element.
  //@{
  int gid;                  //!< Global element id (zero based)
  int mat;                  //!< Material id
  int ix[NNPE_TRI3];        //!< Node-to-element connectivity
  int fid[NUM_TRI3_EDGES];  //!< Face element gid  (0-based)
  static int edges[NUM_TRI3_EDGES][NNPE_TRI3_EDGES]; //<! Sideset face array
  //@}
};


// 4-Node quad element
struct Quad4 {
  //! \name  4-node quadrilateral element
  //! This structure is used to hold all the book-keeping data for the element.
  //@{
  int gid;                   //!< Global element id
  int mat;                   //!< Material id
  int ix[NNPE_QUAD4];        //!< Node-to-element connectivity
  int fid[NUM_QUAD4_EDGES];  //!< Face element gid  (0-based)
  static int edges[NUM_QUAD4_EDGES][NNPE_QUAD4_EDGES]; //<! Sideset face array
  //@}
};

// 4-Node tet element
struct Tet4 {
  //! \name  4-node tet element
  //! This structure is used to hold all the book-keeping data for the element.
  //@{
  int gid;                  //!< Global element id (zero based)
  int mat;                  //!< Material id
  int ix[NNPE_TET4];        //!< Node-to-element connectivity
  int fid[NUM_TET4_FACES];  //!< Face element gid  (0-based)
  static int edges[NUM_TET4_EDGES][NNPE_TET4_EDGES]; //<! Sideset edge array
  static int faces[NUM_TET4_FACES][NNPE_TET4_FACES]; //<! Sideset face array
  //@}
};

// 8-Node hex element
struct Hex8 {
  //! \name  8-node hex element
  //! This structure is used to hold all the book-keeping data for the element.
  //@{
  int gid;                  //!< Global element id (zero based)
  int mat;                  //!< Material id
  int ix[NNPE_HEX8];        //!< Node-to-element connectivity
  int fid[NUM_HEX8_FACES];  //!< Neighbor element gid  (0-based)
  static int edges[NUM_HEX8_EDGES][NNPE_HEX8_EDGES]; //<! Sideset edge array
  static int faces[NUM_HEX8_FACES][NNPE_HEX8_FACES]; //<! Sideset face array
  //@}
};

struct ElPoint {
   int eid;
   Eltype type;
   Real xi, eta, zeta;  //!< Natural coordinates
   Real sf[8];          //!< Shape functions at xi, eta, zeta
};

//! Mesh object -- holds basic mesh information, sidesets, nodesets, and data
//! The mesh object holds basic mesh information, sidesets, nodesets, and data
//! This is the so-called "data-mesh" model where the mesh object basically
//! serves data to the physics object.

class Mesh {

  public:
             Mesh();
    virtual ~Mesh();

    // Member Functions

    //! Allocate all of the variables/objects for the mesh
    void allocateData();

    //! Calculate the bounding box for the grid
    void calcBbox();

    Real findEdata(Real x, Real y);

    Real findNdata(Real x, Real y);

    void invMapQuad4(Real x1, Real x2, Real x3, Real x4,
                     Real y1, Real y2, Real y3, Real y4,
                     Real x,  Real y);

    Real interpNodeField();

    //! Print out the element connectivity
    void printConnectivity();

    //! Print out the mesh parameters
    void echoMeshParms(void);

    //! Store the mesh title
    void setTitle(char* cbuf) {title = cbuf;}

    //! Retrieve a character string for the mesh title
    const char* getTitle()   {return title.c_str();}

    //! Retrieve the C++ string for that holds the mesh title
    string Title()  {return title;}

    //! Set all the relevant mesh parameters
    void setMeshParm(int Ndim, int Nnp,
                     int Nel,
                     int Nel_tri3, int Nel_quad4,
                     int Nel_tet4, int Nel_hex8,
                     int Nmat, int Nnd_sets, int Nsd_sets, int Neblock);

    //! \name Mesh Parameter Functions
    //! These functions are used to access the basic parameters that
    //! describe the mesh, e.g., the number of nodes, number of elements, etc.
    //@{
    int   getNdim()      {return Ndim;}
    int   getNnp()       {return Nnp; }
    int   getNel()       {return Nel; }
    int   getNel_tri3()  {return Nel_tri3; }
    int   getNel_quad4() {return Nel_quad4;}
    int   getNel_tet4()  {return Nel_tet4;}
    int   getNel_hex8()  {return Nel_hex8; }
    int   getNmat()      {return Nmat;}
    int   getNeblock()   {return Neblock;}
    int   getNndsets()   {return Nnd_sets;}
    int   getNsdsets()   {return Nsd_sets;}

    vector<string> getEvar() {return elemvar;}
    vector<string> getNvar() {return nodevar;}
    void addEname(string name) {elemvar.push_back(name);}
    void addNname(string name) {nodevar.push_back(name);}

    void  allocTimes(int Nsteps) {times = new Real [Nsteps];}
    Real* getTimes() {return times;}

    void allocVar(int Npts) {var = new Real [Npts];}
    Real* getVar() {return var;}

    Real* getXmin() {return xmin;}
    Real* getXmax() {return xmax;}

    //! \name DataIndex variables registered by the datamesh
    //! These functions provide access to the nodal coordinates, and volume
    //@{
    Real* getX() {return x;}
    Real* getY() {return y;}
    Real* getZ() {return z;}
    //@}

    //! \name Elements based on dimensionality or element type
    //! These functions provide access to the arrays of elements
    //@{
    Tri3*  getTri3() {return t3;}
    Quad4* getQuad4(){return q4;}
    Tet4*  getTet4() {return t4;}
    Hex8*  getHex8() {return h8;}
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    Mesh(const Mesh&);
    Mesh& operator=(const Mesh&);
    //@}

    //! \name Mesh parameters
    //@{
    string title;  //!< Mesh title
    int meshtype;  //!< Mesh type
    int Ndim;      //!< Dimension of mesh
    int Nnp;       //!< Number of nodes
    int Nel;       //!< Total Number of elements
    int Nel_tri3;  //!< Number of TRI3  elements
    int Nel_quad4; //!< Number of QUAD4 elements
    int Nel_tet4;  //!< Number of TET4  elements
    int Nel_hex8;  //!< Number of HEX8  elements
    int Nmat;      //!< Number of materials
    int Nface;     //!< Total number of faces
    int Nface_int; //!< Number of internal faces
    int Nface_ext; //!< Number of external faces
    int Nedge;     //!< Number of edges
    int Neblock;   //!< Number of element blocks (for Exodus)
    int Nnd_sets;  //!< Number of nodesets
    int Nsd_sets;  //!< Number of sidesets
    //@}

    Tri3*  t3;
    Quad4* q4;
    Tet4*  t4;
    Hex8*  h8;

    //! \name Coordinates -- dimensions are [Nnp]
    //@{
    Real* x;
    Real* y;
    Real* z;
    //@}

    //! \name Bounding box
    Real xmin[3];
    Real xmax[3];

    // Variable names in database
    vector<string> elemvar;
    vector<string> nodevar;

    //! \name Element point
    ElPoint epoint;

    Real* times;   // time planes

    // Data to interpolate
    Real* var;
};

}
#endif /* MESH_H */
