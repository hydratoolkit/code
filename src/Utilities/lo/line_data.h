//******************************************************************************
//! \file    src/Utilities/lo/line_data.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 13:01:01 2011
//! \brief   Line data for line-out utility
//******************************************************************************
#ifndef LINE_DATA_H
#define LINE_DATA_H

#include <iostream>
#include <string>

#include <HydraTypes.h>

namespace Hydra {

// Locations for variables that can be interpolated
enum varLocale {ELEMENT=0, NODE, NUM_LOC};
enum coordType {EULERIAN=0, LAGRANGIAN, NUM_COORDTYPE};

class LineData {
  public:
             LineData();
    virtual ~LineData();

    void readInput();
    void echoInput();
    void checkInput(Mesh* mesh);
    void genLine();
    void interpData(Mesh* mesh);
    void lineout(Mesh* mesh, ostream& ofs);
    void setComment(const string ucomment) {comment = ucomment;}

    varLocale getLocale() {return loc;}
    coordType getCoordType() {return ctype;}
    int getVid() {return var_id;}
    int getTid() {return time_plane;}

    int getDispId(Mesh* mesh, string name);

    void elemDump(Mesh* mesh);

  private:
    int  Npts;

    Real ds;
    Real x1[3];
    Real x2[3];

    Real *x;
    Real *y;
    Real *z;
    Real *data;
    Real offset;

    varLocale loc;
    coordType ctype;
    int       var_id;     // 0-based
    int       time_plane; // 0-based
    string    comment;
    string    var_name;

};

}
#endif // LINE_DATA_H
