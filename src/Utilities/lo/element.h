//******************************************************************************
//! \file    src/Utilities/lo/element.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 13:01:01 2011
//! \brief   Simple element structs w. basic topology information
//******************************************************************************
#ifndef ELEMENT_H
#define ELEMENT_H

namespace Hydra {

// Element Types
enum Eltype {TRI3,             //!< 3-node triangle
             QUAD4,            //!< 4-node quadrilateral
             TET4,             //!< 4-node tetrahedra
             HEX8,             //!< 8-node hex
             NUM_ELEMENT_TYPES //!< # of element types
};

// Define the topology

// 2-Node element
#define NNPE_BEAM2       2  //!< No. of nodes per 2-node beam element

// 3-Node tri element
#define NNPE_TRI3        3  //!< No. of nodes per element
#define NUM_TRI3_EDGES   3  //!< No. of edges per element
#define NNPE_TRI3_EDGES  2  //!< No. of nodes per tri3 edge
#define NUM_TRI3_FACES   1  //!< No. of faces per element
#define NNPE_TRI3_FACES  3  //!< No. of nodes per tri3 face

// 4-Node quad element
#define NNPE_QUAD4       4  //!< No. of nodes per element
#define NUM_QUAD4_EDGES  4  //!< No. of edges per element
#define NNPE_QUAD4_EDGES 2  //!< No. of nodes per quad4 edge
#define NUM_QUAD4_FACES  1  //!< No. of faces per element
#define NNPE_QUAD4_FACES 4  //!< No. of nodes per quad4 face

// 4-Node tet element
#define NNPE_TET4        4   //!< No. of nodes per element
#define NUM_TET4_EDGES   6   //!< No. of edges per element
#define NNPE_TET4_EDGES  2   //!< No. of nodes per tet4 edge
#define NUM_TET4_FACES   4   //!< No. of faces per element
#define NNPE_TET4_FACES  3   //!< No. of nodes per tet4 face

// 8-Node hex element
#define NNPE_HEX8        8   //!< No. of nodes per element
#define NUM_HEX8_EDGES   12  //!< No. of edges per element
#define NNPE_HEX8_EDGES  2   //!< No. of nodes per hex8 edge
#define NUM_HEX8_FACES   6   //!< No. of faces per element
#define NNPE_HEX8_FACES  4   //!< No. of nodes per hex8 face


// 2-Node element
struct Beam2 {
  //! \name  2-node 1-D element (for testing purposes)
  //! This structure is used to hold all the book-keeping data for the element.
  //@{
  int gid;             //!< Global element id (zero based)
  int mat;             //!< Material id
  int ix[NNPE_BEAM2];  //!< Node-to-element connectivity //@}
};


// 3-Node tri element
struct Tri3{
  //! \name  3-node triangular element
  //! This structure is used to hold all the book-keeping data for the element.
  //@{
  int gid;                  //!< Global element id (zero based)
  int mat;                  //!< Material id
  int ix[NNPE_TRI3];        //!< Node-to-element connectivity
  int fid[NUM_TRI3_EDGES];  //!< Face element gid  (0-based)
  static int edges[NUM_TRI3_EDGES][NNPE_TRI3_EDGES]; //<! Sideset face array
  //@}
};


// 4-Node quad element
struct Quad4 {
  //! \name  4-node quadrilateral element
  //! This structure is used to hold all the book-keeping data for the element.
  //@{
  int gid;                   //!< Global element id
  int mat;                   //!< Material id
  int ix[NNPE_QUAD4];        //!< Node-to-element connectivity
  int fid[NUM_QUAD4_EDGES];  //!< Face element gid  (0-based)
  static int edges[NUM_QUAD4_EDGES][NNPE_QUAD4_EDGES]; //<! Sideset face array
  //@}
};


// 4-Node tet element
struct Tet4 {
  //! \name  4-node tet element
  //! This structure is used to hold all the book-keeping data for the element.
  //@{
  int gid;                  //!< Global element id (zero based)
  int mat;                  //!< Material id
  int ix[NNPE_TET4];        //!< Node-to-element connectivity
  int fid[NUM_TET4_FACES];  //!< Face element gid  (0-based)
  static int edges[NUM_TET4_EDGES][NNPE_TET4_EDGES]; //<! Sideset edge array
  static int faces[NUM_TET4_FACES][NNPE_TET4_FACES]; //<! Sideset face array
  //@}
};

// 8-Node hex element
struct Hex8 {
  //! \name  8-node hex element
  //! This structure is used to hold all the book-keeping data for the element.
  //@{
  int gid;                  //!< Global element id (zero based)
  int mat;                  //!< Material id
  int ix[NNPE_HEX8];        //!< Node-to-element connectivity
  int fid[NUM_HEX8_FACES];  //!< Neighbor element gid  (0-based)
  static int edges[NUM_HEX8_EDGES][NNPE_HEX8_EDGES]; //<! Sideset edge array
  static int faces[NUM_HEX8_FACES][NNPE_HEX8_FACES]; //<! Sideset face array
  //@}
};

// Generic face
struct Face {
  //! \name Face structure
  //! This structure holds the relevant information for faces used in
  //! the C-Law formulation
  // int lid1;        //!< Local element id (zero based)
  int gid1;        //!< Global element id (zero based)
  int side1;       //!< Side number
  Eltype etype1;   //!< Element type
  // int lid2;        //!< Local element id (zero based)
  int gid2;        //!< Global element id (zero based)
  int side2;       //!< Side number
  Eltype etype2;   //!< Element type
  bool has_ghost;  //!< True indicates el2 is really a ghost id
  Real en[3];      //!< Surface normal
  Real xs[3];      //!< Surface centroid
  Real gamma;      //!< Surface area (or length in 2D)
};

//==============================================================================
//
// Not used yet -- will probably have to convert to this later
//
class Element {
  //! \name  Element
  //! Base class for elements.
  public:
             Element() {}
    virtual ~Element() {}

    virtual int* getIX() {
      cout << "!!! No Default getIX() in Element classs !!!" << endl;
      return 0;
    }

    void setParm(int id, int mid) {gid = id; mat = mid;}

  protected:
    int gid;  //!< Global element id (required for side sets, renumbering, etc.)
    int mat;  //!< Global material id for this element

  private:
    // Don't permit copy or assignment operators
    Element(const Element&);
    Element& operator=(const Element&);
};


class N_Beam2 : public Element {
  //! \name  2-node 1-D element (for testing purposes)
  //! This structure is used to hold all the book-keeping data for the element.
  public:
             N_Beam2() {}
    virtual ~N_Beam2() {}

    virtual int* getIX() {return ix;}

  protected:

  private:
    // Don't permit copy or assignment operators
    N_Beam2(const N_Beam2&);
    N_Beam2& operator=(const N_Beam2&);

    int ix[NNPE_BEAM2];
};


class N_Tri3 : public Element {
  //! \name  3-node triangular element
  //! This structure is used to hold all the book-keeping data for the element.
  public:
             N_Tri3() {cout << "Allocating Tri3 class" << endl;}
    virtual ~N_Tri3() {}

    virtual int* getIX() {return ix;}

  protected:

  private:
    // Don't permit copy or assignment operators
    N_Tri3(const N_Tri3&);
    N_Tri3& operator=(const N_Tri3&);

    int ix[NNPE_TRI3];
    int nb_el[NUM_TRI3_EDGES];   //!< Neighbor element gid  (0-based)
    int nb_side[NUM_TRI3_EDGES]; //!< Neighbor element side (0-based)
    static int faces[NUM_TRI3_EDGES][NNPE_TRI3_EDGES]; //<! Sideset face array
};


class N_Quad4 : public Element {
  //! \name  4-node quadrilateral element
  //! This structure is used to hold all the book-keeping data for the element.
  public:
             N_Quad4() {}
    virtual ~N_Quad4() {}

    virtual int* getIX() {return ix;}

  protected:

  private:
    // Don't permit copy or assignment operators
    N_Quad4(const N_Quad4&);
    N_Quad4& operator=(const N_Quad4&);

    int ix[NNPE_QUAD4];
    static int faces[NUM_QUAD4_FACES][NNPE_QUAD4_FACES]; //<! Sideset face array
};


class N_Hex8 : public Element {
  //! \name  8-node hex element
  //! This structure is used to hold all the book-keeping data for the element.
  public:
             N_Hex8() {}
    virtual ~N_Hex8() {}

    virtual int* getIX() {return ix;}

  protected:

  private:
    // Don't permit copy or assignment operators
    N_Hex8(const N_Hex8&);
    N_Hex8& operator=(const N_Hex8&);

    //@{
    int ix[NNPE_HEX8];
    static int faces[NUM_HEX8_FACES][NNPE_HEX8_FACES]; //<! Sideset face array
    //@}
};

}

#endif // ELEMENT_H
