//******************************************************************************
//! \file    src/Utilities/converters/fluent2exo.h
//! \author  Mark A. Christon
//! \date    Wed Oct 05 2016
//! \brief   Convert an ASCII FLUENT msh file to Exodus-II file
//******************************************************************************
#ifndef FLUENT2EXO_H
#define FLUENT2EXO_H

enum MeshFileType {EXE_FILE=0,    // Executable name
                   ASCII_FILE,    // Input ASCII mesh file
                   EXODUSII_FILE, // Input Exodus-II mesh file
                   NUM_FILES      // Number of files
};

void finalize();

void initialize(int argc, char **argv);

void parseArgv(int argc, char *argv[], string* fnames);

void setup();

#endif // FLUENT2EXO_H
