//******************************************************************************
//! \file    src/Utilities/converters/ascii2exo.h
//! \author  Mark A. Christon
//! \date    Monday February 15, 2016
//! \brief   Convert plain ASCII nodes, connectivity to Exodus-II
//******************************************************************************
#ifndef ASCII2EXO_H
#define ASCII2EXO_H

enum MeshFileType {EXE_FILE=0,    // Executable name
                   ASCII_FILE,    // Input ASCII mesh file
                   EXODUSII_FILE, // Input Exodus-II mesh file
                   NUM_FILES      // Number of files
};

void finalize();

void initialize(int argc, char **argv);

void parseArgv(int argc, char *argv[], string* fnames);

void setup();

#endif // ASCII2EXO_H
