//******************************************************************************
//! \file    src/Utilities/converters/exo2fluent.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:59:30 2011
//! \brief   Convert Exodus-II file to an ASCII Fluent msh file
//******************************************************************************
#ifndef EXO2FLUENT_H
#define EXO2FLUENT_H

enum MeshFileType {EXE_FILE=0,    // Executable name
                   ASCII_FILE,    // Input ASCII mesh file
                   EXODUSII_FILE, // Input Exodus-II mesh file
                   NUM_FILES      // Number of files
};

void finalize();

void echoDualEdgeParm(ostream& ofs, Hydra::UnsMesh* mesh);

void initialize(int argc, char **argv);

void parseArgv(int argc, char *argv[], string* fnames);

void setup(Hydra::UnsMesh* mesh);

#endif // EXO2FLUENT_H
