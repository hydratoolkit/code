//******************************************************************************
//! \file    src/Utilities/converters/msh2exo.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:59:30 2011
//! \brief   ASCII mesh format to Exodus-II converter
//******************************************************************************
#ifndef MSH2EXO_H
#define MSH2EXO_H

enum MeshFileType {EXE_FILE=0,    // Executable name
                   ASCII_FILE,    // Input ASCII mesh file
                   EXODUSII_FILE, // Input Exodus-II mesh file
                   NUM_FILES      // Number of files
};

void finalize();

void initialize(int argc, char **argv);

void parseArgv(int argc, char *argv[], string* fnames);

void setup();

#endif // MSH2EXO_H
