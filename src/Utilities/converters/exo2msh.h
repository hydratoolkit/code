//******************************************************************************
//! \file    src/Utilities/converters/exo2msh.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:59:30 2011
//! \brief   Convert Exodus-II mesh file to an ASCII msh file
//******************************************************************************
#ifndef EXO2MSH_H
#define EXO2MSH_H

enum MeshFileType {EXE_FILE=0,    // Executable name
                   ASCII_FILE,    // Input ASCII mesh file
                   EXODUSII_FILE, // Input Exodus-II mesh file
                   NUM_FILES      // Number of files
};

void finalize();

void initialize(int argc, char **argv);

void parseArgv(int argc, char *argv[], string* fnames);

void setup();

#endif // EXO2MSH_H
