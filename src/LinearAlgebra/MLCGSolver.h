//******************************************************************************
//! \file    src/LinearAlgebra/MLCGSolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   CG solver with ML pre-conditioner
//******************************************************************************
#ifndef TrilinosMLCGSolver_h
#define TrilinosMLCGSolver_h

// Forward declarations
typedef struct ML_Aggregate_Struct ML_Aggregate;
typedef struct ML_Operator_Subspace_Struct ML_Operator_Subspace;
typedef struct ML_Operator_Struct ML_Operator;
typedef struct ML_Struct ML;

// Begin local includes
#include <NativeSolver.h>

namespace Hydra {

class TrilinosMLCGSolver: public NativeSolver
{
    friend class SolverFactory;

  public:

    //! \name Constructor/Destructor
    //@{
    TrilinosMLCGSolver(Category&, DataContainer&, const string&);
    virtual ~TrilinosMLCGSolver() {}
    //@}

    virtual int dump() const;
    virtual void finalize();
    virtual int getStatus(SolverStatus& status) const;
    virtual void initialize(int nel, int globelNel);
    virtual int solve(const Vector* b, Vector* x);
    virtual int setOperator(Matrix* A);

  private:

    //! Don't permit copy or assignment operators
    //@{
    TrilinosMLCGSolver(const TrilinosMLCGSolver&);
    TrilinosMLCGSolver& operator=(const TrilinosMLCGSolver&);
    //@}

    void mlcga(Real* a, Real* x, const Real* b, Real* p, Real* r,
               Real* ap, Real* z, int neq, const int* di, const int* dj);
    static int getrow(ML_Operator* A_data, int N_requested_rows,
                      int requested_rows[], int allocated_space,
                      int columns[], double values[],
                      int row_lengths[]);
    static int matvec(ML_Operator* A_data, int in_length,
                      double p[], int out_length, double ap[]);


    Matrix* m_A;
    Real m_erra;
    Real m_errb;
    int  m_itknt;

    ML* m_ml;
    ML_Aggregate* m_aggregate;

    //! Data indexes for temporary data
    DataIndex PP;
    DataIndex AP;
    DataIndex W;
    DataIndex R;
    DataIndex Z;
};

}

#endif
