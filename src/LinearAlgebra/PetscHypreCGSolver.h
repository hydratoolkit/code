//******************************************************************************
//! \file    src/LinearAlgebra/PetscHypreCGSolver.h
//! \author  Jozsef Bakosi, Mark A. Christon
//! \date    Thu Aug 23 15:26:58 2012
//! \brief   Hypre pre-conditioner and the PETSc CG solver
//******************************************************************************
#ifndef PetscHypreCGSolver_h
#define PetscHypreCGSolver_h

#include <PetscSolver.h>

namespace Hydra {

//! PETSC-Hypre interface
class PetscHypreCGSolver: public PetscSolver
{
    friend class LASolverFactory;

  public:

    //! \name Constructor/Destructor
    //@{
             PetscHypreCGSolver(Category&, DataContainer&, const string&);
    virtual ~PetscHypreCGSolver() {}
    //@}

    virtual void initializeKSP();
    virtual void initializePC();
    virtual void setOperator(LAMatrix* A);

  protected:

  private:

    //! Don't permit copy or assignment operators
    //@{
    PetscHypreCGSolver(const PetscHypreCGSolver&);
    PetscHypreCGSolver& operator=(const PetscHypreCGSolver&);
    //@}

    int m_operatorNumber;

};

}

#endif
