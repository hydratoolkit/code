//******************************************************************************
//! \file    src/LinearAlgebra/PetscMatrix.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   PETSc Matrix class
//******************************************************************************
#ifndef PetscMatrix_h
#define PetscMatrix_h

struct _p_Mat;
typedef struct _p_Mat* Mat;

#include <LAMatrix.h>

namespace Hydra {

class DataContainer;
class LAVector;

class PetscMatrix: public LAMatrix {

    friend class PetscSolver;

  public:

    //! \name Constructor/Destructor
    //@{
             PetscMatrix(DataContainer& dataContainer, int M, int N);
    virtual ~PetscMatrix();
    //@}

    //! Initialize matrix using two vectors of diagonal and off
    virtual void initialize(int m, int n,
                            DataIndex i,  DataIndex j,  DataIndex a,
                            DataIndex oi, DataIndex oj, DataIndex oa);

    //! Initialize matrix using information on number of non-zero terms per row.
    virtual void initialize(int m, int n, const int* dnz, const int* onz);

    //! Initialize matrix using a constant number of non-zero terms per row.
    virtual void initialize(int m, int n, int ndnz, int nonz);

    //! Deallocate any memory allocated by this matrix
    virtual void finalize();

    //! Return items from a matrix
    virtual void get(int m, const int* i, int n, const int* j, Real* a) const;

    //! Return diagonal of a matrix in the provided vector
    virtual void getDiagonal(LAVector* v) const;

    //! Return local sizes
    virtual void getLocalSize(int&m, int& n) const;

    //! Return equation row ownership range
    virtual void getOwnershipRange(int&m, int& n) const;

    //! Override a block of values in the matrix
    virtual void set(int m, const int* i, int n, const int* j, const Real* a);

    //! Set diagonal of a matrix using values from the provided vector
    virtual void setDiagonal(const LAVector* v);

    //! Add a block of values to the matrix
    virtual void add(int m, const int* i, int n, const int* j, const Real* a);

    //! Assemble global matrix if running in parallel
    virtual void assemble();

    //! Dump matrix to stdout
    virtual void dump() const;

    //! Return true if this is a non-PETSc (native) matrix
    virtual bool isNative() const;

    //! Zero the matrix
    virtual void zero();

    //! Return PETSc matrix
    Mat& getMat() { return m_Mat; }

    //! Return PETSc matrix
    const Mat& constGetMat() const { return m_Mat; }

  private:

    //! Don't permit copy or assignment operators
    //@{
    PetscMatrix(const PetscMatrix&);
    PetscMatrix& operator=(const PetscMatrix&);
    //@}

    Mat m_Mat; //!< PETSc matrix
};

}

#endif
