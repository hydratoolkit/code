//******************************************************************************
//! \file    src/LinearAlgebra/LASolverFactory.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   Linear algebra solver factory
//******************************************************************************
#ifndef LASolverFactory_h
#define LASolverFactory_h

#include <HydraTypes.h>

namespace Hydra {


// Forward declarations
class DataContainer;
class LASolver;
class Category;

// Linear Algebra Solver Factory
class LASolverFactory {

  public:

    //! \name Constructor/Destructor
    //@{
             LASolverFactory(DataContainer& dataContainer);
    virtual ~LASolverFactory();
    //@}

    //! Create a CFD solver
    LASolver* createSolver(Category& eqSolver, string name) const;

    //! Initialize solver sub-system
    static void initialize(int argc, char** argv);

    //! Finalize solver sub-system
    static void finalize();

  protected:

    DataContainer& m_dataContainer;

  private:

    //! Don't permit copy or assignment operators
    //@{
    LASolverFactory(const LASolverFactory&);
    LASolverFactory& operator=(const LASolverFactory&);
    //@}

};

}

#endif
