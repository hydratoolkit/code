//******************************************************************************
//! \file    src/LinearAlgebra/PetscSSORCGSolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   PETSc CG solver .w SSOR preconditioner
//******************************************************************************
#ifndef PetscSSORCGSolver_h
#define PetscSSORCGSolver_h

#include <PetscSolver.h>

namespace Hydra {

//! PETSc - SSOR preconditioned CG
class PetscSSORCGSolver: public PetscSolver
{
    friend class LASolverFactory;

  public:

    //! \name Constructor/Destructor
    //@{
             PetscSSORCGSolver(Category&, DataContainer&, const string&);
    virtual ~PetscSSORCGSolver() {}
    //@}

    virtual void initializePC();
    virtual void initializeKSP();

  private:

    //! Don't permit copy or assignment operators
    //@{
    PetscSSORCGSolver(const PetscSSORCGSolver&);
    PetscSSORCGSolver& operator=(const PetscSSORCGSolver&);
    //@}

};

}

#endif
