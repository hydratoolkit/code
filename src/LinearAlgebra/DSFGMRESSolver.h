//******************************************************************************
//! \file    src/LinearAlgebra/DSFGMRESSolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   Diagonally-scaled FGMRES
//******************************************************************************
#ifndef DSFGMRESSolver_h
#define DSFGMRESSolver_h

#include <NativeSolver.h>

namespace Hydra {

//! Diagonally-scaled FGMRES
class DSFGMRESSolver: public NativeSolver {

    friend class LASolverFactory;

  public:

    //! \name Constructor/Destructor
    //@{
             DSFGMRESSolver(Category& settings, DataContainer& dataContainer,
                            const string& name);
    virtual ~DSFGMRESSolver() {}
    //@}

    //! Initialize solver
    virtual void initialize(int nel, int globalNel);

    //! Finalize this object
    virtual void finalize();

    //! Dump solver data structures to stdout
    virtual int dump() const;

    //! Set solver operator
    virtual void setOperator(LAMatrix* A);

    //! Solve the system of equations
    virtual void solve(const LAVector* b, LAVector* x);

    //! Return the status of the solution
    virtual void getStatus(LASolverStatus& status) const;

  private:

    //! Don't permit copy or assignment operators
    //@{
    DSFGMRESSolver(const DSFGMRESSolver&);
    DSFGMRESSolver& operator=(const DSFGMRESSolver&);
    //@}

    Real computeResidual(int neq,
                         const Real* a,
                         const int* di,
                         const int* dj,
                         const Real* x,
                         const Real* b,
                         Real* r) const;

    void scaleVector(int neq, Real* v, Real factor) const;

    void applyPreconditioner(int neq, const Real* a, const int* di,
                             const int* dj, const Real* v, Real* z) const;

    void orthogonalize(int neq, int idir, Real** vv, Real* v, Real* hh) const;

    LAMatrix* m_A;

    //! Data indexes for temporary data
    DataIndex W;
    DataIndex V;
    DataIndex H;
    DataIndex C;
    DataIndex S;
    DataIndex RS;

    //! Computed quantities
    Real m_erra;
    Real m_errb;
    int  m_itknt;
    int  m_restart;
};

}

#endif
