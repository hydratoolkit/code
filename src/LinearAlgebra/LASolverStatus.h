//******************************************************************************
//! \file    src/LinearAlgebra/LASolverStatus.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   Abstract base class for solver status
//******************************************************************************
#ifndef LASolverStatus_h
#define LASolverStatus_h

#include <iostream>

#include <HydraTypes.h>

namespace Hydra {

//! Abstract base class for solver status
class LASolverStatus {

  public:

    //! \name Constructor/Destructor
    //@{
             LASolverStatus();
    virtual ~LASolverStatus() {}
    //@}

    //! Echo solver results
    void echoResults(ostream& output) const;

    //! Echo solver results
    void echoResults(ostream& output, const char* title) const;

    //! Return true if the solver converged
    bool isConverged() const { return m_reason >= 0; }

    //! Return divergence flag
    int getDivergenceReason() const { return m_reason; }

    //! Return reportConvergence flag
    bool reportConvergence() { return m_reportConvergence; }

    //! Set converged/diverged
    void setConvergence(int reason) { m_reason = reason; }

    //! Set iteration count
    void setIterationCount(int count) { m_count = count; }

    //! Set report convergence
    void setReportConvergence(bool repConv) { m_reportConvergence = repConv; }

    //! Set residual error
    void setResidualError(Real resErr) { m_residualError = resErr; }

    //! Set change in the solution error
    void setChangeError(Real changeErr) { m_changeError = changeErr; }

    inline int getIterCount() {return m_count;} //!< Get iteration count

   //! Get \f$\frac{\left| \left| r \right|\right|}{\left| \left| b \right|\right|} \f$
    inline Real getResidualError() {return m_residualError;}

   //! Get \f$\frac{\left| \left| \delta x \right|\right|}{\left| \left| x \right|\right|} \f$
    inline Real getChangeError() {return m_changeError;}

  private:

    //! Don't permit copy or assignment operators
    //@{
    LASolverStatus(const LASolverStatus&);
    LASolverStatus& operator=(const LASolverStatus&);
    //@}

    bool m_reportConvergence;
    int m_reason;    //!< PETSc KSPConvergedReason
    int m_count;     //!< Iteration count
    Real m_residualError; //!< ||r || / ||b||
    Real m_changeError;   //!< ||dx|| / ||x||

};

}

#endif
