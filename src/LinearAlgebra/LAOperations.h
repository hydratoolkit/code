//******************************************************************************
//! \file    src/LinearAlgebra/LAOperations.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   Matrix-vector operatios
//******************************************************************************
#ifndef Operations_h
#define Operations_h

namespace Hydra {

// Forward declarations
class LAMatrix;
class LAVector;
class PetscMatrix;
class PetscVector;
class NativeMatrix;
class NativeVector;


int MatrixVectorMultiply(const LAMatrix& a, const LAVector& x, LAVector& b);

int MatrixVectorMultiply(const PetscMatrix& a, const PetscVector& x,
                         PetscVector& b);

int MatrixVectorMultiply(const NativeMatrix& a, const NativeVector& x,
                         NativeVector& b);

}

#endif
