//******************************************************************************
//! \file    src/LinearAlgebra/JacobiCGSolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   PETSc Jacobi pre-conditioner and the PETSc CG solver
//******************************************************************************
#ifndef JacobiCGSolver_h
#define JacobiCGSolver_h

#include <PetscSolver.h>

namespace Hydra {

//! PETSc Jacobi pre-conditioner and the PETSc CG solver
class JacobiCGSolver: public PetscSolver
{
    friend class LASolverFactory;

  public:

    //! \name Constructor/Destructor
    //@{
             JacobiCGSolver(Category&, DataContainer&, const string&);
    virtual ~JacobiCGSolver() {}
    //@}

    virtual void initializeKSP();

  private:

    //! Don't permit copy or assignment operators
    //@{
    JacobiCGSolver(const JacobiCGSolver&);
    JacobiCGSolver& operator=(const JacobiCGSolver&);
    //@}

};

}

#endif
