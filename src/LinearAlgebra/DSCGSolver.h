//******************************************************************************
//! \file    src/LinearAlgebra/DSCGSolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   Mark's serial CG solver implementation
//******************************************************************************
#ifndef DSCGSolver_h
#define DSCGSolver_h

#include <NativeSolver.h>

namespace Hydra {

//! Mark's serial CG solver implementation
class DSCGSolver: public NativeSolver {

    friend class LASolverFactory;

  public:

    //! \name Constructor/Destructor
    //@{
             DSCGSolver(Category& settings, DataContainer& dataContainer,
                        const string& name);
    virtual ~DSCGSolver() {}
    //@}

    //! Initialize solver
    virtual void initialize(int nel, int globalNel);

    //! Finalize this object
    virtual void finalize();

    //! Dump solver data structures to stdout
    virtual int dump() const;

    //! Set solver operator
    virtual void setOperator(LAMatrix* A);

    //! Solve the system of equations
    virtual void solve(const LAVector* b, LAVector* x);

    //! Return the status of the solution
    virtual void getStatus(LASolverStatus& status) const;

  private:

    //! Don't permit copy or assignment operators
    //@{
    DSCGSolver(const DSCGSolver&);
    DSCGSolver& operator=(const DSCGSolver&);
    //@}

    //! Original solver routine
    void dscga(Real* a, Real* x, const Real* b, Real* p, Real* r,
               Real* ap, Real* z, Real* rmi, int neq,
               const int* di, const int* dj,
               int itchk, int itmax, Real eps,
               Real& erra, Real& errb, int& itknt);

    LAMatrix* m_A;

    //! Data indexes for temporary data
    DataIndex PP;
    DataIndex AP;
    DataIndex W;
    DataIndex R;
    DataIndex Z;
    DataIndex TMP;

    //! Computed quantities
    Real m_erra;
    Real m_errb;
    int  m_itknt;
};

}

#endif
