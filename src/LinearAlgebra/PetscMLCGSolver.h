//******************************************************************************
//! \file    src/LinearAlgebra/PetscMLCGSolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   ML pre-conditioner and the PETSc CG solver
//******************************************************************************
#ifndef PetscMLCGSolver_h
#define PetscMLCGSolver_h

#include <PetscSolver.h>

namespace Hydra {

//! PETSc - ML CG Solver
class PetscMLCGSolver: public PetscSolver
{
    friend class LASolverFactory;

  public:

    //! \name Constructor/Destructor
    //@{
    PetscMLCGSolver(Category&, DataContainer&, const string&);
    virtual ~PetscMLCGSolver() {}
    //@}

    virtual void initializeKSP();
    virtual void initializePC();
    virtual void setOperator(LAMatrix* A);

  private:

    //! Don't permit copy or assignment operators
    //@{
    PetscMLCGSolver(const PetscMLCGSolver&);
    PetscMLCGSolver& operator=(const PetscMLCGSolver&);
    //@}

    int m_operatorNumber;

};

}

#endif
