//******************************************************************************
//! \file    src/LinearAlgebra/ILUGMRESSolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   Petsc ASM+ILU pre-conditioner and the GMRES solver
//******************************************************************************
#ifndef ILUGMRESSolver_h
#define ILUGMRESSolver_h

#include <JacobiGMRESSolver.h>

namespace Hydra {

//! Petsc ASM+ILU pre-conditioner and the GMRES solver
class ILUGMRESSolver: public JacobiGMRESSolver
{
    friend class LASolverFactory;

  public:

    //! \name Constructor/Destructor
    //@{
             ILUGMRESSolver(Category&, DataContainer&, const string& name);
    virtual ~ILUGMRESSolver() {}
    //@}

    //! Initialize the krylov solver
    virtual void initializeKSP();

    //! Initialize the preconditioner
    virtual void initializePC();

  private:

    //! Don't permit copy or assignment operators
    //@{
    ILUGMRESSolver(const ILUGMRESSolver&);
    ILUGMRESSolver& operator=(const ILUGMRESSolver&);
    //@}

};

}

#endif
