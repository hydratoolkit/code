//******************************************************************************
//! \file    src/LinearAlgebra/NativeVector.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   Vector class for non-PETSc solvers
//******************************************************************************
#ifndef NativeVector_h
#define NativeVector_h

#include <LAVector.h>
#include <DataContainer.h>

namespace Hydra {

//! Vector class for use with CFD linear algebra objects.  There should not be
//! any copying that occurs with this class.  This means that the data stored
//! by the data container should change as the vector is changed.
class NativeVector: public LAVector {

    friend class NativeSolver;

  public:

    //! \name Constructor/Destructor
    //@{
             NativeVector(DataContainer& dataContainer, int N);
    virtual ~NativeVector();
    //@}

    //! Initialize vector using a data container index
    virtual void initialize(int n, DataIndex a);

    //! Initialize vector using a count and a pointer.  The pointer not owned
    virtual void initialize(int n, Real* a);

    //! Deallocate any memory allocated by this vector
    virtual void finalize();

    //! Return items from a vector
    virtual void get(int n, const int* indices, Real* array) const;

    //! Return local size
    virtual void getLocalSize(int& n) const;

    //! Override a block of values in the vector
    virtual void set(int n, const int* i, const Real* a);

    //! Add a block of values to the vector
    virtual void add(int n, const int* i, const Real* a);

    //! Assemble global vector if running in parallel
    virtual void assemble();

    //! Dump vector to stdout
    virtual void dump() const;

    //! Return true if this is a non-PETSc (native) vector
    virtual bool isNative() const;

    //! Zero the vector
    virtual void zero();

    //! Get pointer to vector data
    Real* getA() const {
      return m_ptr != 0 ? m_ptr : m_dataContainer.getVariable<Real>(m_a);
    }

  private:

    //! Don't permit copy or assignment operators
    //@{
    NativeVector(const NativeVector&);
    NativeVector& operator=(const NativeVector&);
    //@}

    int m_n;
    DataIndex m_a;
    Real* m_ptr; //! Used when initialized with pointer
};

}

#endif
