//******************************************************************************
//! \file    src/LinearAlgebra/lapack.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   Interface for lapack code
//******************************************************************************

#ifndef LAPACK_H
#define LAPACK_H

#include <HydraTypes.h>

extern "C" {
  void FORTRAN(dgetrs)(char *, int &, int &, double *, int &, int *,
                       double *, int &, int &);

  void FORTRAN(dgetrf)(int &, int &, double *, int &, int *, int &);
}

#endif //LAPACK_H
