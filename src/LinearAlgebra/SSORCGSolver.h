//******************************************************************************
//! \file    src/LinearAlgebra/SSORCGSolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   Mark's CG + SSOR solver
//******************************************************************************
#ifndef SSORCGSolver_h
#define SSORCGSolver_h

#include <NativeSolver.h>

namespace Hydra {

// Forward declarations
class LAMatrix;


//! Mark's CG + SSOR solver
class SSORCGSolver: public NativeSolver {

    friend class LASolverFactory;

  public:

    //! \name Constructor/Destructor
    //@{
             SSORCGSolver(Category& settings, DataContainer& dataContainer,
                          const string& name);
    virtual ~SSORCGSolver(){}
    //@}

    //! Initialize solver with LHS operator
    virtual void initialize(int nel, int globalNel);

    //! Finalize this object
    virtual void finalize();

    //! Dump solver data structures to stdout
    virtual int dump() const;

    //! Set solver operator
    virtual void setOperator(LAMatrix* A);

    //! Solve the system of equations
    virtual void solve(const LAVector* b, LAVector* x);

    //! Return the status of the solution
    virtual void getStatus(LASolverStatus& status) const;

  private:

    //! Don't permit copy or assignment operators
    //@{
    SSORCGSolver(const SSORCGSolver&);
    SSORCGSolver& operator=(const SSORCGSolver&);
    //@}

    //! Original solver routines
    void ssorcg(const Real* a, Real* x, const Real* b, Real* p,
                Real* r, Real* ap, Real* z, Real* tmp, Real w,
                int neq, const int* di, const int* dj,
                int itchk, int itmax, Real eps,
                Real& erra, Real& errb, int& itknt);

    void ssorb(const Real* a, Real* b, Real* x, Real w,
               const int* di, const int* dj, int neq);

    //! Operator
    LAMatrix* m_A;

    //! Data indexes for temporary data
    DataIndex PP;
    DataIndex AP;
    DataIndex W;
    DataIndex R;
    DataIndex Z;
    DataIndex TMP;

    //! Settings from control object
    Real m_omega;

    //! Computed quantities
    Real m_erra;
    Real m_errb;
    int  m_itknt;
};

}

#endif
