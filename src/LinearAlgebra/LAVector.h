//******************************************************************************
//! \file    src/LinearAlgebra/LAVector.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   Virtual base class for linear algebra vectors
//******************************************************************************
#ifndef LAVector_h
#define LAVector_h

#include <HydraTypes.h>

struct _p_Vec;
typedef struct _p_Vec* Vec;

namespace Hydra {

class DataContainer;
class Matrix;

//! LAVector class for use with CFD linear algebra objects.  There should not be
//! any copying that occurs with this class.  This means that the data stored
//! by the data container should change as the vector is changed.
class LAVector {

  friend int MatrixLAVectorMultiply(const Matrix&, const LAVector&, LAVector&);
  friend class PetscSolver;

  public:

    //! \name Constructor/Destructor
    //@{
             LAVector(DataContainer& dataContainer, int N);
    virtual ~LAVector();
    //@}

    //! Add a block of values to the vector
    virtual void add(int n, const int* i, const Real* a) = 0;

    //! Assemble global vector if running in parallel
    virtual void assemble() = 0;

    //! Dump vector to stdout
    virtual void dump() const = 0;

    //! Return global size - return PETSc error code - for consistency...
    int getGlobalSize(int& n) const { n = m_N; return 0; }

    //! Deallocate any memory allocated by this vector
    virtual void finalize() = 0;

    //! Return items from a vector
    virtual void get(int n, const int* indices, Real* array) const = 0;

    //! Return local size
    virtual void getLocalSize(int& n) const = 0;

    //! Initialize vector using a data container index
    virtual void initialize(int n, DataIndex a) = 0;

    //! Initialize vector using a count and a pointer
    virtual void initialize(int n, Real* a) = 0;

    //! Return true if this is a non-PETSc (native) vector
    virtual bool isNative() const = 0;

    //! Override a block of values in the vector
    virtual void set(int n, const int* i, const Real* a) = 0;

    //! Zero the matrix
    virtual void zero() = 0;

  protected:

    DataContainer& m_dataContainer; // Data Container w/vector memory
    int m_N; // Number of items

  private:

    //! Don't permit copy or assignment operators
    //@{
    LAVector(const LAVector&);
    LAVector& operator=(const LAVector&);
    //@}

};

}

#endif
