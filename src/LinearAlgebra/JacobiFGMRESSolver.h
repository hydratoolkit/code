//******************************************************************************
//! \file    src/LinearAlgebra/JacobiFGMRESSolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   PETSc Jacobi pre-conditioner and the PETSc FGMRES solver
//******************************************************************************
#ifndef JacobiFGMRESSolver_h
#define JacobiFGMRESSolver_h

#include <PetscSolver.h>

namespace Hydra {

//! PETSc Jacobi pre-conditioner and the PETSc FGMRES solver
class JacobiFGMRESSolver: public PetscSolver
{
    friend class LASolverFactory;

  public:

    //! \name Constructor/Destructor
    //@{
             JacobiFGMRESSolver(Category&, DataContainer&, const string&);
    virtual ~JacobiFGMRESSolver();
    //@}

    //! Initialize the krylov solver
    virtual void initializeKSP();

    //! Check convergence of solution
    virtual void checkConvergence(int it, Real rnorm, int& reason);

    virtual void initializeWorkspace(int nel, int globalNel);

  protected:

    bool m_doingRestart;  //<! Indicate that FGMRES is restarting
    int  m_Nrestart;      //<! No. of restart vectors

  private:

    //! Don't permit copy or assignment operators
    //@{
    JacobiFGMRESSolver(const JacobiFGMRESSolver&);
    JacobiFGMRESSolver& operator=(const JacobiFGMRESSolver&);
    //@}

    DataIndex TSOLUTION;       //<! Store current result for convergence test
    PetscVector* m_tsolution;  //<! Current solution

};

}

#endif
