//******************************************************************************
//! \file    src/LinearAlgebra/LASolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   Abstract base class for linear-algebra solvers
//******************************************************************************
#ifndef LASolver_h
#define LASolver_h

#include<string>

#include <HydraTypes.h>

namespace Hydra {

class DataContainer;
class LAMatrix;
class LAVector;
class LASolverFactory;
class LASolverStatus;
class Category;

//! Abstract base class for linear algebra solvers
class LASolver {

  public:

    //! \name Constructor/Destructor
    //@{
             LASolver(Category&, DataContainer&, const string&);
    virtual ~LASolver();
    //@}

    //! Create a matrix that may be used with this solver
    virtual LAMatrix* createMatrix(int N, int M) const = 0;

    //! Create a vector that may be used with this solver
    virtual LAVector* createVector(int N) const = 0;

    //! Dump solver data structures to stdout
    virtual int dump() const = 0;

    //! Finalize this object
    virtual void finalize() = 0;

    //! Initialize solver
    virtual void initialize(const int nel, const int globalNel) = 0;

    //! Return the status of the solution
    virtual void getStatus(LASolverStatus& status) const = 0;

    //! Return the verbose flag setting
    virtual bool getVerbose() const = 0;

    //! Set solver operator
    virtual void setOperator(LAMatrix* A) = 0;

    //! Set convergence settings and tolerances
    virtual void setTolerances(int itmax, int itchk, Real eps) = 0;

    //! Turn convergence/diagnostic output on or off
    virtual void setVerbose(bool report, bool verbose) = 0;

    //! Solve the system of equations
    virtual void solve(const LAVector* b, LAVector* x) = 0;

  protected:

    //! Get solver name for options
    string& m_getName() {return m_name;}

    // Data Members
    Category& m_settings;           //!< Category category for solver
    DataContainer& m_dataContainer; //!< DataContainer for variable registration
    string m_name;                  //!< Name of the specific solver

   private:

    //! Don't permit copy or assignment operators
    //@{
    LASolver(const LASolver&);
    LASolver& operator=(const LASolver&);
    //@}

};

}

#endif
