# ############################################################################ #
#
# Library: LinearAlgebra
# File Definition File
#
############################################################################# #

# Source Files

set(LinearAlgebra_SOURCE_FILES
  DSCGSolver.C
  SSORCGSolver.C
  AConjugateSolver.C
  DSFGMRESSolver.C
  JacobiCGSolver.C
  JacobiFGMRESSolver.C
  JacobiGMRESSolver.C
  ILUFGMRESSolver.C
  ILUGMRESSolver.C
  LAMatrix.C
  LAOperations.C
  LASolver.C
  LAVector.C
  LASolverFactory.C
  LASolverStatus.C
# Trilinos version -- omit for now
# MLCGSolver.C
  NativeMatrix.C
  NativeSolver.C
  NativeVector.C
  PetscMatrix.C
  PetscSolver.C
  PetscVector.C
  PetscMLCGSolver.C
  PetscHypreCGSolver.C
  PetscSSORCGSolver.C
)



