//******************************************************************************
//! \file    src/LinearAlgebra/NativeSolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   Base class for linear solvers not using PETSc
//******************************************************************************
#ifndef NativeSolver_h
#define NativeSolver_h

#include <LASolver.h>

namespace Hydra {

//! Simple Native linear solver
class NativeSolver: public LASolver {

    friend class LASolverFactory;

  public:

    //! \name Constructor/Destructor
    //@{
             NativeSolver(Category& settings, DataContainer& dataContainer,
                          const string& name);
    virtual ~NativeSolver() {}
    //@}

    //! Create a matrix that may be used with this solver
    virtual LAMatrix* createMatrix(int N, int M) const;

    //! Create a vector that may be used with this solver
    virtual LAVector* createVector(int N) const;

    //! Finalize this object
    virtual void finalize();

    //! Return the verbose flag setting
    virtual bool getVerbose() const;

    //! Initialize solver
    virtual void initialize(int nel, int globalNel);

    //! Set convergence settings and tolerances
    virtual void setTolerances(int itmax, int itchk, Real eps);

    //! Turn convergence/diagnostic output on or off
    virtual void setVerbose(bool report, bool verbose);

  protected:

    //! Dot product
    static Real ddot(int n, const Real* v1, int is1, const Real* v2, int is2);

    //!  Matrix-vector multiply
    static void smva(const Real* a, const Real* x, Real* b,
                     const int* di, const int* dj, int neq);

    //!  Add a scaled vector
    static void daxpy(int n, Real s, const Real* x, int isx, Real* y, int isy);

    //! Data members
    int m_itmax;
    int m_itchk;
    Real m_eps;
    bool m_report;
    bool m_verbose;

  private:

    //! Don't permit copy or assignment operators
    //@{
    NativeSolver(const NativeSolver&);
    NativeSolver& operator=(const NativeSolver&);
    //@}

};

}

#endif
