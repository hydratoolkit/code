//******************************************************************************
//! \file    src/LinearAlgebra/AConjugateSolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   A-conjugate projection w. CG
//******************************************************************************
#ifndef AConjugateSolver_h
#define AConjugateSolver_h

// Forward declarations
struct _p_Vec;
typedef struct _p_Vec* Vec;

// Begin local includes
#include <PetscSolver.h>

namespace Hydra {

//! CFD Solver using Mark's serial CG solver implementation
class AConjugateSolver: public PetscSolver {

    friend class LASolverFactory;

  public:

    //! \name Constructor/Destructor
    //@{
             AConjugateSolver(Category& settings, DataContainer& dataContainer,
                              const string& name);
    virtual ~AConjugateSolver() {}
    //@}

    //! Initialize preconditioner
    virtual void initializePC();

    //! Initialize solver
    virtual void initializeKSP();

    //! Initialize workspace
    virtual void initializeWorkspace(int nel, int globalNel);

    //! Finalize this object
    virtual void finalize();

    //! Set solver operator
    virtual void setOperator(LAMatrix* A);

    //! Solve the system of equations
    virtual void solve(const LAVector* b, LAVector* x);

  private:

    //! Don't permit copy or assignment operators
    //@{
    AConjugateSolver(const AConjugateSolver&);
    AConjugateSolver& operator=(const AConjugateSolver&);
    //@}

    //! Update phi vectors
    void updatePhi(Real aNorm, const Vec& xn, const Vec& dxn, const Vec& ax);

    //! Register/unregister temporary data and create/destroy PETSc vector
    void registerVec(int n, int ng, const char* name, DataIndex& di, Vec& vec);
    void unregisterVec(DataIndex& di, Vec& vec);

    // Operator
    LAMatrix* m_A;

    // Temporary data
    DataIndex* m_PHI;
    Vec* m_phi;
    Real* m_alpha;
    DataIndex m_AX;
    Vec m_ax;
    DataIndex m_DXN;
    Vec m_dxn;

    // Settings from control object
    int m_nphi; // Number of phi vectors active
    int m_mphi; // Maximum number of phi vectors

};

}

#endif
