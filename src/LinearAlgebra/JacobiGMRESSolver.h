//******************************************************************************
//! \file    src/LinearAlgebra/JacobiGMRESSolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   PETSc Jacobi pre-conditioner and the PETSc GMRES solver
//******************************************************************************
#ifndef JacobiGMRESSolver_h
#define JacobiGMRESSolver_h

#include <PetscSolver.h>

namespace Hydra {

//! PETSc Jacobi pre-conditioner and the PETSc GMRES solver
class JacobiGMRESSolver: public PetscSolver
{
    friend class LASolverFactory;

  public:

    //! \name Constructor/Destructor
    //@{
             JacobiGMRESSolver(Category&, DataContainer&, const string&);
    virtual ~JacobiGMRESSolver() {}
    //@}

    //! Initialize the krylov solver
    virtual void initializeKSP();

    //! Check convergence of solution
    virtual void checkConvergence(int it, Real rnorm, int& reason);

    //! Initialize temporary workspace
    virtual void initializeWorkspace(int nel, int globalNel);

  protected:

    bool m_doingRestart;  //<! Indicate that FGMRES is restarting
    int  m_Nrestart;      //<! No. of restart vectors

  private:

    //! Don't permit copy or assignment operators
    //@{
    JacobiGMRESSolver(const JacobiGMRESSolver&);
    JacobiGMRESSolver& operator=(const JacobiGMRESSolver&);
    //@}

    DataIndex TSOLUTION;       //<! Store solution for convergence test
    PetscVector* m_tsolution;  //<! Current solution

};

}

#endif
