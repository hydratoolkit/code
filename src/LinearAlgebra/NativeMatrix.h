//******************************************************************************
//! \file    src/LinearAlgebra/NativeMatrix.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   Matrix class for solvers that don't use PETSc
//******************************************************************************
#ifndef NativeMatrix_h
#define NativeMatrix_h

#include <LAMatrix.h>
#include <DataContainer.h>

namespace Hydra {

class LAVector;


class NativeMatrix: public LAMatrix {

    friend class NativeSolver;

  public:

    //! \name Constructor/Destructor
    //@{
             NativeMatrix(DataContainer& dataContainer, int M, int N);
    virtual ~NativeMatrix();
    //@}

    //! Initialize matrix using two vectors of diagonal and off
    virtual void initialize(int m, int n,
                            DataIndex i,  DataIndex j,  DataIndex a,
                            DataIndex oi, DataIndex oj, DataIndex oa);

    //! Initialize matrix using information on number of non-zero terms per row
    virtual void initialize(int m, int n, const int* dnz, const int* onz);

    //! Initialize matrix using a constant number of non-zero terms per row.
    virtual void initialize(int m, int n, int ndnz, int nonz);

    //! Deallocate any memory allocated by this matrix
    virtual void finalize();

    //! Return items from a matrix
    virtual void get(int m, const int* i, int n, const int* j, Real* a) const;

    //! Return diagonal of a matrix in the provided vector
    virtual void getDiagonal(LAVector* v) const;

    //! Return local sizes
    virtual void getLocalSize(int& m, int& n) const;

    //! Return equation row ownership range
    virtual void getOwnershipRange(int&m, int& n) const;

    //! Override a block of values in the matrix
    virtual void set(int m, const int* i, int n, const int* j, const Real* a);

    //! Set diagonal of a matrix using values from the provided vector
    virtual void setDiagonal(const LAVector* v);

    //! Add a block of values to the matrix
    virtual void add(int m, const int* i, int n, const int* j, const Real* a);

    //! Assemble global matrix if running in parallel
    virtual void assemble();

    //! Dump matrix to stdout
    virtual void dump() const;

    //! Return true if this is a non-PETSc (native) matrix
    virtual bool isNative() const;

    //! Zero the matrix
    virtual void zero();

    //! Get pointer to diagonal sub-matrix
    Real* getA() const { return m_dataContainer.getVariable<Real>(m_a); }

    //! Get pointer to diagonal sub-matrix offsets
    int* getDI() const { return m_dataContainer.getVariable<int>(m_i); }

    //! Get pointer to diagonal sub-matrix column numbers
    int* getDJ() const { return m_dataContainer.getVariable<int>(m_j); }

    //! Get pointer to off-diagonal sub-matrix
    Real* getOA() const { return m_dataContainer.getVariable<Real>(m_oa); }

    //! Get pointer to off-diagonal sub-matrix offsets
    int* getOI() const { return m_dataContainer.getVariable<int>(m_oi); }

    //! Get pointer to off-diagonal sub-matrix column numbers
    int* getOJ() const { return m_dataContainer.getVariable<int>(m_oj); }

  private:

    //! Don't permit copy or assignment operators
    //@{
    NativeMatrix(const NativeMatrix&);
    NativeMatrix& operator=(const NativeMatrix&);
    //@}

    int m_m;         //!< Local number of rows
    int m_n;         //!< Local number of columns
    DataIndex m_i;   //!< Offsets for diagonal sub-matrix
    DataIndex m_j;   //!< Column equation numbers for diagonal sub-matrix
    DataIndex m_a;   //!< Diagonal sub-matrix
    DataIndex m_oi;  //!< Offsets for off-diagonal sub-matrix
    DataIndex m_oj;  //!< Column equation numbers for off-diagonal sub-matrix
    DataIndex m_oa;  //!< Diagonal sub-matrix
    bool m_allocate; //!< Memory allocated on data container for this matrix
};

}

#endif
