//******************************************************************************
//! \file    src/LinearAlgebra/AConjugateSolver.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   A-conjugate projection w. CG
//******************************************************************************
#include <sstream>
#include <string>

using namespace std;

#include <petscksp.h>
#include <petscpc.h>
#include <petscmat.h>
#include <petscvec.h>

#include <AConjugateSolver.h>
#include <LAOperations.h>
#include <PetscMatrix.h>
#include <PetscVector.h>
#include <IterativeSolverControl.h>
#include <DataContainer.h>
#include <MPIWrapper.h>

using namespace Hydra;

AConjugateSolver::AConjugateSolver(Category& settings,
                                   DataContainer& dataContainer,
                                   const string& name):
  PetscSolver(settings, dataContainer, name),
  m_PHI(0),
  m_phi(0),
  m_alpha(0),
  m_AX(UNREGISTERED),
  m_DXN(UNREGISTERED),
  m_nphi(0),
  m_mphi(10)
/*******************************************************************************
Routine: AConjugateSolver - constructor
Author : Mark A. Christon
*******************************************************************************/
{
  m_solverType = KSPCG;
  m_preconditionerType = PCJACOBI;
  m_mphi = m_settings.getOptionVal(IterativeSolverControl::MAX_PHI);
}

void
AConjugateSolver::initializePC()
/*******************************************************************************
Routine: initializePC - initialize preconditioner
Author : Mark A. Christon
*******************************************************************************/
{
  // Set preconditioner flags
  PC pc;
  KSPGetPC(m_solver, &pc);
  PCSetType(pc, m_preconditionerType);
  KSPSetInitialGuessNonzero(m_solver, PETSC_TRUE);
  PCSORSetOmega(pc, 1.2);
}

void
AConjugateSolver::initializeKSP()
/*******************************************************************************
Routine: initializeKSP - initialize PETSC Krylov solver
Author : Mark A. Christon
*******************************************************************************/
{
  PetscSolver::initializeKSP();

  // Set convergence tolerances
  int itmax = m_settings.getOptionVal(IterativeSolverControl::ITMAX);
  int itchk = m_settings.getOptionVal(IterativeSolverControl::ITCHK);
  Real eps  = m_settings.getParamVal(IterativeSolverControl::EPS);
  setTolerances(itmax, itchk, eps);
  bool report      = m_settings.getFlagVal(IterativeSolverControl::CONVERGENCE);
  bool diagnostics = m_settings.getFlagVal(IterativeSolverControl::DIAGNOSTICS);

  if (diagnostics)
    p0cout << "\tLinear Solver: Initialize A-Conjugate Projection solver"
           << endl;
  setVerbose(report, diagnostics);

  KSPSetFromOptions(m_solver);
}

void
AConjugateSolver::initializeWorkspace(int nel, int globalNel)
/*******************************************************************************
Routine: initializeWorkspace - initialize workspace for PETSC
Author : Mark A. Christon
*******************************************************************************/
{
  PetscSolver::initializeWorkspace(nel, globalNel);

  // Allocate small temporary data
  if (m_mphi > 0) {
    m_PHI = m_dataContainer.allocTempInt(m_mphi);
    m_phi = reinterpret_cast<Vec*>(m_dataContainer.allocTempData(m_mphi*sizeof(Vec)));
    m_alpha = m_dataContainer.allocTempReal(m_mphi);
    for (int i = 0; i < m_mphi; i++)
      m_alpha[i] = 0.0;
  }

  // Register data for all possible phi vectors
  for (int i = 0; i < m_mphi; i++) {

    string name("PHI-");
    string id;
    stringstream sout;
    sout << i;
    id = sout.str();
    name.append(id);
    registerVec(nel, globalNel, name.c_str(), m_PHI[i], m_phi[i]);
  }

  // Register temporary data for dxn and ax
  registerVec(nel, globalNel, "DXN", m_DXN, m_dxn);
  registerVec(nel, globalNel, "AX",  m_AX,  m_ax);
}

void
AConjugateSolver::setOperator(LAMatrix* A)
/*******************************************************************************
Routine: setOperator - Set the operator for the current solver
Author : Mark A. Christon
*******************************************************************************/
{
  PetscSolver::setOperator(A);
  m_A = A; // Save A for use in solve method
}

void
AConjugateSolver::finalize()
/*******************************************************************************
Routine: finalize - free all data, PETSC finalize
Author : Mark A. Christon
*******************************************************************************/
{
  for (int i = 0; i < m_mphi; i++)
    unregisterVec(m_PHI[i], m_phi[i]);

  unregisterVec(m_DXN, m_dxn);
  unregisterVec(m_AX, m_ax);

  if (m_mphi > 0) {
    m_dataContainer.freeTempData(m_PHI);
    m_dataContainer.freeTempData(m_phi);
    m_dataContainer.freeTempData(m_alpha);
  }

  PetscSolver::finalize();
}

void
AConjugateSolver::solve(const LAVector* B, LAVector* X)
/*******************************************************************************
Routine: solve - solve Ax=b
Author : Mark A. Christon
*******************************************************************************/
{
  // Extract PETSc Vec data structures
  const Vec& b = reinterpret_cast<const PetscVector*>(B)->constGetVec();
  Vec& xn = reinterpret_cast<PetscVector*>(X)->getVec();
  Mat& A  = reinterpret_cast<PetscMatrix*>(m_A)->getMat();

  // Compute alpha new guess
  if (m_mphi > 0 && (m_nphi == 0 || m_nphi == m_mphi)) {
    VecZeroEntries(m_dxn);
  }
  else if (m_nphi > 0) {
    VecMDot(b, m_nphi, m_phi, m_alpha);    // alpha
    VecMAXPY(xn, m_nphi, m_alpha, m_phi);  // guess
    VecCopy(xn, m_dxn);                    // Save xn
  }

  // Solve system Ax = b

  PetscSolver::solve(B, X);

  // Compute dxn
  VecAYPX(m_dxn, -1.0, xn);

  // Compute A norm
  Real aNorm = 0.0;
  if (m_mphi > 0 && (m_nphi == 0 || m_nphi == m_mphi)) {
    MatMult(A, xn, m_ax);
    VecDot(xn, m_ax, &aNorm);
  }
  else if (m_nphi > 0) {
    MatMult(A, m_dxn, m_ax);
    VecDot(m_dxn, m_ax, &aNorm);
  }
  aNorm = sqrt(aNorm);

  // Update phi vectors if the contribution is large enough
  Real normTol = m_settings.getParamVal(IterativeSolverControl::ANORM_TOL);
  if (m_mphi > 0 && aNorm > normTol)
    updatePhi(aNorm, xn, m_dxn, m_ax);
}

void
AConjugateSolver::updatePhi(Real aNorm, const Vec& xn, const Vec& dxn,
                            const Vec& ax)
/*******************************************************************************
Routine: updatePhi - update A-conjugate base vectors
Author : Mark A. Christon
*******************************************************************************/
{
  // Update phi if the solution contributes enough
  if (m_nphi == 0 || m_nphi == m_mphi) {
    VecCopy(xn, m_phi[0]);
    VecScale(m_phi[0], 1.0/aNorm);
    m_nphi = 1;
  }
  else {
    // Compute new alphas based on Ax - flip sign on alphas
    Real alp2 = 0.0;
    VecMDot(ax, m_nphi, m_phi, m_alpha);
    for (int i = 0; i < m_nphi; i++) {
      alp2 += m_alpha[i]*m_alpha[i];
      m_alpha[i] = -m_alpha[i];
    }

    // Start construction of new orthogonal vector starting with dxn
    Real pdot = 0.0;
    VecDot(dxn, ax, &pdot);
    Real aInv = 1.0/sqrt(pdot - alp2);
    VecCopy(dxn, m_phi[m_nphi]);

    // Subtract contributions of other phi vectors
    VecMAXPY(m_phi[m_nphi], m_nphi, m_alpha, m_phi);
    // Should flip sign on alpha back here if it is to be used later

    // Scale the new vector & update the number of vectors
    VecScale(m_phi[m_nphi], aInv);
    m_nphi++;
  }
}

void
AConjugateSolver::registerVec(int n, int nglobal, const char* name,
                              DataIndex& di, Vec& vec)
/*******************************************************************************
Routine: registerVec - register a vector
Author : Mark A. Christon
*******************************************************************************/
{
  int nproc = g_comm->getNproc();
  di = m_dataContainer.registerVariable(n, sizeof(Real), name);
  Real* v = m_dataContainer.getVariable<Real>(di);
  if (nproc > 1) {
      VecCreateMPIWithArray(PETSC_COMM_WORLD, 1, n, nglobal, v, &vec);
  } else {
      VecCreateSeqWithArray(PETSC_COMM_SELF, 1, n, v, &vec);
  }
  VecAssemblyBegin(vec);
  VecAssemblyEnd(vec);
}

void
AConjugateSolver::unregisterVec(DataIndex& di, Vec& vec)
/*******************************************************************************
Routine: unregisterVec - free vectors
Author : Mark A. Christon
*******************************************************************************/
{
  m_dataContainer.freeVariable(di);
  VecDestroy(&vec);
}
