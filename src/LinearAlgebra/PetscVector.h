//******************************************************************************
//! \file    src/LinearAlgebra/PetscVector.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   PETSc vector
//******************************************************************************
#ifndef PetscVector_h
#define PetscVector_h

// Includes

// Forward declarations
struct _p_Vec;
typedef struct _p_Vec* Vec;

#include <LAVector.h>

namespace Hydra {

class DataContainer;
class Matrix;


//! Vector class for use with HYDRA linear algebra objects.  There should not be
//! any copying that occurs with this class.  This means that the data stored
//! by the data container should change as the vector is changed.
class PetscVector: public LAVector {

    friend class PetscSolver;

  public:

    //! \name Constructor/Destructor
    //@{
             PetscVector(DataContainer& dataContainer, int N);
    virtual ~PetscVector();
    //@}

    //! Initialize vector using a data container index
    virtual void initialize(int n, DataIndex a);

    //! Initialize vector using a count and a pointer
    virtual void initialize(int n, Real* a);

    //! Deallocate any memory allocated by this vector
    virtual void finalize();

    //! Return items from a vector
    virtual void get(int n, const int* indices, Real* array) const;

    //! Return local size
    virtual void getLocalSize(int& n) const;

    //! Override a block of values in the vector
    virtual void set(int n, const int* i, const Real* a);

    //! Add a block of values to the vector
    virtual void add(int n, const int* i, const Real* a);

    //! Assemble global vector if running in parallel
    virtual void assemble();

    //! Dump vector to stdout
    virtual void dump() const;

    //! Return true if this is a non-PETSc (native) vector
    virtual bool isNative() const;

    //! Zero the vector
    virtual void zero();

    //! Return PETSc vector
    Vec& getVec() { return m_Vec; }

    //! Return PETSc matrix
    const Vec& constGetVec() const { return m_Vec; }

  private:

    //! Don't permit copy or assignment operators
    //@{
    PetscVector(const PetscVector&);
    PetscVector& operator=(const PetscVector&);
    //@}

    Vec  m_Vec; // PETSc vector
    bool m_initialized;

};

}

#endif
