//******************************************************************************
//! \file    src/LinearAlgebra/ILUFGMRESSolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   Petsc ASM+ILU pre-conditioner and the FGMRES solver
//******************************************************************************
#ifndef ILUFGMRESSolver_h
#define ILUFGMRESSolver_h

#include <JacobiFGMRESSolver.h>

namespace Hydra {

//! Petsc ASM+ILU pre-conditioner and the FGMRES solver
class ILUFGMRESSolver: public JacobiFGMRESSolver
{
    friend class LASolverFactory;

  public:

    //! \name Constructor/Destructor
    //@{
             ILUFGMRESSolver(Category&, DataContainer&, const string&);
    virtual ~ILUFGMRESSolver() {}
    //@}


    //! Initialize the krylov solver
    virtual void initializeKSP();

    //! Initialize the preconditioner
    virtual void initializePC();

  private:

    //! Don't permit copy or assignment operators
    //@{
    ILUFGMRESSolver(const ILUFGMRESSolver&);
    ILUFGMRESSolver& operator=(const ILUFGMRESSolver&);
    //@}

};

}

#endif
