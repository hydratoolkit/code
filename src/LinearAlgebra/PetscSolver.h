//******************************************************************************
//! \file    src/LinearAlgebra/PetscSolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   PETSc Solver base class
//******************************************************************************
#ifndef PetscSolver_h
#define PetscSolver_h

// Forward declarations
struct _p_KSP;
typedef struct _p_KSP*     KSP;

#include <sstream>

#include <petscksp.h>
#include <LASolver.h>
#include <IterativeSolverControl.h>
#include <PetscVector.h>

namespace Hydra {

class LASolverStatus;

//! Base class for PETSc derived solers
class PetscSolver: public LASolver
{

  public:

    //! \name Constructor/Destructor
    //@{
             PetscSolver(Category&, DataContainer&, const string&);
    virtual ~PetscSolver();
    //@}

    //! Check convergence of solution
    virtual void checkConvergence(int it, Real rnorm, int& reason);

    //! Create a matrix that may be used with this solver
    virtual LAMatrix* createMatrix(int N, int M) const;

    //! Create a vector that may be used with this solver
    virtual LAVector* createVector(int N) const;

    //! Dump solver data structures to stdout
    virtual int dump() const;

    //! Finalize this object
    virtual void finalize();

    //! Return the status of the solution
    virtual void getStatus(LASolverStatus& status) const;

    //! Return the verbose flag setting
    virtual bool getVerbose() const;

    //! Initialize solver
    virtual void initialize(const int nel, const int globalNel);

    //! Set solver operator
    virtual void setOperator(LAMatrix* A);

    //! Set convergence settings and tolerances
    virtual void setTolerances(int itmax, int itchk, Real eps);

    //! Turn convergence/diagnostic output on or off
    virtual void setVerbose(bool convergence, bool verbose);

    //! Solve the system of equations
    virtual void solve(const LAVector* b, LAVector* x);

  protected:

    //! Get Petsc option with prefix
    //!   \param[in] option Petcs option
    //!   \return Petsc option with prefix
    const char* getOptWithPrefix(const string& option) {
      m_petscOption = "-" + m_petscPrefix + option;
      return m_petscOption.c_str();}

    virtual void initializeKSP();
    virtual void initializePC();
    virtual void initializeWorkspace(int nel, int globalNel);

    //! Set a custom convergence test
    virtual int setConvergenceTest();


    //! Set PETSc option via PetscOptionSetValue() accepting various value types
    template<class V>
    void setPetscOption(const char* option, const V value) {
      stringstream ss;
      ss << value;
      PetscOptionsSetValue(option, ss.str().c_str());
    }

    KSP m_solver;            //!< PETSc solver object
    string m_petscOption;    //!< String to construct prefix + option
    string m_petscPrefix;    //!< String to hold solver prefix

    const KSPType m_solverType;  //!< PETSc solver type, e.g., CG, GMRES, ...
    const PCType m_preconditionerType; //!< PETSc preconditioner type

    DataIndex PREVIOUS;      //!< Store previous result for convergence test

    PetscVector* m_previous; //!< Previous solution
    PetscVector* m_solution; //!< Current solution

    int m_itmax;             //!< Maximum iterations
    int m_itchk;             //!< Iteration check interval

    Real m_eps;              //!< Error tolerance
    Real m_residualError;    //!< Residual error
    Real m_changeError;      //!< Incremental error
    Real m_normRHS;          //!< 2-norm of the initial RHS

    Real m_epsilon;          //!< numeric_limits<double>::epsilon()

    bool m_report;           //!< True -- report convergence data
    bool m_verbose;          //!< True -- verbose output of solver diagnostics

  private:

    //! Don't permit copy or assignment operators
    //@{
    PetscSolver(const PetscSolver&);
    PetscSolver& operator=(const PetscSolver&);
    //@}
};

}

#endif
