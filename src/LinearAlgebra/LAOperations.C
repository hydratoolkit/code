//******************************************************************************
//! \file    src/LinearAlgebra/LAOperations.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   Matrix-vector operatios
//******************************************************************************
#include <cassert>
#include <vector>

#include <petscmat.h>

using namespace std;

#include <LAOperations.h>
#include <PetscMatrix.h>
#include <PetscVector.h>
#include <NativeVector.h>
#include <NativeMatrix.h>

namespace Hydra {

int MatrixVectorMultiply(const LAMatrix& a, const LAVector& x, LAVector& b)
/*******************************************************************************
Routine: MatrixVectorMultipy - mat-vec interface
Author : Mark A. Christon
*******************************************************************************/
{
  if (a.isNative() && x.isNative() && b.isNative()) {
    const NativeMatrix& aa = reinterpret_cast<const NativeMatrix&>(a);
    const NativeVector& xx = reinterpret_cast<const NativeVector&>(x);
    NativeVector& bb = reinterpret_cast<NativeVector&>(b);
    return MatrixVectorMultiply(aa, xx, bb);
  }
  else if (!a.isNative() && !x.isNative() && !b.isNative()) {
    const PetscMatrix& aa = reinterpret_cast<const PetscMatrix&>(a);
    const PetscVector& xx = reinterpret_cast<const PetscVector&>(x);
    PetscVector& bb = reinterpret_cast<PetscVector&>(b);
    return MatrixVectorMultiply(aa, xx, bb);
  }
  else {
    assert("Mixed native/non-native matvec is unimplemented" == 0);
  }

  return 1;
}

int MatrixVectorMultiply(const PetscMatrix& a,
                         const PetscVector& x,
                         PetscVector& b)
/*******************************************************************************
Routine: MatrixVectorMultipy - mat-vec interface for PETSC
Author : Mark A. Christon
*******************************************************************************/
{
  return MatMult(a.constGetMat(), x.constGetVec(), b.getVec());
}

int MatrixVectorMultiply(const NativeMatrix& a,
                         const NativeVector& x,
                         NativeVector& b)
/*******************************************************************************
Routine: MatrixVectorMultipy - mat-vec interface for native solvers
Author : Mark A. Christon
*******************************************************************************/
{
  int nrow = 0;
  int ncol = 0;
  a.getGlobalSize(nrow, ncol);
  const Real* aa = a.getA();
  const int*  di = a.getDI();
  const int*  dj = a.getDJ();
  const Real* xx = x.getA();
  Real* bb = b.getA();

  for (int i = 0; i < nrow; i++) {
    int offset = di[i];
    int n = di[i+1] - offset;
    const Real* aaa = &aa[offset];
    const int* ii = &dj[offset];
    Real sum = 0.0;
    for (int j = 0; j < n; j++, ii++, aaa++)
      sum += *aaa * xx[*ii];
    bb[i] = sum;
  }
  return 0;
}

}
