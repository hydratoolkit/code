//******************************************************************************
//! \file    src/LinearAlgebra/LAMatrix.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:58 2011
//! \brief   LAMatrix class for use with HYDRA linear algebra objects
//******************************************************************************
#ifndef LAMatrix_h
#define LAMatrix_h

#include <HydraTypes.h>

namespace Hydra {

class DataContainer;
class LAVector;

//! LAMatrix class for use with HYDRA linear algebra objects
class LAMatrix
{
  public:

    //! \name Constructor/Destructor
    //@{
             LAMatrix(DataContainer& dataContainer, int M, int N);
    virtual ~LAMatrix();
    //@}

     //! Add a block of values to the matrix
    virtual void add(int m, const int* i, int n, const int* j, const Real* a) =0;

    //! Assemble global matrix if running in parallel
    virtual void assemble() = 0;

    //! Dump matrix to stdout
    virtual void dump() const = 0;

    //! Deallocate any memory allocated by this matrix
    virtual void finalize() = 0;

    //! Return items from a matrix
    virtual void get(int m, const int* i, int n, const int* j, Real* a) const =0;

    //! Return diagonal of a matrix in the provided vector
    virtual void getDiagonal(LAVector* v) const =0;

    //! Return global sizes
    void getGlobalSize(int& m, int& n) const { m = m_M; n = m_N; }

    //! Return local sizes
    virtual void getLocalSize(int&m, int& n) const = 0;

    //! Return equation row ownership range
    virtual void getOwnershipRange(int&m, int& n) const = 0;

    //! Initialize matrix using two vectors of diagonal and off
    virtual void initialize(int m, int n,
                            DataIndex i,  DataIndex j,  DataIndex a,
                            DataIndex oi, DataIndex oj, DataIndex oa) = 0;

    //! Initialize matrix using information on number of non-zero terms per row.
    virtual void initialize(int m, int n, const int* dnz, const int* onz) = 0;

    //! Initialize matrix using a constant number of non-zero terms per row.
    virtual void initialize(int m, int n, int ndnz, int nonz) = 0;

    //! Return true if this is a non-PETSc (native) matrix
    virtual bool isNative() const = 0;

    //! Override a block of values in the matrix
    virtual void set(int m, const int* i, int n, const int* j, const Real* a) =0;

    //! Set diagonal of a matrix using values from the provided vector
    virtual void setDiagonal(const LAVector* v) = 0;

    //! Zero the matrix
    virtual void zero() = 0;

  protected:

    DataContainer& m_dataContainer; // Data Container w/matrix memory
    int m_M; // Global number of rows
    int m_N; // Global number of columns

  private:

    //! Don't permit copy or assignment operators
    //@{
    LAMatrix(const LAMatrix&);
    LAMatrix& operator=(const LAMatrix&);
    //@}
};

}

#endif
