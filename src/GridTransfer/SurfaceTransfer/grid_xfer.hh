//******************************************************************************
//! \file    src/GridTransfer/SurfaceTransfer/grid_xfer.hh
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:47:49 2011
//! \brief   Surface-to-surface grid transfer
//******************************************************************************

/*#ifndef GRID_XFER_H*/
#define GRID_XFER_H

#include "code_types.h"

/* prototype for main transfer function */

void grid_transfer(
                           /* Inputs */
  int xfer_flag,              /* which call to grid transfer
                                 0 = 1st call
                                 N = subsequent calls
                                -1 = clean-up */
  int lcoord_flag,
  int physics_flag,
  int n_blue,                 /* # of blue cells on this proc */
  int *blue_id,               /* global id # of each blue cell */
  Real *blue_points,          /* 8 corner pts for each blue cell */
  Real *blue_scalar,          /* physics value for each blue cell corner pt */
  Real *blue_vector,          /* physics 3-vec for each blue cell corner pt */
  int n_red,                  /* # of red nodes on this proc */
  int *red_id,                /* global id # of each red node */
  Real *red_points,           /* global x,y,z position of each red node
                                 on input; local s,t,r on output */
                           /* Outputs */
  Real *red_scalar,           /* physics value for each red node */
  Real *red_vector,           /* physics 3-vec for each red node */
  int   red_cell_id,          /* blue cell id for each read node */
  int   red_cell_proc);       /* owner of blue cell for each red node */

/*****************************************************************************/

/* Define max number of nodes per element, and the product of MAX_NNPE and
   the MAX problem dimension
 */
#define MAX_NNPE  27
#define MAX_PROD  3*MAX_NNPE

/*  Type Definition for function to convert from global to local coordinates */

#define PSI               0
#define DPSI_S            1
#define DPSI_T            2
#define DPSI_U            3

/*  Arguments to the conversion function:
 *     Real* global    global coordinates of node
 *     Real* local     local  coordinates of node
 *     Real* vert
 *     Real  diff_tol  convergence tolerance
 *     Int   max_itr   maximum iterations
*/

#define SHAPE_FUNCTION_ARGLIST \
                   Real s, \
                   Real t, \
                   Real u, \
                   Int, \
                   Int

typedef Real SHAPE_FUNCTION(SHAPE_FUNCTION_ARGLIST);
typedef SHAPE_FUNCTION *SHAPE_FN_PTR;


SHAPE_FUNCTION shape_Bilinear_Tri;
SHAPE_FUNCTION shape_Biquad_Tri;
SHAPE_FUNCTION shape_Bilinear_Quad;
SHAPE_FUNCTION shape_S_Biquad_Quad;
SHAPE_FUNCTION shape_Biquad_Quad;
SHAPE_FUNCTION shape_Trilinear_Hex;
SHAPE_FUNCTION shape_S_Triquad_Hex;
SHAPE_FUNCTION shape_Triquad_Hex;
SHAPE_FUNCTION shape_Trilinear_Tet;
SHAPE_FUNCTION shape_Triquad_Tet;
SHAPE_FUNCTION shape_Trilinear_Prism;
SHAPE_FUNCTION shape_Triquad_Prism;


void set_kind(int,int *,int *);
void set_shape(int, SHAPE_FN_PTR * );
void fill_shape(int,Real *, Real **, Real, Real, Real);
int  Global_to_Local(int,Real *,Real *,Real **,Real *,Real *);
int  Element_Vicinity(int,Real *, Real *);
Real Interpolate_Value(Real* value, Real* phi, int nnpe);


char *my_realloc(void *ptr, int n);
void my_free(void *ptr);
char *my_malloc(int n);


/*#endif*/
