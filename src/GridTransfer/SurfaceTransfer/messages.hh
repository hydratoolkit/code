//******************************************************************************
//! \file    src/GridTransfer/SurfaceTransfer/messages.hh
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:47:49 2011
//! \brief   Error messages for surface-to-surface transfers
//******************************************************************************

#ifndef messagesH
#define messagesH

#include <iostream.h>
#include <stdio.h>
#include <stdarg.h>
#include <cstdlib>
#include <cstring>

#include "code_types.h"
#include "String.h"

enum ErrorLevel {FATAL_THROW, FATAL_CATCH,
                 FATAL, BAD, WARNING, INFORMATION,
                 TASK_0_FATAL, TASK_0_BAD, TASK_0_WARNING, TASK_0_INFORMATION};

/* Int get_error_count(Void); */
/* Int get_warning_count(Void); */
/* Int get_infomsg_count(Void); */
/* Int check_fatal_throw(Void); */

/* Void hard_global_abort(Void); */
/* Void soft_global_abort(Void); */

Void code_error( ErrorLevel    error_type,
                 const String& location,
                 const String& message );

void print_log_message( const String& message );

#define METHOD(name) class_name + name

String MainKeyword(char* name);
String MainKeyword(char* name, char* line);
String MainKeyword(String name, String line);
String MainKeywordId(char* name);
String MainKeywordOptId(char* name);
String SubKeyword(char* name);
String SubKeyword(char* name, char* opts);
String SubKeyword(char* name, char* opts, char* def);
String SubKeyword(char* name, Real def);
String SubKeyword(char* name, Int def);
String EndKeyword();
String NoEndKeyword();
String SubSubKeyword(char* name);
String SubSubKeyword(char* name, char* opts);
String SubSubKeyword(char* name, char* opts, char* def);
String SubSubKeyword(char* name, Real def);
String SubSubKeyword(char* name, Int def);
String SubEndKeyword();


#endif
