//******************************************************************************
//! \file    src/GridTransfer/SurfaceTransfer/assertion.hh
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:47:49 2011
//! \brief   Assertion macros for surface transfer code
//******************************************************************************

#ifndef assertionH
#define assertionH

#include <stdio.h>
#include <cstdlib>

// Use this macro to indicate logic paths in the code that are not yet
// implemented.

#define CONSTRUCTION(msg) \
        fprintf(stderr,"Under construction: file \"%s\", line %d\n%s\n", \
        __FILE__, __LINE__, (const char*)msg), (void)abort()

// Use this macro in case statements or the like to indicate logic paths
// that do not exist.

#define BAD_CASE \
        fprintf(stderr,"Bad case: file \"%s\", line %d\n", \
        __FILE__, __LINE__), (void)abort()

#ifdef DBC

// SHOWDBC is used in conjuction with the following tests for passed
// arguments that are not otherwise used in the function called.
// It is used to prevent "variable not used" warning messages from
// certain compilers.

#define SHOWDBC(e) e

// A precondition is defined as a requirement that must be satisfied before a
// snip of code (typically a function) can execute correctly.

#define PRECONDITION(e) \
  (!(e)? fprintf(stderr,"Precondition not satisfied: file \"%s\", \
  line %d\n%s\n", __FILE__, __LINE__, #e), (void)abort() : \
  (void)0)

// A postcondition is defined as a requirement that will be satisfied after a
// snip of code (typically a function) executes successfully.

#define POSTCONDITION(e) \
  (!(e)? fprintf(stderr,"Postcondition not satisfied: file \"%s\", \
  line %d\n%s\n", __FILE__, __LINE__, #e), (void)abort() : \
  (void)0)

// An invariant is defined as a requirement that must be satisfied at the
// completion of each and every statement within some specified scope
// (typically a loop)

#define INVARIANT(e) \
  (!(e)? fprintf(stderr,"Invariant not satisfied: file \"%s\", \
  line %d\n%s\n", __FILE__, __LINE__, #e), (void)abort() : \
  (void)0)

// There is no equivalent to the "old" keyword of Eiffel in C++.
// To allow initial state to be compared to final state in postconditions,
// a macro is provided for declaring a variable to hold the initial state.
// Thus, "REMEMBER(int old_size = size());" might be used to save the
// initial size of a container for comparison against the final size
// in a postcondition.

#define REMEMBER(e) e

#else
// ifndef DBC

// When debugging is turned off, the debugging macros go away.  Thus, they
// should be sprinkled freely throughout a code without worrying about
// the impact on the code efficiency.

// SHOWDBC is used in conjuction with the following tests for passed
// arguments that are not otherwise used in the function called.
// It is used to prevent "variable not used" warning messages from
// certain compilers.

#define SHOWDBC(e)

#define PRECONDITION(e)

#define POSTCONDITION(e)

#define INVARIANT(e)

#define REMEMBER(e)

#endif
// ifdef DBC


/******************************************************************************/
template<class T>
class Safe_Array
/******************************************************************************/
// A convenient class for allocating local scratch arrays from the heap
{
  public:
    /*explicit*/ Safe_Array(int n){ ptr_ = new T[(size_t)n]; n_ = (size_t)n; }
    ~Safe_Array(void){delete[] ptr_;}
    operator T*(void) const {return ptr_;}
    T& operator[](int i){ PRECONDITION((size_t)i<n_); return ptr_[(size_t)i]; }
    const T& operator[](int i) const { PRECONDITION((size_t)i<n_); return ptr_[(size_t)i]; }

  private:
    size_t n_;
    T *ptr_;

    Safe_Array(const Safe_Array<T>&){PRECONDITION(0);}
    Safe_Array<T>& operator=(const Safe_Array<T>&){PRECONDITION(0);return *this;}
};

// When debugging is on, array bounds checking takes place.  When
// debugging is off, the array is converted to a pointer (disabling
// bounds checking) for maximum computational efficiency.
#ifndef SAFE_ARRAY
#ifndef DBC
#define SAFE_ARRAY(type,name,size) \
        Safe_Array<type> name##__owner(size); \
        type *name = name##__owner;
#else
#define SAFE_ARRAY(type,name,size) \
        Safe_Array<type> name(size);
#endif
#endif

#endif
