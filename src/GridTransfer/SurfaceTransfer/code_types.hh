//******************************************************************************
//! \file    src/GridTransfer/SurfaceTransfer/code_types.hh
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:47:49 2011
//! \brief   Code types for surface-to-surface transfers
//******************************************************************************

#ifndef code_typesH
#define code_typesH

#include <float.h>
#include <limits.h>
/* comment out as per drake -plh  */
/* #include "code_version.h" */

/* typedefs to allow easy modification of word size for a particular
 * application or computer */

#if defined SinglePrecision
#define BIG_DBL   FLT_MAX
#define SMALL_DBL FLT_MIN
#define BIG_INT   INT_MAX
#else
#define BIG_DBL   DBL_MAX
#define SMALL_DBL DBL_MIN
#define BIG_INT   INT_MAX
#endif

#if defined SOLARIS || defined P6 || defined OSF1 || defined CPLANT || defined LINUX
#define Void  void
#else
typedef void  Void;
#endif

#if defined LINUX
#define Int int
#else
typedef int             Int;
#endif
typedef unsigned int    UInt;
typedef short           Short;

#if defined SinglePrecision
typedef float           Real;
#elif defined MAC_SEMANTIC
typedef float           Real;
#else
typedef double          Real;
#endif

/* this typedef is used to declare strings that can never be changed, e.g.
 * message strings */

typedef const char *const       ConstString;

#ifndef NULL
#define NULL 0
#endif
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

/* fortran macro to screen linkage rules */

#if defined LC_FLINK
#define FORTRAN(subname) subname##_
#elif defined LCFLINK
#define FORTRAN(subname) subname
#elif defined GNU
#define FORTRAN(subname) subname##_
#endif

/* cpp macros to screen dimensionality */

#ifdef TWO_D
#define SHOW2D(e) e
#else
#define SHOW2D(e)
#endif

#ifdef THREE_D
#define SHOW3D(e) e
#else
#define SHOW3D(e)
#endif

#endif
