//******************************************************************************
//! \file    src/GridTransfer/SurfaceTransfer/pio.hh
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:47:49 2011
//! \brief   Fortran interface for PIO I/O
//******************************************************************************
      PARAMETER (MAXIOCHAR=132,MAXIOLNS=100)
      CHARACTER*(MAXIOCHAR) KOUT(MAXIOLNS)
      CHARACTER*4 IMEMPTY
      PARAMETER ( IMEMPTY='^%&@' )
C
C       KOUT     character  array string for output redirection
C       IMEMPTY  character string to which each KOUT(I) is set
C                to indicate that KOUT(I) is empty.
C
