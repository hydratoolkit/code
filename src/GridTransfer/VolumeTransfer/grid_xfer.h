//******************************************************************************
//! \file    src/GridTransfer/VolumeTransfer/grid_xfer.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:48:09 2011
//! \brief   volume-to-volume grid transfers
//******************************************************************************
#ifndef GRID_XFER_H
#define GRID_XFER_H

#include "code_types.h"

/* prototype for main transfer function */

void grid_transfer(
                           /* Inputs */
  int xfer_flag,              /* which call to grid transfer
                                 0 = 1st call
                                 N = subsequent calls
                                -1 = clean-up */
  int n_blue,                 /* # of blue cells on this proc */
  int  *blue_id,              /* global id # of each blue cell */
  Real *blue_points,          /* 8 corner pts for each blue cell */

  int    num_scalars,
  Real **blue_scalar,         /* physics value for each blue cell corner pt */
  int    num_vectors,
  Real **blue_vector,         /* physics 3-vec for each blue cell corner pt */

  int   n_red,                /* # of red nodes on this proc */
  int  *red_id,               /* global id # of each red node */
  Real *red_points,           /* xyz position of each red node */
                           /* Outputs */
  Real **red_scalar,          /* physics value for each red node */
  Real **red_vector);         /* physics 3-vec for each red node */

/*  The blue physics values for each variable are stored in seperate arrays.
    blue_scalar[i] points to the array storing the un-rolled blue value for
    physics variable i.

    Similarly for vector blue physics.

    On return, the same storage scheme is used for red_scalar & red_vector.

    The storage thus required is

       blue_scalar:  (# blue elmts) * (# nodes per elmt = 8) * num_scalars
       blue_vector:  (# blue elmts) * (# nodes per elmt = 8) * 3 * num_vectors

       red_scalar:   (# blue elmts) * (# nodes per elmt = 8) * num_scalars
       blue_vector:  (# blue elmts) * (# nodes per elmt = 8) * 3 * num_vectors
*/


extern "C"
{

/* FORTRAN to C wrapper */

// #ifdef LC_FLINK
// #define FGRID_TRANSFER   fgrid_transfer_
// #elif LCFLINK
// #define FGRID_TRANSFER   fgrid_transfer
// #elif UCFLINK
// #define FGRID_TRANSFER   FGRID_TRANSFER
// #endif

//void
//FGRID_TRANSFER(
void FORTRAN(fgrid_transfer)(

  int *xfer_flag,       /* pass by "reference" */
  int *n_blue,          /* pass by "reference" */
  int *blue_id,
  Real *blue_points,
  Real *blue_scalar,
  Real *blue_vector,
  int *n_red,           /* pass by "reference" */
  int *red_id,
  Real *red_points,

  Real *red_scalar,
  Real *red_vector);

//   int *xfer_flag,       /* pass by "reference" */
//   int *n_blue,          /* pass by "reference" */
//   int *blue_id,
//   Real *blue_points,
//   int  *num_scalars,    /* pass by "reference" */
//   Real **blue_scalar,
//   int  *num_vectors,    /* pass by "reference" */
//   Real **blue_vector,
//   int *n_red,           /* pass by "reference" */
//   int *red_id,
//   Real *red_points,
//
//   Real **red_scalar,
//   Real **red_vector);

}

#endif
