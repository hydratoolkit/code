// $Id$

#ifndef SIDE_SET_H
#define SIDE_SET_H

#include <iostream>

class ExoII_Read;
class StringX;

class Side_Set {
public:
  
  Side_Set();
  Side_Set(int exo_set_id);
  Side_Set(int exo_set_id, int num_sides, int num_dist_factors = 0);
  Side_Set(const Side_Set&);  // Not written.
 ~Side_Set();
  
  const Side_Set& operator=(const Side_Set&);  // Not written.
  
  int Set_Id() const { return set_id; }
  int Num_Sides() const { return num_sides; }
  
  StringX Load_Set(int exo_file_id);  // (Doesn't load dist factors.)
  StringX Free_Set();                 // Frees all dynamic memory.
  
  StringX Get_Set(const int*& elmts, const short*& sides) const;
  
  void Display_Stats(std::ostream& = std::cout) const;
  void Display      (std::ostream& = std::cout) const;
  int  Check_State() const;
  
private:
  
  int set_id;
  int num_sides;
  int num_dist_factors;
  
  int*    elmts;         // Array.
  short*  sides;         // Array.
  double* dist_factors;  // Array.
  
  
  friend class ExoII_Read;
};

#endif
