// $Id$

#ifndef EXO_BLOCK_H
#define EXO_BLOCK_H

#include <iostream>

#include "stringx.h"
#include "smart_assert.h"

class ExoII_Read;

class Exo_Block {
public:
  
  Exo_Block();
  Exo_Block(int exo_file_id, int exo_block_id);
  Exo_Block(int block_id,
            const char* elmt_type,
            int num_elmts,
            int num_nodes_per_elmt,
            int num_attr = 0);
  Exo_Block(const Exo_Block&);  // Not written.
 ~Exo_Block();
  
  const Exo_Block& operator=(const Exo_Block&);  // Not written.
  
  StringX Load_Block_Params(int exo_file_id, int exo_block_id);
  StringX Load_Connectivity(int exo_file_id);
  StringX Load_Attributes(int exo_file_id);
  StringX Free_Connectivity();
  StringX Free_Attributes();
  
  StringX Load_Results(int exo_file_id, int time_step_num, int elmt_var_index);
  const double* Get_Results(int elmt_var_index) const;
  void    Free_Results();
  
  
  // Access functions:
  
  int            Id()                   const { return block_id;           }
  const StringX& Elmt_Type()            const { return elmt_type;          }
  int            Num_Elmts()            const { return num_elmts;          }
  int            Num_Nodes_per_Elmt()   const { return num_nodes_per_elmt; }
  int            Num_Attributes()       const { return num_attr;           }
  
  
  // Block description access functions:
  
  const int* Connectivity()  const { return conn; }  // 1-offset connectivity
  const int* Connectivity(int elmt_index) const;     // 1-offset connectivity
  const double* Attributes() const { return attr; }
  
  StringX Give_Connectivity(int& num_e,         // Moves connectivity matrix to
                            int& npe,           // conn pointer and sets its own
                            int*& recv_conn);   // to null.
  
  
  // Misc:
  
  int Check_State() const;
  
  void Display_Stats(std::ostream& = std::cout) const;
  void Display      (std::ostream& = std::cout) const;
  
private:
  
  int     block_id;             // Exodus block id.
  int     block_index;          // 0-offset index into Exodus block list.
  StringX elmt_type;
  int     num_elmts;
  int     num_nodes_per_elmt;
  int     num_attr;
  
  int*    truth;         // Array; holds local truth table for this block
  int*    conn;          // Array; holds a matrix, num_elmts by num_nodes_per_elmt.
  double* attr;          // Array; holds a matrix, num_elmts by num_attr;
  
  int      cur_time;     // Time step number of the current results.
  int      num_vars;     // Total number of element variables in the file.
  double** results;      // Array of pointers (length num_vars)
                         // to arrays of results (length num_elmts).
  
  
  friend class ExoII_Read;
};

// inline const int* Exo_Block::Edge_Verts(int edge_index) const
// {
//   SMART_ASSERT(edge_index >= 0 && edge_index < num_edges_per_elmt);
//   SMART_ASSERT(edge_verts != 0);
//   return &edge_verts[2*edge_index];
// }
// 
// inline const int* Exo_Block::Face_Verts(int face_index) const
// {
//   SMART_ASSERT(face_index >= 0 && face_index < num_faces_per_elmt);
//   SMART_ASSERT(face_verts != 0);
//   return &face_verts[4*face_index];
// }
// 
// inline int Exo_Block::Num_Face_Verts(int face_index) const
// {
//   SMART_ASSERT(face_index >= 0 && face_index < num_faces_per_elmt);
//   SMART_ASSERT(num_face_verts != 0);
//   return num_face_verts[face_index];
// }
// 
// inline const int* Exo_Block::Side_Verts(int side_index) const
// {
//   SMART_ASSERT(side_index >= 0 && side_index < num_sides_per_elmt);
//   SMART_ASSERT(side_verts != 0);
//   return &side_verts[4*side_index];
// }
// 
// inline int Exo_Block::Num_Side_Verts(int side_index) const
// {
//   SMART_ASSERT(side_index >= 0 && side_index < num_sides_per_elmt);
//   SMART_ASSERT(num_side_verts != 0);
//   return num_side_verts[side_index];
// }


#endif
