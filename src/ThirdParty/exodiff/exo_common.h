// $Id$

#ifndef EXO_COMMON_H
#define EXO_COMMON_H

class StringX;

StringX Get_Init_Params(int exo_file_id,
                        StringX& title,
                        int& dimension,
                        int& num_nodes,
                        int& num_elmts,
                        int& num_elmt_blocks,
                        int& num_node_sets,
                        int& num_side_sets);

int     Get_Num_Elmt_Blocks(int exo_file_id);
StringX Get_Elmt_Block_Ids(int exo_file_id, int* block_ids);

// Returns 0 if not found.
int Get_Elmt_Block_Index(int exo_file_id, int block_id);

int Get_Num_Elmt_Variables(int exo_file_id);

StringX Get_Elmt_Truth_Table(int exo_file_id, int* truth_table);

int Get_Num_Time_Steps(int exo_file_id);

int     Inquire_int(int exo_file_id, int request);
float   Inquire_flt(int exo_file_id, int request);
StringX Inquire_str(int exo_file_id, int request);

#endif
