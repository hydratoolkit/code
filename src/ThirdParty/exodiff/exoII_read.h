// $Id$

#ifndef EXOII_READ_H
#define EXOII_READ_H

#include "exodusII.h"
#include "netcdf.h"  

#include <iostream>

#include "stringx.h"
#include "smart_assert.h"

//
//  Notes: 1) Most functions will return a string as a result.  An empty string
//            indicates success, while a non-empty string indicates an error or
//            a warning, either of which will be contained in the string.
//


class Exo_Block;
class Node_Set;
class Side_Set;


class ExoII_Read {
public:
  
  ExoII_Read();
  ExoII_Read(const char* file_name);
  ExoII_Read(const ExoII_Read&);  // Not written.
  virtual ~ExoII_Read();
  const ExoII_Read& operator=(const ExoII_Read&);  // Not written.
  
  // File operations:
  
  StringX File_Name(const char*);
  virtual StringX Open_File(const char* = 0);  // Default opens current file name.
  StringX Close_File();
  StringX File_Name()    const { return file_name;      }
  int     Open()         const { return (file_id >= 0); }
  int     IO_Word_Size() const { return io_word_size;   }
  
  // Global data:
  
  const StringX& Title()    const { return title;         }
  int     Dimension()       const { return dimension;     }
  int     Num_Nodes()       const { return num_nodes;     }
  int     Num_Elmts()       const { return num_elmts;     }
  int     Num_Node_Sets()   const { return num_node_sets; }
  int     Num_Side_Sets()   const { return num_side_sets; }
  float Data_Base_Version() const { return db_version;    }
  float Library_Version()   const { return api_version;   }
  const StringX_List& Coordinate_Names()    const { return coord_names; }
        StringX       Coordinate_Name (int) const;
  
  // Times:
  
  int Num_Times() const { return num_times; }
  double Time(int time_num) const;
  
  // Variables:
  
  int Num_Global_Vars() const { return global_vars.Length(); }
  int Num_Nodal_Vars()  const { return nodal_vars.Length();  }
  int Num_Elmt_Vars()   const { return elmt_vars.Length();   }
  const StringX_List& Global_Var_Names() const { return global_vars; }
  const StringX_List& Nodal_Var_Names()  const { return nodal_vars;  }
  const StringX_List& Elmt_Var_Names()   const { return elmt_vars;   }
  const StringX&      Global_Var_Name(int index) const;
  const StringX&      Nodal_Var_Name (int index) const;
  const StringX&      Elmt_Var_Name  (int index) const;
  
  
  // Element blocks:
  
  int Num_Elmt_Blocks() const { return num_elmt_blocks; }
  int Truth(int block_index, int elmt_var_index) const;
  const int* Truth_Table() const { return truth; }
  
  const Exo_Block* Get_Elmt_Block(int block_index) const;
  
  StringX Load_Elmt_Block_Description(int block_index) const;
  StringX Load_Elmt_Block_Descriptions() const;  // Loads all blocks.
  StringX Load_Elmt_Block_Results(int block_index,
                                  int elmt_var_index,
                                  int time_step_num) const;
  StringX Free_Elmt_Block(int block_index) const; // Frees all dynamic memory.
  StringX Free_Elmt_Blocks() const;               // Frees all blocks.
  StringX Free_Elmt_Block_Results(int block_index) const;
  
  // Moves array of connectivities from the block to the conn array.
  StringX Give_Connectivity(int block_index, int& num_e, int& npe, int*& conn);
  
  int Block_Id(int block_index) const;  // Returns associated block id.
  int Block_Index(int block_id) const;  // Returns associated block index.
  
  // Number maps:
  
  StringX    Load_Node_Map();
  StringX    Free_Node_Map();
  const int* Get_Node_Map() { return node_map; }
  StringX    Load_Elmt_Map();
  StringX    Free_Elmt_Map();
  const int* Get_Elmt_Map() { return elmt_map; }
  StringX    Load_Elmt_Order();
  StringX    Free_Elmt_Order();
  const int* Get_Elmt_Order() { return elmt_order; }
  void Free_All_Maps();
  inline int Node_Map  (int node_num) const;  // numbers are global, 1-offset
  inline int Elmt_Map  (int elmt_num) const;  // numbers are global, 1-offset
  inline int Elmt_Order(int elmt_num) const;  // numbers are global, 1-offset
  
  // Nodal data:
  
  StringX Load_Nodal_Coordinates();
  const double* X_Coords() const { return nodes; }
  const double* Y_Coords() const { if (dimension < 2) return 0;
                                   return nodes == 0 ? 0: nodes + num_nodes; }
  const double* Z_Coords() const { if (dimension < 3) return 0;
                                   return nodes == 0 ? 0: nodes + 2*num_nodes; }
  void Free_Nodal_Coordinates();
  
  // (First time step = 1.)
  StringX Load_Nodal_Results(int time_step_num, int var_index);
  const double* Get_Nodal_Results(int var_index) const;
  void Free_Nodal_Results();
  
  // Global data:  (NOTE:  Global and Nodal data are always stored at the same
  //                       time step.  Therefore, if current time step number
  //                       is changed, the results will all be deleted.)
  
  StringX Load_Global_Results(int time_step_num);
  const double* Get_Global_Results() const { return global_vals; }
  
  // Node/Side sets:
  
//  const Node_Set* Get_Node_Set(int set_id);

  int             Side_Set_Id          (int set_index)      const;
  StringX         Load_Side_Set        (int set_id)         const;
  StringX         Load_Side_Sets       ()                   const;
  const Side_Set* Get_Side_Set         (int set_id)         const;
  const Side_Set* Get_Side_Set_by_Index(int side_set_index) const;
  
  
  // Misc functions:
  
  virtual void Display_Stats(std::ostream& = std::cout) const;
  virtual void Display      (std::ostream& = std::cout) const;
  virtual void Display_Maps (std::ostream& = std::cout) const;
  virtual int  Check_State() const;  // Checks state of obj (not the file).
  int  File_ID() const { return file_id; }  // This is temporary.
  StringX Global_to_Block_Local(int global_elmt_num,            // 1-offset
                                int& block_index,               // 0-offset
                                int& local_elmt_index) const;   // 0-offset
  
protected:
  
  StringX file_name;
  int     file_id;    // Exodus file id; also used to determine if file is open.
  
  // GENESIS info:
  
  StringX      title;
  int          num_nodes;
  StringX_List coord_names;
  int          dimension;
  int          num_elmts;
  int          num_elmt_blocks;
  int          num_node_sets;
  int          num_side_sets;
  float        db_version;
  float        api_version;
  int          io_word_size;    // Note: The "compute word size" is always 8.
  
  Exo_Block* eblocks;   // Array.
  Node_Set*  nsets;     // Array.
  Side_Set*  ssets;     // Array.
  
  double* nodes;        // Matrix;  dimension by num_nodes (row major form).
                        //          I.e., all x's then all y's, etc.
  
  int* node_map;        // Array; num_nodes long when filled.
  int* elmt_map;        // Array; num_elmts long when filled.
  int* elmt_order;      // Array; num_elmts long when filled.
  
  // RESULTS info:
  
  StringX_List global_vars;
  StringX_List nodal_vars;
  StringX_List elmt_vars;
  
  int     num_times;
  double* times;
  int*    truth;     // Truth table is num blocks by num elmt variables, in
                     // row major form.
  
  int cur_time;      // Current timestep number of the results (0 means none).
  double** results;  // Array of pointers (to arrays of results data);
                     // length is number of nodal variables.
  double* global_vals;  // Array of global variables for the current timestep.
  
  // Internal methods:
  
  void Get_Init_Data();         // Gets bunch of initial data.
  
  int Elmt_Block_Index(int eblock_id) const;  // Returns index of element block.
  int NSet_Index(int node_set_id) const;      // Returns index of node set.
  int SSet_Index(int side_set_id) const;      // Returns index of side set.
  
  int File_Exists(const char* fname);
};

inline int ExoII_Read::Node_Map(int node_num) const
{
  SMART_ASSERT(Check_State());
  SMART_ASSERT(node_num > 0 && node_num <= num_nodes);
  
  if (node_map) return node_map[ node_num - 1 ];
  return 0;
}

inline int ExoII_Read::Elmt_Map(int elmt_num) const
{
  SMART_ASSERT(Check_State());
  SMART_ASSERT(elmt_num > 0 && elmt_num <= num_elmts);
  
  if (elmt_map) return elmt_map[ elmt_num - 1 ];
  return 0;
}

inline int ExoII_Read::Elmt_Order(int elmt_num) const
{
  SMART_ASSERT(Check_State());
  SMART_ASSERT(elmt_num > 0 && elmt_num <= num_elmts);
  
  if (elmt_order) return elmt_order[ elmt_num - 1 ];
  return 0;
}


#endif
