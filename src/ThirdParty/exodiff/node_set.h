// $Id$

#ifndef NODE_SET_H
#define NODE_SET_H

#include <iostream>

class ExoII_Read;

class Node_Set {
public:
  
  Node_Set();
  Node_Set(int exo_set_id);
  Node_Set(int exo_set_id, int num_nodes, int num_dist_factors = 0);
  Node_Set(const Node_Set&);  // Not written.
 ~Node_Set();
  
  const Node_Set& operator=(const Node_Set&);  // Not written.
  
  void Display_Stats(std::ostream& = std::cout);
  void Display      (std::ostream& = std::cout);
  
private:
  
  int set_id;
  int num_nodes;
  int num_dist_factors;
  
  int     filled;        // Flag; true if nodes and dist_factors are filled.
  int*    nodes;         // Array.
  double* dist_factors;  // Array.
  
  
  friend class ExoII_Read;
};


#endif
