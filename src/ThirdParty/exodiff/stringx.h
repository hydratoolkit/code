// $Id$

#ifndef STRINGX_H
#define STRINGX_H

#include <cstring>
#include <iostream>

#include "smart_assert.h"

//   A reinvention of the wheel -- this string class has the essentials of
// any string class along with special concat operators and some token
// extraction functions.
//   Storage is typically kept rather than thrown away if the string shrinks.
// To reclaim the storage, set the string equal to a null string, strx = "".
//
//  Also, a string list class follows, which stores an array of StringX objects.
// It automatically allocates storage for the strings; the main functionality
// is functions for Add(), Length(), and the index operator.

class StringX {
public:

  StringX() : len(0), buff_size(0), str_ptr(0) { }
  StringX(int);              // Allocates buffer to int size.
  StringX(const char*);      // Conversion constructor.
  StringX(const StringX&);   // Copy constructor.
 ~StringX() { if (str_ptr) delete [] str_ptr; }
  
  
  // Operators:
  
  const StringX& operator=(const char*);
  const StringX& operator=(const StringX&);
  
  inline char operator[](int i) const;
  
  operator const char* () const { return str_ptr != 0 ? str_ptr : ""; }
  
  int operator==(const char*) const;
  int operator!=(const char* sptr) const    { return !( *this == sptr ); }
  int operator< (const char* sptr) const;
  
  StringX &operator<<(const char*);        // These fcns concat the string
  StringX &operator<<(int);                // equivalents of the given args
  StringX &operator<<(double);             // onto the end of the string.
  StringX &operator<<(float);
  
  // Same as "==" when only 1 arg.  Otherwise compares first "num_chars" chars.
  int Equals(const char*, int num_chars = 0) const;
  int Equals_Nocase(const char*, int num_chars = 0) const;  // Ignores case.
  
  // Token fcns:  (the argument is an optional set of delimeters)
  
  int     Num_Tokens(const char* = " \t\n") const;
  StringX Next_Token(const char* = " \t\n"); // Removes and returns first token.
  
  
  // Access fcns:
  
  int         Length()    const { return len;       }
  int         size()      const { return len;       }
  int         Buff_Size() const { return buff_size; }
  const char* Str_Ptr()   const { return str_ptr != 0 ? str_ptr : ""; }
  
  
  // Misc. fcns:
  
  StringX& To_Lower();     // Changes all letters to lower case.
  StringX& To_Upper();     // Changes all letters to upper case.
  
  // Chops chars off end; if no arguments, will remove white space from end.
  StringX& Chop(int num_chars);
  StringX& Chop(const char* white_space = " \t\n");
  
  void Display(std::ostream& = std::cout) const;
  
private:

  int   len;
  int   buff_size;   // Bytes of memory currently allocated to hold the string.
  char* str_ptr;
  
  
  // Internal methods:
  
  void Allocate(int buff);             // Gets new storage; throws away old.
  void Increase_Buff_to(int req_len);  // Gets more storage; copies old data.
  int  strrspn(const char* sptr, const char* delims);  // Reverse strspn(...).

};


#define STRX_BLOCK_SIZE 5

class StringX_List {
public:

  StringX_List() : len(0), buff_size(0), data(0) { }
  StringX_List(int buff_size);
  StringX_List(const StringX_List&);
 ~StringX_List() { Purge(); }
  
  const   StringX_List& operator=(const StringX_List&);
  
  int operator==(const StringX_List&) const;
  int operator!=(const StringX_List& with) const { return !( *this == with ); }
  
  inline const StringX& operator[](int i) const;
  inline       StringX& operator[](int i);
  
  StringX_List& Add(const char*);  // Returns the list itself.
  
  StringX_List& Concat(const StringX_List&); // Returns the list itself.
  
  int  Find(const char*) const; // Returns index if found, otherwise returns -1.
  int  Find_Chop(const char*) const; // Ignores whitespace on ends.
  int  Find_Chop_Nocase(const char*) const; // .. and ignores case.
  int  Remove(const char*);   // Returns true if string was found (and removed.)
  void Remove(int index);
  
  int  Length()    const { return len;       }
  int  Buff_Size() const { return buff_size; }
  
  void Clear();       // Deletes each string but keeps the buffer storage.
  void Purge();       // Deletes all memory.
  
  void Display(std::ostream& = std::cout);
  
private:
  
  short    len;          // Number of StringX objects in use.
  short    buff_size;
  StringX* data;         // Array.
  
  
  // Internal methods:
  
  void Copy(const StringX_List&);   // Copies a list.
  void Allocate(int num);           // Gets storage; throws away old data.
  void Increase_Buff_Size();        // Gets more storage; copies old data.
};



inline char StringX::operator[](int i) const
{
  SMART_ASSERT(len >= 0 && len < buff_size && !( len && !str_ptr ) );
  SMART_ASSERT( !( str_ptr && len != (int)strlen(str_ptr) ) );
  SMART_ASSERT(buff_size >= 0);
  SMART_ASSERT(i >= 0 && i < len);
  
  return str_ptr[i];
}


inline const StringX& StringX_List::operator[](int i) const
{
  SMART_ASSERT(len >= 0 && len <= buff_size && !( len > 0 && !data ) );
  SMART_ASSERT(buff_size >= 0);
  SMART_ASSERT (0 <= i && i < len);  // User error.
  
  return data[i];
}

inline StringX& StringX_List::operator[](int i)
{
  SMART_ASSERT(len >= 0 && len <= buff_size && !( len > 0 && !data ) );
  SMART_ASSERT(buff_size >= 0);
  SMART_ASSERT (0 <= i && i < len);  // User error.
  
  return data[i];
}

#endif
