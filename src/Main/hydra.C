//******************************************************************************
//! \file    src/Main/hydra.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:54:01 2011
//! \brief   Main program for Hydra
//******************************************************************************

#include <cstdlib>
#include <cstring>
#include <time.h>

#include <cassert>
#include <string>
#include <iostream>
#include <fstream>

#ifdef USE_FENV
#include <fenv.h>
#endif

#ifdef HAVE_OPENBLAS
extern "C" void openblas_set_num_threads(int num_threads);
#endif

// PETSc initialization
#include <petsc.h>

using namespace std;

#include <HydraTypes.h>
#include <Control.h>
#include <Exception.h>
#include <fileIO.h>
#include <UnsMesh.h>
#include <MPIWrapper.h>
#include <Physics.h>
#include <PhysicsManager.h>
#include <ParseControl.h>
#include <Timer.h>
#include <Driver.h>

using namespace Hydra;

int main(int argc, char* argv[])
{
#ifdef USE_FENV
  feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
#endif

#ifdef HAVE_OPENBLAS
  openblas_set_num_threads(1);
#endif

  // Initialize the messaging system
  g_comm = new MPIWrapper(&argc, &argv);

  // Set up PETSc to use our communicator
  g_comm->duplicate(&PETSC_COMM_WORLD);

  // Initialize PETSc
  // Initialize should be done by the physics.
  // LASolverFactory::initialize() is currently unused.
  // For now, ignore arguments for PetSC for testing purposes
  // May need to strip command-line arguments
  //int ierr = PetscInitialize(&argc, &argv, 0, 0);
  PetscInitialize(0, 0, 0, 0);

  // Remove PetSC's signal handler
  PetscPopSignalHandler();

#ifdef DEBUG
  // Hook for debugging with gdb
  if(getenv("MPI_DEBUG") != NULL) {
    int pid = g_comm->getPid();
    volatile int i=0;
    cout << "pid = " << pid << " waiting for debugger" << endl;
    if (pid == 2) {
      while (i==0) {
        sleep(5);
      }
    }
  }
  g_comm->syncup();
#endif

  // Single-physics driver
  Driver spd;

  try {

    // Initialize: setup files, create primary objects, start primary timer
    spd.initialize(argc, argv);

    // Analysis Setup procedure
    // Read the mesh, parse controls , etc.
    spd.setup();

    // Solve the problem
    spd.solve();

  } catch(Exception ec) {

    p0cout << "!!!!! Error: " << ec.getError().c_str() << " !!!!!" << endl;

  }

  // Report the time
  spd.reportTime();

  // Close all files, delete all objects & free all memory
  spd.finalize();

  // Finalize PETSc
  PetscFinalize();

  // Terminate the message passing system
  g_comm->finalMP();
  delete g_comm;
}
