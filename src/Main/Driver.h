//******************************************************************************
//! \file    src/Main/Driver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:54:00 2011
//! \brief   Main driver for the hydra toolkit
//******************************************************************************
#ifndef Driver_h
#define Driver_h

namespace Hydra {

//! Driver for physics solution algorithms
class Driver {

  public:

    //! \name Constructor/Destructor
    //@{
             Driver();
    virtual ~Driver();
    //@}

    //! Initialize the driver
    void initialize(int argc, char **argv);

    //! Finalize the driver
    void finalize();

    //! Parse the command-line, and set filenames
    void parseArgv(int argc, char *argv[], string* fnames);

    //! Perform the setup: Read the mesh, parse controls, etc.
    void setup();

    //! Solve the specific physics problem
    void solve();

    //! Printout the banner for performing a restart
    void echoRestart(ostream& ofs);

    //! Print the code banner
    void printBanner(ostream& ofs);

    //! Report all CPU/wall-clock timers
    void reportTime();

 private:

    //! Don't permit copy or assignment operators
    //@{
    Driver(const Driver&);
    Driver& operator=(const Driver&);
    //@}

    int m_pid;    //!< processor rank
    int m_Nproc;  //!< No. of processors

    // All these pointers are OWNED HERE!!!
    fileIO*        m_io;
    UnsMesh*       m_mesh;
    Control*       m_control;
    Physics*       m_physics;
    PhysicsManager m_physman;
    Timer*         m_timer;

    TimerIndex TOTAL_TIME;
    TimerIndex READ_TIME;
    TimerIndex SOLVE_TIME;

};

}

#endif // Driver_h
