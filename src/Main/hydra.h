/*!
\file    src/Main/hydra.h
\author  Mark A. Christon
\date    May, 2011
\brief   Main header file for the Hydra Toolkit

\mainpage C++ Source Code Documentation

\section introduction Introduction

This document provides the source code documentation for the Hydra
Toolkit.  The Hydra Toolkit is open-source, and provided for free
under a BSD License agreement.

The Hydra Toolkit is an extensible multiphysics code that includes
finite-element based solvers for time-dependent heat conduction,
time-dependent advection-diffusion, time-dependent incompressible
flow, Lagrangian hydrodynamics, and rigid-body dynamics. In addition,
unstructured-grid finite-volume solvers are available for solving
time-dependent advection-diffusion, Burgers' equation, the
compressible Euler equationa, and incompressible Navier-Stokes
equations.

The Hydra Toolkit model for development provides lightweight,
high-performance and reusable code modules for agile development.
This makes it possible to either use one of the existing physics
solution methods, or to rapidly develop your own solution method using
the parallel software components for handling unstructured meshes,
performing run-time load balancing, data migration, and one of the
scalable linear solvers available with the toolkit.

\section documentation Documentation

There is extensive documentation available for the Hydra Toolkit at
<a href="http://www.c-sciences.com">c-sciences.com</a>, and includes

o The Hydra Installation Manual.

o The Hydra Keywords Manual.

o The Hydra Mesh Converters & Utilities Manual.

o The Hydra Tutorials Manual.

o The Hydra Verification & Validation Manual.

o The Hydra CFD Theory Manual.

o The Hydra Rigid Body Dynamics Theory Manual.

\section copyright Copyright

Copyright (c) 2015 - 2021. Computational Sciences International, LLC,
All rights reserved.

\section license License

Software License Agreement (BSD License)
----------------------------------------

License: The Hydra Toolkit is distributed under a BSD License.

The redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

o Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the
  distribution.

o Neither the name of Computational Sciences International, LLC,
  Mark A. Christon nor the names of its contributors may be used
  to endorse or promote products derived from this software
  without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

//! \brief Hydra is the primary namespace for the Hydra Toolkit

namespace Hydra {}
