# Common headers/libraries for all code

set(CMAKE_VERBOSE_MAKEFILE 1)

### Set local variables for guesses ###

# Some typical TPL installation guesses
set(GNU_INCLUDE_GUESS   /usr/local/gnu/include  )
set(GNU_LIB_GUESS       /usr/local/gnu/lib      )
set(INTEL_INCLUDE_GUESS /usr/local/intel/include)
set(INTEL_LIB_GUESS     /usr/local/intel/lib    )
set(USE_MATH_LIB ON CACHE BOOL "Link Hydra with libm")

# Cray path guesses for Titan
if(CRAY)
  set(CRAY_NETCDF_INCLUDE_GUESS $ENV{NETCDF_DIR}/include        )
  set(CRAY_NETCDF_LIB_GUESS     $ENV{NETCDF_DIR}/lib            )
  set(CRAY_HDF5_LIB_GUESS       $ENV{HDF5_DIR}/lib              )
  set(CRAY_BLASLAPACK_LIB_GUESS $ENV{CRAY_LIBSCI_PREFIX_DIR}/lib)
endif()

# Mac OS GNU compiler path guesses
if(APPLE)
  set(USE_MATH_LIB OFF CACHE BOOL "Link Hydra with libm" FORCE)
endif(APPLE)

# Include Directories

find_path(EXODUS_INCLUDE_DIR exodusII.h
          ${THIRD_PARTY_PREFIX}/include
          ${GNU_INCLUDE_GUESS}
          ${INTEL_INCLUDE_GUESS}
          ${TPL_INCLUDE_GUESS}
          NO_DEFAULT_PATH
)

find_path(NEMESIS_INCLUDE_DIR ne_nemesisI.h
          ${THIRD_PARTY_PREFIX}/include
          ${GNU_INCLUDE_GUESS}
          ${INTEL_INCLUDE_GUESS}
          NO_DEFAULT_PATH
)

find_path(NETCDF_INCLUDE_DIR netcdf.h
          ${THIRD_PARTY_PREFIX}/include
          ${GNU_INCLUDE_GUESS}
          ${INTEL_INCLUDE_GUESS}
          NO_DEFAULT_PATH
)

if (USE_PARMETIS)
  find_path(PARMETIS_INCLUDE_DIR parmetis.h
            ${THIRD_PARTY_PREFIX}/include
            ${GNU_INCLUDE_GUESS}
            ${INTEL_INCLUDE_GUESS}
            NO_DEFAULT_PATH
  )
endif()

find_path(PETSC_INCLUDE_DIR petsc.h
          ${THIRD_PARTY_PREFIX}/include
          ${GNU_INCLUDE_GUESS}
          ${INTEL_INCLUDE_GUESS}
          NO_DEFAULT_PATH
)

find_path(ZOLTAN_INCLUDE_DIR zoltan_cpp.h
          ${THIRD_PARTY_PREFIX}/include
          ${GNU_INCLUDE_GUESS}
          ${INTEL_INCLUDE_GUESS}
          NO_DEFAULT_PATH
)

if(THIRD_PARTY_PREFIX)
  include_directories(${THIRD_PARTY_PREFIX}/include)
endif(THIRD_PARTY_PREFIX)

include_directories(${EXODUS_INCLUDE_DIR}
                    ${MPI_CXX_INCLUDE_PATH}
                    ${MPI_C_INCLUDE_PATH}
                    ${MPI_Fortran_INCLUDE_PATH}
                    ${NEMESIS_INCLUDE_DIR}
                    ${NETCDF_INCLUDE_DIR}
                    ${PARMETIS_INCLUDE_DIR}
                    ${PETSC_INCLUDE_DIR}
                    ${ZOLTAN_INCLUDE_DIR}
)

# Third-Party Libraries

find_library(EXO_C_LIBRARY
             NAMES exoIIv2c
             PATHS ${EXODUS_LIB_PATH}
                   ${THIRD_PARTY_PREFIX}/lib
                   ${GNU_LIB_GUESS}
                   ${INTEL_LIB_GUESS}
                   NO_DEFAULT_PATH
)

find_library(NEMESIS_LIBRARY
             NAMES nemesis
             PATHS ${NEMESIS_LIB_PATH}
                   ${THIRD_PARTY_PREFIX}/lib
                   ${GNU_LIB_GUESS}
                   ${INTEL_LIB_GUESS}
                   NO_DEFAULT_PATH
)

find_library(NETCDF_LIBRARY
             NAMES libnetcdf.a
             PATHS ${NETCDF_LIB_PATH}
                   ${THIRD_PARTY_PREFIX}/lib
                   NO_DEFAULT_PATH
)

if(NETCDF_LIBRARY STREQUAL "NETCDF_LIBRARY-NOTFOUND")
  find_library(NETCDF_LIBRARY
               NAMES libnetcdf.a
               PATHS ${GNU_LIB_GUESS}
                     ${INTEL_LIB_GUESS}
                     NO_DEFAULT_PATH
)
endif(NETCDF_LIBRARY STREQUAL "NETCDF_LIBRARY-NOTFOUND")

find_library(ZOLTAN_LIBRARY
             NAMES zoltan
             HINTS ${NETCDF_LIB_PATH}
                   ${THIRD_PARTY_PREFIX}/lib
                   ${GNU_LIB_GUESS}
                   ${INTEL_LIB_GUESS}
                   NO_DEFAULT_PATH
)

find_library(HDF5HL_LIBRARY
             NAMES libhdf5_hl.a
             PATHS ${HDF5_LIB_PATH}
                   ${THIRD_PARTY_PREFIX}/lib
                   ${GNU_LIB_GUESS}
                   ${INTEL_LIB_GUESS}
                   NO_DEFAULT_PATH
)

find_library(HDF5_LIBRARY
             NAMES libhdf5.a
             PATHS ${HDF5_LIB_PATH}
                   ${THIRD_PARTY_PREFIX}/lib
                   ${GNU_LIB_GUESS}
                   ${INTEL_LIB_GUESS}
                   NO_DEFAULT_PATH
)

if (USE_PARMETIS)
  find_library(PARMETIS_LIBRARY
               NAMES parmetis
               PATHS ${PARMETIS_LIB_PATH}
                     ${THIRD_PARTY_PREFIX}/lib
                     ${GNU_LIB_GUESS}
                     ${INTEL_LIB_GUESS}
                     NO_DEFAULT_PATH
  )

  find_library(METIS_LIBRARY
              NAMES metis
              PATHS ${METIS_LIB_PATH}
                    ${THIRD_PARTY_PREFIX}/lib
                    ${GNU_LIB_GUESS}
                    ${INTEL_LIB_GUESS}
                    NO_DEFAULT_PATH
  )
endif()

find_library(PETSC_LIBRARY
             NAMES petsc
                   libcraypetsc_gnu_real.a
                   libcraypetsc_cray_real.a
             PATHS ${PETSC_LIB_PATH}
                   ${THIRD_PARTY_PREFIX}/lib
                   ${GNU_LIB_GUESS}
                   ${INTEL_LIB_GUESS}
                   NO_DEFAULT_PATH
)

if (PETSC_LIBRARY)
  list(APPEND PETSC_LIBRARY ${CMAKE_DL_LIBS})
endif ()

find_library(ML_LIBRARY
             NAMES ml
                   libcraypetsc_gnu_real.a
                   libcraypetsc_cray_real.a
             PATHS ${ML_LIB_PATH}
                   ${THIRD_PARTY_PREFIX}/lib
                   ${GNU_LIB_GUESS}
                   ${INTEL_LIB_GUESS}
                   NO_DEFAULT_PATH
)

find_library(HYPRE_LIBRARY
             NAMES HYPRE
                   libcraypetsc_gnu_real.a
                   libcraypetsc_cray_real.a
             PATHS ${HYPRE_LIB_PATH}
                   ${THIRD_PARTY_PREFIX}/lib
                   ${GNU_LIB_GUESS}
                   ${INTEL_LIB_GUESS}
                   NO_DEFAULT_PATH
)

find_library(Z_LIBRARY
             NAMES libz.a
             PATHS ${Z_LIB_PATH}
                   ${THIRD_PARTY_PREFIX}/lib
                   NO_DEFAULT_PATH
)

if(Z_LIBRARY STREQUAL "Z_LIBRARY-NOTFOUND")
  find_library(Z_LIBRARY
               NAMES libz.a
               PATHS ${GNU_LIB_GUESS}
                     ${INTEL_LIB_GUESS}
                     NO_DEFAULT_PATH
)
endif(Z_LIBRARY STREQUAL "Z_LIBRARY-NOTFOUND")

################################################################################
# System lib's

# For now, use the dynamic math library, may need to change to static in
# the future, or for the CRAY's
if (USE_MATH_LIB)
  find_library(MATH_LIBRARY
               NAMES libm.so
               PATHS /usr/lib64
               /usr/lib
  )
endif(USE_MATH_LIB)

# Find the native blas/lapack lib's even if they aren't used
find_library(BLAS_LIBRARY
             NAMES libblas.a
                   libsci.a
                   libsci_gnu_mp.a
                   libsci_intel_mp.a
                   libsci_cray_mp.a
                   libsci_pgi_mp.a
             PATHS ${BLAS_LIB_PATH}
                   ${THIRD_PARTY_LIB_DIR}
                   ${THIRD_PARTY_PREFIX}/lib
                   ${GNU_LIB_GUESS}
                   ${INTEL_LIB_GUESS}
                   ${CRAY_BLASLAPACK_LIB_GUESS}
                   /usr/lib64
                   /usr/lib
)

find_library(LAPACK_LIBRARY
             NAMES liblapack.a
                   libsci.a
                   libsci_gnu_mp.a
                   libsci_intel_mp.a
                   libsci_cray_mp.a
                   libsci_pgi_mp.a
             PATHS ${LAPACK_LIB_PATH}
                   ${THIRD_PARTY_LIB_DIR}
                   ${THIRD_PARTY_PREFIX}/lib
                   ${GNU_LIB_GUESS}
                   ${INTEL_LIB_GUESS}
                   ${CRAY_BLASLAPACK_LIB_GUESS}
                   /usr/lib64
                   /usr/lib
)

IF(CMAKE_CXX_COMPILER_ID MATCHES "Intel")
  set (LAPACK_LIBRARY   "")
  set (BLAS_LIBRARY     "")
  set (GFORTRAN_LIBRARY "")
  set (QUADMATH_LIBRARY "")
  find_library(MKL_INTEL_LIBRARY
               NAMES mkl_intel_lp64    # 32-bit interface layer req'd by PETSc
               PATHS ${MKL_LIB_PATH}
                     $ENV{MKLROOT}/lib/intel64
                     ${THIRD_PARTY_PREFIX}/lib
                     /usr/lib64
                     /usr/lib
  )

  find_library(MKL_INTEL_THREAD_LIBRARY
               NAMES mkl_sequential
               PATHS ${MKL_LIB_PATH}
                     $ENV{MKLROOT}/lib/intel64
                     ${THIRD_PARTY_PREFIX}/lib
                     /usr/lib64
                     /usr/lib
  )

  find_library(MKL_CORE_LIBRARY
               NAMES mkl_core
               PATHS ${MKL_LIB_PATH}
                     $ENV{MKLROOT}/lib/intel64
                     ${THIRD_PARTY_PREFIX}/lib
                     ${THIRD_PARTY_LIB_DIR}
                     /usr/lib64
                     /usr/lib
  )

  find_library(PTHREAD_LIBRARY
             NAMES pthread
             PATHS ${THIRD_PARTY_PREFIX}/lib
                   /usr/lib/x86_64-linux-gnu
                   /usr/lib64
                   /usr/lib
 )
ENDIF()

# Zero out libraries not needed for gnu, find gfortran lib
IF("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  set (MKL_INTEL_LIBRARY         "")
  set (MKL_INTEL_THREAD_LIBRARY  "")
  set (MKL_CORE_LIBRARY          "")
  set (PTHREAD_LIBRARY           "")
  # These are really PetSC/LAPACK dependencies
  find_library(GFORTRAN_LIBRARY
               NAMES libgfortran.a libfortran.dylib gfortran
               PATHS /usr/lib/gcc/x86_64-linux-gnu/7
                     /usr/lib/gcc/x86_64-linux-gnu/9
                     /usr/lib64/gcc/x86_64-suse-linux/4.8
  )
  find_library(QUADMATH_LIBRARY
               NAMES libquadmath.a libquadmath.dylib quadmath
               PATHS /usr/lib/gcc/x86_64-linux-gnu/7
                     /usr/lib/gcc/x86_64-linux-gnu/9
                     /usr/lib64/gcc/x86_64-suse-linux/4.8
  )
ENDIF()

# Zero out libraries not needed for cray
IF(CRAY)
  set (MKL_INTEL_LIBRARY         "")
  set (MKL_INTEL_THREAD_LIBRARY  "")
  set (MKL_CORE_LIBRARY          "")
  set (PTHREAD_LIBRARY           "")
ENDIF()
