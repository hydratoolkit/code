//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLUserVelocityIC.h
//! \author  Mark A. Christon
//! \date    Thu Apr 03 11:50:35 2014
//! \brief   User-defined velocity IC's
//******************************************************************************

#ifndef CCLUserVelocityIC_h
#define CCLUserVelocityIC_h

#include <Material.h>
#include <UnsMesh.h>

namespace Hydra {

//! User-defined velocity IC
class CCLUserVelocityIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLUserVelocityIC();
    virtual ~CCLUserVelocityIC();
    //@}

    //! Virtual function to setup IC's using a mesh and set of DataIndices
    void setICs(UnsMesh* mesh, const DataIndex VEL);

    //! Set user-defined velocity initial conditions
    void setUserVelocity();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLUserVelocityIC(const CCLUserVelocityIC&);
    CCLUserVelocityIC& operator=(const CCLUserVelocityIC&);
    //@}

    //! \name Data presented for the user-defined velocity function
    //@{
    int m_elemId;      //!< Element Id for user access
    int m_matId;       //!< Material Id for user access
    Vector m_coord;    //!< Element centroid coordinates
    Vector m_vel;      //!< Element velocity
    Material* m_mat;   //!< Material class for user access
    //@}

};

}

#endif // CCLUserVelocityIC_h
