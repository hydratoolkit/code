//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLPMNodeSolver2D.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   2-D node solver using Maire's algorithm
//******************************************************************************

#ifndef CCLPMNodeSolver2D_h
#define CCLPMNodeSolver2D_h

#include <HydraTypes.h>
#include <CCLNodeSolver.h>

namespace Hydra {

class CCLPMNodeSolver2D : public CCLNodeSolver {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLPMNodeSolver2D(UnsMesh& mesh,
                               Control& control,
                               CCLagrangianVar& di,
                               CCLagrangian2DVars& di2D,
                               CCLagrangian3DVars& di3D,
                               vector<CCLZeroVelBC*>& velbc,
                               vector<CCLVelocityBC*>& velocitybc,
                               vector<CCLPressureBC*>& pressurebc);
    virtual ~CCLPMNodeSolver2D() {}
    //@}

    //! Setup the nodal projection algorithm
    virtual void setup();

    //! Setup the nodal projection algorithm
    virtual void projectVelocity(Real time);

    //! L and b matrices are constructed for materials with strength
    virtual void constructMatricesWithStrength();

    //! L and b matrices are constructed for materials without strength
    virtual void constructMatricesWithoutStrength();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLPMNodeSolver2D(const CCLPMNodeSolver2D&);
    CCLPMNodeSolver2D& operator=(const CCLPMNodeSolver2D&);
    //@}

    //! Compute maximal node impedance
    void computeMaxNodeImpedance();

    //! Solve for node velocities
    void solveForNodeVelocities();

    //**************************************************************************
    //! \name DataIndex Declarations
    //@{
    DataIndex VSYS2D;  //!< Linear system for velocity projection
    //@}

};

}
#endif // CCLPMNodeSolver2D_h
