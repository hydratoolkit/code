//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLZeroVelBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:40:02 2011
//! \brief   Simple zero velocity BC for cell-centered Lagrangian hydro
//******************************************************************************
#ifndef CCLZeroVelBC_h
#define CCLZeroVelBC_h

#include <UnsMesh.h>
#include <SideSetBC.h>

namespace Hydra {

//! Zero velocity boundary condition based on sidesets
class CCLZeroVelBC : public SideSetBC {

  public:

    enum BCdir {XDIR = 0, YDIR, ZDIR, NDIR};

    //! \name Constructor/Destructor
    //@{
             CCLZeroVelBC(const BCPackage& bc, BCdir bcdir);
    virtual ~CCLZeroVelBC() {}
    //@}

    //! Apply nodal velocity BC's
    void applyNodalBCs(UnsMesh* mesh, CVector nvar);

    //! Setup Dirichlet BC in ghost list for later use in flux calculation
    void setupGhostBC(UnsMesh* mesh, DataIndex U, DataIndex SD, DataIndex GD,
                      DataIndex FLGVEL, DataIndex GSD, bool solveForShearStress);

    //! Set the direction for a given sideset bc
    void setDir(BCdir xyz) {m_dir = xyz;}

    //! Get the specified temperature value for a given sideset bc
    BCdir getDir() {return m_dir;}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLZeroVelBC(const CCLZeroVelBC&);
    CCLZeroVelBC& operator=(const CCLZeroVelBC&);
    //@}

    BCdir m_dir;

};

}

#endif // CCLZeroVelBC_h
