//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLVelocityBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:40:02 2011
//! \brief   Velocity BC for cell-centered Lagrangian hydro
//******************************************************************************
#ifndef CCLVelocityBC_h
#define CCLVelocityBC_h

#include <UnsMesh.h>
#include <SideSetBC.h>

namespace Hydra {

//! Velocity boundary conditions based on sidesets
class CCLVelocityBC : public SideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLVelocityBC(const BCPackage& bc, int offset);
    virtual ~CCLVelocityBC() {}
    //@}

    enum BCtype {VELX, VELY, VELZ};

    //! Apply nodal velocity BC's
    void applyNodalBCs(UnsMesh* mesh, CVector nvar, Real t);

    //! Setup velocity BC in ghost list for later use in flux calculation
    void setupGhostBC(UnsMesh* mesh, DataIndex GD,
                      DataIndex FLGVEL, DataIndex SD, DataIndex GSD,
                      bool solveForShearStress, Real t);

    //! Get velocity direction
    GlobalDirection getDirection() const {return m_direction;}

  protected:

  private:
    //! Don't permit copy or assignment operators
    //@{
    CCLVelocityBC(const CCLVelocityBC&);
    CCLVelocityBC& operator=(const CCLVelocityBC&);
    //@}

    GlobalDirection m_direction;
};

}

#endif // CCLVelocityBC_h
