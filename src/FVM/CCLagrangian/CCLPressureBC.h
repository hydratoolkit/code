//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLPressureBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:40:02 2011
//! \brief   Pressure BC for cell-centered Lagrangian hydro
//******************************************************************************
#ifndef CCLPressureBC_h
#define CCLPressureBC_h

#include <UnsMesh.h>
#include <SideSetBC.h>

namespace Hydra {

//! Pressure boundary conditions based on sidesets
class CCLPressureBC : public SideSetBC {
  public:
    //! \name Constructor/Destructor
    //@{
             CCLPressureBC(const BCPackage& bc, int offset);
    virtual ~CCLPressureBC() {}
    //@}

    //! Setup velocity BC in ghost list for later use in flux calculation
    void setupGhostBC(UnsMesh* mesh, Real* gp, Real* flgVelBC, Real* flgPressBC,
                      DataIndex GSD, bool solveForShearStress, Real t);

  protected:

  private:
    //! Don't permit copy or assignment operators
    //@{
    CCLPressureBC(const CCLPressureBC&);
    CCLPressureBC& operator=(const CCLPressureBC&);
    //@}

};

}

#endif // CCLPressureBC_h
