//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLVsys.h
//! \author  Shiv K. Sambasivan, Mark A. Christon
//! \date    July, 2014
//! \brief   Structs used for node-based CC-Lagrangian solvers
//******************************************************************************

#ifndef CCLVsys_h
#define CCLVsys_h

namespace Hydra {

//! Maire node solver
struct Vsys2D {
  Real L[4];
  Real b[2];
};

struct Vsys3D {
  Real L[9];
  Real b[3];
};

//! Don Burton's node solver
struct VsysDB2D {
  Real L[2];
  Real b;
};

struct VsysDB3D {
  Real L[3];
  Real b;
};

}
#endif
