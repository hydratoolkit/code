//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLagrangianHistoryDelegates.h
//! \author  Mark A. Christon
//! \date    Tue Aug 21 15:11:59 MDT 2018
//! \brief   History delegates for FVM CC Lagrangian
//******************************************************************************
#ifndef CCLagrangianHistoryDelegates_h
#define CCLagrangianHistoryDelegates_h

#include <CCLagrangian.h>

namespace Hydra {


OUTPUT_DELEGATE_DECL(CCLagrangian,
                     writeNodeVectorHistory,
                     CCLagrangianNodeVectorHistory)
}

#endif // CCLagrangianHistoryDelegates.h
