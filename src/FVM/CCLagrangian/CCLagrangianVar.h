//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLagrangianVar.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   DataIndex values for transport
//******************************************************************************
#ifndef CCLagrangianVar_h
#define CCLagrangianVar_h

#include <HydraTypes.h>

namespace Hydra {

// Used for static mem allocation
#define FVMCCL_MAXNNPE 100
#define FVMCCL_MAXNFPE 100
#define FVMCCL_MAXNEPE 100

struct subcellGeometry{
  vector<Real> normalX;
  vector<Real> normalY;
  vector<Real> normalZ;
  vector<Real> area;
  vector<int>  lconn;
  vector<int>  conn;
};

struct CCLagrangian2DVars{
  DataIndex VORTICITY_ZZ;
};

struct CCLagrangian3DVars{
  DataIndex VORTICITY_XX;
  DataIndex VORTICITY_YY;
  DataIndex VORTICITY_ZZ;
  DataIndex FACE_GEOMETRY;
};

//! DataIndex values for cell-centered hydro
struct CCLagrangianVar {

  // Field variables: Variables that are store at the element center.
  // These variables have a fixed size as long as new generators/elements
  // arent introduced.
  DataIndex PRESSURE;
  DataIndex DENSITY;
  DataIndex ELEM_MASS;
  DataIndex ELEM_VEL;
  DataIndex INT_ENERGY;
  DataIndex TOT_ENERGY;
  DataIndex DEV_STRESS;
  DataIndex CUR_VOLUME;
  DataIndex DELTA_MOMENTUM;
  DataIndex DELTA_ENERGY;
  DataIndex TOT_ENERGY_OLD;
  DataIndex ELEM_VEL_OLD;
  DataIndex DEV_STRESS_OLD;
  DataIndex ELEMENT_IMPEDANCE;
  DataIndex VELOCITY_GRADIENT;
  DataIndex PRESSURE_GRADIENT;
  DataIndex SXX_GRADIENT;
  DataIndex SYY_GRADIENT;
  DataIndex SZZ_GRADIENT;
  DataIndex SXY_GRADIENT;
  DataIndex SYZ_GRADIENT;
  DataIndex SZX_GRADIENT;
  DataIndex EQ_PLASTIC_STRAIN;
  DataIndex EQ_PLASTIC_STRAINRATE;

  // Node Variables: These variables do not have a fixed size during
  //                 ReALE computations. They need to be managed
  //                 each time new Voronoi mesh is constructed.
  DataIndex NODE_IMPEDANCE_LLF;
  DataIndex NODE_COORDS_OLD;
  DataIndex NODE_DISP_OLD;
  DataIndex NODE_DISP;
  DataIndex NODE_VEL;

  // Edge Variables: These variables are of the size of Next_edge.
  //                 These variables do not have a fixed size during
  //                 ReALE computations. They need to be managed
  //                 each time new Voronoi mesh is constructed.
  DataIndex GHOST_XC;        // ghost coordinates
  DataIndex GHOST_VEL;       // ghost Velocity
  DataIndex GHOST_PRESSURE;  // ghost Pressure
  DataIndex GHOST_DENSITY;   // Ghost Density
  DataIndex GHOST_INT_ENERGY;// Ghost IE
  DataIndex GHOST_IMPEDANCE; // Ghost Element Impedance
  DataIndex GHOST_DEV_STRESS;
  DataIndex FLAG_VELOCITYBC_EDGES;
  DataIndex FLAG_PRESSUREBC_EDGES;

  // Send and recv buffers for node communicators
  DataIndex SEND_BUF;
  DataIndex RECV_BUF;

  // Temporary variables
  DataIndex TEMP_ELEM;
  DataIndex TEMP_NODE;

};

}

#endif // CCLagrangianVar_h
