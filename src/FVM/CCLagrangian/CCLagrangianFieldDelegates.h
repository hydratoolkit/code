//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLagrangianFieldDelegates.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Field delegates for FVM CC Lagrangian
//******************************************************************************
#ifndef CCLagrangianFieldDelegates_h
#define CCLagrangianFieldDelegates_h

#include <CCLagrangian.h>

namespace Hydra {


OUTPUT_DELEGATE_DECL(CCLagrangian,
                     writeElemEqDeviatoricStressField,
                     CCLagrangianEqDeviatoricStress)

OUTPUT_DELEGATE_DECL(CCLagrangian,
                     writeElemDeviatoricStressField,
                     CCLagrangianDeviatoricStress)

OUTPUT_DELEGATE_DECL(CCLagrangian,
                     writeElemEqStressField,
                     CCLagrangianEqStress)

OUTPUT_DELEGATE_DECL(CCLagrangian,
                     writeElemStressField,
                     CCLagrangianStress)
}


#endif // CCLagrangianFieldDelegates.h
