//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLNodeSolver.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Virtual base class for velocity node solver
//******************************************************************************

#ifndef CCLNodeSolver_h
#define CCLNodeSolver_h

#include <HydraTypes.h>
#include <UnsMesh.h>
#include <Control.h>
#include <CCLagrangianVar.h>
#include <CCLPressureBC.h>
#include <CCLVelocityBC.h>
#include <CCLZeroVelBC.h>

namespace Hydra {

class CCLNodeSolver {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLNodeSolver(UnsMesh& mesh,
                           Control& control,
                           CCLagrangianVar& di,
                           CCLagrangian2DVars& di2D,
                           CCLagrangian3DVars& di3D,
                           vector<CCLZeroVelBC*>& velbc,
                           vector<CCLVelocityBC*>& velocitybc,
                           vector<CCLPressureBC*>& pressurebc);
    virtual ~CCLNodeSolver() {}
    //@}

    //! Setup the nodal projection algorithm
    virtual void setup() = 0;

    //! Setup the nodal projection algorithm
    virtual void projectVelocity(Real time) = 0;

    //! Form the L and b matrices
    void constructMatrices();

    //! L and b matrices are constructed for materials with strength
    virtual void constructMatricesWithStrength() = 0;

    //! L and b matrices are constructed for materials without strength
    virtual void constructMatricesWithoutStrength() = 0;

    //! Check if there are materials with strength present in the domain
    bool domainHasMatStrength();

  protected:

    bool m_solveForShearStress;

    int m_Nel;
    int m_Nnp;
    int m_Nedge;
    int m_Nedge_ext;
    int m_Nproc;

    int  m_RiemannType;       //!< Type of Riemann solver
    int  m_RiemannIterMax;    //!< Maximum number of Riemann iterations

    Real m_flagVertexLLF;     //!< Flag for vertex Local Lax-Freidrichs
    Real m_flagSpatialOrder;  //!< Flag for spatial order - reconstruction
    Real m_flagElastoPlastic; //!< Elastic-plastic flag

    Real m_time;              //!< Current simulation time

    CCLagrangianVar m_di;
    CCLagrangian2DVars m_di2D;
    CCLagrangian3DVars m_di3D;

    vector<CCLZeroVelBC*>&  m_zero_velbc;
    vector<CCLVelocityBC*>& m_velocitybc;
    vector<CCLPressureBC*>& m_pressurebc;

    UnsMesh& m_mesh;

    Control& m_control;

    DualEdge* edges;          //!< Dual-edge pointer held, not owned

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLNodeSolver(const CCLNodeSolver&);
    CCLNodeSolver& operator=(const CCLNodeSolver&);
    //@}

};

}
#endif // CCLNodeSolver_h
