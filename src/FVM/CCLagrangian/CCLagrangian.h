//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLagrangian.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:40:02 2011
//! \brief   Cell-Centered Lagrangian Hydro
//******************************************************************************

#ifndef CCLagrangian_h
#define CCLagrangian_h

#include <Block.h>
#include <Control.h>
#include <fileIO.h>
#include <FVMCCLagrangianControl.h>
#include <UnsMesh.h>
#include <CCclaw.h>
#include <CCLBlockICs.h>
#include <CCLagrangianVar.h>
#include <CCLPressureBC.h>
#include <CCLVelocityBC.h>
#include <CCLZeroVelBC.h>
#include <UserIC.h>
#include <CCLUserTemperatureIC.h>
#include <CCLUserVelocityIC.h>

namespace Hydra {

//! Coefficients set upto 4th order
//! rkCoeff[time_order][time_iteration];
const Real rkCoeffA[3][3] = {{1.0, 0.0 , 0.0 },
                             {1.0, 0.5 , 0.0 },
                             {1.0, 0.75, 1.0/3.0}};
const Real rkCoeffB[3][3] = {{0.0, 0.0 , 0.0 },
                             {0.0, 0.5 , 0.0 },
                             {0.0, 0.25, 2.0/3.0}};
const Real rkCoeffC[3][3] = {{1.0, 0.0 , 0.0 },
                             {1.0, 0.5 , 0.0 },
                             {1.0, 0.25, 2.0/3.0}};

class CCLNodeSolver;

//! Initial condition package
struct CCLICPackage {
  Real velx;
  Real vely;
  Real velz;
  Real temp;
  CCLICPackage() : velx(0.0),
                   vely(0.0),
                   velz(0.0),
                   temp(0.0) {}
};

//! Cell-centered Lagrangian class
class CCLagrangian : public CCclaw {

  public:

    //! Enum's for CCLagrangian
    enum PredefICType {
      NO_IC,
      NOH_IC,
      SEDOV_IC,
      ONEDWITHSTRENGTH_IC,
      ALSHOCKTUBE_IC,
      VERNEY_IC,
      VERNEYCYL_IC,
      TAYLORBAR_IC,
      PLATEVIBRATION_IC,
      SPHERICALSHOCKTUBE_IC,
      RIGID_ROTATION_IC
    };

    //! Velocity limter
    enum VelocityLimiterOption{
      NORMAL_LIMITER      = 1,
      VELOCITY_PROJECTION = 2,
      VELOCITY_MAGNITUDE  = 3
    };

    //! Limiter type
    enum LimiterType {
      BARTH_JESPERSON = 1,
      VENKATAKRISHNAN = 2
    };

    //! \name Constructor/Destructor
    //@{
             CCLagrangian();
    virtual ~CCLagrangian();
    //@}

    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData();
    //@}

    //! \name Virtual Physics Functions
    //! These functions are virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    virtual void setup();

    virtual void initialize();

    virtual void solve();

    virtual void finalize();

    virtual void setupMergedSetBCs() {}
    //@}

    //! \name Virtual Functions for I/O
    //! These functions are implemented for each specific physics
    //! to permit custom options to be coded for reading and writing
    //! restart files, time-history data, bc's, etc.
    //@{
    virtual void writeSolving();

    virtual void echoOptions(ostream& ofs);
    //@}

    //**************************************************************************
    //! \name Boundary Conditions
    //@{
    //! Echo boundary conditions
    void echoBCs(ostream& ofs);

    //! Echo no-penetration boundary conditions
    void echoNoPenetrationBCs(ostream& ofs);

    //! Echo pressure boundary conditions
    void echoPressureBCs(ostream& ofs);

    //! Echo velocity boundary conditions
    void echoVelocityBCs(ostream& ofs);

    //@}

    //**************************************************************************
    //! \name Output Methods
    //@{
    //! Write the header for the global time-history data file
    void writeGlobHeader();

    //! Write the global time history data at each step
    virtual void writeGlobFile();

    //! Write the screen report
    //!   \param[in] ofs  Reference to output stream
    virtual void writeReport(ostream& ofs) = 0;

    //! Write the header for the screen report
    //!   \param[in] ofs  Reference to output stream
    virtual void writeReportHeader(ostream& ofs) = 0;
    //@}

    //**************************************************************************
    //! Initial Conditions
    //@{
    //! Add a block velocity initial condition
    void addBlockICs() {m_blockICs.push_back(new CCLBlockICs);}

    //! Load a block velocity IC object
    CCLBlockICs* getBlockICs(int i) {return m_blockICs[i];}

    //! How many block velocity IC's are loaded
    int numBlockICs() {return m_blockICs.size();}

    //! Add pre-defined initial condtion
    void addPredefIC(PredefICType ictype);

    //! Add user-defined temperature initial condition
    void addUserTemperatureIC();

    //! Add user-defined velocity initial condition
    void addUserVelocityIC();

    //! Driver for application of global, block, pre-defined, user-defined IC's
    void applyICs();

    //! Apply block-based initial conditions
    void applyBlockICs();

    //! Apply global initial conditions
    void applyGlobalICs();

    //! Apply pre-defined initial conditions
    void applyPredefICs();

    //! Apply user-defined initial conditions
    void applyUserICs();

    //! Echo the block IC's
    void echoBlockICs(ostream& ofs);

    // Echo all the IC's
    void echoICs(ostream& ofs);

    //! Echo velocity IC's
    void echoVelocityICs(ostream& ofs);

    //! Set the prescribed initial conditions
    void setICs(CCLICPackage& ic);

    //@}

    //! Update momentum and energy
    virtual void updateState() = 0;

    // The ghost varaibles on the external edges
    // are initializedm BCs are applied and
    // communication is performed if needed.
    virtual void setGhostVarsAndApplyBCs() = 0;

    //! This function updates the old time step values (not done every RK step).
    //! This is required for high-order RK based time stepping schemes.
    virtual void updateOldStateVars() = 0;

    //! Gradients for the field variables that are required by the
    //! Lagrangian flow solver are computed here
    //@{
    virtual void calcLimitedGradients() = 0;

    //! Compute gradients based on mimetic operator
    virtual void calcMimeticGradients() = 0;
    //@}

    //! Compute Least Squares and/or Limit gradients
    //@{
    virtual void limitGradients(vector<DataIndex> listVarsLimit,
                                vector<DataIndex> listVarsGrad) = 0;

    virtual void vorticityLimiterForVelocityGradients() = 0;

    virtual void calcContourIntegralBasedElemVort() = 0;

    virtual void adjustGradP() = 0;
    //@}

    //! L2 Projection of the element velocity to the nodes
    virtual void projectElemVelocity() = 0;

    //! Calculate the centroids for ghost data at boundaries
    virtual void setGhostCentroids() = 0;

    //! Update the configuration
    virtual void updateConfiguration() = 0;

    //! Calculate the stable time step
    //@{
    void calcTimeStep();

    virtual Real calcTimeStep(Real dtmin) = 0;
    //@}

    //###### DEBUG
    virtual void scatterAndOneDPlots(void) = 0;
    //###### DEBUG

    //! Update the pressure
    void updatePressure();

     //! Compute Mass
    void formMass();

    //! Calculate element impedance
    void calcElementImpedance();

    /*! \name CCLagrangian specific boundary conditions
        These functions are specific to the Euler eqs. and are only
        implemented for this physics class.
     */
    //@{
    //! Add a pressure boundary condition
    void addPressureBC(const BCPackage& bc);

    CCLPressureBC* getPressureBC(int i) {return m_pressurebc[i];}

    //! How many pressure bc's are loaded
    int numPressureBC() {return m_pressurebc.size();}

    //! Add a zero velocity boundary condition
    void addZeroVelBC(const BCPackage& bc, CCLZeroVelBC::BCdir dir);

    //! Load a zero velocity boundary condtion
    CCLZeroVelBC* getZeroVelBC(int i) {return m_zero_velbc[i];}

    //! How zero velocity temperature bc's are loaded
    int numZeroVelBC() {return m_zero_velbc.size();}

    //! Add a velocity boundary condition
    void addVelocityBC(const BCPackage& bc);

    //! Load a velocity boundary condtion
    CCLVelocityBC* getVelocityBC(int i) {return m_velocitybc[i];}

    //! How many velocity bc's are loaded
    int numVelocityBC() {return m_velocitybc.size();}

    //! Enforce (plastic) consistency condition
    void radialReturn(Element* ec);
    //@}

    void setCFL(Real parm) {m_CFL = parm;}

    int sizeofVsys();

    //**************************************************************************
    //! \name Output Delegates & Utility Routines
    //@{
    //! Calculate global internal and kinetic energy
    virtual void calcGlobalEnergy() = 0;

    //! Output the equivalent deviatoric stress
    void writeElemEqDeviatoricStressField(const OutputDelegateKey& key,
                                          int plnum, int varId);

    //! Output the deviatoric stress
    void writeElemDeviatoricStressField(const OutputDelegateKey& key,
                                        int plnum, int varId);

    //! Output the equivalent stress
    void writeElemEqStressField(const OutputDelegateKey& key,
                                int plnum, int varId);

    //! Output the stress
    void writeElemStressField(const OutputDelegateKey& key,
                              int plnum, int varId);

    //! Output the vector time history
    void writeNodeVectorHistory(const OutputDelegateKey& key,
                                int plnum, int varId);
    //@}

    //**************************************************************************
    //! \name Material property evaluation/update
    //@{
    //! setup material property dependencies
    void setupMaterialDependence();
    //@}

 protected:

    //! Time step control
    Real m_dt;
    Real m_time;

    //! CFL
    Real m_CFLmin;
    Real m_CFL;
    Real m_dtscale;

    //! Energies
    Real m_kin_energy;  //!< Global kinetic energy
    Real m_int_energy;  //!< Global internal energy
    Real m_tot_energy;  //!< Tlobal total energy

    //! R-K parameters
    int  m_rkIter;       //!< Number of R-K iterations
    int  m_rkOrder;      //!< R-K order

    // Flags
    int  m_RiemannType;                    //!< Type of Riemann solver
    int  m_spatialOrder;                   //!< Spatial order for reconstruction
    Real m_flagVenkatakrishnanLimiter;     //!< Venkatakrishnan limiter
    Real m_safetyLimiterParameter;         //!< Safety valve for limiter
    int  m_gradientMethod;                 //!< Gradient method
    int  m_velocityLimiter;                //!< Velocity limiter
    int  m_limiterOption;                  //!< Limiter option
    bool m_correctVelocityGradientForDivU;
    bool m_correctVelocityGradientForVorticity; //not implemented
    bool m_solveForShearStress;

    vector<DataIndex> m_listSolVarsLimit;
    vector<DataIndex> m_listSolVarsGrad;

    //**************************************************************************
    //! \name STL vectors of BC objects
    //@{
    vector<CCLZeroVelBC*>  m_zero_velbc;
    vector<CCLVelocityBC*> m_velocitybc;
    vector<CCLPressureBC*> m_pressurebc;
    //@}

    //**************************************************************************
    //! /name Initial Conditions
    //@{
    //! Global IC's
    CCLICPackage m_ics;

    //! Block Initial Condition Containers
    vector<CCLBlockICs*> m_blockICs;

    //! Pre-defined initial conditions
    vector<UserIC*> m_predefic;

    // User-defined IC's
    vector<CCLUserTemperatureIC*> m_userTemperatureICs;
    vector<CCLUserVelocityIC*>    m_userVelocityICs;
    //@}

    CCLNodeSolver* m_node_solver;
    CCLagrangianVar m_di;
    CCLagrangian2DVars m_di2D;
    CCLagrangian3DVars m_di3D;

    int m_elemvar_dim;
    int m_sendvar_dim;

    bool m_isRestart;

    int m_Ninc;
    int m_Nsteps;
    int m_plti;
    int m_plnum;
    int m_thti;
    int m_thnum;
    int m_ttyi;
    int m_dmpi;
    int m_thiscnt;
    int m_plotcnt;
    int m_scrncnt;
    int m_dumpcnt;
    Real m_term;
    Real m_flag1DWithStrength;

    Category* m_ocat;

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLagrangian(const CCLagrangian&);
    CCLagrangian& operator=(const CCLagrangian&);
    //@}

    //**************************************************************************
    //! \name Registration for all variables and communications buffers
    //@{
    //! Register element-centered data
    void registerElemData();

    //! Register node-centered data
    void registerNodeData();

    //! Register edge-centered data
    void registerEdgeData();

    //! Register edge-centered data
    void registerExternalEdgeData();

    //! Register parallel Data
    void registerParallelData();

    //! Register temporary data
    void registerTempData();
    //@}

    bool domainHasMatStrength();

};

}
#endif // CCLagrangian_h
