//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLBlockICs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Block-based IC's for the cell-centered Lagrangian hydro
//******************************************************************************

#ifndef CCLBlockICs_h
#define CCLBlockICs_h

#include <HydraTypes.h>
#include <Block.h>

namespace Hydra {

//! CC Lagrangian Block Velocity Initial Conditions
class CCLBlockICs : public Block {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLBlockICs() {}
    virtual ~CCLBlockICs() {}
    //@}

    //! \name CCLBlockICs access functions
    //! The CCLBlockICs class is used to setup physics-specific
    //! block initial velocity conditions.
    //@{
    void setTemp(Real T) {temp = T;}
    void setVelIC(Real* vel) {velic[0] = vel[0];
                              velic[1] = vel[1];
                              velic[2] = vel[2];}

    Real  getTemp()  {return temp;}
    Real* getVelIC() {return velic;}
    Real  getVelx()  {return velic[0];}
    Real  getVely()  {return velic[1];}
    Real  getVelz()  {return velic[2];}
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLBlockICs(const CCLBlockICs&);
    CCLBlockICs& operator=(const CCLBlockICs&);
    //@}

    Real velic[3];
    Real temp;
};

}

#endif // CCLBlockICs_h
