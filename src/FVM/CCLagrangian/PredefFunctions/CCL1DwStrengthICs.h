//******************************************************************************
//! \file    src/FVM/CCLagrangian/PredefFunctions/CCL1DwStrengthICs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   1-D w. Strength IC's for cell-centered Lagrangian
//******************************************************************************

#ifndef CCL1DwStrengthICs_h
#define CCL1DwStrengthICs_h

#include <HydraTypes.h>
#include <UnsMesh.h>
#include <UserIC.h>

namespace Hydra {

//! 1-D shock tube with strength
class CCL1DWithStrengthICs : public UserIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCL1DWithStrengthICs() {};
    virtual ~CCL1DWithStrengthICs() {};
    //@}

    //! Setup IC's using a mesh and set of DataIndices
    virtual void setICs(UnsMesh*, vector<DataIndex>& vars);

    //! Return a name for the IC's
    virtual string getName() {return "1-D with Strength";}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCL1DWithStrengthICs(const CCL1DWithStrengthICs&);
    CCL1DWithStrengthICs& operator=(const CCL1DWithStrengthICs&);
    //@}

};

}

#endif // CCL1DwStrengthICs_h
