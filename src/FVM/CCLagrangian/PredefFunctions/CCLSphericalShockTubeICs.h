//******************************************************************************
//! \file    src/FVM/CCLagrangian/PredefFunctions/CCLSphericalShockTubeICs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Spherical Shock Tube IC's for cell-centered Lagrangian
//******************************************************************************

#ifndef CCLSphericalShockTubeICs_h
#define CCLSphericalShockTubeICs_h

#include <HydraTypes.h>
#include <UnsMesh.h>
#include <UserIC.h>

namespace Hydra {

//! Spherical shock tube IC's
class CCLSphericalShockTubeICs : public UserIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLSphericalShockTubeICs() {};
    virtual ~CCLSphericalShockTubeICs() {};
    //@}

    //! Setup IC's using a mesh and set of DataIndices
    virtual void setICs(UnsMesh*, vector<DataIndex>& vars);

    //! Return a name for the IC's
    virtual string getName() {return "Spherical Shock Tube";}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLSphericalShockTubeICs(const CCLSphericalShockTubeICs&);
    CCLSphericalShockTubeICs& operator=(const CCLSphericalShockTubeICs&);
    //@}

};

}

#endif // CCLSphericalShockTubeICs_h
