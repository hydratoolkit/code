//******************************************************************************
//! \file    src/FVM/CCLagrangian/PredefFunctions/CCLSedovICs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Sedov problem IC's for cell-centered Lagrangian
//******************************************************************************

#ifndef CCLSedovICs_h
#define CCLSedovICs_h

#include <HydraTypes.h>
#include <UnsMesh.h>
#include <UserIC.h>

namespace Hydra {

//! Sedov Problem IC's
class CCLSedovICs : public UserIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLSedovICs() {};
    virtual ~CCLSedovICs() {};
    //@}

    //! Setup IC's using a mesh and set of DataIndices
    virtual void setICs(UnsMesh*, vector<DataIndex>& vars);

    //! Return a name for the IC's
    virtual string getName() {return "Sedov Problem";}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLSedovICs(const CCLSedovICs&);
    CCLSedovICs& operator=(const CCLSedovICs&);
    //@}
};

}

#endif // CCLSedovICs_h
