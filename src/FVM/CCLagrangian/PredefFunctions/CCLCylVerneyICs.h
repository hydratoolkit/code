//******************************************************************************
//! \file    src/FVM/CCLagrangian/PredefFunctions/CCLCylVerneyICs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Cylindrical Verney problem IC's for cell-centered Lagrangian
//******************************************************************************

#ifndef CCLCylVerneyICs_h
#define CCLCylVerneyICs_h

#include <HydraTypes.h>
#include <UnsMesh.h>
#include <UserIC.h>

namespace Hydra {

//! Cylindrical Verney problem IC's
class CCLCylVerneyICs : public UserIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLCylVerneyICs() {};
    virtual ~CCLCylVerneyICs() {};
    //@}

    //! Setup IC's using a mesh and set of DataIndices
    virtual void setICs(UnsMesh*, vector<DataIndex>& vars);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLCylVerneyICs(const CCLCylVerneyICs&);
    CCLCylVerneyICs& operator=(const CCLCylVerneyICs&);
    //@}
};

}

#endif // CCLCylVerneyICs_h
