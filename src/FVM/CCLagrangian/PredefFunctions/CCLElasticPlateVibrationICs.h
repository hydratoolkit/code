//******************************************************************************
//! \file    src/FVM/CCLagrangian/PredefFunctions/CCLElasticPlateVibrationICs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Elastic Plate Vibration IC's for cell-centered Lagrangian
//******************************************************************************

#ifndef CCLElasticPlateVibrationICs_h
#define CCLElasticPlateVibrationICs_h

#include <HydraTypes.h>
#include <UnsMesh.h>
#include <UserIC.h>

namespace Hydra {

//! Elastic-Plastic Plate Vibration
class CCLElasticPlateVibrationICs : public UserIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLElasticPlateVibrationICs() {};
    virtual ~CCLElasticPlateVibrationICs() {};
    //@}

    //! Setup IC's using a mesh and set of DataIndices
    virtual void setICs(UnsMesh*, vector<DataIndex>& vars);

    //! Return a name for the IC's
    virtual string getName() {return "Elastic-Plastic Plate Vibration";}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLElasticPlateVibrationICs(const CCLElasticPlateVibrationICs&);
    CCLElasticPlateVibrationICs& operator=(const CCLElasticPlateVibrationICs&);
    //@}

};

}

#endif // CCLElasticPlateVibrationICs_h
