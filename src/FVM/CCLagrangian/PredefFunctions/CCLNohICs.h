//******************************************************************************
//! \file    src/FVM/CCLagrangian/PredefFunctions/CCLNohICs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Noh problem IC's for cell-centered Lagrangian
//******************************************************************************

#ifndef CCLNohICs_h
#define CCLNohICs_h

#include <HydraTypes.h>
#include <UnsMesh.h>
#include <UserIC.h>

namespace Hydra {

//! IC's for Lagrangian Noh problem
class CCLNohICs : public UserIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLNohICs() {};
    virtual ~CCLNohICs() {};
    //@}

    //! Setup IC's using a mesh and set of DataIndices
    virtual void setICs(UnsMesh*, vector<DataIndex>& vars);

    //! Return a name for the IC's
    virtual string getName() {return "Noh Problem";}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLNohICs(const CCLNohICs&);
    CCLNohICs& operator=(const CCLNohICs&);
    //@}

};

}

#endif // CCLNohICs_h
