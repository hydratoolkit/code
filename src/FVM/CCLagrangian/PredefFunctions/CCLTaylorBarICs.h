//******************************************************************************
//! \file    src/FVM/CCLagrangian/PredefFunctions/CCLTaylorBarICs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Taylor Bar IC's for cell-centered Lagrangian
//******************************************************************************
#ifndef CCLTaylorBarICs_h
#define CCLTaylorBarICs_h

#include <HydraTypes.h>
#include <UnsMesh.h>
#include <UserIC.h>

namespace Hydra {

//! Taylor Anvil IC's
class CCLTaylorBarICs : public UserIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLTaylorBarICs() {};
    virtual ~CCLTaylorBarICs() {};
    //@}

    //! Setup IC's using a mesh and set of DataIndices
    virtual void setICs(UnsMesh*, vector<DataIndex>& vars);

    //! Return a name for the IC's
    virtual string getName() {return "Taylor Anvil";}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLTaylorBarICs(const CCLTaylorBarICs&);
    CCLTaylorBarICs& operator=(const CCLTaylorBarICs&);
    //@}

};

}

#endif // CCLTaylorBarICs_h
