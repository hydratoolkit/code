//******************************************************************************
//! \file    src/FVM/CCLagrangian/PredefFunctions/CCLRigidRotationICs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Rigid-body rotation abut the origin
//******************************************************************************
#ifndef CCLRigidRotationICs_h
#define CCLRigidRotationICs_h

#include <HydraTypes.h>
#include <UnsMesh.h>
#include <UserIC.h>

namespace Hydra {

//! Alumninum shock-tube IC's
class CCLRigidRotationICs : public UserIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLRigidRotationICs() {};
    virtual ~CCLRigidRotationICs() {};
    //@}

    //! Setup IC's using a mesh and set of DataIndices
    virtual void setICs(UnsMesh*, vector<DataIndex>& vars);

    //! Return a name for the IC's
    virtual string getName() {return "Rigid Body Rotation";}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLRigidRotationICs(const CCLRigidRotationICs&);
    CCLRigidRotationICs& operator=(const CCLRigidRotationICs&);
    //@}
};

}

#endif // CCLRigidRotationICs_h
