//******************************************************************************
//! \file    src/FVM/CCLagrangian/PredefFunctions/CCLVerneyICs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Verney problem IC's for cell-centered Lagrangian
//******************************************************************************

#ifndef CCLVerneyICs_h
#define CCLVerneyICs_h

#include <HydraTypes.h>
#include <UnsMesh.h>
#include <UserIC.h>

namespace Hydra {

//! Verney Problem IC's
class CCLVerneyICs : public UserIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLVerneyICs() {};
    virtual ~CCLVerneyICs() {};
    //@}

    //! Setup IC's using a mesh and set of DataIndices
    virtual void setICs(UnsMesh*, vector<DataIndex>& vars);

    //! Return a name for the IC's
    virtual string getName() {return "Verney problem";}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLVerneyICs(const CCLVerneyICs&);
    CCLVerneyICs& operator=(const CCLVerneyICs&);
    //@}
};

}

#endif // CCLVerneyICs_h
