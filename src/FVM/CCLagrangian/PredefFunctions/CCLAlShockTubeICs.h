//******************************************************************************
//! \file    src/FVM/CCLagrangian/PredefFunctions/CCLAlShockTubeICs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Aluminum shock tube IC's for cell-centered Lagrangian
//******************************************************************************

#ifndef CCLALShockTubeICs_h
#define CCLALShockTubeICs_h

#include <HydraTypes.h>
#include <UnsMesh.h>
#include <UserIC.h>

namespace Hydra {

//! Alumninum shock-tube IC's
class CCLAlShockTubeICs : public UserIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLAlShockTubeICs() {};
    virtual ~CCLAlShockTubeICs() {};
    //@}

    //! Setup IC's using a mesh and set of DataIndices
    virtual void setICs(UnsMesh*, vector<DataIndex>& vars);

    //! Return a name for the IC's
    virtual string getName() {return "Aluminum Shock Tube";}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLAlShockTubeICs(const CCLAlShockTubeICs&);
    CCLAlShockTubeICs& operator=(const CCLAlShockTubeICs&);
    //@}
};

}

#endif // CCLALShockTubeICs_h
