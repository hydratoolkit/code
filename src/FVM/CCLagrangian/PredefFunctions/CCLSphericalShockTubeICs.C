//******************************************************************************
//! \file    src/FVM/CCLagrangian/PredefFunctions/CCLSphericalShockTubeICs.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:40:02 2011
//! \brief   Spherical Shock Tube IC's for cell-centered Lagrangian
//******************************************************************************
#include <cassert>
#include <cmath>

using namespace std;

#include <CCLSphericalShockTubeICs.h>
#include <Material.h>

using namespace Hydra;

void
CCLSphericalShockTubeICs::setICs(UnsMesh* mesh, vector<DataIndex>& vars)
/*******************************************************************************
Routine: setICs - setup IC's for the spherical shock tube problem
Author : Mark A. Christon
*******************************************************************************/
{
  // DataIndex map:
  // var entry  | DataIndex =| Variable
  // ===========|============|=========================
  // 0          | ELEM_VEL   | element velocity
  // 1          | INT_ENERGY | internal energy
  // 2          | TOT_ENERGY | total energy
  // 3          | VOLUME     | element volume
  // 4          | DENSITY    | element density
  // 5          | PRESSURE   | element Pressure

  // Initialize velocities to zero by default

  const Real ShockLoc = 1.5;

  Real* e   = mesh->getVariable<Real>(vars[1]);
  Real* E   = mesh->getVariable<Real>(vars[2]);
  Real* rho = mesh->getVariable<Real>(vars[4]);
  Real* p   = mesh->getVariable<Real>(vars[5]);
  Real* xc  = mesh->getXC();
  Real* yc  = mesh->getYC();
  Real* zc  = mesh->getZC();
  int Nec = mesh->numElementClass();
  for (int j=0; j<Nec; j++) {
    Element* ec = mesh->getElementClass(j);
    int nel = ec->getNel();
    if (nel > 0) {
      int  mid = ec->getMatId();
      int* gids = ec->getGlobalIds();
      Material* mat = mesh->getMaterial(mid);
      for (int i=0; i<nel; i++) {
        int gid = gids[i];
        p[gid]  = 0.0;
        Real radius = sqrt(xc[i]*xc[i] + yc[i]*yc[i] + zc[i]*zc[i]);
        if(radius < ShockLoc){
          rho[gid] = 2764.2;
          p[gid]   = 1.60e+9;
          //Real sigma_xx =-1.79e+9;
          //shear.XX[gid] = sigma_xx + p[gid];
        }
        e[gid] = mat->getEOS().evaluateInternalEnergy(p[gid],rho[gid]);
        E[gid] = e[gid];
      }
    }
  }
}
