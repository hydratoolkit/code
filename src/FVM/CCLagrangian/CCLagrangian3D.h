//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLagrangian3D.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:40:02 2011
//! \brief   3-D cell-centered FVM Lagrangian hydro
//******************************************************************************

#ifndef CCLagrangian3D_h
#define CCLagrangian3D_h

#include <Control.h>
#include <fileIO.h>
#include <UnsMesh.h>
#include <CCclaw.h>
#include <CCLagrangian.h>
#include <CCLagrangianVar.h>

namespace Hydra {

//! Cell-centered Lagrangian class
class CCLagrangian3D : public CCLagrangian {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLagrangian3D();
    virtual ~CCLagrangian3D();
    //@}

    //! \name Virtual Physics Functions
    //! These functions are virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    virtual void setup();

    virtual void initialize();

    virtual void registerData();
    //@}

    //! Write the screen report
    //!   \param[in] ofs  Reference to output stream
    virtual void writeReport(ostream& ofs);

    //! Write the header for the screen report
    virtual void writeReportHeader(ostream& ofs);

    //###### DEBUG
    virtual void scatterAndOneDPlots();
    //###### DEBUG

    //! Update momentum and energy
    virtual void updateState();

    // The ghost varaibles on the external edges
    // are initializedm BCs are applied and
    // communication is performed if needed.
    virtual void setGhostVarsAndApplyBCs();

     //! This function updates the old time step values (not done every RK step).
    //! This is required for high-order RK based time stepping schemes.
    virtual void updateOldStateVars();

    //! Gradients for the field variables that are required by the
    //! Lagrangian flow solver are computed here
    //@{
    virtual void calcLimitedGradients();

    //! Compute gradients based on mimetic operator
    virtual void calcMimeticGradients();
    //@}

    //@{
    //! Compute Least Squares and/or Limit gradients
    virtual void limitGradients(vector<DataIndex> listVarsLimit,
                                vector<DataIndex> listVarsGrad);

    virtual void vorticityLimiterForVelocityGradients();

    virtual void calcContourIntegralBasedElemVort();

    virtual void adjustGradP();
    //@}

    //! L2 Projection of the element velocity to the nodes
    virtual void projectElemVelocity();

    //! Calculate the centroids for ghost data at boundaries
    virtual void setGhostCentroids();

    //! Update the configuration
    virtual void updateConfiguration();

    //! Calculate global internal and kinetic energy
    virtual void calcGlobalEnergy();

    //! Calculate the stable time step
    virtual Real calcTimeStep(Real dtmin);

  protected:

    //! Update the face geometry based on the new configuration
    void updateFaceGeometryData();

    //! Update stress state
    void updateMaterialState();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLagrangian3D(const CCLagrangian3D&);
    CCLagrangian3D& operator=(const CCLagrangian3D&);
    //@}

    //subcellGeometry* m_faceGeometry;

  };

}
#endif // CCLagrangian3D_h
