//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLUserTemperatureIC.h
//! \author  Mark A. Christon
//! \date    Thu Apr 03 11:50:35 2014
//! \brief   User-defined Temperature IC's
//******************************************************************************
#ifndef CCLUserTemperatureIC_h
#define CCLUserTemperatureIC_h

#include <Material.h>
#include <UnsMesh.h>

namespace Hydra {

//! User-defined temperature IC
class CCLUserTemperatureIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLUserTemperatureIC();
    virtual ~CCLUserTemperatureIC();
    //@}

    //! Virtual function to setup IC's using a mesh and set of DataIndices
    void setICs(UnsMesh* mesh, const DataIndex TEMPERATURE);

    //! Set user-defined temperature initial conditions
    void setUserTemperature();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLUserTemperatureIC(const CCLUserTemperatureIC&);
    CCLUserTemperatureIC& operator=(const CCLUserTemperatureIC&);
    //@}

    //! \name Data presented for the user-defined temperature function
    //@{
    int m_elemId;      //!< Element Id for user access
    int m_matId;       //!< Material Id for user access
    Vector m_coord;    //!< Element centroid coordinates
    Real   m_T;        //!< Element temperature
    Material* m_mat;   //!< Material class for user access
    //@}
};

}

#endif // CCLUserTemperatureIC_h
