//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLagrangian2D.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:40:02 2011
//! \brief   2-D cell-centered FVM Lagrangian hydro
//******************************************************************************

#ifndef CCLagrangian2D_h
#define CCLagrangian2D_h

#include <Control.h>
#include <fileIO.h>
#include <UnsMesh.h>
#include <CCclaw.h>
#include <CCLagrangian.h>
#include <CCLagrangianVar.h>

namespace Hydra {

//! 2-D cell-centered Lagrangian hydro
class CCLagrangian2D : public CCLagrangian {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLagrangian2D();
    virtual ~CCLagrangian2D();
    //@}

    //! \name Virtual Physics Functions
    //! These functions are virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    virtual void setup();

    virtual void registerData();

    virtual void initialize();
    //@}

    //! Write the screen report
    //!   \param[in] ofs  Reference to output stream
    virtual void writeReport(ostream& ofs);

    //! Write the header for the screen report
    //!   \param[in] ofs  Reference to output stream
    virtual void writeReportHeader(ostream& ofs);

// ###### DEBUG
    virtual void scatterAndOneDPlots();
//###### DEBUG

    //! Update momentum and energy
    virtual void updateState();

    // The ghost varaibles on the external edges
    // are initializedm BCs are applied and
    // communication is performed if needed.
    virtual void setGhostVarsAndApplyBCs();

     //! This function updates the old time step values (not done every RK step).
    //! This is required for high-order RK based time stepping schemes.
    virtual void updateOldStateVars();

    //! Gradients for the field variables that are required by the
    //! Lagrangian flow solver are computed here
    //@{
    virtual void calcLimitedGradients();

    //! Compute gradients based on mimetic operator
    virtual void calcMimeticGradients();
    //@}

    //! Compute Least Squares and/or Limit gradients
    //@{
    virtual void limitGradients(vector<DataIndex> listVarsLimit,
                                vector<DataIndex> listVarsGrad);

    virtual void vorticityLimiterForVelocityGradients();

    virtual void calcContourIntegralBasedElemVort();

    virtual void adjustGradP();
    //@}

    //! L2 Projection of the element velocity to the nodes
    virtual void projectElemVelocity();

    //! Calculate the centroids for ghost data at boundaries
    virtual void setGhostCentroids();

    //! Update the configuration
    virtual void updateConfiguration();

    //! Calculate the stable time step
    virtual Real calcTimeStep(Real dtmin);

    //! Calculate global internal and kinetic energy
    virtual void calcGlobalEnergy();

    // Swap centroid coordinates for overlapping sub-domains
    void swapGhostCentroids();

  protected:

    //! Update stress state
    void updateMaterialState();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLagrangian2D(const CCLagrangian2D&);
    CCLagrangian2D& operator=(const CCLagrangian2D&);
    //@}
};

}
#endif // CCLagrangian2D_h
