//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLPMNodeSolver3D.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   3-D node solver using Maire's algorithm
//******************************************************************************

#ifndef CCLPMNodeSolver3D_h
#define CCLPMNodeSolver3D_h

#include <HydraTypes.h>
#include <CCLNodeSolver.h>

namespace Hydra {

class CCLPMNodeSolver3D : public CCLNodeSolver {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLPMNodeSolver3D(UnsMesh& mesh,
                               Control& control,
                               CCLagrangianVar& di,
                               CCLagrangian2DVars& di2D,
                               CCLagrangian3DVars& di3D,
                               vector<CCLZeroVelBC*>& velbc,
                               vector<CCLVelocityBC*>& velocitybc,
                               vector<CCLPressureBC*>& pressurebc);
    virtual ~CCLPMNodeSolver3D() {}
    //@}

    //! Setup the nodal projection algorithm
    virtual void setup();

    //! Setup the nodal projection algorithm
    virtual void projectVelocity(Real time);

    //! L and Vsys matrices are constructed for materials with strength
    virtual void constructMatricesWithStrength();

    //! L and Vsys matrices are constructed for materials without strength
    virtual void constructMatricesWithoutStrength();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLPMNodeSolver3D(const CCLPMNodeSolver3D&);
    CCLPMNodeSolver3D& operator=(const CCLPMNodeSolver3D&);
    //@}

    //! Compute maximal node impedance
    void computeMaxNodeImpedance();

    //! Solve for node velocities
    void solveForNodeVelocities();

    //**************************************************************************
    //! \name DataIndex Declarations
    //@{
    DataIndex VSYS3D;  //!< Linear system for velocity projection
    //@}

};

}
#endif // CCLPMNodeSolver3D_h
