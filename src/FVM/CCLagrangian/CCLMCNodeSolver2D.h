//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLMCNodeSolver2D.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   MAC's 2-D node solver using edge viscosity
//******************************************************************************

#ifndef CCLMCNodeSolver2D_h
#define CCLMCNodeSolver2D_h

#include <HydraTypes.h>
#include <CCLNodeSolver.h>

namespace Hydra {

class CCLMCNodeSolver2D : public CCLNodeSolver {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLMCNodeSolver2D(UnsMesh& mesh,
                               Control& control,
                               CCLagrangianVar& di,
                               CCLagrangian2DVars& di2D,
                               CCLagrangian3DVars& di3D,
                               vector<CCLZeroVelBC*>& velbc,
                               vector<CCLVelocityBC*>& velocitybc,
                               vector<CCLPressureBC*>& pressurebc);
    virtual ~CCLMCNodeSolver2D() {}
    //@}

    //! Setup the nodal projection algorithm
    virtual void setup();

    //! Setup the nodal projection algorithm
    virtual void projectVelocity(Real time);

    virtual void constructMatricesWithStrength() {}

    virtual void constructMatricesWithoutStrength() {}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLMCNodeSolver2D(const CCLMCNodeSolver2D&);
    CCLMCNodeSolver2D& operator=(const CCLMCNodeSolver2D&);
    //@}

    //**************************************************************************
    //! \name DataIndex Declarations
    //@{
    DataIndex VSYS2D;  //!< Linear system for velocity projection
    //@}

};

}
#endif // CCLMCNodeSolver2D_h
