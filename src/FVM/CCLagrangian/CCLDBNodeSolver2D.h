//******************************************************************************
//! \file    src/FVM/CCLagrangian/CCLDBNodeSolver2D.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   2-D node solver using Don Burton's algorithm
//******************************************************************************

#ifndef CCLDBNodeSolver2D_h
#define CCLDBNodeSolver2D_h

#include <HydraTypes.h>
#include <CCLNodeSolver.h>

namespace Hydra {

class CCLDBNodeSolver2D : public CCLNodeSolver {

  public:
    //! \name Constructor/Destructor
    //@{
             CCLDBNodeSolver2D(UnsMesh& mesh,
                               Control& control,
                               CCLagrangianVar& di,
                               CCLagrangian2DVars& di2D,
                               CCLagrangian3DVars& di3D,
                               vector<CCLZeroVelBC*>& velbc,
                               vector<CCLVelocityBC*>& velocitybc,
                               vector<CCLPressureBC*>& pressurebc);
    virtual ~CCLDBNodeSolver2D() {}
    //@}

    //! Setup the nodal projection algorithm
    virtual void setup();

    //! Setup the nodal projection algorithm
    virtual void projectVelocity(Real time);

    //! L and b matrices are constructed for materials with strength
    virtual void constructMatricesWithStrength();

    //! L and b matrices are constructed for materials without strength
    virtual void constructMatricesWithoutStrength();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLDBNodeSolver2D(const CCLDBNodeSolver2D&);
    CCLDBNodeSolver2D& operator=(const CCLDBNodeSolver2D&);
    //@}

    //! Compute maximal node impedance
    void computeMaxNodeImpedance();

    //! Solve for node velocities
    void solveForNodeVelocities();

    /**************************************************************************/
    /* DataIndex Declarations                                                 */
    //@{
    DataIndex VSYS2D;  //!< Linear system for velocity projection
    //@}

};

}
#endif // CCLDBNodeSolver2D_h
