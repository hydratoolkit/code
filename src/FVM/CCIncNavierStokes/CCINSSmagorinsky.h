//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSSmagorinsky.h
//! \author  Jozsef Bakosi, Mark A. Christon
//! \date    Thu Dec 22 8:43:22 2011
//! \brief   The Smagorinsky sub-grid turbulence model for incompressible flow
//******************************************************************************
#ifndef CCINSSmagorinsky_h
#define CCINSSmagorinsky_h

#include <CCINSTurbulence.h>

namespace Hydra {

//! Implements the Smagorinksy model for in large eddy simulation
class CCINSSmagorinsky : public CCINSTurbulence {

  public:

    //! \name Constructor/Destructor
    //@{
            CCINSSmagorinsky(UnsMesh& mesh,
                             Control& control,
                             DualEdgeGradOp& edgeGradOp,
                             CCINSErrors& errors,
                             fileIO& io,
                             CCINSTransportVar& di,
                             LASolver& transport_solver,
                             LAMatrix& transport_A,
                             LAVector& transport_diagA,
                             LAVector& transport_b,
                             LAVector& transport_x,
                             CCINSAdapter& adapter);
    virtual ~CCINSSmagorinsky();
    //@}

    //! Calculate the turbulent viscosity for the Smagorinsky model
    virtual void calcTurbulentViscosity();

    //! Virtual form transport Rhs function
    //!   \param[in] CCINSIncParm Time increment parameters
    //!   \param[in] SourceMode   Time-weighting/time-level for source terms
    //! No rhs as no transport equation for eddy viscosity in WALE model
    virtual void formRhs(const CCINSIncParm& /*incParm*/,
                         FVMCCINS::SourceMode /*srcmode*/,
                         Real* /*rhs=0*/) {};

    //! Solve generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    //!   \details No-op: Smagorinsky does not solve a transport equation
    virtual void solve(const CCINSIncParm& /*incParm*/) {}

    //! Set the wall viscosity according to the specific model
    virtual void setWallConductivity(int nedges, int* edge_list, DualEdge* edges,
                                     Real* kmol, Real* keff, Real* edgek);

    //! Set the wall viscosity according to the specific model
    virtual void setWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                  Real* mu, Real* mueff, Real* edgemu);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSSmagorinsky(const CCINSSmagorinsky&);
    CCINSSmagorinsky& operator=(const CCINSSmagorinsky&);
    //@}

};

}

#endif // CCINSSmagorinsky_h
