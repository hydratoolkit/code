//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSPVDepBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Pressure-Volume dependent pressure BC
//******************************************************************************
#ifndef CCINSPVDepBC_h
#define CCINSPVDepBC_h

#include <CCSideSetBC.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class LAMatrix;
class UnsMesh;

class CCINSPVDepBC: public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSPVDepBC(const BCPackage& bc, int offset);
    virtual ~CCINSPVDepBC();
    //@}

    //! Apply a nodal-penalty to the diagonal of the linear system
    //!   \param[in] Nproc       number of processors
    //!   \param[in] nodeEqStart first equation number
    //!   \param[in] nodeEqEnd   last equation number
    //!   \param[in] nodeeqmap   Nodal equation ID's in global space
    //!   \param[in] Adiag       Diagonal of the linear system
    void applyNodePenalty(int Nproc, int nodeEqStart, int nodeEqEnd,
                          int* nodeeqmap, Real* Adiag);

    //! Apply a nodal-penalty BC
    //!   \param[in] mesh        Data mesh
    //!   \param[in] Nproc       number of processors
    //!   \param[in] nodeEqStart first equation number
    //!   \param[in] nodeEqEnd   last equation number
    //!   \param[in] nodeeqmap   Nodal equation ID's in global space
    //!   \param[in] A           LAMatrix for penalty
    //!   \param[in] bcvol       Surface volume
    //!   \param[in] rhs         Right-hand-side of linear system
    void applyNodePenaltyBC(UnsMesh& mesh, int Nproc, int nodeEqStart,
                            int nodeEqEnd, int* nodeeqmap, LAMatrix* A,
                            Real* bcvol, Real* rhs, Real scale);

    //! Apply a nodal-penalty for an incremental BC
    //!   \param[in] mesh        Data mesh
    //!   \param[in] Nproc       number of processors
    //!   \param[in] nodeEqStart first equation number
    //!   \param[in] nodeEqEnd   last equation number
    //!   \param[in] nodeeqmap   Nodal equation ID's in global space
    //!   \param[in] A           LAMatrix for penalty
    //!   \param[in] scale       Scale-factor applied to the BC
    //!   \param[in] volflowInc  Incremental volume change
    //!   \param[in] bcvol       Surface volume
    //!   \param[in] rhs         Right-hand-side of linear system
    void applyNodePenaltyIncBC(UnsMesh& mesh, int Nproc, int nodeEqStart,
                               int nodeEqEnd, int* nodeeqmap, LAMatrix* A,
                               Real scale, Real volflowInc, Real* bcvol,
                               Real* rhs);
    //! Insert nodal Dirichlet BCs
    //!   \param[in] mesh   Data mesh
    //!   \param[in] bcvol  Volume related to the surface
    //!   \param[in] var    Nodal variable
    void insertNodalBCs(UnsMesh& mesh, Real* bcvol, Real* var);

    //! Mark the prescribed boundary condition
    //!   \param[in] kthBc  k-th boundary condition, the last one always wins.
    //!   \param[in] nodeBc An array to mark node (output)
    void markNodePenalty(int kthBc, int* nodeBc);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSPVDepBC(const CCINSPVDepBC&);
    CCINSPVDepBC& operator=(const CCINSPVDepBC&);
    //@}

    Real m_penalty;
};

}

#endif // CCINSPVDepBC_h
