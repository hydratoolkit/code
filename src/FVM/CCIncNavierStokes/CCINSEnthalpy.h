//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSEnthalpy.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Solve the enthalpy-transport equation
//******************************************************************************
#ifndef CCINSEnthalpy_h
#define CCINSEnthalpy_h

#include <CCINSEnergy.h>
#include <CCINSTemperatureBC.h>

namespace Hydra {

//! Enthalpy transport class
class CCINSEnthalpy : public CCINSEnergy {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSEnthalpy(UnsMesh& mesh,
                           Control& control,
                           DualEdgeGradOp& edgeGradOp,
                           CCINSErrors& errors,
                           fileIO& io,
                           CCINSTransportVar& di,
                           vector<CCINSTemperatureBC*>& tempbc,
                           vector<CCINSHeatFluxBC*>& heatfluxbc,
                           vector<CCINSHeatSource*>& heat_source,
                           vector<CCINSSurfChem*>& surfchem,
                           LASolver& transport_solver,
                           LAMatrix& transport_A,
                           LAVector& transport_b,
                           LAVector& transport_x,
                           CCINSTurbulence& turbulence,
                           CCINSAdapter& adapter);
    virtual ~CCINSEnthalpy();
    //@}

    //! Assemble generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void assembleSystem(const CCINSIncParm& incParm);

    //! Virtual form transport Rhs function
    //!   \param[in] incParm Reference to time increment parameters
    //!   \param[in,out] rhs Pointer to the rhs vector
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs);

    virtual void formRhsInc(const CCINSIncParm& incParm, Real* rhs=0);

    //! Solve generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);

    //! Convert temperature to an energy variable, e.g., enthalpy
    //!   \param[in]  T temperature
    //!   \param[out] h enthalpy
    virtual void convertTempVar(Real* T, Real* h);

    //! Convert energy variable to temperature
    //!   \param[in]  h  enthalpy
    //!   \param[out] T  temperature
    virtual void convertEnergyVar(Real* h, Real* T);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSEnthalpy(const CCINSEnthalpy&);
    CCINSEnthalpy& operator=(const CCINSEnthalpy&);
    //@}

};

}

#endif // CCINSEnthalpy_h
