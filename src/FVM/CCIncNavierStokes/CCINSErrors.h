//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSErrors.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Error handler for incompressible Navier-Stokes
//******************************************************************************
#ifndef CCINSErrors_h
#define CCINSErrors_h

#include <CCINSErrorTypes.h>
#include <CCINSMeshDeformErrors.h>
#include <CCINSSolverErrors.h>
#include <CCINSTurbErrors.h>
#include <DataContainer.h>

namespace Hydra {

// Forward Declaration
class UnsMesh;
class fileIO;


//! Error handler for incompressible Navier-Stokes
class CCINSErrors {

  public:

    //! \name Constructor/Destructor
    //@{
              CCINSErrors(UnsMesh& mesh, fileIO& io);
    virtual  ~CCINSErrors();
    //@}

    //! Setup temporary work arrays
    void setTmpWorkArrays(DataIndex gArray, DataIndex gArraySum);

    //! Accumulate mesh deformation errors
    void addError(CCINSMeshDeformError e);

    //! Accumulate multiple mesh deformation errors
    void addError(CCINSMeshDeformError e, int numerr);

    //! Accumulate turbulence model errors
    void addError(CCINSTurbError e);

    //! Accumulate multiple turbulence model errors
    void addError(CCINSTurbError e, int numerr);

    //! Accumulate flow solver errors
    void addError(CCINSSolverError e);

    //! Accumulate multiple flow solver errors
    void addError(CCINSSolverError e, int numerr);

    //! Check error status
    bool errorStatus();

 private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSErrors(const CCINSErrors&);
    CCINSErrors& operator=(const CCINSErrors&);
    //@}

    fileIO& m_io;
    UnsMesh& m_mesh;

    //! Error accumulators
    CCINSMeshDeformErrors m_deformMeshError;
    CCINSSolverErrors m_uccinsError;
    CCINSTurbErrors m_turbError;

    //! Offsets for error categories
    int m_deformMeshOffset;
    int m_turbOffset;
    int m_uccinsOffset;
    int m_totalErrors;

    //! working storage
    DataIndex m_workArray;
    DataIndex m_workArraySum;
};

}

#endif // CCINSErrors_h
