//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSIncParm.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Struct that holds the time incrementation parameters for CCINSFlow
//******************************************************************************
#ifndef CCINSIncParm_h
#define CCINSIncParm_h

#include <HydraTypes.h>

namespace Hydra {

struct CCINSIncParm {
  //! \name Time incrementation parameters for CINSFlow
  //@{
  Real dtm1;       //!< time increment (from previous time step)
  Real dt;         //!< time increment
  Real time;       //!< time at level n
  Real time_np1;   //!< time at level n+1
  int  Ninc;       //!< time step id
  Real thetaKn;    //!< 1 - thetaK
  Real thetaKnp1;  //!<     thetaK
  Real thetaAn;    //!< 1 - thetaA
  Real thetaAnp1;  //!<     thetaA
  Real thetaFn;    //!< 1 - thetaF
  Real thetaFnp1;  //!<     thetaF
  Real thetaPn;    //!< 1 - thetaP
  Real thetaPnp1;  //!<     thetaP
  Real CFL0;       //!< CFL number at t=0 for CFL-controlled time-incrementation
  Real dtmax;      //!< Maximum allowable time-increment size
  //@}
};

}

#endif // CCINSIncParm_h
