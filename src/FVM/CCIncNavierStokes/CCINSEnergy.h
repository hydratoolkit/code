//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSEnergy.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Base energy transport class
//******************************************************************************
#ifndef CCINSEnergy_h
#define CCINSEnergy_h

#include <CCINSTransport.h>

namespace Hydra {

class Category;
class CCINSHeatFluxBC;
class CCINSHeatSource;
class CCINSTemperatureBC;
class CCINSSurfChem;
class CCINSTurbulence;

//! Base energy transport class
class CCINSEnergy : public CCINSTransport {

  public:

    //! \name Constructor/Destructor
    //@{
            CCINSEnergy(UnsMesh& mesh,
                        Control& control,
                        DualEdgeGradOp& edgeGradOp,
                        CCINSErrors& errors,
                        fileIO& io,
                        CCINSTransportVar& di,
                        vector<CCINSTemperatureBC*>& temperaturebc,
                        vector<CCINSHeatFluxBC*>& heatfluxbc,
                        vector<CCINSHeatSource*>& heat_source,
                        vector<CCINSSurfChem*>& surfchem,
                        LASolver& transport_solver,
                        LAMatrix& transport_A,
                        LAVector& transport_b,
                        LAVector& transport_x,
                        CCINSTurbulence& turbulence,
                        CCINSAdapter& adapter);
    virtual ~CCINSEnergy();
    //@}

    //**************************************************************************
    //! \name Virtual interface for energy transport
    //@{
    //! Assemble generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void assembleSystem(const CCINSIncParm& incParm) = 0;

    //! Form the RHS for the transport equation
    //!   \param[in]  CCINSIncParm Time increment parameters
    //!   \param[in]  SourceMode   Time-weighting/time-level for source terms
    //!   \param[out] rhs          Right-hand-side for transport eq.
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs=0)=0;

    //! Solve generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm) = 0;

    //! Convert temperature to an energy variable, e.g., enthalpy
    //!   \param[in]  temperature  temperature
    //!   \param[out] energyvar    energy variable
    virtual void convertTempVar(Real* T, Real* energyvar) = 0;

    //! Convert energy variable to temperature
    //!   \param[in]  energyvar    energy variable
    //!   \param[out] temperature  temperature
    virtual void convertEnergyVar(Real* energyvar, Real* T) = 0;
    //@}

    //**************************************************************************
    //! \name Heat sources and fluxes
    //@{
    //! Apply prescribed surface heat flux BCs
    void applyHeatFlux(const Real theta, const Real time,
                       const Real dt, Real* rhs);

    //! Apply prescribed surface heat flux BCs
    void applyHeatFlux(Real* rhs,
                const FVMCCINS::SourceMode mode=FVMCCINS::TIME_LEVEL_WEIGHTED);

    //! Apply prescribed surface heat flux BCs
    void applyHeatSources(Real* rhs,
                 const FVMCCINS::SourceMode mode=FVMCCINS::TIME_LEVEL_WEIGHTED);

    //! Apply prescribed surface temperature BCs
    //!   \param[in] t        Time
    //!   \param[in] var      Pointer to the scalar field
    //!   \param[in] mode     Flag, indicating where it is called from
    //!   \param[in] exchange Flag, to activate some parallelization calls
    void applyTemperatureBCs(const Real t,
                             Real* var,
                             Real* varg,
                             bool* bcflag,
                             FVMCCINS::EnergyConversionMode mode,
                             const bool exchange=true);
    //@}

    //**************************************************************************
    //! \name Evaluate thermal properties
    //@{
    //! Calculate the cell and edge thermal conductivity
    void calcEdgeConductivity(Real* kmol, Real*Cp, Real* keff, Real* edgek);

    //! Update the thermal conductivity and specific heats
    void updateThermalProp(Real* k, Real* Cp, Real* Cv=0);
    //}

  protected:

    //! Calculate edge conductivity comming from wall-function
    void calcEdgeConductivityWallFunctionKEps(Real* Cp, Real* Cv,
                                              Real* kmol, Real* edgek);

    //! Calculate edge viscosity comming from wall-function
    void calcElemConductivityWallFunctionKEps(Real* Cp, Real* kmol,
                                              Real* flag, Real* elemk);

    //! Calculate the y+ for linear and logaritmic intersection
    void calcYPlusLinLogIntersec(Real Pr, Real PrT, Real kappai,
                                 Real E, Real P, int iter, Real& ypE11);

    //! Linear Algebra
    LASolver& m_transport_solver;
    LAMatrix& m_transport_A;
    LAVector& m_transport_b;
    LAVector& m_transport_x;

    //! Heat flux and source
    vector<CCINSHeatFluxBC*>& m_heatfluxbc;
    vector<CCINSHeatSource*>& m_heat_source;
    vector<CCINSSurfChem*>&   m_surfchem;

    // Temperature BCs
    vector<CCINSTemperatureBC*>& m_temperatureBCs;

    //! Turbulence model
    CCINSTurbulence& m_turbulence;

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSEnergy(const CCINSEnergy&);
    CCINSEnergy& operator=(const CCINSEnergy&);
    //@}

};

}

#endif // CCINSEnergy_h
