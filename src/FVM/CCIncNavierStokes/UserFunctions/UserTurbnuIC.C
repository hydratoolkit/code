//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/UserFunctions/UserTurbnuIC.C
//! \author  Mark A. Christon
//! \date    Tue Sep  5 15:44:10 MDT 2017
//! \brief   User-defined Spalart-Allmaras initial condition
//******************************************************************************
#include <cmath>

using namespace std;

#include <CCINSUserTurbnuIC.h>

using namespace Hydra;

void
CCINSUserTurbnuIC::setUserTurbnu()
/*******************************************************************************
Routine: setUserTurbnu - set user-defined Spalart-Allamaras initial conditions
Author : Mark A. Christon
*******************************************************************************/
{
  // The data that is available for the user-defined Spalart-Allmaras initial
  // conditions follows:
  //
  // Variable   Type    Meaning
  // ======== ========= ========================================================
  // m_elemId      int  Element (global) Id attached to the dual-edge for the BC
  // m_matId       int  Material Id -- used to identify materials
  // m_coord    Vector  Centroid coordinates (m_coord.X, m_coord.Y, m_coord.Z)
  // m_turbnu     Real  Centroid Spalart-Allmaras variable

  // Define parameters and variables based on your needs.
  //
  // PI is defined in Base/HydraTypes.h

  m_turbnu =  0.5;
}
