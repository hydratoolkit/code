//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/UserFunctions/UserTurbEpsBC.C
//! \author  Mark A. Christon
//! \date    Fri Sep  1 10:45:30 MDT 2017
//! \brief   User defined turbulent dissipation rate condition based on sidesets
//******************************************************************************
#include <cmath>

using namespace std;

#include <CCINSTurbEpsBC.h>

using namespace Hydra;

Real
CCINSTurbEpsBC::setUserValue()
/*******************************************************************************
Routine: setUserValue - set the user-defined turbulent dissipation rate
Author : Mark A. Christon
*******************************************************************************/
{
  // The data that is available for the user-defined dissipation rate boundary
  // condition follows:
  //
  // Variable   Type    Meaning
  // ======== ========= ========================================================
  // m_elemId      int  Element (global) Id attached to the dual-edge for the BC
  // m_edgeId      int  Dual-Edge Id for the BC
  // m_time       Real  Time for the BC evaluation
  // m_coord    Vector  Dual-edge coordinates (m_coord.X, m_coord.Y, m_coord.Z)
  // m_mat    Material* Pointer to the material model for property evaluation
  //
  // Evaluating the viscosity may be coded as
  // Real mu = m_mat->getViscosity().evaluate(m_elemId);
  //
  // Evaulating the thermal conductivity may be coded as
  // Real k = m_mat->getConductivity().evaluate(m_elemId);
  //
  // Define parameters and variables based on your needs.
  //
  // PI is defined in Base/HydraTypes.h

  // Evaluate the BC
  Real bcval = 0.0;

  return bcval;
}
