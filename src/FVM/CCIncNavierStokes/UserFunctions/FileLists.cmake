# ############################################################################ #
#
# Library: CCINSUserFunctions
# File Definition File
#
# ############################################################################ #

# Source Files

set(CCINSUserFunctions_SOURCE_FILES
  UserDensityBC.C
  UserDisplacementBC.C
  UserHeatFluxBC.C
  UserTemperatureBC.C
  UserTurbEpsBC.C
  UserTurbKEBC.C
  UserTurbNuBC.C
  UserTurbOmegaBC.C
  UserVelocityBC.C
  UserVolFracBC.C
  UserVelocityIC.C
  UserTemperatureIC.C
  UserTurbKEIC.C
  UserTurbEpsIC.C
  UserTurbnuIC.C
  UserOmegaIC.C
  UserHeatSource.C
)


