//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSSymmVelBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Symmetry velocity boundary condition based on sidesets
//******************************************************************************
#ifndef CCINSSymmVelBC_h
#define CCINSSymmVelBC_h

#include <CCSideSetBC.h>
#include <DataShapes.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;
class LAMatrix;


//! Symmetrcy velocity boundary condition
class CCINSSymmVelBC : public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSSymmVelBC(const BCPackage& bc, int offset);
    virtual ~CCINSSymmVelBC();
    //@}

    //! Get velocity direction
    GlobalDirection getDirection() const { return m_direction; }

    //! Set the acceleration in the ghost buffer
    //!   \param[in] mesh     DataMesh
    //!   \param[in] gd       Ghost buffer
    void setAcceleration(UnsMesh& mesh, CVector& gd);

    //! Set the acceleration in the ghost buffer
    //!   \param[in] mesh     DataMesh
    //!   \param[in] grho     Density ghost buffer
    //!   \param[in] gwt      Ghost Weight for acceleration
    //!   \param[in] gd       Ghost buffer
    void setAcceleration(UnsMesh& mesh, Real* grho, Real* gwt, CVector& gd);

    //! Setup the edge-data for a given BC
    //!   \param[in] mesh     DataMesh
    //!   \param[in] bcflag   Bool flag to be set for Dirichlet BC's
    void setupEdgeBCs(UnsMesh& mesh, CBoolVector& bcflag);

    //! Set the ghost-data for a given symmetry velocity BC
    //!   \param[in] mesh     DataMesh
    //!   \param[in] gd       Ghost buffer
    //!   \param[in] t        Total simulated time for load-curve lookup
    void setGhostBC(UnsMesh& mesh, CVector& gd, Real t);

    //! Set the ghost-data & flag for a given symmetry velocity BC
    //!   \param[in] mesh     DataMesh
    //!   \param[in] bcflag   Bool flag to be set for Dirichlet BC's
    //!   \param[in] gd       Ghost buffer
    //!   \param[in] t        Total simulated time for load-curve lookup
    void setGhostBC(UnsMesh& mesh, CBoolVector& bcflag, CVector& gd, Real t);

    //! Setup the primary symmetry direction and no-slip boolean
    //!  \param[in] mesh     DataMesh
    void setup(UnsMesh& mesh);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSSymmVelBC(const CCINSSymmVelBC&);
    CCINSSymmVelBC& operator=(const CCINSSymmVelBC&);
    //@}

    Real m_tol;

    GlobalDirection m_direction;  //!< Global direction for the symmetry BC

    DataIndex NO_SLIP_LIST; //!< Bool array indicating where no-slip is active
};

}

#endif // CCINSSymmVelBC_h
