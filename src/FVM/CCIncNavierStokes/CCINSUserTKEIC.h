//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSUserTKEIC.h
//! \author  Mark A. Christon
//! \date    Tue Sep  5 15:44:10 MDT 2017
//! \brief   User-defined TKE IC's
//******************************************************************************
#ifndef CCINSUserTKEIC_h
#define CCINSUserTKEIC_h

#include <CCINSFlow.h>
#include <UserIC.h>

namespace Hydra {

//! User-defined TKE IC
class CCINSUserTKEIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSUserTKEIC();
    virtual ~CCINSUserTKEIC();
    //@}

    //! Virtual function to setup IC's using a mesh and set of DataIndices
    void setICs(UnsMesh* mesh, const DataIndex TURB_KE);

    //! Set user-defined temperature initial conditions
    void setUserTKE();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSUserTKEIC(const CCINSUserTKEIC&);
    CCINSUserTKEIC& operator=(const CCINSUserTKEIC&);
    //@}

    //! \name Data presented for the user-defined TKE function
    //@{
    int m_elemId;      //!< Element Id for user access
    int m_matId;       //!< Material Id for user access
    Vector m_coord;    //!< Element centroid coordinates
    Real   m_TKE;      //!< Element turbulent kinetic energy
    Material* m_mat;   //!< Material class for user access
    //@}
};

}

#endif // CCINSUserTKEIC_h
