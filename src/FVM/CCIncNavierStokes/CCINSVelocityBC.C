//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSVelocityBC.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:23 2011
//! \brief   Velocity boundary condition based on sidesets
//******************************************************************************
#include <cmath>

using namespace std;

#include <CCINSVelocityBC.h>
#include <UnsMesh.h>
#include <BCPackage.h>

using namespace Hydra;

CCINSVelocityBC::CCINSVelocityBC(const BCPackage& bc, int offset) :
  CCSideSetBC(bc, offset), m_direction(static_cast<GlobalDirection>(bc.m_dir))
/*******************************************************************************
Routine: CCINSVelocityBC - constructor
Author : Mark A. Christon
*******************************************************************************/
{}

CCINSVelocityBC::~CCINSVelocityBC()
/*******************************************************************************
Routine: ~CCINSVelocityBC - destructor
Author : Mark A. Christon
*******************************************************************************/
{
  // Easy way to cleanup all registered data
  freeAllVariables();
}

void
CCINSVelocityBC::formInvDistOp(UnsMesh& mesh, const Real t, Real* nvar,
                               Real* invDistWF)
/*******************************************************************************
Routine: formInvDistOp - Form the inverse distance operator
Author : Mark A. Christon
*******************************************************************************/
{
  Sideset* surf = mesh.getSidesets();
  int nel = surf[m_set].Nel;
  if (nel == 0)
    return;

  int  conn[4];
  int lconn[4];

  int Ndim = mesh.getNdim();

  Real rc2ni;

  int* side_list = mesh.getVariable<int>(surf[m_set].SIDE_LIST);
  int* el_list   = mesh.getVariable<int>(surf[m_set].EL_LIST);

  // Map from global element Id to class
  int* cidmap     = mesh.getElementClassMap();
  DualEdge* edges = mesh.getDualEdges();
  CVector coord   = mesh.getNodeCoord();

  Real lcv = 1.0;
  if (m_tblid > 0) {
    Table* tbl = mesh.getTable(m_tblid);
    lcv = tbl->lookup(t);
  }

  Real bcval = m_amp*lcv;

  switch(m_type) {
  case BC_CONSTANT:
  case BC_TIME_DEPENDENT:
    for (int i = 0; i < nel; i++) {
      int el  =   el_list[i];
      int sd  = side_list[i];
      int cid = cidmap[el];
      Element* ec=mesh.getElementClass(cid);
      int lid   = el - ec->getElementOffset();
      int* eids = ec->getDualEdgeIds();
      int nepe  = ec->getFacesPerElement();
      int joff  = lid*nepe;
      int eid   = eids[joff+sd];
      int nnpf;
      if (Ndim == TWOD) {
        nnpf = ec->getNodesPerEdge(sd);
      } else {
        nnpf = ec->getNodesPerFace(sd);
      }
      ec->getFaceConnectivity(lid, sd, conn, lconn);

      if (Ndim == TWOD) {
        Real xsc = edges[eid].xs[0];
        Real ysc = edges[eid].xs[1];
        for (int j = 0; j < nnpf; j++) {
          int nd = conn[j];
          rc2ni= 1.0/sqrt( (coord.X[nd] - xsc)*(coord.X[nd] - xsc) +
                           (coord.Y[nd] - ysc)*(coord.Y[nd] - ysc) );
          nvar[nd] += getBCvalue(bcval,el)*rc2ni;
          invDistWF[nd] += rc2ni;
        }
      } else if (Ndim == THREED) {
        Real xsc = edges[eid].xs[0];
        Real ysc = edges[eid].xs[1];
        Real zsc = edges[eid].xs[2];
        for (int j = 0; j < nnpf; j++) {
          int nd = conn[j];
          rc2ni= 1.0/sqrt( (coord.X[nd] - xsc)*(coord.X[nd] - xsc) +
                           (coord.Y[nd] - ysc)*(coord.Y[nd] - ysc) +
                           (coord.Z[nd] - zsc)*(coord.Z[nd] - zsc) );
          nvar[nd] += getBCvalue(bcval,el)*rc2ni;
          invDistWF[nd] += rc2ni;
        }
      }
    }
    break;

  case BC_SPACE_DEPENDENT:
  case BC_SPACETIME_DEPENDENT: {
    for (int i = 0; i < nel; i++) {
      int el  = el_list[i];
      int sd  = side_list[i];
      int cid = cidmap[el];
      Element* ec=mesh.getElementClass(cid);
      int lid = el - ec->getElementOffset();
      int* eids = ec->getDualEdgeIds();
      int Nepe = ec->getFacesPerElement();
      int joff = lid*Nepe;
      int eid  = eids[joff+sd];
      int nnpf;
      if (Ndim == TWOD) {
        nnpf = ec->getNodesPerEdge(sd);
      } else {
        nnpf = ec->getNodesPerFace(sd);
      }
      ec->getFaceConnectivity(lid, sd, conn, lconn);

      if (Ndim == TWOD) {
        Real xsc= edges[eid].xs[0];
        Real ysc= edges[eid].xs[1];
        for (int j = 0; j < nnpf; j++) {
          int nd = conn[j];
          rc2ni= 1.0/sqrt( (coord.X[nd] - xsc)*(coord.X[nd] - xsc) +
                           (coord.Y[nd] - ysc)*(coord.Y[nd] - ysc) );
          nvar[nd] += getBCvalue(bcval,el)*rc2ni;
          invDistWF[nd] += rc2ni;
        }
      } else if (Ndim == THREED) {
        Real xsc= edges[eid].xs[0];
        Real ysc= edges[eid].xs[1];
        Real zsc= edges[eid].xs[2];
        for (int j = 0; j < nnpf; j++) {
          int nd = conn[j];
          rc2ni= 1.0/sqrt( (coord.X[nd] - xsc)*(coord.X[nd] - xsc) +
                           (coord.Y[nd] - ysc)*(coord.Y[nd] - ysc) +
                           (coord.Z[nd] - zsc)*(coord.Z[nd] - zsc) );
          nvar[nd] += getBCvalue(bcval,el)*rc2ni;
          invDistWF[nd] += rc2ni;
        }
      }
    }
    break;
  }

  case BC_USER_DEFINED: {
    int* edge_list = mesh.getVariable<int>(surf[m_set].DUALEDGE_LIST);
    for (int i = 0; i < nel; i++) {
      Element* ec = mesh.getElementClass(cidmap[el_list[i]]);
      // Data presented to setUserVelocity function
      m_elemId = el_list[i];
      m_edgeId = edge_list[i];
      m_time = t;
      m_mat = mesh.getMaterial(ec->getMatId());
      m_coord.X = edges[m_edgeId].xs[0];
      m_coord.Y = edges[m_edgeId].xs[1];
      m_coord.Z = edges[m_edgeId].xs[2];

      // Evaluate the boundary condition
      bcval = setUserValue();

      int sd   = side_list[i];
      int lid  = m_elemId - ec->getElementOffset();
      int nnpf = ec->getNodesPerFace(sd);
      ec->getFaceConnectivity(lid, sd, conn, lconn);

      for (int j = 0; j < nnpf; j++) {
        int nd = conn[j];
        rc2ni= 1.0/sqrt( (coord.X[nd] - m_coord.X)*(coord.X[nd] - m_coord.X) +
                         (coord.Y[nd] - m_coord.Y)*(coord.Y[nd] - m_coord.Y) +
                         (coord.Z[nd] - m_coord.Z)*(coord.Z[nd] - m_coord.Z) );
        nvar[nd] += bcval*rc2ni;
        invDistWF[nd] += rc2ni;
      }
    }
  }
    break;

  } // End of switch
}

void
CCINSVelocityBC::setAcceleration(UnsMesh& mesh, Real* gd)
/*******************************************************************************
Routine: setAcceleration - set ghost acceleration data for Velocity BC's
Author : Mark A. Christon
*******************************************************************************/
{
  // Haul out the sideset data
  Sideset* surf = mesh.getSidesets();
  int Nel  = surf[m_set].Nel;

  if (Nel == 0) return;

  // Set the time-derivative of the load-curve, i.e., acceleration
  Real lcv = 0.0;
  if (m_tblid > 0) {
    Table* tbl = mesh.getTable(m_tblid);
    lcv = tbl->calcDeriv(0.0);
  }
  Real bcval = m_amp*lcv;

  int* edge_list = mesh.getVariable<int>(surf[m_set].DUALEDGE_LIST);

  switch(m_type) {
  case BC_CONSTANT:
  case BC_TIME_DEPENDENT:
    for (int i=0; i<Nel; i++) {
      gd[edge_list[i]] = bcval;
    }
    break;

  case BC_SPACE_DEPENDENT:
  case BC_SPACETIME_DEPENDENT: {
    assert ("No space or spacetime acceleration in this class!!!" == 0);
    break;
  }

  case BC_USER_DEFINED: {
    int* el_list   = mesh.getVariable<int>(surf[m_set].EL_LIST);
    int* cidmap    = mesh.getElementClassMap();
    DualEdge* edges = mesh.getDualEdges();
    for (int i=0; i<Nel; i++) {
      Element* ec = mesh.getElementClass(cidmap[el_list[i]]);
      // Data presented to setUserAcceleration function
      m_elemId = el_list[i];
      m_edgeId = edge_list[i];
      m_time = 0.0;
      m_mat = mesh.getMaterial(ec->getMatId());
      m_coord.X = edges[m_edgeId].xs[0];
      m_coord.Y = edges[m_edgeId].xs[1];
      m_coord.Z = edges[m_edgeId].xs[2];

      gd[edge_list[i]] = setUserAcceleration();
    }
  }
    break;

  } // End of switch
}

void
CCINSVelocityBC::setAcceleration(UnsMesh& mesh, Real* grho, Real* gwt,
                                 Real* gd)
/*******************************************************************************
Routine: setAcceleration - set ghost acceleration data for Velocity BC's
Author : Mark A. Christon

This function sets a time-rate of change of momentum, possibly weighted
with a volume-fraction or other weight.
*******************************************************************************/
{
  // Haul out the sideset data
  Sideset* surf = mesh.getSidesets();
  int Nel  = surf[m_set].Nel;

  if (Nel == 0) return;

  // Set the time-derivative of the load-curve, i.e., acceleration
  Real lcv = 0.0;
  if (m_tblid > 0) {
    Table* tbl = mesh.getTable(m_tblid);
    lcv = tbl->calcDeriv(0.0);
  }
  Real bcval = m_amp*lcv;

  int* edge_list = mesh.getVariable<int>(surf[m_set].DUALEDGE_LIST);

  switch(m_type) {
  case BC_CONSTANT:
  case BC_TIME_DEPENDENT:
    for (int i=0; i<Nel; i++) {
      int eid = edge_list[i];
      gd[eid] = bcval*grho[eid]*gwt[eid];
    }
    break;

  case BC_SPACE_DEPENDENT:
  case BC_SPACETIME_DEPENDENT: {
    assert ("No space or spacetime acceleration in this class!!!" == 0);
    break;
  }

  case BC_USER_DEFINED: {
    int* el_list   = mesh.getVariable<int>(surf[m_set].EL_LIST);
    int* cidmap    = mesh.getElementClassMap();
    DualEdge* edges = mesh.getDualEdges();
    for (int i=0; i<Nel; i++) {
      Element* ec = mesh.getElementClass(cidmap[el_list[i]]);
      // Data presented to setUserAcceleration function
      m_elemId = el_list[i];
      m_edgeId = edge_list[i];
      m_time = 0.0;
      m_mat = mesh.getMaterial(ec->getMatId());
      m_coord.X = edges[m_edgeId].xs[0];
      m_coord.Y = edges[m_edgeId].xs[1];
      m_coord.Z = edges[m_edgeId].xs[2];

      gd[edge_list[i]] = setUserAcceleration();
    }
  }
    break;

  } // End of switch
}

void
CCINSVelocityBC::setEdgeBCs(UnsMesh& mesh, Real* gd, const Real t)
/*******************************************************************************
Routine: setEdgeBCs - Set edge data for Dirichlet condition
Author : Mark A. Christon
*******************************************************************************/
{
  // Haul out the sideset data
  Sideset* surf = mesh.getSidesets();
  int Nel  = surf[m_set].Nel;

  if (Nel == 0) return;

  // Handle the load-curve first for both CONSTANT and TIME_DEPENDENT
  Real lcv = 1.0;
  if (m_tblid > 0) {
    Table* tbl = mesh.getTable(m_tblid);
    lcv = tbl->lookup(t);
  }
  Real bcval = m_amp*lcv;

  int* el_list   = mesh.getVariable<int>(surf[m_set].EL_LIST);
  int* edge_list = mesh.getVariable<int>(surf[m_set].DUALEDGE_LIST);

  switch(m_type) {
  case BC_CONSTANT:
  case BC_TIME_DEPENDENT:
    for (int i=0; i<Nel; i++) {
      gd[edge_list[i]] = getBCvalue(bcval,el_list[i]);
    }
    break;

  case BC_SPACE_DEPENDENT:
  case BC_SPACETIME_DEPENDENT: {
    Field f = mesh.getField(m_fieldid);
    Real* fieldvar = mesh.getVariable<Real>(f.FIELD);
    for (int i=0; i<Nel; i++) {
      gd[edge_list[i]] = getBCvalue(bcval,el_list[i])*fieldvar[i+m_offset];
    }
    break;
  }

  case BC_USER_DEFINED: {
    int* cidmap = mesh.getElementClassMap();
    DualEdge* edges = mesh.getDualEdges();
    for (int i=0; i<Nel; i++) {
      Element* ec = mesh.getElementClass(cidmap[el_list[i]]);
      // Data presented to setUserValue function
      m_elemId = el_list[i];
      m_edgeId = edge_list[i];
      m_time = t;
      m_mat = mesh.getMaterial(ec->getMatId());
      m_coord.X = edges[m_edgeId].xs[0];
      m_coord.Y = edges[m_edgeId].xs[1];
      m_coord.Z = edges[m_edgeId].xs[2];
      gd[edge_list[i]] = setUserValue();
    }
  }
    break;

  } // End of switch
}

void
CCINSVelocityBC::setEdgeBCs(UnsMesh& mesh, bool* bcflag, Real* gd, const Real t)
/*******************************************************************************
Routine: setEdgeBCs - Set edge data for Dirichlet condition
Author : Mark A. Christon
*******************************************************************************/
{
  // Haul out the sideset data
  Sideset* surf = mesh.getSidesets();
  int Nel  = surf[m_set].Nel;

  if (Nel == 0) return;

  // Handle the load-curve first for both CONSTANT and TIME_DEPENDENT
  Real lcv = 1.0;
  if (m_tblid > 0) {
    Table* tbl = mesh.getTable(m_tblid);
    lcv = tbl->lookup(t);
  }
  Real bcval = m_amp*lcv;

  int* el_list   = mesh.getVariable<int>(surf[m_set].EL_LIST);
  int* edge_list = mesh.getVariable<int>(surf[m_set].DUALEDGE_LIST);

  switch(m_type) {
  case BC_CONSTANT:
  case BC_TIME_DEPENDENT:
    for (int i=0; i<Nel; i++) {
      bcflag[edge_list[i]] = true;
      gd[edge_list[i]] = getBCvalue(bcval,el_list[i]);
    }
    break;

  case BC_SPACE_DEPENDENT:
  case BC_SPACETIME_DEPENDENT: {
    Field f = mesh.getField(m_fieldid);
    Real* fieldvar = mesh.getVariable<Real>(f.FIELD);
    for (int i=0; i<Nel; i++) {
      Real fval = getBCvalue(bcval,el_list[i])*fieldvar[i+m_offset];
      bcflag[edge_list[i]] = true;
      gd[edge_list[i]] = fval;
    }
    break;
  }

  case BC_USER_DEFINED: {
    int* cidmap = mesh.getElementClassMap();
    DualEdge* edges = mesh.getDualEdges();
    for (int i=0; i<Nel; i++) {
      Element* ec = mesh.getElementClass(cidmap[el_list[i]]);
      // Data presented to setUserValue function
      m_elemId = el_list[i];
      m_edgeId = edge_list[i];
      m_time = t;
      m_mat = mesh.getMaterial(ec->getMatId());
      m_coord.X = edges[m_edgeId].xs[0];
      m_coord.Y = edges[m_edgeId].xs[1];
      m_coord.Z = edges[m_edgeId].xs[2];
      bcflag[edge_list[i]] = true;
      gd[edge_list[i]] = setUserValue();
    }
  }
    break;

  } // End of switch
}

void
CCINSVelocityBC::setupEdgeBCs(UnsMesh& mesh, bool* bcflag)
/*******************************************************************************
Routine: setupEdgeBCs - setup edge velocity b.c. flag for velocity BC's
Author : Mark A. Christon
*******************************************************************************/
{
  // Haul out the sideset data
  Sideset* surf = mesh.getSidesets();
  int Nel = surf[m_set].Nel;

  if (Nel == 0) return;

  int* edge_list = mesh.getVariable<int>(surf[m_set].DUALEDGE_LIST);

  for (int i=0; i<Nel; i++) {
    bcflag[edge_list[i]] = true;
  }
}

void
CCINSVelocityBC::setGhostBC(UnsMesh& mesh, Real* gd, const Real t)
/*******************************************************************************
Routine: setGhostBC - setup ghost data for Velocity condition
Author : Mark A. Christon
*******************************************************************************/
{
  // Haul out the sideset data
  Sideset* surf = mesh.getSidesets();
  int Nel  = surf[m_set].Nel;

  if (Nel == 0) return;

  // Handle the load-curve first for both CONSTANT and TIME_DEPENDENT
  Real lcv = 1.0;
  if (m_tblid > 0) {
    Table* tbl = mesh.getTable(m_tblid);
    lcv = tbl->lookup(t);
  }
  Real bcval = m_amp*lcv;

  int* edge_list = mesh.getVariable<int>(surf[m_set].DUALEDGE_LIST);

  switch(m_type) {
  case BC_CONSTANT:
  case BC_TIME_DEPENDENT:
    for (int i=0; i<Nel; i++) {
      gd[edge_list[i]] = bcval;
    }
    break;

  case BC_SPACE_DEPENDENT:
  case BC_SPACETIME_DEPENDENT: {
    Field f = mesh.getField(m_fieldid);
    Real* fieldvar = mesh.getVariable<Real>(f.FIELD);
    for (int i=0; i<Nel; i++) {
      gd[edge_list[i]] = bcval*fieldvar[i+m_offset];
    }
    break;
  }

  case BC_USER_DEFINED: {
    int* el_list   = mesh.getVariable<int>(surf[m_set].EL_LIST);
    int* cidmap    = mesh.getElementClassMap();
    DualEdge* edges = mesh.getDualEdges();
    for (int i=0; i<Nel; i++) {
      Element* ec = mesh.getElementClass(cidmap[el_list[i]]);
      // Data presented to setUserVelocity function
      m_elemId = el_list[i];
      m_edgeId = edge_list[i];
      m_time = t;
      m_mat = mesh.getMaterial(ec->getMatId());
      m_coord.X = edges[m_edgeId].xs[0];
      m_coord.Y = edges[m_edgeId].xs[1];
      m_coord.Z = edges[m_edgeId].xs[2];
      gd[m_edgeId] = setUserValue();
    }

  }
    break;

  } // End of switch
}
