//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSUserTemperatureIC.h
//! \author  Yidong (Tim) Xia
//! \date    Thu Apr 03 11:50:35 2014
//! \brief   User-defined Temperature IC's
//******************************************************************************
#ifndef CCINSUserTemperatureIC_h
#define CCINSUserTemperatureIC_h

#include <CCINSFlow.h>
#include <UserIC.h>

namespace Hydra {

//! User-defined temperature IC
class CCINSUserTemperatureIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSUserTemperatureIC();
    virtual ~CCINSUserTemperatureIC();
    //@}

    //! Virtual function to setup IC's using a mesh and set of DataIndices
    void setICs(UnsMesh* mesh, const DataIndex TEMPERATURE);

    //! Set user-defined temperature initial conditions
    void setUserTemperature();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSUserTemperatureIC(const CCINSUserTemperatureIC&);
    CCINSUserTemperatureIC& operator=(const CCINSUserTemperatureIC&);
    //@}

    //! \name Data presented for the user-defined temperature function
    //@{
    int m_elemId;      //!< Element Id for user access
    int m_matId;       //!< Material Id for user access
    Vector m_coord;    //!< Element centroid coordinates
    Real   m_T;        //!< Element temperature
    Material* m_mat;   //!< Material class for user access
    //@}
};

}

#endif // CCINSUserTemperatureIC_h
