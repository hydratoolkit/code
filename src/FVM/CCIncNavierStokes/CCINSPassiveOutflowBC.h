//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSPassiveOutflowBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Passive outflow boundary condition based on surfaces
//******************************************************************************
#ifndef CCINSPassiveOutflowBC_h
#define CCINSPassiveOutflowBC_h

#include <SideSetBC.h>
#include <DataShapes.h>

namespace Hydra {

// Forward declarations
struct BCPackage;


class CCINSPassiveOutflowBC : public SideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSPassiveOutflowBC(const BCPackage& bc, int offset);
    virtual ~CCINSPassiveOutflowBC();
    //@}

    //! Apply the passive outflow BC
    //!   \param[out] rhs      Reference to the rhs vector
    //!   \param[in]  mesh     Reference to mesh object
    //!   \param[in]  thetaAx  Time level weight
    //!   \param[in]  dt       Time step
    void apply(UnsMesh& mesh,
               const Real thetaAx,
               const Real dt,
               const CVector& xc,
               const CVector& gxc,
               const Real* wt,
               const Real* vf,
               const Real* phi,
               const Real* gphi,
               const CVector& dphi,
               const CVector& gdphi,
               Real* rhs);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSPassiveOutflowBC(const CCINSPassiveOutflowBC&);
    CCINSPassiveOutflowBC& operator=(const CCINSPassiveOutflowBC&);
    //@}

};

}

#endif
