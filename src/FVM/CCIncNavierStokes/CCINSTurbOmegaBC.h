//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSTurbOmegaBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Omega BC's
//******************************************************************************
#ifndef CCINSTurbOmegaBC_h
#define CCINSTurbOmegaBC_h

#include <CCSideSetBC.h>
#include <BCPackage.h>

namespace Hydra {

//! Inverset time-scale (Omega) BC's
class CCINSTurbOmegaBC: public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSTurbOmegaBC(const BCPackage& bc, int offset);
    virtual ~CCINSTurbOmegaBC();
    //@}

    //! Set a user-defined BC value
    virtual Real setUserValue();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSTurbOmegaBC(const CCINSTurbOmegaBC&);
    CCINSTurbOmegaBC& operator=(const CCINSTurbOmegaBC&);
    //@}

};

}

#endif // CCINSTurbOmegaBC_h
