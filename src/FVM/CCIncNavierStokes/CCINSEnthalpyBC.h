//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSEnthalpyBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Enthalpy boundary condition based on sidesets
//******************************************************************************
#ifndef CCINSEnthalpyBC_h
#define CCINSEnthalpyBC_h

#include <CCSideSetBC.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;


//! Enthalpy boundary condition based on surfaces
class CCINSEnthalpyBC : public CCSideSetBC
{

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSEnthalpyBC(const BCPackage& bc, int offset);
    virtual ~CCINSEnthalpyBC();
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSEnthalpyBC(const CCINSEnthalpyBC&);
    CCINSEnthalpyBC& operator=(const CCINSEnthalpyBC&);
    //@}

};

}

#endif // CCINSEnthalpyBC_h
