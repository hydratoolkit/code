//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSHistoryDelegates.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   History output delegates
//******************************************************************************
#ifndef CCINSHistDelegates_h
#define CCINSHistDelegates_h

#include <CCINSFlow.h>

namespace Hydra {

//! Declaration of the history delegates

//! Generic
OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemScalarHistory,
                     CCINSFlowElemScalarHistory)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemVectorHistory,
                     CCINSFlowElemVectorHistory)

//! Element time-history

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemEddyViscosityHistory,
                     CCINSFlowElemEddyViscosityHistory)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemEnstrophyHistory,
                     CCINSFlowElemEnstrophyHistory)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemHelicityHistory,
                     CCINSFlowElemHelicityHistory)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemPressureHistory,
                     CCINSFlowElemPressureHistory)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemVorticityHistory,
                     CCINSFlowElemVorticityHistory)

//! Node time-history


//! Surface time-history (integrated)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeAvgSurfacePressureHistory,
                     CCINSFlowAvgSurfacePressureHistory)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeAvgSurfaceTemperatureHistory,
                     CCINSFlowAvgSurfaceTemperatureHistory)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeAvgSurfaceVelocityHistory,
                     CCINSFlowAvgSurfaceVelocityHistory)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfaceAreaHistory,
                     CCINSFlowSurfaceAreaHistory)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfaceFillVolumeHistory,
                     CCINSFlowSurfaceFillVolumeHistory)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfaceForceHistory,
                     CCINSFlowSurfaceForceHistory)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfaceHeatFlowHistory,
                     CCINSFlowSurfaceHeatFlowHistory)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfacePressureForceHistory,
                     CCINSFlowSurfacePressureForceHistory)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfaceViscousForceHistory,
                     CCINSFlowSurfaceViscousForceHistory)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfaceMassFlowRateHistory,
                     CCINSFlowSurfaceMassFlowRateHistory)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfaceVolumeFlowRateHistory,
                     CCINSFlowSurfaceVolumeFlowRateHistory)

}

#endif // CCINSHistDelegates_h
