//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSPassiveOutflowBC.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:23 2011
//! \brief   Passive outflow boundary condition based on surfaces
//******************************************************************************
#include <cmath>

using namespace std;

#include <BCPackage.h>
#include <UnsMesh.h>
#include <CCINSPassiveOutflowBC.h>

using namespace Hydra;

CCINSPassiveOutflowBC::CCINSPassiveOutflowBC(const BCPackage& bc, int offset):
  SideSetBC(bc, offset)
/*******************************************************************************
Routine: CCINSPassiveOutflowBC - constructor
Author : Mark A. Christon
*******************************************************************************/
{}

CCINSPassiveOutflowBC::~CCINSPassiveOutflowBC()
/*******************************************************************************
Routine: ~CCINSPassiveOutflowBC - destructor
Author : Mark A. Christon
*******************************************************************************/
{
  // Easy way to cleanup all registered data
  freeAllVariables();
}

void
CCINSPassiveOutflowBC::apply(UnsMesh& mesh,
                             const Real thetaAx,
                             const Real dt,
                             const CVector& xc,
                             const CVector& gxc,
                             const Real* wt,
                             const Real* vf,
                             const Real* phi,
                             const Real* gphi,
                             const CVector& dphi,
                             const CVector& gdphi,
                             Real* rhs)
/*******************************************************************************
Routine: apply - apply passive outflow pressure BC
Author : Mark A. Christon
*******************************************************************************/
{
  // Haul out the surface data for the current set
  Sideset* surf = mesh.getSidesets();
  int Nel = surf[m_set].Nel;
  if (Nel == 0) return;

  Real phi_m, phi_p;
  Vector dr;

  int* edge_list = mesh.getVariable<int>(surf[m_set].DUALEDGE_LIST);

  DualEdge* edges = mesh.getDualEdges();

  Real alpha = dt*thetaAx/2.0;

  for (int i=0; i<Nel; i++) {
    int eid = edge_list[i];
    int gid = edges[eid].gid1;
    if( vf[eid] < 0.0 ) {
      // Reconstruct the '-' state
      dr.X  = edges[eid].xs[0] - xc.X[gid];
      dr.Y  = edges[eid].xs[1] - xc.Y[gid];
      dr.Z  = edges[eid].xs[2] - xc.Z[gid];

      phi_m = wt[gid]*(phi[gid] +
                       dphi.X[gid]*dr.X +
                       dphi.Y[gid]*dr.Y +
                       dphi.Z[gid]*dr.Z);

      // Reconstruct the '+' state
      dr.X  = edges[eid].xs[0] - gxc.X[eid];
      dr.Y  = edges[eid].xs[1] - gxc.Y[eid];
      dr.Z  = edges[eid].xs[2] - gxc.Z[eid];

      phi_p = wt[gid]*(gphi[eid] +
                       gdphi.X[eid]*dr.X +
                       gdphi.Y[eid]*dr.Y +
                       gdphi.Z[eid]*dr.Z);

      // Compute normal flux and do a conservative update
      rhs[gid] += alpha*edges[eid].gamma*
        (vf[eid]*(phi_p + phi_m) - fabs(vf[eid])*(phi_p - phi_m));
    }
  }
}
