//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSDeform.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Deforming mesh
//******************************************************************************
#include <cassert>
#include <stdio.h>
#include <cmath>
#include <vector>

using namespace std;

#include <macros.h>
#include <ArrayLimits.h>
#include <Control.h>
#include <FVMCCINSControl.h>
#include <DataShapes.h>
#include <Element.h>
#include <Exception.h>
#include <fileIO.h>
#include <LAMatrix.h>
#include <LAVector.h>
#include <LASolver.h>
#include <LASolverFactory.h>
#include <LASolverStatus.h>
#include <UnsMesh.h>
#include <MPIWrapper.h>
#include <Timer.h>
#include <CCINSDeform.h>
#include <CCINSFlow.h>
#include <CCINSDisplacementBC.h>
#include <CCINSSurfDispBC.h>

using namespace Hydra;

void
CCINSFlow::calcMeshDualEdgeVelocity()
/*******************************************************************************
Routine: calcMeshDualEdgeVelocity - Compute dual-edge mesh velocity and adjust
                                    the dual-edge velocity
Author : Mark A. Christon
*******************************************************************************/
{
  using namespace ArrayLimits;

  int  conn[4];
  int lconn[4];

  int* cidmap   = mesh->getElementClassMap();
  Real* vfn     = mesh->getVariable<Real>(DUALEDGE_VELN);
  CVector disp  = mesh->getCVector(NODE_DISP);
  CVector dispN = mesh->getCVector(NODE_DISPN);
  CVector vm    = mesh->getCVector(NODE_VEL);

  // Compute the nodal velocity
  Real dti = 1.0/m_incParm.dt;
  for (int i=0; i<Nnp; i++) {
    vm.X[i] = dti*(disp.X[i] - dispN.X[i]);
    vm.Y[i] = dti*(disp.Y[i] - dispN.Y[i]);
    vm.Z[i] = dti*(disp.Z[i] - dispN.Z[i]);
  }

  Vector vmmin;
  Vector vmmax;
  getVarLimits(vm, vmmin, vmmax, Nnp);
  cout << "V-Min: = " << vmmin.X << ", " << vmmin.Y << ", " << vmmin.Z << endl;
  cout << "V-Max: = " << vmmax.X << ", " << vmmax.Y << ", " << vmmax.Z << endl;
  writeEdgeLimits(cout, vfn);

  // Compute face mesh velocity and adjust
  for (int i=0; i<Nedge; i++) {
    int el = edges[i].gid1;
    int sd = edges[i].side1;
    int nc = cidmap[el];
    Element* ec = mesh->getElementClass(nc);
    int eoffset = ec->getElementOffset();
    int lid = el - eoffset;
    int Nnpf = ec->getNodesPerFace(sd);
    ec->getFaceConnectivity(lid, sd, conn, lconn);

    Real vfmx = 0.0;
    Real vfmy = 0.0;
    Real vfmz = 0.0;
    for(int j=0; j< Nnpf; j++) {
      vfmx += vm.X[conn[j]];
      vfmy += vm.Y[conn[j]];
      vfmz += vm.Z[conn[j]];
    }
    vfn[i] -= (vfmx*edges[i].en[0] +
               vfmy*edges[i].en[1] +
               vfmz*edges[i].en[2])/Nnpf ;
  }

  cout << "After subtracting off mesh velocity" << endl;
  writeEdgeLimits(cout, vfn);
#ifdef DONT
  Sideset* surf = mesh->getSidesets();
  int nel = surf[2].Nel;
  int* edge_list = mesh->getVariable<int>(surf[2].DUALEDGE_LIST);
  cout << "Set Id = " << surf[2].id << endl;
  cout << "Ext. Edge - vf" << endl;
  for (int i=0; i<nel; i++) {
    cout  << i << " - " << vfn[edge_list[i]] << endl;
  }
#endif

  // Compute the wall face-center velocity to be used in
  // the wall-function approach
  if (m_hasTurbModel) calcWallMeshVelocity(vm);

}

void
CCINSFlow::calcWallMeshVelocity(CVector& velmesh)
/*******************************************************************************
Routine: calcWallMeshVelocity - Compute the mesh velocity at the wall dual edges
Author : Mark A. Christon
*******************************************************************************/
{
  if(m_Nedge_wall <= 0) return;

  int lconn[4];
  int conn[4];
  int* edwall = mesh->getVariable<int>(EDGE_WALL_LIST);
  int* cidmap = mesh->getElementClassMap();

  CVector velwall = mesh->getCVector(EDGE_WALL_VEL);

  for (int i=0; i<m_Nedge_wall; i++) {
    int eid = edwall[i];
    int el  = edges[eid].gid1;
    int sd  = edges[eid].side1;
    int nc  = cidmap[el];
    Element* ec = mesh->getElementClass(nc);
    int eoffset = ec->getElementOffset();
    int lid     = el - eoffset;
    int Nnpf    = ec->getNodesPerFace(sd);
    ec->getFaceConnectivity(lid, sd, conn, lconn);

    Real Nnpfi = 1.0/Nnpf;
    Real velx  = 0.0;
    Real vely  = 0.0;
    Real velz  = 0.0;

    for(int j=0; j< Nnpf; j++) {
      velx += velmesh.X[conn[j]];
      vely += velmesh.Y[conn[j]];
      velz += velmesh.Z[conn[j]];
    }
    velx *= Nnpfi;
    vely *= Nnpfi;
    velz *= Nnpfi;

    velwall.X[i] = velx;
    velwall.Y[i] = vely;
    velwall.Z[i] = velz;
  }
}

void
CCINSFlow::deformMesh()
/*******************************************************************************
Routine: deformMesh - Deform the mesh according to prescribed motion
Author : Mark A. Christon
*******************************************************************************/
{
  m_meshUpdated = 0;

  Category* pcat = control->getCategory(FVM_CC_NAVIERSTOKES_CATEGORY);
  int type = pcat->getOptionVal(FVMCCINSControl::DEFORMING_MESH);

  // Spring analogy relaxation
  if (type == SPRING_DEFORM) {
    timer->startTimer(MESH_DEFORMATION);

    CVector coordOrig = mesh->getCVector(NODE_COORD);
    CVector disp      = mesh->getCVector(NODE_DISP);
    CVector dispN     = mesh->getCVector(NODE_DISPN);
    CVector coord     = mesh->getNodeCoord();
    CVector dispInc   = mesh->getCVector(TMP_VEC1);

    m_meshUpdated = 1;
    Real numInc = 1.0;

    Real scale = 1.0;
    // Please do not simplify to memset(dispInc.X, 0,Ndim*Nnp*sizeof(Real));
    // This does not handle correctly since the size in registered data
    // might be different
    memset(dispInc.X, 0, Nnp*sizeof(Real));
    memset(dispInc.Y, 0, Nnp*sizeof(Real));
    memset(dispInc.Z, 0, Nnp*sizeof(Real));
    updateDisplacementAndCoordinates(numInc, scale, coordOrig,
                                     dispInc, disp, dispN, coord);

    // Recalculate the volume
    calcVolume(VOLUME);

    // Calculate dual-edge mesh velocity and adjust the time level n
    // dual-edge velocity for advection.
    calcMeshDualEdgeVelocity();

    timer->stopTimer(MESH_DEFORMATION);
  }
}

void
CCINSFlow::filterHourglass(CVector& dispInc)
/*******************************************************************************
Routine: filterHourglass - filter hourglass modes
Author : Mark A. Christon
*******************************************************************************/
{
  // Explicit Hourglass Filter for HEX8 elements
  int Nec = mesh->numElementClass();
  for (int n=0; n<Nec; ++n) {
    Element* ec = mesh->getElementClass(n);
    Eltype type = ec->getEltype();
    if (type == HEX8) {
      int nel = ec->getNel();
      Hex8* h8ec = reinterpret_cast<Hex8*>(ec);
      Hex8IX* h8 = h8ec->getHex8Connectivity();
      Real o8th = 1.0/8.0;
      for (int i=0; i<nel; i++) {
        int n0 = h8[i].ix[0];
        int n1 = h8[i].ix[1];
        int n2 = h8[i].ix[2];
        int n3 = h8[i].ix[3];
        int n4 = h8[i].ix[4];
        int n5 = h8[i].ix[5];
        int n6 = h8[i].ix[6];
        int n7 = h8[i].ix[7];

        // X-Displacement
        Real a1 =
            dispInc.X[n0] + dispInc.X[n1] - dispInc.X[n2] - dispInc.X[n3] -
            dispInc.X[n4] - dispInc.X[n5] + dispInc.X[n6] + dispInc.X[n7];

        Real a2 =
            dispInc.X[n0] - dispInc.X[n1] - dispInc.X[n2] + dispInc.X[n3] -
            dispInc.X[n4] + dispInc.X[n5] + dispInc.X[n6] - dispInc.X[n7];

        Real a3 =
            dispInc.X[n0] - dispInc.X[n1] + dispInc.X[n2] - dispInc.X[n3] +
            dispInc.X[n4] - dispInc.X[n5] + dispInc.X[n6] - dispInc.X[n7];

        Real a4 =
          - dispInc.X[n0] + dispInc.X[n1] - dispInc.X[n2] + dispInc.X[n3] +
            dispInc.X[n4] - dispInc.X[n5] + dispInc.X[n6] - dispInc.X[n7];

        dispInc.X[n0] -= o8th*(  a1 + a2 + a3 - a4);
        dispInc.X[n1] -= o8th*(  a1 - a2 - a3 + a4);
        dispInc.X[n2] -= o8th*(- a1 - a2 + a3 - a4);
        dispInc.X[n3] -= o8th*(- a1 + a2 - a3 + a4);
        dispInc.X[n4] -= o8th*(- a1 - a2 + a3 + a4);
        dispInc.X[n5] -= o8th*(- a1 + a2 - a3 - a4);
        dispInc.X[n6] -= o8th*(  a1 + a2 + a3 + a4);
        dispInc.X[n7] -= o8th*(  a1 - a2 - a3 - a4);

        // Y-Displacement
        a1 =
            dispInc.Y[n0] + dispInc.Y[n1] - dispInc.Y[n2] - dispInc.Y[n3] -
            dispInc.Y[n4] - dispInc.Y[n5] + dispInc.Y[n6] + dispInc.Y[n7];

        a2 =
            dispInc.Y[n0] - dispInc.Y[n1] - dispInc.Y[n2] + dispInc.Y[n3] -
            dispInc.Y[n4] + dispInc.Y[n5] + dispInc.Y[n6] - dispInc.Y[n7];

        a3 =
            dispInc.Y[n0] - dispInc.Y[n1] + dispInc.Y[n2] - dispInc.Y[n3] +
            dispInc.Y[n4] - dispInc.Y[n5] + dispInc.Y[n6] - dispInc.Y[n7];

        a4 =
          - dispInc.Y[n0] + dispInc.Y[n1] - dispInc.Y[n2] + dispInc.Y[n3] +
            dispInc.Y[n4] - dispInc.Y[n5] + dispInc.Y[n6] - dispInc.Y[n7];

        dispInc.Y[n0] -= o8th*(  a1 + a2 + a3 - a4);
        dispInc.Y[n1] -= o8th*(  a1 - a2 - a3 + a4);
        dispInc.Y[n2] -= o8th*(- a1 - a2 + a3 - a4);
        dispInc.Y[n3] -= o8th*(- a1 + a2 - a3 + a4);
        dispInc.Y[n4] -= o8th*(- a1 - a2 + a3 + a4);
        dispInc.Y[n5] -= o8th*(- a1 + a2 - a3 - a4);
        dispInc.Y[n6] -= o8th*(  a1 + a2 + a3 + a4);
        dispInc.Y[n7] -= o8th*(  a1 - a2 - a3 - a4);

        // Z-Displacement
        a1 =
            dispInc.Z[n0] + dispInc.Z[n1] - dispInc.Z[n2] - dispInc.Z[n3] -
            dispInc.Z[n4] - dispInc.Z[n5] + dispInc.Z[n6] + dispInc.Z[n7];

        a2 =
            dispInc.Z[n0] - dispInc.Z[n1] - dispInc.Z[n2] + dispInc.Z[n3] -
            dispInc.Z[n4] + dispInc.Z[n5] + dispInc.Z[n6] - dispInc.Z[n7];

        a3 =
            dispInc.Z[n0] - dispInc.Z[n1] + dispInc.Z[n2] - dispInc.Z[n3] +
            dispInc.Z[n4] - dispInc.Z[n5] + dispInc.Z[n6] - dispInc.Z[n7];

        a4 =
          - dispInc.Z[n0] + dispInc.Z[n1] - dispInc.Z[n2] + dispInc.Z[n3] +
            dispInc.Z[n4] - dispInc.Z[n5] + dispInc.Z[n6] - dispInc.Z[n7];

        dispInc.Z[n0] -= o8th*(  a1 + a2 + a3 - a4);
        dispInc.Z[n1] -= o8th*(  a1 - a2 - a3 + a4);
        dispInc.Z[n2] -= o8th*(- a1 - a2 + a3 - a4);
        dispInc.Z[n3] -= o8th*(- a1 + a2 - a3 + a4);
        dispInc.Z[n4] -= o8th*(- a1 - a2 + a3 + a4);
        dispInc.Z[n5] -= o8th*(- a1 + a2 - a3 - a4);
        dispInc.Z[n6] -= o8th*(  a1 + a2 + a3 + a4);
        dispInc.Z[n7] -= o8th*(  a1 - a2 - a3 - a4);
      }
    }
  }
}

void
CCINSFlow::initDeformMesh()
/*******************************************************************************
Routine: initDeformMesh - Initialize displacements/velocities
Author : Mark A. Christon
*******************************************************************************/
{
  Category* pcat = control->getCategory(FVM_CC_NAVIERSTOKES_CATEGORY);
  int type = pcat->getOptionVal(FVMCCINSControl::DEFORMING_MESH);

  m_meshUpdated = 1;

  // Spring analogy
  if (type == SPRING_DEFORM) {
    // Set any necessary mesh deformation parameters
    setupDeformMeshParam();
  }
}

void
CCINSFlow::seedOvectors(Real* ke, Real* kii, Real* Kphi, vector<Real*>& phi,
                        Real* coord, Real* coordOrig,
                        Real* dispN, Real* dispInc)
/*******************************************************************************
Routine: seedOvectors - seed ortho-vectors for a single diplacement component
Author : Mark A. Christon
*******************************************************************************/
{
  int nvec = phi.size();

  // Calculate the phi vector for the first-mode (constant mode)
  springMatVec(ke, kii, phi[0], Kphi);
  Real anorm = 0.0;
  for (int i=0; i<Nnp; i++) {
    anorm += phi[0][i]*Kphi[i];
  }
  anorm = sqrt(anorm);
  if (anorm > 1.0e-14) {
    for (int i=0; i<Nnp; i++) {
      phi[0][i] /= anorm;
    }
  } else {
    for (int i=0; i<Nnp; i++) {
      phi[0][i] = 0.0;
    }
  }

  // Orthogonalize and normalize the remaining vectors
  for (int j=1; j<nvec; j++) {

    // Orthogonalize
    springMatVec(ke, kii, phi[j-1], Kphi);
    anorm = 0.0;
    for (int i=0; i<Nnp; i++) {
      anorm += phi[j-1][i]*Kphi[i];
    }
    anorm = sqrt(anorm);

    for (int k=j; k<nvec; k++) {
      Real xdot  = 0.0;
      for (int i=0; i<Nnp; i++) {
        xdot += phi[k][i]*Kphi[i];
      }
      xdot /= anorm;
      for (int i=0; i<Nnp; i++) {
        phi[k][i] -= xdot*phi[j-1][i];
      }
    }

    // Normalize
    springMatVec(ke, kii, phi[j], Kphi);
    anorm = 0.0;
    for (int i=0; i<Nnp; i++) {
      anorm += phi[j][i]*Kphi[i];
    }
    anorm = sqrt(anorm);
    if (anorm > 1.0e-14) {
      for (int i=0; i<Nnp; i++) {
        phi[j][i] /= anorm;
      }
    } else {
      // Drop the base vector
      for (int i=0; i<Nnp; i++) {
        phi[j][i] = 0.0;
      }
    }
  }

  // Compute the alpha's
  springMatVec(ke, kii, dispInc, Kphi);
  for (int j=0; j<nvec; j++) {
    Real alpha  = 0.0;
    for (int i=0; i<Nnp; i++) {
      alpha -= phi[j][i]*Kphi[i];
    }

    cout << "mode = " << j << ", alpha = " << alpha << endl;

    // Accumulate the displacement
    for (int i=0; i<Nnp; i++) {
      dispInc[i] += alpha*phi[j][i];
    }
  }

  // Update the coordinates
  for(int i=0; i<Nnp; i++) {
    coord[i] = coordOrig[i] + dispN[i] + dispInc[i];
  }
}

void
CCINSFlow::setupDeformMeshParam()
/*******************************************************************************
Routine: setupDeformMeshParam - setup the deforming mesh variables
Author : Mark A. Christon
*******************************************************************************/
{
}

void
CCINSFlow::springMatVec(Real* ke, Real* kii, Real* phi, Real* Kphi)
/*******************************************************************************
Routine: springMatVec - Perform edge-by-edge mat-vec
Author : Mark A. Christon
*******************************************************************************/
{
  vector< pair<int,int> >& uniquePrimalEdges = mesh->getUniquePrimalEdges();
  int nedge = uniquePrimalEdges.size();

  memset(Kphi, 0, Nnp*sizeof(Real));
  for (int i=0; i<nedge; i++) {
    int n1 = uniquePrimalEdges[i].first;
    int n2 = uniquePrimalEdges[i].second;
    Real k = ke[i];
    Kphi[n1] -= k*phi[n2];
    Kphi[n2] -= k*phi[n1];
  }

  // Diagonal term
  for (int i=0; i<Nnp; i++) {
    Kphi[i] += kii[i]*phi[i];
  }

}

void
CCINSFlow::updateDisplacementAndCoordinates(Real& /*numInc*/,
                                            Real scale,
                                            CVector& coordOrig,
                                            CVector& dispInc,
                                            CVector& disp,
                                            CVector& dispN,
                                            CVector& coord)
/*******************************************************************************
Routine: updateDisplacmentandCoordinates - update mesh configuration
Author : Mark A. Christon
*******************************************************************************/
{

  vector< pair<int,int> >& uniquePrimalEdges = mesh->getUniquePrimalEdges();
  int nedge = uniquePrimalEdges.size();

  DataIndex KEI = mesh->registerVariable(nedge,
                                         sizeof(Real),
                                         "TMP_KE");

  DataIndex KDI = mesh->registerVariable(Nnp,
                                         sizeof(Real)*Ndim,
                                         "TMP_K",
                                         NODE_CTR,
                                         VECTOR_VAR,
                                         COLUMN_MAJOR_ARRAY);

  Real* ke = mesh->getVariable<Real>(KEI);
  CVector K = mesh->getCVector(KDI);

  // Register phi-vectors, k-phi product
  vector<DataIndex> di;
  vector<Real*> phi;

  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "ONEMODE"));
  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "X1MODE"));
  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "X2MODE"));
  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "X3MODE"));
  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "X4MODE"));
  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "X5MODE"));
  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "Y1MODE"));
  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "Y2MODE"));
  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "Y3MODE"));
  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "Y4MODE"));
  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "Y5MODE"));
  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "Z1MODE"));
  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "Z2MODE"));
  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "Z3MODE"));
  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "Z4MODE"));
  di.push_back(mesh->registerVariable(Nnp, sizeof(Real), "Z5MODE"));

  // Load the pointers for local ease of use
  int nvec = di.size();
  for (int i=0; i<nvec; i++) {
    phi.push_back(mesh->getVariable<Real>(di[i]));
  }

  DataIndex KPHI = mesh->registerVariable(Nnp, sizeof(Real), "KPHI");
  Real* Kphi = mesh->getVariable<Real>(KPHI);

  using namespace ArrayLimits;
  Vector xmin;
  Vector xmax;
  getVarLimits(coord, xmin, xmax, Nnp);
  cout << "X-Min: = " << xmin.X << ", " << xmin.Y << ", " << xmin.Z << endl;
  cout << "X-Max: = " << xmax.X << ", " << xmax.Y << ", " << xmax.Z << endl;

  cout << "Base stiffness" << endl;

  // Setup the edge stiffness, i.e., freeze in the current config
  Real hmin = 1.0e+35;
  Real hmax = 0.0;
  Real kmin = 1.0e+35;
  Real kmax = 0.0;
  for (int i=0; i<nedge; i++) {
    int n1 = uniquePrimalEdges[i].first;
    int n2 = uniquePrimalEdges[i].second;
    Real dx = coord.X[n2] - coord.X[n1];
    Real dy = coord.Y[n2] - coord.Y[n1];
    Real dz = coord.Z[n2] - coord.Z[n1];
    Real h = sqrt(dx*dx + dy*dy + dz*dz);
    Real k = 1.0/h;
    ke[i] = k;

    // cout << h << " " << k << endl;

    if (k <= kmin) kmin = k;
    if (k >= kmax) kmax = k;
    if (h <= hmin) hmin = h;
    if (h >= hmax) hmax = h;
  }

  cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
  cout << "Initial stiffness range" << endl;
  cout << "kmin, kmax = " << kmin << ", " << kmax << endl;

  cout << "Scaled stiffness" << endl;
  // Scale the stiffnes matrix to stiffen locally small edges
  Real dhi = 1.0/(hmax - hmin);
  kmin = 1.0e+35;
  kmax = 0.0;
  for (int i=0; i<nedge; i++) {
    int n1 = uniquePrimalEdges[i].first;
    int n2 = uniquePrimalEdges[i].second;
    Real dx = coord.X[n2] - coord.X[n1];
    Real dy = coord.Y[n2] - coord.Y[n1];
    Real dz = coord.Z[n2] - coord.Z[n1];
    Real h = sqrt(dx*dx + dy*dy + dz*dz);
    Real hstar = dhi*(h - hmin) + 1.0e-3;
    // Real hstar = dhi*(h - hmin) + 1.0e-4;
    // Real hstar = dhi*(h - hmin) + 1.0e-6;
    // Real hstar = sqrt(dhi*(h - hmin)) + 1.0e-4;
    // Real hstar = dhi*(h - hmin);
    // hstar = hstar*hstar + 1.0e-4;
    ke[i] = ke[i]/hstar;
    if (ke[i] <= kmin) kmin = ke[i];
    if (ke[i] >= kmax) kmax = ke[i];
    // cout << h << " " << ke[i] << endl;
  }
  cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
  cout << "Scaled stiffness range" << endl;
  cout << "kmin, kmax = " << kmin << ", " << kmax << endl;

  kmin = 1.0e+35;
  kmax = 0.0;
  for (int i=0; i<nedge; i++) {
    if (ke[i] <= kmin) kmin = ke[i];
    if (ke[i] >= kmax) kmax = ke[i];
  }

  cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
  cout << "Re-Scaled stiffness range" << endl;
  cout << "kmin, kmax = " << kmin << ", " << kmax << endl;

  // Assemble the diagonal for the LHS (Don't need a vector)
  memset(K.X, 0, Nnp*Ndim*sizeof(Real));
  for (int i=0; i<nedge; i++) {
    int n1 = uniquePrimalEdges[i].first;
    int n2 = uniquePrimalEdges[i].second;
    Real k = ke[i];
    K.X[n1] += k;
    K.X[n2] += k;
    K.Y[n1] += k;
    K.Y[n2] += k;
    K.Z[n1] += k;
    K.Z[n2] += k;
  }

  // Enforce the node-based displacement BC's
  int nbc = m_displacementBCs.size();
  for (int i=0; i<nbc; i++) {
    CCINSDisplacementBC* bc = m_displacementBCs[i];
    switch(bc->getBCVar()) {
    case CCINSDisplacementBC::DISPX:
      bc->applyCartesianInc(*mesh, scale, dispN.X, dispInc.X, m_time_np1);
      break;
    case CCINSDisplacementBC::DISPY:
      bc->applyCartesianInc(*mesh, scale, dispN.Y, dispInc.Y, m_time_np1);
      break;
    case CCINSDisplacementBC::DISPZ:
      bc->applyCartesianInc(*mesh, scale, dispN.Z, dispInc.Z, m_time_np1);
      break;
    case CCINSDisplacementBC::DISPXT:
    case CCINSDisplacementBC::DISPYT:
    case CCINSDisplacementBC::DISPZT:
      bc->applyCartesianInc(*mesh, scale, dispInc, m_time_np1);
      break;
    }
  }

  // Surface-based node BC's
  nbc = m_surfDispBCs.size();
  for (int i=0; i<nbc; i++) {
    CCINSSurfDispBC* bc = m_surfDispBCs[i];
    switch(bc->getBCVar()) {
    case CCINSSurfDispBC::DISPX:
      bc->applyCartesianInc(*mesh, scale, dispN.X, dispInc.X, m_time_np1,
                            coordOrig);
      break;
    case CCINSSurfDispBC::DISPY:
      bc->applyCartesianInc(*mesh, scale, dispN.Y, dispInc.Y, m_time_np1,
                            coordOrig);
      break;
    case CCINSSurfDispBC::DISPZ:
      bc->applyCartesianInc(*mesh, scale, dispN.Z, dispInc.Z, m_time_np1,
                            coordOrig);
      break;
    case CCINSDisplacementBC::DISPXT:
    case CCINSDisplacementBC::DISPYT:
    case CCINSDisplacementBC::DISPZT:
      break;
    }
    //#####DEBUG
#ifdef DONT_USE
    bc->applyDisplacementInc(*mesh, scale, dispN, dispInc, m_time_np1);
#endif
    //#####DEBUG
  }

  // Start of C-G
  DataIndex PP  = mesh->registerVariable(Nnp,
                                         sizeof(Real)*Ndim,
                                         "PP",
                                         NODE_CTR,
                                         VECTOR_VAR,
                                         COLUMN_MAJOR_ARRAY);
  DataIndex AP  = mesh->registerVariable(Nnp*Ndim,
                                         sizeof(Real)*Ndim,
                                         "AP",
                                         NODE_CTR,
                                         VECTOR_VAR,
                                         COLUMN_MAJOR_ARRAY);
  DataIndex R   = mesh->registerVariable(Nnp*Ndim,
                                         sizeof(Real)*Ndim,
                                         "R",
                                         NODE_CTR,
                                         VECTOR_VAR,
                                         COLUMN_MAJOR_ARRAY);
  DataIndex Z   = mesh->registerVariable(Nnp*Ndim,
                                         sizeof(Real)*Ndim,
                                         "Z",
                                         NODE_CTR,
                                         VECTOR_VAR,
                                         COLUMN_MAJOR_ARRAY);


  // Compute a reference error w/o seeded O-vectors
  CVector ap = mesh->getCVector(AP);
  CVector r  = mesh->getCVector(R);
  memset(ap.X, 0, Nnp*Ndim*sizeof(Real));

  // Setup the residual for iteration-0
  springMatVec(ke, K.X, dispInc.X, ap.X);
  springMatVec(ke, K.Y, dispInc.Y, ap.Y);
  springMatVec(ke, K.Z, dispInc.Z, ap.Z);
  for (int i=0; i<Nnp; i++) {
    r.X[i] = - ap.X[i];
    r.Y[i] = - ap.Y[i];
    r.Z[i] = - ap.Z[i];
  }
  // Force the homogeneous displacement BC's
  nbc = m_surfDispBCs.size();
  for (int i=0; i<nbc; i++) {
    CCINSSurfDispBC* bc = m_surfDispBCs[i];
    switch(bc->getBCVar()) {
    case CCINSSurfDispBC::DISPX:
      bc->applyZeroDisplacement(r.X);
      break;
    case CCINSSurfDispBC::DISPY:
      bc->applyZeroDisplacement(r.Y);
      break;
    case CCINSSurfDispBC::DISPZ:
      bc->applyZeroDisplacement(r.Z);
      break;
    case CCINSDisplacementBC::DISPXT:
    case CCINSDisplacementBC::DISPYT:
    case CCINSDisplacementBC::DISPZT:
      break;
    }
  }
  Real errRef = 0.0;
  for (int i=0; i<Nnp; i++) {
    errRef += r.X[i]*r.X[i] + r.Y[i]*r.Y[i] + r.Z[i]*r.Z[i];
  }
  errRef = sqrt(errRef);
  cout << "Reference residual error = " << errRef << endl;

  // Seed the incremental displacement vector with ortho-vectors

  // Initialize the base vectors
  mesh->getBbox(xmin, xmax);
  Real dxi = 1.0/(xmax.X - xmin.X);
  Real dyi = 1.0/(xmax.Y - xmin.Y);
  Real dzi = 1.0/(xmax.Z - xmin.Z);

  Real dxnorm = 0.0;
  Real dynorm = 0.0;
  Real dznorm = 0.0;
  for (int i=0; i<Nnp; i++) {
    dxnorm += dispInc.X[i]*dispInc.X[i];
    dynorm += dispInc.Y[i]*dispInc.Y[i];
    dznorm += dispInc.Z[i]*dispInc.Z[i];
  }
  dxnorm = sqrt(dxnorm);
  dynorm = sqrt(dynorm);
  dznorm = sqrt(dznorm);

  // x-displacement modes
  Real* one    = mesh->getVariable<Real>(di[ 0]);
  Real* x1mode = mesh->getVariable<Real>(di[ 1]);
  Real* x2mode = mesh->getVariable<Real>(di[ 2]);
  Real* x3mode = mesh->getVariable<Real>(di[ 3]);
  Real* x4mode = mesh->getVariable<Real>(di[ 4]);
  Real* x5mode = mesh->getVariable<Real>(di[ 5]);
  Real* y1mode = mesh->getVariable<Real>(di[ 6]);
  Real* y2mode = mesh->getVariable<Real>(di[ 7]);
  Real* y3mode = mesh->getVariable<Real>(di[ 8]);
  Real* y4mode = mesh->getVariable<Real>(di[ 9]);
  Real* y5mode = mesh->getVariable<Real>(di[10]);
  Real* z1mode = mesh->getVariable<Real>(di[11]);
  Real* z2mode = mesh->getVariable<Real>(di[12]);
  Real* z3mode = mesh->getVariable<Real>(di[13]);
  Real* z4mode = mesh->getVariable<Real>(di[14]);
  Real* z5mode = mesh->getVariable<Real>(di[15]);

  // Coefficients for unit-amplitude scaling
  Real C2 = 4.0;
  Real C3 = 20.7846;
  Real C4 = 81.0045;
  Real C5 = 281.9824;

  if (dxnorm > 1.0e-14) {
    cout << "Seeding x-solution" << endl;
    for (int i=0; i<Nnp; i++) {
      Real xstar = (coord.X[i] - xmin.X)*dxi;
      Real ystar = (coord.Y[i] - xmin.Y)*dyi;
      Real zstar = (coord.Z[i] - xmin.Z)*dzi;
      one[i]    = 1.0;
      x1mode[i] = xstar;
      x2mode[i] = C2*xstar*(1.0 - xstar);
      x3mode[i] = C3*xstar*(0.5 - xstar)*(1.0 - xstar);
      x4mode[i] = C4*xstar*(0.3333 - xstar)*(0.6666 - xstar)*(1.0 - xstar);
      x5mode[i] = C5*xstar*(0.25 - xstar)*(0.5 - xstar)*(0.75 - xstar)*(1.0 - xstar);
      y1mode[i] = ystar;
      y2mode[i] = C2*ystar*(1.0 - ystar);
      y3mode[i] = C3*ystar*(0.5 - ystar)*(1.0 - ystar);
      y4mode[i] = C4*ystar*(0.3333 - ystar)*(0.6666 - ystar)*(1.0 - ystar);
      y5mode[i] = C5*ystar*(0.25 - ystar)*(0.5 - ystar)*(0.75 - ystar)*(1.0 - ystar);
      z1mode[i] = zstar;
      z2mode[i] = C2*zstar*(1.0 - zstar);
      z3mode[i] = C3*zstar*(0.5 - zstar)*(1.0 - zstar);
      z4mode[i] = C4*zstar*(0.3333 - zstar)*(0.6666 - zstar)*(1.0 - zstar);
      z5mode[i] = C5*zstar*(0.25 - zstar)*(0.5 - zstar)*(0.75 - zstar)*(1.0 - zstar);
#ifdef POLYNOMIAL
      x1mode[i] = xstar;
      x2mode[i] = sin(PI*xstar);
      x3mode[i] = sin(2.0*PI*xstar);
      x4mode[i] = sin(3.0*PI*xstar);
      x5mode[i] = sin(4.0*PI*xstar);
      y1mode[i] = ystar;
      y2mode[i] = sin(PI*ystar);
      y3mode[i] = sin(2.0*PI*ystar);
      y4mode[i] = sin(3.0*PI*ystar);
      y5mode[i] = sin(4.0*PI*ystar);
      z1mode[i] = zstar;
      z2mode[i] = sin(PI*zstar);
      z3mode[i] = sin(2.0*PI*zstar);
      z4mode[i] = sin(3.0*PI*zstar);
      z5mode[i] = sin(4.0*PI*zstar);
#endif
    }

    // Force the homogeneous displacement BC's
    nbc = m_surfDispBCs.size();
    for (int i=0; i<nbc; i++) {
      CCINSSurfDispBC* bc = m_surfDispBCs[i];
      if(bc->getBCVar() == CCINSSurfDispBC::DISPX) {
        for (int j=0; j<nvec; j++) {
          bc->applyZeroDisplacement(phi[j]);
        }
      }
    }

    seedOvectors(ke, K.X, Kphi, phi, coord.X, coordOrig.X, dispN.X, dispInc.X);

  }

  if (dynorm > 1.0e-14) {
    cout << "Seeding y-solution" << endl;
    for (int i=0; i<Nnp; i++) {
      Real xstar = (coord.X[i] - xmin.X)*dxi;
      Real ystar = (coord.Y[i] - xmin.Y)*dyi;
      Real zstar = (coord.Z[i] - xmin.Z)*dzi;
      one[i]    = 1.0;
      x1mode[i] = xstar;
      x2mode[i] = C2*xstar*(1.0 - xstar);
      x3mode[i] = C3*xstar*(0.5 - xstar)*(1.0 - xstar);
      x4mode[i] = C4*xstar*(0.3333 - xstar)*(0.6666 - xstar)*(1.0 - xstar);
      x5mode[i] = C5*xstar*(0.25 - xstar)*(0.5 - xstar)*(0.75 - xstar)*(1.0 - xstar);
      y1mode[i] = ystar;
      y2mode[i] = C2*ystar*(1.0 - ystar);
      y3mode[i] = C3*ystar*(0.5 - ystar)*(1.0 - ystar);
      y4mode[i] = C4*ystar*(0.3333 - ystar)*(0.6666 - ystar)*(1.0 - ystar);
      y5mode[i] = C5*ystar*(0.25 - ystar)*(0.5 - ystar)*(0.75 - ystar)*(1.0 - ystar);
      z1mode[i] = zstar;
      z2mode[i] = C2*zstar*(1.0 - zstar);
      z3mode[i] = C3*zstar*(0.5 - zstar)*(1.0 - zstar);
      z4mode[i] = C4*zstar*(0.3333 - zstar)*(0.6666 - zstar)*(1.0 - zstar);
      z5mode[i] = C5*zstar*(0.25 - zstar)*(0.5 - zstar)*(0.75 - zstar)*(1.0 - zstar);
#ifdef POLYNOMIAL
      x1mode[i] = xstar;
      x2mode[i] = sin(PI*xstar);
      x3mode[i] = sin(2.0*PI*xstar);
      x4mode[i] = sin(3.0*PI*xstar);
      x5mode[i] = sin(4.0*PI*xstar);
      y1mode[i] = ystar;
      y2mode[i] = sin(PI*ystar);
      y3mode[i] = sin(2.0*PI*ystar);
      y4mode[i] = sin(3.0*PI*ystar);
      y5mode[i] = sin(4.0*PI*ystar);
      z1mode[i] = zstar;
      z2mode[i] = sin(PI*zstar);
      z3mode[i] = sin(2.0*PI*zstar);
      z4mode[i] = sin(3.0*PI*zstar);
      z5mode[i] = sin(4.0*PI*zstar);
#endif
    }

    // Force the homogeneous displacement BC's
    nbc = m_surfDispBCs.size();
    for (int i=0; i<nbc; i++) {
      CCINSSurfDispBC* bc = m_surfDispBCs[i];
      if(bc->getBCVar() == CCINSSurfDispBC::DISPY) {
        for (int j=0; j<nvec; j++) {
          bc->applyZeroDisplacement(phi[j]);
        }
      }
    }

    seedOvectors(ke, K.Y, Kphi, phi, coord.Y, coordOrig.Y, dispN.Y, dispInc.Y);
  }

  if (dznorm > 1.0e-14) {
    cout << "Seeding z-solution" << endl;
    for (int i=0; i<Nnp; i++) {
      Real xstar = (coord.X[i] - xmin.X)*dxi;
      Real ystar = (coord.Y[i] - xmin.Y)*dyi;
      Real zstar = (coord.Z[i] - xmin.Z)*dzi;
      one[i]    = 1.0;
      x1mode[i] = xstar;
      x2mode[i] = C2*xstar*(1.0 - xstar);
      x3mode[i] = C3*xstar*(0.5 - xstar)*(1.0 - xstar);
      x4mode[i] = C4*xstar*(0.3333 - xstar)*(0.6666 - xstar)*(1.0 - xstar);
      x5mode[i] = C5*xstar*(0.25 - xstar)*(0.5 - xstar)*(0.75 - xstar)*(1.0 - xstar);
      y1mode[i] = ystar;
      y2mode[i] = C2*ystar*(1.0 - ystar);
      y3mode[i] = C3*ystar*(0.5 - ystar)*(1.0 - ystar);
      y4mode[i] = C4*ystar*(0.3333 - ystar)*(0.6666 - ystar)*(1.0 - ystar);
      y5mode[i] = C5*ystar*(0.25 - ystar)*(0.5 - ystar)*(0.75 - ystar)*(1.0 - ystar);
      z1mode[i] = zstar;
      z2mode[i] = C2*zstar*(1.0 - zstar);
      z3mode[i] = C3*zstar*(0.5 - zstar)*(1.0 - zstar);
      z4mode[i] = C4*zstar*(0.3333 - zstar)*(0.6666 - zstar)*(1.0 - zstar);
      z5mode[i] = C5*zstar*(0.25 - zstar)*(0.5 - zstar)*(0.75 - zstar)*(1.0 - zstar);
#ifdef POLYNOMIAL
      x1mode[i] = xstar;
      x2mode[i] = sin(PI*xstar);
      x3mode[i] = sin(2.0*PI*xstar);
      x4mode[i] = sin(3.0*PI*xstar);
      x5mode[i] = sin(4.0*PI*xstar);
      y1mode[i] = ystar;
      y2mode[i] = sin(PI*ystar);
      y3mode[i] = sin(2.0*PI*ystar);
      y4mode[i] = sin(3.0*PI*ystar);
      y5mode[i] = sin(4.0*PI*ystar);
      z1mode[i] = zstar;
      z2mode[i] = sin(PI*zstar);
      z3mode[i] = sin(2.0*PI*zstar);
      z4mode[i] = sin(3.0*PI*zstar);
      z5mode[i] = sin(4.0*PI*zstar);
#endif
    }

    // Force the homogeneous displacement BC's
    nbc = m_surfDispBCs.size();
    for (int i=0; i<nbc; i++) {
      CCINSSurfDispBC* bc = m_surfDispBCs[i];
      if(bc->getBCVar() == CCINSSurfDispBC::DISPZ) {
        for (int j=0; j<nvec; j++) {
          bc->applyZeroDisplacement(phi[j]);
        }
      }
    }

    seedOvectors(ke, K.Z, Kphi, phi, coord.Z, coordOrig.Z, dispN.Z, dispInc.Z);
  }

  // int itmax = 0;
  // int itmax = 500;
  // int itmax = 1500;
  int itmax = 5000;
  // int itmax = 10000;
  // int itmax = 40000;
  Real eps = 1.0e-6;
  Real zrn = 0.0;
  Real zro = 0.0;
  Real alphap = 0.0;
  Real beta = 0.0;
  CVector p  = mesh->getCVector(PP);
  CVector z  = mesh->getCVector(Z);

  memset( z.X, 0, Nnp*Ndim*sizeof(Real));
  memset(ap.X, 0, Nnp*Ndim*sizeof(Real));

  // Setup the residual for iteration-0
  springMatVec(ke, K.X, dispInc.X, ap.X);
  springMatVec(ke, K.Y, dispInc.Y, ap.Y);
  springMatVec(ke, K.Z, dispInc.Z, ap.Z);
  for (int i=0; i<Nnp; i++) {
    r.X[i] = - ap.X[i];
    r.Y[i] = - ap.Y[i];
    r.Z[i] = - ap.Z[i];
  }
  // Force the homogeneous displacement BC's
  nbc = m_surfDispBCs.size();
  for (int i=0; i<nbc; i++) {
    CCINSSurfDispBC* bc = m_surfDispBCs[i];
    switch(bc->getBCVar()) {
    case CCINSSurfDispBC::DISPX:
      bc->applyZeroDisplacement(r.X);
      break;
    case CCINSSurfDispBC::DISPY:
      bc->applyZeroDisplacement(r.Y);
      break;
    case CCINSSurfDispBC::DISPZ:
      bc->applyZeroDisplacement(r.Z);
      break;
    case CCINSDisplacementBC::DISPXT:
    case CCINSDisplacementBC::DISPYT:
    case CCINSDisplacementBC::DISPZT:
      break;
    }
  }
  Real err0 = 0.0;
  for (int i=0; i<Nnp; i++) {
    err0 += r.X[i]*r.X[i] + r.Y[i]*r.Y[i] + r.Z[i]*r.Z[i];
  }
  err0 = sqrt(err0);

  Real err = err0/errRef;
  cout << "Starting residual error = " << err0 << endl;
  cout << "Starting error = " << err << endl;

  // Make this real epsilon? or just small
  if (err >= eps) {

  int it = 0;

//  while (it < itmax && err > eps) {
  while (it < itmax && err > eps) {

    // Jacobi precondition, i.e., diagonal-scaling
    for (int i=0; i<Nnp; i++) {
      z.X[i] = r.X[i]/K.X[i];
      z.Y[i] = r.Y[i]/K.Y[i];
      z.Z[i] = r.Z[i]/K.Z[i];
    }

    zrn = 0.0;
    for (int i=0; i<Nnp; i++) {
      zrn += z.X[i]*r.X[i] + z.Y[i]*r.Y[i] + z.Z[i]*r.Z[i];
    }

    if (it == 0) {
      beta = 0.0;
    } else {
      beta = zrn/zro;
    }

    // Update p
    for (int i=0; i<Nnp; i++) {
      p.X[i] = z.X[i] + beta*p.X[i];
      p.Y[i] = z.Y[i] + beta*p.Y[i];
      p.Z[i] = z.Z[i] + beta*p.Z[i];
    }

    // Ap product
    memset(ap.X, 0, Nnp*Ndim*sizeof(Real));
    springMatVec(ke, K.X, p.X, ap.X);
    springMatVec(ke, K.Y, p.Y, ap.Y);
    springMatVec(ke, K.Z, p.Z, ap.Z);

    // Force the homogeneous displacement BC's
    nbc = m_surfDispBCs.size();
    for (int i=0; i<nbc; i++) {
      CCINSSurfDispBC* bc = m_surfDispBCs[i];
      switch(bc->getBCVar()) {
      case CCINSSurfDispBC::DISPX:
        bc->applyZeroDisplacement(ap.X);
        break;
      case CCINSSurfDispBC::DISPY:
        bc->applyZeroDisplacement(ap.Y);
        break;
      case CCINSSurfDispBC::DISPZ:
        bc->applyZeroDisplacement(ap.Z);
        break;
      case CCINSDisplacementBC::DISPXT:
      case CCINSDisplacementBC::DISPYT:
      case CCINSDisplacementBC::DISPZT:
        break;
      }
    }

    Real pap = 0.0;
    for (int i=0; i<Nnp; i++) {
      pap += p.X[i]*ap.X[i] + p.Y[i]*ap.Y[i] + p.Z[i]*ap.Z[i];
    }
    alphap = zrn/pap;

    // Update solution, residual
    for (int i=0; i<Nnp; i++) {
      dispInc.X[i] += alphap*p.X[i];
      dispInc.Y[i] += alphap*p.Y[i];
      dispInc.Z[i] += alphap*p.Z[i];

      r.X[i] -= alphap*ap.X[i];
      r.Y[i] -= alphap*ap.Y[i];
      r.Z[i] -= alphap*ap.Z[i];
    }

    zro = zrn;

    // Test error
    err = 0.0;
    for (int i=0; i<Nnp; i++) {
      err += r.X[i]*r.X[i] + r.Y[i]*r.Y[i] + r.Z[i]*r.Z[i];
    }
    err = sqrt(err)/errRef;

    // Explicit Hourglass Filter for HEX8 elements
    filterHourglass(dispInc);

    // Surface-based node BC's
    nbc = m_surfDispBCs.size();
    for (int i=0; i<nbc; i++) {
      CCINSSurfDispBC* bc = m_surfDispBCs[i];
      switch(bc->getBCVar()) {
      case CCINSSurfDispBC::DISPX:
        bc->applyCartesianInc(*mesh, scale, dispN.X, dispInc.X, m_time_np1,
                              coordOrig);
        break;
      case CCINSSurfDispBC::DISPY:
        bc->applyCartesianInc(*mesh, scale, dispN.Y, dispInc.Y, m_time_np1,
                              coordOrig);
        break;
      case CCINSSurfDispBC::DISPZ:
        bc->applyCartesianInc(*mesh, scale, dispN.Z, dispInc.Z, m_time_np1,
                              coordOrig);
        break;
      case CCINSDisplacementBC::DISPXT:
      case CCINSDisplacementBC::DISPYT:
      case CCINSDisplacementBC::DISPZT:
        break;
      }
    }

    it++;

    cout << "Iteration = " << it << ", Error = " << err << endl;

  }

  // Final residual
  memset(ap.X, 0, Nnp*Ndim*sizeof(Real));
  springMatVec(ke, K.X, dispInc.X, ap.X);
  springMatVec(ke, K.Y, dispInc.Y, ap.Y);
  springMatVec(ke, K.Z, dispInc.Z, ap.Z);
  for (int i=0; i<Nnp; i++) {
    r.X[i] = - ap.X[i];
    r.Y[i] = - ap.Y[i];
    r.Z[i] = - ap.Z[i];
  }

  // Force the homogeneous displacement BC's
  nbc = m_surfDispBCs.size();
  for (int i=0; i<nbc; i++) {
    CCINSSurfDispBC* bc = m_surfDispBCs[i];
    switch(bc->getBCVar()) {
    case CCINSSurfDispBC::DISPX:
      bc->applyZeroDisplacement(r.X);
      break;
    case CCINSSurfDispBC::DISPY:
      bc->applyZeroDisplacement(r.Y);
      break;
    case CCINSSurfDispBC::DISPZ:
      bc->applyZeroDisplacement(r.Z);
      break;
    case CCINSDisplacementBC::DISPXT:
    case CCINSDisplacementBC::DISPYT:
    case CCINSDisplacementBC::DISPZT:
      break;
    }
  }

  Real errf = 0.0;
  for (int i=0; i<Nnp; i++) {
    errf += r.X[i]*r.X[i] + r.Y[i]*r.Y[i] + r.Z[i]*r.Z[i];
  }
  errf = sqrt(errf);
  cout << "Final true residual error = " << errf << endl;

  } // End of if-test on initial error

  // Update the coordinates
  for(int i=0; i<Nnp; i++) {
    coord.X[i] = coordOrig.X[i] + dispN.X[i] + dispInc.X[i];
    coord.Y[i] = coordOrig.Y[i] + dispN.Y[i] + dispInc.Y[i];
    coord.Z[i] = coordOrig.Z[i] + dispN.Z[i] + dispInc.Z[i];
  }

  // Update the displacements and coordinates
  for (int k=0; k<Nnp; k++) {
    disp.X[k] = dispN.X[k] + dispInc.X[k];
    disp.Y[k] = dispN.Y[k] + dispInc.Y[k];
    disp.Z[k] = dispN.Z[k] + dispInc.Z[k];
  }

  // Update the mesh bounding box
  mesh->calcBbox();

  // Release the memory
  mesh->freeVariable(KEI);
  mesh->freeVariable(KDI);
  mesh->freeVariable(PP);
  mesh->freeVariable(AP);
  mesh->freeVariable(R);
  mesh->freeVariable(Z);
  for (int i=0; i<nvec; i++) {
    mesh->freeVariable(di[i]);
  }
  di.clear();
  phi.clear();

}

