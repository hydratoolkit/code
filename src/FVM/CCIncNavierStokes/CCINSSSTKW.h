//******************************************************************************
/*!
  \file    src/FVM/CCIncNavierStokes/CCINSSSTKW.h
  \author  Mark A. Christon
  \date    Thu Jul 14 12:46:22 2011
  \brief   Solve the SST k-w model equations
 */
//******************************************************************************
#ifndef CCINSSSTKW_h
#define CCINSSSTKW_h

#include <CCINSTurbulence.h>
#include <CCINSTurbEpsBC.h>
#include <CCINSTurbKEBC.h>
#include <CCINSTurbOmegaBC.h>

namespace Hydra {

class CCINSSSTKW : public CCINSTurbulence {

  public:

    //! Constructor
    CCINSSSTKW(UnsMesh& mesh,
               Control& control,
               DualEdgeGradOp& edgeGradOp,
               CCINSErrors& errors,
               fileIO& io,
               CCINSTransportVar& di,
               vector<CCINSTurbEpsBC*>& turbEpsBCs,
               vector<CCINSTurbKEBC*>& turbKEBCs,
               vector<CCINSTurbOmegaBC*>& turbOmegaBCs,
               LASolver& transport_solver,
               LAMatrix& transport_A,
               LAVector& transport_diagA,
               LAVector& transport_b,
               LAVector& transport_x,
               CCINSAdapter& adapter);

    //! Destructor
    virtual ~CCINSSSTKW();

    //! Set BC ghost values
    void applyTurbEpsBCs(const Real t, Real* var, Real* varg, bool* bcflag,
                         bool exchange);

    //! Set BC ghost values
    void applyTurbKEBCs(const Real t, Real* var, Real* varg, bool* bcflag,
                        bool exchange);

    //! Set BC ghost values
    void applyTurbOmegaBCs(const Real t, Real* var, Real* varg, bool* bcflag,
                           bool exchange);

    //! Calculate the turbulent viscosity for the K-Omega (SST) model
    virtual void calcTurbulentViscosity() {};

    //! Virtual form transport Rhs function
    //! \param[in] CCINSIncParm Time increment parameters
    //! \param[in] SourceMode   Time-weighting/time-level for source terms
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs=0);

    //! Set the wall viscosity according to the specific model
    virtual void setWallConductivity(int /*nedges*/, int* /*edge_list*/,
                                     DualEdge* /*edges*/, Real* /*kmol*/,
                                     Real* /*keff*/, Real* /*edgek*/) {}

    //! Set the wall viscosity according to the specific model
    virtual void setWallViscosity(int /*nedges*/, int* /*edge_list*/,
                                  DualEdge* /*edges*/, Real* /*mu*/,
                                  Real* /*mueff*/, Real* /*edgemu*/) {};

    //! Solve generic scalar transport equation
    //! \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);

  protected:

    //! Add the source terms to the LHS and RHS
    void addSourcesKE(LAMatrix& S, Real* rhs);

    //! Add the source terms to the LHS and RHS
    void addSourcesOmega(LAMatrix& S, Real* rhs);

    //! Form k-transport RHS
    void formRhsKE(const CCINSIncParm& incParm,
                   FVMCCINS::SourceMode srcmode,
                   Real* rhs);

    //! Form w-transport RHS
    void formRhsOmega(const CCINSIncParm& incParm,
                      FVMCCINS::SourceMode srcmode,
                      Real* rhs);

    //! Set the variables at time level n
    void setKOmegaVariables();

    //! Solve KE scalar transport equation
    void solveKE(const CCINSIncParm& incParm);

    //! Solve Omega scalar transport equation
    void solveOmega(const CCINSIncParm& incParm);

  private:
    // Don't permit copy or assignment operators
    CCINSSSTKW(const CCINSSSTKW&);
    CCINSSSTKW& operator=(const CCINSSSTKW&);

    // Boundary conditions
    vector<CCINSTurbEpsBC*>& m_turbEpsBCs;
    vector<CCINSTurbKEBC*>& m_turbKEBCs;
    vector<CCINSTurbOmegaBC*>& m_turbOmegaBCs;
};

}

#endif // CCINSSSTKW_h
