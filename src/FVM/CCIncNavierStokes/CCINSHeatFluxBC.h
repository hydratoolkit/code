//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSHeatFluxBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Heat flux boundary condition based on sidesets
//******************************************************************************
#ifndef CCINSHeatFluxBC_h
#define CCINSHeatFluxBC_h

#include <CCSideSetBC.h>
#include <CCINSSolutionOptions.h>

namespace Hydra {

struct BCPackage;

class CCINSHeatFluxBC: public CCSideSetBC
{
  public:

    //! \name Constructor/Destructor
    //@{
             CCINSHeatFluxBC(const BCPackage& bc);
    virtual ~CCINSHeatFluxBC();
    //@}

    //! Apply a prescribed heat-flux BC
    void apply(UnsMesh& mesh, const Real thetaFn, const Real thetaFnp1,
               const Real tn, const Real dt, const Real tnp1, Real* rhs,
               const FVMCCINS::SourceMode mode=FVMCCINS::TIME_LEVEL_WEIGHTED);

    //! Apply a prescribed heat-flux BC
    void apply(UnsMesh& mesh, const Real theta, const Real time,
               const Real dt, Real* rhs);

    //! Set the edge-temperature for a given heat flux BC
    void calcEdgeTemperature(UnsMesh& mesh, Real* T, const Real* edgek,
                             Real* gd, Real t);

    //! Evaluate the heat-flux at the boundary
    void eval(UnsMesh& mesh, const Real time, Real* hflux);

    //! Computes the nodes projection at face boundaries
    void invDistOp(UnsMesh& mesh, Real t, const Real* T, const Real* edgek,
                   Real* node_var, Real* inverse_distance_wf) const;

    //! User function for setting heat flux
    virtual Real setUserValue();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSHeatFluxBC(const CCINSHeatFluxBC&);
    CCINSHeatFluxBC& operator=(const CCINSHeatFluxBC&);
    //@}

};

}

#endif
