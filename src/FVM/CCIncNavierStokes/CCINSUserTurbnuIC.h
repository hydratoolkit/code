//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSUserTurbnuIC.h
//! \author  Mark A. Christon
//! \date    Tue Sep  5 15:44:10 MDT 2017
//! \brief   User-defined epsilon IC's
//******************************************************************************
#ifndef CCINSUserTurbnuIC_h
#define CCINSUserTurbnuIC_h

#include <CCINSFlow.h>
#include <UserIC.h>

namespace Hydra {

//! User-defined Turbnu (SA) IC
class CCINSUserTurbnuIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSUserTurbnuIC();
    virtual ~CCINSUserTurbnuIC();
    //@}

    //! Virtual function to setup IC's using a mesh and set of DataIndices
    void setICs(UnsMesh* mesh, const DataIndex TURB_NU);

    //! Set user-defined Spalart-Allmaras initial conditions
    void setUserTurbnu();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSUserTurbnuIC(const CCINSUserTurbnuIC&);
    CCINSUserTurbnuIC& operator=(const CCINSUserTurbnuIC&);
    //@}

    //! \name Data presented for the user-defined velocity/acceleration functions
    //@{
    int m_elemId;      //!< Element Id for user access
    int m_matId;       //!< Material Id for user access
    Vector m_coord;    //!< Element centroid coordinates
    Real   m_turbnu;   //!< Element temperature
    Material* m_mat;   //!< Material class for user access
    //@}
};

}

#endif // CCINSUserTurbnuIC_h
