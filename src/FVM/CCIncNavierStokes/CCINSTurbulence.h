//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSTurbulence.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Virtual base class for turbulence models
//******************************************************************************
#ifndef CCINSTurbulence_h
#define CCINSTurbulence_h

#include <CCINSTransport.h>

namespace Hydra {

class CCINSTurbulence : public CCINSTransport {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSTurbulence(UnsMesh& mesh,
                             Control& control,
                             DualEdgeGradOp& edgeGradOp,
                             CCINSErrors& errors,
                             fileIO& io,
                             CCINSTransportVar& di,
                             LASolver& transport_solver,
                             LAMatrix& transport_A,
                             LAVector& transport_diagA,
                             LAVector& transport_b,
                             LAVector& transport_x,
                             CCINSAdapter& adapter);
    virtual ~CCINSTurbulence();
    //@}

    //**************************************************************************
    //! \name Virtual Turbulence Model Interface
    //@{
    //! Calculate turbulent conductivity
    virtual void calcTurbulentConductivity(Real* kturb, Real* Cp);

    //! Calculate turbulent viscosity
    virtual void calcTurbulentViscosity() = 0;

    //! Calculate y* on a surface
    virtual void calcSurfYstar(const int nel, const int* edge_list, Real* ystar);

    //! Calculate y* in the volume
    virtual void calcYstar(Real* dist, Real* ystar);

    //! Form transport Rhs function
    //! \param[in] CCINSIncParm Time increment parameters
    //! \param[in] SourceMode   Time-weighting/time-level for source terms
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode, Real* rhs=0);

    //! Set the wall viscosity according to the specific model
    virtual void setWallConductivity(int nedges, int* edge_list, DualEdge* edges,
                                     Real* kmol, Real* keff, Real* edgek) = 0;

    //! Set the wall viscosity according to the specific model
    virtual void setWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                  Real* mu, Real* mueff, Real* edgemu) = 0;

    //! Solve generic scalar transport equation(s)
    //! \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm) = 0;
    //@}

  protected:

    //! Linear solver objects
    LASolver& m_transport_solver;
    LAMatrix& m_transport_A;
    LAVector& m_transport_diagA;
    LAVector& m_transport_b;
    LAVector& m_transport_x;

    //**************************************************************************
    //! \name Model constants/parameters
    //@{
    Real m_Prt;  //!< Turbulent Prandtle Number
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSTurbulence(const CCINSTurbulence&);
    CCINSTurbulence& operator=(const CCINSTurbulence&);
    //@}

};

}

#endif // CCINSTurbulence_h
