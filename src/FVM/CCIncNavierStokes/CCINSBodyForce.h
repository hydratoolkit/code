//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSBodyForce.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Generic body force for incompressible
//******************************************************************************
#ifndef CCINSBodyForce_h
#define CCINSBodyForce_h

#include <map>
#include <HydraTypes.h>
#include <BodyForce.h>
#include <DataShapes.h>
#include <CCINSSolutionOptions.h>

namespace Hydra {

class UnsMesh;

//! Generic body force for incompressible flow solver
class CCINSBodyForce : public BodyForce {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSBodyForce() {};
             CCINSBodyForce(const int tblid,
                            const Real amp,
                            const int setid,
                            const Real* en) : BodyForce(tblid, amp, setid, en) {}
    virtual ~CCINSBodyForce() {}
    //@}

    //! Apply the body force to the RHS of the momentum equations
    void apply(UnsMesh& mesh,
               CVector& rhs,
               const Real thetaFn,
               const Real thetaFnp1,
               const Real timen,
               const Real timenp1,
               const Real dt,
               const DataIndex VOLUME,
               const DataIndex VOLUMEN,
               const FVMCCINS::SourceMode mode);

    //! Apply the body force for the partial acceleration at startup
    void apply(UnsMesh& mesh, CVector& rhs, const Real* V, const Real time);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSBodyForce(const CCINSBodyForce&);
    CCINSBodyForce& operator=(const CCINSBodyForce&);
    //@}

};

//! Boussinesq body force
class CCINSBoussinesqForce : public BodyForce {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSBoussinesqForce() {}
             CCINSBoussinesqForce(const int tblid,
                                  const Real amp,
                                  const int setid,
                                  const Real* en) :
                                  BodyForce(tblid, amp, setid, en) {}
    virtual ~CCINSBoussinesqForce() {}
    //@}

    //! Apply the body force for the normal time-step
    void apply(UnsMesh& mesh,
               CVector& rhs,
               const Real* rho,
               const DataIndex TEMPERATURE,
               const DataIndex MASSFRAC,
               const Real* V,
               map<int,int>& transportSpecies,
               int etype,
               int stype,
               const Real time);

    //! Apply the body force for the normal time-step
    void apply(const bool constantExpansion,
               UnsMesh& mesh,
               CVector& rhs,
               const Real thetaFn,
               const Real thetaFnp1,
               const Real* rho,
               const Real timen,
               const Real timenp1,
               const Real dt,
               const DataIndex TEMPERATURE,
               const DataIndex TEMPERATUREN,
               const DataIndex MASSFRAC,
               const DataIndex MASSFRACN,
               const DataIndex VOLUME,
               const DataIndex VOLUMEN,
               map<int,int>& transportSpecies,
               int etype,
               int stype,
               const FVMCCINS::SourceMode mode);

    //! Apply the solutal correction for the normal time-step
    void applyKECorrectionSolutalBinary(UnsMesh& mesh,
                                        Real* V,
                                        CVector& dMFxi,
                                        map<int,int>& transportSpecies,
                                        Real* mut,
                                        Real& ScTi,
                                        Real& time,
                                        Real& dt,
                                        Real* flag,
                                        Real* rhs);

    //! Apply the thermal correction for the normal time-step
    void applyKECorrectionThermal(UnsMesh& mesh,
                                  Real* V,
                                  CVector& dTxi,
                                  Real* mut,
                                  Real& PrTi,
                                  Real& time,
                                  Real& dt,
                                  Real* flag,
                                  Real* rhs);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSBoussinesqForce(const CCINSBoussinesqForce&);
    CCINSBoussinesqForce& operator=(const CCINSBoussinesqForce&);
    //@}

};

//! Gravity body force
class CCINSGravityForce : public BodyForce {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSGravityForce() {}
             CCINSGravityForce(const int tblid,
                               const Real amp,
                               const int setid,
                               const Real* en) :
                               BodyForce(tblid, amp, setid, en) {}
    virtual ~CCINSGravityForce() {}
    //@}

    //! Apply the body force for the normal time-step
    void apply(UnsMesh& mesh,
               CVector& rhs,
               const Real* rho,
               const Real* V,
               const Real time);

    //! Apply the body force for the normal time-step
    void apply(UnsMesh& mesh,
               CVector& rhs,
               const Real thetaFn,
               const Real thetaFnp1,
               const Real timen,
               const Real timenp1,
               const Real dt,
               const DataIndex DENSITY,
               const DataIndex DENSITYN,
               const DataIndex VOLUME,
               const DataIndex VOLUMEN,
               const FVMCCINS::SourceMode mode);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSGravityForce(const CCINSGravityForce&);
    CCINSGravityForce& operator=(const CCINSGravityForce&);
    //@}

};

}

#endif
