//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSTurbProdBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Turbulence production BC base on sidesets
//******************************************************************************
#ifndef CCINSTurbProdBC_h
#define CCINSTurbProdBC_h

#include <CCSideSetBC.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;

//! Turbulence production boundary condition based on surfaces
class CCINSTurbProdBC : public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSTurbProdBC(const BCPackage& bc, int offset);
    virtual ~CCINSTurbProdBC();
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSTurbProdBC(const CCINSTurbProdBC&);
    CCINSTurbProdBC& operator=(const CCINSTurbProdBC&);
    //@}

};

}

#endif // CCINSTurbProdBC_h
