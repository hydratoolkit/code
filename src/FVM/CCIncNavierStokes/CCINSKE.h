//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSKE.h
//! \author  Mark A. Christon
//! \date    Tue Sep 27 13:00:00 2016
//! \brief   Base class for deriving k-epsilon turbulence models
//******************************************************************************
#ifndef CCINSKE_H
#define CCINSKE_H

#include <CCINSTurbulence.h>

namespace Hydra {

class CCINSBoussinesqForce;
class CCINSTemperatureBC;
class CCINSTurbEpsBC;
class CCINSTurbKEBC;

//! Virtual base class for k-epsilon turbulence equations
class CCINSKE : public CCINSTurbulence {

  public:

    //! \name Constructor/Destructor
    //@{
            CCINSKE(UnsMesh& mesh,
                    Control& control,
                    DualEdgeGradOp& m_edgeGradOp,
                    CCINSErrors& errors,
                    fileIO& io,
                    CCINSTransportVar& di,
                    vector<CCINSTemperatureBC*>& temperatureBC,
                    vector<CCINSTurbEpsBC*>& turbEpsBCs,
                    vector<CCINSTurbKEBC*>& turbKEBCs,
                    vector<CCINSBoussinesqForce*>& boussinesq_force,
                    LASolver& transport_solver,
                    LAMatrix& transport_A,
                    LAVector& transport_diagA,
                    LAVector& transport_b,
                    LAVector& transport_x,
                    CCINSAdapter& adapter);
    virtual ~CCINSKE();
    //@}

    //**************************************************************************
    //! \name Virtual k-epsilon interface
    //! These functions are pure virtual functions that need to be
    //! implemented for each specific of k-epsilon model.
    //@{
    //! Add contributions to the LHS matrix and to the RHS vector due to the
    //! linearized sources for the e-equation
    //!   \param[in,out] S       Reference to the LHS matrix
    //!   \param[in,out] rhs     Pointer to the RHS vector
    virtual void addSourcesEps(LAMatrix* S, Real* rhs)=0;

    //! Add contributions to the LHS matrix and to the RHS vector due to the
    //! linearized sources for the k-equation
    //!   \param[in,out] S       Reference to the LHS matrix
    //!   \param[in,out] rhs     Pointer to the RHS vector
    virtual void addSourcesKE(LAMatrix* S, Real* rhs)=0;

    //! Apply the Bouyancy correction
    virtual void applyBuoyancyCorrectionKE(Real* rhs) = 0;

    //! Apply the epsilon penalty at walls
    virtual void applyWallEpsPenalty(LAMatrix& S, LAVector& B, Real* rhs) = 0;

    //! Form transport Rhs function
    //!   \param[in] CCINSIncParm Time increment parameters
    //!   \param[in] SourceMode   Time-weighting/time-level for source terms
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs=0);

    //! Form incremental part of RHS function for k-equation
    //!   \param[in]     incParm Increment parameter functions
    //!   \param[in,out] rhs     Pointer to the rhs vector
    virtual void formRhsKEInc(const CCINSIncParm& incParm, Real* rhs);

    //! Solve k transport equation
    //!   \param[in] incParm Reference to the class with increment parameters
    virtual void solveKE(const CCINSIncParm& incParm);

    //! Solve the epsilon scalar transport equation
    //!   \param[in] incParm Reference to the class with increment parameters
    virtual void solveEps(const CCINSIncParm& incParm);
    //@}

    //! Limit the minimum value of dissipation rate epsilon.
    void limitMinEpsilon();

    //! Limit the minimum value of turbulent kinetic energy
    void limitMinKE();

  protected:

    //! Apply prescribed surface temperature BCs
    void applyTemperatureBCs(Real t, Real* var, Real* varg, bool* bcflag);

    //! Set BC ghost values
    void applyTurbEpsBCs(const Real t, Real* var, Real* varg, bool* bcflag,
                         bool exchange);

    //! Set BC ghost values
    void applyTurbKEBCs(const Real t, Real* var, Real* varg, bool* bcflag,
                        bool exchange);

    //! Calculate the edge viscosity
    //!   \param[in]  sigma  Diffusion coefficient parameter
    //!   \param[in]  mu     Pointer to the diffusion coefficient array
    //!   \param[in]  mut    Pointer to the turbulent diffusion coefficient array
    //!   \param[in]  mueff  Pointer to the effective diffusion coefficient array
    //!   \param[out] edgemu Pointer to the edge diffusion coefficient array
    void calcEdgeViscosity(const Real sigma, Real* mu, Real* mut, Real* mueff,
                           Real* edgemu);

    //! Form RHS function for e-equation
    //!   \param[in] incParm Reference to time increment parameters
    //!   \param[in,out] rhs Pointer to the rhs vector
    void formRhsEps(const CCINSIncParm& incParm, Real* rhs);

    //! Form incremental part of function for e-equation
    //!   \param[in] incParm Reference to time increment parameters
    //!   \param[in,out] RHS vector
    void formRhsEpsInc(const CCINSIncParm& incParm, Real* rhs);

    //! Form RHS function for k-equation
    //!   \param[in] incParm Reference to time increment parameters
    //!   \param[in,out] rhs Pointer to the rhs vector
    void formRhsKE(const CCINSIncParm& incParm, Real* rhs);

    //! Set the flag to turn on sources based on
    //!   \param[out] flag Array of flags, set to 1.0 for wall-attached
    //!               elements, and to 0.0 otherwise
    void setWallLayerElements(Real* flag);

    //! \name Data common to all k-epsilon models
    //@{
    int  m_wftype; //!< Wall function type

    Real m_epsCoeff; //!< Coefficient for epsilon limiter
    Real m_sigki;    //!< \f$ 1/\sigma_k \f$ for kinetic energy
    Real m_sigei;    //!< \f$ 1/\sigma_\epsilon \f$ for dissipation rate

    //! Boussinesq body force container class
    vector<CCINSBoussinesqForce*>& m_boussinesq_force;

    //! Boundary condition container classess
    vector<CCINSTemperatureBC*>& m_temperatureBCs;
    vector<CCINSTurbEpsBC*>&     m_turbEpsBCs;
    vector<CCINSTurbKEBC*>&      m_turbKEBCs;
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSKE(const CCINSKE&);
    CCINSKE& operator=(const CCINSKE&);
    //@}

};

}

#endif // CCINSKE_h
