//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSTurbNuBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Nu-tilde BC's for Spalart-Allmaras
//******************************************************************************
#ifndef CCINSTurbNuBC_h
#define CCINSTurbNuBC_h

#include <CCSideSetBC.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;

// Spalart-Allmaras variable BC's (nu-tilde)
class CCINSTurbNuBC : public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSTurbNuBC( const BCPackage& bc, int offset);
    virtual ~CCINSTurbNuBC();
    //@}

    //! Set a user-defined BC value
    virtual Real setUserValue();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSTurbNuBC(const CCINSTurbNuBC&);
    CCINSTurbNuBC& operator=(const CCINSTurbNuBC&);
    //@}

};

}

#endif // CCINSTurbNuBC_h
