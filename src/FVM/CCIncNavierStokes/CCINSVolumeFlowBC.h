//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSVolumeFlowBC.h
//! \author  Mark A. Christon
//! \date    Tue Jul 16, 2016
//! \brief   Volume flow boundary condition based on sidesets
//******************************************************************************
#ifndef CCINSVolumeFlowBC_h
#define CCINSVolumeFlowBC_h

#include <CCSideSetBC.h>

namespace Hydra {

// Forward declarations
struct BCPackage;

//! Volume fraction BC
class CCINSVolumeFlowBC : public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSVolumeFlowBC(const BCPackage& bc, int offset);
    virtual ~CCINSVolumeFlowBC();
    //@}

    //! Set the acceleration in the ghost buffer
    //!   \param[in] mesh     DataMesh
    //!   \param[in] gd       Ghost buffer
    void setAcceleration(UnsMesh& mesh, CVector& gd);

    //! Setup the edge-data for a given BC
    //!   \param[in] mesh     DataMesh
    //!   \param[in] bcflag   Bool flag to be set for Dirichlet BC's
    void setupEdgeBCs(UnsMesh& mesh, CBoolVector& bcflag);

    //! Set the ghost-data for a given symmetry velocity BC
    //!   \param[in] mesh     DataMesh
    //!   \param[in] gd       Ghost buffer
    //!   \param[in] t        Total simulated time for load-curve lookup
    void setGhostBC(UnsMesh& mesh, CVector& gd, Real t);

    //! Set the ghost-data & flag for a given symmetry velocity BC
    //!   \param[in] mesh     DataMesh
    //!   \param[in] bcflag   Bool flag to be set for Dirichlet BC's
    //!   \param[in] gd       Ghost buffer
    //!   \param[in] t        Total simulated time for load-curve lookup
    void setGhostBC(UnsMesh& mesh, CBoolVector& bcflag, CVector& gd, Real t);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSVolumeFlowBC(const CCINSVolumeFlowBC&);
    CCINSVolumeFlowBC& operator=(const CCINSVolumeFlowBC&);
    //@}

};

}

#endif
