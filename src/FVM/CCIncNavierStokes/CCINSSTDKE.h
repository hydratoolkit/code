
//******************************************************************************
/*!
  \file    src/FVM/CCIncNavierStokes/CCINSSTDKE.h
  \author  Mark A. Christon
  \date    Tue Sep 27 13:00:00 2016
  \brief   Standard k-epsilon model
 */
//******************************************************************************
#ifndef CCINSSTDKE_h
#define CCINSSTDKE_h

#include <CCINSKE.h>

namespace Hydra {

class CCINSBoussinesqForce;
class CCINSTemperatureBC;
class CCINSTurbEpsBC;
class CCINSTurbKEBC;

//! Standard K-Epsilon transport equations
class CCINSSTDKE : public CCINSKE {

  public:

    //! Constructor
    CCINSSTDKE(UnsMesh& mesh,
               Control& control,
               DualEdgeGradOp& m_edgeGradOp,
               CCINSErrors& errors,
               fileIO& io,
               CCINSTransportVar& di,
               vector<CCINSTemperatureBC*>& temperatureBC,
               vector<CCINSTurbEpsBC*>& turbEpsBCs,
               vector<CCINSTurbKEBC*>& turbKEBCs,
               vector<CCINSBoussinesqForce*>& boussinesq_force,
               LASolver& transport_solver,
               LAMatrix& transport_A,
               LAVector& transport_diagA,
               LAVector& transport_b,
               LAVector& transport_x,
               CCINSAdapter& adapter);

    //! Destructor
    virtual ~CCINSSTDKE();

    //**************************************************************************
    //! \name Virtual k-epsilon interface
    //! These functions are pure virtual functions that need to be
    //! implemented for each specific of k-epsilon model.
    //@{
    //! Add contributions to the LHS matrix and to the RHS vector due to the
    //! linearized sources for the e-equation
    //! \param[in,out] S       Reference to the LHS matrix
    //! \param[in,out] rhs     Pointer to the RHS vector
    virtual void addSourcesKE(LAMatrix* S, Real* rhs);

    //! Add contributions to the LHS matrix and to the RHS vector due to the
    //! linearized sources for the k-equation
    //! \param[in,out] S       Reference to the LHS matrix
    //! \param[in,out] rhs     Pointer to the RHS vector
    virtual void addSourcesEps(LAMatrix* S, Real* rhs);

    //! Apply the Bouyancy correction
    virtual void applyBuoyancyCorrectionKE(Real* rhs);

    //! Apply the epsilon penalty based on the wall function type
    virtual void applyWallEpsPenalty(LAMatrix& S, LAVector& B, Real* rhs);

    //! Calculate the turbulent viscosity for the K-Eps (STD) model
    virtual void calcTurbulentViscosity();

    //! Calculate y* on a surface
    virtual void calcSurfYstar(const int nel, const int* edge_list, Real* ystar);

    //! Calculate y* in the volume
    virtual void calcYstar(Real* dist, Real* ystar);

    //! Set the wall conductivity according to the specific model
    virtual void setWallConductivity(int nedges, int* edge_list, DualEdge* edges,
                                     Real* kmol, Real* keff, Real* edgek);

    //! Set the wall viscosity according to the specific model
    virtual void setWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                  Real* mu, Real* mueff, Real* edgemu);

    //! Solve generic scalar transport equation(s)
    //! \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);
    //@}
    //**************************************************************************

  protected:

    //! Add compound wall-function sources to the LHS/RHS k-transport eq.
    void addCompoundWallSourcesKE(LAMatrix* S, Real* rhs);

    //! Add compound wall-function sources to the LHS/RHS k-transport eq.
    void addCompoundWallSourcesKEold(LAMatrix* S, Real* rhs);

    //! Add the wall-function source terms to the LHS and RHS
    void addScalableWallSourcesKE(LAMatrix* S, Real* rhs);

    //! Add two-layer wall-function sources to the LHS/RHS k-transport eq.
    void addTwolayerWallSourcesKE(LAMatrix* S, Real* rhs);

    //! Apply the epsilon penalty for compound wall function
    void applyCompoundWallEpsPenalty(LAMatrix& S, LAVector& B, Real* rhs);

    //! Apply the epsilon penalty for scalable wall function
    void applyScalableWallEpsPenalty(LAMatrix& S, LAVector& B, Real* rhs);

    //! Apply the epsilon penalty for scalable wall function
    void applyTwolayerWallEpsPenalty(LAMatrix& S, LAVector& B, Real* rhs);

    //! Calculate the wall viscosity for compound wall treatment
    void calcCompoundWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                   Real* mu, Real* mueff, Real* edgemu);

    //! Calculate the wall viscosity for scalable wall functions
    void calcScalableWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                   Real* mu, Real* mueff, Real* edgemu);

    //! Calculate the wall viscosity for two-layer wall functions
    void calcTwolayerWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                   Real* mu, Real* mueff, Real* edgemu);

    //! Compute integral average for Kader functions
    void intKader(const Real a, const Real b, const Real ystar,
                  Real& eGamma, Real& eoGamma );

    //! Compute mean dissipation rate
    void meanEpsilon(Real tke, Real rho, Real mu, Real Aeps, Real kappa,
                     Real yn, Real& epsbar);

    //! Compute mean Kader weight
    void meanKader(const Real a, const Real b, const Real ystar, Real& ebar);

  private:

    // Don't permit copy or assignment operators
    CCINSSTDKE(const CCINSSTDKE&);
    CCINSSTDKE& operator=(const CCINSSTDKE&);

    //**************************************************************************
    //! \name Model constants/parameters
    //@{
    Real m_Cmu;
    Real m_kappa;
    Real m_E;
    Real m_Cmu14;
    Real m_Cmu12;
    Real m_Cmu34;
    Real m_ln2;
    Real m_yp11;
    Real m_c1eps;
    Real m_c2eps;
    Real m_alpha;
    //@}
    //**************************************************************************

};

}

#endif // CCINSSTDKE_h
