//******************************************************************************
/*!
  \file    src/FVM/CCIncNavierStokes/CCINSVolFrac.h
  \author  Mark A. Christon
  \date    Mon Jul 31 17:36:30 MDT 2017
  \brief   Solve the volume-fraction transport equation
 */
//******************************************************************************
#ifndef CCINSVolFrac_h
#define CCINSVolFrac_h

#include <CCINSTransport.h>
#include <CCINSVolFracBC.h>

namespace Hydra {

  class CCINSVolFrac : public CCINSTransport {

  public:

    //! Constructor
    CCINSVolFrac(UnsMesh& mesh,
                 Control& control,
                 DualEdgeGradOp& edgeGradOp,
                 CCINSErrors& errors,
                 fileIO& io,
                 CCINSTransportVar& di,
                 vector<CCINSVolFracBC*>& volfracBCs,
                 LASolver& transport_solver,
                 LAMatrix& transport_A,
                 LAVector& transport_b,
                 LAVector& transport_x,
                 CCINSAdapter& adapter);

    //! Destructor
    virtual ~CCINSVolFrac();

    //! Assemble generic scalar transport equation
    //! \param[in] CCINSIncParm Time increment parameters
    virtual void assembleSystem(const CCINSIncParm& incParm);

    //! Virtual form transport Rhs function
    //! \param[in] CCINSIncParm Time increment parameters
    //! \param[in] SourceMode   Time-weighting/time-level for source terms
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs=0);


    //! Solve generic scalar transport equation
    //! \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);

    //! Apply prescribed surface volume fraction BCs
    //! \param[in] t         Time
    //! \param[in,out] var   Scalar field to set BCs on
    //! \param[in,out] varg  Ghost array for scalar field to set BCs on
    //! \param[in] mode      Flag, indicating the time marching scheme
    //! \param[in] exchange  Flag, indicating if communication is needed
    void applyVolFracBCs(const Real t,
                         Real* var,
                         Real* varg,
                         bool* bcflag,
                         const bool exchange = true);

    /**************************************************************************/

  private:

    // Don't permit copy or assignment operators
    CCINSVolFrac(const CCINSVolFrac&);
    CCINSVolFrac& operator=(const CCINSVolFrac&);

    //! Volume fraction BCs for one field
    vector<CCINSVolFracBC*>& m_volfracBCs;

    //**************************************************************************
    //! \name Linear Algebra
    //@{
    LASolver& m_transport_solver; //!< Linear solver
    LAMatrix& m_transport_A;      //!< Operator
    LAVector& m_transport_b;      //!< Right-hand-side
    LAVector& m_transport_x;      //!< Solution vector
    //@}
  };

}

#endif // CCINSVolFrac_h
