//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSBlockICs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Block-based IC's for the hybrid Navier-Stokes solver
//******************************************************************************

#ifndef CCINSBlockICs_h
#define CCINSBlockICs_h

#include <HydraTypes.h>
#include <Block.h>

namespace Hydra {

//! CCINS Block Velocity Initial Conditions
class CCINSBlockICs : public Block {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSBlockICs() {}
    virtual ~CCINSBlockICs() {}
    //@}

    //! \name CCINSBlockICs access functions
    //! The CCINSBlockICs class is used to setup physics-specific
    //! block initial velocity conditions.
    //@{
    void setTemp(Real T) {temp = T;}
    void setVelIC(Real* vel) {velic[0] = vel[0];
                              velic[1] = vel[1];
                              velic[2] = vel[2];}

    Real  getTemp()  {return temp;}
    Real* getVelIC() {return velic;}
    Real  getVelx()  {return velic[0];}
    Real  getVely()  {return velic[1];}
    Real  getVelz()  {return velic[2];}
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSBlockICs(const CCINSBlockICs&);
    CCINSBlockICs& operator=(const CCINSBlockICs&);
    //@}

    Real velic[3];
    Real temp;
};

}

#endif // CCINSBlockICs_h
