//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSErrorTypes.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Error types for cell-centered incompressible flow
//******************************************************************************
#ifndef  CCINSErrorTypes_h
#define  CCINSErrorTypes_h

namespace Hydra {

//! Basic solver errors
enum CCINSSolverError {
  CCINS_XMOMENTUM_CONVERGENCE_ERROR=0,
  CCINS_YMOMENTUM_CONVERGENCE_ERROR,
  CCINS_ZMOMENTUM_CONVERGENCE_ERROR,
  CCINS_BMATRIX_QUADRATURE_ERROR,
  CCINS_PPE_QUADRATURE_ERROR,
  CCINS_PPE_CONVERGENCE_ERROR,
  CCINS_ENERGY_CONVERGENCE_ERROR,
  CCINS_SPECIESMASSFRAC_CONVERGENCE_ERROR,
  CCINS_VOLFRAC_CONVERGENCE_ERROR,
  CCINS_LSGRADIENT_ERROR,
  CCINS_TOTAL_ERRORS
};


//! Turbulence model errors
enum CCINSTurbError {
  CCINSTurb_DISTANCE_FUNCTION_QUADRATURE_ERROR = 0,
  CCINSTurb_DISTANCE_FUNCTION_CONVERGENCE_ERROR,
  CCINSTurb_SA_CONVERGENCE_ERROR,
  CCINSTurb_DES_CONVERGENCE_ERROR,
  CCINSTurb_TKE_CONVERGENCE_ERROR,
  CCINSTurb_EPS_CONVERGENCE_ERROR,
  CCINSTurb_NEGATIVE_EPS,
  CCINSTurb_TOTAL_ERRORS
};

//! Deforming mesh errors
enum CCINSMeshDeformError {
  UNSMESH_ERROR_1=0,
  UNSMESH_ERROR_2,
  UNSMESH_TOTAL_ERRORS
};

}

#endif // CCINSErrorTypes_h
