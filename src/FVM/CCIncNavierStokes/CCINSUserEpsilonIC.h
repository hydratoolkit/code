//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSUserEpsilonIC.h
//! \author  Mark A. Christon
//! \date    Tue Sep  5 15:44:10 MDT 2017
//! \brief   User-defined epsilon IC's
//******************************************************************************
#ifndef CCINSUserEpsilonIC_h
#define CCINSUserEpsilonIC_h

#include <CCINSFlow.h>
#include <UserIC.h>

namespace Hydra {

//! User-defined epsilon IC
class CCINSUserEpsilonIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSUserEpsilonIC();
    virtual ~CCINSUserEpsilonIC();
    //@}

    //! Virtual function to setup IC's using a mesh and set of DataIndices
    void setICs(UnsMesh* mesh, const DataIndex TURB_EPS);

    //! Set user-defined temperature initial conditions
    void setUserEpsilon();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSUserEpsilonIC(const CCINSUserEpsilonIC&);
    CCINSUserEpsilonIC& operator=(const CCINSUserEpsilonIC&);
    //@}

    //! \name Data presented for the user-defined epsilon function
    //@{
    int m_elemId;      //!< Element Id for user access
    int m_matId;       //!< Material Id for user access
    Vector m_coord;    //!< Element centroid coordinates
    Real   m_epsilon;  //!< Element epsilon
    Material* m_mat;   //!< Material class for user access
    //@}
};

}

#endif // CCINSUserEpsilonIC_h
