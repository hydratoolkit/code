//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSPorousDrag.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Porous drag body force
//******************************************************************************
#ifndef CCINSPorousDrag_h
#define CCINSPorousDrag_h


#include <HydraTypes.h>
#include <Source.h>
#include <CCINSFlow.h>


namespace Hydra {

class UnsMesh;

//! Porous drag body force
class CCINSPorousDrag : public Source {

  public:

    //! \name Constructor/Destructor
    //@{
            CCINSPorousDrag();
            CCINSPorousDrag(int tblid, Real amp, int setid) :
                            Source(tblid, amp, setid) {}
    virtual ~CCINSPorousDrag() {}
    //@}

    //! Calculate the advective weights for porous media flows
    void adjustAdvectiveWeights(UnsMesh& mesh, const Real* rho,
                                Real* wtAdvect, Real time);

    //! Calculate the weights for porous media flows
    void adjustLambdaWeights(UnsMesh& mesh, Real* wtLambda, Real time);

    //! Apply the Porous drag force to the LHS of the momentum equations
    void apply(bool /* deformableMesh */, UnsMesh& mesh, const Real* rho,
               const Real* mu, Real* porosity,
               LAMatrix& K, CVector& veln, Real thetaFnp1,
               Real timenp1, Real dt, DataIndex VOLUME);

    //! Apply the Porous drag force to the RHS of the momentum equations
    void apply(bool deformableMesh, UnsMesh& mesh, const Real* rho,
               const Real* mu, Real* porosity,
               CVector& rhs, CVector& veln, Real thetaFn,
               Real timen, DataIndex VOLUME, DataIndex VOLUMEN);

    //! Apply the body force for the partial acceleration calculation
    void apply(UnsMesh& mesh, CVector& rhs,CVector& vel,
               Real* porosity, const Real* rho, const Real* mu,
               const Real* V,  Real time);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSPorousDrag(const CCINSPorousDrag&);
    CCINSPorousDrag& operator=(const CCINSPorousDrag&);
    //@}
};

}

#endif
