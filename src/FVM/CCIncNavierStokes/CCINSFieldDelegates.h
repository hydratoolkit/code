//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSFieldDelegates.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Field delegates for incompressible flow
//******************************************************************************
#ifndef CCINSFieldDelegates_h
#define CCINSFieldDelegates_h

#include <FieldStatistics.h>
#include <CCINSFlow.h>

namespace Hydra {

//! Declaration of the statistics field accumulators

ACCUMULATOR_DECL(CCINSFlowAccumulateTKE)
ACCUMULATOR_DECL(CCINSFlowAccumulateReynoldsStress)
ACCUMULATOR_DECL(CCINSFlowAccumulateMeanVorticity)
ACCUMULATOR_DECL(CCINSFlowAccumulateMeanEnstrophy)
ACCUMULATOR_DECL(CCINSFlowAccumulateMeanHelicity)
ACCUMULATOR_DECL(CCINSFlowAccumulateMeanTurbViscosity)

//! Declaration of the element field delegates

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemCFLField,
                     CCINSFlowCFLField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemCMUField,
                     CCINSFlowElemCMUField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemCovDensPresField,
                     CCINSFlowElemCovDensPresField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemCovPresVelField,
                     CCINSFlowElemCovPresVelField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemEddyViscosityField,
                     CCINSFlowElemEddyViscosityField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemLepsField,
                     CCINSFlowElemLepsField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemReyField,
                     CCINSFlowElemReyField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemViscosityField,
                     CCINSFlowElemViscosityField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemYstarField,
                     CCINSFlowElemYstarField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemEnstrophyField,
                     CCINSFlowElemEnstrophyField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeEnthalpyField,
                     CCINSFlowNodeEnthalpyField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemHelicityField,
                     CCINSFlowElemHelicityField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemLinReynoldStress,
                     CCINSFlowElemLinReynoldStress)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemLQCField,
                     CCINSFlowElemLQCField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemMeanDensityField,
                     CCINSFlowElemMeanDensityField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemMeanEnstrophyField,
                     CCINSFlowElemMeanEnstrophyField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemMeanHelicityField,
                     CCINSFlowElemMeanHelicityField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemMeanPressureField,
                     CCINSFlowElemMeanPressureField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemMeanTemperatureField,
                     CCINSFlowElemMeanTemperatureField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemMeanTurbnuField,
                     CCINSFlowElemMeanTurbnuField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemMeanVelocityVectorField,
                     CCINSFlowElemMeanVelocityVectorField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemMeanVorticityVectorField,
                     CCINSFlowElemMeanVorticityVectorField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemPKField,
                     CCINSFlowElemPKField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemPressureField,
                     CCINSFlowElemPressureField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemPresVarianceField,
                     CCINSFlowElemPresVarianceField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemQCriteriaField,
                     CCINSFlowElemQCriteriaField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemQuadReynoldStress,
                     CCINSFlowElemQuadReynoldStress)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemReynoldsStressField,
                     CCINSFlowElemReynoldsStressField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemRMSPressureField,
                     CCINSFlowElemRMSPressureField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemRMSTemperatureField,
                     CCINSFlowElemRMSTemperatureField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemTempVarianceField,
                     CCINSFlowElemTempVarianceField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemTKEField,
                     CCINSFlowElemTKEField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemTotReynoldStress,
                     CCINSFlowElemTotReynoldStress)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemVelGrad,
                     CCINSFlowElemVelGrad)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemVorticityVectorField,
                     CCINSFlowElemVorticityVectorField)

// Declaration of the nodal field delegates

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeCovDensPresField,
                     CCINSFlowNodeCovDensPresField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeCovPresVelField,
                     CCINSFlowNodeCovPresVelField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeDensityField,
                     CCINSFlowNodeDensityField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeEnstrophyField,
                     CCINSFlowNodeEnstrophyField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeHelicityField,
                     CCINSFlowNodeHelicityField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeIntEnergyField,
                     CCINSFlowNodeIntEnergyField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeMassFracField,
                     CCINSFlowNodeMassFracField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeMeanDensityField,
                     CCINSFlowNodeMeanDensityField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeMeanEnstrophyField,
                     CCINSFlowNodeMeanEnstrophyField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeMeanHelicityField,
                     CCINSFlowNodeMeanHelicityField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeMeanPressureField,
                     CCINSFlowNodeMeanPressureField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeMeanTemperatureField,
                     CCINSFlowNodeMeanTemperatureField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeMeanVelocityVectorField,
                     CCINSFlowNodeMeanVelocityVectorField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeMeanVorticityVectorField,
                     CCINSFlowNodeMeanVorticityVectorField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodePressureField,
                     CCINSFlowNodePressureField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodePresVarianceField,
                     CCINSFlowNodePresVarianceField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeQCriteriaField,
                     CCINSFlowNodeQCriteriaField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeRMSPressureField,
                     CCINSFlowNodeRMSPressureField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeRMSTemepratureField,
                     CCINSFlowNodeRMSTemperatureField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeReynoldsStressField,
                     CCINSFlowNodeReynoldsStressField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeTempVarianceField,
                     CCINSFlowNodeTempVarianceField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeTKEField,
                     CCINSFlowNodeTKEField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeTemperatureField,
                     CCINSFlowNodeTemperatureField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeElemTurbDistField,
                     CCINSFlowElemTurbDistField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeTurbEpsField,
                     CCINSFlowNodeTurbEpsField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeTurbKeField,
                     CCINSFlowNodeTurbKeField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeTurbNuField,
                     CCINSFlowNodeTurbNuField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeTurbOmegaField,
                     CCINSFlowNodeTurbOmegaField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeVelocityVectorField,
                     CCINSFlowNodeVelocityVectorField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeVolFracField,
                     CCINSFlowNodeVolFracField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeNodeVorticityVectorField,
                     CCINSFlowNodeVorticityVectorField)

} // Hydra namespace

#endif // CCINSFieldDelegates.h
