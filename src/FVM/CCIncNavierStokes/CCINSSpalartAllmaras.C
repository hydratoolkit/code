//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSSpalartAllmaras.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:23 2011
//! \brief   The Spalart-Allmaras turbulence model for incompressible flow
//******************************************************************************
#include <cassert>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

#include <Category.h>
#include <FVMCCINSControl.h>
#include <SpalartAllmarasControl.h>
#include <macros.h>
#include <MathOp.h>
#include <ArrayLimits.h>
#include <MPIWrapper.h>
#include <CCINSSpalartAllmaras.h>
#include <CCINSTurbNuBC.h>

using namespace Hydra;
using namespace Hydra::MathOp;
using namespace Hydra::ArrayLimits;

void
CCINSSpalartAllmaras::solve(const CCINSIncParm& incParm)
/*******************************************************************************
Routine: solve - solve the Spalart-Allmaras transport equation
Author : Mark A. Christon
*******************************************************************************/
{
  Real* nut    = m_mesh.getVariable<Real>(m_di.TURB_NU);
  Real* eqVar  = m_mesh.getVariable<Real>(m_di.ELEMEQ_VAR);

  assembleSystem(incParm);

  //Solve the transport equation
  m_transport_solver.solve(&m_transport_b, &m_transport_x);

  // Copy-back the solution
  memcpy(nut, eqVar, m_Nel*sizeof(Real));

  // Check status
  LASolverStatus status;
  m_transport_solver.getStatus(status);

  if(m_pid==0)
  status.echoResults(cout,"-- S-A Transport Model Equation --");

  if(!status.isConverged()) {
    m_io.writeWarning(" Spalart-Allmaras model may not have fully converged");
    m_errors.addError(CCINSTurb_SA_CONVERGENCE_ERROR);
  }

}

CCINSSpalartAllmaras::CCINSSpalartAllmaras(UnsMesh& mesh,
                                           Control& control,
                                           DualEdgeGradOp& edgeGradOp,
                                           CCINSErrors& errors,
                                           fileIO& io,
                                           CCINSTransportVar& di,
                                           vector<CCINSTurbNuBC*>& turbNuBCs,
                                           LASolver& transport_solver,
                                           LAMatrix& transport_A,
                                           LAVector& transport_diagA,
                                           LAVector& transport_b,
                                           LAVector& transport_x,
                                           CCINSAdapter& adapter):
  CCINSTurbulence(mesh, control, edgeGradOp, errors, io, di, transport_solver,
                  transport_A, transport_diagA, transport_b, transport_x,
                  adapter),
  m_turbNuBCs(turbNuBCs)
/*******************************************************************************
Routine: CCINSSpalartAllmaras - constructor
Author : Mark A. Christon
*******************************************************************************/
{
  Category* pcat = m_control.getCategory(FVM_CC_NAVIERSTOKES_CATEGORY);
  Category* tcat = pcat->getCategory(FVMCCINSControl::SPALART_ALLMARAS_CAT);

  m_DESmode = tcat->getFlagVal(SpalartAllmarasControl::DETACHED_EDDY_MODE);
  m_curvCorr = tcat->getFlagVal(SpalartAllmarasControl::CURVATURE_CORRECTION);
  m_cdes    = tcat->getParamVal(SpalartAllmarasControl::C_DES);
  m_Cb1     = tcat->getParamVal(SpalartAllmarasControl::C_B1);
  m_Cb2     = tcat->getParamVal(SpalartAllmarasControl::C_B2);
  m_Cv1     = tcat->getParamVal(SpalartAllmarasControl::C_V1);
  m_Cw2     = tcat->getParamVal(SpalartAllmarasControl::C_W2);
  m_Cw3     = tcat->getParamVal(SpalartAllmarasControl::C_W3);
  m_kappa   = tcat->getParamVal(SpalartAllmarasControl::KAPPA);
  m_sigma   = tcat->getParamVal(SpalartAllmarasControl::SIGMA);
  m_Prt     = tcat->getParamVal(SpalartAllmarasControl::PRANDTL);

  m_Cw1     = m_Cb1/(m_kappa*m_kappa) + (1.0 + m_Cb2)/m_sigma;
  m_Cv13    = m_Cv1*m_Cv1*m_Cv1;
  m_Cv16    = m_Cv13*m_Cv13;
  m_Cw36    = m_Cw3*m_Cw3*m_Cw3*m_Cw3*m_Cw3*m_Cw3;
  m_C1      = (1.0 + m_Cb2)/m_sigma;
  m_C2      = m_Cb2/m_sigma;
  m_o3rd    = 1.0/3.0;
  m_o6th    = 1.0/6.0;
}

CCINSSpalartAllmaras::~CCINSSpalartAllmaras()
/*******************************************************************************
Routine: ~CCINSSpalartAllmaras - destructor
Author : Mark A. Christon
*******************************************************************************/
{}

void
CCINSSpalartAllmaras::addSources(LAMatrix* S, Real* rhs)
/*******************************************************************************
Routine: addSources - Add source terms to the LHS matrix and to the RHS
Author : Mark A. Christon
*******************************************************************************/
{
  // Model constants
  Real r_cutoff = 10.0;
  Real kappa2   = m_kappa*m_kappa;

  Real* rho  = m_mesh.getVariable<Real>(m_di.DENSITY);
  DataIndex ELEM_VISCOSITY = m_di.ELEM_MOL_VISCOSITY;
  if (m_variableViscosity)
    ELEM_VISCOSITY = m_di.ELEM_MOL_VISCOSITYN;
  Real* mu  = m_mesh.getVariable<Real>(ELEM_VISCOSITY);
  Real* nut = m_mesh.getVariable<Real>(m_di.TURB_NUN);
  Real* V   = m_mesh.getVariable<Real>(m_di.VOLUME);
  Real* d   = m_mesh.getVariable<Real>(m_di.TEMP_ELEM);

  // Calculate the normal distance at the cell-element
  // Be carefull do not overwrite the TEMP_ELEM array!
  calcElementNormalDistance(m_di.TURB_DIST, m_di.TEMP_ELEM);

  // DES mode: modify length scale of Spalart-Allmaras based on distance to wall
  // and element size, i.e. use Detached Eddy Simulation
  if(m_DESmode) {
    int Nec = m_mesh.numElementClass();
    for (int n = 0; n < Nec; n++) {
      Element* ec = m_mesh.getElementClass(n);
      Material* mat = m_mesh.getMaterial(ec->getMatId());
      int nel = ec->getNel();
      if (mat->isFluid() && nel > 0) {
        int* gids = ec->getGlobalIds();
        for(int i=0;i<nel;i++) {
          int gid = gids[i];
          d[gid] = amin(d[gid], m_cdes*pow(V[gid],m_o3rd));
        }
      }
    }
  }

  // Correction for ALE w. finite mesh deformation
  Real* Vn = 0;
  if(m_deformableMesh)
    Vn = m_mesh.getVariable<Real>(m_di.VOLUMEN);

  // Element velocity gradient is computed at the end of each time-step
  CTensor uij = m_mesh.getCTensor(m_di.ELEM_TENSOR);

  // Trip over elements to compute the LHS and RHS source terms
  int Nec = m_mesh.numElementClass();
  for (int n = 0; n < Nec; n++) {
    Element* ec = m_mesh.getElementClass(n);
    Material* mat = m_mesh.getMaterial(ec->getMatId());
    int nel = ec->getNel();
    if (mat->isFluid() && nel > 0) {
      int* gids = ec->getGlobalIds();
      for(int i=0;i<nel;i++) {

        int gid = gids[i];
        Real X  = nut[gid]*rho[gid]/mu[gid];
        Real X3 = pow(X,3);
        Real X4 = pow(X,4);
        Real X6 = pow(X,6);

        Real od2   = 1.0/(d[gid]*d[gid]);
        Real oK2d2 = od2/(kappa2);

        // Calculate the ani-symmetric Rij tensor and it's magnitude
        AntiTensor Rij;
        antisymmetric(gid, uij, Rij);
        Real Sr;
        normL2(2.0, Rij, Sr);

        if(m_curvCorr) {
          SymTensor Sij;
          symmetric(gid, uij, Sij);
          Real Ss;
          normL2(2.0, Sij, Ss);
          Sr += 2.0*amin(0.0, Ss-Sr);
        }

        Real fv1 = X3/(X3+m_Cv13);

        Real fv2 = 1.0 - X/(1.0 + X*fv1);

        Real Sa = Sr + nut[gid]*fv2*oK2d2;

        // Correction to bound r positive ala S-A paper
        Real r = nut[gid]*oK2d2/fabs(Sa);
        if (r > r_cutoff) r = r_cutoff;

        Real g = r*(1.0 - m_Cw2) + m_Cw2*pow(r,6);

        Real gfrac = (1.0 + m_Cw36)/(pow(g,6) + m_Cw36);

        Real fw = g*pow(gfrac,m_o6th);

        Real denom = X4 + X3 + m_Cv13;

        Real fv2p = rho[gid]*(-X6 + 3.0*m_Cv13*X4 - 2.0*m_Cv13*X3 - m_Cv16)/
                             (mu[gid]*denom*denom);

        Real Sap  = oK2d2*(fv2 + nut[gid]*fv2p);

        Real rp = r*(1.0 - nut[gid]*Sap/Sa)/nut[gid];

        Real gp = rp*(1.0 + m_Cw2*(6.0*pow(r,5) - 1.0));

        Real fwp = fw*gp*(m_Cw36/(m_Cw36 + pow(g,6)))/g;

        Real rhoV = rho[gid]*V[gid];

        Real P = rhoV*m_Cb1*Sa;

        Real D = rhoV*m_Cw1*fw*nut[gid]*od2;

        Real Pp = rhoV*m_Cb1*Sap;

        Real Dp = rhoV*od2*m_Cw1*(fwp*nut[gid] + fw);

        Real DmP = D - P;

        Real DbmPb = amax(0.0, (DmP)) + amax(0.0, (Dp - Pp))*nut[gid];

        // Account for the ALE contributions to the source terms.
        if (m_deformableMesh)
          DmP *= (m_thetaFnp1 + m_thetaFn*(Vn[gid]/V[gid]));

        // Definitions:
        // m_thetaFn = 1 - \theta
        // m_thetaFnp1 = \theta
        // LHS contribution
        Real aii = m_thetaFnp1*m_dt*DbmPb;
        int eqId = gid + m_elEqIDOffset;
        S->add(1, &eqId, 1, &eqId, &aii);

        // RHS contribution
        rhs[gid] += m_dt*(m_thetaFnp1*DbmPb - DmP)*nut[gid];

      } // End of loop over elements
    }
  }
}

void
CCINSSpalartAllmaras::applyTurbNuBCs(Real t, Real* var, Real* varg,
                                     bool* bcflag, bool exchange)
/*******************************************************************************
Routine: applyTurbNuBCs - Apply TURB_NU BCs and get flags for LHS operator
Author : Mark A. Christon
*******************************************************************************/
{
  DualEdge* edges = m_mesh.getDualEdges();

  //First setup default data on external edges from initial data
  for(int i=0; i<m_Nedge_ifc;i++) {
    varg[i] = var[edges[i].gid1];
    bcflag[i] = false;
  }

  // Process BC's
  int nbc = m_turbNuBCs.size();
  for (int ibc = 0; ibc < nbc; ibc++) {
    CCINSTurbNuBC* bc = m_turbNuBCs[ibc];
    bc->setEdgeBCs(m_mesh, bcflag, varg, t);
  }

  // Swap boundary-data as necessary
  if (m_Nproc > 1 && exchange) swapGhostVar(var, varg);
}

void
CCINSSpalartAllmaras::assembleSystem(const CCINSIncParm& incParm)
/*******************************************************************************
Routine: solve - solve the Spalart-Allmaras transport equation
Author : Mark A. Christon
*******************************************************************************/
{
  Real* nut    = m_mesh.getVariable<Real>(m_di.TURB_NU);
  Real* gnut   = m_mesh.getVariable<Real>(m_di.GHOST_VAR);
  Real* rhs    = m_mesh.getVariable<Real>(m_di.ELEM_RHS);
  if (m_deformableMesh) rhs = m_mesh.getVariable<Real>(m_di.ELEM_SCALAR_RHS);
  Real* rho    = m_mesh.getVariable<Real>(m_di.DENSITY);
  Real* eqVar  = m_mesh.getVariable<Real>(m_di.ELEMEQ_VAR);

  bool* nutflg = m_mesh.getVariable<bool>(m_di.VEL_BC);

  // Set the incrementation parameters
  setIncParm(incParm);

  // Set pointers for the diffusion coefficient for S-A: (mu + mu_t)
  Real* mu     = m_mesh.getVariable<Real>(m_di.ELEM_MOL_VISCOSITY);
  Real* mueff  = m_mesh.getVariable<Real>(m_di.TMP);
  Real* edgemu = m_mesh.getVariable<Real>(m_di.EDGE_VISCOSITY);

  // Need to form the right-hand-side if it is not deformable.
  // Right-hand-side already computed for deformable case.
  if (!m_deformableMesh) {
    Category* pcat = m_control.getCategory(FVM_CC_NAVIERSTOKES_CATEGORY);
    int method = pcat->getOptionVal(FVMCCINSControl::SOLUTION_STRATEGY);
    switch(method) {
    case FVMCCINS::PROJECTION:
      // Need to form the right-hand-side
      formRhs(incParm, FVMCCINS::TIME_LEVEL_WEIGHTED, rhs);
      break;
    case FVMCCINS::PICARD:
      // Add the inremental part of the RHS
      formRhsInc(incParm);
      break;
    }
  }

  // Calculate the edge version of mueff -- this is repeated here
  // since this is in the current configuration, and requires the
  // new weights for interpolation be used.
  calcEdgeViscosity(rho, mu, nut, mueff, edgemu);

  // Copy the RHS for the deformable case
  if(m_deformableMesh) {
    Real* rhse = m_mesh.getVariable<Real>(m_di.TURB_NU_RHS);
    memcpy(rhs, rhse, m_Nel*sizeof(Real));
  }

  // Initialize LHS matrix with zeros
  m_transport_A.zero();

  // form the diagonal Mass terms
  formMassLhs(rho, m_transport_A);

  // Add in the Lhs advective component
  addScalarAdvLhs(rho, m_transport_A);

  // Add diffusive terms to the LHS
  m_edgeGradOp.addDiffusiveLhs(m_C1, m_C2, m_thetaKnp1, m_dt, mueff,
                               edgemu, m_transport_A);

  // Add in the yucky source terms;
  addSources(&m_transport_A, rhs);

  // Set BC ghost values at time level n+1 for
  // LHS without incurring communications
  applyTurbNuBCs(m_time_np1, nut, gnut, nutflg, false);

  // Adjust the right hand side for Dirichlet BC's
  m_edgeGradOp.adjustRhs(m_C1, m_C2, m_thetaKnp1, m_dt, mueff, edgemu,
                         nutflg, gnut, rhs, m_di.GHOST_NU, m_transport_b);

  // Modify the left hand side
  m_edgeGradOp.adjustLhs(m_C1, m_C2, m_thetaKnp1, m_dt, mueff, edgemu,
                         nutflg, m_di.GHOST_NU, m_transport_A);

  // Modify the LHS and RHS due to implict advection treatment.
  adjustAdvectVar(rho, nutflg, gnut, m_transport_A, rhs);

  // Copy-in the old-solution to eqVar
  memcpy(eqVar, nut, m_Nel*sizeof(Real));

  //Assemble the operator and the vectors
  m_transport_x.assemble();
  m_transport_b.assemble();
  m_transport_A.assemble();
  m_transport_solver.setOperator(&m_transport_A);

  // penalty enforcement in rigid material for CHT
  applyRigidElementPenalty(m_transport_A, m_transport_diagA, m_di.TEMP_NODE,rhs);
}

void
CCINSSpalartAllmaras::calcEdgeViscosity(Real* rho, Real* mu, Real* nut,
                                        Real* mueff, Real* edgemu)
/*******************************************************************************
Routine: calcEdgeViscosity - calculate edge viscosity for all edges
Author : Mark A. Christon
*******************************************************************************/
{
  memset(mueff, 0, m_Nel*sizeof(Real));

  // Calculate the viscosity coefficient for LHS
  int Nec = m_mesh.numElementClass();
  for (int n = 0; n < Nec; n++) {
    Element* ec = m_mesh.getElementClass(n);
    Material* mat = m_mesh.getMaterial(ec->getMatId());
    int nel = ec->getNel();
    if (mat->isFluid() && nel > 0) {
      int* gids = ec->getGlobalIds();
      for(int i=0;i<nel;i++) {
        int gid = gids[i];
        mueff[gid] = (mu[gid] + rho[gid]*nut[gid]);
      }
    }
  }

  // Calculate the edge viscosity accounting for fluid-solid interfaces
  calcEdgeMatPropIFC(mueff, edgemu);

}

void
CCINSSpalartAllmaras::calcTurbulentViscosity()
/*******************************************************************************
Routine: calcTurbulentViscosity - turbulent viscosity for Spalart-Allmaras
Author : Mark A. Christon
*******************************************************************************/
{
  Real* rho  = m_mesh.getVariable<Real>(m_di.DENSITY);
  Real* ntsa = m_mesh.getVariable<Real>(m_di.TURB_NU);
  Real* mu   = m_mesh.getVariable<Real>(m_di.ELEM_MOL_VISCOSITY);
  Real* mut  = m_mesh.getVariable<Real>(m_di.ELEM_TURB_VISCOSITY);

  const Real dbleps = numeric_limits<Real>::epsilon();

  // Model coefficients
  Real cv13 = m_Cv1*m_Cv1*m_Cv1;

  Real numol, nute, chi3, fv1, chi;

  // initialize mut so that value in rigid elements is zero
  memset(mut, 0, m_Nel*sizeof(Real));

  int Nec = m_mesh.numElementClass();
  for (int n = 0; n < Nec; n++) {
    Element* ec = m_mesh.getElementClass(n);
    Material* mat = m_mesh.getMaterial(ec->getMatId());
    int nel = ec->getNel();
    if (mat->isFluid() && nel > 0) {
      int* gids = ec->getGlobalIds();
      for(int i=0;i<nel;i++) {
        int gid = gids[i];
        numol  = mu[gid]/rho[gid];
        nute   = ntsa[gid];
        chi    = nute/numol;
        chi3   = chi*chi*chi;
        fv1    = chi3/(chi3 + cv13);
        mut[gid] = fv1*nute*rho[gid];
        // Clip SA variable and make viscosity positive.
        // This seems to be required for very rough, coarse meshes. (MAC)
        // Note: SA_NEGATIVE_VISCOSITY error accumulator is no longer used.
        if (mut[gid] < 0.0) {
          mut[gid] = fabs(mut[gid]);
          ntsa[gid] = dbleps;
        }
      }
    }
  }
}

void
CCINSSpalartAllmaras::formRhs(const CCINSIncParm& incParm,
                              FVMCCINS::SourceMode srcmode,
                              Real* rhs)
/*******************************************************************************
Routine: formRhs - form RHS for the Spalart-Allmaras transport equation
Author : Mark A. Christon
*******************************************************************************/
{
  Real* nutn   = m_mesh.getVariable<Real>(m_di.TURB_NUN);
  Real* gnut   = m_mesh.getVariable<Real>(m_di.GHOST_VAR);
  Real* rho    = m_mesh.getVariable<Real>(m_di.DENSITY);
  bool* nutflg = m_mesh.getVariable<bool>(m_di.VEL_BC);

  CVector edge_grad = m_mesh.getCVector(m_di.EDGE_GRADIENT);

  // Set the incrementation parameters
  setIncParm(incParm);

  // Calculate the edge version of mueff -- this is repeated here
  // since this is in the current configuration, and requires the
  // new weights for interpolation be used.  GHOST_NU is also freshened
  // since it may be out of date.
  Real* mun     = m_mesh.getVariable<Real>(m_di.ELEM_MOL_VISCOSITY);
  Real* mueffn  = m_mesh.getVariable<Real>(m_di.TMP);
  Real* edgemun = m_mesh.getVariable<Real>(m_di.EDGE_VISCOSITY);

  calcEdgeViscosity(rho, mun, nutn, mueffn, edgemun);

  // Set BC ghost values at time level n and conduct communications
  applyTurbNuBCs(m_time, nutn, gnut, nutflg, true);

  // Compute the edge gradient of the transport variable
  m_edgeGradOp.calcGrad(m_restrictOp, nutn, gnut, m_di.EDGE_GRADIENT);

  // Add Mass (M) terms at time level n
  formMassRhs(nutn, rho, rhs);

  // Diffusion terms on the RHS
  m_edgeGradOp.addDiffusiveRhs(m_C1, m_C2, m_thetaKn, m_dt,mueffn,
                               edgemun, nutn, gnut, edge_grad, rhs);

  // Add in the explicit advective component
  addScalarAdvRhs(nutn, gnut, rho, rhs, m_di.DUALEDGE_VELN, srcmode);

}

void
CCINSSpalartAllmaras::formRhsInc(const CCINSIncParm& /* incParm */ , Real*)
/*******************************************************************************
Routine: formRhsInc - form incremmental RHS for the Spalart-Allmaras equation
Author : Mark A. Christon
*******************************************************************************/
{
  Real* rhs    = m_mesh.getVariable<Real>(m_di.ELEM_RHS);
  Real* rhsn   = m_mesh.getVariable<Real>(m_di.TURBNU_RHSN);
  Real* nut    = m_mesh.getVariable<Real>(m_di.TURB_NU);
  Real* gnut   = m_mesh.getVariable<Real>(m_di.GHOST_VAR);
  bool* nutflg = m_mesh.getVariable<bool>(m_di.VEL_BC);
  Real* rho    = m_mesh.getVariable<Real>(m_di.DENSITY);

  // Copy the starting RHS at time-level n for the current iteration
  memcpy(rhs, rhsn, m_Nel*sizeof(Real));

  // Recover the boundary values at n+1
  // Set BC ghost values at time level n+1 and conduct communications
  applyTurbNuBCs(m_time_np1, nut, gnut, nutflg, true);

  // Compute the edge gradient
  m_edgeGradOp.calcGrad(m_restrictOp, nut, gnut, m_di.EDGE_GRADIENT);

  // Add the 2nd-order correction due to advection (slope contribution)
  addScalarAdvRhsInc(nut, gnut, rho, rhs);

}

void
CCINSSpalartAllmaras::setWallConductivity(int nedges, int* edge_list,
                                          DualEdge* edges, Real* kmol,
                                          Real* /*keff*/, Real* edgek)
/*******************************************************************************
Routine: setWallConductivity - Set the wall conductivity
Author : Mark A. Christon
*******************************************************************************/
{
  // S-A model reverts to the molecular conductivity at walls
  for (int i=0; i<nedges; i++) {
    int eid = edge_list[i];
    int gid = edges[eid].gid1;
    edgek[eid] = kmol[gid];
  }
}

void
CCINSSpalartAllmaras::setWallViscosity(int nedges, int* edge_list,
                                       DualEdge* edges, Real* mu,
                                       Real* /*mueff*/, Real* edgemu)
/*******************************************************************************
Routine: setWallViscosity - Set the wall-viscosity the Spalart-Allmaras model
Author : Mark A. Christon
*******************************************************************************/
{
  // S-A model reverts to the molecular viscosity at walls
  for (int i=0; i<nedges; i++) {
    int eid = edge_list[i];
    int gid = edges[eid].gid1;
    edgemu[eid] = mu[gid];
  }
}
