//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSWALE.h
//! \author  Jozsef Bakosi, Mark A. Christon
//! \date    Mon Jan 2 12:18:22 2012
//! \brief   The WALE sub-grid turbulence model for incompressible flow
//******************************************************************************
#ifndef CCINSWALE_h
#define CCINSWALE_h

#include <CCINSTurbulence.h>
#include <Exception.h>

namespace Hydra {

//! Wall-adapted large-eddy (WALE) model for the unresolved scales in LES
class CCINSWALE : public CCINSTurbulence {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSWALE(UnsMesh& mesh,
                       Control& control,
                       DualEdgeGradOp& edgeGradOp,
                       CCINSErrors& errors,
                       fileIO& io,
                       CCINSTransportVar& di,
                       LASolver& transport_solver,
                       LAMatrix& transport_A,
                       LAVector& transport_diagA,
                       LAVector& transport_b,
                       LAVector& transport_x,
                       CCINSAdapter& adapter);
    virtual ~CCINSWALE();
    //@}

    //! Calculate the turbulent viscosity for the WALE model
    virtual void calcTurbulentViscosity();

    //! Virtual form transport Rhs function
    //!   \param[in] CCINSIncParm Time increment parameters
    //!   \param[in] SourceMode   Time-weighting/time-level for source terms
    //! No rhs as no transport equation for eddy viscosity in WALE model
    virtual void formRhs(const CCINSIncParm& /*incParm*/,
                         FVMCCINS::SourceMode /*srcmode*/,
                         Real* /*rhs=0*/) {};

    //! Solve generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    //!   \details No-op: WALE does not solve a transport equation
    virtual void solve(const CCINSIncParm& /*incParm*/) {}

    //! Set the wall viscosity according to the specific model
    virtual void setWallConductivity(int nedges, int* edge_list, DualEdge* edges,
                                     Real* kmol, Real* keff, Real* edgek);

    //! Set the wall viscosity according to the specific model
    virtual void setWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                  Real* mu, Real* mueff, Real* edgemu);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSWALE(const CCINSWALE&);
    CCINSWALE& operator=(const CCINSWALE&);
    //@}
};

}

#endif // CCINSWALE_h
