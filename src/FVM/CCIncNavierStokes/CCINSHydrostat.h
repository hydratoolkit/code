//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSHydrostat.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Hydrostatic pressure, i.e., reference pressure, condition
//******************************************************************************
#ifndef CCINSHydrostat_h
#define CCINSHydrostat_h

#include <HydraTypes.h>
#include <DataShapes.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
struct Vector;
class UnsMesh;
class LAMatrix;

class CCINSHydrostat {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSHydrostat(int setid, int userid, int tblid, Real amp,
                            Vector& coord, bool useCoord);
    virtual ~CCINSHydrostat();
    //@}

    //**************************************************************************
    //! \name Accessors for hydrostatic condition
    //@{
    int  getSetId()              { return m_setid; }
    int  getId()                 { return m_userid; }
    int  getNode()               { return m_node; }
    int  getUserId()             { return m_userId;}
    int  getsetPbc()             { return m_setPbc; }
    int  getTableId()            { return m_tblid; }
    Real getAmp()                { return m_amp; }
    Vector& getCoord()           { return m_coord; }

    void setPbc()                { m_setPbc = true; }
    void setId(int id)           { m_setid = id; }
    void setTableId(int id)      { m_tblid = id; }
    void setAmp(Real val)        { m_amp = val; }
    void setNode(int nodeId)     { m_node = nodeId; }
    void setNode(UnsMesh& mesh);
    //@}

    //! Apply the penalty to the node-based PPE operator for the hydrostat
    void apply(int Nproc, int nodeEqStart, int nodeEqEnd, int* nodeeqmap,
               Real* Adiag);

    //! Apply the penalty to the node-based PPE operator for the hydrostat
    void apply(UnsMesh& mesh, int nodeEqStart, int nodeEqEnd, int* nodeeqmap,
               LAMatrix* A, Real* rhs, Real t, Real scale);

    //! Apply the penalty to the node-based PPE operator for the hydrostat
    void apply(UnsMesh& mesh, int nodeEqStart, int nodeEqEnd, int* nodeeqmap,
               LAMatrix* A, Real* rhs, Real tn, Real tnp1, Real scale);

    //! Adjust the pressure based on the hydrostat
    void adjustPressure(UnsMesh& mesh, Real t, Real* p);

    //! Adjust the pressure based on the hydrostat for history elements
    void adjustPressure(UnsMesh& mesh, int Nhist, Real t, int* gids, Real* p,
                        Real* work);

    //! Calculate the hydrostatic pressure adjustment
    Real calcDeltaP(UnsMesh& mesh, Real t, Real* p);

    //! Evaluate the hydrostatic pressure
    Real hydrostaticPressure(UnsMesh& mesh, Real t);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSHydrostat(const CCINSHydrostat&);
    CCINSHydrostat& operator=(const CCINSHydrostat&);
    //@}

    bool m_useCoord;  //!< Use a nodal coordinate instead of a set
    bool m_setPbc;    //!< Set a pressure BC to remove constant pressure mode
    int  m_node;      //!< Local node for hydrostat
    int  m_userId;    //!< User node Id in serial space
    int  m_setid;     //!< Set Id -- internal set Id a single node
    int  m_userid;    //!< User set Id
    int  m_tblid;     //!< Table Id
    Real m_amp;       //!< Amplitude
    Real m_penalty;   //!< Penalty multiplier
    Vector m_coord;   //!< Nodal coordinate
};

}

#endif
