//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSSpeciesMassFracBC.h
//! \author  Alan K. Stagg, Mark A. Christon
//! \date    2015
//! \brief   Species mass fraction BCs
//******************************************************************************
#ifndef CCINSSpeciesMassFracBC_h
#define CCINSSpeciesMassFracBC_h


#include <CCSideSetBC.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;


//! Species mass fraction boundary condition based on surfaces
class CCINSSpeciesMassFracBC : public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSSpeciesMassFracBC(const BCPackage& bc, int offset);
    virtual ~CCINSSpeciesMassFracBC();
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSSpeciesMassFracBC(const CCINSSpeciesMassFracBC&);
    CCINSSpeciesMassFracBC& operator=(const CCINSSpeciesMassFracBC&);
    //@}

};

}

#endif // CCINSSpeciesMassFracBC_h
