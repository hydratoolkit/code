//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSKsgs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   k-sgs transport equation
//******************************************************************************
#ifndef CCINSKsgs_h
#define CCINSKsgs_h

#include <CCINSTurbulence.h>

namespace Hydra {

class CCINSTurbKEBC;

class CCINSKsgs : public CCINSTurbulence{

  public:

    //! \name Constructor/Destructor
    //@{
            CCINSKsgs(UnsMesh& mesh,
                      Control& control,
                      DualEdgeGradOp& edgeGradOp,
                      CCINSErrors& errors,
                      fileIO& io,
                      CCINSTransportVar& di,
                      vector<CCINSTurbKEBC*>& turbKEBCs,
                      LASolver& transport_solver,
                      LAMatrix& transport_A,
                      LAVector& transport_diagA,
                      LAVector& transport_b,
                      LAVector& transport_x,
                      CCINSAdapter& adapter);
    virtual ~CCINSKsgs();
    //@}

    //! Add the source terms to the LHS and RHS
    virtual void addSources(LAMatrix& S, Real* rhs);

    //! Assemble generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void assembleSystem(const CCINSIncParm& incParm);

    //! Set BC ghost values
    void applyTurbKEBCs(const Real t, Real* var, Real* varg, bool* bcflag,
                        bool exchange);

    //! Calculate the turbulent viscosity for the ksgs model
    virtual void calcTurbulentViscosity() {}

    //! Virtual form transport Rhs function
    //!   \param[in] CCINSIncParm Time increment parameters
    //!   \param[in] SourceMode   Time-weighting/time-level for source terms
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs=0);

    //! Set the wall viscosity according to the specific model
    virtual void setWallConductivity(int /*nedges*/, int* /*edge_list*/,
                                     DualEdge* /*edges*/, Real* /*kmol*/,
                                     Real* /*keff*/, Real* /*edgek*/) {}


    //! Set the wall viscosity according to the specific model
    virtual void setWallViscosity(int /*nedges*/, int* /*edge_list*/,
                                  DualEdge* /*edges*/, Real* /*mu*/,
                                  Real* /*mueff*/, Real* /*edgemu*/) {}

    //! Solve generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);

  protected:

    //! Don't permit copy or assignment operators
    //@{
    CCINSKsgs(const CCINSKsgs&);
    CCINSKsgs& operator=(const CCINSKsgs&);
    //@}

    vector<CCINSTurbKEBC*>& m_turbKEBCs;
};

}

#endif // CCINSKsgs_h
