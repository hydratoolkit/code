//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSRNGKE.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   RNG K-Epsilon transport equations
//******************************************************************************
#ifndef CCINSRNGKE_h
#define CCINSRNGKE_h

#include <CCINSKE.h>

namespace Hydra {

class CCINSBoussinesqForce;
class CCINSSpeciesMassFracBC;
class CCINSTemperatureBC;
class CCINSTurbEpsBC;
class CCINSTurbKEBC;

//! RNG K-Epsilon transport equations
class CCINSRNGKE : public CCINSKE {

  public:

    //! \name Constructor/Destructor
    //@{
            CCINSRNGKE(UnsMesh& mesh,
                       Control& control,
                       DualEdgeGradOp& m_edgeGradOp,
                       CCINSErrors& errors,
                       fileIO& io,
                       CCINSTransportVar& di,
                       map<int,int>& transportSpecies,
                       vector<CCINSSpeciesMassFracBC*>& massFracBCs,
                       vector<CCINSTemperatureBC*>& temperatureBCs,
                       vector<CCINSTurbEpsBC*>& turbEpsBCs,
                       vector<CCINSTurbKEBC*>& turbKEBCs,
                       vector<CCINSBoussinesqForce*>& boussinesq_force,
                       LASolver& transport_solver,
                       LAMatrix& transport_A,
                       LAVector& transport_diagA,
                       LAVector& transport_b,
                       LAVector& transport_x,
                       CCINSAdapter& adapter);
    virtual ~CCINSRNGKE();
    //@}

    //**************************************************************************
    //! \name Virtual k-epsilon interface
    //! These functions are pure virtual functions that need to be
    //! implemented for each specific of k-epsilon model.
    //@{
    //! Add contributions to the LHS matrix and to the RHS vector due to the
    //! linearized sources for the e-equation
    //!   \param[in,out] S       Reference to the LHS matrix
    //!   \param[in,out] rhs     Pointer to the RHS vector
    virtual void addSourcesKE(LAMatrix* S, Real* rhs);

    //! Add contributions to the LHS matrix and to the RHS vector due to the
    //! linearized sources for the k-equation
    //!   \param[in,out] S       Reference to the LHS matrix
    //!   \param[in,out] rhs     Pointer to the RHS vector
    virtual void addSourcesEps(LAMatrix* S, Real* rhs);

    //! Apply the Bouyancy correction
    virtual void applyBuoyancyCorrectionKE(Real* rhs);

    //! Apply the epsilon penalty based on the wall function type
    virtual void applyWallEpsPenalty(LAMatrix& S, LAVector& B, Real* rhs);

    //! Calculate the turbulent viscosity for the K-Eps (RNG) model
    virtual void calcTurbulentViscosity();

    //! Calculate y* on a surface
    virtual void calcSurfYstar(const int nel, const int* edge_list, Real* ystar);

    //! Calculate y* in the volume
    virtual void calcYstar(Real* dist, Real* ystar);

    //! Set the wall viscosity according to the specific model
    virtual void setWallConductivity(int nedges, int* edge_list, DualEdge* edges,
                                     Real* kmol, Real* keff, Real* edgek);

    //! Set the wall viscosity according to the specific model
    virtual void setWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                  Real* mu, Real* mueff, Real* edgemu);

    //! Solve generic scalar transport equation(s)
    //! \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);
    //@}

  protected:

    //! Add compound wall-function sources to the LHS/RHS k-transport eq.
    void addCompoundWallSourcesKE(LAMatrix* S, Real* rhs);

    //! Add the wall-function source terms to the LHS and RHS
    void addScalableWallSourcesKE(LAMatrix* S, Real* rhs);

    //! Add two-layer wall-function sources to the LHS/RHS k-transport eq.
    void addTwolayerWallSourcesKE(LAMatrix* S, Real* rhs);

    //! Apply the epsilon penalty for compound wall function
    void applyCompoundWallEpsPenalty(LAMatrix& S, LAVector& B, Real* rhs);

    //! Apply the epsilon penalty for scalable wall function
    void applyScalableWallEpsPenalty(LAMatrix& S, LAVector& B, Real* rhs);

    //! Apply the epsilon penalty for scalable wall function
    void applyTwolayerWallEpsPenalty(LAMatrix& S, LAVector& B, Real* rhs);

    //! Calculate the wall viscosity for compound wall treatment
    void calcCompoundWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                   Real* mu, Real* mueff, Real* edgemu);

    //! Calculate the wall viscosity according to the specific model
    void calcScalableWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                   Real* mu, Real* mueff, Real* edgemu);

    //! Calculate the wall viscosity for two-layer wall functions
    void calcTwolayerWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                   Real* mu, Real* mueff, Real* edgemu);

    //! Compute mean dissipation rate
    void meanEpsilon(Real tke, Real rho, Real mu, Real Aeps, Real kappa,
                     Real yn, Real& epsbar);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSRNGKE(const CCINSRNGKE&);
    CCINSRNGKE& operator=(const CCINSRNGKE&);
    //@}

    //! Apply prescdribed mass fraction BCs
    void applyMassFracBCs(const Real t, Real* var, Real* varg, bool* bcflag);

    // Boundary conditions
    vector<CCINSSpeciesMassFracBC*>& m_massFracBCs;

    //! transport species
    map<int,int> m_transportSpecies;

    //**************************************************************************
    //! \name Model constants/parameters
    //@{
    Real m_Cmu;
    Real m_kappa;
    Real m_E;
    Real m_Cmu14;
    Real m_Cmu12;
    Real m_Cmu34;
    Real m_ln2;
    Real m_yp11;
    Real m_c1eps;
    Real m_c2eps;
    Real m_beta;
    Real m_eta0;
    Real m_alpha;
    //@}

};

}

#endif // CCINSRNGKE_h
