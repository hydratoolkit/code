//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSMomentum.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Solve the momentum transport equations, using Semi-Implicit
//!          P2 Projection algorithm
//******************************************************************************
#ifndef CCINSMomentum_h
#define CCINSMomentum_h

#include <CCINSTransport.h>

namespace Hydra {

class Category;
class CCINSBodyForce;
class CCINSGravityForce;
class CCINSSymmVelBC;
class CCINSVelocityBC;
class CCINSTractionBC;
class CCINSPassiveOutflowBC;
class CCINSPorousDrag;
class CCINSPressureOutflowBC;
class CCINSMassFlowBC;
class CCINSMassFluxBC;
class CCINSVolumeFlowBC;
class CCINSTurbulence;

//! Solve the momentum transport equations
class CCINSMomentum : public CCINSTransport {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSMomentum(UnsMesh& mesh,
                           Control& control,
                           DualEdgeGradOp& edgeGradOp,
                           CCINSErrors& errors,
                           fileIO& io,
                           CCINSTransportVar& di,
                           map<int,int>& transportSpecies,
                           vector<CCINSSymmVelBC*>& symmvelbc,
                           vector<CCINSVelocityBC*>& velocitybc,
                           vector<CCINSTractionBC*>& tractionbc,
                           vector<CCINSPassiveOutflowBC*>& passiveoutflowbc,
                           vector<CCINSPressureOutflowBC*>& pressureoutflowbc,
                           vector<CCINSMassFlowBC*>& massflowbc,
                           vector<CCINSMassFluxBC*>& massfluxbc,
                           vector<CCINSVolumeFlowBC*>& volumeflowbc,
                           vector<CCINSBodyForce*>& body_force,
                           vector<CCINSBoussinesqForce*>& boussinesq_force,
                           vector<CCINSGravityForce*>& gravity_force,
                           vector<CCINSPorousDrag*>& porousdrag,
                           LASolver& momentum_solver,
                           LAMatrix& momentum_A,
                           LAVector& momentum_diagA,
                           LAVector& momentum_diagA_temp,
                           LAVector& momentum_bx,
                           LAVector& momentum_by,
                           LAVector& momentum_bz,
                           LAVector& momentum_x,
                           LAVector& momentum_y,
                           LAVector& momentum_z,
                           CCINSTurbulence& turbulence,
                           CCINSAdapter& adapter);
    virtual ~CCINSMomentum();
    //@}

    //! Virtual form transport Rhs function
    //!   \param[in] incParm Reference to time increment parameters
    //!   \param[in] SourceMode   Time-weighting/time-level for source terms
    //!   \param[in,out] rhs Pointer to the rhs vector
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs=0);

    void formRhsInc(const CCINSIncParm& incParm, Real* rhs=0);

    //! Solve momentum equations
    //!   \param[in] incParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);

    //! Assemble one momentum equation to fill matrix
    //!   \param[in] incParm Time increment parameters
    virtual void assembleSystem(const CCINSIncParm& incParm, const int dim);

    //! Assemble direction independent part of momentum equations
    //!   \param[in] incParm Time increment parameters
    void assembleInitial(const CCINSIncParm& incParm);

    //! Assemble edge gradient operator for momentum equations
    //!   \param[in] incParm Time increment parameters
    void assembleEdgeGradOp(const CCINSIncParm& incParm, const int dim,
                            LAVector* m_momentum_comp,
                            LAVector* m_momentum_bcomp);

    //! Compute residual for the momentum equations
    //!   \param[in] incParm Time increment parameters
    //!   \param[in] rhsVecN Rhs vector at time n
    virtual void calcResiduals(const CCINSIncParm& incParm,
                               CVector* rhsVecN);

    //! Apply body force terms momentum
    void applyBodyForces(Real thetaFn, Real thetaFnp1,
                         CVector& rhs, FVMCCINS::SourceMode mode);

    //! Apply body force terms to partial acceleration
    void applyBodyForces(CVector& rhs);

    //! Apply porous drag force terms to momentum LHS
    void applyPorousDrag( Real thetaFnp1, LAMatrix& K);

    //! Apply porous drag force terms to momentum RHS
    void applyPorousDrag(Real thetaFn, CVector& rhs);

    //! Apply body force terms to partial acceleration
    void applyPorousDrag(CVector& rhs);

    //! Apply velocity BC's
    void applyVelBCs(Real time,
                     CVector& vel, CVector& gvel,
                     bool exchange);

    //! Apply velocity BC's and setup boundary flags for LS
    void applyVelBCs(Real t,
                     CVector& vel, CVector& gvel,
                     CBoolVector& vel_bcflag, bool exchange);

    //! Apply prescribed surface traction BCs
    void applyTractionBCs(Real thetaFn, Real thetaFnp1, CVector& rhs);

    //! Apply prescribed surface traction BCs
    void applyDeformMeshTractionBCs(Real theta, Real time, CVector& rhs);

    //! Calculate the edge viscosity
    void calcEdgeViscosity(Real* mu, Real* edgemu);

    //! Compute the velocity gradient at dual-edges
    //!   \param[in] VEL          Data index for cell-centered velocity vectors
    //!   \param[in] GHOST_VEL    Data index for ghost-cell velocity vectors
    //!   \param[in] EDGE_TENSOR  Data index for edge tensor, to be computed
    void calcEdgeVelGrad(DataIndex VEL,
                         DataIndex GHOST_VEL,
                         DataIndex EDGE_TENSOR);

    //! Compute the velocity gradient at element centers
    void calcElemVelGrad(DataIndex VEL,
                         DataIndex GHOST_VEL,
                         DataIndex EDGE_TENSOR);

    //! Update the velocity gradient
    //!   \param[in] time         Current time
    void updateVelocityGradient(Real& time);

    //! Add the momentum advection terms to the LHS
    //!   \param[in]  wt Weighting coefficient
    //!   \param[out] K  L.H.S. matrix
    void addAdvectiveLhs(Real* wt, LAMatrix& K);

    //! Add the viscous terms to the RHS
    void addDiffusiveRhs(Real theta, Real dt, Real* edgemu,
                         CVector& vel, CVector& gvel, CVector& rhs);

    //! Add Pressure contribution to RHS
    void addPressureRhs();

    //! Adjust the pressure gradient for Lagrangian mesh deformation
    void adjustGradP(CVector& coord, Real* V,
                     CVector& pgrad, CVector& rhs);

    //! Calculate nonlinear contribution to stress
    void calcAuxTurbRhs(CVector& rhs, Real dt);

    //! Calculate quadratic stress
    void calcQuadStress(Real CNL1, Real CNL2, Real CNL3, Real CNL6, Real CNL7,
                        Real edgeCMU, Real k_e, Tensor vel, Tensor& tau);

    //**************************************************************************
    //! \name Linear Algebra
    //@{
    LASolver& m_momentum_solver;
    LAMatrix& m_momentum_A;
    LAVector& m_momentum_diagA;
    LAVector& m_momentum_diagA_temp;
    LAVector& m_momentum_bx;
    LAVector& m_momentum_by;
    LAVector& m_momentum_bz;
    LAVector& m_momentum_x;
    LAVector& m_momentum_y;
    LAVector& m_momentum_z;
    //@}

    //**************************************************************************
    //! \name Body forces, porous drag forces, and mass flux
    //@{
    vector<CCINSBodyForce*>& m_body_force;
    vector<CCINSBoussinesqForce*>& m_boussinesq_force;
    vector<CCINSGravityForce*>& m_gravity_force;
    vector<CCINSPorousDrag*>& m_porousdrag;
    vector<CCINSMassFlowBC*>& m_massflowbc;
    vector<CCINSMassFluxBC*>& m_massfluxbc;
    vector<CCINSVolumeFlowBC*>& m_volumeflowbc;
    //@}

    //! Traction BC
    vector<CCINSTractionBC*>& m_tractionbc;

    //! Simple outflow modification
    vector<CCINSPassiveOutflowBC*>& m_passiveoutflowbc;
    vector<CCINSPressureOutflowBC*>& m_pressureoutflowbc;

    //! Velocity BCs
    vector<CCINSSymmVelBC*>& m_symmVelBCs;
    vector<CCINSVelocityBC*>& m_velocityBCs;

  protected:

    //! Apply the passive outflow BC
    //!   \param[out] rhs   Reference to the rhs vector
    //!   \param[in]  mesh  Reference to mesh object
    void applyPassiveOutflowBCs(Real thetaAn,
                                CVector& xc,
                                CVector& gxc,
                                Real* wt,
                                Real* vf,
                                Real* phi,
                                Real* gphi,
                                CVector& dphi,
                                CVector& gdphi,
                                Real* rhs);

    //! Apply a prescribed traction BC
    //!   \param[in]  p     Pointer to pressure field
    //!   \param[in]  dt    Time step
    //!   \param[out] rhs   Reference to the rhs vector
    void applyPressureOutflowBCs(Real* p,
                                 Real dt,
                                 CVector& rhs);

    //! Add the momentum advection terms to the RHS
    //!   \param[out] rhs          Reference to the rhs vector
    //!   \param[in]  DUALEDGE_VEL Data index for face velocity vector
    //!   \param[in]  VEL          Data index for cell-centered velocity vector
    //!   \param[in]  mode         Mode of advection operator treatment
    void addAdvectiveRhs(CVector& rhs,
                         DataIndex DUALEDGE_VEL,
                         DataIndex VEL,
                      FVMCCINS::SourceMode mode = FVMCCINS::TIME_LEVEL_WEIGHTED);
    //! Add the incremental momentum advection terms to the RHS
    //!   \param[out] rhs          Reference to the rhs vector
    //!   \param[in]  DUALEDGE_VEL Data index for face velocity vector
    //!   \param[in]  VEL          Data index for cell-centered velocity vector
    void addAdvectiveRhsInc(CVector& rhs,
                            DataIndex DUALEDGE_VEL,
                            DataIndex VEL);

    //! return penalty constraint value (CHT)
    Real getPenaltyConstraint(Material* mat, GlobalDirection direction) const;

    //! transport species
    map<int,int> m_transportSpecies;

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSMomentum(const CCINSMomentum&);
    CCINSMomentum& operator=(const CCINSMomentum&);
    //@}

    //! Pointer to rhs vector, evaluated at (n)-th time level Used in Picard
    CVector* m_rhsVelN;

    //! Equation names for error reporting
    vector<string> m_eqname;

    //! Prefix for creation of an error message
    vector<string> m_warn_prefix;

    //! Error codes for convergence error reporting
    vector<CCINSSolverError> m_error_codes;

    //! x,y, and z component of the momentum solution
    vector<LAVector*> m_momentum_comp;

    //! x,y, and z component of the momentum rhs
    vector<LAVector*> m_momentum_bcomp;

    //! Turbulence model
    CCINSTurbulence& m_turbulence;
};

}

#endif // CCINSMomentum_h
