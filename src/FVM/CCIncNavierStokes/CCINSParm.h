//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSParm.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Penalty multipliers for hybrid Navier-Stokes solver
//******************************************************************************
#ifndef CCINSParm_h
#define CCINSParm_h

namespace Hydra {

//! Reduced penalty multipler to 1.0e+10 from 1.0e+15 to avoid
//! parallel sensitivity to this parameter with PETSc's GMRES/FGMRES
//! Gram-Schmidt procedures.

#define CCINS_PENALTY_MULTIPLIER 1.0e+10

//! For the rigid-block penalty multiplier, use a slightly larger value.
//! This forces the block velocity to match the material velocity.

#define CCINS_RIGID_PENALTY_MULTIPLIER 1.0e+15

}

#endif // CCINSParm_h
