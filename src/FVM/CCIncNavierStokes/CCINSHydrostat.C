//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSHydrostat.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:23 2011
//! \brief   Hydrostatic pressure, i.e., reference pressure, condition
//******************************************************************************
#include <string>
#include <sstream>

using namespace std;

#include <CCINSParm.h>
#include <CCINSHydrostat.h>
#include <UnsMesh.h>
#include <MPIWrapper.h>
#include <LAMatrix.h>
#include <Exception.h>

using namespace Hydra;

CCINSHydrostat::CCINSHydrostat(int setid, int userid, int tblid, Real amp,
                               Vector& coord, bool useCoord):
  m_useCoord(useCoord),
  m_setPbc(false),
  m_node(-1),
  m_userId(-1),
  m_setid(setid),
  m_userid(userid),
  m_tblid(tblid),
  m_amp(amp),
  m_penalty(CCINS_PENALTY_MULTIPLIER)
/*******************************************************************************
Routine: CCINSHydrostat - constructor
Author : Mark A. Christon
*******************************************************************************/
{
  m_coord.X = coord.X;
  m_coord.Y = coord.Y;
  m_coord.Z = coord.Z;
}

CCINSHydrostat::~CCINSHydrostat()
/*******************************************************************************
Routine: ~CCINSHydrostat - destructor
Author : Mark A. Christon
*******************************************************************************/
{}

void
CCINSHydrostat::adjustPressure(UnsMesh& mesh, Real t, Real* p)
/*******************************************************************************
Routine: adjustPressure - adjust the pressure based on the hydrostat
Author : Mark A. Christon
*******************************************************************************/
{
  // Evaluate the hydrostat -- probably never really use a table, but ...
  Real lcv = 1.0;
  if (m_tblid > 0) {
    Table* tbl = mesh.getTable(m_tblid);
    lcv = tbl->lookup(t);
  }
  Real phstat = m_amp*lcv;

  // Calculate the pressure adjustment based on the hydrostat
  Real dp = 0.0;
  if (m_node > -1) {
    dp = phstat - p[m_node];
  }

  // Broadcast the pressure difference so all proc's have the correct value
  // for the hydrostatic adjustment
  g_comm->broadcast(&dp, 1, 0);

  // Finally adjust the pressure
  for (int i = 0; i < mesh.getNnp(); i++) {
    p[i] += dp;
  }
}

void
CCINSHydrostat::adjustPressure(UnsMesh& mesh, int nhist, Real t,
                               int* gids, Real* p, Real* work)
/*******************************************************************************
Routine: adjustPressure - adjust the pressure based on the hydrostat
Author : Mark A. Christon
*******************************************************************************/
{
  // Evaluate the hydrostat -- probably never really use a table, but ...
  Real lcv = 1.0;
  if (m_tblid > 0) {
    Table* tbl = mesh.getTable(m_tblid);
    lcv = tbl->lookup(t);
  }
  Real phstat = m_amp*lcv;

  // Calculate the pressure adjustment based on the hydrostat
  Real dp = 0.0;
  if (m_node > -1) {
    dp = phstat - p[m_node];
  }

  // Broadcast the pressure difference so all proc's have the correct value
  // for the hydrostatic adjustment
  g_comm->broadcast(&dp, 1, 0);

  if(nhist > 0) {
    int* cidmap = mesh.getElementClassMap();
    for (int i =0; i < nhist; i++){
      int gid = gids[i];
      int cid = cidmap[gid];
      Element* ec = mesh.getElementClass(cid);
      int  nnpe = ec->getNnpe();
      int* conn = ec->getConnectivity();
      int  eoffset = ec->getElementOffset();
      int  lid     = gid - eoffset;
      for (int j = 0; j < nnpe; j++) {
        int ndi = conn[lid*nnpe + j];
        work[ndi] = p[ndi] + dp;
      }
    }

    // Finally adjust the pressure
    for (int i =0; i < nhist; i++){
      int gid = gids[i];
      int cid = cidmap[gid];
      Element* ec = mesh.getElementClass(cid);
      int  nnpe = ec->getNnpe();
      int  eoffset = ec->getElementOffset();
      int  lid = gid - eoffset;
      int* conn = ec->getConnectivity();
      for (int j = 0; j < nnpe; j++) {
        int ndi = conn[lid*nnpe + j];
        p[ndi] = work[ndi];
      }
    }
  }
}

void
CCINSHydrostat::apply(int Nproc, int nodeEqStart, int nodeEqEnd,
                      int* nodeeqmap, Real* Adiag)
/*******************************************************************************
Routine: apply - apply penalty to the PPE operator for the hydrostatic pressure
Author : Mark A. Christon
*******************************************************************************/
{
  if (m_node > -1 && m_setPbc) {
    if (Nproc > 1) {
      int eqId = nodeeqmap[m_node];
      // Check ownership of the node to be sure it resides on-processor
      if (eqId >= nodeEqStart && eqId < nodeEqEnd) {
        int rnd = eqId - nodeEqStart;
        Adiag[rnd] *= m_penalty;
      }
    }
    else {
      Adiag[m_node] *= m_penalty;
    }
  }
}

void
CCINSHydrostat::apply(UnsMesh& mesh, int nodeEqStart, int nodeEqEnd,
                      int* nodeeqmap, LAMatrix* A, Real* rhs, Real t,
                      Real scale)
/*******************************************************************************
Routine: apply - apply penalty to the PPE operator for the Dirichlet BC's
Author : Mark A. Christon
*******************************************************************************/
{
  // This is really a node-based BC that uses a surface set.
  // It assumes that the LHS operator has already had the penalty applied.
  Real aii;
  if (m_node > -1 && m_setPbc) {
    // Evaluate the hydrostat -- probably never really use a table
    Real lcv = 1.0;
    if (m_tblid > 0) {
      Table* tbl = mesh.getTable(m_tblid);
      lcv = tbl->lookup(t);
    }
    Real bcval = scale*m_amp*lcv;
    int eqId = nodeeqmap[m_node];
    if( eqId >= nodeEqStart && eqId < nodeEqEnd) {
      A->get(1, &eqId, 1, &eqId, &aii);
      rhs[m_node] = aii*bcval;
    }
  }
}

void
CCINSHydrostat::apply(UnsMesh& mesh, int nodeEqStart, int nodeEqEnd,
                      int* nodeeqmap, LAMatrix* A, Real* rhs,
                      Real timen, Real timenp1, Real scale)
/*******************************************************************************
Routine: apply - apply penalty to the RHS for the Dirichlet BC's -- incremental
Author : Mark A. Christon
*******************************************************************************/
{
  // This is really a node-based BC that uses a surface set.
  // It assumes that the LHS operator has already ahd the penalty applied.
  Real aii;

  if (m_node > -1 && m_setPbc) {
    if (m_tblid > 0) {
      Table* tbl = mesh.getTable(m_tblid);
      Real lcv1 = tbl->lookup(timen);
      Real lcv2 = tbl->lookup(timenp1);

      Real bcval = scale*m_amp*(lcv2-lcv1);
      int eqId = nodeeqmap[m_node];
      if( eqId >= nodeEqStart && eqId < nodeEqEnd) {
        A->get(1, &eqId, 1, &eqId, &aii);
        rhs[m_node] = aii*bcval;
      }
    }
    else {
      // constant pressure difference
      rhs[m_node] = 0;
    }
  }
}

Real
CCINSHydrostat::calcDeltaP(UnsMesh& mesh, Real t, Real* p)
/*******************************************************************************
Routine: calcDeltaP- Adjust the pressure based on the hydrostat
Author : Mark A. Christon
*******************************************************************************/
{
  // Evaluate the hydrostat -- probably never really use a table, but ...
  Real lcv = 1.0;
  if (m_tblid > 0) {
    Table* tbl = mesh.getTable(m_tblid);
    lcv = tbl->lookup(t);
  }
  Real phstat = m_amp*lcv;

  // Calculate the pressure adjustment based on the hydrostat
  Real dp = 0.0;
  if (m_node > -1) {
    dp = phstat - p[m_node];
  }

  // Broadcast the pressure difference so all proc's have the correct value
  // for the hydrostatic adjustment
  g_comm->broadcast(&dp, 1, 0);

  return dp;
}

Real
CCINSHydrostat::hydrostaticPressure(UnsMesh& mesh, Real t)
/*******************************************************************************
Routine: hydrostaticPressure - evaluate the hydrostatic pressure
Author : Mark A. Christon
*******************************************************************************/
{
  Real lcv = 1.0;
  if (m_tblid > 0) {
    Table* tbl = mesh.getTable(m_tblid);
    lcv = tbl->lookup(t);
  }
  return(m_amp*lcv);
}

void
CCINSHydrostat::setNode(UnsMesh& mesh)
/*******************************************************************************
Routine: CCINSHydrostat - setNode
Author : Mark A. Christon
*******************************************************************************/
{
  if (m_useCoord) {
    // Handle the coordinate case
    int nodeId;
    int userId;
    mesh.findNode(m_coord, nodeId, userId);
    m_node = nodeId;
    m_userId = userId;
    if (m_userId < 1) { // No node found
      stringstream ssError;
      ssError << "The user node specified for the hydrostatic pressure"
              << " was not found in the mesh."
              << "Please check the mesh or control file for the correct node.";
      throw Exception(ssError.str());
    }
  } else {
    // Handle the nodeset case
    Nodeset* nset = mesh.getNodesets();
    int numNodesInSet = nset[m_setid].Nnp;
    int maxNodesInSet = g_comm->globalMax(numNodesInSet);
    // Do some error trapping to make sure we have the right nodeset
    if (maxNodesInSet < 1) { // If the nodeset is empty
      stringstream ssError;
      ssError << "A hydrostat was specified for nodeset " << m_setid
              << ", which is empty.   Please check the mesh or control file"
              << "for the correct nodeset.";
      throw Exception(ssError.str());
    } else if (maxNodesInSet > 1) { // If the nodeset has multiple nodes
      stringstream ssError;
      ssError << "A hydrostat was specified for nodeset " << m_setid
              << ", which has multiple nodes - a hydrostat is only valid "
              << " for one node.";
      throw Exception(ssError.str());
    } else if (numNodesInSet == 1) { // If the node belongs to this proc
      int* ndlist = mesh.getVariable<int>(nset[m_setid].NODE_LIST);
      // There should really only be one node in this set -- so use the first one
      int* node_map = mesh.getNodeMap();
      m_node = ndlist[0];
      m_userId = node_map[m_node];
      // Store off the coordinate for data echo
      CVector coord = mesh.getNodeCoord();
      m_coord.X = coord.X[m_node];
      m_coord.Y = coord.Y[m_node];
      m_coord.Z = coord.Z[m_node];
    }
  }
}

