//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSPressureOutflowBC.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:23 2011
//! \brief   Pressure outflow boundary condition based on surfaces
//******************************************************************************

#include <vector>

using namespace std;

#include <BCPackage.h>
#include <UnsMesh.h>
#include <CCINSPressureOutflowBC.h>

using namespace Hydra;

CCINSPressureOutflowBC::CCINSPressureOutflowBC(const BCPackage& bc, int offset):
  SideSetBC(bc, offset)
/*******************************************************************************
Routine: CCINSPressureOutflowBC - constructor
Author : Mark A. Christon
*******************************************************************************/
{}

CCINSPressureOutflowBC::~CCINSPressureOutflowBC()
/*******************************************************************************
Routine: ~CCINSPressureOutflowBC - destructor
Author : Mark A. Christon
*******************************************************************************/
{
  // Easy way to cleanup all registered data
  freeAllVariables();
}

void
CCINSPressureOutflowBC::apply(UnsMesh& mesh, const Real dt, const Real* p, CVector& rhs)
/*******************************************************************************
Routine: apply - Apply a pressure outflow BC condition to momentum equations
Author : Mark A. Christon
*******************************************************************************/
{
  // Haul out the surface data for the current set
  Sideset* surf = mesh.getSidesets();
  int Nel = surf[m_set].Nel;

  if (Nel == 0) return;

  int*   el_list = mesh.getVariable<int>(surf[m_set].EL_LIST);
  int* edge_list = mesh.getVariable<int>(surf[m_set].DUALEDGE_LIST);

  int* cidmap = mesh.getElementClassMap();

  DualEdge* edges = mesh.getDualEdges();

  for (int i=0; i<Nel; i++) {
    int  el =   el_list[i];
    int eid = edge_list[i];
    int gid = edges[eid].gid1;
    int nc  = cidmap[el];
    Element* ec = mesh.getElementClass(nc);
    int nnpe  = ec->getNnpe();
    int* conn  = ec->getConnectivity();
    int eoffset = ec->getElementOffset();
    int lid   = gid - eoffset;
    Real pavg  = 0.0;
    for (int j=0; j<nnpe; j++) {
      int ndi = conn[lid*nnpe + j];
      pavg += p[ndi];
    }
    Real ff = dt*pavg*edges[eid].gamma/nnpe;
    rhs.X[gid] += ff*edges[eid].en[0];
    rhs.Y[gid] += ff*edges[eid].en[1];
    rhs.Z[gid] += ff*edges[eid].en[2];
  }
}
