//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSSpeciesMassFrac.h
//! \author  Alan K. Stagg, Mark A. Christon
//! \date    2015
//! \brief   Solve the species mass fraction transport equation
//******************************************************************************
#ifndef CCINSSpeciesMassFrac_h
#define CCINSSpeciesMassFrac_h

#include <CCINSTransport.h>
#include <CCINSSpeciesMassFracBC.h>
#include <CCINSSpeciesDiffusionFluxBC.h>

namespace Hydra {

//! Species Mass Fraction Transport
class CCINSSpeciesMassFrac : public CCINSTransport {

  public:

    //! \name Constructor/Destructor
    //@{
            CCINSSpeciesMassFrac(UnsMesh& mesh,
                                 Control& control,
                                 DualEdgeGradOp& edgeGradOp,
                                 CCINSErrors& errors,
                                 fileIO& io,
                                 CCINSTransportVar& di,
                                 int speciesMatID,
                                 vector<CCINSSpeciesMassFracBC*>& massFracBCs,
                                 vector<CCINSSpeciesDiffusionFluxBC*>& diffusionFluxBCs,
                                 LASolver& transport_solver,
                                 LAMatrix& transport_A,
                                 LAVector& transport_b,
                                 LAVector& transport_x,
                                 CCINSAdapter& adapter);
    virtual ~CCINSSpeciesMassFrac();
    //@}

    //! Assemble generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void assembleSystem(const CCINSIncParm& incParm);

    //! calculate cell and edge density*diffusivity
    virtual void calcRhoDiffusivity(Real* rho,
                                    Real* dmol,
                                    Real* deff,
                                    Real* edged);

    //! Virtual form transport Rhs function
    //!   \param[in] CCINSIncParm Time increment parameters
    //!   \param[in] SourceMode   Time-weighting/time-level for source terms
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs=0);

    virtual void formRhsInc(const CCINSIncParm& incParm, Real* rhs=0);


    //! Solve generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);

    //! Apply prescribed diffusion flux BCs
    void applyDiffusionFlux(const Real theta, const Real time,
                            const Real dt, Real* rhs);
    void applyDiffusionFlux(const Real /*thetaFn*/, const Real /*thetaFnp1*/,
                            Real* rhs, const FVMCCINS::SourceMode mode);

    //! Apply prescribed species mass fraction BCs
    //!   \param[in] t         Time
    //!   \param[in,out] var   Scalar field to set BCs on
    //!   \param[in,out] varg  Ghost array for scalar field to set BCs on
    //!   \param[in] mode      Flag, indicating the time marching scheme
    //!   \param[in] exchange  Flag, indicating if communication is needed
    void applyMassFracBCs(const Real t,
                          Real* var,
                          Real* varg,
                          bool* bcflag,
                          const bool exchange = true);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSSpeciesMassFrac(const CCINSSpeciesMassFrac&);
    CCINSSpeciesMassFrac& operator=(const CCINSSpeciesMassFrac&);
    //@}

    //! Species BCs
    vector<CCINSSpeciesMassFracBC*>& m_massFracBCs;
    vector<CCINSSpeciesDiffusionFluxBC*>& m_diffusionFluxBCs;

    //! species material ID
    int m_speciesMatID;

    //**************************************************************************
    //! \name Linear Algebra
    //@{
    LASolver& m_transport_solver; //!< Linear solver
    LAMatrix& m_transport_A;      //!< Operator
    LAVector& m_transport_b;      //!< Right-hand-side
    LAVector& m_transport_x;      //!< Solution vector
    //@}

};

}

#endif // CCINSSpeciesMassFrac_h
