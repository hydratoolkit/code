//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSSpeciesDiffusionFluxBC.h
//! \author  Alan K. Stagg
//! \date    Tue Nov 18 2014
//! \brief   Species mass fraction flux boundary condition based on sidesets
//******************************************************************************
#ifndef CCINSSpeciesDiffusionFluxBC_h
#define CCINSSpeciesDiffusionFluxBC_h

#include <CCSideSetBC.h>
#include <CCINSSolutionOptions.h>

namespace Hydra {

struct BCPackage;

//! Species Diffusion Flux Boundary Condition
class CCINSSpeciesDiffusionFluxBC: public CCSideSetBC
{
  public:

    //! \name Constructor/Destructor
    //@{
             CCINSSpeciesDiffusionFluxBC(const BCPackage& bc);
    virtual ~CCINSSpeciesDiffusionFluxBC();
    //@}

    //! Apply a prescribed diffusion flux BC
    void apply(UnsMesh& mesh, const Real thetaFn, const Real thetaFnp1,
               const Real tn, const Real dt, const Real tnp1, Real* rhs,
               const FVMCCINS::SourceMode mode=FVMCCINS::TIME_LEVEL_WEIGHTED);

    //! Apply a prescribed heat-flux BC
    void apply(UnsMesh& mesh, const Real theta, const Real time,
               const Real dt, Real* rhs);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSSpeciesDiffusionFluxBC(const CCINSSpeciesDiffusionFluxBC&);
    CCINSSpeciesDiffusionFluxBC& operator=(const CCINSSpeciesDiffusionFluxBC&);
    //@}

};

}

#endif
