//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSLDKMKsgs.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:23 2011
//! \brief   LDKM subgrid kinetic energy turbulence model equation
//******************************************************************************
#include <cassert>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

#include <macros.h>
#include <MathOp.h>
#include <CCINSLDKMKsgs.h>
#include <CCINSTurbKEBC.h>
#include <FVMCCINSControl.h>
#include <LDKMKsgsControl.h>

using namespace Hydra;
using namespace Hydra::MathOp;

void
CCINSLDKMKsgs::solve(const CCINSIncParm& incParm)
/*******************************************************************************
Routine: solve - solve the LDKM Ksgs one equation model
Author : Mark A. Christon
*******************************************************************************/
{
  Real* tke    = m_mesh.getVariable<Real>(m_di.TURB_KE);
  Real* eqVar  = m_mesh.getVariable<Real>(m_di.ELEMEQ_VAR);

  // Assemble the system
  assembleSystem(incParm);

  //Solve the transport equation
  m_transport_solver.solve(&m_transport_b, &m_transport_x);

  // Copy-back the solution
  memcpy(tke, eqVar, m_Nel*sizeof(Real));

  // Check status
  LASolverStatus status;
  m_transport_solver.getStatus(status);

  string eqname = "-- KE  Transport Model Equation (K-EPS) --";
  if (m_pid == 0)
    status.echoResults(cout, eqname.c_str());

  if (! status.isConverged()){
    m_io.writeWarning("TKE model equation may not have fully converged");
  }
}

CCINSLDKMKsgs::CCINSLDKMKsgs(UnsMesh& mesh,
                             Control& control,
                             DualEdgeGradOp& edgeGradOp,
                             CCINSErrors& errors,
                             fileIO& io,
                             CCINSTransportVar& di,
                             vector<CCINSTurbKEBC*>& turbKEBCs,
                             LASolver& transport_solver,
                             LAMatrix& transport_A,
                             LAVector& transport_diagA,
                             LAVector& transport_b,
                             LAVector& transport_x,
                             CCINSAdapter& adapter):
  CCINSKsgs(mesh, control, edgeGradOp, errors, io, di,
            turbKEBCs, transport_solver, transport_A, transport_diagA,
            transport_b, transport_x, adapter)
/*******************************************************************************
Routine: CCINSLDKMKsgs - constructor
Author : Mark A. Christon
*******************************************************************************/
{}

CCINSLDKMKsgs::~CCINSLDKMKsgs()
/*******************************************************************************
Routine: ~CCINSLDKMKsgs - destructor
Author : Mark A. Christon
*******************************************************************************/
{}

void
CCINSLDKMKsgs::addSources(LAMatrix& S, Real* rhs)
/*******************************************************************************
Routine: addSources - compute Lhs sources for the ksgs model
Author : Mark A. Christon
*******************************************************************************/
{
  Real* ke  = m_mesh.getVariable<Real>(m_di.TURB_KEN);

  Real* V   = m_mesh.getVariable<Real>(m_di.VOLUME);
  //Real* rho = m_mesh.getVariable<Real>(m_di.DENSITY);
  Real* mut = m_mesh.getVariable<Real>(m_di.ELEM_TURB_VISCOSITY);

  // Velocity gradient ui,j at the element
  CTensor uij   = m_mesh.getCTensor(m_di.ELEM_TENSOR);

  // Calculate the normal distance at the cell-element
  // Be carefull do not overwrite the TEMP_ELEM array!
  //Real* ndist = m_mesh.getVariable<Real>(m_di.TEMP_ELEM);//!!!
  calcElementNormalDistance(m_di.TURB_DIST, m_di.TEMP_ELEM);

  const Real dbleps = numeric_limits<Real>::epsilon();

  Real dtThetaLhs = -m_dt*m_thetaFnp1;
  Real dtThetaRhs =  m_dt*m_thetaFn;

  int Nec = m_mesh.numElementClass();
  for (int n = 0; n < Nec; n++) {
    Element* ec = m_mesh.getElementClass(n);
    Material* mat = m_mesh.getMaterial(ec->getMatId());
    int nel = ec->getNel();
    if (mat->isFluid() && nel > 0) {
      int* gids = ec->getGlobalIds();
      for(int i=0;i<nel;i++) {
        int gid = gids[i];

        //Real rhoe = rho[gid];
        Real tke  = ke[gid];
        //Real dist = ndist[gid];

        // Compute the Strain rate Sij
        SymTensor Sij;
        symmetric(gid, uij, Sij);

        // Compute the Reynolds stress tensor
        SymTensor Tij;
        Real mtwomut = 2.0 * mut[gid];
        scale(Sij, mtwomut, Tij);

        // Compute Production
        Real pro;
        innerProduct(Sij, Tij, pro);

        // Evaluate the Lhs sources!
        Real aii  = (pro - pro)/(tke + dbleps);
        aii      *= dtThetaLhs*V[i];

        int eqId = gid + m_elEqIDOffset;
        S.add(1, &eqId, 1, &eqId, &aii);

        // Evaluate the Rhs sources!
        Real srhs = pro - pro;
        srhs     *= dtThetaRhs*V[gid];
        rhs[gid] += srhs;
      }
    }
  }
}

void
CCINSLDKMKsgs::applyTurbKEBCs(const Real t, Real* var,
                              Real* varg, bool* bcflag,
                              bool exchange)
/*******************************************************************************
Routine: applyTurbKEBCs - apply scalar BC and get flags for LHS operator
Author : Mark A. Christon
*******************************************************************************/
{
  DualEdge* edges = m_mesh.getDualEdges();

  //First setup default data on external edges from initial data
  for (int i=0; i<m_Nedge_ifc; i++) {
    varg[i] = var[edges[i].gid1];
    bcflag[i] = false;
  }

  // Turb KE BC's
  int nbc = m_turbKEBCs.size();
  for (int ibc = 0; ibc < nbc; ibc++) {
    CCINSTurbKEBC* bc = m_turbKEBCs[ibc];
    bc->setEdgeBCs(m_mesh,bcflag,varg,t);
  }

  // Swap boundary-data as necessary
  if (m_Nproc > 1 && exchange) swapGhostVar(var, varg);
}

void
CCINSLDKMKsgs::assembleSystem(const CCINSIncParm& incParm)
/*******************************************************************************
Routine: solve - solve the LDKM Ksgs one equation model
Author : Mark A. Christon
*******************************************************************************/
{
  Real* tke    = m_mesh.getVariable<Real>(m_di.TURB_KE);
  Real* tken   = m_mesh.getVariable<Real>(m_di.TURB_KEN);
  Real* gtke   = m_mesh.getVariable<Real>(m_di.GHOST_VAR);
  Real* rhs    = m_mesh.getVariable<Real>(m_di.ELEM_RHS);
  if (m_deformableMesh)
    rhs = m_mesh.getVariable<Real>(m_di.ELEM_SCALAR_RHS);
  Real* rho    = m_mesh.getVariable<Real>(m_di.DENSITY);
  //Real* V      = m_mesh.getVariable<Real>(m_di.VOLUME);
  Real* eqVar  = m_mesh.getVariable<Real>(m_di.ELEMEQ_VAR);

  bool* tkeflg = m_mesh.getVariable<bool>(m_di.VEL_BC);

  // Calculate the diffusion coefficient: mu + mu_t

  Real  sigi  = 1.0;
  Real* mu     = m_mesh.getVariable<Real>(m_di.ELEM_MOL_VISCOSITY);
  Real* mut    = m_mesh.getVariable<Real>(m_di.ELEM_TURB_VISCOSITY);
  Real* mueff  = m_mesh.getVariable<Real>(m_di.TMP);
  Real* edgemu = m_mesh.getVariable<Real>(m_di.EDGE_VISCOSITY);

  // Set the incrementation parameters
  setIncParm(incParm);

  // Need to form the right-hand-side if it is not deformable.
  // Right-hand-side already computed for deformable case.
  if (!m_deformableMesh) formRhs(incParm, FVMCCINS::TIME_LEVEL_WEIGHTED, rhs);

  if (m_deformableMesh || m_variableViscosity ) {
    // Need diffusion coefficient for LHS
    memset(mueff, 0, m_Nel*sizeof(Real));
    int Nec = m_mesh.numElementClass();
    for (int n = 0; n < Nec; n++) {
      Element* ec = m_mesh.getElementClass(n);
      Material* mat = m_mesh.getMaterial(ec->getMatId());
      int nel = ec->getNel();
      if (mat->isFluid() && nel > 0) {
        int* gids = ec->getGlobalIds();
        for(int i=0;i<nel;i++) {
          int gid = gids[i];
          mueff[gid] = (mu[gid] + mut[gid]*sigi);
        }
      }
    }
    // Calculate the edge version of mueff -- this is repeated here
    // since this is in the current configuration, and requires the
    // new weights for interpolation be used.
    calcEdgeMatPropIFC(mueff, edgemu);
    if( m_deformableMesh ) {
      Real* rhse = m_mesh.getVariable<Real>(m_di.TURB_KE_RHS);
      memcpy(rhs, rhse, m_Nel*sizeof(Real));
    }
  }

  // Initialize LHS matrix with zeros
  m_transport_A.zero();

  // form the diagonal Mass terms
  formMassLhs(rho, m_transport_A);

  // Add the LHS diffusive terms
  m_edgeGradOp.addDiffusiveLhs(m_restrictOp, edgemu, m_thetaKnp1, m_dt,
                               m_transport_A);

  // Add in the Lhs advective component
  addScalarAdvLhs(rho, m_transport_A);

  // Add in the source terms;
  addSources(m_transport_A, rhs);

  // Set BC ghost values at time level n+1 for
  // LHS without incurring communications
  applyTurbKEBCs(m_time_np1, tken, gtke, tkeflg, false);

  // Adjust the right hand side for Dirichlet BC's
  m_edgeGradOp.adjustRhs(m_restrictOp, edgemu, m_thetaKnp1, m_dt, tkeflg, gtke,
                         rhs, m_transport_b);

  // Modify the left hand side
  m_edgeGradOp.adjustLhs(m_restrictOp, edgemu, m_thetaKnp1, m_dt, tkeflg,
                         m_transport_A );

  // Modify the LHS and RHS due to implict advection treatment.
  adjustAdvectVar(rho, tkeflg, gtke, m_transport_A, rhs);

  // Copy-in the old-solution to eqVar
  memcpy(eqVar, tke, m_Nel*sizeof(Real));

  //Assemble the operator and the vectors
  m_transport_x.assemble();
  m_transport_b.assemble();
  m_transport_A.assemble();
  m_transport_solver.setOperator(&m_transport_A);
}

void
CCINSLDKMKsgs::formRhs(const CCINSIncParm& incParm,
                       FVMCCINS::SourceMode /* srcmode */,
                       Real* rhs)
/*******************************************************************************
Routine: formRhs - form RHS for the ksgs model
Author : Mark A. Christon
*******************************************************************************/
{
  Real* tke    = m_mesh.getVariable<Real>(m_di.TURB_KE);
  Real* tken   = m_mesh.getVariable<Real>(m_di.TURB_KEN);
  Real* gtke   = m_mesh.getVariable<Real>(m_di.GHOST_VAR);
  Real* rho    = m_mesh.getVariable<Real>(m_di.DENSITY);

  bool* tkeflg = m_mesh.getVariable<bool>(m_di.VEL_BC);

  CVector edge_grad = m_mesh.getCVector(m_di.EDGE_GRADIENT);

  // Calculate the diffusion coefficient: mu + mu_t
  Real  sigi   = 1.0;

  DataIndex ELEM_VISCOSITY = m_di.ELEM_MOL_VISCOSITY;
  if (m_variableViscosity)
    ELEM_VISCOSITY = m_di.ELEM_MOL_VISCOSITYN;
  Real* mu = m_mesh.getVariable<Real>(ELEM_VISCOSITY);
  Real* mut  = m_mesh.getVariable<Real>(m_di.ELEM_TURB_VISCOSITY);
  Real* mueff  = m_mesh.getVariable<Real>(m_di.TMP);
  Real* edgemu = m_mesh.getVariable<Real>(m_di.EDGE_VISCOSITY);

  // Set the incrementation parameters
  setIncParm(incParm);

  memset(mueff, 0, m_Nel*sizeof(Real));
  int Nec = m_mesh.numElementClass();
  for (int n = 0; n < Nec; n++) {
    Element* ec = m_mesh.getElementClass(n);
    Material* mat = m_mesh.getMaterial(ec->getMatId());
    int nel = ec->getNel();
    if (mat->isFluid() && nel > 0) {
      int* gids = ec->getGlobalIds();
      for(int i=0;i<nel;i++) {
        int gid = gids[i];
        mueff[gid] = (mu[gid] + mut[gid]*sigi);
      }
    }
  }
  // Calculate the edge version of mueff
  calcEdgeMatPropIFC(mueff, edgemu);

  // Save the old values of nut
  memcpy(tken, tke, m_Nel*sizeof(Real));

  // Set BC ghost values at time level n and conduct communications
  applyTurbKEBCs(m_time, tken, gtke, tkeflg, true);

  // Compute the edge gradient of the transport variable
  m_edgeGradOp.calcGrad(m_restrictOp, tken, gtke, m_di.EDGE_GRADIENT);

  // Zero the RHS array, then form all the RHS terms
  memset(rhs, 0, m_Nel*sizeof(Real));

  // Add Mass (M) terms at time level n
  formMassRhs(tken, rho, rhs);

  // Diffusion terms on the RHS
  m_edgeGradOp.addDiffusiveRhs(m_restrictOp, edgemu, m_thetaKn, m_dt,
                               tken, gtke, edge_grad, rhs);

  // Add in the explicit advective component
  addScalarAdvRhs(tken, gtke, rho, rhs);
}
