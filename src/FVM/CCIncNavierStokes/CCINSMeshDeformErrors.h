//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSMeshDeformErrors.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Error handler for deforming meshes
//******************************************************************************
#ifndef CCINSMeshDeformErrors_h
#define CCINSMeshDeformErrors_h

#include <CCINSErrorTypes.h>

namespace Hydra {

// Forward Declaration
class UnsMesh;
class fileIO;


//! Error handler for deforming meshes
class CCINSMeshDeformErrors
{

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSMeshDeformErrors(UnsMesh& mesh, fileIO& io);
    virtual ~CCINSMeshDeformErrors();
    //@}

    //! Add the errors corresponding to Mesh Memory error enumerator value
    void add(CCINSMeshDeformError e);

    //! Add the errors corresponding to Mesh Memory error enumerator value
    void add(CCINSMeshDeformError e, int numerr);

    //! Get the total number of errors for Deform Mesh category
    static int getSize();

    //! Gather the errors from Deform Mesh category and position
    void gather(int* arrayGlobal, int offset);

    //! Prepare Error Report in Deform Mesh category
    bool report(int* sumArrayGlobal, int offset);

    //! Get the maximum allowed error counts corresponding to
    int getOccurrences(CCINSMeshDeformError e);

    //! Get the error message corresponding to Deforming
    const char* getMessage(CCINSMeshDeformError e);

    //! Reset the error counts to 0 corresponding to Deforming
    void resetCount(CCINSMeshDeformError e);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSMeshDeformErrors(const CCINSMeshDeformErrors&);
    CCINSMeshDeformErrors& operator=(const CCINSMeshDeformErrors&);
    //@}

    UnsMesh& m_mesh;
    fileIO& m_io;

    int m_mesherr[UNSMESH_TOTAL_ERRORS];
};

}

#endif // CCINSMeshDeformErrors_h
