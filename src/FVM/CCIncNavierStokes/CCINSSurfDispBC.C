//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSSurfDispBC.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:23 2011
//! \brief   Displacement boundary condition based on sidesets
//******************************************************************************

#include <cassert>
#include <set>

using namespace std;

#include <BCPackage.h>
#include <CCINSSurfDispBC.h>
#include <UnsMesh.h>
#include <MPIWrapper.h>

using namespace Hydra;

CCINSSurfDispBC::CCINSSurfDispBC(const BCPackage& bc, int offset):
  CCSideSetBC(bc, offset), m_direction(static_cast<GlobalDirection>(bc.m_dir)),
  m_rotation(bc.m_rotation)
/*******************************************************************************
Routine: CCINSSurfDispBC - constructor
Author : Mark A. Christon
*******************************************************************************/
{
  if(!m_rotation) {
    switch(bc.m_dir) {
    case GLOBAL_XDIR:
      m_var = DISPX;
      break;
    case GLOBAL_YDIR:
      m_var = DISPY;
      break;
    case GLOBAL_ZDIR:
      m_var = DISPZ;
      break;
    }
  } else {
    switch(bc.m_dir) {
    case GLOBAL_XDIR:
      m_r[0] = 1.0 - bc.m_r[0][0]*bc.m_r[0][0];
      m_r[1] =     - bc.m_r[0][0]*bc.m_r[1][0];
      m_r[2] =     - bc.m_r[0][0]*bc.m_r[2][0];
      m_r[3] = 1.0 - bc.m_r[1][0]*bc.m_r[1][0];
      m_r[4] =     - bc.m_r[1][0]*bc.m_r[2][0];
      m_r[5] = 1.0 - bc.m_r[2][0]*bc.m_r[2][0];
      m_r[6] = bc.m_r[0][0];
      m_r[7] = bc.m_r[1][0];
      m_r[8] = bc.m_r[2][0];
      m_var  = DISPXT;
      break;
    case GLOBAL_YDIR:
      m_r[0] = 1.0 - bc.m_r[0][1]*bc.m_r[0][1];
      m_r[1] =     - bc.m_r[0][1]*bc.m_r[1][1];
      m_r[2] =     - bc.m_r[0][1]*bc.m_r[2][1];
      m_r[3] = 1.0 - bc.m_r[1][1]*bc.m_r[1][1];
      m_r[4] =     - bc.m_r[1][1]*bc.m_r[2][1];
      m_r[5] = 1.0 - bc.m_r[2][1]*bc.m_r[2][1];
      m_r[6] = bc.m_r[0][1];
      m_r[7] = bc.m_r[1][1];
      m_r[8] = bc.m_r[2][1];
      m_var  = DISPYT;
      break;
    case GLOBAL_ZDIR:
      m_r[0] = 1.0 - bc.m_r[0][2]*bc.m_r[0][2];
      m_r[1] =     - bc.m_r[0][2]*bc.m_r[1][2];
      m_r[2] =     - bc.m_r[0][2]*bc.m_r[2][2];
      m_r[3] = 1.0 - bc.m_r[1][2]*bc.m_r[1][2];
      m_r[4] =     - bc.m_r[1][2]*bc.m_r[2][2];
      m_r[5] = 1.0 - bc.m_r[2][2]*bc.m_r[2][2];
      m_r[6] = bc.m_r[0][2];
      m_r[7] = bc.m_r[1][2];
      m_r[8] = bc.m_r[2][2];
      m_var  = DISPZT;
      break;
    }
  }
}

CCINSSurfDispBC::~CCINSSurfDispBC()
/*******************************************************************************
Routine: ~CCINSSurfDispBC - destructor
Author : Mark A. Christon
*******************************************************************************/
{
  // Easy way to cleanup all registered data
  freeAllVariables();
}

void
CCINSSurfDispBC::setupUniqueNodeList(UnsMesh& mesh)
/*******************************************************************************
Routine: setupUniqueNodeList - unique node list for a given SurfDisp condition
Author : Mark A. Christon
*******************************************************************************/
{
  // This is intended primarily for nodal DOF that use a surface set
  // to apply a prescribed SurfDisp condition.

  // Haul out the surface data for the current set
  Sideset* surf = mesh.getSidesets();
  int Nel  = surf[m_set].Nel;

  set<int> unique;

  if (Nel > 0) {

    int  conn[4];
    int lconn[4];

    int* side_list = mesh.getVariable<int>(surf[m_set].SIDE_LIST);
    int* el_list   = mesh.getVariable<int>(surf[m_set].EL_LIST);

    // Map from global element Id to class
    int* cidmap = mesh.getElementClassMap();

    // Trip over all elements in the surface set
    for (int i=0; i<Nel; i++) {
      int el  =   el_list[i];
      int sd  = side_list[i];
      int cid = cidmap[el];
      Element* ec = mesh.getElementClass(cid);
      int Nnpf = ec->getNodesPerFace(sd);
      int lid = el - ec->getElementOffset();
      ec->getFaceConnectivity(lid, sd, conn, lconn);
      for (int j=0; j<Nnpf; j++)
        unique.insert(conn[j]);
    }
  }

  if( g_comm->getNproc() > 1) {
    NodeCommunicator& node_comm = mesh.getNodeComm();
    int mNnp  = node_comm.getNnp();
    if( mNnp > 0) {
      int  Nnp     = mesh.getNnp();
      int* nproc   = mesh.allocTempInt(Nnp);
      memset(nproc, 0, Nnp*sizeof(int));

      // Mark the nodes on this surface for this processor
      set<int>::const_iterator iter;
      for (iter = unique.begin(); iter != unique.end(); iter++) {
        int nd = *iter;
        nproc[nd] = 1;
      }
      // send and receive the marked nodes from off-processor
      int* send_list   = node_comm.getLocalList();
      int* send_buf    = mesh.allocTempInt(mNnp);
      int* recv_buf    = mesh.allocTempInt(mNnp);
      memset(recv_buf, 0, mNnp*sizeof(int));
      node_comm.swapNodeVector(nproc, send_buf, recv_buf);
      for (int i=0; i<mNnp; i++) {
        int nd = send_list[i];
        if (recv_buf[i] > 0)
          unique.insert(nd);
      }
      mesh.freeTempData(nproc);
      mesh.freeTempData(send_buf);
      mesh.freeTempData(recv_buf);
    }
  }

  m_Nnp = unique.size();
  if (m_Nnp > 0) {
    m_hasNodeList = true;
    UNIQUE_NODE_LIST = registerVariable(m_Nnp, sizeof(int), "UNIQUE_NODE_LIST");
    int* node_list = getVariable<int>(UNIQUE_NODE_LIST);
    int  nd = 0;
    set<int>::const_iterator iter;
    for (iter = unique.begin(); iter != unique.end(); iter++, nd++)
      node_list[nd] = *iter;
  }
}

void
CCINSSurfDispBC::applyCartesianInc(UnsMesh& mesh, Real scale,
                                   Real* dispN, Real* disInc, Real t,
                                   CVector& coordOrig)
/*******************************************************************************
Routine: applyCartesianInc - apply incremental Cartesian nodal displacement BC's
Author : Mark A. Christon
*******************************************************************************/
{
  if (m_Nnp == 0) return;
  int* node_list = getUniqueNodeList();

  Real lcv = 1.0;
  if (m_tblid > 0) {
    Table* tbl = mesh.getTable(m_tblid);
    lcv = tbl->lookup(t);
  }
  Real bcval = m_amp*lcv;

  switch(m_type) {
  case BC_CONSTANT:
  case BC_TIME_DEPENDENT:
    for (int i=0; i<m_Nnp; i++) {
      int nd = node_list[i];
      disInc[nd] = scale*(bcval - dispN[nd]);
    }
    break;

  case BC_SPACE_DEPENDENT:
  case BC_SPACETIME_DEPENDENT: {
    Field f = mesh.getField(m_fieldid);
    Real* fieldvar = mesh.getVariable<Real>(f.FIELD);
    for (int i=0; i<m_Nnp; i++) {
      int nd = node_list[i];
      disInc[nd] = scale*(bcval*fieldvar[i] - dispN[nd]);
    }
    break;
  }

  case BC_USER_DEFINED:
    for (int i=0; i<m_Nnp; i++) {
      m_time    = t;
      m_nodeId  = node_list[i];
      m_coord.X = coordOrig.X[m_nodeId];
      m_coord.Y = coordOrig.Y[m_nodeId];
      m_coord.Z = coordOrig.Z[m_nodeId];
      bcval = setUserValue();
      disInc[m_nodeId] = scale*(bcval - dispN[m_nodeId]);
    }
    break;
  }
}


void
CCINSSurfDispBC::applyDisplacementInc(UnsMesh& mesh, Real scale,
                                      CVector& dispN, CVector& disInc, Real t)
/*******************************************************************************
Routine: applyDisplacementInc - Apply nodal displacement BC from solid/structure
Author : Mark A. Christon
*******************************************************************************/
{
  if (m_Nnp == 0) return;
  int nd;
  int* node_list = getUniqueNodeList();

  Real lcv = 1.0;
  if (m_tblid > 0) {
    Table* tbl = mesh.getTable(m_tblid);
    lcv = tbl->lookup(t);
  }
  Real bcval = m_amp*lcv;

  switch(m_type) {
  case BC_CONSTANT:
  case BC_TIME_DEPENDENT:
    for (int i=0; i<m_Nnp; i++) {
      nd = node_list[i];
      disInc.X[nd] = scale*(bcval - dispN.X[nd]);
      disInc.Y[nd] = scale*(bcval - dispN.Y[nd]);
      disInc.Z[nd] = scale*(bcval - dispN.Z[nd]);
    }
    break;

  case BC_SPACE_DEPENDENT:
  case BC_SPACETIME_DEPENDENT: {
    Field f = mesh.getField(m_fieldid);
    // Row vector for field
    Real* fieldvar = mesh.getVariable<Real>(f.FIELD);
    int is = 0;
    for (int i=0; i<m_Nnp; i++) {
      nd = node_list[i];
      disInc.X[nd] = scale*(bcval*fieldvar[is++] - dispN.X[nd]);
      disInc.Y[nd] = scale*(bcval*fieldvar[is++] - dispN.Y[nd]);
      disInc.Z[nd] = scale*(bcval*fieldvar[is++] - dispN.Z[nd]);
    }
    break;
  }

  case BC_USER_DEFINED:
    assert ("No User-Defined BC's in this class!!!" == 0);
    break;
  }
}

void
CCINSSurfDispBC::applyZeroDisplacement(Real* disp)
/*******************************************************************************
Routine: applyZeroDisplacement - apply zero incremental nodal displacement
Author : Mark A. Christon
*******************************************************************************/
{
  if (m_Nnp == 0) return;
  int* node_list = getUniqueNodeList();

  for (int i=0; i<m_Nnp; i++) {
    int nd = node_list[i];
    disp[nd] = 0.0;
  }
}

