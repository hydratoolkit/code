//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSVelocityBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Velocity boundary condition based on sidesets
//******************************************************************************
#ifndef CCINSVelocityBC_h
#define CCINSVelocityBC_h

#include <CCSideSetBC.h>
#include <DataShapes.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;
class LAMatrix;


//! Velocity BC
class CCINSVelocityBC : public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSVelocityBC(const BCPackage& bc, int offset);
    virtual ~CCINSVelocityBC();
    //@}

    //! Project surface data to nodes
    //! \param[in]  mesh     Reference to the mesh object
    //! \param[in]  t        Time
    //! \param[out] node_var Pointer to node variables
    //! \param[out] inverse_distance_wf inverse distance weighting function
    virtual void formInvDistOp(UnsMesh& mesh, const Real t, Real* node_var,
                               Real* inverse_distance_wf);

    //! Get velocity direction
    GlobalDirection getDirection() const {return m_direction;}

    //! Set the acceleration in the ghost buffer
    //!   \param[in] mesh     DataMesh
    //!   \param[in] gd       Ghost buffer
    virtual void setAcceleration(UnsMesh& mesh, Real* gd);

    //! Set the acceleration in the ghost buffer
    //!   \param[in] mesh     DataMesh
    //!   \param[in] grho     Density ghost buffer
    //!   \param[in] gwt      Ghost Weight for acceleration
    //!   \param[in] gd       Ghost buffer
    //!   \param[in] ttime    Total simulated time for load-curve lookup
    void setAcceleration(UnsMesh& mesh, Real* grho, Real* gwt, Real* gd);

    //! Set the edge-data for a given BC
    //!   \param[in]  mesh   Reference to the mesh object
    //!   \param[in]  t      Time
    //!   \param[out] gd     Pointer to ghost data array
    void setEdgeBCs(UnsMesh& mesh, Real* gd, const Real t);

    //! Set the edge-data for a given BC
    //!   \param[in]  mesh   Reference to the mesh object
    //!   \param[in]  t      Time
    //!   \param[out] bcflag BC flag
    //!   \param[out] gd     Pointer to ghost data array
    virtual void setEdgeBCs(UnsMesh& mesh, bool* bcflag, Real* gd,
                            const Real t);

    //! Setup the edge-data for a given BC
    //!   \param[in] mesh     DataMesh
    //!   \param[in] bcflag   Bool flag to be set for Dirichlet BC's
    void setupEdgeBCs(UnsMesh& mesh, bool* bcflag);

    //! Set the ghost-data for a given Velocity BC
    //!   \param[in] mesh     DataMesh
    //!   \param[in] gd       Ghost buffer
    //!   \param[in] t        Total simulated time for load-curve lookup
    virtual void setGhostBC(UnsMesh& mesh, Real* gd, const Real t);

    //! User function for setting acceleration
    virtual Real setUserAcceleration();

    //! Set a user-defined BC value
    virtual Real setUserValue();

  protected:

    GlobalDirection m_direction;

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSVelocityBC(const CCINSVelocityBC&);
    CCINSVelocityBC& operator=(const CCINSVelocityBC&);
    //@}

};

}

#endif // CCINSVelocityBC_h
