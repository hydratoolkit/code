//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSUserOmegaIC.h
//! \author  Mark A. Christon
//! \date    Tue Sep  5 15:44:10 MDT 2017
//! \brief   User-defined epsilon IC's
//******************************************************************************
#ifndef CCINSUserOmegaIC_h
#define CCINSUserOmegaIC_h

#include <CCINSFlow.h>
#include <UserIC.h>

namespace Hydra {

//! User-defined temperature IC
class CCINSUserOmegaIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSUserOmegaIC();
    virtual ~CCINSUserOmegaIC();
    //@}

    //! Virtual function to setup IC's using a mesh and set of DataIndices
    void setICs(UnsMesh* mesh, const DataIndex TURB_OMEGA);

    //! Set user-defined omega initial conditions
    void setUserOmega();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSUserOmegaIC(const CCINSUserOmegaIC&);
    CCINSUserOmegaIC& operator=(const CCINSUserOmegaIC&);
    //@}

    //! \name Data presented for the user-defined omega function
    //@{
    int m_elemId;      //!< Element Id for user access
    int m_matId;       //!< Material Id for user access
    Vector m_coord;    //!< Element centroid coordinates
    Real   m_omega;    //!< Element inverse dissipation time scale
    Material* m_mat;   //!< Material class for user access
    //@}
};

}

#endif // CCINSUserOmegaIC_h
