//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSTurbZetaBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Turb zeta boundary condition based on sidesets
//******************************************************************************
#ifndef CCINSTurbZetaBC_h
#define CCINSTurbZetaBC_h

#include <CCSideSetBC.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;


class CCINSTurbZetaBC : public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSTurbZetaBC(const BCPackage& bc, int offset);
    virtual ~CCINSTurbZetaBC();
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSTurbZetaBC(const CCINSTurbZetaBC&);
    CCINSTurbZetaBC& operator=(const CCINSTurbZetaBC&);
    //@}

};

}

#endif // CCINSTurbZetaBC_h
