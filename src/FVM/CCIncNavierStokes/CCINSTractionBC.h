//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSTractionBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Traction boundary condition based on sidesets
//******************************************************************************
#ifndef CCINSTractionBC_h
#define CCINSTractionBC_h

#include <SideSetBC.h>

namespace Hydra {

// Forward declarations
struct BCPackage;

//! Traction boundary condition
class CCINSTractionBC : public SideSetBC {

  public:

    enum BCvar {
      TRVECX = 0,       // X-traction
      TRVECY,           // Y-traction
      TRVECZ            // Z-traction
    };

    //! \name Constructor/Destructor
    //@{
             CCINSTractionBC(const BCPackage& bc, int offset);
    virtual ~CCINSTractionBC();
    //@}

    //! Apply a prescribed traction BC
    void apply(UnsMesh& mesh, Real thetaFn, Real thetaFnp1, Real tn,
               Real tnp1, Real* rhs);

    //! Apply a prescribed traction BC
    void apply(UnsMesh& mesh, Real theta, Real time, Real* rhs);

    //! Get the prescribed variable
    BCvar getBCVar() const { return m_var; }

    //! Get velocity direction
    GlobalDirection getDirection() const {return m_direction;}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSTractionBC(const CCINSTractionBC&);
    CCINSTractionBC& operator=(const CCINSTractionBC&);
    //@}

    BCvar m_var;
    int   m_dir;  //!< direction
    GlobalDirection m_direction;
};

}

#endif
