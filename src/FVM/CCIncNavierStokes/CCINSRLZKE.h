//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSRLZKE.h
//! \author  Mark A. Christon
//! \date    Tue Sep 27 14:54:22 2016
//! \brief   Realizable K-Epsilon transport equations
//******************************************************************************
#ifndef CCINSRLZKE_h
#define CCINSRLZKE_h

#include <CCINSKE.h>

namespace Hydra {

class CCINSBoussinesqForce;
class CCINSTemperatureBC;
class CCINSTurbEpsBC;
class CCINSTurbKEBC;

//! Realizable K-Epsilon transport equations
class CCINSRLZKE : public CCINSKE {

  public:

    //! \name Constructor/Destructor
    //@{
            CCINSRLZKE(UnsMesh& mesh,
                       Control& control,
                       DualEdgeGradOp& m_edgeGradOp,
                       CCINSErrors& errors,
                       fileIO& io,
                       CCINSTransportVar& di,
                       vector<CCINSTemperatureBC*>& temperatureBC,
                       vector<CCINSTurbEpsBC*>& turbEpsBCs,
                       vector<CCINSTurbKEBC*>& turbKEBCs,
                       vector<CCINSBoussinesqForce*>& boussinesq_force,
                       LASolver& transport_solver,
                       LAMatrix& transport_A,
                       LAVector& transport_diagA,
                       LAVector& transport_b,
                       LAVector& transport_x,
                       CCINSAdapter& adapter);
    virtual ~CCINSRLZKE();
    //@}

    //**************************************************************************
    //! \name Virtual k-epsilon interface
    //! These functions are pure virtual functions that need to be
    //! implemented for each specific of k-epsilon model.
    //@{
    //! Calculate the turbulent viscosity for the Realizable k-e model
    //! Set contributions to the LHS matrix and to the RHS vector due to the
    //! linearized sources for the k-equation
    //!   \param[in,out] S       Reference to the LHS matrix
    //!   \param[in,out] rhs     Pointer to the RHS vector
    virtual void addSourcesKE(LAMatrix* S, Real* rhs);

    //! Set contributions to the LHS matrix and to the RHS vector due to the
    //! linearized sources for the e-equation
    //!   \param[in,out] S       Reference to the LHS matrix
    //!   \param[in,out] rhs     Pointer to the RHS vector
    virtual void addSourcesEps(LAMatrix* S, Real* rhs);

    //! Apply the Bouyancy correction
    virtual void applyBuoyancyCorrectionKE(Real* rhs);

    //! Apply the epsilon penalty based on the wall function type
    virtual void applyWallEpsPenalty(LAMatrix& S, LAVector& B, Real* rhs);

    //! Calculate the turbulent viscosity
    virtual void calcTurbulentViscosity();

    //! Calculate y* on a surface
    virtual void calcSurfYstar(const int nel, const int* edge_list, Real* ystar);

    //! Calculate y* in the volume
    virtual void calcYstar(Real* dist, Real* ystar);

    //! Set the wall viscosity according to the specific model
    virtual void setWallConductivity(int nedges, int* edge_list, DualEdge* edges,
                                     Real* kmol, Real* keff, Real* edgek);

    //! Set the wall viscosity according to the specific model
    virtual void setWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                  Real* mu, Real* mueff, Real* edgemu);

    //! Solve generic scalar transport equation(s)
    //! \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);
    //@}
    //**************************************************************************

  protected:

    //! Add the wall-function source terms to the LHS and RHS
    //!   \param[in,out] rhs  Pointer to the rhs vector
    //!   \param[in,out] S    Pointer to the lhs matrix
    void addScalableWallSourcesKE(LAMatrix* S, Real* rhs);

    //! Apply the epsilon penalty for scalable wall function
    void applyScalableWallEpsPenalty(LAMatrix& S, LAVector& B, Real* rhs);

    //! Calculate Cmu for the Realizable k-e model
    void calcElemCmu();

    //! Calculate the wall viscosity according to the specific model
    void calcScalableWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                   Real* mu, Real* mueff, Real* edgemu);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSRLZKE(const CCINSRLZKE&);
    CCINSRLZKE& operator=(const CCINSRLZKE&);
    //@}

    //**************************************************************************
    //! \name Model constants/parameters
    //@{
    Real m_o3rd;     //!< 1/3
    Real m_r6;       //!< sqrt(6)
    Real m_A0;       //!< Model constant
    Real m_C1max;    //!< Model constant
    Real m_C2;       //!< Model constant
    Real m_Cmumax;   //!< Maximum Cmu
    Real m_coeff1;   //!< epsilon limiter constant
    Real m_coeff2;   //!< epsilon limiter constant
    Real m_kappa;    //!< Model constant
    Real m_E;        //!< Model constant
    Real m_ln2;      //!< Model constant
    Real m_yp11;     //!< Model constant
    Real m_beta;     //!< Model constant
    Real m_alpha;    //!< Model constant
    //@}
    //**************************************************************************

};

}

#endif // CCINSRLZKE_h
