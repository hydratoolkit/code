//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSDensityBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Density boundary condition based on sidesets
//******************************************************************************
#ifndef CCINSDensityBC_h
#define CCINSDensityBC_h

#include <CCSideSetBC.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;


class CCINSDensityBC : public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSDensityBC(const BCPackage& bc, int offset);
    virtual ~CCINSDensityBC();
    //@}

    //! Set a user-defined BC value
    virtual Real setUserValue();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSDensityBC(const CCINSDensityBC&);
    CCINSDensityBC& operator=(const CCINSDensityBC&);
    //@}

};

}

#endif // CCINSDensityBC_h
