//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSTransportVar.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   DataIndex values for transport
//******************************************************************************
#ifndef CCINSTransportVar_h
#define CCINSTransportVar_h

#include <HydraTypes.h>

namespace Hydra {

struct CCINSTransportVar {
  //! \name DataIndex values for transport
  //@{
  //! Scalar quantities
  //! Note: If you put a new field above GHOST_VAR, make sure the initializations
  //! in the physics classes are also updated.
  DataIndex GHOST_VAR;      //!< Generic ghost variable for scalar transport
  DataIndex GHOST_NU;       //!< Ghost Turbulent eddy-viscosity for S-A model

  DataIndex DENSITY;        //!< Density at time n+1
  DataIndex DENSITYN;       //!< Density at time n
  DataIndex DENSITYNM1;     //!< Density at time n-1

  DataIndex PRESSURE;       //!< Pressure at time n+1
  DataIndex PRESSUREN;      //!< Pressure at time n

  DataIndex LAMBDA;         //!< Lagrange multipler (node-centered)

  DataIndex MASSFRAC;       //!< Species mass fraction at time n+1
  DataIndex MASSFRACN;      //!< Species mass fraction at time n
  DataIndex MASSFRACNM1;    //!< Species mass fraction at time n-1

  DataIndex TEMPERATURE;    //!< Temperature at time n+1
  DataIndex TEMPERATUREN;   //!< Temperature at time n
  DataIndex TEMPERATURENM1; //!< Temperature at time n-1

  DataIndex INTENERGY;      //!< Specific internal energy at time n+1
  DataIndex INTENERGYN;     //!< Specific internal energy at time n
  DataIndex INTENERGYNM1;   //!< Specific internal energy at time n-1

  DataIndex ENTHALPY;       //!< Enthalpy at time n+1
  DataIndex ENTHALPYN;      //!< Enthalpy at time n
  DataIndex ENTHALPYNM1;    //!< Enthalpy at time n-1

  DataIndex TURB_DIST;      //!< Distance function for wall modelling
  DataIndex EDGE_WALL_LIST; //!< List of external edges at walls
  DataIndex EDGE_WALL_VEL ; //!< Mesh velocity at the face
  DataIndex ELEM_WALL_LIST; //!< Unique list of elements attached to walls

  DataIndex TURB_KE;        //!< Turbulent kinetic energy at time n+1
  DataIndex TURB_KEN;       //!< Turbulent kinetic energy at time n
  DataIndex TURB_KENM1;     //!< Turbulent kinetic energy at time n-1

  DataIndex TURB_EPS;       //!< Turbulent dissipation rate at time n+1
  DataIndex TURB_EPSN;      //!< Turbulent dissipation rate at time n
  DataIndex TURB_EPSNM1;    //!< Turbulent dissipation rate at time n-1

  DataIndex TURB_OMEGA;     //!< Turbulent time scale rate at time level n+1
  DataIndex TURB_OMEGAN;    //!< Turbulent time scale rate at time level n
  DataIndex TURB_OMEGANM1;  //!< Turbulent time scale rate at time level n-1

  DataIndex TURB_PROD;      //!< Turbulent production (aka f) at time n+1
  DataIndex TURB_PRODN;     //!< Turbulent production (aka f) at time n
  DataIndex TURB_PRODNM1;   //!< Turbulent production (aka f) at time n-1

  DataIndex TURB_ZETA;      //!< Turbulent velocity ratio at time level n+1
  DataIndex TURB_ZETAN;     //!< Turbulent velocity ratio at time level n
  DataIndex TURB_ZETANM1;   //!< Turbulent velocity ratio at time level n-1

  DataIndex TURB_NU;        //!< Turbulent viscosity for S-A at time n+1
  DataIndex TURB_NUN;       //!< Turbulent viscosity for S-A at time n
  DataIndex TURB_NUNM1;     //!< Turbulent viscosity for S-A at time n-1

  DataIndex DUALEDGE_VEL;   //!< Dual-edge velocity at time n+1
  DataIndex DUALEDGE_VELN;  //!< Dual-edge velocity at time n

  DataIndex DIVERGENCE;     //!< Divergence

  //! Vector quantities
  DataIndex GHOST_GRAD_DENSITY; //!< Ghost buffer for gradient of density
  DataIndex VEL;                //!< Velocity at time level n+1
  DataIndex VELN;               //!< Velocity at time level n
  DataIndex VELNM1;             //!< Velocity at time level n-1
  DataIndex GHOST_VEL;          //!< ghost velocity
  DataIndex VEL_BC;             //!< Buffer for velocity BC's for projection

  DataIndex GHOST_VOL;          //!< ghost volume
  DataIndex GHOST_ASSEM_VOL;    //!< Inverse assembled volume for G-G smoother

  //! Gradient data
  DataIndex ELEM_GRADIENT;      //!< Element-centered gradient such as pressure
  DataIndex ELEM_GHOST_GRAD;    //!< Ghost buffer for gradients
  DataIndex EDGE_GRADIENT;      //!< Edge gradient

  //! Data required for the nodal equations: PPE, Distance, etc.
  DataIndex NODE_RHS;           //!< Node-based RHS for the PPE
  DataIndex NODEEQ_MAP;         //!< node to equation number map
  DataIndex NODEEQ_VAR;         //!< solution array for PPE

  //! Data required for the element equations
  DataIndex ELEMEQ_VAR;        //!< solution array for elements
  DataIndex ELEM_RHS;          //!< Element-based RHS for momentum equation
  DataIndex ELEM_DIAG;         //!< Element-based LHS diagonal term for momentum

  //! Interpolation data
  DataIndex EDGE_XI;         //!< \xi value in the owner element for the edge

  //! Data required for deformable mesh
  DataIndex NODE_COORD;      //!< Node-based original coordinates
  DataIndex NODE_VEL;        //!< Node-based velocity
  DataIndex NODE_DISP;       //!< Node-based displacement at current time
  DataIndex NODE_DISPN;      //!< Node-based displacement at previous time
  DataIndex VOLUME;          //!< Volume at n+1
  DataIndex VOLUMEN;         //!< Volume at n

  DataIndex ELEM_SCALAR_RHS; //!< Element-based RHS for scalar equation
  DataIndex TURB_NU_RHS;     //!< Element-based RHS for SA equation
  DataIndex TURB_KE_RHS;     //!< Element-based RHS for KE equation
  DataIndex TURB_EPSILON_RHS;//!< Element-based RHS for Epsilon equation
  DataIndex TURB_OMEGA_RHS;  //!< Element-based RHS for Omega equation

  //! Storage for material property update/evaluation
  DataIndex ELEM_MOL_VISCOSITY;       //!< Element-level viscosity at time n+1
  DataIndex ELEM_MOL_VISCOSITYN;      //!< Element-level viscosity at n
  DataIndex ELEM_TURB_VISCOSITY;      //!< Turbulent viscosity at time n+1
  DataIndex EDGE_VISCOSITY;           //!< Effective edge viscosity
  DataIndex EDGE_CONDUCTIVITY;        //!< Effective edge conductivity
  DataIndex EDGE_SCALED_CONDUCTIVITY; //!< Scaled effective edge conductivity
  DataIndex EDGE_DIFFUSIVITY;         //!< Effective edge diffusivity

  DataIndex F_MU;

  DataIndex C_MU;          //!< Cmu calculated at each cell for nonlinear k-e model
  DataIndex P_K;           //!< Pk -- Production Term (Reynolds Stress) for nonlinear k-e model
  DataIndex LIN_QUAD_COMP; //!< Difference in magnitude between linear and quadratic stress

  DataIndex LIN_REYN_STRESS;  //!< Quadratic Reynolds Stress for Nonlinear Model
  DataIndex QUAD_REYN_STRESS; //!< Quadratic Reynolds Stress for Nonlinear Model
  DataIndex TOT_REYN_STRESS;  //!< Quadratic Reynolds Stress for Nonlinear Model
  DataIndex NL_VEL_GRAD;      //!< Quadratic Reynolds Stress for Nonlinear Model

  //! Parallel communications data
  DataIndex GHOST_XC;
  DataIndex SEND_BUF;
  DataIndex RECV_BUF;

  //! Temporary storage:
  //! Note that TMP is simply a 'temporary' variable.
  DataIndex ELEM_TENSOR;   //!< Work array for element tensor
  DataIndex TMP;           //!< Just a temporary vector
  DataIndex TEMP_ELEM;     //!< element temporary vector
  DataIndex TEMP_NODE;     //!< node temporary vector
  DataIndex TEMP_EDGE;     //!< edge (internal + external) temporary vector

  //! RHS at start of non-linear iteration at time-level n
  DataIndex MOMENTUM_RHSN;  //!< momentum equation right hand side
  DataIndex ENERGY_RHSN;    //!< energy equation right hand side
  DataIndex TURBKE_RHSN;    //!< k-eps model, k-equation right hand side
  DataIndex TURBEPS_RHSN;   //!< k-eps model, eps-equation right hand side
  DataIndex TURBNU_RHSN;    //!< SA model, turbnu-equation right hand side
  DataIndex SPECIES_RHSN;   //!< species transport equation right hand side

  //! Volume fraction
  DataIndex VOLFRAC;          //!< Volume fraction at time n+1
  DataIndex VOLFRACN;         //!< Volume fraction at time n
  DataIndex EXTEDGE_VOLFRAC;  //!< External edge volume fraction at time n+1
  DataIndex EXTEDGE_VOLFRACN; //!< External edge volume fraction at time n
  //@}
};

}

#endif // CCINSTransportVar_h
