//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSSpalartAllmaras.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   The Spalart-Allmaras turbulence model for incompressible flow
//******************************************************************************
#ifndef CCINSSpalartAllmaras_h
#define CCINSSpalartAllmaras_h

#include <CCINSTurbulence.h>
#include <CCINSTurbNuBC.h>

namespace Hydra {

class CCINSSpalartAllmaras : public CCINSTurbulence {

  public:

    //! \name Constructor/Destructor
    //@{
            CCINSSpalartAllmaras(UnsMesh& mesh,
                                 Control& control,
                                 DualEdgeGradOp& edgeGradOp,
                                 CCINSErrors& errors,
                                 fileIO& io,
                                 CCINSTransportVar& di,
                                 vector<CCINSTurbNuBC*>& turbNuBCs,
                                 LASolver& transport_solver,
                                 LAMatrix& transport_A,
                                 LAVector& transport_diagA,
                                 LAVector& transport_b,
                                 LAVector& transport_x,
                                 CCINSAdapter& adapter);
    virtual ~CCINSSpalartAllmaras();
    //@}

    //! Set BC ghost values
    void applyTurbNuBCs(Real t, Real* var, Real* varg, bool* bcflag,
                        bool exchange);

    //! Assemble scalar transport equation for turbulent viscosity
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void assembleSystem(const CCINSIncParm& incParm);

    //! Calculate the turbulent viscosity for the Spalar-Allmaras model
    virtual void calcTurbulentViscosity();

    //! Virtual form transport Rhs function
    //!   \param[in] CCINSIncParm Time increment parameters
    //!   \param[in] SourceMode   Time-weighting/time-level for source terms
    //! No rhs as no transport equation for eddy viscosity in WALE model
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs=0);

    virtual void formRhsInc(const CCINSIncParm& incParm, Real* rhs=0);

    //! Set the wall viscosity according to the specific model
    virtual void setWallConductivity(int nedges, int* edge_list, DualEdge* edges,
                                     Real* kmol, Real* keff, Real* edgek);

    //! Set the wall viscosity according to the specific model
    virtual void setWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                  Real* mu, Real* mueff, Real* edgemu);

    //! Solve scalar transport equation for turbulent viscosity
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);

    //! Set contributions to the LHS matrix and to the RHS vector due to the
    //! linearized sources for the Spalart-Allmaras model
    //!   \param[in,out] S       Reference to the LHS matrix
    //!   \param[in,out] rhs     Pointer to the RHS vector
    virtual void addSources(LAMatrix* S, Real* rhs);

    //! Calculate the edge viscosity
    //!   \param[in]  rho    pointer to the density
    //!   \param[in]  mu     Pointer to the diffusion coefficient array
    //!   \param[in]  nut    Pointer to the turbulent diffusion coefficient array
    //!   \param[in]  mueff  Pointer to the effective diffusion coefficient array
    //!   \param[out] edgemu Pointer to the edge diffusion coefficient array
    void calcEdgeViscosity(Real* rho, Real* mu, Real* nut, Real* mueff,
                           Real* edgemu);

    //! Form transport Rhs function
    //!   \param[in]     incParm Increment parameter functions
    //!   \param[in]     mode    Mode of advection operator treatment
    //!   \param[in,out] rhs     Pointer to the rhs vector
    void m_formRhs(const CCINSIncParm& incParm, Real* rhs,
                   const FVMCCINS::SourceMode mode);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSSpalartAllmaras(const CCINSSpalartAllmaras&);
    CCINSSpalartAllmaras& operator=(const CCINSSpalartAllmaras&);
    //@}

    vector<CCINSTurbNuBC*>& m_turbNuBCs;

    //**************************************************************************
    //! \name Model constants/parameters
    //@{
    bool m_DESmode;
    bool m_curvCorr;
    Real m_cdes;
    Real m_Cb1;
    Real m_Cb2;
    Real m_Cv1;
    Real m_Cv13;
    Real m_Cv16;
    Real m_Cw1;
    Real m_Cw2;
    Real m_Cw3;
    Real m_Cw36;
    Real m_kappa;
    Real m_sigma;
    Real m_o3rd;
    Real m_o6th;
    Real m_C1;
    Real m_C2;
    //@}
};

}

#endif // CCINSSpalartAllmaras_h
