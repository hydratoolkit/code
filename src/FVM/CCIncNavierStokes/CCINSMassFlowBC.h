//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSMassFlowBC.h
//! \author  Mark A. Christon
//! \date    Tue Jul 16, 2016
//! \brief   Mass flow boundary condition based on sidesets
//******************************************************************************
#ifndef CCINSMassFlowBC_h
#define CCINSMassFlowBC_h

#include <CCSideSetBC.h>

namespace Hydra {

// Forward declarations
struct BCPackage;

class CCINSMassFlowBC : public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSMassFlowBC(const BCPackage& bc, int offset);
    virtual ~CCINSMassFlowBC();
    //@}

    //! Set the acceleration in the ghost buffer
    //!   \param[in] mesh     DataMesh
    //!   \param[in] DENSITY  Density DataIndex
    //!   \param[in] gd       Ghost buffer
    void setAcceleration(UnsMesh& mesh, DataIndex DENSITY, CVector& gd);

    //! Setup the edge-data for a given BC
    //!   \param[in] mesh     DataMesh
    //!   \param[in] bcflag   Bool flag to be set for Dirichlet BC's
    void setupEdgeBCs(UnsMesh& mesh, CBoolVector& bcflag);

    //! Set the ghost-data for a given symmetry velocity BC
    //!   \param[in] mesh     DataMesh
    //!   \param[in] DENSITY  Density DataIndex
    //!   \param[in] gd       Ghost buffer
    //!   \param[in] t        Total simulated time for load-curve lookup
    void setGhostBC(UnsMesh& mesh, DataIndex DENSITY, CVector& gd, Real t);

    //! Set the ghost-data & flag for a given symmetry velocity BC
    //!   \param[in] mesh     DataMesh
    //!   \param[in] DENSITY  Density DataIndex
    //!   \param[in] bcflag   Bool flag to be set for Dirichlet BC's
    //!   \param[in] gd       Ghost buffer
    //!   \param[in] t        Total simulated time for load-curve lookup
    void setGhostBC(UnsMesh& mesh, DataIndex Density, CBoolVector& bcflag,
                    CVector& gd, Real t);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSMassFlowBC(const CCINSMassFlowBC&);
    CCINSMassFlowBC& operator=(const CCINSMassFlowBC&);
    //@}

};

}

#endif
