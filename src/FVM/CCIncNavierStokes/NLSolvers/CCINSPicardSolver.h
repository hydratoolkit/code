//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/NLSolvers/CCINSPicardSolver.h
//! \author  Robert N. Nourgaliev
//! \date    Thu Oct 03 13:00:00 2012
//! \brief   Picard-iteration-based non-linear solver class for CCINSFlow
//******************************************************************************
#ifndef CCINSPicardSolver_h
#define CCINSPicardSolver_h

#include <CCINSFImplicitSolvers.h>
#include <CCINSSolutionOptions.h>

namespace Hydra {

class CCINSPicardSolver : public CCINSFImplicitSolvers {

  public:

    //! \name Constructor/Destructor
    //@{
    //! Constructor
    //!  \param[in] adapter Reference to the adapter for CCINSFlow class
    //!  \param[in] mesh    Reference to the mesh class
    //!  \param[in] cat     Reference to the category class for keyword access
    //!  \param[in] io      Reference to the fileIO object is used for all
    //!                     I/O operations
    //!  \param[in] di      Transport variable DataIndex structure
    //!  \param[in] incParm Reference to the time increment parameters
    //!  \param[in] GSVoffsets       Pointer to the list of global
    //!                              solution vector offsets
    //!  \param[in] GSVnames         Pointer to the list of global
    //!                              solution vector names
    //!  \param[in] GSVDataIndex     Pointer to the list of data indexes
    //!                              for variables in the global solution
    //!                              vector
    //!  \param[in] GSVIterDataIndex Pointer to the list of data indexes
    //!                              for variables in the global solution
    //!                              vector of old (current) nonlinear
    //!                              iteration
    //!  \param[in] GSVIncrDataIndex Pointer to the list of data indexes
    //!                              for variables in the global solution
    //!                              vector of increments
    //!  \param[in] GSVResDataIndex  Pointer to the list of data indexes
    //!                              for variables in the global solution
    //!                              vector of residuals
    //!  \param[in] GSVRhsDataIndex  Pointer to the list of data indexes
    //!                              for variables in the global solution
    //!                              vector of rhs
    CCINSPicardSolver(CCINSAdapter& adapter,
                      Category& cat,
                      UnsMesh& mesh,
                      fileIO& io,
                      CCINSTransportVar& di,
                      CCINSIncParm& incParm,
                      vector<int>&       GSVoffsets,
                      vector<string>&    GSVnames,
                      vector<DataIndex>& GSVDataIndex,
                      vector<DataIndex>& GSVIterDataIndex,
                      vector<DataIndex>& GSVIncrDataIndex,
                      vector<DataIndex>& GSVResDataIndex,
                      vector<DataIndex>& GSVRhsDataIndex) :
      CCINSFImplicitSolvers(adapter,
                            cat,
                            mesh,
                            io,
                            di,
                            incParm,
                            GSVoffsets,
                            GSVnames,
                            GSVDataIndex,
                            GSVIterDataIndex,
                            GSVIncrDataIndex,
                            GSVResDataIndex,
                            GSVRhsDataIndex) {}
    virtual ~CCINSPicardSolver() {}
    //@}

    //**************************************************************************
    //! \name Non-Linear SolverVirtual Interface
    //@{
    //! Check convergence of non-linear iterations
    //!   \return \c true if the iterations should continue
    virtual bool checkConvergence();

    //! Indicate a cutback has been detected/performed
    virtual bool didCutback() {return m_cutback;}

    //! Check status of a time-step/increment for convergence
    virtual bool isConverged() {return m_converged;}

    //!< Calculate the error for a non-linear iteration
    virtual void calcError();

    //! Initialize
    virtual void initialize();

    //! Setup the global solution vectors
    //!   \param[in] Ndof               Total number of degrees of freedom
    //!                                 in the solution vector
    //!   \param[in] GLOBAL_SOLUTION    Data index for the solution vector
    virtual void setupGlobalVar(const int          Ndof,
                                const DataIndex    GLOBAL_SOLUTION);

    //! Solve
    virtual void solve();

    //! Write the convergence file
    virtual void writeConvFile();

    //! Set the convergence file header
    virtual void writeConvHeader();

    //! Write the screen report
    //!   \param[in] ofs     Reference to output stream
    void writeReport(ostream& ofs);

    //! Write the screen report footer
    //!   \param[in] ofs     Reference to output stream
    void writeReportFooter(ostream& ofs);

    //! Write the screen report header
    //!   \param[in] ofs     Reference to output stream
    void writeReportHeader(ostream& ofs);
    //@}

  private:

    void calcCompositeError();
    void calcMaxError();

    //! Don't permit copy or assignment operators
    //@{
    CCINSPicardSolver(const CCINSPicardSolver&);
    CCINSPicardSolver& operator=(const CCINSPicardSolver&);
    //@}

};

} // end of "namespace Hydra"

#endif
