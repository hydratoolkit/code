//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/NLSolvers/CCINSFImplicitSolvers.h
//! \author  Robert N. Nourgaliev
//! \date    Thu Oct 03 13:00:00 2012
//! \brief   Picard-iteration for cell-centered incompressible Navier-Stokes
//******************************************************************************
#ifndef CCINSFImplicitSolvers_h
#define CCINSFImplicitSolvers_h

#include <CCINSNLSolver.h>
#include <UnsMesh.h>

namespace Hydra {

class CCINSFImplicitSolvers : public CCINSNLSolver {

  public:

    //! \name Constructor/Destructor
    //@{
    //! Constructor
    //!   \param[in] adapter Reference to the adapter for CCINSFlow class
    //!  \param[in] mesh    Reference to the mesh class
    //!  \param[in] cat     Reference to the category class for keyword access
    //!  \param[in] io      Reference to the fileIO object is used for all
    //!                     I/O operations
    //!  \param[in] di      Transport variable DataIndex structure
    //!  \param[in] incParm Reference to the time increment parameters
    //!  \param[in] GSVoffsets       Pointer to the list of global
    //!                              solution vector offsets
    //!  \param[in] GSVnames         Pointer to the list of global
    //!                              solution vector names
    //!  \param[in] GSVDataIndex     Pointer to the list of data indexes
    //!                              for variables in the global solution
    //!                              vector
    //!  \param[in] GSVIterDataIndex Pointer to the list of data indexes
    //!                              for variables in the global solution
    //!                              vector of old (current) nonlinear
    //!                              iteration
    //!  \param[in] GSVIncrDataIndex Pointer to the list of data indexes
    //!                              for variables in the global solution
    //!                              vector of increments
    //!  \param[in] GSVResDataIndex  Pointer to the list of data indexes
    //!                              for variables in the global solution
    //!                              vector of residuals
    //!  \param[in] GSVRhsDataIndex  Pointer to the list of data indexes
    //!                              for variables in the global solution
    //!                              vector of rhs
    CCINSFImplicitSolvers(CCINSAdapter& adapter,
                          Category& cat,
                          UnsMesh& mesh,
                          fileIO& io,
                          CCINSTransportVar& di,
                          CCINSIncParm& incParm,
                          vector<int>&       GSVoffsets,
                          vector<string>&    GSVnames,
                          vector<DataIndex>& GSVDataIndex,
                          vector<DataIndex>& GSVIterDataIndex,
                          vector<DataIndex>& GSVIncrDataIndex,
                          vector<DataIndex>& GSVResDataIndex,
                          vector<DataIndex>& GSVRhsDataIndex) :
      CCINSNLSolver(adapter, cat, io, di, incParm),
      m_GLOBAL_SOLUTION(UNREGISTERED),
      m_GLOBAL_CURRENT_ITER(UNREGISTERED),
      m_GLOBAL_INCREMENT(UNREGISTERED),
      m_GLOBAL_RESIDUAL(UNREGISTERED),
      m_GLOBAL_RHSN(UNREGISTERED),
      m_GSVoffsets(GSVoffsets),
      m_GSVnames(GSVnames),
      m_GSVDataIndex(GSVDataIndex),
      m_GSVIterDataIndex(GSVIterDataIndex),
      m_GSVIncrDataIndex(GSVIncrDataIndex),
      m_GSVResDataIndex(GSVResDataIndex),
      m_GSVRhsDataIndex(GSVRhsDataIndex),
      m_mesh(mesh)
        {}
    virtual ~CCINSFImplicitSolvers() {}
    //@}

    //**************************************************************************
    //! \name Non-Linear Solver Functions
    //@{
    //! Finalize
    virtual void finalize();

    //! Initialize
    virtual void initialize();

    //! Setup the global solution vectors
    //!  \param[in] Ndof               Total number of degrees of freedom
    //!                                in the solution vector
    //!  \param[in] GLOBAL_SOLUTION    Data index for the solution vector
    virtual void setupGlobalVar(const int Ndof,
                                const DataIndex GLOBAL_SOLUTION);
    //@}

  protected:

    //! Map variables inside a given global vector
    //!   \param[in] gvIndex  Global vector data index
    //!   \param[in] varsIndx Pointer to the array of variables
    //!                       comprising the given global vector
    //!   \param[in] label    Identifying label for variable names
    void mapGlobalVec(const DataIndex gvIndex,
                      vector<DataIndex>& varsIndx,
                      const string& label);

    //! Register given global vector
    //!   \param[out] gvIndex  Pointer to the global vector data index
    //!   \param[in]  varsIndx Pointer to the array of variables
    //!                       comprising the given global vector
    //!   \param[in]  label    Identifying label for variable names
    void registerGlobalVec(DataIndex& gvIndex,
                           vector<DataIndex>& varsIndx,
                           const string& label);

    //**************************************************************************
    //! \name Global solution vector data indices and related
    //@{
    DataIndex m_GLOBAL_SOLUTION;     //!< Global solution vector
    DataIndex m_GLOBAL_CURRENT_ITER; //!< Global solution current iteration
    DataIndex m_GLOBAL_INCREMENT;    //!< Global solution increment
    DataIndex m_GLOBAL_RESIDUAL;     //!< Global residual vector
    DataIndex m_GLOBAL_RHSN;         //!< Global RHS vector, evaluated
                                         //   at (n) time level
    //! The list of global solution vector sizes
    vector<int> m_GSVsizes;

    //! The list of global solution vector offsets
    vector<int>& m_GSVoffsets;

    //! The list of global solution vector names
    vector<string>& m_GSVnames;

    //! The list of data indexes for variables in the global solution vector
    vector<DataIndex>& m_GSVDataIndex;

    //! The list of data indexes for variables in the global
    //  solution vector of current nonlinear iteration
    vector<DataIndex>& m_GSVIterDataIndex;

    //! The list of data indexes for variables in the global
    //  solution vector of increments
    vector<DataIndex>& m_GSVIncrDataIndex;

    //! The list of data indexes for variables in the global
    //  solution vector of residuals
    vector<DataIndex>& m_GSVResDataIndex;

    //! The list of data indexes for variables in the global
    //  solution vector of rhs
    vector<DataIndex>& m_GSVRhsDataIndex;
    //@}

    UnsMesh& m_mesh; //!< Pointer to the finite element mesh

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSFImplicitSolvers(const CCINSFImplicitSolvers&);
    CCINSFImplicitSolvers& operator=(const CCINSFImplicitSolvers&);
    //@}

};

} // end of "namespace Hydra"

#endif
