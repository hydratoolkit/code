//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/NLSolvers/CCINSNLSolver.h
//! \author  Robert N. Nourgaliev
//! \date    Thu Oct 03 13:00:00 2012
//! \brief   Nonlinear solver class for hybrid Navier-Stokes solver
//******************************************************************************
#ifndef CCINSNLSolver_h
#define CCINSNLSolver_h

#include <NonLinearSolver.h>
#include <CCINSAdapter.h>
#include <FVMCCINSControl.h>
#include <CCINSTransportVar.h>
#include <CCINSIncParm.h>
#include <fileIO.h>

namespace Hydra {

class CCINSNLSolver : public NonLinearSolver {

  public:

    //! \name Constructor/Destructor
    //@{
    //! Constructor
    //!   \param[in] adapter Reference to the adapter for CCINSFlow class
    //!   \param[in] cat     Reference to the category class for keyword access
    //!   \param[in] io      Reference to the fileIO object is used for all
    //!                      I/O operations
    //!   \param[in] di      Transport variable DataIndex structure
    //!   \param[in] incParm Reference to the time increment parameters
    CCINSNLSolver(CCINSAdapter& adapter, Category& cat, fileIO& io,
                  CCINSTransportVar& di, CCINSIncParm& incParm) :
       NonLinearSolver(),
       m_adapter(adapter),
       m_cat(cat),
       m_io(io),
       m_di(di),
       m_incParm(incParm),
       m_converged(false),
       m_cutback(false),
       m_solveP(true),
       m_Ndof(0),
       m_Iters(0),
       m_GIters(0)
         {}
     virtual ~CCINSNLSolver() {}
    //@}

    //**************************************************************************
    //! \name Non-Linear Solver Virtual Interface
    //@{
    //! Check convergence of non-linear iterations
    //!   \return \c true if the iterations should continue
    virtual bool checkConvergence() = 0;

    //! Indicate a cutback has been detected/performed
    virtual bool didCutback() {return false;}

    //! Check status of a time-step/increment for convergence
    virtual bool isConverged() {return true;}

    //! Get the number of non-linear iterations taken
    virtual int getIters() {return m_Iters;}

    //! Check the convergence of the pressure
    virtual bool isPConverged() { return !m_solveP; }

    //! Set the pressure convergence flag
    virtual void setSolveP(bool flag) { m_solveP=flag; }

    //! Setup the global solution vectors
    //!   \param[in] Ndof               Total number of degrees of freedom
    //!                                 in the solution vector
    //!   \param[in] GLOBAL_SOLUTION    Data index for the solution vector
    virtual void setupGlobalVar(const int Ndof,
                                const DataIndex GLOBAL_SOLUTION) = 0;

    //! Setup
    virtual void setup();

    //! Write the convergence file
    virtual void writeConvFile() = 0;

    //! Set the convergence file header
    virtual void writeConvHeader() = 0;

    //! Write the screen report header
    //!   \param[in] ofs     Reference to output stream
    virtual void writeReport(ostream& ofs) = 0;

    //! Write the screen report footer
    //!   \param[in] ofs     Reference to output stream
    virtual void writeReportFooter(ostream& ofs) = 0;

    //! Write the screen report header
    //!   \param[in] ofs     Reference to output stream
    virtual void writeReportHeader(ostream& ofs) = 0;

    //! Return the error norm for a particular field
    virtual Real getFieldErrorNorm(int i) { return m_DofErr[i]; }

    //! Return the index mapped to pressure
    virtual int getPressureIndex() { return m_DofErr.size() - 1 ; }
    //@}

  protected:

    //! Reference to the adapter for CCINSFlow class
    CCINSAdapter& m_adapter;

    Category& m_cat; //!< Reference to the category class for keyword access

    fileIO& m_io; //!< The fileIO object is used for all I/O operations

    CCINSTransportVar& m_di; //!< Object with transport DataIndex values

    CCINSIncParm& m_incParm; //!< Object with time increment parameters

    bool m_conv;      //!< true : write non-linear convergence history to a file
    bool m_diag;      //!< true : write convergence diagnostics to the screen
    bool m_converged; //!< true = time-step converged, false = didn't converge
    bool m_cutback;   //!< true = previous time-step cutback
    bool m_solveP;    //!< true = solve pressure, false = do not solve pressure

    //! The total number of degrees of freedom in the solution vector
    int  m_Ndof;

    //**************************************************************************
    //! \name Convergence Data
    //@{
    int  m_itmax;  //!< Maximum number of non-linear iterations
    int  m_Iters;  //!< The number of non-linear iterations taken
    long m_GIters; //!< The global number of non-linear iterations taken

    Real m_eps;    //!< Convergence criteria for non-linear iterations
    Real m_err;    //!< Error norm for non-linear increments
    Real m_pnorm;  //!< dp L2 norm, required to compute error norms for subcycle

    vector<Real> m_DofErr;  //!< Error norm for each variable
    //@}

    //! Return the index for the pressure field

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSNLSolver(const CCINSNLSolver&);
    CCINSNLSolver& operator=(const CCINSNLSolver&);
    //@}

};

} // end of "namespace Hydra"

#endif
