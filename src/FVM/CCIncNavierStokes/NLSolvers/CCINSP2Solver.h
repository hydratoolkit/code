//******************************************************************************
/*!
  \file    src/FVM/CCIncNavierStokes/NLSolvers/CCINSP2Solver.h
  \author  Robert N. Nourgaliev
  \date    Thu Oct 03 13:00:00 2012
  \brief   Projection-based hybrid Naver-Stokes interface
 */
//******************************************************************************
#ifndef CCINSP2Solver_h
#define CCINSP2Solver_h

#include <CCINSNLSolver.h>
#include <CCINSSolutionOptions.h>

namespace Hydra {

class CCINSP2Solver : public CCINSNLSolver {

  public:

    /*! Constructor
     * \param[in] adapter Reference to the adapter for CCINSFlow class
     * \param[in] cat     Reference to the category class for keyword access
     * \param[in] io      Reference to the fileIO object is used for all
     *                    I/O operations
     * \param[in] di      Transport variable DataIndex structure
     * \param[in] incParm Reference to the object with time increment parameters
     */
     CCINSP2Solver(CCINSAdapter& adapter,
                   Category& cat,
                   fileIO& io,
                   CCINSTransportVar& di,
                   CCINSIncParm& incParm) :
       CCINSNLSolver(adapter,cat,io,di,incParm) {}

     //! Destructor
     virtual ~CCINSP2Solver() {}

    //**************************************************************************
    //! \name Non-Linear SolverVirtual Interface
    //@{

    /*! Check convergence of non-linear iterations
     *  \return \c true if the iterations should continue
     */
    virtual bool checkConvergence() {return true;}

    //! Finalize
    virtual void finalize();

    //! Get the number of non-linear iterations taken
    virtual int getIters() {return 1;}

    //! Initialize
    virtual void initialize();

    /*! Setup the global solution vectors
     *  \param[in] Ndof               Total number of degrees of freedom
     *                                in the solution vector
     *  \param[in] GLOBAL_SOLUTION    Data index for the solution vector
     */
    virtual void setupGlobalVar(const int /*Ndof*/,
                                const DataIndex /*GLOBAL_SOLUTION*/) {}

    //! Setup
    virtual void setup();

    //! Solve
    virtual void solve();

    //! Write the convergence file
    virtual void writeConvFile() {}

    //! Set the convergence file header
    virtual void writeConvHeader() {}

    /*! Write the screen report header
     *  \param[in] ofs     Reference to output stream
     */
    virtual void writeReport(ostream& /*ofs*/) {}

    /*! Write the screen report footer
     *  \param[in] ofs     Reference to output stream
     */
    void writeReportFooter(ostream& /*ofs*/) {}

    /*! Write the screen report header
     *  \param[in] ofs     Reference to output stream
     */
    void writeReportHeader(ostream& /*ofs*/) {}

    //@}

  private:

    // Don't permit copy or assignment operators
    CCINSP2Solver(const CCINSP2Solver&);
    CCINSP2Solver& operator=(const CCINSP2Solver&);

};

} // end of "namespace Hydra"

#endif
