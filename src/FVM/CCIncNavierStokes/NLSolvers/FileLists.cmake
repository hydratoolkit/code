# ############################################################################ #
#
# Library: CCINSNLSolver
# File Definition File 
#
# ############################################################################ #

# Source Files

set(CCINSNLSolver_SOURCE_FILES
            CCINSNLSolver.C
	    CCINSFImplicitSolvers.C
	    CCINSPicardSolver.C
	    CCINSP2Solver.C
)


