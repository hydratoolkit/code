//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSSolverErrors.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Error handler for CCINSFlow
//******************************************************************************
#ifndef CCINSSolverErrors_h
#define CCINSSolverErrors_h


#include <CCINSErrorTypes.h>

namespace Hydra {

class UnsMesh;
class fileIO;

struct CCINSSolverErrorTable
{
  CCINSSolverError errType;
  int occurrences;
  const char* msg;
  bool performGlobalSum;
};

//! Error handler for cell-centered Incompressible Navier Stokes
class CCINSSolverErrors {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSSolverErrors(UnsMesh& mesh, fileIO& io);
    virtual ~CCINSSolverErrors();
    //@}

    //! Add the errors based on the UCCINSFlow physics enumerator value
    void add(CCINSSolverError e);

    //! Add the errors based on the UCCINSFlow physics enumerator value
    void add(CCINSSolverError e, int numerr);

    //! Get the total number of errors for UCCINS Flow Physics category
    static int getSize();

    //! Gather the errors from UCCINS Flow Physics category and
    void gather(int* S, int offset);

    //! Prepare Error Report in UCCINS Flow Physics category
    bool report(int* W, int offset);

    //! Get the maximum allowed error counts for UCCINSFlow Physics
    int getOccurrences(CCINSSolverError e);

    //! Get the error message corresponding to UCCINSFlow Physics
    const char* getMessage(CCINSSolverError e);

    //! Reset the error counts to 0 corresponding to the UCCINSFlow
    void resetCount(CCINSSolverError e);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSSolverErrors(const CCINSSolverErrors&);
    CCINSSolverErrors& operator=(const CCINSSolverErrors&);
    //@}

    UnsMesh& m_mesh;
    fileIO& m_io;

    //! Array for UCCINSFlow Physics Errors
    int m_physerr[CCINS_TOTAL_ERRORS];
};

}

#endif // CCINSSolverErrors_h
