//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSFlow.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Cell-centered incompressible Naver-Stokes flow solver
//******************************************************************************
#ifndef CCINSFlow_h
#define CCINSFlow_h

#include <vector>
#include <iomanip>

#include <HydraTypes.h>
#include <FieldStatistics.h>
#include <CCclaw.h>
#include <CCINSBlockICs.h>
#include <CCINSSurfChem.h>
#include <CCINSErrors.h>
#include <CCINSEnthalpy.h>
#include <CCINSIntEnergy.h>
#include <CCINSIncParm.h>
#include <CCINSLDKMKsgs.h>
#include <CCINSKE.h>
#include <CCINSRLZKE.h>
#include <CCINSRNGKE.h>
#include <CCINSSTDKE.h>
#include <CCINSNLKE.h>
#include <CCINSSSTKW.h>
#include <CCINSMomentum.h>
#include <CCINSParm.h>
#include <CCINSWALE.h>
#include <CCINSSmagorinsky.h>
#include <CCINSSpalartAllmaras.h>
#include <CCINSSpeciesMassFrac.h>
#include <CCINSSpeciesDiffusionFluxBC.h>
#include <CCINSTemperature.h>
#include <CCINSTractionBC.h>
#include <CCINSHeatFluxBC.h>
#include <CCINSMassFlowBC.h>
#include <CCINSMassFluxBC.h>
#include <PeriodicSideBC.h>
#include <CCINSPVDepBC.h>
#include <CCINSTransportVar.h>
#include <CCINSAdapter.h>
#include <CCINSPicardSolver.h>
#include <CCINSP2Solver.h>
#include <CCINSUserEpsilonIC.h>
#include <CCINSUserOmegaIC.h>
#include <CCINSUserTemperatureIC.h>
#include <CCINSUserTKEIC.h>
#include <CCINSUserTurbnuIC.h>
#include <CCINSUserVelocityIC.h>
#include <CCINSVolumeFlowBC.h>
#include <CCINSVolFrac.h>
#include <CCINSVolFracBC.h>
#include <nonlinear_krylov_accelerator.h>
#include <SourcePackage.h>

namespace Hydra {

// Forward Declarations
class UnsMesh;
class OutputRequest;
class Timer;
class fileIO;
class Control;
class Category;
class LASolver;
class LAMatrix;
class LAVector;

class CCINSBodyForce;
class CCINSBoussinesqForce;
class CCINSGravityForce;
class CCINSDensityBC;
class CCINSDisplacementBC;
class CCINSFsiPenalty;
class CCINSHeatFluxBC;
class CCINSHydrostat;
class CCINSMassFluxBC;
class CCINSPassiveOutflowBC;
class CCINSPorousDrag;
class CCINSPressureBC;
class CCINSPressureOutflowBC;
class CCINSHeatSource;
class CCINSSpeciesMassFracBC;
class CCINSSpeciesDiffusionFluxBC;
class CCINSSurfDispBC;
class CCINSSymmVelBC;
class CCINSTemperatureBC;
class CCINSTractionBC;
class CCINSTurbDistBC;
class CCINSTurbEpsBC;
class CCINSTurbKEBC;
class CCINSTurbNuBC;
class CCINSTurbOmegaBC;
class CCINSTurbProdBC;
class CCINSTurbZetaBC;
class CCINSUserEpsilonIC;
class CCINSUserOmegaIC;
class CCINSUserTKEIC;
class CCINSUserTemperatureIC;
class CCINSUserTurbnuIC;
class CCINSUserVelocityIC;
class CCINSVelocityBC;

struct SurfChemInfo;

//! BC Var's for parsing/creation
enum CCINSBCVar {CCINS_DENSITYBC = 0,
                 CCINS_DISTBC,
                 CCINS_ENTHALPYBC,
                 CCINS_INTENERGYBC,
                 CCINS_EPSBC,
                 CCINS_MASSFRACBC,
                 CCINS_PRESSUREBC,
                 CCINS_TEMPERATUREBC,
                 CCINS_TURBNUBC,
                 CCINS_TKEBC,
                 CCINS_OMEGABC,
                 CCINS_VOLFRACBC
};

//! Initial condition package
struct CCINSICPackage {
  Real velx;
  Real vely;
  Real velz;
  Real temp;
  Real eps;
  Real massfrac;
  Real omega;
  Real tke;
  Real turbnu;
  Real volfrac;
  CCINSICPackage() : velx(0.0),
                     vely(0.0),
                     velz(0.0),
                     temp(0.0),
                     eps(0.0),
                     massfrac(0.0),
                     omega(0.0),
                     tke(0.0),
                     turbnu(0.0),
                     volfrac(0.0) {}
};

//! Derived physics class for the hybrid FEM/FVM Navier-Stokes solver with
//! This is a pressure-based solver.

class CCINSFlow : public CCclaw, protected CCINSAdapter {

  public:

    //! Statistics Accumulator Enumeration
    enum CCINSAccumulator {CCINS_MEANPRESSURE = 0,
                           CCINS_MEANDENSITY,
                           CCINS_MEANVELOCITY,
                           CCINS_MEANTEMPERATURE,
                           CCINS_MEANVORTICITY,
                           CCINS_MEANHELICITY,
                           CCINS_MEANENSTROPHY,
                           CCINS_MEANHEATFLUX,
                           CCINS_MEANSHEARTRACTION,
                           CCINS_MEANNORMALTRACTION,
                           CCINS_MEANTRACTION,
                           CCINS_MEANWALLSHEAR,
                           CCINS_MEANTURBVISCOSITY,
                           CCINS_TKE,
                           CCINS_REYNOLDSSTRESS,
                           CCINS_COVDENSPRES,
                           CCINS_COVPRESVEL,
                           CCINS_VARPRESSURE,
                           CCINS_VARTEMPERATURE,
                           NUM_OF_CCINS_ACCUMULATORS
    };

    //! \name Constructor/Destructor
    //@{
             CCINSFlow();
    virtual ~CCINSFlow();
    //@}

    //**************************************************************************
    //! \name Virtual interface for Hybrid FEM/FVM Navier-Stokes Solver
    //@{
    //! Check boundary conditions for this physics
    virtual void checkBCs();

    //! Check the health of the code during solution phase
    virtual bool codeStatus();

    //! Allocate and setup physics specific variables
    virtual void setup();

    //! Initialize the physics
    virtual void initialize();

    //! Finalize the physics
    virtual void finalize();

    //! Solve the physics problem
    virtual void solve();

    //! Register all solution variables
    virtual void registerData();

    //! Register surface chemistry variables
    void registerSurfChem();

    //! Echo physics-specific options
    virtual void echoOptions(ostream& ofs);

    //! Setup the internal set Id for BC's with a merged set Id
    virtual void setupMergedSetBCs();
    //@}

    //**************************************************************************
    //! \name Body Forces
    //@{
    //! Add a body force -- simple constructor
    void addBodyForce();

    //! Add a body force
    void addBodyForce(int setid, int tblid, Real amp, Real* en);

    //! Add a Boussinesq body force -- simple constructor
    void addBoussinesqForce();

    //! Add a Boussinesq body force
    void addBoussinesqForce(int setid, int tblid, Real amp, Real* en);

    //! Add a gravity body force
    void addGravityForce(int setid, int tblid, Real amp, Real* en);

    //! Add a Porous drag (body) force -- simple constructor
    void addPorousDrag();

    //! Add a Porous drag (body) force
    void addPorousDrag(int setid, int tblid, Real amp);

    //! Echo the body foces
    void echoBodyForces(ostream &ofs);

    //! Echo the porous drag body foces
    void echoPorousDrag(ostream &ofs);

    //! Get a body force
    CCINSBodyForce& getBodyForce(int i) {return *(m_body_force[i]);}

    //! Get a Boussinesq body force
    CCINSBoussinesqForce& getBoussinesqForce(int i) {
      return *(m_boussinesq_force[i]);
    }

    //! Get the number of body force objects
    int numBodyForce() {return m_body_force.size();}

    //! Get the number of Boussinesq body force objects
    int numBoussinesqForce() {return m_boussinesq_force.size();}
    //@}

    //**************************************************************************
    //! \name Boundary Conditions
    //@{
    //! Add a Dirichlet boundary condition
    void addDirichletBC(const BCPackage& bc, int bcvar);

    //! Add a user-defined Dirichlet boundary condition
    void addUserDirichletBC(const BCPackage& bc, int bcvar);

    //! Add a displacement boundary condition
    void addDisplacementBC(const BCPackage& bc);

    //! Add a boundary condition for various Dirichlet BC types
    template<class BC>
      void addBC(vector<BC*>& bcv, const BCPackage& bc) {
        bcv.push_back(new BC(bc, 0));
    }

    //! Add a heat flux BC
    void addHeatFluxBC(const BCPackage& bc);

    //! Add a hydrostatic pressure
    void addHydrostat(const BCPackage& bc, Vector& pt, bool useCoord);

    //! Add a mass flow BC
    void addMassFlowBC(const BCPackage& bc);

    //! Add a mass flux BC
    void addMassFluxBC(const BCPackage& bc);

    //! Add a periodic boundary condition
    void addPeriodicSideBC(int master_ssid, int master_set,
                           int slave_ssid,  int slave_set );

    //! Add list of species to transport
    void addTransportSpecies(map<int,int> transportSpecies) {
      m_transportSpecies = transportSpecies;
    }

    //! Add passive outflow boundary conditions
    void addPassiveOutflowBC(const BCPackage& bc);

    //! Add pressure outflow boundary conditions
    void addPressureOutflowBC(const BCPackage& bc);

    //! Add pressure-volume dependent boundary conditions
    void addPVDepBC(const BCPackage& bc);

    //! Add a species mass fraction flux boundary condition
    void addSpeciesDiffusionFluxBC(const BCPackage& bc);

    //! Add symmetry velocity boundary conditions
    void addSymmVelBC(const BCPackage& bc);

    // Add in a surface-chemistry interface
    void addSurfChem(const SurfChemInfo& msci, UnsMesh* mesh);

    //! Add traction boundary conditions
    void addTractionBC(const BCPackage& bc);

    //! Add velocity boundary conditions
    void addVelocityBC(const BCPackage& bc);

    //! Add a volume flow BC
    void addVolumeFlowBC(const BCPackage& bc);

    //! Apply homogeneous EBC's for the Lagrange multipler
    void applyLambdaBCs();

    //! Apply pressure BC's and peg hydrostatic pressure
    void applyPressureBCs();

    //! Apply incremental pressure BC's for the Lagrange multiplier
    void applyPressureIncBCs();

    //! Apply penalty multiplier to the node-based PPE operator
    void applyPPEPenalty();

    //! Apply penalty multiplier to the node-based PPE operator
    void applyPPEFsiPenalty();

    //! Create interface conditions for interface sidesets
    void createIFBCs();

    //! Echo boundary conditions
    void echoBCs(ostream& ofs);

    //! Echo mesh displacement conditions
    void echoDisplacementBCs(ostream& ofs);

    //! Echo all prescribed Dirichlet conditions
    void echoDirichletBCs(ostream& ofs);

    //! Echo prescribed Dirichlet condition for various scalar CCINSBC types
    template<class CCINSBC>
    void echoScalarSetBC(const vector<CCINSBC*>& bcv, ostream& ofs) const {
      typename vector<CCINSBC*>::const_iterator bc;
      for (bc=bcv.begin(); bc!=bcv.end(); bc++)
        (*bc)->echo(ofs);
    }

    //! Echo prescribed Dirichlet condition for various vector CCINSBC types
    template<class CCINSBC>
    void echoVectorSetBC(const vector<CCINSBC*>& bcv, ostream& ofs) const {
      typename vector<CCINSBC*>::const_iterator bc;
      for (bc=bcv.begin(); bc!=bcv.end(); bc++) {
        string dirLabel = "";
        GlobalDirection dir = (*bc)->getDirection();
        switch (dir) {
          case GLOBAL_XDIR: dirLabel = "X"; break;
          case GLOBAL_YDIR: dirLabel = "Y"; break;
          case GLOBAL_ZDIR: dirLabel = "Z"; break;
        }
        (*bc)->echo(ofs, dirLabel);
      }
    }

    //! Echo the heat flux BC's
    void echoHeatFluxBCs(ostream &ofs);

    //! Echo the heat sources
    void echoHeatSources(ostream &ofs);

    //! Echo the mass flow BC's
    void echoMassFlowBCs(ostream &ofs);

    //! Echo the mass flux BC's
    void echoMassFluxBCs(ostream &ofs);

    //! Echo pressure BC's, hydrotstat, contraints, etc.
    void echoPressureBCs(ostream& ofs);

    //! Echo passive and pressure outflow BCs
    void echoOutflowBCs(ostream& ofs);

    //! Echo periodic BCs
    void echoPeriodicSideBCs(ostream& ofs);

    //! Echo surface chemistry interfaces
    void echoSurfChem(ostream& ofs);

    //! Echo traction BCs
    void echoTractionBCs(ostream& ofs);

    //! Echo the volume flow BC's
    void echoVolumeFlowBCs(ostream &ofs);

    //! Query memory used by boundary conditions in bytes
    Real getBCMemory();

    //! Query memory used by boundary condition for various BC types
    template<class BC>
      void queryBCMem(const vector<BC*>& bcv, Real& mem) {
      typename vector<BC*>::const_iterator bc;
      for (bc=bcv.begin(); bc!=bcv.end(); bc++)
        mem += (*bc)->getMemory();
    }

    //! Setup the symmetry velocity BC's
    void setupSymmVelBCs();

    //! Utility to identify unique nodes on sidesets for nodal BC's
    void setupUniqueNodeLists();

    //! Setup the velocity boundary condition flag  -- used for LS gradient
    CBoolVector setupVelBCFlag();
    //@}

    //**************************************************************************
    //! \name Communications functions & related utilities
    //@{
    //! Swap equation number for parallel
    void swapNodeEqNumber(int* nodeeqmap);

    //! Swap the nodes marked with PPE penalty conditions
    void swapNodeMarkPPE(int* nodeBc);
    //@}

    //**************************************************************************
    //! \name Coupled-physics interface
    //@{
    //! Calculate exported fields for co-processing
    void calcExportedFields() {}

    //! Compute the nodal capacitance for co-processing
    void calcNodalCapacitance(vector<int>& fieldId);

    //! Compute the nodal mass for co-processing
    void calcNodalMass(vector<int>& fieldId);

    //! Compute the normal heat flux for co-processing
    void calcNormalHeatFlux(vector<int>& fieldId);

    //! Compute the traction vector for co-processing
    void calcTraction(vector<int>& fieldId);
    //@}

    //**************************************************************************
    //! \name Deforming mesh functions
    //@{
    //! calculate the dual-edge mesh velocity
    void calcMeshDualEdgeVelocity();

    //! calculate the mesh velocity at the wall face-center
    void calcWallMeshVelocity(CVector& velmesh);

    //! Deform the mesh
    void deformMesh();

    //! Filter hourglass modes
    void filterHourglass(CVector& dispInc);

    //! Seed the ortho-vectors
    void seedOvectors(Real* ke, Real* kii, Real* Kphi, vector<Real*>& phi,
                      Real* coord, Real* coordOrig, Real* dispN, Real* dispInc);

    //! Spring mat-vec
    void springMatVec(Real* ke, Real* kii, Real* phi, Real* Kphi);

    //! Initialize deforming mesh
    void initDeformMesh();

    //! Setup the original coordinates for deforming mesh
    void setupOriginalCoordinates();

    //! Setup the deforming mesh parameters
    void setupDeformMeshParam();

    //! Update all discrete operators from n to n+1 for deformable case
    void updateOperators();

    //! Update displacement and coordinates
    void updateDisplacementAndCoordinates(Real& numInc,
                                          Real scale,
                                          CVector& coordOrig,
                                          CVector& dispInc,
                                          CVector& disp,
                                          CVector& dispN,
                                          CVector& coord);
    //@}

    //**************************************************************************
    //! \name Gradient operators
    //@{
    //! Compute edge-centered gradient from the FEM element-centered gradient
    void calcEdgeGradFEM(const DataIndex ELGRAD, const DataIndex EDGRAD);

    //! Setup the element-level B-matrices
    void setupBmatrix();
    //@}

    //**************************************************************************
    //! \name Initial Conditions
    //@{

    //! Add a block velocity initial condition
    void addBlockICs() {m_blockICs.push_back(new CCINSBlockICs);}

    //! Load a block velocity IC object
    CCINSBlockICs* getBlockICs(int i) {return m_blockICs[i];}

    //! How many block velocity IC's are loaded
    int numBlockICs() {return m_blockICs.size();}

    //! Echo the block velocity IC's
    void echoBlockICs(ostream& ofs, int etype);

    //! Add a user-defined dissipation rate initial condition
    void addUserEpsilonIC();

    //! Add a user-defined inverte dissipation time-scale initial condition
    void addUserOmegaIC();

    //! Add a user-defined temperature initial condition
    void addUserTemperatureIC();

    //! Add a user-defined TKE initial condition
    void addUserTKEIC();

    //! Add a user-defined Spalart-Allmaras initial condition
    void addUserTurbnuIC();

    //! Add a user-defined velocity initial condition
    void addUserVelocityIC();

    //! Driver for application of initial conditions
    void applyICs();

    //! Apply block-based initial conditions
    void applyBlockICs();

    //! Apply global initial conditions
    void applyGlobalICs();

    //! Apply rigid material initial conditions for CHT
    void applyRigidICs();

    //! Apply user-defined initial conditions
    void applyUserICs();

    //! Echo initial conditions
    void echoICs(ostream& ofs);

    //! Set the prescribed initial conditions
    void setICs(CCINSICPackage& ic);
    //@}

    //**************************************************************************
    //! \name Linear Algebra
    //@{
    //! Calculate the row size on processor
    void calcNodeRowSizeOn();

    //! Calculate the row size off processor
    void calcNodeRowSizeOff();

    //! Finalize the linear equation solvers, matrices and vectors
    void finalLinearSolvers();

    //! Initialize the linear equation solvers, matrices and vectors
    void initLinearSolvers();

    //! Initialize the linear equations solver for the momentum equation
    void initLinearSolverMom(Category* const pcat,
                             LASolverFactory* const factory,
                             const int Nel_glob,
                             LASolverStatus* const status);


    //! Initialize the linear equation solver for PPE
    void initLinearSolverPPE(Category* const pcat,
                             LASolverFactory* const factory,
                             const int Nnp_glob,
                             LASolverStatus* const status);

    //! Initialize the linear equations solver for transport equations
    void initLinearSolverTrans(Category* const pcat,
                               LASolverFactory* const factory,
                               const int Nel_glob,
                               LASolverStatus* const status);

    //! Perform a dummy solve to initialize the PPE solver on restart
    void initPPESolver();

    //! Map global node Id's to equation numbers
    void mapNodeEqNumber();

    //! Mark the nodes associated with nodal penalty conditions
    void markPressureNodePenalty(int* nproc, int* nodebc, int* send_buf,
                                 int* recv_buf);

    //! Mark the nodes associated with nodal penalty conditions
    void markDistanceNodePenalty(int* nproc, int* nodebc,
                                 int* send_buf, int* recv_buf);
    //@}

    //**************************************************************************
    //! \name Material property evaluation/update
    //@{
    //! Check material property dependencies
    void checkMaterialDependence();

    //! Return the turbulent Prandtl number
    Real getTurbPrandtl();

    //! Initialize the material state
    void initMaterialState();

    //! Setup the element data in the ghost buffer
    void setGhostElemData(DataIndex EVAR, DataIndex GHOSTVAR);

    //! Setup the assembled ghost element volumes for G-G smoothing
    void setGhostAssemVolume();

    //! setup material property dependencies
    void setupMaterialDependence();

    //! Update the density (for an EOS)
    void updateDensity();

    //! Update the material state
    void updateMaterialState();

    //! Update the fluid molecular viscosity
    void updateMolecularViscosity();

    //! Update the molecular, turbulent and edge viscosity
    void updateViscosity();

    //! Update the molecular, turbulent and edge conductivity
    void updateThermalConductivity();

    //@}

    //**************************************************************************
    //! \name Memory utility methods
    //@{
    //! Compute/collect the memory summary
    void computeMemory();

    //! Print memory allocated in detail
    void printMemory(ostream& ofs);
    //@}

    //**************************************************************************
    //! \name Non-Linear Solver and related methods
    //@{
    //! Get current non-linear iteration number
    int getNLIter() {return m_solver->getIters();}

    //! Get the list of data indexes for variables in the global
    //! solution vector of old nonlinear iteration
    const vector<DataIndex>& getGSVIterDataIndex() {
      return m_GSVIterDataIndex;
    }

    //! Get the list of data indexes for variables in the global
    //! solution vector of new nonlinear iteration
    const vector<DataIndex>& getGSVDataIndex() {
      return m_GSVDataIndex;
    }

    //! Get the list of data indexes for variables in the global
    //! solution vector of solution increments
    const vector<DataIndex>& getGSVIncrDataIndex() {
      return m_GSVIncrDataIndex;
    }

    //!< Get mapping indexes from m_GSVnames to m_dofmap
    const int* getGSVindx() { return m_GSVindx; }
    //@}

    //**************************************************************************
    //! \name Normal distance calculations
    //@{
    //! Apply homogeneous EBC's to for the distance function
    void applyDistanceBCs();

    //! Apply penalty multiplier to the node-based distance operator
    void applyDistancePenalty();

    //! Calculate the normal distance function
    void calcNormalDistance();

    //! Enforce exact Dirichlet conditions for distance function
    void enforceNodalDistanceBCs();

    //! Extract the normal nodal distance from phi. The final implementation
    //! computes the node values using the inverse of the cell-to-node
    //! distance. However, we keep the volume and inverse volume weighting
    //! routines for possible future use.
    void extractNormalDistanceV();
    void extractNormalDistanceIV();
    void extractNormalDistanceID();

    //! Driver to form the unit-Poisson operator for distance calculations
    void formDistanceOp();

    //! Element-specific unit-Poisson operators for distance calculation
    void formDistOpHex8(Element& ec);
    void formDistOpPyr5(Element& ec);
    void formDistOpTet4(Element& ec);
    void formDistOpWedge6(Element& ec);

    //! Form the RHS for the distance potential
    void formDistanceRhs(DataIndex RHS, DataIndex RHSTMP);

    //! Insert the known (zero) distance BC's at walls
    void insertDistanceBCs();
    //@}

    //**************************************************************************
    //! \name Output Methods
    //@{
    //! Write the glob file
    void writeGlobFile(int inc, bool field, bool statfield, bool history,
                       bool restart);

    //! Set the glob file header
    void writeGlobHeader();

    //! Write min/max for edge data
    void writeEdgeLimits(ostream& ofs, Real* vf);

    //! Write the screen report
    //! \param[in] ofs  Reference to output stream
    //! \param[in] Ninc No. of time-steps between reports
    void writeReport(ostream& ofs, const int Ninc);

    //! Write the header for the screen report
    void writeReportHeader(ostream& ofs);

    //! Write a message identifying the physics solver
    virtual void writeSolving();

    //@}

    //**************************************************************************
    //! \name Output Field Delegates & Utility Routines
    //@{
    //! Calculate the temeprature at the external edges
    void calcExternalEdgeTemperature(Real& time, Real* Tedge);

    //! Compute the nodal projection at face boundaries for turb dist BCs
    void formTurbDistSurfProjID(Real* node_var, Real* work, int* iflag);

    //! Compute the nodal projection at face boundaries for
    //!   \param[in]   ENERGY         Energy Dataindex to project
    //!   \param[in]   EDGE_WALL_COND Edge wall condutctivity Dataindex
    //!   \param[in]   tbc            Temperature boundary condition vector
    //!   \param[in]   hbc            Heat flux boundary condition vector
    //!   \param[in]   convMode       Energy conversion mode
    //!   \param[out]  node_var       Variable to put the nodel projection
    //!   \param[out]  work           Work array for inverse distance operator
    void formEnergySurfProjID(DataIndex ENERGY,
                              DataIndex EDGE_WALL_COND,
                              const vector<CCINSTemperatureBC*>& tbc,
                              const vector<CCINSHeatFluxBC*>& hbc,
                              FVMCCINS::EnergyConversionMode convMode,
                              Real* node_var,
                              Real* work,
                              int* iflag);

    //! Output delegate for C_MU at elements for nonlinear k-e model
    void writeElemCMUField(const OutputDelegateKey& key,
                           int plnum, int varId);

    //! Output delegate for P_K at elements for nonlinear k-e model
    void writeElemPKField(const OutputDelegateKey& key,
                          int plnum, int varId);

    //! Output delegate for LIN_QUAD_COMP at elements for nonlinear k-e model
    void writeElemLQCField(const OutputDelegateKey& key,
                           int plnum, int varId);

    //! Output delegate for mass fraction at nodes
    void writeNodeMassFracField(const OutputDelegateKey& key,
                               int plnum, int varId);

    //! Field delegate for CFL number at elements
    void writeElemCFLField(const OutputDelegateKey& key,
                           int plnum, int varId);

    //! Output delegate for density at nodes
    void writeNodeDensityField(const OutputDelegateKey& key,
                               int plnum, int varId);

    //! Field delegate for eddy viscosity at elements
    void writeElemEddyViscosityField(const OutputDelegateKey& key,
                                     int plnum, int varId);

    //! Field delegate for Lambda-eps field for k-e models
    void writeElemLepsField(const OutputDelegateKey& key,
                            int plnum, int varId);

    //! Field delegate for Rey field for k-e models
    void writeElemReyField(const OutputDelegateKey& key,
                           int plnum, int varId);

    //! Field delegate for molecular viscosity at elements
    void writeElemViscosityField(const OutputDelegateKey& key,
                                 int plnum, int varId);

    //! Field delegate for y* field for k-e models
    void writeElemYstarField(const OutputDelegateKey& key,
                             int plnum, int varId);

    //! Field delegate for enstrophy at elements
    void writeElemEnstrophyField(const OutputDelegateKey& key,
                                 int plnum, int varId);

    //! Field delegate for enstrophy at nodes
    void writeNodeEnstrophyField(const OutputDelegateKey& key,
                                 int plnum, int varId);

    //! Field delegate for helicity at elements
    void writeElemHelicityField(const OutputDelegateKey& key,
                                int plnum, int varId);

    //! Field delegate for helicity at nodes
    void writeNodeHelicityField(const OutputDelegateKey& key,
                                int plnum, int varId);

    //! Field delegate for pressure at the element grid
    void writeElemPressureField(const OutputDelegateKey& key,
                                int plnum, int varId);


    //! Field delegate for pressure at nodes
    void writeNodePressureField(const OutputDelegateKey& key,
                                int plnum, int varId);

    //! Field delegate for the Q-criteria
    void writeElemQCriteriaField(const OutputDelegateKey& key,
                                 int plnum, int varId);

    //! Field delegate for the Q-criteria at nodes
    void writeNodeQCriteriaField(const OutputDelegateKey& key,
                                 int plnum, int varId);

    //! Field delegate for temperature at nodes
    void writeNodeTemperatureField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Field delegate for internal energy at nodes
    void writeNodeIntEnergyField(const OutputDelegateKey& key,
                                 int plnum, int varId);

    //! Field delegate for enthalpy at nodes
    void writeNodeEnthalpyField(const OutputDelegateKey& key,
                                int plnum, int varId);

    //! Field delegate for normal-distance at the element grid
    void writeElemTurbDistField(const OutputDelegateKey& key,
                                int plnum, int varId);

    //! Field delegate for energy dissipation rate (EPS)
    void writeNodeTurbEpsField(const OutputDelegateKey& key,
                               int plnum, int varId);

    //! Field delegate for  Turbulent Kinetic energy (KE)
    void writeNodeTurbKeField(const OutputDelegateKey& key,
                              int plnum, int varId);

    //! Field delegate for S-A nut at nodes
    void writeNodeTurbNuField(const OutputDelegateKey& key,
                              int plnum, int varId);

    //! Field delegate for Omega at nodes
    void writeNodeTurbOmegaField(const OutputDelegateKey& key,
                                 int plnum, int varId);

    //! Output delegate for element vorticity
    void writeElemVorticityVectorField(const OutputDelegateKey& key,
                                       int plnum, int varId);

    //! Output delegate for nodal velocity
    void writeNodeVelocityVectorField(const OutputDelegateKey& key,
                                      int plnum, int varId);

    //! Output delegate for nodal vorticity
    void writeNodeVorticityVectorField(const OutputDelegateKey& key,
                                       int plnum, int varId);

    //! Field delegate for element mean pressure
    void writeElemMeanPressureField(const OutputDelegateKey& key,
                                    int plnum, int varId);

    //! Field delegate for nodal mean pressure
    void writeNodeMeanPressureField(const OutputDelegateKey& key,
                                    int plnum, int varId);

    //! Field delegate for element mean density
    void writeElemMeanDensityField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Field delegate for nodal mean pressure
    void writeNodeMeanDensityField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Field delegate for element mean turbulent viscosity
    void writeElemMeanTurbnuField(const OutputDelegateKey& key,
                                  int plnum, int varId);

    //! Field delegate for element mean velocity
    void writeElemMeanVelocityVectorField(const OutputDelegateKey& key,
                                          int plnum, int varId);

    //! Field delegate for nodal mean velocity
    void writeNodeMeanVelocityVectorField(const OutputDelegateKey& key,
                                          int plnum, int varId);

    //! Field delegate for element mean vorticity
    void writeElemMeanVorticityVectorField(const OutputDelegateKey& key,
                                           int plnum, int varId);

    //! Field delegate for nodal mean vorticity
    void writeNodeMeanVorticityVectorField(const OutputDelegateKey& key,
                                           int plnum, int varId);

    //! Field delegate for element mean helicity
    void writeElemMeanHelicityField(const OutputDelegateKey& key,
                                    int plnum, int varId);

    //! Field delegate for nodal mean helicity
    void writeNodeMeanHelicityField(const OutputDelegateKey& key,
                                    int plnum, int varId);

    //! Field delegate for element mean temperature
    void writeElemMeanTemperatureField(const OutputDelegateKey& key,
                                       int plnum, int varId);

    //! Field delegate for nodal mean temperature
    void writeNodeMeanTemperatureField(const OutputDelegateKey& key,
                                       int plnum, int varId);

    //! Field delegate for element turbulent kinetic energy
    void writeElemTKEField(const OutputDelegateKey& key,
                           int plnum, int varId);

    //! Field delegate for nodal turbulent kinetic energy
    void writeNodeTKEField(const OutputDelegateKey& key,
                           int plnum, int varId);

    //! Field delegate for element mean enstrophy
    void writeElemMeanEnstrophyField(const OutputDelegateKey& key,
                                     int plnum, int varId);

    //! Field delegate for nodal mean enstrophy
    void writeNodeMeanEnstrophyField(const OutputDelegateKey& key,
                                     int plnum, int varId);

    //! Field delegate for element density-pressure covariance
    void writeElemCovDensPresField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Field delegate for nodal density-pressure covariance
    void writeNodeCovDensPresField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Field delegate for element pressure-velocity covariance
    void writeElemCovPresVelField(const OutputDelegateKey& key,
                                  int plnum, int varId);

    //! Field delegate for nodal pressure-velocity covariance
    void writeNodeCovPresVelField(const OutputDelegateKey& key,
                                  int plnum, int varId);

    //! Field delegate for element pressure variance
    void writeElemPresVarianceField(const OutputDelegateKey& key,
                                    int plnum, int varId);

    //! Field delegate for node pressure variance
    void writeNodePresVarianceField(const OutputDelegateKey& key,
                                    int plnum, int varId);

    //! Field delegate for node rms pressure
    void writeNodeRMSPressureField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Field delegate for elem rms pressure
    void writeElemRMSPressureField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Field delegate for element temperature variance
    void writeElemTempVarianceField(const OutputDelegateKey& key,
                                    int plnum, int varId);

    //! Field delegate for node temperature variance
    void writeNodeTempVarianceField(const OutputDelegateKey& key,
                                    int plnum, int varId);

    //! Field delegate for node rms temperature
    void writeNodeRMSTemperatureField(const OutputDelegateKey& key,
                                      int plnum, int varId);

    //! Field delegate for elem rms temperature
    void writeElemRMSTemperatureField(const OutputDelegateKey& key,
                                      int plnum, int varId);

    //! Field delegate for elem Reynolds stress tensor
    void writeElemReynoldsStressField(const OutputDelegateKey& key,
                                      int plnum, int varId);

    //! Field delegate for nodal Reynolds stress tensor
    void writeNodeReynoldsStressField(const OutputDelegateKey& key,
                                      int plnum, int varId);


    //! Field delegates for Linear Portion of Reynolds stress for nonlinear k-e
    void writeElemLinReynoldStress(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Field delegate for Quadratic Portion of Reynolds stress for nonlinear k-e
    void writeElemQuadReynoldStress(const OutputDelegateKey& key,
                                    int plnum, int varId);

    //! Field delegate for elem Total Reynolds stress for nonlinear model
    void writeElemTotReynoldStress(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Field delegate for elem Total Reynolds stress for nonlinear model
    void writeElemVelGrad(const OutputDelegateKey& key, int plnum, int varId);

    //! Output delegate for volume fraction at nodes
    void writeNodeVolFracField(const OutputDelegateKey& key,
                               int plnum, int varId);
    //@}

    //**************************************************************************
    //! \name Output History Delegates & Utility Routines
    //@{
    void writeElemScalarHistory(const OutputDelegateKey& key,
                                int plnum, int varId);

    void writeElemVectorHistory(const OutputDelegateKey& key,
                                int plnum, int varId);

    void writeElemEddyViscosityHistory(const OutputDelegateKey& key,
                                       int plnum, int varId);

    void writeElemEnstrophyHistory(const OutputDelegateKey& key,
                                   int plnum, int varId);

    void writeElemHelicityHistory(const OutputDelegateKey& key,
                                  int plnum, int varId);

    void writeElemPressureHistory(const OutputDelegateKey& key,
                                  int plnum, int varId);

    void writeElemVorticityHistory(const OutputDelegateKey& key,
                                   int plnum, int varId);

    void writeAvgSurfacePressureHistory(const OutputDelegateKey& key,
                                        int plnum, int varId);

    void writeAvgSurfaceTemperatureHistory(const OutputDelegateKey& key,
                                           int plnum, int varId);

    void writeAvgSurfaceVelocityHistory(const OutputDelegateKey& key,
                                        int plnum, int varId);

    void writeSurfaceAreaHistory(const OutputDelegateKey& key,
                                 int plnum, int varId);

    void writeSurfaceFillVolumeHistory(const OutputDelegateKey& key,
                                       int plnum, int varId);

    void writeSurfaceForceHistory(const OutputDelegateKey& key,
                                  int plnum, int varId);

    void writeSurfaceHeatFlowHistory(const OutputDelegateKey& key,
                                     int plnum, int varId);

    void writeSurfacePressureForceHistory(const OutputDelegateKey& key,
                                          int plnum, int varId);

    void writeSurfaceViscousForceHistory(const OutputDelegateKey& key,
                                         int plnum, int varId);

    void writeSurfaceMassFlowRateHistory(const OutputDelegateKey& key,
                                         int plnum, int varId);

    void writeSurfaceVolumeFlowRateHistory(const OutputDelegateKey& key,
                                           int plnum, int varId);
    //@}

    //**************************************************************************
    //! \name Output Surface Delegates & Utility Routines
    //@{
    //! Surface delegate for surface area at element faces
    void writeSurfAreaField(const OutputDelegateKey& key,
                            int plnum, int varId);

    //! Surface coordinates at element faces
    void writeSurfCoordVectorField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Surface delegate for heat flux vector at element faces
    void writeSurfHeatFluxVectorField(const OutputDelegateKey& key,
                                      int plnum, int varId);

    //! Surface delegate for mean heat flux vector at element faces
    void writeSurfMeanHeatFluxVectorField(const OutputDelegateKey& key,
                                          int plnum, int varId);

    //! Surface delegate for mean shear traction vector at element faces
    void writeSurfMeanShearTractionField(const OutputDelegateKey& key,
                                         int plnum, int varId);

    //! Surface delegate for mean normal traction vector at element faces
    void writeSurfMeanNormalTractionField(const OutputDelegateKey& key,
                                          int plnum, int varId);

    //! Surface delegate for mean traction vector at element faces
    void writeSurfMeanTractionField(const OutputDelegateKey& key,
                                    int plnum, int varId);

    //! Surface delegate for mean wall shear at element faces
    void writeSurfMeanWallShearField(const OutputDelegateKey& key,
                                     int plnum, int varId);

    //! Surface delegate for mean pressure at element faces
    void writeSurfMeanPressureField(const OutputDelegateKey& key,
                                    int plnum, int varId);

    //! Surface delegate for pressure variance at element faces
    void writeSurfPresVarianceField(const OutputDelegateKey& key,
                                    int plnum, int varId);

    //! Surface delegate for rms pressure at element faces
    void writeSurfRMSPressureField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Surface delegate for temperature variance at element faces
    void writeSurfTempVarianceField(const OutputDelegateKey& key,
                                    int plnum, int varId);

    //! Surface delegate for rms temperature at element faces
    void writeSurfRMSTemperatureField(const OutputDelegateKey& key,
                                      int plnum, int varId);

    //! Surface delegate for density-pressure covariance at element faces
    void writeSurfCovDensPresField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Surface delegate for mean density at element faces
    void writeSurfMeanDensityField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Surface delegate for mean velocity at element faces
    void writeSurfMeanVelocityVectorField(const OutputDelegateKey& key,
                                          int plnum, int varId);

    //! Surface delegate for mean temperature at element faces
    void writeSurfMeanTemperatureField(const OutputDelegateKey& key,
                                       int plnum, int varId);

    //! Surface delegate for normal heat flux at element faces
    void writeSurfNormalHeatFluxField(const OutputDelegateKey& key,
                                      int plnum, int varId);

    //! Surface Delegate for surface traction vector at element faces
    void writeSurfNormalTractionVectorField(const OutputDelegateKey& key,
                                            int plnum, int varId);

    //! Surface Delegate for surface traction vector at element faces
    void writeSurfShearTractionVectorField(const OutputDelegateKey& key,
                                           int plnum, int varId);

    //! Surface Delegate for surface traction vector at element faces
    void writeSurfTractionVectorField(const OutputDelegateKey& key,
                                      int plnum, int varId);

    //! Surface delegate for wall shear element-face output
    void writeSurfWallShearField(const OutputDelegateKey& key,
                                 int plnum, int varId);

    //! Surface delegate for yplus element-face output
    void writeSurfYplusField(const OutputDelegateKey& key,
                             int plnum, int varId);

    //! Surface delegate for y* element-face output
    void writeSurfYstarField(const OutputDelegateKey& key,
                             int plnum, int varId);

    //! Surface delegate for the "variation of yplus" element-face output
    void writeSurfVarYplusField(const OutputDelegateKey& key,
                                int plnum, int varId);
    //@}

    //**************************************************************************
    //! \name Registration for all variables and communications buffers
    //@{
    //! Register variables for deformable mesh
    void registerDeformVar();

    //! Register derived output delegates ONLY
    void registerDerivedVar();

    //! Register global solution variables for JFNK, etc.
    void registerGlobalVar();

    //! Register material data
    void registerMaterialData();

    //! Register primary primitive variables and their output delegates
    void registerPrimVar();

    //! Register species variables and their output delegates
    void registerSpeciesVar();

    //! Register statistics delegates (known to code)
    void registerStatisticsDelegates();

    //! Register accumulators delegates (known to code)
    void registerAccumulatorDelegates(void* obj);

    //! Register statistics accumulators
    void registerStatistics();

    //! Register turbulence variables and their output delegates
    void registerTurbVar();

    //! Register turbulence derived output delegates
    void registerDerivedTurbVar();

    //! Register communication buffers for parallel
    void registerCommBuff();

    //! Register temporary data if the size of external edge with
    //  2nd order term is large
    void reRegisterTemporaryData(int extEdge2ndOrderSize);

    //! Register step, aka physics, timers
    void registerTimers();
    //@}

    //**************************************************************************
    //! \name PPE Functions
    //@{
    //! Driver to form the FEM PPE operator
    void formPPE(const DataIndex IDENSITY);

    //! Element-specific PPE operators
    void formPPEHex8(Element& ec, const DataIndex IDENSITY);
    void formPPEPyr5(Element& ec, const DataIndex IDENSITY);
    void formPPETet4(Element& ec, const DataIndex IDENSITY);
    void formPPEWedge6(Element& ec, const DataIndex IDENSITY);

    //! Form the simple RHS for the PPE problem
    void formPPERhs(const DataIndex ELEM_VEC, const DataIndex EDGE_VEC,
                    const DataIndex RHS, const DataIndex RHSTMP);

    //! Form the simple RHS for the PPE problem
    void formPPERhsMAC(DataIndex RHS, DataIndex DIV);

    //! Compute residuals for the pressure equation
    void calcPressureResidual();

    //! Set diagonal to 1 for rigid material nodes for CHT
    void applyRigidPenalty(Real* Adiag);
    //@}

    //**************************************************************************
    //! \name Solution methods, time integration and related
    //@{
    //! Advance the solution
    void advanceSolution();

    //! Echo the time integrator
    void echoTimeInt(ostream& ofs);

    //! Apply Picard iteration based on 2nd-order projection
    void picardSolve();

    //! Advance solution using 2nd-order Projection (MAC's projection-2)
    void projectionSolve();

    //! Restart the NKA subspace
    void restartNKA() {if(m_useNKA) nka_restart(m_nka);}

    //! Set the solution increment parameters
    void setIncParam();

    //! Return boolean flag to test if the pressure equation
    //! should be included to the next solver iteration
    bool solveP(void);
    //@}

    //**************************************************************************
    //! \name Startup and projection functions
    //@{
    //! Compute the divergence from edge-normal velocities
    //!   \param[in]  EDGEVEL Data index for edge velocity
    //!   \param[in]  DIC     Data index for velocity divergence
    void calcDiv(DataIndex EDGEVEL, DataIndex DIV);

    //! Calculate the velocity in the edge-normal direction
    //!   \param[in]  VEL    DataIndex of velocity
    //!   \param[in]  GVEL   DataIndex of ghost velocity
    //!   \param[in]  VELBC  DataIndex of velocity boundary conditions
    //!   \param[in]  VF     DataIndex of edge velocity
    void calcEdgeVel(const DataIndex V,
                     const DataIndex GVEL,
                     const DataIndex VBC,
                     const DataIndex VF);

    //! Calculate the partial acceleration at element centers
    void calcPartialAcc();

    //! Calculate the partial acceleration on external edges
    void calcEdgePartialAcc(Real* af);

    //! Calclulate div-free dual-edge velocities on restart
    void calcRestartVelocity();

    //! Check external edge range
    void checkExternalEdgeRange() const;

    //! Compute the initial div-Free velocity field
    void divFree();

    //! Compute the initial pressure field
    void initPressure();

    //! Project edge velocities to a div-free subspace
    void projectEdgeVelocity(const Real time, const Real alpha);

    //! Project element velocities to a div-free subspace
    void projectElemVelocity(const Real alpha);

    //! Calculate the RMS divergence error from a given edge-velocity field
    //! \param[in] DIV  Data index for divergence field
    Real rmsDiv(const DataIndex DIV);

    //! Save the previous-time state/solution vector
    void saveStateVector();

    //! Setup basic operators
    void setupOperators();

    //! Setup the lists for wall treatment
    void setupWallLists();

    //! Update velocity and pressure
    void updateVelocityandPressure();

    //! Update the pressure using the Lagrange multiplier
    //! \param[in] LAG_MULT  Lagrange multiplier         (LAMBDA     )
    //! \param[in] P         Pressure (current)          (PRESSURE   )
    //! \param[in] PN        Pressure at level n         (PRESSUREN  )
    void updatePressure(DataIndex LAG_MULT, DataIndex P, DataIndex PN);
    //@}

    //**************************************************************************
    //! \name Statistics Accumulators
    //@{
    //! Check if vector of accumulators are available
    bool accumulatorsAvailable(const vector<AccumulatorId>& acc);

    //! Accumulator Definitions
    ACCUMULATOR_MEMB(accumulateMeanVorticity)
    ACCUMULATOR_MEMB(accumulateMeanEnstrophy)
    ACCUMULATOR_MEMB(accumulateMeanHelicity)
    ACCUMULATOR_MEMB(accumulateMeanHeatFlux)
    ACCUMULATOR_MEMB(accumulateMeanShearTraction)
    ACCUMULATOR_MEMB(accumulateMeanNormalTraction)
    ACCUMULATOR_MEMB(accumulateMeanTraction)
    ACCUMULATOR_MEMB(accumulateMeanWallShear)
    ACCUMULATOR_MEMB(accumulateTKE)
    ACCUMULATOR_MEMB(accumulateReynoldsStress)
    ACCUMULATOR_MEMB(accumulateMeanTurbViscosity)

    //! Register an accumulator delegate
    void registerAccumulatorDelegate(const AccumulatorId& accId,
                                     const string& name,
                                     const bool cenmom,
                                     const MemoryLayout& layout,
                                     const VariableType& vtype,
                                     const bool plot,
                                     const AccumulatorFuncOwner& owner,
                                     void* statClassPtr,
                                     const AccumulatorFunc& func,
                                     const vector<DataIndex>& inst);

    //! Register a statistics delegate
    void registerStatisticsDelegate(const string& name,
                                    const VariableCentering& centering,
                                    const VariableType& vtype,
                                    const OutputDelegateType& otype,
                                    const OutputDelegateWrapperCall& func,
                                    const vector<AccumulatorId>& vecAccId);

    //! Setup the availability of accumulators
    void setupAvailableAccumulators();

    //! Associate instantaneous variables to their means
    void setupMeans();
    //@}

    //**************************************************************************
    //! \name Time step calculation
    //@{
    //! Driver to calculate the minimum stable time-step
    void calcTimeStep(Real soltime, int& idtchk);

    //! Calculate the minimum stable time-step for Hex8
    void calcTimeStepHex8(Element& ec,
                          const Real cfl,
                          Real& dtmin,
                          Real& cflmax,
                          const DataIndex V,
                          bool& setdt);

    //! Calculate the minimum stable time-step for Pyr5
    void calcTimeStepPyr5(Element& ec,
                          const Real cfl,
                          Real& dtmin,
                          Real& cflmax,
                          const DataIndex V,
                          bool& setdt);

    //! Calculate the minimum stable time-step for Tet4
    void calcTimeStepTet4(Element& ec,
                          const Real cfl,
                          Real& dtmin,
                          Real& cflmax,
                          const DataIndex V,
                          bool& setdt);

    //! Calculate the minimum stable time-step for Wedge6
    void calcTimeStepWedge6(Element& ec,
                            const Real cfl,
                            Real& dtmin,
                            Real& cflmax,
                            const DataIndex VEL,
                            bool& setdt);
    //@}

    //**************************************************************************
    //! \name Transport Equations
    //@{
    //! Add the viscous terms to the RHS of the momentum equations
    void addViscousRhs(Real* mu, CTensor& vgrad, CVector& rhs);

    //! Compute residual for the momentum equation
    //! \param[in] rhsVelN Pointer to the rhs at time level n
    void calcMomentumResidual(CVector* rhsVelN);

    //! Form all RHS for initial non-linear iteration
    void formAllRhs();

    //! Form all the RHS terms and new operators for deformable mesh
    void formAllRhsAndOperators();

    //! Form the RHS terms for turbulence models for deformable mesh
    void formTurbulenceTransportRhs();

    //! Get flag, indicating the energy equation is present
    bool hasEnergyEq() {return m_hasEnergyEq;}

    //! Initialize the primary flow variables
    void initFlowVars();

    //! Initialize surface chemistry
    void initSurfChem();

    //! Initialize the transport equation solvers
    void initTransportSolvers();

    //! Initialize the transport equation DataIndexes
    void initTransportVars();

    //! Solve energy transport equations
    void solveEnergyTransport();

    //! Solve momentum transport equations
    void solveMomentumTransport();

    //! Solve species transport equations
    void solveSpeciesTransport();

    //! Solve surface chemistry
    void solveSurfChem();

    //! Solve turbulence transport equations
    void solveTurbulenceTransport();

    //! Solve volume fraction transport equations
    void solveVolFracTransport();

    //@}

    //**************************************************************************
    //! \name Volumetric Heat Source Terms
    //@{
    //! Add volumetric heat sources
    void addHeatSource(SourcePackage& sp);

    //! Get the heat source container
    CCINSHeatSource& getHeatSource(int i) {return *(m_heat_source[i]);}

    //! Number of heat sources
    int numHeatSources() {return m_heat_source.size();}
    //@}

    //**************************************************************************
    //! \name Utility functions (CCINSUtil.C)
    //@{
    //! Calculate surface-average value of velocity vector on a surface
    void calcAverageEdgeVelocityVector(int nel,
                                       const int* cidmap,
                                       const int* side_list,
                                       const int* el_list,
                                       Real& surfarea,
                                       Real* velVect);

    //! Calculate average pressure force on a surface
    void calcAveragePressureForce(int nel,
                                  const int* cidmap,
                                  const int* side_list,
                                  const int* el_list,
                                  Real& surfarea,
                                  Real& forcePress);

    //! Calculate an overall volumetric flux balance on the domain
    Real calcFluxBalance(const DataIndex DUALEDGE_VEL);

    //! Calculate the kinetic energy for output statistics
    Real calcKE();

    //! Calculate residuals
    void calcResiduals();

    //!< Calculate CFL number for Hex8
    void calcCFLHex8(Element& ec, const Real dt, const DataIndex V, Real *cfl);

    //!< Calculate CFL number for Pyr5
    void calcCFLPyr5(Element& ec, const Real dt, const DataIndex V, Real *cfl);

    //!< Calculate CFL number for Tet4
    void calcCFLTet4(Element& ec, const Real dt, const DataIndex V, Real *cfl);

    //!< Calculate CFL number for Wedge6
    void calcCFLWedge6(Element& ec, const Real dt, const DataIndex V,Real *cfl);

    //! Calculate heat flux vector on a surface
    void calcHeatFlux(int nel, const int* cidmap,
                      const int* side_list, const int* el_list,
                      CVector& heatflux);

    //! Calculate volume flow rate across a surface
    void calcHeatFlow(int nel, const int* cidmap,
                      const int* side_list, const int* el_list,
                      Real& heatflow);

    //! Calculate an element-centered Q-criteria for coeherent structures
    void calcQcriteria(Real* qcri, const DataIndex ELEM_VGRAD);

    //! Calculate volume flow rate across a surface
    void calcSurfaceArea(int nel, const int* cidmap,
                         const int* side_list, const int* el_list,
                         Real& surfarea);

    //! Calculate the surface areas on a given surface
    void calcSurfaceAreas(int nel, const int* cidmap,
                          const int* side_list, const int* el_list,
                          Real* surfarea);

    void calcSurfaceCoord(int nel, const int* cidmap,
                          const int* side_list, const int* el_list,
                          CVector& coord);

    //! Calculate average value of a scalar on a surface
    void calcScalarAverage(int nel, const int* cidmap,
                           const int* side_list, const int* el_list,
                           const Real* scalar, const Real* Tedge,
                           Real& surfarea, Real& scalarflux);

    //! Calculate mass flow rate across a surface
    void calcMassFlowRate(int nel, const int* cidmap,
                          const int* side_list, const int* el_list,
                          Real& massflow);

    //! Calclulate incremental volume flow for specific surface
    Real calcVolumeFlowInc(const int setid, const Real dt);

    //! Calculate volume flow rate across a surface
    void calcVolumeFlowRate(int nel, const int* cidmap,
                            const int* side_list, const int* el_list,
                            Real& volflow);

    //! Calculate normal traction vector on a surface
    void calcNormalTraction(const int nel,
                            const int* cidmap,
                            const int* side_list,
                            const int* el_list,
                            CTensor& vgrad,
                            CVector& normTrac);

    //! Calculate shear traction or surface traction acting on a surface
    void calcShearTraction(const int nel,
                           const int* cidmap,
                           const int* side_list,
                           const int* el_list,
                           CTensor& vgrad,
                           CVector& surfTrac);

    //! Calculate total traction (normal + surface) acting on a surface
    void calcTotalTraction(const int nel,
                           const int* cidmap,
                           const int* side_list,
                           const int* el_list,
                           CTensor& vgrad,
                           CVector& totTrac);

    //! Calculate pressure force acting on a surface
    void calcPressureForce(int nel, const int* cidmap,
                           const int* side_list, const int* el_list,
                           Real* forcePress);

    //! Calculate Viscous force acting on a surface
    void calcViscousForce(const int nel,
                          const int* cidmap,
                          const int* side_list,
                          const int* el_list,
                          CTensor& vgrad,
                          Real* forceVisc);

    //! Calculate total force (pressure + viscous) acting on a surface
    void calcTotalForce(const int nel,
                        const int* cidmap,
                        const int* side_list,
                        const int* el_list,
                        CTensor& vgrad,
                        Real* forceTot);

    //! Calculate an element-centered vorticity from an edge-centered
    void calcVorticity(CVector& vort, CTensor& vgrad);

    //! Calculate the spatial variation of y+
    void calcVarYplus(const int nel, const int* cidmap, const int* el_list,
                      const Real* yplus, Real* varyplus);

    //! Calculate wall shear
    void calcWallShear(const int nel,
                       const int* cidmap,
                       const int* side_list,
                       const int* el_list,
                       CTensor& vgrad,
                       Real* wallshear);

    //! Calculate normal heat flux
    void calcNormalHeatFlux(int nel, const int* cidmap, const int* side_list,
                            const int* el_list, Real* normheatflux);

    //! Calculate y+
    void calcYplus(const int nel, const int* edge_list, Real* yplus);

    //! Interpolate an edge variable to an element-centered variable
    void interpEdgeVar(Real* edge_var, Real* elem_var);

    //! Interpolate an edge variable to an element-centered variable
    void interpEdgeVarToElem(int* edge_var, Real* elem_var);
    //@}

  protected:

    //! Bools that show whether the accumulator is available in this physics
    bool m_availableAccumulators[NUM_OF_CCINS_ACCUMULATORS];

    //**************************************************************************
    //! \name DataIndex Declarations -- inherited
    //@{
    // Global solution/residual vectors
    DataIndex GLOBAL_SOLUTION;  //!< Global solution vector at time n+1
    DataIndex GLOBAL_SOLUTIONN; //!< Global solution vector at time n

    // Scalar quantities
    DataIndex PRESSURE;       //!< Pressure at time n+1
    DataIndex PRESSUREN;      //!< Pressure at time n
    DataIndex PRESSURENM1;    //!< Pressure at time n-1

    DataIndex LAMBDA;         //!< Lagrange multipler at time n+1

    DataIndex TURB_DIST;      //!< Distance function for wall modelling

    DataIndex TURB_KE;        //!< Turbulent kinetic energy at time n+1
    DataIndex TURB_KEN;       //!< Turbulent kinetic energy at time n
    DataIndex TURB_KENM1;     //!< Turbulent kinetic energy at time n-1

    DataIndex TURB_EPS;       //!< Turbulent dissipation rate at time n+1
    DataIndex TURB_EPSN;      //!< Turbulent dissipation rate at time n
    DataIndex TURB_EPSNM1;    //!< Turbulent dissipation rate at time n-1

    DataIndex TURB_OMEGA;     //!< Turbulent time scale rate at time level n+1
    DataIndex TURB_OMEGAN;    //!< Turbulent time scale rate at time level n
    DataIndex TURB_OMEGANM1;  //!< Turbulent time scale rate at time level n-1

    DataIndex TURB_PROD;      //!< Turbulent production (aka f) at time n+1
    DataIndex TURB_PRODN;     //!< Turbulent production (aka f) at time n
    DataIndex TURB_PRODNM1;   //!< Turbulent production (aka f) at time n-1

    DataIndex TURB_ZETA;      //!< Turbulent velocity ratio at time level n+1
    DataIndex TURB_ZETAN;     //!< Turbulent velocity ratio at time level n
    DataIndex TURB_ZETANM1;   //!< Turbulent velocity ratio at time level n-1

    DataIndex TURB_NU;        //!< Turbulent viscosity at time n+1
    DataIndex TURB_NUN;       //!< Turbulent viscosity at time n
    DataIndex TURB_NUNM1;     //!< Turbulent viscosity at time n-1

    DataIndex EDGE_WALL_LIST; //!< List of external edges at walls
    DataIndex EDGE_WALL_VEL ; //!< Mesh velocity at the face
    DataIndex ELEM_WALL_LIST; //!< Unique list of elements attached to walls

    DataIndex GHOST_VOL;      //!< Ghost volume
    DataIndex GHOST_ASSEM_VOL;//!< Inverse assembled volume for G-G smoother

    DataIndex DIVERGENCE;     //!< Divergence

    // Vector quantities
    DataIndex GHOST_GRAD_DENSITY; //!< Ghost buffer for gradient of density

    // Gradient data
    DataIndex ELEM_GHOST_GRAD;    //!< Ghost buffer for gradients

    // Data required for the nodal equations: PPE, Distance, etc.
    DataIndex NODE_RHS;           //!< Node-based RHS for the PPE
    DataIndex NODEEQ_MAP;         //!< node to equation number map
    DataIndex NODEEQ_VAR;         //!< solution array for PPE

    // Data required for the element equations
    DataIndex ELEMEQ_VAR;         //!< solution array for elements
    DataIndex ELEM_RHS;           //!< Element-based RHS for momentum equation
    DataIndex ELEM_DIAG;          //!< Element-based LHS diagonal for momentum

    // Data required for deformable mesh
    DataIndex NODE_COORD;          //!< Node-based original coordinates
    DataIndex NODE_VEL;            //!< Node-based velocity
    DataIndex NODE_DISP;           //!< Node-based displacement at current time
    DataIndex NODE_DISPN;          //!< Node-based displacement at previous time
    DataIndex VOLUMEN;             //!< Volume at n

    DataIndex ELEM_SCALAR_RHS;     //!< Element-based RHS for scalar RHS
    DataIndex TURB_NU_RHS;         //!< Element-based RHS for SA equation
    DataIndex TURB_KE_RHS;         //!< Element-based RHS for K equation
    DataIndex TURB_EPSILON_RHS;    //!< Element-based RHS for Epsilon equation
    DataIndex TURB_OMEGA_RHS;      //!< Element-based RHS for Omega equation

    // Storage for variables associated with the surface
    DataIndex SURFACE_BCVOLUME;    //!< Volumes used for PVDEP B.C on surfaces

    // Scratch arrays
    DataIndex TMP1;                 //!< Scalar scratch
    DataIndex TMP2;                 //!< Scalar scratch
    DataIndex TMP_VEC1;             //!< Vector scratch
    //@}

    //**************************************************************************
    //! \name Global solution vector and related
    //@{
    CCINSNLSolver* m_solver; //!< Pointer to non-linear solver class
    //! The total number of degrees of freedom in the solution vector
    int m_Ndof;
    //! DoF Map for managing memory offsets in data container
    int m_dofmap[NUMBER_DOF];
    //! Mapping indexes from m_GSVnames to m_dofmap
    int m_GSVindx[NUMBER_DOF];
    //! The list of global solution vector offsets
    vector<int> m_GSVoffsets;
    //! The list of global solution vector names
    vector<string> m_GSVnames;
    //! The list of data indexes for variables in the global solution vector
    vector<DataIndex> m_GSVDataIndex;
    //! The list of data indexes for variables in the global solution vector of
    //! current nonlinear iteration
    vector<DataIndex> m_GSVIterDataIndex;
    //! The list of data indices for variables in the global solution vector of
    //! increments
    vector<DataIndex> m_GSVIncrDataIndex;
    //! The list of data indexes for variables in the global solution vector of
    //! residuals
    vector<DataIndex> m_GSVResDataIndex;
    //! The list of data indexes for variables in the global solution vector of
    //! rhs
    vector<DataIndex> m_GSVRhsDataIndex;
    //@}

    //**************************************************************************
    //! \name Control parameters
    //@{
    bool m_last;                 //!< Flag to indicate last time-step (grunt)
    bool m_deformableMesh;       //!< Mesh is a deformable mesh
    bool m_variableConductivity; //!< True for variable thermal conductivity
    bool m_variableDensity;      //!< True for variable density
    bool m_variableExpansion;    //!< True for variable expansion coefficient
    bool m_variableSpecificHeat; //!< True for variable specific heat
    bool m_variableViscosity;    //!< True for variable viscosity
    bool m_isRestart;            //!< Current analysis is a restart
    bool m_hasDistBCs;           //!< Normal distance BC's are present
    bool m_hasEnergyEq;          //!< Energy equation is present
    bool m_hasPorousFlow;        //!< Porous flow model is present
    bool m_hasSharpInt;          //!< Sharp interface is present
    bool m_hasSpeciesEq;         //!< Species equation is present
    bool m_hasTurbModel;         //!< Turbulence model is present
    bool m_hasUnsPBCs;           //!< Unsteady pressure BC's are present
    bool m_calcTempEdgeGrad;     //!< Construct the edge temperature gradient
    bool m_calcEdgeTemp;         //!< Construct temperature at the external edges
    int  m_massChanged;          //!< non-zero indicates density changed
    int  m_heatcapChanged;       //!< non-zero indicates heat capacitance changed
    int  m_strategy;             //!< Solution strategy, Projection or Picard
    int  m_meshUpdated;          //!< Non-zero -- current mesh has been updated
    Real m_rmsdiv;               //!< RMS divergence metric
    //@}

    //**************************************************************************
    //! \name Wall function global parameters
    //@{
    int m_Nel_wall;          //!< No. of edges at no-slip/no-penetration walls
    int m_Nedge_wall;        //!< No. of edges at no-slip/no-penetration walls
    //@}

    //**************************************************************************
    //! \name Time incrementation parameters
    //@{
    bool m_checkdt;
    int  m_checkint;
    int  m_Ninc;
    int  m_ttyi;          //!< Interval for screen prints
    Real m_time;
    Real m_time_np1;
    Real m_totalTime;
    Real m_cflmax;
    CCINSIncParm m_incParm;
    //@}

    // Data indexes for the transport solves
    CCINSTransportVar m_di;

    // BC containers
    vector<CCINSHydrostat*>         m_hydrostats;
    vector<PeriodicSideBC*>         m_periodicBCs;
    vector<CCINSPressureBC*>        m_pressureBCs;
    vector<CCINSPressureOutflowBC*> m_pressureOutflowBCs;
    vector<CCINSPassiveOutflowBC*>  m_passiveOutflowBCs;
    vector<CCINSTurbDistBC*>        m_turbDistBCs;     //!< Normal distance
    vector<CCINSTurbEpsBC*>         m_turbEpsBCs;      //!< Dissipation BC's
    vector<CCINSTurbKEBC*>          m_turbKEBCs;       //!< Turbulent-KE BC's
    vector<CCINSTurbNuBC*>          m_turbNuBCs;       //!< SA turbnu BC's
    vector<CCINSTurbOmegaBC*>       m_turbOmegaBCs;    //!< Omega BC's
    vector<CCINSTurbProdBC*>        m_turbProdBCs;     //!< Turbulence prod.
    vector<CCINSTurbZetaBC*>        m_turbZetaBCs;     //!< Zeta BC's
    vector<CCINSDisplacementBC*>    m_displacementBCs; //!< Nodeset displacement
    vector<CCINSSurfDispBC*>        m_surfDispBCs;     //!< Sideset displacement

    int elEqIDOffset;
    int mxElrow;
    int mxElrowOff;
    vector<int> m_el_d_nnz;
    vector<int> m_el_o_nnz;
    int m_mxNdrow;        //!< maximum node row size in processor
    int m_mxNdrowOff;     //!< maximum node row size off processor
    vector<int> m_d_nnz;  //!< per row maximum node row size on processor
    vector<int> m_o_nnz;  //!< per row maximum node row size off processor
    int m_nodeEqStart;    //!< First number of the node equation number
    int m_nodeEqEnd;      //!< Last number of the node equation number
    int m_Nnp_eq;         //!< number of node equation number

    //! Normal-distance Linear Solver
    bool m_dist_convergence;
    bool m_dist_needAdiag;
    LASolver* m_dist_solver;

    //! PPE Linear Solver
    bool m_ppe_convergence;
    bool m_ppe_A_set;
    LASolver* m_ppe_solver;
    LAMatrix* m_node_A;
    LAVector* m_node_b;
    LAVector* m_node_x;

    //! Momentum Equation solver
    LASolver* m_momentum_solver;
    LAVector* m_momentum_bx;
    LAVector* m_momentum_by;
    LAVector* m_momentum_bz;
    LAVector* m_momentum_diagA;
    LAVector* m_momentum_diagA_temp;

    //! Matrix for element-based linear solver
    LAMatrix* m_transport_A;

    //! Transport Equation Linear Solver
    bool m_transport_convergence;
    LASolver* m_transport_solver;
    LAVector* m_transport_diagA;
    LAVector* m_transport_b;
    LAVector* m_transport_x;

    //! Timers for incompressible flow
    TimerIndex DIST_TIME;
    TimerIndex PPE_TIME;
    TimerIndex RESIDUALS_TIME;
    TimerIndex MOMENTUM_TIME;
    TimerIndex ENERGY_TIME;
    TimerIndex SPECIES_TIME;
    TimerIndex TURB_TIME;
    TimerIndex VOLFRAC_TIME;
    TimerIndex WRITE_TIME;
    TimerIndex RESTART_WRITE_TIME;
    TimerIndex EXPORT_TIME;
    TimerIndex DT_CALC_TIME;
    TimerIndex MESH_DEFORMATION;
    TimerIndex ALE_UPDATE;
    TimerIndex UPDATE_VELGRAD;
    TimerIndex UPDATE_MATERIAL_STATE;
    TimerIndex VP_UPDATE;

    //! Error Handling Interface
    CCINSErrors* m_errors;

    // Transport solvers
    CCINSSpeciesMassFrac* m_Species;
    map<int,int> m_transportSpecies;

    //! Dual-edge gradient operator
    DualEdgeGradOp* m_edgeGradOp;

    //! FieldStatistics class instance
    FieldStatistics* m_fieldStatistics;

    //! Statistics delegates (known to code)
    StatisticsDelegateMap m_statisticsDelegates;

    //! Accumulator delegates (known to code)
    AccumulatorDelegateMap m_accumulatorDelegates;

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSFlow(const CCINSFlow&);
    CCINSFlow& operator=(const CCINSFlow&);
    //@}

    //! Find index of variable with given name in the global solution vector
    //    \param[in] name Given name of the variable
    int getPlaceInGSV(string name) {
      int size = m_GSVnames.size();
      for(int i=0;i<size;++i) {
        if(m_GSVnames[i] == name) return i;
      }
      return UNREGISTERED;
    }

    //**************************************************************************
    //! \name DataIndex Declarations - not inherited
    //@{
    // !Scalar quantities.
    DataIndex DENSITY;        //!< Density at time n+1
    DataIndex DENSITYN;       //!< Density at time n
    DataIndex DENSITYNM1;     //!< Density at time n-1
    DataIndex ENTHALPY;       //!< Enthalpy at time n+1
    DataIndex ENTHALPYN;      //!< Enthalpy at time n
    DataIndex ENTHALPYNM1;    //!< Enthalpy at time n-1
    DataIndex INTENERGY;      //!< Specific internal energy at time n+1
    DataIndex INTENERGYN;     //!< Specific internal energy at time n
    DataIndex INTENERGYNM1;   //!< Specific internal energy at time n-1
    DataIndex MASSFRAC;       //!< Species mass fraction at time n+1
    DataIndex MASSFRACN;      //!< Species mass fraction at time n
    DataIndex MASSFRACNM1;    //!< Species mass fraction at time n-1
    DataIndex TEMPERATURE;    //!< Temperature at time n+1
    DataIndex TEMPERATUREN;   //!< Temperature at time n
    DataIndex TEMPERATURENM1; //!< Temperature at time n-1

    DataIndex VOLFRAC;        //!< Volume fraction at time n+1
    DataIndex VOLFRACN;       //!< Volume fraction at time n

    //! Dual-edge/ghost data
    DataIndex DUALEDGE_VEL;   //!< Dual-edge velocity at time n+1
    DataIndex DUALEDGE_VELN;  //!< Dual-edge velocity at time n
    DataIndex GHOST_VAR;      //!< Generic ghost variable for scalar transport
    DataIndex GHOST_NU;       //!< Ghost Turbulent eddy-viscosity for S-A model

    //! Vector quantities
    DataIndex VEL;            //!< Velocity at time level n+1
    DataIndex VELN;           //!< Velocity at time level n
    DataIndex VELNM1;         //!< Velocity at time level n-1
    DataIndex GHOST_VEL;      //!< Ghost velocity
    DataIndex VEL_BC;         //!< Buffer for velocity BC's for projection

    //! Storage for material property update/evaluation
    DataIndex ELEM_MOL_VISCOSITY;       //!< Element-level viscosity at n+1
    DataIndex ELEM_MOL_VISCOSITYN;      //!< Element-level viscosity at n
    DataIndex ELEM_TURB_VISCOSITY;      //!< Turbulent viscosity
    DataIndex EDGE_VISCOSITY;           //!< Effective edge viscosity at n+1
    DataIndex EDGE_VISCOSITYN;          //!< Effective edge viscosity at n
    DataIndex EDGE_CONDUCTIVITY;        //!< Effective edge conductivity at n+1
    DataIndex EDGE_DIFFUSIVITY;         //!< Effective edge diffusivity at n+1
    DataIndex EDGE_SCALED_CONDUCTIVITY; //!< Scaled effective edge conductivity

    DataIndex F_MU;
    DataIndex C_MU;

//#####DEBUG
    // ***************************************
    // EVEN MORE TESTS (BEN)
    DataIndex P_K;              //!< P_K (Reynolds Stress) for NLKE
    DataIndex LIN_QUAD_COMP;    //!< Compare Linear - Quadratic Reynolds Stresses
    DataIndex LIN_REYN_STRESS;  //!< Linear Reynolds Stress for NLKE
    DataIndex QUAD_REYN_STRESS; //!< Quadratic Reynolds Stress for NLKE
    DataIndex TOT_REYN_STRESS;  //!< Total Reynolds Stress for NLKE
    DataIndex NL_VEL_GRAD;      //!< Total Reynolds Stress for NLKE
    // ***************************************
//#####DEBUG

    // Gradient data
    DataIndex ELEM_GRADIENT;  //!< Element-centered gradient such as pressure

    //! Tensor quantities
    DataIndex ELEM_TENSOR;    //!< Work array for element tensor
    //@}

    //**************************************************************************
    //! \name BC containers
    //@{
    vector<CCINSDensityBC*>              m_densityBCs;
    vector<CCINSHeatFluxBC*>             m_heatFluxBCs;
    vector<CCINSMassFlowBC*>             m_massFlowBCs;
    vector<CCINSMassFluxBC*>             m_massFluxBCs;
    vector<CCINSPVDepBC*>                m_pvDepBCs;
    vector<CCINSSpeciesMassFracBC*>      m_massFracBCs;
    vector<CCINSSpeciesDiffusionFluxBC*> m_diffusionFluxBCs;
    vector<CCINSSymmVelBC*>              m_symmVelBCs;
    vector<CCINSTemperatureBC*>          m_temperatureBCs;
    vector<CCINSTractionBC*>             m_tractionBCs;
    vector<CCINSVelocityBC*>             m_velocityBCs;
    vector<CCINSVolumeFlowBC*>           m_volumeFlowBCs;
    vector<CCINSVolFracBC*>              m_volFracBCs;
    //@}

    //**************************************************************************
    //! \name Body forces
    //@{
    vector<CCINSBodyForce*>       m_body_force;
    vector<CCINSBoussinesqForce*> m_boussinesq_force;
    vector<CCINSGravityForce*>    m_gravity_force;
    //@}

    //**************************************************************************
    //! \name Porous drag forces treated as a body force.
    //@{
    vector<CCINSPorousDrag*> m_porousdrag;
    //@}

    //**************************************************************************
    //! \name FSI Penalties
    //@{
    vector<CCINSFsiPenalty*> m_fsiPenalties;
    //@}

    //**************************************************************************
    //! \name Volumetric heat sources
    //@{
    vector<CCINSHeatSource*> m_heat_source;
    //@}

    //**************************************************************************
    //! \name Surface chemistry interfaces
    //@{
    vector<CCINSSurfChem*> m_surfchem;
    //@}

    //**************************************************************************
    //! /name Initial Conditions
    //@{
    CCINSICPackage m_ics;

    //! Initial Condition Containers
    vector<CCINSBlockICs*>          m_blockICs;
    vector<CCINSUserEpsilonIC*>     m_userEpsilonICs;
    vector<CCINSUserOmegaIC*>       m_userOmegaICs;
    vector<CCINSUserTKEIC*>         m_userTKEICs;
    vector<CCINSUserTemperatureIC*> m_userTemperatureICs;
    vector<CCINSUserTurbnuIC*>      m_userTurbnuICs;
    vector<CCINSUserVelocityIC*>    m_userVelocityICs;
    //@}

    //**************************************************************************
    //! \name Transport solvers - not inherited
    //@{
    CCINSEnergy*     m_Energy;
    CCINSMomentum*   m_Momentum;
    CCINSTurbulence* m_Turbulence;
    CCINSVolFrac*    m_volFrac;
    //@}

    //**************************************************************************
    //! \name Momentum Equation unknowns
    //@{
    LAVector* m_momentum_x;
    LAVector* m_momentum_y;
    LAVector* m_momentum_z;
    //@}

    //**************************************************************************
    //! \name nonlinear Kyrlov accelerator object
    //@{
    bool m_useNKA;
    NKA m_nka;
    //@}
};

}

#endif // CCINSFlow_h
