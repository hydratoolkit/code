//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSTurbKEBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   k-boundary conditions
//******************************************************************************
#ifndef CCINSTurbKEBC_h
#define CCINSTurbKEBC_h

#include <CCSideSetBC.h>
#include <BCPackage.h>

namespace Hydra {

class CCINSTurbKEBC : public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSTurbKEBC(const BCPackage& bc, int offset);
    virtual ~CCINSTurbKEBC();
    //@}

    //! Set a user-defined BC value
    virtual Real setUserValue();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSTurbKEBC(const CCINSTurbKEBC&);
    CCINSTurbKEBC& operator=(const CCINSTurbKEBC&);
    //@}

};

}

#endif // CCINSTurbKEBC_h
