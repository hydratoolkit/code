//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSTurbDistBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Normal distance boundary conditions based on sidesets
//******************************************************************************
#ifndef CCINSTurbDistBC_h
#define CCINSTurbDistBC_h

#include <CCSideSetBC.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;
class LAMatrix;

//! Turbulence normal distance function
class CCINSTurbDistBC : public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSTurbDistBC(const BCPackage& bc, int offset);
    virtual ~CCINSTurbDistBC();
    //@}

    //! Apply a nodal-penalty
    void applyDistancePenalty(int pid, int* oproc, Real* diagTmp);

    //! Apply a nodal-penalty normal-distanc BC
    void applyDistancePenaltyBC(UnsMesh& mesh, int pid, int* oproc,
                               Real* diagTmp, Real* rhs, Real t, Real scale);

    //! Apply a nodal-penalty to the diagonal of the linear system
    void applyNodePenalty(int Nproc, int nodeEqStart, int nodeEqEnd,
                          int* nodeeqmap, Real* Adiag);

    //! Apply a nodal-penalty BC
    void applyNodePenaltyBC(UnsMesh& mesh, int Nproc, int nodeEqStart,
                            int nodeEqEnd, int* nodeeqmap, LAMatrix* A,
                            Real* rhs, Real t, Real scale);

    //! Insert nodal TurbDist BCs
    void insertNodalBCs(UnsMesh& mesh, DataIndex VAR, Real t, Real scale);

    //! Insert nodal TurbDist BCs
    void insertNodalBCs(UnsMesh& mesh, Real val, Real* var);

    //! Mark the prescribed boundary condition
    void markNodePenalty(int kthBc, int* nodeBc);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSTurbDistBC(const CCINSTurbDistBC&);
    CCINSTurbDistBC& operator=(const CCINSTurbDistBC&);
    //@}

    Real m_penalty;
};

}

#endif // CCINSTurbDistBC_h
