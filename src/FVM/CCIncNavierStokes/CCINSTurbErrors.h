//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSTurbErrors.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Error handler for turbulence models
//******************************************************************************
#ifndef CCINSTurbErrors_h
#define CCINSTurbErrors_h

#include <CCINSErrorTypes.h>

namespace Hydra {

class UnsMesh;
class fileIO;


class CCINSTurbErrors {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSTurbErrors(UnsMesh& mesh, fileIO& io);
    virtual ~CCINSTurbErrors();
    //@}

    //! Add one error
    void add(CCINSTurbError e);

    //! Add multiple errors
    void add(CCINSTurbError e, int numerr);

    //! Get the total number of errors for Turbulence category
    static int getSize();

    //! Gather the errors from Turbulence category and position Global array
    void gather(int* arrayGlobal, int offset);

    //! Report errors in the Turbulence category
    bool report(int* sumArrayGlobal, int offset);

    //! Get the maximum allowed error counts for turbulence model
    int getOccurrences(CCINSTurbError e);

    //! Get the error message corresponding to the model enumerator
    const char* getMessage(CCINSTurbError e);

    //! Reset the error counts to 0
    void resetCount(CCINSTurbError e);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSTurbErrors(const CCINSTurbErrors&);
    CCINSTurbErrors& operator=(const CCINSTurbErrors&);
    //@}

    UnsMesh& m_mesh;
    fileIO& m_io;

    int m_turberr[CCINSTurb_TOTAL_ERRORS];
};

}

#endif // CCINSTurbErrors_h
