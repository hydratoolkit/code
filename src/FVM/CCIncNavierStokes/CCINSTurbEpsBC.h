//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSTurbEpsBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Dissipation rate (Epsilon) boundary conditions
//******************************************************************************
#ifndef CCINSTurbEpsBC_h
#define CCINSTurbEpsBC_h

#include <CCSideSetBC.h>
#include <BCPackage.h>

namespace Hydra {

//! Turbulent dissipation rate boundary condition
class CCINSTurbEpsBC : public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSTurbEpsBC(const BCPackage& bc, int offset);
    virtual ~CCINSTurbEpsBC();
    //@}

    //! Set a user-defined BC value
    virtual Real setUserValue();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSTurbEpsBC(const CCINSTurbEpsBC&);
    CCINSTurbEpsBC& operator=(const CCINSTurbEpsBC&);
    //@}

};

}

#endif // CCINSTurbEpsBC_h
