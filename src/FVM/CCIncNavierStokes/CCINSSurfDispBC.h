//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSSurfDispBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Displacement boundary condition based on sidesets
//******************************************************************************
#ifndef CCINSSurfDispBC_h
#define CCINSSurfDispBC_h


#include <CCSideSetBC.h>
#include <DataShapes.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;

class CCINSSurfDispBC : public CCSideSetBC {

  public:

    enum BCvar {
      DISPX,   //!< X-displacement
      DISPY,   //!< Y-displacement
      DISPZ,   //!< Z-displacement
      DISPXT,  //!< X-displacement enforced in local (transformed) coordinates
      DISPYT,  //!< Y-displacement enforced in local (transformed) coordinates
      DISPZT   //!< Z-displacement enforced in local (transformed) coordinates
    };

    //! \name Constructor/Destructor
    //@{
             CCINSSurfDispBC(const BCPackage& bc, int offset);
    virtual ~CCINSSurfDispBC();
    //@}

    //! Apply Cartesian incremental displacement BC's one component at a time
    void applyCartesianInc(UnsMesh& mesh, Real scale,
                           Real* dispN, Real* disInc, Real t,
                           CVector& coordOrig);

    //! Apply incremental displacement BC's
    void applyDisplacementInc(UnsMesh& mesh, Real scale,
                              CVector& dispN, CVector& disInc, Real t);

    //! Apply homogeneous displacement BC's one component at a time
    void applyZeroDisplacement(Real* disp);

    //! Setup the unique node list
    virtual void setupUniqueNodeList(UnsMesh& mesh);

    //! Set a user-defined BC value
    virtual Real setUserValue();

    //! Set the prescribed variable
    void setBCvar(BCvar var) {m_var = var;}

    //! Get the prescribed variables
    BCvar getBCVar() {return m_var;}

    //! Get displacement direction
    GlobalDirection getDirection() const {return m_direction;}

  protected:

    GlobalDirection m_direction;

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSSurfDispBC(const CCINSSurfDispBC&);
    CCINSSurfDispBC& operator=(const CCINSSurfDispBC&);
    //@}

    BCvar m_var;      //!< Displacement variable type
    bool  m_rotation; //!< Flag to indicate the rotation matrix is active
    Real  m_r[9];     //!< generalized rotation matrix

    //! \name Additional data presented for the user-defined function
    int m_nodeId;     //!< Node Id for user access
};

}

#endif // CCINSSurfDispBC_h
