//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSTemperatureBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Temperature BC's
//******************************************************************************
#ifndef CCINSTemperatureBC_h
#define CCINSTemperatureBC_h

#include <SideSetBC.h>
#include <CCINSSolutionOptions.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;

//! Temperature boundary condition based on surfaces
class CCINSTemperatureBC : public SideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSTemperatureBC(const BCPackage& bc, int offset);
    virtual ~CCINSTemperatureBC();
    //@}

    //! Set the edge-data for a given BC
    //!   \param[in]  mode   Flag, indicating how to convert temperature BC
    //!                       - (0) Temperature equation
    //!                       - (1) Specific internal energy equation
    //!                       - (2) Enthalpy equation
    //!   \param[in]  mesh   Reference to the mesh object
    //!   \param[in]  t      Time
    //!   \param[out] gd     Pointer to ghost data array
    virtual void setEdgeTemp(FVMCCINS::EnergyConversionMode mode,
                             UnsMesh& mesh, Real* gd, const Real t);

    //! Set the edge-data for a given BC
    //!   \param[in] mode    Flag, indicating how to convert temperature BC
    //!                       - TOTEMPERATURE: Temperature equation
    //!                       - TOINTENERGY  : Specific internal energy equation
    //!                       - TOENGHTALPY  : Enthalpy equation
    //!   \param[in]  mesh   Reference to the mesh object
    //!   \param[in]  t      Time
    //!   \param[out] bcflag BC flag
    //!   \param[out] gd     Pointer to ghost data array
    virtual void setEdgeTemp(FVMCCINS::EnergyConversionMode mode,
                             UnsMesh& mesh, bool* bcflag, Real* gd,
                             const Real t);

    //! Project surface data to nodes
    //!   \param[in] mode      Flag, indicating how to convert temperature BC
    //!                       - TOTEMPERATURE: Temperature equation
    //!                       - TOINTENERGY  : Specific internal energy equation
    //!                       - TOENGHTALPY  : Enthalpy equation
    //!   \param[in]  mesh     Reference to the mesh object
    //!   \param[in]  t        Time
    //!   \param[out] node_var Pointer to node variables
    //!   \param[out] inverse_distance_wf inverse distance weighting function
    virtual void invDistOp(FVMCCINS::EnergyConversionMode mode,
                           UnsMesh& mesh, const Real t, Real* node_var,
                           Real* inverse_distance_wf);

    //! Set a user-defined BC value
    virtual Real setUserValue();

  protected:

    //! Convert default (temperature) BC value into implementation-specific
    //! (internal energy/enthalpy) value
    //!   \param[in] tem_bcval Temperature BC value
    //!   \param[in] gid       Element global id
    //!   \param[in] mat       Pointer to the material class
    //!   \param[in] mode      Flag, indicating how to convert temperature BC
    //!                       - TOTEMPERATURE: Temperature equation
    //!                       - TOINTENERGY  : Specific internal energy equation
    //!                       - TOENGHTALPY  : Enthalpy equation
    //!   \return internal energy/enthalpy boundary condition value
    virtual Real getBCvalue(Real& tem_bcval,
                            const int /*gid*/,
                            Material& mat,
                            int mode=FVMCCINS::TOTEMPERATURE)
                            const {
      switch(mode) {
      case FVMCCINS::TOTEMPERATURE:
        return tem_bcval;
        break;
      case FVMCCINS::TOINTENERGY:
        return mat.convTemptoEnergy(tem_bcval);
        break;
      case FVMCCINS::TOENTHALPY:
        return mat.convertTemptoEnthalpy(tem_bcval);
        break;
      } return tem_bcval;}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSTemperatureBC(const CCINSTemperatureBC&);
    CCINSTemperatureBC& operator=(const CCINSTemperatureBC&);
    //@}

};

}

#endif // CCINSTemperatureBC_h
