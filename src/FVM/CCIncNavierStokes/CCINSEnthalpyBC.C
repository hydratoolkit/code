//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSEnthalpyBC.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:23 2011
//! \brief   Enthalpy boundary condition based on sidesets
//******************************************************************************

using namespace std;

#include <CCINSEnthalpyBC.h>
#include <BCPackage.h>

using namespace Hydra;

CCINSEnthalpyBC::CCINSEnthalpyBC(const BCPackage& bc, int offset):
  CCSideSetBC(bc, offset)
/*******************************************************************************
Routine: CCINSEnthalpyBC - constructor
Author : Mark A. Christon
*******************************************************************************/
{}

CCINSEnthalpyBC::~CCINSEnthalpyBC()
/*******************************************************************************
Routine: CCINSEnthalpyBC - destructor
Author : Mark A. Christon
*******************************************************************************/
{
  // Easy way to cleanup all registered data
  freeAllVariables();
}
