//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSUserVelocityIC.h
//! \author  Yidong (Tim) Xia
//! \date    Thu Apr 03 11:50:35 2014
//! \brief   User-defined velocity IC's
//******************************************************************************

#ifndef CCINSUserVelocityIC_h
#define CCINSUserVelocityIC_h

#include <CCINSFlow.h>
#include <UserIC.h>

namespace Hydra {

//! User-defined velocity IC
class CCINSUserVelocityIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSUserVelocityIC();
    virtual ~CCINSUserVelocityIC();
    //@}

    //! Virtual function to setup IC's using a mesh and set of DataIndices
    void setICs(UnsMesh* mesh, const DataIndex VEL);

    //! Set user-defined velocity initial conditions
    void setUserVelocity();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSUserVelocityIC(const CCINSUserVelocityIC&);
    CCINSUserVelocityIC& operator=(const CCINSUserVelocityIC&);
    //@}

    //! \name Data presented for the user-defined velocity function
    //@{
    int m_elemId;      //!< Element Id for user access
    int m_matId;       //!< Material Id for user access
    Vector m_coord;    //!< Element centroid coordinates
    Vector m_vel;      //!< Element velocity
    Material* m_mat;   //!< Material class for user access
    //@}

};

}

#endif // CCINSUSERVELOCITYIC_H
