//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSNLKE.h
//! \author  Mark A. Christon
//! \date    Tue Sep 27 14:54:22 2016
//! \brief   Nonlinear K-Epsilon transport equations
//******************************************************************************
#ifndef CCINSNLKE_h
#define CCINSNLKE_h

#include <CCINSKE.h>

namespace Hydra {

class CCINSBoussinesqForce;
class CCINSTemperatureBC;
class CCINSTurbEpsBC;
class CCINSTurbKEBC;

//! Nonlinear K-Epsilon transport equations
class CCINSNLKE : public CCINSKE {

  public:

    //! \name Constructor/Destructor
    //@{
            CCINSNLKE(UnsMesh& mesh,
                      Control& control,
                      DualEdgeGradOp& m_edgeGradOp,
                      CCINSErrors& errors,
                      fileIO& io,
                      CCINSTransportVar& di,
                      vector<CCINSTemperatureBC*>& temperatureBC,
                      vector<CCINSTurbEpsBC*>& turbEpsBCs,
                      vector<CCINSTurbKEBC*>& turbKEBCs,
                      vector<CCINSBoussinesqForce*>& boussinesq_force,
                      LASolver& transport_solver,
                      LAMatrix& transport_A,
                      LAVector& transport_diagA,
                      LAVector& transport_b,
                      LAVector& transport_x,
                      CCINSAdapter& adapter);
    virtual ~CCINSNLKE();
    //@}

    //**************************************************************************
    //! \name Virtual k-epsilon interface
    //! These functions are pure virtual functions that need to be
    //! implemented for each specific of k-epsilon model.
    //@{
    //! Add contributions to the LHS matrix and to the RHS vector due to the
    //! linearized sources for the e-equation
    //!   \param[in,out] S       Reference to the LHS matrix
    //!   \param[in,out] rhs     Pointer to the RHS vector
    virtual void addSourcesKE(LAMatrix* S, Real* rhs);

    //! Add contributions to the LHS matrix and to the RHS vector due to the
    //! linearized sources for the k-equation
    //!   \param[in,out] S       Reference to the LHS matrix
    //!   \param[in,out] rhs     Pointer to the RHS vector
    virtual void addSourcesEps(LAMatrix* S, Real* rhs);

    //! Apply the epsilon penalty based on the wall function type
    virtual void applyWallEpsPenalty(LAMatrix& S, LAVector& B, Real* rhs);

    //! Calculate the turbulent viscosity for the K-Eps (Nonlinear) model
    virtual void calcTurbulentViscosity();

    //! Calculate y* on a surface
    virtual void calcSurfYstar(const int nel, const int* edge_list, Real* ystar);

    //! Calculate y* in the volume
    virtual void calcYstar(Real* dist, Real* ystar);

    //! Set the wall viscosity according to the specific model
    virtual void setWallConductivity(int nedges, int* edge_list, DualEdge* edges,
                                     Real* kmol, Real* keff, Real* edgek);

    //! Set the wall viscosity according to the specific model
    virtual void setWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                  Real* mu, Real* mueff, Real* edgemu);

    //! Solve generic scalar transport equation(s)
    //! \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);
    //@}
    //**************************************************************************

  protected:

    //! Apply the epsilon penalty for scalable wall function
    void applyScalableWallEpsPenalty(LAMatrix& S, LAVector& B, Real* rhs);

    //! Add the wall-function source terms to the LHS and RHS
    void addScalableWallSourcesKE(LAMatrix* S, Real* rhs);

    //! Apply the Bouyancy correction
    void applyBuoyancyCorrectionKE(Real* rhs);

    // ******************* TEST FUNCTIONS **********************************
    void calcDPDK(Real& dPdk, Real ke, Real eps, Real rho, Tensor uij);

    void calcDPDE(Real& dPde, Real ke, Real eps, Real rho, Tensor uij);
    // *********************************************************************

    //! Calculate CMU for the nonlinear k-e model
    void calcElemCmu();

    //! Calculate Reynolds Stresses
    void calcReynoldsStress();

    //! Calculate the wall viscosity according to the specific model
    void calcScalableWallViscosity(int nedges, int* edge_list, DualEdge* edges,
                                   Real* mu, Real* mueff, Real* edgemu);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSNLKE(const CCINSNLKE&);
    CCINSNLKE& operator=(const CCINSNLKE&);
    //@}

    //**************************************************************************
    //! \name Model constants/parameters
    //@{
    bool m_debug;   //!< Model flag to check if to run in debug mode

    Real m_kappa;   //!< von Karman constant, \f$\kappa\f$
    Real m_E;       //!< \f$E = \exp(\kappa B)\f$
    Real m_ln2;     //!< Model constant
    Real m_yp11;    //!< \f$y^* = 11.225\f$
    Real m_c1eps;   //!< Model constant
    Real m_c2eps;   //!< Model constant
    Real m_alpha;   //!< Model constant
    Real m_Cmumax;  //!< Maximum Cmu
    Real m_coeff1;  //!< Model constant for epsilon limiter
    Real m_coeff2;  //!< Model constant for epsilon limiter

    //! \Anisotropic Reynolds Stress Constants
    Real m_CNL1;    //!< Model Constant for Quadratic Reynolds Stress Term 1
    Real m_CNL2;    //!< Model Constant for Quadratic Reynolds Stress Term 2
    Real m_CNL3;    //!< Model Constant for Quadratic Reynolds Stress Term 3
    Real m_CNL6;    //!< Model Constant for Quadratic Reynolds Stress
    Real m_CNL7;    //!< Model Constant for Quadratic Reynolds Stress
    Real m_A0;      //!< Model constant for calculation of C_MU
    Real m_A1;      //!< Model constant for calculation of C_MU
    Real m_A2;      //!< Model constant for calculation of C_MU
    Real m_A3;      //!< Model constant for calculation of C_MU
    //@}
};

}

#endif // CCINSNLKE_h
