//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSHeatSource.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Volumetric heat source terms
//******************************************************************************
#ifndef CCINSHeatSource_h
#define CCINSHeatSource_h

#include <HydraTypes.h>
#include <macros.h>
#include <Source.h>
#include <CCINSFlow.h>
#include <CCINSSolutionOptions.h>

namespace Hydra {

class UnsMesh;

class CCINSHeatSource : public Source {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSHeatSource(SourcePackage& sp) : Source(sp) {}
             CCINSHeatSource(int tblid, Real amp, int setid) :
                             Source(tblid, amp, setid) {}

    virtual ~CCINSHeatSource() {}
    //@}

    //! Apply the heat source
    void apply(UnsMesh& mesh,
               const Real timen,
               const Real timenp1,
               const Real dt,
               const Real thetaFn,
               const Real thetaFnp1,
               const DataIndex TEMPERATURE,
               Real* vol,
               Real* voln,
               Real* rhs,
               const FVMCCINS::SourceMode mode=FVMCCINS::TIME_LEVEL_WEIGHTED);

    //! Set a user-defined source value
    virtual Real setUserValue();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSHeatSource(const CCINSHeatSource&);
    CCINSHeatSource& operator=(const CCINSHeatSource&);
    //@}

    //! \name Data presented for the user-defined heat source function
    //@{
    int m_elemId;      //!< Element Id for user access
    int m_matId;       //!< Material Id for user access
    Real m_time;       //!< Time for source term evaluation
    Real m_T;          //!< Element temperature at time n
    Vector m_coord;    //!< Element centroid coordinates
    Material* m_mat;   //!< Material class for user access
    //@}

};

}

#endif
