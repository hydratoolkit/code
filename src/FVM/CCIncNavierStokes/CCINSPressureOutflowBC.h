//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSPressureOutflowBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Pressure outflow boundary condition based on surfaces
//******************************************************************************
#ifndef CCINSPressureOutflowBC_h
#define CCINSPressureOutflowBC_h


#include <SideSetBC.h>
#include <DataShapes.h>

namespace Hydra {

// Forward declarations
struct BCPackage;


class CCINSPressureOutflowBC : public SideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSPressureOutflowBC(const BCPackage& bc, int offset);
    virtual ~CCINSPressureOutflowBC();
    //@}

    //! Apply a prescribed traction BC
    //!   \param[in]  mesh  Reference to mesh object
    //!   \param[in]  dt    Time step
    //!   \param[in]  p     Pointer to pressure field
    //!   \param[out] rhs   Reference to the rhs vector
    void apply(UnsMesh& mesh, const Real dt, const Real* p, CVector& rhs);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSPressureOutflowBC(const CCINSPressureOutflowBC&);
    CCINSPressureOutflowBC& operator=(const CCINSPressureOutflowBC&);
    //@}

};

}

#endif
