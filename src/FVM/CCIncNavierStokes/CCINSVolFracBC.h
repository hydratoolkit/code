//******************************************************************************
/*!
  \file    src/FVM/CCIncNavierStokes/CCINSVolFracBC.h
  \author  Mark A. Christon
  \date    Mon Jul 31 17:36:30 MDT 2017
  \brief   Volume fraction BCs
 */
//******************************************************************************
#ifndef CCINSVolFracBC_h
#define CCINSVolFracBC_h


#include <CCSideSetBC.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;


//! Volume fraction BC's
class CCINSVolFracBC : public CCSideSetBC {

  public:
             CCINSVolFracBC(const BCPackage& bc, int offset);
    virtual ~CCINSVolFracBC();

    //! Set a user-defined BC value
    virtual Real setUserValue();

  private:
    // Don't permit copy or assignment operators
    CCINSVolFracBC(const CCINSVolFracBC&);
    CCINSVolFracBC& operator=(const CCINSVolFracBC&);
};

}

#endif // CCINSVolFracBC_h
