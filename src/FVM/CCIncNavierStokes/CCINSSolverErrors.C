//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSSolverErrors.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:23 2011
//! \brief   Error handler for CCINSFlow
//******************************************************************************
#include <cmath>
#include <iostream>

using namespace std;

#include <CCINSSolverErrors.h>
#include <fileIO.h>
#include <UnsMesh.h>
#include <MPIWrapper.h>

using namespace Hydra;

static CCINSSolverErrorTable uccinsError[] = {
  { CCINS_XMOMENTUM_CONVERGENCE_ERROR, 50,
    "X-momentum equation - Too many non-converged linear solver iterations."
    "Check:"
    "Linear solver convergence criteria and preconditioner, "
    "Time step size,"
    "Velocity boundary condtions, and initial divergence.",
    false },
  { CCINS_YMOMENTUM_CONVERGENCE_ERROR, 50,
    "Y-momentum equation - Too many non-converged linear solver iterations."
    " Check:"
    " Linear solver convergence criteria and preconditioner,"
    " Time step size,"
    " Velocity boundary condtions and initial divergence",
    false },
  { CCINS_ZMOMENTUM_CONVERGENCE_ERROR, 50,
    "Z-momentum equation - Too many non-converged linear solver iterations."
    "Check:"
    "Linear solver convergence criteria and preconditioner,"
    "Time step size,"
    "Velocity boundary condtions, and initial divergence.",
    false },
  { CCINS_BMATRIX_QUADRATURE_ERROR, 1,
    // Quadrature errors here are cumulative for the entire class,
    // so 1 is the most we can tolerate.
    "Quadrature Errors in B-matrix calculation - Element(s) may be "
    " severely distorted.",
    true },
  { CCINS_PPE_QUADRATURE_ERROR, 8,
    "Quadrature Errors in Pressure Poisson Equation - Element(s) may be"
    " severely distorted.",
    true },
  { CCINS_PPE_CONVERGENCE_ERROR, 50,
    "Pressure Poisson Equation - Too many non-converged linear solver iterations."
    "Check:"
    "Linear solver convergence criteria and preconditioner,"
    "Time step size,"
    "Velocity boundary condtions, and initial divergence.",
    false },
  { CCINS_ENERGY_CONVERGENCE_ERROR, 50,
    "Energy equation - Too many non-converged linear solver iterations."
    "Check:"
    "Linear solver convergence criteria and preconditioner,"
    "Time step size,"
    "Velocity boundary condtions, and initial divergence.",
    false },
  { CCINS_SPECIESMASSFRAC_CONVERGENCE_ERROR, 50,
    "Species mass fraction equation - Too many non-converged linear solver iterations."
    "Check:"
    "Linear solver convergence criteria and preconditioner,"
    "Time step size,"
    "Velocity boundary condtions, and initial divergence.",
    false },
  { CCINS_VOLFRAC_CONVERGENCE_ERROR, 50,
    "Volume fraction equation - Too many non-converged linear solver iterations."
    "Check:"
    "Linear solver convergence criteria and preconditioner,"
    "Time step size,"
    "Velocity boundary condtions, and initial divergence.",
    false },
  { CCINS_LSGRADIENT_ERROR, 25,
    "Errors have been detected in the Least-Squares gradient estimator."
    "Check:"
    "Mesh configuration and element quality.",
    false},
  { CCINS_TOTAL_ERRORS, -1, 0, false }
};

CCINSSolverErrors::CCINSSolverErrors(UnsMesh& mesh,fileIO& io):
  m_mesh(mesh),
  m_io(io)
/*******************************************************************************
Routine: CCINSSolverErrors - constructor
Author : Mark A. Christon
*******************************************************************************/
{
  memset(m_physerr,0,CCINS_TOTAL_ERRORS*sizeof(int));
}

CCINSSolverErrors::~CCINSSolverErrors()
/*******************************************************************************
Routine: ~CCINSSolverErrors - destructor
Author : Mark A. Christon
*******************************************************************************/
{}

void
CCINSSolverErrors::add( CCINSSolverError e)
/*******************************************************************************
Routine: add - increment the CCINSFlow Physics Errors
Author : Mark A. Christon
*******************************************************************************/
{
  m_physerr[e]++;
}

void
CCINSSolverErrors::add( CCINSSolverError e, int numerr)
/*******************************************************************************
Routine: add - increment the CCINSFlow Physics Errors
Author : Mark A. Christon
*******************************************************************************/
{
  m_physerr[e] += numerr;
}

void
CCINSSolverErrors::gather(int* arrayGlobal, int offset)
/*******************************************************************************
Routine: gather - assemble CCINSFlow Physics Errors in the Global Error Array
Author : Mark A. Christon
*******************************************************************************/
{
  for(int i=0; i<CCINS_TOTAL_ERRORS; i++) {
    CCINSSolverError e = static_cast<CCINSSolverError>(i);
    if((g_comm->getPid() != 0) && !uccinsError[e].performGlobalSum)
      resetCount(e);
    arrayGlobal[offset+i] = m_physerr[i];
  }
}

const char*
CCINSSolverErrors::getMessage( CCINSSolverError e)
/*******************************************************************************
Routine: getMessage - return Diagnostic Message For CCINSFlow Physics Error
Author : Mark A. Christon
*******************************************************************************/
{
  return uccinsError[e].msg;
}

int
CCINSSolverErrors::getOccurrences( CCINSSolverError e)
/*******************************************************************************
Routine: getOccurences - Maximum Allowable Occurrences For Each CCINSFlow Error
Author : Mark A. Christon
*******************************************************************************/
{
  return uccinsError[e].occurrences;
}

int
CCINSSolverErrors::getSize()
/*******************************************************************************
Routine: getSize - Maximum number of errors for CCINS
Author : Mark A. Christon
*******************************************************************************/
{
  return CCINS_TOTAL_ERRORS;
}

bool
CCINSSolverErrors::report(int* sumArrayGlobal, int offset)
/*******************************************************************************
Routine: report - write errors
Author : Mark A. Christon
*******************************************************************************/
{
  bool m_mustExit = false;
  for(int i=0; i<CCINS_TOTAL_ERRORS; i++) {
    CCINSSolverError e = static_cast<CCINSSolverError>(i);
    int getCount = sumArrayGlobal[offset+i];
    int canOccur = getOccurrences(e);
    if(getCount > canOccur) {
      const char* msg = getMessage(e);
      m_io.writeError(msg);
      m_mustExit = true;
    }
  }
  return m_mustExit;
}

void
CCINSSolverErrors::resetCount( CCINSSolverError e)
/*******************************************************************************
Routine: resetCount - Reset Specified CCINSFlow Physics Error Count to 0
Author : Mark A. Christon
*******************************************************************************/
{
  m_physerr[e] = 0;
}
