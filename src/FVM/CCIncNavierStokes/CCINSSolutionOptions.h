//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSSolutionOptions.h
//! \author  Robert N. Nourgaliev
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Solution options for CCINSFlow
//******************************************************************************
#ifndef FVMCCINSSolutionOptions_h
#define FVMCCINSSolutionOptions_h

namespace Hydra {

//! \brief FVMCCINS provides solver options for the Hybrid Navier-Stokes solver

namespace FVMCCINS {

//! Source mode flags:
//! TIME_LEVEL_WEIGHTED -- really means time-weighted or centered in time
//! OLD_TIME_LEVEL -- really means explicit at time level n
//! NEW_TIME_LEVEL -- really means implicit at time level n+1

//! Flags for treating sources and advection term contributions to the RHS
enum SourceMode {
  TIME_LEVEL_WEIGHTED, //!< Time-weighted treatment of source terms
  OLD_TIME_LEVEL,      //!< Add old time level contribution
                       //!< both CC and slope contributions in advection
  NEW_TIME_LEVEL       //!< Add new time level contribution (sources)
};

//! Flags for conversion of energy BC's
enum EnergyConversionMode {
  TOTEMPERATURE = 0,  //!< Convert to temperature
  TOINTENERGY,        //!< Convert to specific internal energy
  TOENTHALPY          //!< Convert to enthalpy
};

//! Flags for building linear algebra operators
enum LABuildFLAGS {
  ADD2LHS = 0,  //!< Add to left-hand-side matrix only
  ADD2RHS,      //!< Add to right-hand-side vector only
  ADD2LHS_RHS   //!< Add to both left-hand-side matrix and right-hand-side
};

//! Flags for (non-linear) solution strategies ...
enum SolutionMethod {
  PROJECTION,
  PICARD,
  NEWTONKRYLOV
};

//! Flags for time step control
enum TimeStepType {
  FIXED_TIME_STEP,
  FIXED_CFL_STEP
};

//! Flags for time integration
enum TimeInt {
  THETA_INT,
  BDF2_INT
};

//! Flags for error norm types
enum ErrorNorm {
  ENORM_COMPOSITE,
  ENORM_MAX,
  ENORM_WEIGHT,
  ENORM_RESIDUAL
};

}

}

#endif // FVMCCINSSolutionOptions_h
