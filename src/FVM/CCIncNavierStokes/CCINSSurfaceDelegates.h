//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSSurfaceDelegates.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Surface field output delegates
//******************************************************************************
#ifndef CCINSSurfDelegates_h
#define CCINSSurfDelegates_h

#include <CCINSFlow.h>

namespace Hydra {

//! Declaration of the statistics surface accumulators

ACCUMULATOR_DECL(CCINSFlowAccumulateMeanHeatFlux)
ACCUMULATOR_DECL(CCINSFlowAccumulateMeanShearTraction)
ACCUMULATOR_DECL(CCINSFlowAccumulateMeanNormalTraction)
ACCUMULATOR_DECL(CCINSFlowAccumulateMeanTraction)
ACCUMULATOR_DECL(CCINSFlowAccumulateMeanWallShear)

//! Declaration of the surface delegates

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfAreaField,
                     CCINSFlowSurfAreaField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfCoordVectorField,
                     CCINSFlowSurfCoordVectorField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfHeatFluxVectorField,
                     CCINSFlowSurfHeatFluxVectorField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfMeanHeatFluxVectorField,
                     CCINSFlowSurfMeanHeatFluxVectorField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfMeanShearTractionField,
                     CCINSFlowSurfMeanShearTractionField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfMeanNormalTractionField,
                     CCINSFlowSurfMeanNormalTractionField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfMeanTractionField,
                     CCINSFlowSurfMeanTractionField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfMeanWallShearField,
                     CCINSFlowSurfMeanWallShearField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfMeanPressureField,
                     CCINSFlowSurfMeanPressureField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfPresVarianceField,
                     CCINSFlowSurfPresVarianceField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfRMSPressureField,
                     CCINSFlowSurfRMSPressureField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfTempVarianceField,
                     CCINSFlowSurfTempVarianceField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfRMSTemperatureField,
                     CCINSFlowSurfRMSTemperatureField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfCovDensPresField,
                     CCINSFlowSurfCovDensPresField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfMeanDensityField,
                     CCINSFlowSurfMeanDensityField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfMeanVelocityVectorField,
                     CCINSFlowSurfMeanVelocityVectorField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfMeanTemperatureField,
                     CCINSFlowSurfMeanTemperatureField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfNormalHeatFluxField,
                     CCINSFlowSurfNormalHeatFluxField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfNormalTractionVectorField,
                     CCINSFlowSurfNormalTractionVectorField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfShearTractionVectorField,
                     CCINSFlowSurfShearTractionVectorField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfTractionVectorField,
                     CCINSFlowSurfTractionVectorField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfWallShearField,
                     CCINSFlowSurfWallShearField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfYplusField,
                     CCINSFlowSurfYplusField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfYstarField,
                     CCINSFlowSurfYstarField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfVarYplusField,
                     CCINSFlowSurfVarYplusField)

OUTPUT_DELEGATE_DECL(CCINSFlow,
                     writeSurfChemField,
                     CCINSFlowSurfChemField)

}

#endif // CCINSSurfDelegates_h
