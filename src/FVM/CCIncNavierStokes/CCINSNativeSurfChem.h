//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSNativeSurfChem.h
//! \author  Balu Nadiga
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   NativeSurface Chemistry Interface
//******************************************************************************
#ifndef CCINSNativeSurfChem_h
#define CCINSNativeSurfChem_h

#include <HydraTypes.h>
#include <SurfChemInfo.h>
#include <CCINSSurfChem.h>
#include <CCINSHeatFluxBC.h>

namespace Hydra {

//! Native surface chemistry
class CCINSNativeSurfChem: public CCINSSurfChem {

  public:

    //! \name Constructor/Destructor
    //@{
            CCINSNativeSurfChem(const SurfChemInfo& msci,
                                UnsMesh* mesh,
                                CCINSTransportVar& di);
    virtual ~CCINSNativeSurfChem();
    //@}

    void apply(Real* wallshear, Real& ptime, Real& /*dtflow*/);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSNativeSurfChem(const CCINSNativeSurfChem&);
    CCINSNativeSurfChem& operator=(const CCINSNativeSurfChem&);
    //@}

};

}

#endif
