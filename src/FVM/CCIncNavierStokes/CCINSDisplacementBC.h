//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSDisplacementBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Displacement boundary condition based on node sets
//******************************************************************************
#ifndef CCINSDisplacementBC_h
#define CCINSDisplacementBC_h

#include <NodeSetBC.h>
#include <DataShapes.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;

//! Displacement boundary conditions based on node sets
class CCINSDisplacementBC : public NodeSetBC {

  public:

    enum BCvar {
      DISPX,   //!< X-displacement
      DISPY,   //!< Y-displacement
      DISPZ,   //!< Z-displacement
      DISPXT,  //!< X-displacement enforced in local (transformed) coordinates
      DISPYT,  //!< Y-displacement enforced in local (transformed) coordinates
      DISPZT   //!< Z-displacement enforced in local (transformed) coordinates
    };

    //! \name Constructor/Destructor
    //@{
             CCINSDisplacementBC(const BCPackage& bc);
    virtual ~CCINSDisplacementBC();
    //@}

    //! Apply Cartesian displacement BC's one component at a time
    void applyCartesian(UnsMesh& mesh, Real* disp, Real t);

    //! Apply Cartesian incremental displacement BC's one component at a time
    void applyCartesianInc(UnsMesh& mesh, Real scale,
                           Real* dispN, Real* disInc, Real t);

    //! Apply Cartesian incremental displacement BC's one component at a time
    void applyCartesianInc(UnsMesh& mesh, Real scale,
                           CVector& dispInc, Real t);

    //! Set the prescribed variable
    void setBCvar(BCvar var) {m_var = var;}

    //! Get the prescribed variables
    BCvar getBCVar() {return m_var;}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSDisplacementBC(const CCINSDisplacementBC&);
    CCINSDisplacementBC& operator=(const CCINSDisplacementBC&);
    //@}

    BCvar m_var;      //!< Displacement variable type
    bool  m_rotation; //!< Flag to indicate the rotation matrix is active
    Real  m_r[9];     //!< generalized rotation matrix
};

}

#endif
