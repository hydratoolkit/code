//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSPressureBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Pressure boundary condition based on sidesets
//******************************************************************************
#ifndef CCINSPressureBC_h
#define CCINSPressureBC_h


#include <CCSideSetBC.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;
class LAMatrix;


//! Pressure boundary condition based on surfaces
class CCINSPressureBC : public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSPressureBC(const BCPackage& bc, int offset);
    virtual ~CCINSPressureBC();
    //@}

    //! Apply a nodal-penalty to the diagonal of the linear system
    void applyNodePenalty(int Nproc, int nodeEqStart, int nodeEqEnd,
                          int* nodeeqmap, Real* Adiag);

    //! Apply a nodal-penalty BC
    void applyNodePenaltyBC(UnsMesh& mesh, int Nproc, int nodeEqStart,
                            int nodeEqEnd, int* nodeeqmap, LAMatrix* A,
                            Real* rhs, Real t, Real scale);

    //! Apply a nodal-penalty for an incremental BC
    void applyNodePenaltyIncBC(UnsMesh& mesh, int Nproc, int nodeEqStart,
                               int nodeEqEnd, int* nodeeqmap, LAMatrix* A,
                               Real* rhs, Real tn, Real tnp1, Real scale);

    //! Insert nodal Pressure BCs
    void insertNodalBCs(UnsMesh& mesh, Real* var, Real t);

    //! Check if surface based bc
    bool isSurfaceBased() const;

    //! Mark the prescribed boundary condition
    void markNodePenalty(int kthBc, int* nodeBc);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSPressureBC(const CCINSPressureBC&);
    CCINSPressureBC& operator=(const CCINSPressureBC&);
    //@}

    Real m_penalty;
};

}

#endif // CCINSPressureBC_h
