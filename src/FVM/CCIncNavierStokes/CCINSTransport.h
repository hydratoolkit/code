//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSTransport.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Virutal base class for solving transport equations
//******************************************************************************
#ifndef CCINSTransport_h
#define CCINSTransport_h

#include <vector>

#include <DataShapes.h>
#include <Control.h>
#include <fileIO.h>
#include <LAMatrix.h>
#include <LASolver.h>
#include <LASolverStatus.h>
#include <LAVector.h>
#include <DualEdgeGradOp.h>
#include <UnsMesh.h>
#include <HydraTypes.h>
#include <CCINSErrors.h>
#include <CCINSIncParm.h>
#include <CCINSParm.h>
#include <CCINSTransportVar.h>
#include <CCINSAdapter.h>
#include <CCINSSolutionOptions.h>

namespace Hydra {

//! Transport parameters bundle
struct TransportParam {

  //! Constructor
  TransportParam(const bool dm,   //!< Deformable mesh
                 const bool vc,   //!< Variable conductivity
                 const bool vd,   //!< Varaible density
                 const bool ve,   //!< Variable thermal expansion
                 const bool vs,   //!< Variable specific heat
                 const bool vv,   //!< Variable viscosity
                 const bool hee,  //!< Has energy equation
                 const bool hpf,  //!< Has porous flow
                 const bool hsi,  //!< Has sharp interface
                 const bool hse,  //!< Has species equation
                 const bool htm,  //!< Has turbulence model
                 const int ndw,   //!< Number of wall edges
                 const int nw ) : //!< Number of wall elements
    deformableMesh(dm),
    variableConductivity(vc),
    variableDensity(vd),
    variableExpansion(ve),
    variableSpecificHeat(vs),
    variableViscosity(vv),
    hasEnergyEq(hee),
    hasPorousFlow(hpf),
    hasSharpInt(hsi),
    hasSpeciesEq(hse),
    hasTurbModel(htm),
    Nedge_wall(ndw),
    Nel_wall(nw) {}

  bool deformableMesh;
  bool variableConductivity;
  bool variableDensity;
  bool variableExpansion;
  bool variableSpecificHeat;
  bool variableViscosity;
  bool hasEnergyEq;
  bool hasPorousFlow;
  bool hasSharpInt;
  bool hasSpeciesEq;
  bool hasTurbModel;
  int Nedge_wall;
  int Nel_wall;
};

// Base transport class to derive transport solvers
class CCINSTransport {

  public:

    //! \name Constructor/Destructor
    //@{
            CCINSTransport(UnsMesh& mesh,
                           Control& control,
                           DualEdgeGradOp& edgeGradOp,
                           CCINSErrors& errors,
                           fileIO& io,
                           CCINSTransportVar& di,
                           CCINSAdapter& adapter);
    virtual ~CCINSTransport();
    //@}

    //! Form the RHS for the transport equation
    //!   \param[in]  CCINSIncParm Time increment parameters
    //!   \param[in]  SourceMode   Time-weighting/time-level for source terms
    //!   \param[out] rhs          Right-hand-side for transport eq.
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs=0)=0;

    //! SetUp the base transport class
    virtual void setup(const TransportParam& param);

    //! Solve generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm) = 0;

    //! SetUp the incrementation parameters for the transport solution
    //!   \param[in] CCINSIncParm  Time incrementation parameters
    void setIncParm(const CCINSIncParm& incParm);

    //! Calculate the least-squares gradient
    int calcLSGradient(int Nepe, int Nmel, CVector& elem_grad);

    //! Calculate the local min/max for gradient limiting
    //!   \param[in] Nepe The number of faces per element
    //!   \param[in] Nmel The number of elements in the strip-mining block
    void calcMinMax(int Nepe, int Nmel);

    //! Calculate the error norm for iterative temperature solution
    //!   \param[in] Nel  Number of elements (in the mesh)
    //!   \param[in] Tn   Temperature at time level n
    //!   \param[in] T    Temperature at current time level/iterate
    Real calcErrorNorm(int Nel, Real* Tn, Real* T);

    //! Gather the neighbor data for the limiting procedure
    void gatherNeighbors(int* gids,          // Global element Id's
                         int* eids,          // Dual-edge Id's
                         int  Nepe,          // No. edges per element
                         int  eoffset,       // Element class offset
                         int  Nmel,          // No. of elements in group
                         int  e_start,       // Starting element
                         const CVector&  xc, // Element centroid
                         const CVector& gxc, // Ghost   centroid
                         Real*  var,         // Input variable
                         Real* gvar);        // Input variable -- ghost data

    //! Interpolate a scalar nodal field to get element-centroid values
    void interpElemField(DataIndex node_field, DataIndex elem_field);

    //! Swap ghost vars (scalar version)
    void swapGhostVar(Real* elem_v, Real* ghost_v);

    //! Swap ghost vars (CVector version)
    void swapGhostVar(CVector& elem_v, CVector& ghost_v);

    //! Calculate edge material prop's using element prop's
    void calcEdgeMatProp(Real* elem_prop, Real* edge_prop);

    //! Calculate edge material prop's using element prop's w. IFCs
    void calcEdgeMatPropIFC(Real* elem_prop, Real* edge_prop);

    //! Add the advection terms to the LHS for scalar transport
    //!   \param[in]  wt Pointer to array with weights
    //!   \param[out] K  Reference to the lhs matrix
    void addScalarAdvLhs(Real* wt, LAMatrix& K);

    //! Add the advection terms to the RHS vector for scalar transport
    //!   \param[in]  var          Pointer to scalar
    //!   \param[in]  gvar         Pointer to scalar ghost data
    //!   \param[in]  wt           Pointer to array with weights
    //!   \param[out] rhs          Reference to the rhs vector
    void addScalarAdvRhs(Real* var, Real* gvar, Real* wt, Real* rhs);

    //! Add the advection increment to the RHS vector for scalar transport
    //!   \param[in]  var          Pointer to scalar
    //!   \param[in]  gvar         Pointer to scalar ghost data
    //!   \param[in]  wt           Pointer to array with weights
    //!   \param[out] rhs          Reference to the rhs vector
    void addScalarAdvRhsInc(Real* var, Real* gvar, Real* wt, Real* rhs);

    //! Add the advection terms to the RHS vector for scalar transport
    //!   \param[in]  var          Pointer to scalar
    //!   \param[in]  gvar         Pointer to scalar ghost data
    //!   \param[in]  wt           Pointer to array with weights
    //!   \param[out] rhs          Reference to the rhs vector
    //!   \param[in]  DUALEDGE_VEL Data index for face velocity vector
    //!   \param[in]  mode         Mode of advection operator treatment
    void addScalarAdvRhs(Real* var, Real* gvar, Real* wt, Real* rhs,
                         DataIndex DUALEDGE_VEL, int mode);

    //! Adjust LHS for implicit treatment of advection and do-nothing bcs
    void adjustAdvectVar(Real* wt, bool* bcflag, Real* gphi, LAMatrix& K,
                         Real* rhs);

    //! Add the advection terms to the LHS for scalar transport
    //!   \param[in]  wt  Weighting coefficient
    //!   \param[in]  div Velocity divergence
    //!   \param[out] K   L.H.S. matrix
    void advectVarLhs(Real* wt, Real* div, LAMatrix& K);

    //! Add advection terms to RHS for scalar transport
    //!   \param[in]  xc     Coordinates of cell centers
    //!   \param[in]  gxc    Coordinates of ghost cell centers
    //!   \param[in]  wt     Weight for advection, e.g., density
    //!   \param[in]  vf     dual-edge velocity
    //!   \param[in]  phi    Pointer to the cell-centered scalar function
    //!   \param[in]  gphi   Pointer to the ghost scalar function
    //!   \param[in]  dphi   Limited gradient of scalar field
    //!   \param[in]  gdphi  Limited gradient of scalar field in ghosts
    //!   \param[in]  div    Divergence term for incompresible correction
    //!   \param[out] rhs    RHS vector
    void advectVarRhs(CVector& xc,
                      CVector& gxc,
                      Real*    wt,
                      Real*    vf,
                      Real*    phi,
                      Real*    gphi,
                      CVector& dphi,
                      CVector& gdphi,
                      Real*    div,
                      Real*    rhs,
                      int      mode=FVMCCINS::TIME_LEVEL_WEIGHTED);

    //! Add incremental advection terms to RHS for scalar transport
    //!   \param[in]  xc     Coordinates of cell centers
    //!   \param[in]  gxc    Coordinates of ghost cell centers
    //!   \param[in]  wt     Weight for advection, e.g., density
    //!   \param[in]  vf     dual-edge velocity
    //!   \param[in]  dphi   Limited gradient of scalar field
    //!   \param[in]  gdphi  Limited gradient of scalar field in ghosts
    //!   \param[out] rhs    RHS vector
    void advectVarRhsInc(CVector& xc,
                         CVector& gxc,
                         Real*    wt,
                         Real*    vf,
                         CVector& dphi,
                         CVector& gdphi,
                         Real* rhs);

    //! Calculate the element-centered gradient using least-squares
    //!   \param[in]  xc        Centroids
    //!   \param[in]  gxc       Ghost centroids
    //!   \param[in]  var       Advected variable
    //!   \param[in]  gvar      Ghost variable
    //!   \param[in]  elem_grad Limited element gradient
    void calcGradient(const CVector& xc,
                      const CVector& gxc,
                      Real*  var,
                      Real* gvar,
                      CVector& elem_grad);

    //! Calculate the limited gradient for a given variable
    //!   \param[in]  xc        Centroids
    //!   \param[in]  gxc       Ghost centroids
    //!   \param[in]  var       Advected variable
    //!   \param[in]  gvar      Ghost variable
    //!   \param[in]  elem_grad Limited element gradient
    void calcLimitedGradient(const CVector& xc,
                             const CVector& gxc,
                             Real*  var,
                             Real* gvar,
                             CVector& elem_grad);

    //! Limit the current gradient
    //!   \param[in]  Nepe      The number of faces per element
    //!   \param[in]  Nmel      The number of elements in the strip-mining block
    //!   \param[out] elem_grad The holder for gradient
    void limitGradient(int Nepe, int Nmel, CVector& elem_grad);

    //! Compute the element-centered gradient using the FEM B-matrix
    //!   \param[in]  NODE_VAR  Data index for nodal scalar function
    //!   \param[out] grad      Holder for gradient vector
    void calcElemGradFEM(DataIndex NODE_VAR, CVector& grad);

    //! Interpolate edge gradients to element-centers (unweighted)
    void interpEdgeGradient(CVector& edge_grad, CVector& elem_grad);

    //! For the mass matrix LHS terms for a scalar equation
    void formMassLhs(Real* diag, LAMatrix& M);

    //! For the mass matrix RHS terms for a scalar equation
    void formMassRhs(Real* var, Real* wt, Real* rhs);

    //! This routine calculates the element wall-normal distance
    void calcElementNormalDistance(DataIndex NODE_FIELD,
                                   DataIndex ELEM_FIELD);

    //! Enforce the wall distance on the elements attached to the wall
    void enforceDistanceAtWallElements(Real* ndis);

    //! Calculate weighted edge material property
    void calcWeightedEdgeProp(Real* edgeprop, Real* elemwt, Real* edgewt,
                              Real* wtprop);

    //! reset edge scalar
    void resetEdgeScalar(Real* elemval, Real* edgeval);

    //! Update scalar field variables with relaxation
    //!   \param[in] omega  Relaxation parameter
    //!   \param[in] Nvar   Number of variables in the field
    //    \param[in] NEWVAR Most recently computed variable
    //!   \param[in] OLDVAR Field variable from the previosu time-level
    //!   \param[in] INCVAR Non-linear increment in the solution from n to n+1
    void updateScalarVar(Real omega, int Nvar, DataIndex NEWVAR,
                         DataIndex OLDVAR, DataIndex INCVAR);

    //! Update vector field variables with relaxation
    //!   \param[in] omega  Relaxation parameter
    //!   \param[in] Nvar   Number of variables in the field
    //    \param[in] NEWVAR Most recently computed variable
    //!   \param[in] OLDVAR Field variable from the previosu time-level
    //!   \param[in] INCVAR Non-linear increment in the solution from n to n+1
    void updateVectorVar(Real omega, int Nvar, DataIndex NEWVAR,
                         DataIndex OLDVAR, DataIndex INCVAR);

    //! Apply element penalty for rigid material (CHT)
    void applyRigidElementPenalty(LAMatrix& S, LAVector& B, DataIndex BDIAG,
                                  Real* rhs,
                                  GlobalDirection direction = GLOBAL_XDIR);

    //! Return rigid mat velocity for applying penalty (CHT)
    virtual Real getPenaltyConstraint(Material* mat,
                                      GlobalDirection direction) const;

  protected:

    CCINSTransportVar& m_di;

    UnsMesh& m_mesh;

    CCINSAdapter& m_adapter;  //!< Reference CCINSFlow adapter

    DualEdgeGradOp& m_edgeGradOp;  //!< The edge-based least squares machinery


    Control& m_control;

    fileIO& m_io;

    // Error Handling Interface
    CCINSErrors& m_errors;

    int m_Nproc;
    int m_pid;
    int m_elEqIDOffset;
    int m_Nel;
    int m_Nnp;
    int m_Ndim;
    int m_Nedge;
    int m_Nedge_ext;
    int m_Nedge_ifc;
    int m_Nedge_int;
    int m_Nedge_ghost;

    bool m_deformableMesh;       //!< Indicate current mesh is a deformable mesh
    bool m_restrictOp;           //!< True - use restricted operators w. IFC's
    bool m_variableConductivity; //!< True for variable conductitivity
    bool m_variableDensity;      //!< True for variable density
    bool m_variableExpansion;    //!< True for thermal expansion coefficient
    bool m_variableSpecificHeat; //!< True for specific heat is constant
    bool m_variableViscosity;    //!< True for variable viscosity
    bool m_hasEnergyEq;          //!< Has an energy equation
    bool m_hasPorousFlow;        //!< Porous flow model is present
    bool m_hasSharpInt;          //!< Sharp interface is present
    bool m_hasSpeciesEq;         //!< Has aSpecies equation
    bool m_hasTurbModel;         //!< Has a turbulence model
    int  m_Nedge_wall;           //!< No. edges at no-slip/no-penetration walls
    int  m_Nel_wall;             //!< No. elem. at no-slip/no-penetration walls
    int  m_energyItmax;          //!< Max. It. for non-constant specific heat
    Real m_energyEps;            //!< tolerance for non-constant specific heat

    Real m_penalty;       //!< Penalty multiplier for Dirichlet BC's
    Real m_rigid_penalty; //!< Penalty multiplier for rigid materials

    Real m_dt;
    Real m_time;
    Real m_time_np1;
    Real m_thetaKn;    // 1 - thetaK
    Real m_thetaKnp1;  //     thetaK
    Real m_thetaAn;    // 1 - thetaA
    Real m_thetaAnp1;  //     thetaA
    Real m_thetaFn;    // 1 - thetaF
    Real m_thetaFnp1;  //     thetaF
    Real m_thetaPn;    // 1 - thetaP
    Real m_thetaPnp1;  //     thetaP

    //**************************************************************************
    //! \name Strip mining data
    //@{
    int geid[BLKSIZE];

    //! Strip-mining data for element operations
    Real   m_wc[BLKSIZE];
    Real  m_xec[BLKSIZE], m_yec[BLKSIZE], m_zec[BLKSIZE];

    //! Neighbor values and distances to adjacent cells
    Real  m_ws[BLKSIZE][MAXNB];
    Real m_dxe[BLKSIZE][MAXNB], m_dye[BLKSIZE][MAXNB], m_dze[BLKSIZE][MAXNB];

    //! Face centroids (flux quadrature points)
    Real m_xs[BLKSIZE][MAXNB], m_ys[BLKSIZE][MAXNB], m_zs[BLKSIZE][MAXNB];

    //! Distances from element centroid to faces
    Real m_dx[BLKSIZE][MAXNB], m_dy[BLKSIZE][MAXNB], m_dz[BLKSIZE][MAXNB];

    //! Unit normal
    Real m_ex[BLKSIZE][MAXNB], m_ey[BLKSIZE][MAXNB], m_ez[BLKSIZE][MAXNB];

    //! Min/Max primitive variables & gradients
    Real m_wmax[BLKSIZE], m_wmin[BLKSIZE];
    Real m_dwdx[BLKSIZE], m_dwdy[BLKSIZE], m_dwdz[BLKSIZE];

    //! Temporary array used for optimization in limitGradient
    //! do not move, since this could potentially alter the
    //! optimization in this routine (MAC)
    Real m_ui[MAXNB];
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSTransport(const CCINSTransport&);
    CCINSTransport& operator=(const CCINSTransport&);
    //@}

};

}

#endif // CCINSTransport_h
