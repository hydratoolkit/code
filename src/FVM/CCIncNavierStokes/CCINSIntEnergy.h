//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSIntEnergy.h
//! \author  Robert N. Nourgaliev, Mark A. Christon
//! \date    Thu Jun 25 17:00:00 2012
//! \brief   Solve the energy equation in specific internal energy form
//******************************************************************************
#ifndef CCINSIntEnergy_h
#define CCINSIntEnergy_h

#include <CCINSEnergy.h>
#include <CCINSTemperatureBC.h>

namespace Hydra {

class CCINSIntEnergy : public CCINSEnergy {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSIntEnergy(UnsMesh& mesh,
                            Control& control,
                            DualEdgeGradOp& edgeGradOp,
                            CCINSErrors& errors,
                            fileIO& io,
                            CCINSTransportVar& di,
                            vector<CCINSTemperatureBC*>& tempBCs,
                            vector<CCINSHeatFluxBC*>& heatfluxbc,
                            vector<CCINSHeatSource*>& heat_source,
                            vector<CCINSSurfChem*>& surfchem,
                            LASolver& transport_solver,
                            LAMatrix& transport_A,
                            LAVector& transport_b,
                            LAVector& transport_x,
                            CCINSTurbulence& turbulence,
                            CCINSAdapter& adapter);
    virtual ~CCINSIntEnergy();
    //@}

    //! Assemble generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void assembleSystem(const CCINSIncParm& incParm);

    //! Solve generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);

    //! Virtual form transport Rhs function
    //!   \param[in] CCINSIncParm Time increment parameters
    //!   \param[in] SourceMode   Time-weighting/time-level for source terms
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs=0);

    virtual void formRhsInc(const CCINSIncParm& incParm, Real* rhs=0);

    //! Convert temperature to an energy variable, e.g., enthalpy
    //!   \param[in]  T    temperature
    //!   \param[out] iene internal energy
    virtual void convertTempVar(Real* T, Real* ien);

    //! Convert energy variable to temperature
    //!   \param[in]  iene  internal energy
    //!   \param[out] T     temperature
    virtual void convertEnergyVar(Real* iene, Real* T);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSIntEnergy(const CCINSIntEnergy&);
    CCINSIntEnergy& operator=(const CCINSIntEnergy&);
    //@}

};

}

#endif // CCINSIntEnergy_h

