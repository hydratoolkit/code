//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSFsiPenalty.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Penalty BC for segregated FSI
//******************************************************************************
#ifndef CCINSFsiPenalty_h
#define CCINSFsiPenalty_h

#include <CCSideSetBC.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;
class LAMatrix;

//! Fsi penalty condition
class CCINSFsiPenalty: public CCSideSetBC
{
  public:

    //! \name Constructor/Destructor
    //@{
             CCINSFsiPenalty(const BCPackage& bc, int offset);
    virtual ~CCINSFsiPenalty();
    //@}

    //! Apply a PPE penalty for FSI
    void applyPPEFsiPenalty(UnsMesh& mesh, int Nproc,
                            int nodeEqStart, int nodeEqEnd,
                            int* nodeeqmap,
                            Real dt,  Real uscale, Real* V,
                            Real* uf, Real* rho, Real* adiag,
                            LAMatrix* K);
  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSFsiPenalty(const CCINSFsiPenalty&);
    CCINSFsiPenalty& operator=(const CCINSFsiPenalty&);
    //@}
};

}

#endif // CCINSFsiPenalty_h
