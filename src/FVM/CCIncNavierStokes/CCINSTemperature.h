//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSTemperature.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Solve the energy equation in temperature form
//******************************************************************************
#ifndef CCINSTemperature_h
#define CCINSTemperature_h

#include <CCINSEnergy.h>

namespace Hydra {

class CCINSTemperature : public CCINSEnergy {

  public:

    //! \name Constructor/Destructor
    //@{
             CCINSTemperature(UnsMesh& mesh,
                              Control& control,
                              DualEdgeGradOp& edgeGradOp,
                              CCINSErrors& errors,
                              fileIO& io,
                              CCINSTransportVar& di,
                              vector<CCINSTemperatureBC*>& tempBCs,
                              vector<CCINSHeatFluxBC*>& heatfluxbc,
                              vector<CCINSHeatSource*>& heat_source,
                              vector<CCINSSurfChem*>& surfchem,
                              LASolver& transport_solver,
                              LAMatrix& transport_A,
                              LAVector& transport_b,
                              LAVector& transport_x,
                              CCINSTurbulence& turbulence,
                              CCINSAdapter& adapter);
    virtual ~CCINSTemperature();
    //@}

    //! Assemble generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void assembleSystem(const CCINSIncParm& incParm);

    //! Convert energy variable to temperature
    //!   \param[in]  energyvar    energy variable
    //!   \param[out] temperature  temperature
    virtual void convertEnergyVar(Real* /*energyvar*/, Real* /*T*/) {}

    //! Convert temperature to an energy variable, e.g., enthalpy
    //!   \param[in]  temperature  temperature
    //!   \param[out] energyvar    energy variable
    virtual void convertTempVar(Real* /*T*/, Real* /*energyvar*/) {}

    //! Virtual form transport Rhs function
    //!   \param[in] CCINSIncParm Time increment parameters
    //!   \param[in] SourceMode   Time-weighting/time-level for source terms
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs=0);

    virtual void formRhsInc(const CCINSIncParm& incParm, Real* rhs=0);

    //! Modify the LHS for iteration w. variable Cp
    void modifyLhs(Real* Cp, Real* rho, Real* rhoCp, LAMatrix& M);

    //! Solve generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);

  protected:

    //! Form transport Rhs function
    //    \param[in]     incParm Increment parameter functions
    //    \param[in]     mode    Mode of advection operator treatment
    //    \param[in,out] rhs     Pointer to the rhs vector
    ///
    void m_formRhs(const CCINSIncParm& incParm, Real* rhs,
                 const FVMCCINS::SourceMode mode=FVMCCINS::TIME_LEVEL_WEIGHTED);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSTemperature(const CCINSTemperature&);
    CCINSTemperature& operator=(const CCINSTemperature&);
    //@}

};

}

#endif // CCINSTemperature_h

