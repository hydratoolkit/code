//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSSurfChem.h
//! \author  Balu Nadiga, Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Base class for surface chemistry
//******************************************************************************
#ifndef CCINSSurfChem_h
#define CCINSSurfChem_h

#include <HydraTypes.h>
#include <SurfChemInfo.h>
#include <SurfChem.h>
#include <CCINSHeatFluxBC.h>
#include <UnsMesh.h>
#include <CCINSSolutionOptions.h>
#include <CCINSTransportVar.h>
#include <Category.h>
#include <Control.h>

namespace Hydra {

class fileIO;
struct OutputDelegateKey;

struct SCDI {
  //! \name DataIndex values for Surf Chem fields
  //@{
  DataIndex TCOOL;
  DataIndex TCLAD;
  DataIndex SAREA;
  DataIndex SHEAR;
  DataIndex RCRUD;
  DataIndex LCRUD;
  DataIndex HFLUX;
  DataIndex EDGEK;
  //@}
};

class CCINSSurfChem: public SurfChem {

  public:

    //! \name Constructor/Destructor
    //@{
            CCINSSurfChem(const SurfChemInfo& msci, UnsMesh* mesh,
                          CCINSTransportVar& di);
    virtual ~CCINSSurfChem();
    //@}

    //! Method to set associated heatfluxbc pointer
    void setHeatFluxBC(CCINSHeatFluxBC* p) {m_heatfluxbc = p;}

    //! Method to get associated heatfluxbc pointer
    CCINSHeatFluxBC* getHeatFluxBC() const {return m_heatfluxbc;}

    //! Method to take into account CRUD resistivity
    void modifyEdgeThermalConductivity(Real* edgek, Real& ptime);

    //! Method to echo input
    virtual void echoInput(ostream& ofs);

    //! Method to register basic data
    virtual void registerData();

    //! Method to do basic initialization
    virtual void initialize(Real* surfarea, CCINSHeatFluxBC* hf,
                            Control* control);

    //! (pure virtual) apply method
    virtual void apply(Real* wallshear, Real& ptime, Real& /*dtflow*/) =0;

  protected:

    CCINSTransportVar& m_di;

    UnsMesh* m_mesh;

    CCINSHeatFluxBC* m_heatfluxbc;

    SCDI m_scdi;

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSSurfChem(const CCINSSurfChem&);
    CCINSSurfChem& operator=(const CCINSSurfChem&);
    //@}

};

}

#endif
