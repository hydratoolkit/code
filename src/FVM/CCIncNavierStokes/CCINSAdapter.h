//******************************************************************************
//! \file    src/FVM/CCIncNavierStokes/CCINSAdapter.h
//! \author  Robert N. Nourgaliev, Mark A. Christon
//! \date    Thu Oct 03 13:00:00 2012
//! \brief   Virtual adapter (bridge) class for CCINSFlow
//******************************************************************************
#ifndef CCINSAdapter_h
#define CCINSAdapter_h

#include <Control.h>
#include <UnsMesh.h>

namespace Hydra {

class CCINSAdapter {

  public:

    //! DoF names for global solution vector
    enum dofName {
      VELOCITY_DOF = 0, //!< Velocity
      ENERGY_DOF,       //!< Temperature, enthalpy or internal energy
      PRESSURE_DOF,     //!< Pressure or Lagrange multiplier
      SPECIES_DOF,      //!< Species mass fraction
      TURBNU_DOF,       //!< Spalart-Allmaras variable
      TURBKE_DOF,       //!< Turbulent kinetic energy
      TURBEPS_DOF,      //!< Turbulent dissipation rate
      TURBZETA_DOF,     //!< Zeta for k-e-zeta-f model
      TURBPROD_DOF,     //!< Turbulent production
      TURBOMEGA_DOF,    //!< Inverse time-scale (omega)
      NUMBER_DOF        //!< Number of DOF offsets in this physics
    };

    //! \name Constructor/Destructor
    //@{
             CCINSAdapter() {}
    virtual ~CCINSAdapter() {}
    //@}

    //! Picard solve
    virtual void picardSolve()=0;

    virtual void formAllRhs()=0;

    //! NKA restart
    virtual void restartNKA() = 0;

    //! Projection solve
    virtual void projectionSolve()=0;

    //!< Get the number of non-linear iterations
    virtual int getNLIter()=0;

    //! Get flag, indicating the energy equation is present
    virtual bool hasEnergyEq()=0;

    //! Get the list of data indexes for variables in the global
    //! solution vector of old nonlinear iteration
    virtual const vector<DataIndex>& getGSVIterDataIndex()=0;

    //! Get the list of data indexes for variables in the global
    //! solution vector of new nonlinear iteration
    virtual const vector<DataIndex>& getGSVDataIndex()=0;

    //! Get the list of data indexes for variables in the global
    //! solution vector of solution increments
    virtual const vector<DataIndex>& getGSVIncrDataIndex()=0;

    //! Get mapping indexes from m_GSVnames to m_dofmap
    virtual const int* getGSVindx()=0;

    //! Compute edge-centered gradient from the FEM element-centered gradient
    //!   \param[in]  ELGRAD  Elem gradient
    //!   \param[in]  EDGRAD  Edge gradient
    virtual void calcEdgeGradFEM(const DataIndex ELGRAD,
                                 const DataIndex EDGRAD) = 0;

 private:

    //! Don't permit copy or assignment operators
    //@{
    CCINSAdapter(const CCINSAdapter&);
    CCINSAdapter& operator=(const CCINSAdapter&);
    //@}

};

} // end of "namespace Hydra"

#endif
