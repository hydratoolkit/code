//******************************************************************************
//! \file    src/FVM/CCBurgers/CCBurgers.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:39 2011
//! \brief   Class for solving Burgers' eq.
//******************************************************************************

#ifndef CCBurgers_h
#define CCBurgers_h

#include <Control.h>
#include <fileIO.h>
#include <UnsMesh.h>
#include <CCclaw.h>
#include <CCAdvection.h>

namespace Hydra {

//! Finite Volume Burgers' class
class CCBurgers : public CCAdvection {

  public:

    //! \name Constructor/Destructor
    //@{
             CCBurgers() {}
    virtual ~CCBurgers() {}
    //@}

    //**************************************************************************
    //! Virtual function to setup initial conditions for advection-diffusion
    virtual void initialize();

    virtual void finalize() {}

    virtual void writeSolving();
    //**************************************************************************

    //! Function to evaluate exact solution for verification studies
    void evalExactField(int icopt, Real t, Real* u);

    virtual void updateField(Real dt, Real* temp1, Real* temp);

    //! \name I/O functions specific to this solver
    //@{
    //! Write the header for the global time-history data file
    virtual void globHeader(ofstream& globf);

    //! Write the global time history data at each step
    virtual void writeGlobal(ofstream& globf, Real time, Real ke);
    //@}

    // Dummy functions for Burgers' Eq. for now
    virtual void calcDiff(Real /*t*/,
                          DataIndex /*U*/,
                          DataIndex /*UEXACT*/,
                          DataIndex /*UDIFF*/) {}

    virtual void calcErrors(Real /*t*/, DataIndex /*U*/, DataIndex /*UEXACT*/) {}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCBurgers(const CCBurgers&);
    CCBurgers& operator=(const CCBurgers&);
    //@}
};

}
#endif // CCBurgers_h
