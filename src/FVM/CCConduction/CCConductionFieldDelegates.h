//******************************************************************************
//! \file    src/FVM/CCConduction/CCConductionFieldDelegates.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Field delegates for cell-centered heat conduction
//******************************************************************************

#include <CCConduction.h>

namespace Hydra {

//! Declaration of the field delegates

OUTPUT_DELEGATE_DECL(CCConduction,
                     writeNodeCapacitanceField,
                     CCConductionNodeCapacitanceField)

OUTPUT_DELEGATE_DECL(CCConduction,
                     writeNodeTemperatureField,
                     CCConductionNodeTemperatureField)

}
