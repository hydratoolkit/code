//******************************************************************************
//! \file    src/FVM/CCConduction/CCConductionErrorTypes.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:27 2011
//! \brief   Error types for cell-centere heat conduction
//******************************************************************************
#ifndef  CCConductionErrorTypes_h
#define  CCConductionErrorTypes_h

//! Error Enumerators for Cell-centered Heat Conduction Physics

//! Basic solver errors
enum CCConductionSolverError {
    CCConduction_CONVERGENCE_ERROR=0,  //! Total no. failures to converge
    CCConduction_TOTAL_ERRORS
};

#endif // CCConductionErrorTypes_h
