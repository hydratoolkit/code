//******************************************************************************
//! \file    src/FVM/CCConduction/CCConductionSolverErrors.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:27 2011
//! \brief   Solver errors for cell-centered FVM heat conduction
//******************************************************************************
#ifndef CCConductionSolverErrors_h
#define CCConductionSolverErrors_h

#include <CCConductionErrorTypes.h>

namespace Hydra {

// Forward Declaration
class UnsMesh;
class fileIO;

struct CCConductionErrorTable
{
  CCConductionSolverError errType;
  int occurrences;
  const char* msg;
  bool performGlobalSum;
};

//! Class for accumulating errors in cell-centered heat conduction category
class CCConductionSolverErrors
{
  public:

    //! \name Constructor/Destructor
    //@{
             CCConductionSolverErrors(UnsMesh& mesh, fileIO& io);
    virtual ~CCConductionSolverErrors();
    //@}

    //! Add the errors corresponding to the CCConduction enumerator value
    void addError(CCConductionSolverError e);

    //! Add the errors corresponding to the CCConduction enumerator value
    void addError(CCConductionSolverError e, int numerr);

    //! Check for cumulative error status to see if any error exceeds
    //  the prescribed max number of occurrences
    bool cumulativeErrExitStatus();

    //! Get the maximum allowed error counts
    int getOccurrences(CCConductionSolverError e);

    //! Get the error message corresponding to the CCConduction enumerator
    const char* getMessage(CCConductionSolverError e);

    //! Reset the error counts to 0
    void resetCount(CCConductionSolverError e);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCConductionSolverErrors(const CCConductionSolverErrors&);
    CCConductionSolverErrors& operator=(const CCConductionSolverErrors&);
    //@}

    //! Mesh, IO objects
    UnsMesh& m_mesh;
    fileIO& m_io;

    //! Array for conduction errors
    int m_error[CCConduction_TOTAL_ERRORS];
};

}

#endif // CCConductionSolverErrors_h
