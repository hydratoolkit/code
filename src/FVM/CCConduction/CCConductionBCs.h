//******************************************************************************
//! \file    src/FVM/CCConduction/CCConductionBCs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:27 2011
//! \brief   BC's for cell-centered FVM heat conduction
//******************************************************************************

#ifndef CCConductionBCs_h
#define CCConductionBCs_h

#include <UnsMesh.h>
#include <SideSetBC.h>

namespace Hydra {

//! Forward declarations
struct BCPackage;

//! Prescribed heat flux boundary condition based on surface sets
class CCConductionConvectionBC : public SideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCConductionConvectionBC();
    virtual ~CCConductionConvectionBC();
    //@}

    //! Set the Dirichlet BC data in ghost list
    //!   \param[in] mesh  CFD data mesh
    //!   \param[in] U     DataIndex for the temperature
    //!   \param[in] GD    DataIndex for the ghost-data
    void setGhostBC(UnsMesh& /*mesh*/, DataIndex /*U*/, DataIndex /*GD*/) {}

    Real getTambient() {return m_Tambient;}

  protected:

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCConductionConvectionBC(const CCConductionConvectionBC&);
    CCConductionConvectionBC& operator=(const CCConductionConvectionBC&);
    //@}

    Real m_Tambient;

};

//! HeatFlux boundary condition based on surface sets
class CCConductionHeatFluxBC : public SideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCConductionHeatFluxBC();
    virtual ~CCConductionHeatFluxBC();
    //@}


  private:

    //! Don't permit copy or assignment operators
    //@{
    CCConductionHeatFluxBC(const CCConductionHeatFluxBC&);
    CCConductionHeatFluxBC& operator=(const CCConductionHeatFluxBC&);
    //@}

};

//! Temperature boundary condition based on sidesets
class CCConductionTemperatureBC : public SideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCConductionTemperatureBC(const BCPackage& bc);
    virtual ~CCConductionTemperatureBC();
    //@}

    //! Apply temperature BC's and flag
    //!   \param[in] mesh   CFD data mesh
    //!   \param[in] gbc    Ghost bc array
    //!   \param[in] bcflag Bc Flag array
    //!   \param[in] time   time
    void apply(UnsMesh& mesh, Real* gbc, bool* bcflag, Real time);

    //! Apply temperature BC's -- data for RHS
    //!   \param[in] mesh CFD data mesh
    //!   \param[in] RHS  DataIndex for the RHS vector
    //!   \param[in] time time
    //!   \param[in] dt   time-step
    void apply(UnsMesh& mesh, DataIndex RHS, Real time, Real dt);

    //! Apply temperature BC's -- data for RHS
    //!   \param[in] mesh  CFD data mesh
    //!   \param[in] MPK   DataIndex for the "[M+K]" LHS operator
    //!   \param[in] DIAGI DataIndex for the index for the diagonal entries
    //!   \param[in] dt    time-step
    void apply(UnsMesh& mesh, DataIndex MPK, DataIndex DIAGI, Real dt);

    //! Get temperature BC flag
    //!   \param[in] mesh   CFD data mesh
    //!   \param[in] bcflag BC Flag array
    void setEdgeBCs(UnsMesh& mesh,  bool* bcflag);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCConductionTemperatureBC(const CCConductionTemperatureBC&);
    CCConductionTemperatureBC& operator=(const CCConductionTemperatureBC&);
    //@}
};

}
#endif // CCConductionBCs_h
