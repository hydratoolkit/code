//******************************************************************************
//! \file    src/FVM/CCConduction/CCConductionSource.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:27 2011
//! \brief   Heat conduction source term
//******************************************************************************

#ifndef CCConductionSource_h
#define CCConductionSource_h

#include <Source.h>

namespace Hydra {

class UnsMesh;

//! Volumetric heat source terms
class CCConductionSource : public Source {

  public:

    //! \name Constructor/Destructors
    //@{
             CCConductionSource(int tblid, Real amp, int setid) :
               Source(tblid, amp, setid) {}
    virtual ~CCConductionSource() {}
    //@}

    //! Apply the heat source
    void apply(UnsMesh& mesh,
               const Real timen, const Real timenp1, const Real dt,
               const Real thetaFn, const Real thetaFnp1,
               const Real* vol, Real* rhs);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCConductionSource(const CCConductionSource&);
    CCConductionSource& operator=(const CCConductionSource&);
    //@}

};

}
#endif
