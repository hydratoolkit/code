//******************************************************************************
//! \file    src/FVM/CCConduction/CCConduction.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:27 2011
//! \brief   Cell-centered FVM heat conduction
//******************************************************************************

#ifndef CCConduction_h
#define CCConduction_h

#include <iostream>
#include <vector>

#include <CCclaw.h>
#include <CCConductionBCs.h>
#include <CCConductionSolverErrors.h>

namespace Hydra {

class Control;
class DualEdgeGradOp;
class LASolver;
class LAVector;
class LAMatrix;
class UnsMesh;
class fileIO;
class Timer;
class CCConductionConvectionBC;
class CCConductionHeatFluxBC;
class CCConductionTemperatureBC;
class CCConductionSource;

//! Struct for IC's
struct CCConductionICPackage {
  Real temp;
  CCConductionICPackage() : temp (0.0) {}

 };

//! Cell-centered FVM heat conduction
class CCConduction : public CCclaw {

  public:

    //! \name Constructor/Destructor
    //@{
             CCConduction();
    virtual ~CCConduction() {}
    //@}

    //**************************************************************************
    //! \name Virtual public interface
    //!@{
    //! Check the health of the code during solution phase
    virtual bool codeStatus();

    //! Echo physics-specific options
    virtual void echoOptions(ostream& ofs);

    //! Finalize the physics
    virtual void finalize();

    //! Initialize the physics
    virtual void initialize();

    //! Register all solution variables
    virtual void registerData();

    //! Allocate and setup physics specific variables
    virtual void setup();

    //! Setup variables that can be migrated for dynamics load-balancing
    virtual void setupMigrationVars(vector<DataIndex>& elem_var,
                                    vector<DataIndex>& node_var);

    //! Solve the physics problem
    virtual void solve();

    //! Write a message identifying the physics solver
    virtual void writeSolving();

    //! Setup merged sideset BC's
    virtual void setupMergedSetBCs();
    //@}

    //**************************************************************************
    //! \name Boundary Conditions and Initial Conditions
    //@{
    //! Manage Temperature BC's, heat flux, sources, etc.
    void addTemperatureBC(const BCPackage& bc);

    void addConvectionBC();

    void addHeatSource(int tblid, Real amp, int setid);

    void addHeatFluxBC();

    void setICs(CCConductionICPackage& ics);
    //@}

    //**************************************************************************
    //! \name Output related functions
    //@{
    //! Output delegate for capacitance at the node
    void writeNodeCapacitanceField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Output delegate for Processor ID at the element
    void writeElemProcessorIDField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Output delegate for Processor ID at the node
    void writeNodeProcessorIDField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Output delegate for temperature at the node
    void writeNodeTemperatureField(const OutputDelegateKey& key,
                                   int plnum, int varId);
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCConduction(const CCConduction&);
    CCConduction& operator=(const CCConduction&);
    //@}

    //**************************************************************************
    //! \name Boundary condition and source methods
    //@{
    //! Apply boundary conditions
    void applyBCs();

    //! Apply boundary conditions and get the flag for rhs and lhs
    void applyBCsandFlag(Real time,Real* var, Real* gt, bool* bcflag);

    //! Apply prescribed ConvectionBC's
    void applyConvectionBCs();

    //! Apply heat source
    void applyHeatSources(Real* rhs);

    //! Setup the BC flag
    bool* setupBCFlag();
    //@}

    //**************************************************************************
    //! \name Communications functions & related utilities
    //@{
    //! Setup the ghost centroid coordinates for boundary data
    void setGhostCentroids();
    //@}

    //**************************************************************************
    //! \name Material evaluation/update
    //@{
    //! Calculate edge material prop's using element prop's
    void calcEdgeMatProp(Real* elem_prop, Real* edge_prop);

    //! Check external edge range
    void checkExternalEdgeRange() const;

    //! Update the material state
    void updateMaterialState();

    //! Update the thermal conductivity
    void updateThermalConductivity();
    //@}

    //**************************************************************************
    //! \name Heat conduction equation specific methods
    //@{
    //! Calculate the heat capacitance
    void calcCapacitance();

    //! Compute the mass operator
    void formMassLhs(LAMatrix* M);

    //! Calculate the right-hand-side terms
    void formMassRhs(Real* Rhs);

    //! Initialize the field variables
    void initFieldVars();

    //! Set the increment solution parameter
    void setIncParam();
    //@}

    //**************************************************************************
    //! \name linear algebra related functions
    //@{
    //! Initialize the linear equation solvers, matrices and vectors
    void initLinearSolvers();

    //! Finalize the linear equation solvers, matrices and vectors
    void finalLinearSolvers();
    //@}

    //**************************************************************************
    //! \name Output related functions
    //@{
    //! Echo out boundary conditions
    void echoBCs(ostream& ofs);

    //! Echo the heat sources
    void echoHeatSources(ostream &ofs);

    //! Echo IC's
    void echoICs(ostream& ofs);

    //! Echo the time integrator
    void echoTimeInt(ostream& ofs);

    //! Write the glob file
    void writeGlobFile(int inc, bool field, bool history, bool restart);

    //! Write the screen report
    void writeReport(ostream& ofs, int Ninc);

    //! Write the screen report header
    void writeReportHeader(ostream& ofs);

    //! Generic history delegate for scalar
    void formSurfProjID(Real* node_var, Real* work, int* iflag);
    //@}

    //**************************************************************************
    //! \name Registration for all variables, communications buffers, timers
    //@{
    //! Register turbulence derived output delegates
    void registerCommBuff();

    //! Register derived output delegates ONLY
    void registerDerivedVar();

    //! Register material data
    void registerMaterialData();

    //! Register primary primitive variables and their output delegates
    void registerPrimVar();

    //! Register all timers for this physics
    void registerTimers();
    //@}

    //**************************************************************************
    //! \name Data members
    //@{
    //! Dual-edge gradient operator
    DualEdgeGradOp* m_edgeGradOp;

    //! Time step data
    Real m_dt;
    Real m_totalTime;

    //! The increment parameters
    int m_Ninc;
    Real m_time;
    Real m_time_np1;

    bool m_isRestart;      //!< Indicate current step is a restart

    //! Time weights
    Real m_thetaKn;   // thetak
    Real m_thetaKnp1; // thetak

    int m_ttyi;       // Interval for screen prints

    int m_elemvar_dim;
    int m_sendvar_dim;

    //! Basic variables
    DataIndex TEMPERATURE;
    DataIndex CAPACITANCE;
    DataIndex SOURCE;

    //! Storage for material property update/evaluation
    DataIndex ELEM_CONDUCTIVITY;        // Element-level thermal conductivity
    DataIndex EDGE_CONDUCTIVITY;        // Dual-edge thermal conductivity

    //! Data required for the element equations
    DataIndex ELEMEQ_VAR;        // solution array for element

    //! Scratch arrays
    DataIndex TEMP_NODE;
    DataIndex TEMP_ELEM;

    //! Ghost data arrays
    DataIndex GHOST_VAR;
    DataIndex GHOST_VARFLG;

    //! Arrays of BC objects
    vector<CCConductionTemperatureBC*> m_temperaturebc;
    vector<CCConductionConvectionBC*>  m_convectionbc;
    vector<CCConductionHeatFluxBC*>    m_heatfluxbc;

    //! Volumetric heat sources
    vector<CCConductionSource*> m_heat_source;

    //! Conduction Linear Solver
    int elEqIDOffset;
    int mxElrow;
    int mxElrowOff;
    vector<int> m_el_d_nnz;
    vector<int> m_el_o_nnz;

    LASolver* m_solver;
    LAMatrix* m_A;
    LAVector* m_b;
    LAVector* m_x;

    //! Timers
    TimerIndex LSGRAD_TIME;
    TimerIndex RHS_TIME;
    TimerIndex SOLVE_TIME;
    TimerIndex LASOLVE_TIME;
    TimerIndex WRITE_TIME;

    //! Error Accumulation Interface
    CCConductionSolverErrors* m_errors;

    //! Simple IC's
    CCConductionICPackage m_ics;
    //@}

};

}
#endif // CCConduction_h
