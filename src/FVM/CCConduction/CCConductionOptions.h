//******************************************************************************
//! \file    src/FVM/CCConduction/CCConductionOptions.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Solution options for CCConduction
//******************************************************************************
#ifndef CCConductionOptions_h
#define CCConductionOptions_h

namespace Hydra {

//! \brief CCConductionOptions provides the solution options

namespace CCConductionOptions {

//! Flags for time step control
enum StepType {
  FIXED_TIME_STEP,
  ADAPTIVE_TIME_STEP
};

//! Flags for time integration
enum TimeInt {
  THETA_INT,
  BDF2_INT
};

} // end of namespace CCConductionOptions

}

#endif // CCConductionOptions_h
