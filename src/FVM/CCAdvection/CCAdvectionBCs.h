//******************************************************************************
//! \file    src/FVM/CCAdvection/CCAdvectionBCs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:27 2011
//! \brief   Cell-centered advection boundary conditions
//******************************************************************************

#ifndef CCAdvectionBCs_h
#define CCAdvectionBCs_h

#include <HydraTypes.h>
#include <UnsMesh.h>
#include <SideSetBC.h>

namespace Hydra {

//! Convected field boundary condition based on sidesets
class CCAdvectionBC: public SideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCAdvectionBC(const BCPackage& bc, int offset);
    virtual ~CCAdvectionBC() {}
    //@}

    //! Setup Dirichlet BC in ghost list for later use in flux calculation
    void setupGhostBC(UnsMesh* mesh, Real* gt, Real t);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCAdvectionBC(const CCAdvectionBC&);
    CCAdvectionBC& operator=(const CCAdvectionBC&);
    //@}

};

}
#endif // CCAdvectionBCs_h
