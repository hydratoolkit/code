//******************************************************************************
//! \file    src/FVM/CCAdvection/CCError.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:27 2011
//! \brief   Hierarchical error evaluation for cell-centered advection
//******************************************************************************

#ifndef CCError_h
#define CCError_h

namespace Hydra {

typedef struct _Quad4R {
  //! \name 4-node Quadrilateral Refinement Structure
  //! 4-node quadrilateral structure for refinement-based error estimation
  //@{
  struct _Quad4R *next;
  struct _Quad4R *prev;
  Real x[4];
  Real y[4];
  Real v;
  int  gen;
  //@}
} Quad4R;

}
#endif
