//******************************************************************************
//! \file    src/FVM/CCAdvection/CCAdvection.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:27 2011
//! \brief   Cell-centered DG/FVM advection
//******************************************************************************

#ifndef CCAdvection_h
#define CCAdvection_h

#include <BCPackage.h>
#include <Control.h>
#include <fileIO.h>
#include <UnsMesh.h>
#include <CCclaw.h>
#include <CCAdvectionBCs.h>
#include <CCError.h>

namespace Hydra {

//! Finite Volume Advection-Diffusion class
class CCAdvection : public CCclaw {

  public:

    //! \name Constructor/Destructor
    //@{
             CCAdvection();
    virtual ~CCAdvection() {}
    //@}

    //**************************************************************************
    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData();
    //@}

    //! \name Virtual Physics Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    virtual void setup();

    virtual void initialize();

    virtual void solve();

    virtual void finalize() {}

    virtual void setupMergedSetBCs();
    //@}

    //! \name Virtual Functions for I/O
    //! These functions are implemented for each specific physics
    //! to permit custom options to be coded for reading and writing
    //! restart files, time-history data, bc's, etc.
    //@{
    virtual void writeSolving();

    virtual void echoOptions(ostream& ofs);

    //@}
    //**************************************************************************

    //! Echo boundary conditions
    void echoBCs(ostream& ofs);

    //! Function to evaluate exact solution for verification studies
    void evalExactField(Real t, Real* u);

    //! \name Functions for cache-blocking
    //! These functions are used to setup the cache-blocks for the
    //! gradient computation.
    //@{

    void gatherNeighbors(int* gids,      // Global element Id's
                         int* eids,      // Dual-edge Id's
                         int  eoffset,   // Element offset
                         Real* temp,     // Scalar variable
                         Real* gt,       // Primitive ghost data
                         CVector& gxc,   // Ghost centroid
                         Real* xc,       // X-centroid
                         Real* yc);      // Y-centroid

    void gatherNeighbors(int* gids,      // Global element Id's
                         int* eids,      // Dual-edge Id's
                         int  eoffset,  // element offset
                         Real* temp,     // Conserved variables
                         Real* gt,       // Primitive ghost data
                         CVector& gxc,   //ghost centroid
                         Real* xc,       // X-centroid
                         Real* yc,       // Y-centroid
                         Real* zc);      // Z-centroid

    void scatterNeighbors(Real* tx, Real* ty);

    void scatterNeighbors(Real* tx, Real* ty, Real* tz);
    //@}

    void setupGhosts(Real* temp, Real* gt);

    void calcLimitedGradient(Real* temp);

    void calcGaussGradient();

    void calcLSGradient();

    void calcMinMax();

    void limitGradient();

    void KRlimitGradient();

    virtual void updateField(Real dt, Real* temp1, Real* temp);

    //! \name Global utility functions
    //@{
    //! Compute the global kinetic energy
    Real calcKE(DataIndex U);

    //! Compute L1, L2, Linf errors
    virtual void calcErrors(Real t, DataIndex U, DataIndex UEXACT);

    virtual void calcDiff(Real t,
                          DataIndex U, DataIndex UEXACT, DataIndex UDIFF);

    Real calcUbar(Real* x, Real* y, Real V, Real t, Eltype etype);

    Real evalU(Real x, Real y, Real t);

    void splitQuad(Quad4R* q);
    //@}

    //! \name I/O functions specific to this solver
    //@{
    //! Write the header for the global time-history data file
    virtual void globHeader(ofstream& globf);

    //! Write the global time history data at each step
    virtual void writeGlobal(ofstream& globf, Real time, Real ke);
    //@}


    //! \name CCAdvection specific boundary conditions
    //! These functions are specific to conduction and are only
    //! implemented for this physics class.
    //@{
    //! Add a temperature boundary condition
    void addTemperatureBC(const BCPackage& bc);

    //! Load a temperature boundary condtion
    CCAdvectionBC* getTemperatureBC(int i) {return tempbc[i];}

    //! How many temperature bc's are loaded
    int numTemperatureBC() {return tempbc.size();}
    //@}

    void setGhostCentroids();

    void setICOpt(int opt) {m_icopt = opt;}

  protected:

    //! \name Data registered for scalar C-Law advection
    //! CCclaw takes care of all the other parameters.
    //@{
    DataIndex TEMPERATURE;
    DataIndex TEMP1;
    DataIndex TN;
    DataIndex TEXACT;    //!< Holds exact solution for convergence studies
    DataIndex TDIFF;     //!< TEMPERATURE - TEXACT
    DataIndex VELX;      //!< Y-velocity component
    DataIndex VELY;      //!< X-velocity component
    DataIndex VELZ;      //!< X-velocity component
    DataIndex PHI ;      //!< gradient limiter
    DataIndex GHOST_TEMP;
    DataIndex DTDX;      //!< X-temperature gradient
    DataIndex DTDY;      //!< Y-temperature gradient
    DataIndex DTDZ;      //!< Y-temperature gradient
    DataIndex UPDATE_COUNT;
    DataIndex GHOST_XC;
    DataIndex SEND_BUF;
    DataIndex RECV_BUF;
    //@}

    //! Blocking for limited gradient
    int m_Nepe;

    //! Global element id
    int geid[BLKSIZE];

    //! Cell data
    Real  tc[BLKSIZE], xec[BLKSIZE], yec[BLKSIZE], zec[BLKSIZE];
    Real  ev[BLKSIZE], evi[BLKSIZE];

    //! Neighbor temperatures and distances to adjacent cells
    Real ts[BLKSIZE][MAXNB];
    Real dxe[BLKSIZE][MAXNB], dye[BLKSIZE][MAXNB], dze[BLKSIZE][MAXNB];

    //! Face centroids (flux quadrature points)
    Real xs[BLKSIZE][MAXNB], ys[BLKSIZE][MAXNB], zs[BLKSIZE][MAXNB];

    //! Unit normals
    Real ex[BLKSIZE][MAXNB], ey[BLKSIZE][MAXNB], ez[BLKSIZE][MAXNB];

    //! Distances from element centroid to faces
    Real dx[BLKSIZE][MAXNB], dy[BLKSIZE][MAXNB], dz[BLKSIZE][MAXNB];

    //! Face area/length
    Real gam[BLKSIZE][MAXNB];

    //! Min/Max temperature & gradients
    Real tmax[BLKSIZE], tmin[BLKSIZE];
    Real dudx[BLKSIZE], dudy[BLKSIZE], dudz[BLKSIZE];

    //! \name STL vectors of BC objects
    //@{
    vector<CCAdvectionBC*> tempbc;
    //@}

    int m_icopt;

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCAdvection(const CCAdvection&);
    CCAdvection& operator=(const CCAdvection&);
    //@}

    //! \name Error metrics
    //@{
    Real L1err;
    Real L2err;
    Real Linferr;
    Real Amperr;
    //@}
};

}
#endif // CCAdvection_h
