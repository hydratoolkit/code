//******************************************************************************
//! \file    src/FVM/CCDummy/CCDummy.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:27 2011
//! \brief   Dummy physics for testing
//******************************************************************************

#ifndef CCDummy_h
#define CCDummy_h

#include <Control.h>
#include <fileIO.h>
#include <UnsMesh.h>
#include <CCclaw.h>


namespace Hydra {

//! Dummy physics class
class CCDummy : public CCclaw {

  public:

    //! \name Constructor/Destructor
    //@{
             CCDummy();
    virtual ~CCDummy() {}
    //@}

    //**************************************************************************
    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData();
    //@}

    //! \name Virtual Physics Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    virtual void setup();

    virtual void initialize();

    virtual void solve();

    virtual void finalize() {}

    virtual void setupMergedSetBCs() {}
    //@}

    //! \name Virtual Functions for I/O
    //! These functions are implemented for each specific physics
    //! to permit custom options to be coded for reading and writing
    //! restart files, time-history data, bc's, etc.
    //@{
    virtual void writeSolving();

    virtual void echoOptions(ostream& ofs);
    //@}
    //**************************************************************************

    //! \name I/O functions specific to this solver
    //@{
    //! Write the header for the global time-history data file
    virtual void globHeader(ofstream& globf);

    //! Write the global time history data at each step
    virtual void writeGlobal(ofstream& globf, Real time, Real ke);
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCDummy(const CCDummy&);
    CCDummy& operator=(const CCDummy&);
    //@}

};

}
#endif // CCDummy_h
