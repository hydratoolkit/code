//******************************************************************************
//! \file    src/FVM/CCLset/CCLset.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:53 2011
//! \brief   Cell-centered FVM level set class
//******************************************************************************

#ifndef CCLset_h
#define CCLset_h

#include <Control.h>
#include <fileIO.h>
#include <UnsMesh.h>
#include <CCclaw.h>
#include <CCAdvectionBCs.h>
#include <CCError.h>

namespace Hydra {

#define GEN_TOL 1.0e-6
#define EXT_TOL 1.0e-8

//! Element Category
enum Elcat {FLUID,             //!< fluid element
            FLUIDCUT,          //!< fluid element cut by interface
            GHOST,             //!< ghost element
            GHOSTCUT,          //!< ghost element cut by interface
            DEAD,              //!< dead element
            NUMELCATS          //!< # of element categories
};

//! Finite Volume Level Set class
class CCLset : public CCclaw {

  public:

    //! \name Constructor/Destructor
    //@{
             CCLset();
    virtual ~CCLset() {}
    //@}

    //**************************************************************************
    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData();
    //@}

    //! \name Virtual Physics Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    virtual void setup();

    virtual void initialize();

    virtual void solve();

    virtual void finalize() {}

    virtual void setupMergedSetBCs() {}
    //@}

    //! \name Virtual Functions for I/O
    //! These functions are implemented for each specific physics
    //! to permit custom options to be coded for reading and writing
    //! restart files, time-history data, bc's, etc.
    //@{
    virtual void writeSolving();

    virtual void echoOptions(ostream& ofs);
    //@}

    //! Echo boundary conditions
    void echoBCs(ostream& ofs);

    //! \name Functions for cache-blocking
    //! These functions are used to setup the cache-blocks for the
    //! gradient computation.
    //@{


    //@}

    void setupGhosts(Real* temp, Real* gt);

    //! \name I/O functions specific to this solver
    //@{
    //! Write the header for the global time-history data file
    virtual void globHeader(ofstream& globf);

    //! Write the global time history data at each step
    virtual void writeGlobal(ofstream& globf);
    //@}

    void transferGrid();

    void transferGrid2();

    void transferGridNodeMethod();

    void transferGridQuadrature1();

    void transferGridQuadrature4();

    void transferGridQuadratureSS();

    void transferGrid2NodeMethod();

    void transferGrid2Quadrature1();

    void transferGrid2Quadrature4();

    void transferGrid2QuadratureSS();

    void calcCCphi();

    void calcCCgradPhi();

    void calcDistFuncExact();

    void calcNormals();

    void calcCurvature();

    void calcNormalsExact();

    void calcIntNormCurv();

    void formMass(Real* ml);

    void formMass4(Real* ml);

    void formMassSS(Real* ml);

    void formRHSnormals2D(Real* nx, Real* ny);

    void formRHSnormals3D(Real* nx, Real* ny, Real* nz);

    void formRHSphi1(Real* phi);

    void setElemCategory();

    void extInterface();

    void extEdgeInterface2D(int e1, int e2);

    void invMapQuad4(Real x1, Real x2, Real x3, Real x4,
                     Real y1, Real y2, Real y3, Real y4,
                     Real x,  Real y,  Real &xi, Real &eta);

    void calcErrors();

    void calcErrors2();

    void calcNormErrors();

    void calcCurvErrors();

  protected:

    //! \name Data registered for level sets
    //@{
    DataIndex PHI ;        //!< level set field at the nodes
    DataIndex PHICC;       //!< level set field at the cell centers
    DataIndex GRADPHICCX;  //!< gradient of level set at cell centers
    DataIndex GRADPHICCY;  //!< gradient of level set at cell centers
    DataIndex GRADPHICCZ;  //!< gradient of level set at cell centers
    DataIndex PHIQUAD;     //!< level set field at the center cell quad point
    DataIndex DELTA_E;     //!< exact distance function
    DataIndex NX_E;        //!< exact x component of the normal at the nodes
    DataIndex NY_E;        //!< exact y component of the normal at the nodes
    DataIndex NZ_E;        //!< exact z component of the normal at the nodes
    DataIndex NX;          //!< x component of the normal at the nodes
    DataIndex NY;          //!< y component of the normal at the nodes
    DataIndex NZ;          //!< z component of the normal at the nodes
    DataIndex LUMPED_MASS; //!< lumped mass matrix for normal calculation
    DataIndex ELEMCAT;     //!< element category - for visualization
    DataIndex ECAT;        //!< element category -- the real one
    DataIndex NX_INT;      //!< x normal at the extracted interface location
    DataIndex NY_INT;      //!< y normal at the extracted interface location
    DataIndex NZ_INT;      //!< z normal at the extracted interface location
    DataIndex KAPPA;       //!< curvature at the extracted interface location
    DataIndex NX_INT_E;    //!< exact x normal at the exact interface location
    DataIndex NY_INT_E;    //!< exact y normal at the exact interface location
    DataIndex NZ_INT_E;    //!< exact z normal at the exact interface location
    DataIndex KAPPA_E;     //!< exact curvature at the extracted int location
    DataIndex CURVATURE;   //!< curvature at the nodes
    //@}

    //! Global element id
    //int geid[BLKSIZE];

    //! \name STL vectors of BC objects
    //@{
    vector<CCAdvectionBC*> tempbc;
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCLset(const CCLset&);
    CCLset& operator=(const CCLset&);
    //@}

    Real ccx;      // circle (sphere) center x-location
    Real ccy;      // circle (sphere) center y-location
    Real ccz;      // circle (sphere) center z-location
    Real rad;      // circle (sphere) radius

    Real int_loc;  // interface location on a quasi-1D grid

    Real xint;
    Real yint;

    Real L1;
    Real L1_2;
    Real L1_norm;
    Real L1_curv;
    Real L2;
    Real L2_2;
    Real L2_norm;
    Real L2_curv;
    Real Linf;
    Real Linf_2;
    Real Linf_norm;
    Real Linf_curv;

    int Nint;      // number of cut interfaces

};

}
#endif // CCLset_h
