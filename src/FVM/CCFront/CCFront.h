//******************************************************************************
//! \file    src/FVM/CCFront/CCFront.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:01 2011
//! \brief   Frontier front-tracking
//******************************************************************************
#ifndef CCFront_h
#define CCFront_h

#include <Control.h>
#include <fileIO.h>
#include <UnsMesh.h>
#include <CCclaw.h>
#include <CCError.h>
#include <InterfaceTracker.h>

namespace Hydra {

enum GeomInterfaceType {INTFC_NONE, INTFC_LINE, INTFC_CIRCLE, INTFC_ELLIPSE,
                        INTFC_3D_SPHERE};

typedef struct _CCFrontOptions {
  GeomInterfaceType initial_interface;
  Real x0, y0, z0;
  Real x1, y1, z1;
  Real radius;
  Real x_radius, y_radius;
  int num_pts;
  char output_dir[64];    // TODO: Is there a Qacina default for these?
  char output_prefix[64];
} CCFrontOptions;

//! Finite Volume Front Tracking class
class CCFront : public CCclaw {

  public:

    //! \name Constructor/Destructor
    //@{
             CCFront();
    virtual ~CCFront() {}
    //@}

    //**************************************************************************
    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData();
    //@}

    //! \name Virtual Physics Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    virtual void setup();

    virtual void initialize();

    virtual void solve();

    virtual void finalize() {}

    virtual void setupMergedSetBCs() {}
    //@}

    //! \name Virtual Functions for I/O
    //! These functions are implemented for each specific physics
    //! to permit custom options to be coded for reading and writing
    //! restart files, time-history data, bc's, etc.
    //@{
    virtual void writeRestart(ofstream& dumpf);

    virtual void readRestart();

    virtual void writeSolving();

    virtual void echoOptions(ostream& ofs);

    //@}
    //**************************************************************************

    //! Echo boundary conditions
    void echoBCs(ostream& ofs);

    //! \name Functions for cache-blocking
    //! These functions are used to setup the cache-blocks for the
    //! gradient computation.
    //@{
    //@}

    //! \name I/O functions specific to this solver
    //@{
    //! Write the header for the global time-history data file
    virtual void globHeader(ofstream& globf);

    //! Write the global time history data at each step
    virtual void writeGlobal(ofstream& globf, Real time, Real ke);
    //@}

    //! \name CCEuler specific interface tracker functions
    //! These functions are specific to the Euler eqs. and are only
    //! implemented for this physics class.
    //@{
    //! Add an interface tracker
    void addTracker() {intfc_tracker.push_back(new InterfaceTracker);}

    //! Load an interface tracker object
    InterfaceTracker* getTracker(int i) {return intfc_tracker[i];}

    //! How many interface tracker objects are loaded
    int numTracker() {return intfc_tracker.size();}

    //! Echo the interface tracker options
    void echoTracker(ostream& ofs);
    //@}


    //! \name Input file options
    //@{
    //! Set the startup options
    void setOptions(CCFrontOptions *opts);

    //! Get the startup options
    void getOptions(CCFrontOptions *opts);
    //@}

    //! Get the velocity of the fluid at a point
    void getVelocity(Real x, Real y, Real z, Real *u, Real *v, Real *w);

  protected:

    //! \name Data registered for front tracking
    //@{
    DataIndex PHI ;        //!< level set field at the nodes
    //@}

    //! \name STL vectors of InterfaceTracker Objects
    //@{
    vector<InterfaceTracker*> intfc_tracker;
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCFront(const CCFront&);
    CCFront& operator=(const CCFront&);
    //@}

    CCFrontOptions options;

};

}
#endif // CCFront_h
