//******************************************************************************
//! \file    src/FVM/CCFront/CCFront.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:01 2011
//! \brief   Frontier front-tracking
//******************************************************************************
#include <cmath>
#include <iomanip>
#include <iostream>

using namespace std;

#include <CCFront.h>
#include <Control.h>
#include <OutputControl.h>
#include <MPIWrapper.h>
#include <UnsPhysicsFieldDelegates.h>

using namespace Hydra;

void
CCFront::initialize()
/*******************************************************************************
Routine: initialize - setup initial conditions
Author : Mark A. Christon
*******************************************************************************/
{
}

#ifdef FRONTIER
void
CCFront::solve()
/*******************************************************************************
Routine: solve - driver for front tracking
Author : Mark A. Christon
*******************************************************************************/
{
  Category* ocat = control->getCategory(OUTPUT_CATEGORY);

  int Nsteps = control->getOptionVal(Control::NSTEPS);

  int plti   = ocat->getOptionVal(OutputControl::PLTI);
  int plnum  = ocat->getOptionVal(OutputControl::PLNUM);
  int ttyi   = ocat->getOptionVal(OutputControl::TTYI);

  Real t = control->getParam(TOTALTIME);

  //io->writePlot(plnum,0,t,control, mesh);

  switch (options.initial_interface) {
  case INTFC_NONE:
    cerr << "Error: No initial interface specified" << endl;
    break;
  case INTFC_LINE:
    intfc_tracker[0]->createLine(options.x0, options.y0,
                                 options.x1, options.y1, options.num_pts);
    break;
  case INTFC_CIRCLE:
    intfc_tracker[0]->createCircle(options.x0, options.y0,
                                   options.radius, options.num_pts);
    break;
  case INTFC_ELLIPSE:
    cout << "Ellipse initial conditions currently doing very bad things.  Email Mark" << endl;
    break;
  default:
    cerr << "Error: Unknown initial interface type" << endl;
  }
  intfc_tracker[0]->setBoundary();

  // Output a GeomView file to show the interace position

  intfc_tracker[0]->createGeomViewFile(options.output_dir, options.output_prefix);

  // Advance the front
  // TODO: set dt, nsteps in cntl file
  int nsteps =  25;
  Real dt, dt_frac;
  dt = 0.05;
  for (int i=0; i < nsteps; i++) {
    intfc_tracker[0]->advanceFront(dt, &dt_frac);
  }

}

CCFront::CCFront()
/*******************************************************************************
Routine: CCFront - default constructor
Author : Mark A. Christon
*******************************************************************************/
{
  PHI = UNREGISTERED;
}

void
CCFront::getVelocity(Real x, Real y, Real z, Real *u, Real *v, Real *w)
/*******************************************************************************
Routine: getVelocity - get the fluid velocity at a point in the domain
Author : Mark A. Christon
*******************************************************************************/
{
  // TODO: check the setup of the class to get something meaningful
  //  for now, use a simple 2D shear velocity
  /*
  *u = y;
  *v = 0.0;
  *w = 0.0;
  */
  *u = -y;
  *v = 0.0;
/*
  *u = 0.1;
  *v = 0.0;
*/
}

void
CCFront::getOptions(CCFrontOptions *opts)
/*******************************************************************************
Routine: getOptions - get interface tracking options
Author : Mark A. Christon
*******************************************************************************/
{
  memcpy(opts, &options, sizeof(CCFrontOptions));
}

void
CCFront::setOptions(CCFrontOptions *opts)
/*******************************************************************************
Routine: setOptions - set interface tracking options
Author : Mark A. Christon
*******************************************************************************/
{
  memcpy(&options, opts, sizeof(CCFrontOptions));
}

void
CCFront::registerData()
/*******************************************************************************
Routine: registerData() - register conduction specific data
Author : Mark A. Christon
*******************************************************************************/
{

  int Nnp     = mesh->getNnp();

  PHI         = mesh->registerVariable(Nnp,
                                       sizeof(Real),
                                       "PHI",
                                       NODE_CTR,
                                       SCALAR_VAR,
                                       LINEAR_ARRAY,
                                       true,
                                       true);

  io->registerDelegate("PHI",
                       ELEMENT_CTR,
                       SCALAR_VAR,
                       FIELD_DELEGATE,
                       this,
                       UnsPhysicsScalarField);

  // Register the data for the base class too
  CCclaw::registerData();

}

void
CCFront::setup()
/*******************************************************************************
Routine: setup - setup for advection-diffusion
Author : Mark A. Christon
*******************************************************************************/
{
  // Setup the base-class first -- also calls registerData()
  CCclaw::setup();

  // Read restart file
  try {
    readRestart();
  }
  catch (Exception ec) {
    io->writeError(ec.getError().c_str());
    g_comm->finalMP();
    exit(0);
  }

  // Initialize the interface tracker (s)
  for (int i=0;i<intfc_tracker.size();i++) {
    intfc_tracker[i]->setup(mesh);
  }

  // Open the glob file ONLY for flow related problems
  ofstream& globf = io->openGlob(control->getOption(DORESTART));
  globHeader(globf);

  // Align output delegates with field, history, surface output requests
  io->setupOutputDelegates(control, mesh, PLOT);
}

void
CCFront::writeRestart(ofstream& dumpf)
/*******************************************************************************
Routine: writeRestart - write out all data to the dump file
Author : Mark A. Christon
*******************************************************************************/
{

#ifdef NOT_YET
  Real* temp = (Real*) mesh->getVariable(TEMPERATURE);
  Real* velx = (Real*) mesh->getVariable(VELX);
  Real* vely = (Real*) mesh->getVariable(VELY);
  dumpf.write(reinterpret_cast<char*>(&Nel), sizeof(int)     );
  dumpf.write(reinterpret_cast<char*>(temp), Nel*sizeof(Real));
  dumpf.write(reinterpret_cast<char*>(velx), Nel*sizeof(Real));
  dumpf.write(reinterpret_cast<char*>(vely), Nel*sizeof(Real));
  if (Ndim == THREED) {
    Real* velz = (Real*) mesh->getVariable(VELZ);
    dumpf.write(reinterpret_cast<char*>(velz), Nel*sizeof(Real));
  }
#endif

}

void
CCFront::readRestart()
/*******************************************************************************
Routine: readRestart - read in all data from the dump file
Author : Mark A. Christon
*******************************************************************************/
{

#ifdef NOT_YET
  int r_nel;

  Real* temp = (Real*) mesh->getVariable(TEMPERATURE);
  Real* velx = (Real*) mesh->getVariable(VELX);
  Real* vely = (Real*) mesh->getVariable(VELY);

  restf.read(reinterpret_cast<char*>(&r_nel), sizeof(int));
  if (r_nel != Nel) {
    p0cout << "Restart error: read Nel = " << r_nel
           << ", Nel = " << Nel << endl;
    exit(0);
  }
  restf.read(reinterpret_cast<char*>(temp), Nel*sizeof(Real));
  restf.read(reinterpret_cast<char*>(velx), Nel*sizeof(Real));
  restf.read(reinterpret_cast<char*>(vely), Nel*sizeof(Real));
  if (Ndim == THREED) {
    Real* velz = (Real*) mesh->getVariable(VELZ);
    restf.read(reinterpret_cast<char*>(velz), Nel*sizeof(Real));
  }
#endif

}

void
CCFront::writeSolving()
/*******************************************************************************
Routine: writeSolving() - write solution type
Author : Mark A. Christon
*******************************************************************************/
{
  if (pid == 0) {
    cout << endl
         << "\tSolving front propagation problem..."
         << endl;

    ofstream& ofs = io->getOut();
    ofs  << endl
         << "\tSolving front propagation problem..."
         << endl;
  }
}

void
CCFront::echoBCs(ostream& ofs)
/*******************************************************************************
Routine: echoBCs - echo all BC's
Author : Mark A. Christon
*******************************************************************************/
{
#ifdef NOT_YET
  char cbuf[MAXCHR];

  // Echo the Dirichlet temperature BC's
  int Ntbc = tempbc.size();
  if (Ntbc > 0 && pid == 0) {
    ofs << endl;
    ofs << "\tT E M P E R A T U R E   B O U N D A R Y  C O N D I T I O N S"
        << endl
        << "\t============================================================"
        << endl
        << endl
        << "\tNumber of temperature bcs ............................... "
        << Ntbc << endl
        << endl;

    ofs << "\tSide Set   Side Set ID  Table  Amplitude"
        << endl
        << "\t========   ===========  =====  ========="
        << endl;
    for (int i=0; i<Ntbc; i++) {
      sprintf(cbuf,"\t%8d    %10d  %10d   %10.4e",
              tempbc[i]->getSet(), tempbc[i]->getID(),
              tempbc[i]->getTableID(), tempbc[i]->getTbc());
      ofs << cbuf << endl;
    }
    ofs << endl;
  }
#endif
}

void
CCFront::echoOptions(ostream& ofs)
/*******************************************************************************
Routine: echoOptions - echo all solver options
Author : Mark A. Christon
*******************************************************************************/
{
  echoBCs(ofs);
  echoTracker(ofs);
}

void
CCFront::echoTracker(ostream& ofs)
/*******************************************************************************
Routine: echoTracker - echo interface tracker options
Author : Mark A. Christon
*******************************************************************************/
{
  //char cbuf[MAXCHR];

  int Ntrack = intfc_tracker.size();
  if (Ntrack > 0 && pid == 0 ) {
    ofs << endl;
    ofs << "\tI N T E R F A C E  T R A C K E R  O P T I O N S"
        << endl
        << "\t==============================================="
        << endl
        << endl
        << "\tNumber of interfaces to be tracked   .................... "
        << Ntrack << endl
        << endl;
    ofs << endl;
  }
}

void
CCFront::globHeader(ofstream& globf)
/*******************************************************************************
Routine: globHeader - write the global file header
Author : Mark A. Christon
*******************************************************************************/
{
  if (pid == 0) {
    globf << "#" << endl;
    globf << "# HYDRA: global time history data" << endl;
    globf << "#      Time           TKE              L1              "
          << "L2           Linf         Amp"
          << endl;
    globf << "#" << endl;
  }
}

void
CCFront::writeGlobal(ofstream& globf, Real time, Real ke)
/*******************************************************************************
Routine: writeGlobal - output global data
Author : Mark A. Christon
*******************************************************************************/
{

#ifdef NOT_YET
  if (pid == 0) {
    globf << setiosflags( ios::scientific | ios::showpoint) << setprecision(8);
    globf << time  << "  " << ke    << "  "
          << L1err << "  " << L2err << "  " << Linferr << " " << Amperr << endl;
  }
#endif

}


#endif // FRONTIER
