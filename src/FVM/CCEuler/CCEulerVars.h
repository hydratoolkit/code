//******************************************************************************
//! \file    src/FVM/CCEuler/CCEulerVars.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:40:02 2011
//! \brief   Conserved and primitive variable structures
//******************************************************************************

#ifndef FVM_CC_EULER_VARS_H
#define FVM_CC_EULER_VARS_H

namespace Hydra {

//! Conserved & Primitive variables
struct U2D {
  Real rho;
  Real rhou;
  Real rhov;
  Real E;
};

struct U3D {
  Real rho;
  Real rhou;
  Real rhov;
  Real rhow;
  Real E;
};

struct W2D {
  Real rho;
  Real u;
  Real v;
  Real e;
  Real flip; // Used to control the sign of un for no-penetration bc's
};

struct W3D {
  Real rho;
  Real u;
  Real v;
  Real w;
  Real e;
  Real flip; // Used to control the sign of un for no-penetration bc's
};

}

#endif // FVM_CC_EULER_VARS_H
