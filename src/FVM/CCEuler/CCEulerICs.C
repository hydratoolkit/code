//******************************************************************************
//! \file    src/FVM/CCEuler/CCEulerICs.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:40:02 2011
//! \brief   User-defined IC's for cell-centered Euler solver
//******************************************************************************
#include <cmath>

using namespace std;

#include <CCEulerICs.h>
#include <CCEulerVars.h>

using namespace Hydra;

void
CCEulerRTIC::setICs(UnsMesh* mesh, vector<DataIndex>& vars) {
/*******************************************************************************
Routine: setICs - setup IC's for the Rayleigh-Taylor problems
Author : Mark A. Christon

IC's for Ideal Gas only
*******************************************************************************/

  // DataIndex map:
  // var entry  | DataIndex =| Variable
  // ===========|============|==================================================
  // 0          | U1         | vector of conserved variables

  // const Real EPS   = 0.1;  // perturbation size
  const Real P0    = 1.0;
  const Real rho_h = 2.0;
  const Real rho_l = 1.0;
  const Real gamma = 1.4;
  const Real g     = 0.1;

  // Grid: [-1,1]x[-2,2]
  if (mesh->getNdim() == TWOD) {
    CVector centroid = mesh->getCentCoord();
    Real* xc = centroid.X;
    Real* yc = centroid.Y;
    U2D* U = mesh->getVariable<U2D>(vars[0]);
    int Nec = mesh->numElementClass();
    for (int n = 0; n < Nec; n++) {
      Element* ec = mesh->getElementClass(n);
      int  nel = ec->getNel();
      int* gids = ec->getGlobalIds();
      for (int i=0; i<nel; i++) {
        int gid = gids[i];

        // Perturb the interface slightly
        //Real pert = - EPS*cos(xc[gid]*3.0*PI/2.0);
        //Real pert = - EPS*cos(xc[gid]*5.0*PI/2.0);
        //Real pert = EPS*cos(xc[gid]*PI);
        Real pert = 0.01*cos(6.0*PI*xc[gid]);  // From Liska & Wendroff
        // Real dpert_dx = -6.0*PI*0.01*sin(6.0*PI*xc[gid]);

        // Fluid initially at rest
        Real uic = 0.0;
        Real vic = 0.0;

        // Density based on geometric perturbation
        // Use the pressure gradient to balance the gravity body force
        Real p;
        Real rhoic;
        if (yc[gid] > (0.0 + pert)) {
          rhoic = rho_h;
          p = P0 + rho_h*g*(1.0 - yc[gid]);
        } else {
          rhoic = rho_l;
          p = (P0 + rho_h*g*(1.0-pert)) + rho_l*g*(0.0+pert-yc[gid]);
        }

        Real eic = p/((gamma-1.0));
        // e = p/((gamma-1.0)*rho);
        // e = rho*(0.0 + 0.5 * (u*u + v*v));

        // Alternative perturbation
        // 1. Loop over nodes and perturb interface with y-decay to get
        //    smooth variation from interface
        // 2. re-compute the element volumes
        // 3. re-compute the centroid coordinates
        // 4. setup the pressure -- assume a 2-block model for density init.

        U[gid].rho  = rhoic;
        U[gid].rhou = rhoic*uic;
        U[gid].rhov = rhoic*vic;
        U[gid].E    = eic;
      }
    }

  } else if (mesh->getNdim() == THREED) {
    CVector centroid = mesh->getCentCoord();
    Real* xc = centroid.X;
    Real* yc = centroid.Y;
    Real* zc = centroid.Z;
    U3D* U = mesh->getVariable<U3D>(vars[0]);
    int Nec = mesh->numElementClass();
    for (int n = 0; n < Nec; n++) {
      Element* ec = mesh->getElementClass(n);
      int  nel  = ec->getNel();
      int* gids = ec->getGlobalIds();
      for (int i=0; i<nel; i++) {
        int gid = gids[i];

        // Perturb the interface slightly
        //Real pert = - EPS*cos(x*3.0*PI/2.0);
        //Real pert = - EPS*cos(x*5.0*PI/2.0);
        //Real pert = EPS*cos(x*PI);
        Real pert = 0.02*cos(3.0*PI*xc[gid])*cos(3.0*PI*yc[gid]);

        // Fluid initially at rest
        Real uic = 0.0;
        Real vic = 0.0;
        Real wic = 0.0;

        // Density based on geometric perturbation
        // Use the pressure gradient to balance the gravity body force
        Real p;
        Real rhoic;

        // Use the pressure gradient to balance the gravity body force
        if (zc[gid] > (0.0 + pert)) {
          rhoic = rho_h;
          p = P0 + rho_h*g*(0.5 - zc[gid]);
        } else {
          rhoic = rho_l;
          p = (P0 + rho_h*g*(0.5-pert)) + rho_l*g*(0.0+pert-zc[gid]);
        }

        Real eic = p/((gamma-1.0));

        U[gid].rho  = rhoic;
        U[gid].rhou = rhoic*uic;
        U[gid].rhov = rhoic*vic;
        U[gid].rhow = rhoic*wic;
        U[gid].E    = eic;
      }
    }
  }

}
