//******************************************************************************
//! \file    src/FVM/CCEuler/InterfaceLsetDynamic.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:40:02 2011
//! \brief   Level sets for dynamic interfaces in FVM Euler solver
//******************************************************************************

#ifndef InterfaceLsetDynamic_h
#define InterfaceLsetDynamic_h

#include <Interface.h>
#include <UnsMesh.h>
#include <DualEdge.h>
#include <MPIWrapper.h>
#include <CCEulerVars.h>
#include <InterfaceLset.h>

namespace Hydra {

//! Options used for dynamic level-set in Euler solver
typedef struct _LSetOptionsDynamic {
  GeomInterfaceType interface_geom;
  Real x0, y0, z0;
  Real radius;
  int quad_rule;
  Real xlength, ylength, zlength;
  Real u, v, w;
} LSetOptionsDynamic;


//! This class implements interface capturing using level sets
class InterfaceLSetDynamic : public Interface {

  public:

    //! \name Constructor/Destructor
    //@{
             InterfaceLSetDynamic() {}
    virtual ~InterfaceLSetDynamic() {}
    //@}

    //! \name InterfaceLSetDynamic access functions
    //! The InterfaceLSetDynamic class is used to setup interface capturing
    //! and BC implementation for fluid-solid interaction problems.
    //@{
    void setup(UnsMesh *mesh){}; // called from physics setup function
    //@}

    void setupLSetDynamic(UnsMesh* mesh);

    void setupLSet2D(UnsMesh* mesh, Face* face, Real* phi, Real* phicc,
                     Real* phiq, Real* ml, Real* elemcat, Elcat* ecat,
                     DataIndex XC, DataIndex YC, DataIndex ELSTATUS);

    void setupLSet3D(UnsMesh* mesh, Face* face, Real* phi, Real* phicc,
                     Real* phiq, Real* ml, Real* elemcat, Elcat* ecat,
                     DataIndex XC, DataIndex YC, DataIndex ZC,
                     DataIndex ELSTATUS);
                    // set up level sets

    void setElemStatus(UnsMesh* mesh, Face* face, Elcat* ecat, DataIndex ELSTATUS,
                       DataIndex XC, DataIndex YC);

    void setElemStatus(UnsMesh* mesh, Face* face, Elcat* ecat, DataIndex ELSTATUS,
                       DataIndex XC, DataIndex YC, DataIndex ZC);

    void transferGrid(UnsMesh* mesh, Real* phi, Real* phiq, Real* ml);

    void transferGridNodeMethod(){}

    void transferGridQuadrature1(){}

    void transferGridQuadrature4(){}

    void transferGridQuadratureSS(UnsMesh* mesh, Real* phi, Real* phiq, Real* ml);

    void formMassSS(UnsMesh* mesh, Real* ml);

    void calcPhiCC(UnsMesh* mesh, Real* phi, Real* phicc);

    void setElemCategory(UnsMesh* mesh, Face* face, Real* phi, Real* phicc,
                         Real* elemcat, Elcat* ecat, DataIndex XC,
                         DataIndex YC);

    void setElemCategory(UnsMesh* mesh, Face* face, Real* phi, Real* phicc,
                         Real* elemcat, Elcat* ecat, DataIndex XC,
                         DataIndex YC, DataIndex ZC);

    void identifyCutEdges(UnsMesh* mesh, Face* face, Elcat* ecat);

    void extInterfaceCircle(UnsMesh* mesh, Face* face, Elcat* ecat);

    void extInterfaceSphere(UnsMesh* mesh, Face* face, Elcat* ecat);

    void extInterfaceRectangle(UnsMesh* mesh, Face* face, Elcat* ecat,
                               DataIndex XC, DataIndex YC);

    void extInterfaceBox(UnsMesh* mesh, Face* face, Elcat* ecat,
                         DataIndex XC, DataIndex YC, DataIndex ZC);


    void findGhostSource(UnsMesh* mesh, Face* face, Elcat* ecat,
                         DataIndex XC, DataIndex YC);

    void findGhostSource(UnsMesh* mesh, Face* face, Elcat* ecat,
                         DataIndex XC, DataIndex YC, DataIndex ZC);

    void findQuad(UnsMesh* mesh, Face* face,
                  Real xp, Real yp, int& index,
                  DataIndex XC, DataIndex YC);

    void findHex(UnsMesh* mesh, Face* face,
                 Real xp, Real yp, Real zp, int& index,
                 DataIndex XC, DataIndex YC, DataIndex ZC);

    void setupInteriorGhosts(UnsMesh* mesh, Face* face, DataIndex ECAT,
                             DataIndex U1, DataIndex GHOST_CUT_DATA);

    void setupInteriorBCs(UnsMesh* mesh, Face* face, DataIndex ECAT,
                          DataIndex U1, DataIndex U2, DataIndex GHOST_CUT_DATA,
                          DataIndex XC, DataIndex YC, DataIndex DWDX,
                          DataIndex DWDY);

    void setupInteriorBCs(UnsMesh* mesh, Face* face, DataIndex ECAT,
                          DataIndex U1, DataIndex U2, DataIndex GHOST_CUT_DATA,
                          DataIndex XC, DataIndex YC, DataIndex ZC,
                          DataIndex DWDX, DataIndex DWDY, DataIndex DWDZ);

    bool invMapQuad4(Real x1, Real x2, Real x3, Real x4,
                     Real y1, Real y2, Real y3, Real y4,
                     Real x,  Real y,  Real &xi, Real &eta);

    bool invMapHex8(Real x1, Real x2, Real x3, Real x4,
                    Real x5, Real x6, Real x7, Real x8,
                    Real y1, Real y2, Real y3, Real y4,
                    Real y5, Real y6, Real y7, Real y8,
                    Real z1, Real z2, Real z3, Real z4,
                    Real z5, Real z6, Real z7, Real z8,
                    Real x, Real y, Real z,
                    Real &xi, Real &eta, Real &zeta);

    void updateLSet(UnsMesh* mesh, Real dt);


    // Quadrature stuff - move eventually
    void setupSSquad4();

    void setupQuad4ss4(int Nnpe);

    void setupQuad4ss16(int Nnpe);

    void setupQuad4ss49(int Nnpe);

    void setupQuad4ss100(int Nnpe);

    void setupSShex8();

    void setupHex8ss8(int Nnpe);

    void setupHex8ss64(int Nnpe);

    void setupHex8ss343(int Nnpe);

    void setupHex8ss1000(int Nnpe);

    Real sf4(Real xi, Real eta, int node);

    Real sf4xi(Real xi, Real eta, int node);

    Real sf4eta(Real xi, Real eta, int node);

    Real sf8(Real xi, Real eta, Real zeta, int node);

    Real sf8xi(Real xi, Real eta, Real zeta, int node);

    Real sf8eta(Real xi, Real eta, Real zeta, int node);

    Real sf8zeta(Real xi, Real eta, Real zeta, int node);
    // End Quadrature stuff


    //! \name Input file options
    //@{
    //! Set the startup options
    void setOptions(LSetOptionsDynamic *opts);

    //! Get the startup options
    void getOptions(LSetOptionsDynamic *opts);
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    InterfaceLSetDynamic(const InterfaceLSetDynamic&);
    InterfaceLSetDynamic& operator=(const InterfaceLSetDynamic&);
    //@}

    DataIndex BUCKETS;
    DataIndex ELEMBAND;
    DataIndex NODEMARK1;
    DataIndex NODEMARK2;
    DataIndex DIAMETERS;

    //! Quadrature stuff - move eventually
    int Nqpt_ls;
    Real ls_sf[8][1000][4];
    Real ls_qpt[1000][3];        // need to fix these bounds
    Real ls_Qwt[1000];

    int Nel_in_band;

    Real x_ls[3];

    //! Shape functions for inverse Hex8 mapping
    Real shpf[8][4];    //8: nodes per hex   4: shape function & 3 derivatives

    //! Level Set Interface Options
    LSetOptionsDynamic options;

    int Nint;           //!< number of cut edges
    int Nbuckets;       //!< number of buckets for the searching
    int Ndivisions;     //!< number of divisions in each direction to
                        //!< create buckets
    Real xlength;
    Real ylength;
    Real zlength;
};

}

#endif // InterfaceLsetDynamic_h
