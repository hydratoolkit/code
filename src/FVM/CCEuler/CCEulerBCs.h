//******************************************************************************
//! \file    src/FVM/CCEuler/CCEulerBCs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:40:02 2011
//! \brief   Euler BC's
//******************************************************************************
#ifndef CCEulerBCs_h
#define CCEulerBCs_h

#include <HydraTypes.h>
#include <UnsMesh.h>
#include <SideSetBC.h>
#include <BCPackage.h>

namespace Hydra {

//! Zero velocity boundary condition based on sidesets
class ZeroVelBC : public SideSetBC {

  public:

    enum BCdir {XDIR = 0, YDIR, ZDIR, NDIR};

    //! \name Constructor/Destructor
    //@{
             ZeroVelBC(const BCPackage& bc, BCdir bcdir);
    virtual ~ZeroVelBC() {}
    //@}

    //! Setup Dirichlet BC in ghost list for later use in flux calculation
    void setupGhostBC(UnsMesh* mesh, DataIndex UD, DataIndex GD);

    //! Set the direction for a given sideset bc
    void setDir(BCdir xyz) {m_dir = xyz;}

    //! Get the specified temperature value for a given sideset bc
    BCdir getDir() {return m_dir;}


  private:

    //! Don't permit copy or assignment operators
    //@{
    ZeroVelBC(const ZeroVelBC&);
    ZeroVelBC& operator=(const ZeroVelBC&);
    //@}

    BCdir m_dir;

};

//! Dirichlet boundary condition based on sidesets
class DirichletBC : public SideSetBC {
  public:

    enum BCvar {RHO = 0, VELX, VELY, VELZ, ENERGY};

    //! \name Constructor/Destructor
    //@{
             DirichletBC(const BCPackage& bc, BCvar var);
    virtual ~DirichletBC() {}
    //@}

    //! Setup Dirichlet BC in ghost list for later use in flux calculation
    void setupGhostBC(UnsMesh* mesh, DataIndex GD, Real t);

    //! Set the direction for a given sideset bc
    void setType(BCvar xyz) {m_var = xyz;}

    //! Get the variable type specified for a Dirichlet BC
    BCvar getType() {return m_var;}

  private:

    //! Don't permit copy or assignment operators
    //@{
    DirichletBC(const DirichletBC&);
    DirichletBC& operator=(const DirichletBC&);
    //@}

    BCvar m_var;
};

}
#endif // CCEulerBCs_h
