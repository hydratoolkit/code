//******************************************************************************/
//! \file    src/FVM/CCEuler/CCEulerBodyForce.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:40:02 2011
//! \brief   CCEuler Body Forces
//******************************************************************************

#ifndef CCEulerBodyForce_h
#define CCEulerBodyForce_h

#include <HydraTypes.h>
#include <BodyForce.h>

namespace Hydra {

//! Body-force interface for the cell-centered Euler solver
class CCEulerBodyForce : public BodyForce {

  public:

    //! \name Constructor/Destructor
    //@{
             CCEulerBodyForce() {
               force_amp[0] = force_amp[1] = force_amp[2] = 0.0;
               force_tblid[0] = force_tblid[1] = force_tblid[2] = 0;
             }
    virtual ~CCEulerBodyForce(){}
    //@}

    void setXForceAmp(Real amp) { force_amp[0] = amp; }
    void setYForceAmp(Real amp) { force_amp[1] = amp; }
    void setZForceAmp(Real amp) { force_amp[2] = amp; }

    void setXForceID(int id) { force_tblid[0] = id; }
    void setYForceID(int id) { force_tblid[1] = id; }
    void setZForceID(int id) { force_tblid[2] = id; }

    Real getXForceAmp() { return force_amp[0]; }
    Real getYForceAmp() { return force_amp[1]; }
    Real getZForceAmp() { return force_amp[2]; }

    int getXForceID() { return force_tblid[0]; }
    int getYForceID() { return force_tblid[1]; }
    int getZForceID() { return force_tblid[2]; }

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCEulerBodyForce(const CCEulerBodyForce&);
    CCEulerBodyForce& operator=(const CCEulerBodyForce&);
    //@}

   Real force_amp[3];      //scalar factors to multiply the force by.
   int  force_tblid[3];     //IDs of force tables.

};

}

#endif
