//******************************************************************************
//! \file    src/FVM/CCEuler/CCEulerICs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   User-defined IC's for cell-centered Euler solver
//******************************************************************************

#ifndef CCEulerICs_h
#define CCEulerICs_h

#include <HydraTypes.h>
#include <UnsMesh.h>
#include <UserIC.h>

namespace Hydra {

//! IC's for Rayleigh-Taylor calculations
class CCEulerRTIC : public UserIC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCEulerRTIC() {};
    virtual ~CCEulerRTIC() {};
    //@}

    //! Setup IC's using a mesh and set of DataIndices
    virtual void setICs(UnsMesh*, vector<DataIndex>& vars);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCEulerRTIC(const CCEulerRTIC&);
    CCEulerRTIC& operator=(const CCEulerRTIC&);
    //@}
};

}

#endif // CCEulerICs_h
