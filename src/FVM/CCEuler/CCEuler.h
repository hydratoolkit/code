//******************************************************************************
//! \file    src/FVM/CCEuler/CCEuler.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:40:02 2011
//! \brief   Solve the Euler equations in conservation-law form
//******************************************************************************

#ifndef CCEuler_h
#define CCEuler_h

#include <CCclaw.h>
#include <CCEulerVars.h>
#include <CCEulerBCs.h>
#include <CCEulerBlockICs.h>
#include <CCEulerBodyForce.h>
#include <CCEulerICs.h>
#include <Control.h>
#include <fileIO.h>
#include <UnsMesh.h>
#include <InterfaceTracker.h>
#include <InterfaceReconstruction.h>

namespace Hydra {

  enum TimeIntType {FORWARD_EULER=0,     //!< Forward-Euler integrator
                    RK2,                 //!< 2nd-order Runge Kutta
                    SSPRK2,              //!< 2nd-order SSP Runge-Kutta
                    NUM_INTEGRATOR_TYPES //!< # of element types
  };

//! Cell-centered C-Law class
class CCEuler : public CCclaw {

  public:

    //! \name Constructor/Destructor
    //@{
    CCEuler();
    virtual ~CCEuler();
    //@}

    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData();
    //@}

    //! \name Virtual Physics Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    virtual void setup();

    virtual void initialize();

    virtual void solve();

    virtual void finalize() {}

    virtual void setupMergedSetBCs();
    //@}

    //! \name Virtual Functions for I/O
    //! These functions are implemented for each specific physics
    //! to permit custom options to be coded for reading and writing
    //! restart files, time-history data, bc's, etc.
    //@{
    virtual void writeSolving();

    virtual void echoOptions(ostream& ofs);
    //@}

    //! \name I/O functions specific to this solver
    //@{
    //! Echo out boundary conditions
    void echoBCs(ostream& ofs);

    //! Write the header for the global time-history data file
    virtual void globHeader(ofstream& /*globf*/) {}

    //! Write the global time history data at each step
    virtual void writeGlobal(ofstream& /*globf*/) {}
    //@}

    void gatherGhostPrimitiveVar();

    void gatherNeighbors(int* gids,     // Global element Id's
                         int* eids,     // Dual-edge Id's
                         int   eoffset, // element offset
                         U2D* U,        // Conserved variables
                         W2D* gt,       // Primitive ghost data
                         CVector& gxc,  //ghost centroid
                         Real* xc,      // X-centroid
                         Real* yc,      // Y-centroid
                         Real* v);      // Volume

    void gatherNeighbors(int* gids,     // Global element Id's
                         int* eids,     // Dual-edge Id's
                         int   eoffset, // element offset
                         U3D* U,        // Conserved variables
                         W3D* gt,       // Primitive ghost data
                         CVector& gxc,  //ghost centroid
                         Real* xc,      // X-centroid
                         Real* yc,      // Y-centroid
                         Real* zc,      // Z-centroid
                         Real* v);      // Volume

    void scatterNeighbors(W2D* dWdx, W2D* dWdy);

    void scatterNeighbors(W3D* dWdx, W3D* dWdy, W3D* dWdz);

    void setupGhosts();
    void setupGhosts2D();
    void setupGhosts3D();

    void initGhostGradient();

    void calcLimitedGradient(DataIndex UD, Real* v);
    void calcLSGradient2D();
    void calcLSGradient3D();
    void KRlimitGradient2D();
    void KRlimitGradient3D();

    void calcMinMax2D();
    void calcMinMax3D();

    void updateFields(Real dt, DataIndex U1, DataIndex U2);
    void updateFields2D(Real dt, DataIndex U1, DataIndex U2);
    void updateFields3D(Real dt, DataIndex U1, DataIndex U2);

    void updateState(DataIndex UN, DataIndex U1, DataIndex U2);

    void copyState(DataIndex SRC, DataIndex DST);

    //! Calculate primitive variables from conserved for time-history output
    void calcTHprimVar(U2D U, int lid, Real W[6]);
    void calcTHprimVar(U3D U, int lid, Real W[7]);

    //! Calculate the stable time step
    void calcTimeStep(Real time);
    void calcTimeStep2D(Real time);
    void calcTimeStep3D(Real time);
    Real calcTimeStepHex8(Element& ec, Real dtmin);
    Real calcTimeStepPyr5(Element& ec, Real dtmin);
    Real calcTimeStepTet4(Element& ec, Real dtmin);
    Real calcTimeStepWedge6(Element& ec, Real dtmin);

    //! Set the time integrator type
    void setTimeInt(TimeIntType tint) {time_int = tint;}
    void echoTimeInt(ostream& ofs);

    //**************************************************************************
    //! \name CCEuler specific boundary conditions
    //! These functions are specific to the Euler eqs. and are only
    //! implemented for this physics class.
    //@{
    //! Add a zero velocity boundary condition
    void addZeroVelBC(const BCPackage& bc, ZeroVelBC::BCdir dir);

    //! Load a zero velocity boundary condtion
    ZeroVelBC* getZeroVelBC(int i) {return zero_velbc[i];}

    //! How many temperature bc's are loaded
    int numZeroVelBC() {return zero_velbc.size();}

    //! Add a Dirichlet-type boundary condition
    void addDirichletBC(const BCPackage& bc, DirichletBC::BCvar var);

    //! Load a zero velocity boundary condtion
    DirichletBC* getDirichletBC(int i) {return dirichletbc[i];}

    //! How many temperature bc's are loaded
    int numDirichletBC() {return dirichletbc.size();}
    //@}
    //**************************************************************************

    //**************************************************************************
    //! \name CCEuler specific initial conditions
    //! These functions are specific to the Euler eqs. and are only
    //! implemented for this physics class.
    //@{
    //! Add a block velocity initial condition
    void addBlockICs() {block_ics.push_back(new CCEulerBlockICs);}

    //! Use user specified initial conditions
    void addUserIC() {m_useric.push_back(new CCEulerRTIC); }

    //! Load a block velocity IC object
    CCEulerBlockICs* getBlockICs(int i) {return block_ics[i];}

    //! Check block id's in block velocity list
    void checkBlockICs();

    //! Echo the block velocity IC's
    void echoBlockICs(ostream& ofs);

    //! Echo global IC's
    void echoGlobalICs(ostream& ofs);

    //! How many block velocity IC's are loaded
    int numBlockICs() {return block_ics.size();}

    //! Set global IC's
    void setGlobalICs(Real uic, Real vic, Real wic, Real tic);
    //@}
    //**************************************************************************

    //! \name CCEuler specific body forces
    //! These functions are specific to the Euler eqs. and are only
    //! implemented for this physics class.
    //@{
    //! Add a body force
    int addBodyForce() {
      body_forces.push_back(new CCEulerBodyForce);
      return (body_forces.size()-1);
    }

    //! How many body forces are being applied
    int numBodyForces() { return body_forces.size(); }

    //! Get a specific body force
    CCEulerBodyForce *getBodyForce(int i) { return body_forces[i]; }

    //! Echo the body foces
    void echoBodyForces(ostream &ofs);
    //@}

    //**************************************************************************
    //! \name CCEuler specific interface tracker functions
    //! These functions are specific to the Euler eqs. and are only
    //! implemented for this physics class.
    //@{
    //! Add an interface tracker
    void addTracker() {intfc_tracker.push_back(new InterfaceTracker);}

    //! Load an interface tracker object
    InterfaceTracker* getTracker(int i) {return intfc_tracker[i];}

    //! How many interface tracker objects are loaded
    int numTracker() {return intfc_tracker.size();}

    //! Echo the interface tracker options
    void echoTracker(ostream& ofs);
    //@}
    //**************************************************************************

    void swapGhostCentroids();

    void swapGhostPrimitiveVar();

    void swapGhostGradient();

    // Set CFL
    void setCFL(Real parm) {CFL = parm;}

    //**************************************************************************
    //! \name Material property evaluation/update
    //@{
    //! setup material property dependencies
    void setupMaterialDependence();
    //@}
    //**************************************************************************

    //**************************************************************************
    //! \name Output Delegates
    //@{
    void formSurfProjID(DirichletBC::BCvar& bcvareval,
                        Real* node_var, Real* work, int* iflag);

    //! Field delegate for density at elements
    void writeElemDensityField(const OutputDelegateKey& key,
                               int plnum, int varId);

    //! Field delegate for density at nodes
    void writeNodeDensityField(const OutputDelegateKey& key,
                               int plnum, int varId);

    //! Field delegate for internal energy at elements
    void writeElemInternalEnergyField(const OutputDelegateKey& key,
                                      int plnum, int varId);

    //! Field delegate for internal energy at nodes
    void writeNodeInternalEnergyField(const OutputDelegateKey& key,
                                      int plnum, int varId);

    //! Field delegate for Mach No. at elements
    void writeElemMachNumberField(const OutputDelegateKey& key,
                                  int plnum, int varId);

    //! Field delegate for Mach No. at nodes
    void writeNodeMachNumberField(const OutputDelegateKey& key,
                                  int plnum, int varId);

    //! Field delegate for pressure at elements
    void writeElemPressureField(const OutputDelegateKey& key,
                                int plnum, int varId);

    //! Field delegate for pressure at nodes
    void writeNodePressureField(const OutputDelegateKey& key,
                                int plnum, int varId);

    //! Field delegate for temperature at elements
    void writeElemTemperatureField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Field delegate for temperature at nodes
    void writeNodeTemperatureField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Field delegate for total energy at elements
    void writeElemTotalEnergyField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Field delegate for total energy at nodes
    void writeNodeTotalEnergyField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Field delegate for velocity at elements
    void writeElemVelocityVectorField(const OutputDelegateKey& key,
                                      int plnum, int varId);

    //! Field delegate for velocity at nodes
    void writeNodeVelocityVectorField(const OutputDelegateKey& key,
                                      int plnum, int varId);
    //@}

    // History Delegates


  private:

    //! Don't permit copy or assignment operators
    //@{
    CCEuler(const CCEuler&);
    CCEuler& operator=(const CCEuler&);
    //@}

    //! Data required for C-Law solver
    DataIndex UN;
    DataIndex U1;
    DataIndex U2;
    DataIndex DWDX;
    DataIndex DWDY;
    DataIndex DWDZ;
    DataIndex GHOST_DATA;
    DataIndex GHOST_DWDX;
    DataIndex GHOST_DWDY;
    DataIndex GHOST_DWDZ;

    //! Temporary storage
    DataIndex VEL;
    DataIndex TEMP_NODE;
    DataIndex TEMP_ELEM;

    //! Global element id & matid
    int geid[BLKSIZE];

    //! Cell data
    W3D    wc[BLKSIZE];
    Real  xec[BLKSIZE], yec[BLKSIZE], zec[BLKSIZE];
    Real   ev[BLKSIZE], evi[BLKSIZE];

    //! Neighbor values and distances to adjacent cells
    W3D   ws[BLKSIZE][MAXNB];
    Real dxe[BLKSIZE][MAXNB], dye[BLKSIZE][MAXNB], dze[BLKSIZE][MAXNB];

    //! Face centroids (flux quadrature points)
    Real xs[BLKSIZE][MAXNB], ys[BLKSIZE][MAXNB], zs[BLKSIZE][MAXNB];

    //! Distances from element centroid to faces
    Real dx[BLKSIZE][MAXNB], dy[BLKSIZE][MAXNB], dz[BLKSIZE][MAXNB];

    //! Face area/length
    Real gamma[BLKSIZE][MAXNB];

    //! Unit normal
    Real ex[BLKSIZE][MAXNB], ey[BLKSIZE][MAXNB], ez[BLKSIZE][MAXNB];

    //! Min/Max primitive variables & gradients
    W3D wmax[BLKSIZE], wmin[BLKSIZE];
    W3D dwdx[BLKSIZE], dwdy[BLKSIZE], dwdz[BLKSIZE];

    //! Time step control
    Real m_time;
    Real dt;
    Real CFL;

    TimeIntType time_int;

    //! \name STL vectors of BC objects
    //@{
    vector<ZeroVelBC*> zero_velbc;
    vector<DirichletBC*> dirichletbc;
    //@}

    //! \name STL vector of IC Objects
    //@{
    vector<CCEulerBlockICs*> block_ics;
    //@}

    //! \name STL vector of body forces. The net force is the sum of all these.
    //@{
    vector<CCEulerBodyForce*> body_forces;
    //@}

    //! \name STL vector of InterfaceTracker Objects
    //@{
    vector<InterfaceTracker*> intfc_tracker;
    //@}

    //! \name STL vector of InterfaceReconstruction Objects
    //@{
    vector<InterfaceReconstruction*> intfc_recon;
    //@}

    // Simple IC's
    Real m_vel_ic[3];  //!< Global Velocity IC's
    Real m_tic;        //!< Global temperature IC's

    //! \name User defined initial conditions (should only use one for now)
    //@{
    vector<UserIC*> m_useric;
    //@}

    int m_elemvar_dim;
    int m_sendvar_dim;

  };

}
#endif // CCEuler_h
