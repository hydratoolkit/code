//******************************************************************************
//! \file    src/FVM/CCEuler/CCEulerHistoryDelegates.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Time-history delegates for cell-centered Euler solver
//******************************************************************************

#ifndef HYDRA_CCEULER_HISTORY_DELEGATES_H
#define HYDRA_CCEULER_HISTORY_DELEGATES_H

namespace Hydra {

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeElemPressureHistory,
                     CCEulerElemPressureHistory);

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeInternalEnergyElementHistory,
                     CCEulerInternalEnergyElementHistory);

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeTotalEnergyElementHistory,
                     CCEulerTotalEnergyElementHistory);

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeTemperatureElementHistory,
                     CCEulerTemperatureElementHistory);

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeVelocityVectorElementHistory,
                     CCEulerVelocityVectorElementHistory);

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeMachNumberElementHistory,
                     CCEulerMachNumberElementHistory);

}

#endif
