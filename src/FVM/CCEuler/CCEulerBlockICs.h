//******************************************************************************
//! \file    src/FVM/CCEuler/CCEulerBlockICs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Block-based IC's for Euler solver
//******************************************************************************

#ifndef CCEulerBlockICs_h
#define CCEulerBlockICs_h

#include <HydraTypes.h>
#include <Block.h>

namespace Hydra {

//! CCEuler Block Velocity Initial Conditions
class CCEulerBlockICs : public Block {

  public:

    //! \name Constructor/Destructor
    //@{
             CCEulerBlockICs() {}
    virtual ~CCEulerBlockICs() {}
    //@}


    //! \name CCEulerBlockICs access functions
    //! The CCEulerBlockICs class is used to setup physics-specific
    //! block initial velocity conditions.
    //@{
    void setVelIC(Real* vel) {velic[0] = vel[0];
                              velic[1] = vel[1];
                              velic[2] = vel[2];}
    void setTemperatureIC(Real T) {Tic = T;}

    Real* getVelIC() {return velic;}
    Real  getVelx()  {return velic[0];}
    Real  getVely()  {return velic[1];}
    Real  getVelz()  {return velic[2];}
    Real  getTemperature() {return Tic;}
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCEulerBlockICs(const CCEulerBlockICs&);
    CCEulerBlockICs& operator=(const CCEulerBlockICs&);
    //@}

    Real  velic[3];
    Real  Tic;
};

}

#endif // CCEulerBlockICs_h
