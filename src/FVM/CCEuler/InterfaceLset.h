//******************************************************************************
//! \file    src/FVM/CCEuler/InterfaceLset.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:40:02 2011
//! \brief   Level set interface for cell-centered FVM Euler solver
//******************************************************************************

#ifndef InterfaceLset_h
#define InterfaceLset_h

namespace Hydra {

#define GEN_TOL 1.0e-5

enum Elcat {FLUID=0,           //!< fluid element
            FLUIDCUT,          //!< fluid element cut by interface
            GHOST,             //!< ghost element
            GHOSTCUT,          //!< ghost element cut by interface
            DEAD,              //!< dead element
            NUMELCATS          //!< # of element categories
};

enum Nmark {YES=0, NO};

enum GeomInterfaceType {NONE=0, CIRCLE, SPHERE, RECTANGLE, BOX};

struct Bucket {
  Real xmin, xmax;
  Real ymin, ymax;
  Real zmin, zmax;
  int  Nel;
  DataIndex BUCKET_INDEX;
};

}
#endif // InterfaceLset_h
