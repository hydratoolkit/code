//******************************************************************************
//! \file    src/FVM/CCEuler/CCEulerFieldDelegates.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Field delegates for incompressible flow
//******************************************************************************
#ifndef CCEulerFieldDelegates_h
#define CCEulerFieldDelegates_h

#include <CCEuler.h>

namespace Hydra {

//! Declaration of the field delegates

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeElemDensityField,
                     CCEulerElemDensityField)

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeNodeDensityField,
                     CCEulerNodeDensityField)

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeElemInternalEnergyField,
                     CCEulerElemInternalEnergyField)

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeNodeInternalEnergyField,
                     CCEulerNodeInternalEnergyField)

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeElemMachNumberField,
                     CCEulerElemMachNumberField)

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeNodeMachNumberField,
                     CCEulerNodeMachNumberField)

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeElemPressureField,
                     CCEulerElemPressureField)

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeNodePressureField,
                     CCEulerNodePressureField)

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeElemTotalEnergyField,
                     CCEulerElemTotalEnergyField)

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeElemTemperatureField,
                     CCEulerElemTemperatureField)

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeNodeTemperatureField,
                     CCEulerNodeTemperatureField)

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeNodeTotalEnergyField,
                     CCEulerNodeTotalEnergyField)

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeElemVelocityVectorField,
                     CCEulerElemVelocityVectorField)

OUTPUT_DELEGATE_DECL(CCEuler,
                     writeNodeVelocityVectorField,
                     CCEulerNodeVelocityVectorField)

}

#endif // CCEulerFieldDelegates.h
