//******************************************************************************
//! \file    src/FVM/CCMultiField/CCMFSourceMap.h
//! \author  Jozsef Bakosi
//! \date    Fri Sep 28 11:50:34 2011
//! \brief   Map of vectors of sources
//******************************************************************************
#ifndef CCMFSourceMap_h
#define CCMFSourceMap_h

#include <ostream>
#include <iomanip>

#include <VecMap.h>
#include <CCMFDrag.h>

namespace Hydra {

//! Map of vectors of pointer to CCMF source types
template<class T, typename Key = int>
class CCMFSourceMap : public VecMap<Key, T> {

    using VecMap<Key, T>::m_cit;
    using VecMap<Key, T>::m_mapvec;
    using VecMap<Key, T>::m_Nfield;
    typedef typename VecMap<Key, T>::vector_type vector_type;

  public:

    //! \name Constructor/Destructor
    //@{
             CCMFSourceMap() {}
    virtual ~CCMFSourceMap() {}
    //@}

    //! Add a source to the map (for default Key = int)
    //!   \param[in] tblid     table id
    //!   \param[in] amp      Amplitude of body force
    //!   \param[in] setid    Exodus class id
    //!   \param[in] field    vector<int> with field ids this source relates to
    void add(int tblid, Real amp, int setid, const vector<int>& field) {
      const int nfields = field.size();
      for (int i=0; i<nfields; i++)
        m_mapvec[field[i]].push_back(new T(tblid,amp,setid));
    }

    //! Specialization of add() for mapped objects (e.g., CCMFDrag, CCMFLift)
    //!   \details Definitions are in CCMFDragMap.C, CCMFLiftMap.C, etc.
    //!   \param[in] param   P parameters bundle
    //!   \param[in] ndim    Number of spatial dimensions
    template< class P >
    void add(const P& param, const int& ndim);

    //! Echo all sources
    //! \param[out] ofs      Output stream to echo to
    void echo(ostream& ofs) {
      for (m_cit=m_mapvec.begin(); m_cit!=m_mapvec.end(); ++m_cit) {
        const vector_type* v = &m_cit->second;
        const int numvec = v->size();
        for (int i=0; i<numvec; i++)
          echoSource((*v)[i], m_cit->first, ofs);
      }
    }

    //! Return a vector of T* for a field based on half-sym-storage-keyed map
    //!   \param[in]  i  field whose row to get
    //!   \return        pair of <field-ids, drag-pointers>
    pair< vector_type, vector<int> > getRowPtr(int i) {
      pair< vector_type, vector<int> > ret;
      for (int j=0; j<m_Nfield; j++) {
         int k=i, l=j;
         if (k>l) swap(k,l);    // map keys are stored for only i<j
         if (k!=l) {
           vector_type& v = m_mapvec[pair<int,int>(k,l)];
           if (v.size()) {
             ret.first.push_back(v.front()); // first of vec has the ptr
             ret.second.push_back(j);        // field id other than i
           }
         }
      }
      return ret;
    }

    //! Fill raw pointers to physical variables in all mapped objects
    //!   \param[in] mesh    Mesh object
    //!   \param[in] vars    DataIndex variable bundle
    template< class V >
    void getVars(UnsMesh* const mesh, const V& vars) {
      for (m_cit=m_mapvec.begin(); m_cit!=m_mapvec.end(); ++m_cit) {
        const vector_type* v = &m_cit->second;
        const int numvec = v->size();
        for (int i=0; i<numvec; i++) {
          (*v)[i]->getVars(mesh, vars);
        }
      }
    }

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCMFSourceMap(const CCMFSourceMap&);
    CCMFSourceMap& operator=(const CCMFSourceMap&);
    //@}

    //! insert T pointer to vector
    //!   \param[in]     source   pointer to insert
    //!   \param[inout]  v        vector to insert to
    void insert(T* const source, vector_type& v) {
      if (v.size()) {     // if exist, overwrite
        delete v[0];      // delete existing one
        v[0] = source;
      } else {            // if does not exist, insert
        v.push_back(source);
      }
    }

    // Echo for field when Key = int
    //!   \param[inout]  ofs   Output stream
    //!   \param[in]     f     field key
    void echoField(ostream& ofs, int f) {
      ofs << setw(12) << right << f+1 << "  ";
    }

    // Echo for field when Key = pair<int,int>
    //!   \param[inout]  ofs   Output stream
    //!   \param[in]     f     field key
    void echoField(ostream& ofs, const pair<int,int>& f) {
      ofs << setw(10) << right << f.first+1 << "," << f.second+1 << "  ";
    }

    //! Echo one-liner info for source
    //!   \param[in]  source   Source to echo
    //!   \param[in]  field    Field id to echo information of
    //!   \param[out] ofs      Output stream to echo to
    void echoSource(T* const source, const Key& field, ostream& ofs) {
      ofs << "\t";
      echoField(ofs, field);  //!< Different depending on key type
      source->echo(ofs);      //!< The rest of the line is model-specific
      ofs << endl;
    }
};

}

#endif
