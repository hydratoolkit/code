//******************************************************************************
//! \file    src/FVM/CCMultiField/CCMFIntEnergy.h
//! \author  Jozsef Bakosi
//! \date    Mon Jan 6 12:46:22 2011
//! \brief   Multifield internal energy transport
//******************************************************************************
#ifndef CCMFIntEnergy_h
#define CCMFIntEnergy_h

#include <CCINSIntEnergy.h>

namespace Hydra {

class CCMFIntEnergy : public CCINSIntEnergy {

  public:

    //! \name Constructor/Destructor
    //@{
             CCMFIntEnergy(UnsMesh& mesh,
                           Control& control,
                           DualEdgeGradOp& edgeGradOp,
                           CCINSErrors& errors,
                           fileIO& io,
                           CCINSTransportVar& di,
                           vector<CCINSTemperatureBC*>& tempBCs,
                           vector<CCINSHeatFluxBC*>& heatfluxbc,
                           vector<CCINSHeatSource*>& heat_source,
                           vector<CCINSSurfChem*>& surfchem,
                           LASolver& transport_solver,
                           LAMatrix& transport_A,
                           LAVector& transport_b,
                           LAVector& transport_x,
                           CCINSTurbulence& turbulence,
                           CCINSAdapter& adapter);
    virtual ~CCMFIntEnergy();
    //@}

    //! Assemble generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void assembleSystem(const CCINSIncParm& incParm);

    //! Solve generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);

    //! Form transport Rhs function
    //!   \param[in]     incParm Increment parameter functions
    //!   \param[in,out] rhs     Pointer to the rhs vector
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs=0);

    /**************************************************************************/

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCMFIntEnergy(const CCMFIntEnergy&);
    CCMFIntEnergy& operator=(const CCMFIntEnergy&);
    //@}

};

}

#endif // CCMFIntEnergy_h

