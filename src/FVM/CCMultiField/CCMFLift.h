//******************************************************************************
//! \file    src/FVM/CCMultiField//CCMFLift.h
//! \author  Jozsef Bakosi, Mark A. Christon
//! \date    Wed Oct 23 10:50:35 2013
//! \brief   Base class for momentum lift terms for multiphase flow
//******************************************************************************
#ifndef CCMFLift_h
#define CCMFLift_h

#include <vector>

#include <HydraTypes.h>
#include <CCMFMomentumExchange.h>
#include <UnsMesh.h>

namespace Hydra {

//! Lift (momentum exchange for multiphase flow) types
enum LiftType { CONST_LIFT=0 };

//! Associate velocity derivatives with human-readable tags
enum VelocityDerivatives { UY=0, VZ, WX, VX, WY, UZ };

//! Lift parameters bundle
struct LiftParam {
  LiftType type;                         //!< Lift type
  int setId;                             //!< Material set ID
  DisperseID dispId;                     //!< Association of the disperse ID
  int tblid;                              //!< Load cureve id
  Real amp;                              //!< Amplitude of table
  Real bd;                               //!< Bubble diameter
  Real coeff;                            //!< (Constant) lift coefficient
  const vector< pair<int,int> >& field;  //!< Field id pairs lift acts between

  //! Initializer constructor
  //!   \param[in] t      Lift type, see enum LiftType
  //!   \param[in] sid    Material set id
  //!   \param[in] did    Association of the disperse ID
  //!   \param[in] lid    table id
  //!   \param[in] amp    Amplitude of table
  //!   \param[in] bd     Bubble diameter
  //!   \param[in] coeff  (Constant) lift coefficient
  //!   \param[in] field  Field id pairs lift acts between
  LiftParam(LiftType t,
            int sid,
            DisperseID did,
            int lid,
            Real a,
            Real b,
            Real c,
            const vector< pair<int,int> >& f) : type(t),
                                                setId(sid),
                                                dispId(did),
                                                tblid(lid),
                                                amp(a),
                                                bd(b),
                                                coeff(c),
                                                field(f) {}
};

//! Lift DataIndex variables bundle
struct LiftVar {
  DataIndex VOLUME;
  const vector< DataIndex >& VOLFRAC;
  const vector< DataIndex >& DENSITY;
  const vector< DataIndex >& VEL;
  const vector< DataIndex >& VELN;
  const vector< DataIndex >& ELEM_TENSOR;

  //! Initializer constructor
  LiftVar(DataIndex volume,
          const vector< DataIndex >& volfrac,
          const vector< DataIndex >& density,
          const vector< DataIndex >& vel,
          const vector< DataIndex >& veln,
          const vector< DataIndex >& elem_tensor) : VOLUME(volume),
                                                    VOLFRAC(volfrac),
                                                    DENSITY(density),
                                                    VEL(vel),
                                                    VELN(veln),
                                                    ELEM_TENSOR(elem_tensor) {}
};

//! Base class for lift momentum exchange terms
class CCMFLift : public MomentumExchange {

  public:

    //! \name Constructor/Destructor
    //@{
    //! Initializer constructor
    //!   \param[in]  param    Lift parameters, see struct LiftParam
    //!   \param[in]  pair     Field ID pair lift acts between
    //!   \param[in]  nfield   Total number of fields
    //!   \param[in]  ndim     Number of spatial dimensions
    //!   \details Note that nfield and ndim are references as they do not have
    //!          the correct values at parsing (when this constructor is called)
    CCMFLift(const LiftParam& param,
             int pair,
             const int& nfield,
             const int& ndim);
    virtual ~CCMFLift();
    //@}

    //! Echo one-liner info
    //!   \param[inout]  ofs  output stream to echo to
    virtual void echo(ostream& ofs) const;

    //! Return string describing lift type (for echo)
    //!   \return string describing the type of lift
    virtual string type() const = 0;

    //! Compute and return lift force operator:
    //! i.e., lift_coeff * volfrac * rho * ( ) x vorticity
    //!   \param[in]     id1    Field id 1
    //!   \param[in]     id2    Field id 2
    //!   \param[in]     bo     Block offset to access physical variable
    //!   \param[in]     bs     Block size
    //!   \param[inout]  L      Computed force operator
    virtual void calcForceOp(int id1,
                             int id2,
                             int bo,
                             int bs,
                             vector< vector<Real*> >& L) = 0;

    //! Fill raw pointers to physical variables
    //!   \param[in] mesh   Mesh object
    //!   \param[in] var    Lift DataIndex variables bundle
    void getVars(UnsMesh* const mesh, const LiftVar& var);

    //! Return relative velocity at the current time level
    //!   \param[in]  id1  Field id 1
    //!   \param[in]  id2  Field id 2
    //!   \param[in]  idx  Cell index
    //!   \param[in]  dim  Dimension (i.e., direction) to compute
    //!   \return     Relative velocity between field-pair at current time level
    Real rvel(int id1, int id2, int idx, int dim) const {
      return vel[id1][dim][idx] - vel[id2][dim][idx];
    }

    //! Return relative velocity at the previous time level
    //!   \param[in]  id1  Field id 1
    //!   \param[in]  id2  Field id 2
    //!   \param[in]  idx  Cell index
    //!   \param[in]  dim  Dimension (i.e., direction) to compute
    //!   \return     Relative velocity between field-pair at previous time level
    Real rveln(int id1, int id2, int idx, int dim) const {
      return veln[id1][dim][idx] - veln[id2][dim][idx];
    }

  protected:

    const int m_dispId;               //!< Disperse phase ID
    const int m_contId;               //!< Continuous phase ID
    const Real m_bubbleRad;           //!< Bubble radius

    //! Raw pointers to physical variables required for lift force calculation
    //! DO NOT EVER ATTEMPT TO CALL DELETE ON THESE POINTERS !!!
    Real* V;
    vector< Real* > vf;
    vector< Real* > rho;
    vector< vector<Real*> > vel;
    vector< vector<Real*> > veln;
    vector<Real*> vgrad;

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCMFLift(const CCMFLift&);
    CCMFLift& operator=(const CCMFLift&);
    //@}

};

}

#endif
