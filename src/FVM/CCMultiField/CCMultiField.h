//******************************************************************************
//! \file    src/FVM/CCMultiField/CCMultiField.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:39 2011
//! \brief   Multi field physics
//******************************************************************************

#ifndef CCMultiField_h
#define CCMultiField_h

#include <cassert>
#include <vector>
#include <sstream>

#include <Control.h>
#include <fileIO.h>
#include <UnsMesh.h>
#include <CCclaw.h>
#include <CCINSFlow.h>
#include <CCMFMomentum.h>
#include <CCMFVolFrac.h>
#include <ScalMap.h>
#include <CCMFBCMap.h>
#include <CCMFForceMap.h>
#include <CCMFSourceMap.h>
#include <CCMFVolFracBC.h>
#include <UnsPhysicsFieldDelegates.h>
#include <CCINSFieldDelegates.h>
#include <CCINSSurfaceDelegates.h>
#include <CCINSHistoryDelegates.h>
#include <CCMFDrag.h>
#include <CCMFLift.h>

namespace Hydra {

//! Base names for per-field variable names, will get augmented by field ID
//! during registration
const string            VOLFRAC_BASENAME = "VOLFRAC";
const string           VOLFRACN_BASENAME = "VOLFRACN";
const string    EXTEDGE_VOLFRAC_BASENAME = "EXTEDGE_VOLFRAC";
const string   EXTEDGE_VOLFRACN_BASENAME = "EXTEDGE_VOLFRACN";
const string           GHOSTVAR_BASENAME = "GHOST_VAR";
const string            DENSITY_BASENAME = "DENSITY";
const string           DENSITYN_BASENAME = "DENSITYN";
const string         DENSITYNM1_BASENAME = "DENSITYNM1";
const string    EXTEDGE_DENSITY_BASENAME = "EXTEDGE_DENSITY";
const string   EXTEDGE_DENSITYN_BASENAME = "EXTEDGE_DENSITYN";
const string       MACRODENSITY_BASENAME = "MACRODENSITY";
const string      MACRODENSITYN_BASENAME = "MACRODENSITYN";
const string    MACRODENSITYNM1_BASENAME = "MACRODENSITYNM1";
const string        TEMPERATURE_BASENAME = "TEMP";
const string       TEMPERATUREN_BASENAME = "TEMPN";
const string     TEMPERATURENM1_BASENAME = "TEMPNM1";
const string           ENTHALPY_BASENAME = "ENTHALPY";
const string          ENTHALPYN_BASENAME = "ENTHALPYN";
const string        ENTHALPYNM1_BASENAME = "ENTHALPYNM1";
const string          INTENERGY_BASENAME = "INTENERGY";
const string         INTENERGYN_BASENAME = "INTENERGYN";
const string       INTENERGYNM1_BASENAME = "INTENERGYNM1";
const string                VEL_BASENAME = "VEL";
const string               VELN_BASENAME = "VELN";
const string              VELNN_BASENAME = "VELNN";
const string          GHOST_VEL_BASENAME = "GHOST_VEL";
const string          GHOST_VOL_BASENAME = "GHOST_VOL";
const string    GHOST_ASSEM_VOL_BASENAME = "GHOST_ASSEM_VOL";
const string             VEL_BC_BASENAME = "VEL_BC";
const string       DUALEDGE_VEL_BASENAME = "DUALEDGE_VEL";
const string      DUALEDGE_VELN_BASENAME = "DUALEDGE_VELN";
const string                DIV_BASENAME = "DIV";
const string           PRESSURE_BASENAME = "PRESSURE";
const string          PRESSUREN_BASENAME = "PRESSUREN";
const string             LAMBDA_BASENAME = "LAMBDA";
const string          VORTICITY_BASENAME = "VORTICITY";
const string        ELEM_TENSOR_BASENAME = "ELEM_TENSOR";
const string          ENSTROPHY_BASENAME = "ENSTROPHY";
//#####DEBUG -- SORT OUT MAT'L PROP's
const string ELEM_MOL_VISCOSITY_BASENAME = "ELEM_MOL_VISCOSITY";
const string          EDGE_COND_BASENAME = "EDGE_CONDUCTIVITY";
//#####DEBUG -- SORT OUT MAT'L PROP's
//#####DEBUG -- TEMPORARY REMOVE THIS
// Jozsef need to remove this + memory allocation
const string               DRAG_BASENAME = "DRAG";
const string               LIFT_BASENAME = "LIFT";
//#####DEBUG -- TEMPORARY REMOVE THIS

//! Finite Volume multi field class
class CCMultiField : public CCINSFlow {

  //! Shorthand for a pair of <field-ids, drag-pointers>
  typedef pair< vector<CCMFDrag*>, vector<int> > Drag;
  //! Shorthand for a pair of <field-ids, lift-pointers>
  typedef pair< vector<CCMFLift*>, vector<int> > Lift;

  public:

    //! \name Constructor/Destructor
    //@{
             CCMultiField();
    virtual ~CCMultiField() {}
    //@}

    //! Allocate and setup physics specific variables
    virtual void setup();

    //! Initialize the physics
    virtual void initialize();

    //! Finalize the physics
    virtual void finalize();

    //! Solve the physics problem
    virtual void solve();

    //! Register all solution variables
    virtual void registerData();

    //! Write a message identifying the physics solver
    virtual void writeSolving();

    //! Echo physics-specific options
    virtual void echoOptions(ostream& ofs);

    virtual void setupMergedSetBCs() {}

    //! Initialize field maps (BCs, forces) with empty fields
    //! \param[in] nfields Number of fields to initialize
    void initFieldMaps(const int nfields);

    //! Add a Dirichlet boundary condition
    void addDirichletBC(const BCPackage& bc, int bcvar,
                        const vector<int>& field);

    //! Add velocity boundary conditions
    void addVelocityBC(const BCPackage& bc, const vector<int>& field);

    //! Apply BCs and calculate external edge data for a single field
    //!   \param[in]  bc        Vector of boundary conditions
    //!   \param[in]  VAR       Variable to operate on (e.g. DENSITY)
    //!   \param[in]  GHOSTVAR  Ghost variable to operate on (e.g. GHOST_DENSITY)
    //!   \param[in]  time      Physical time (for potential table look-up)
    //!   \param[in]  exchange  Whether to exchange proc-boundary ghosts
    template<class BC>
    void calcEdgeVar(vector<BC*>& bc,
                     const DataIndex VAR,
                     const DataIndex GHOSTVAR,
                     const Real time,
                     const bool exchange) {

      Real* var = mesh->getVariable<Real>(VAR);
      Real* gvar = mesh->getVariable<Real>(GHOSTVAR);
      Real* xi = mesh->getVariable<Real>(EDGE_XI);

      // Setup default data on external edges with a symmetric mirror
      for (int i=0; i<Nedge_ext; i++) gvar[i] = var[edges[i].gid1];

      // Set data from Dirichlet BCs
      typename vector<BC*>::const_iterator it;
      for (it=bc.begin(); it!=bc.end(); ++it) {
        (*it)->setEdgeBCs(*mesh,gvar,time);
      }

      // Swap boundary-data as necessary
      if (Nproc>1 && exchange) swapGhostVar(var, gvar);

      // Calculate edge data for INTERPROC_EDGE's
      for (int i=0; i<Nedge_ext; i++) {
        if (edges[i].type == INTERPROC_EDGE) {
          gvar[i] = xi[i]*var[edges[i].gid1] + (1.0-xi[i])*gvar[i];
        }
      }
    }

    //! Update the Nth volume fraction in cell centers by enforcing unit-sum
    void enforceCompatibility();

    //! Manage volumetric heat sources
    void addHeatSource(const int tblid,
                       const Real amp,
                       const int setid,
                       const vector<int>& field);

    //! Set initial conditions
    void setICs(CCINSICPackage& ic, const vector<int>& field);

    //! Echo initial conditions
    void echoICs(ostream& ofs);

    //! Add a body force
    void addBodyForce(int setid,
                      int tblid,
                      Real amp,
                      const Real* const en,
                      const vector<int>& field);

    //! Add a Boussinesq body force
    void addBoussinesqForce(int setid,
                            int tblid,
                            Real amp,
                            const Real* const en,
                            const vector<int>& field);
    //! Add a drag force
    void addDrag(DragType type,
                 UnsMesh* mesh,
                 int setid,
                 DisperseID dispId,
                 int tblid,
                 Real amp,
                 Real bd,
                 Real st,
                 Real coeff,
                 const vector< pair<int,int> >& field);

    //! Add a gravity body force
    void addGravityForce(int setid,
                         int tblid,
                         Real amp,
                         const Real* const en,
                         const vector<int>& field);

    //! Add a lift force
    void addLift(LiftType type,
                 UnsMesh* mesh,
                 int setid,
                 DisperseID dispId,
                 int tblid,
                 Real amp,
                 Real bd,
                 Real coeff,
                 const vector< pair<int,int> >& field);

    //**************************************************************************
    //! \name Output Delegates
    //@{
    //! Get Field ID from multi-field variable-name
    //!   \param[in] name    Variable name
    //!   \return            Field ID
    int fieldID(const string& name);

    //! Interpolate an elem velocity component to nodes, applying BCs
    //!   \param[in] evar    Elem velocity component
    //!   \param[in] bc      Vector of boundary conditions
    //!   \param[in] dir     BC direction
    //!   \param[in] nvar    Node velocity component
    void projectVelToNodes(Real* evar,
                           const vector<CCINSVelocityBC*>& bc,
                           const GlobalDirection dir,
                           Real* nvar);

    //! Field Delegates

    //! Output delegate for density at nodes
    //!   \param[in] key    Output delegate key
    //!   \param[in] plnum  Plot number (i.e. counter)
    //!   \param[in] varId  Variable ID (in output file)
    void writeNodeDensityField(const OutputDelegateKey& key,
                               int plnum, int varId);

    //! Output delegate for velocity at nodes
    //!   \param[in] key    Output delegate key
    //!   \param[in] plnum  Plot number (i.e. counter)
    //!   \param[in] varId  Variable ID (in output file)
    void writeNodeVelocityVectorField(const OutputDelegateKey& key,
                                      int plnum, int varId);

    //! Output delegate for vorticity at elements
    //!   \param[in] key    Output delegate key
    //!   \param[in] plnum  Plot number (i.e. counter)
    //!   \param[in] varId  Variable ID (in output file)
    void writeElemVorticityVectorField(const OutputDelegateKey& key,
                                       int plnum, int varId);

    //! Output delegate for vorticity at nodes
    //!   \param[in] key    Output delegate key
    //!   \param[in] plnum  Plot number (i.e. counter)
    //!   \param[in] varId  Variable ID (in output file)
    void writeNodeVorticityVectorField(const OutputDelegateKey& key,
                                       int plnum, int varId);

    //! Output delegate for volume fractions at nodes
    //!   \param[in] key    Output delegate key
    //!   \param[in] plnum  Plot number (i.e. counter)
    //!   \param[in] varId  Variable ID (in output file)
    void writeNodeVolFracField(const OutputDelegateKey& key,
                               int plnum, int varId);

    //! Output delegate for temperature at nodes
    //!   \param[in] key    Output delegate key
    //!   \param[in] plnum  Plot number (i.e. counter)
    //!   \param[in] varId  Variable ID (in output file)
    void writeNodeTemperatureField(const OutputDelegateKey& key,
                                   int plnum, int varId);

    //! Output delegate for internal energy at nodes
    //!   \param[in] key    Output delegate key
    //!   \param[in] plnum  Plot number (i.e. counter)
    //!   \param[in] varId  Variable ID (in output file)
    void writeNodeIntEnergyField(const OutputDelegateKey& key,
                                 int plnum, int varId);

    //! Output delegate for enthalpy at nodes
    //!   \param[in] key    Output delegate key
    //!   \param[in] plnum  Plot number (i.e. counter)
    //!   \param[in] varId  Variable ID (in output file)
    void writeNodeEnthalpyField(const OutputDelegateKey& key,
                                int plnum, int varId);

    //! Output delegate for mixture density at elements
    //!   \param[in] key    Output delegate key
    //!   \param[in] plnum  Plot number (i.e. counter)
    //!   \param[in] varId  Variable ID (in output file)
    void writeElemMixtureDensityField(const OutputDelegateKey& key,
                                      int plnum, int varId);

    //! Output delegate for mixture density at nodes
    //!   \param[in] key    Output delegate key
    //!   \param[in] plnum  Plot number (i.e. counter)
    //!   \param[in] varId  Variable ID (in output file)
    void writeNodeMixtureDensityField(const OutputDelegateKey& key,
                                      int plnum, int varId);

    //! Output delegate for mixture velocity at elements
    //!   \param[in] key    Output delegate key
    //!   \param[in] plnum  Plot number (i.e. counter)
    //!   \param[in] varId  Variable ID (in output file)
    void writeElemMixtureVelocityVectorField(const OutputDelegateKey& key,
                                             int plnum, int varId);

    //! Output delegate for mixture velocity at nodes
    //!   \param[in] key    Output delegate key
    //!   \param[in] plnum  Plot number (i.e. counter)
    //!   \param[in] varId  Variable ID (in output file)
    void writeNodeMixtureVelocityVectorField(const OutputDelegateKey& key,
                                             int plnum, int varId);
    //! Output delegate for drag force at nodes
    //!   \param[in] key    Output delegate key
    //!   \param[in] plnum  Plot number (i.e. counter)
    //!   \param[in] varId  Variable ID (in output file)
    void writeNodeDragVectorField(const OutputDelegateKey& key,
                                  int plnum, int varId);

    //! Output delegate for lift force at nodes
    //!   \param[in] key    Output delegate key
    //!   \param[in] plnum  Plot number (i.e. counter)
    //!   \param[in] varId  Variable ID (in output file)
    void writeNodeLiftVectorField(const OutputDelegateKey& key,
                                  int plnum, int varId);
    //@}

  protected:

    //**************************************************************************
    //! \name Utility Functions
    //@{
    //! Compute/collect the memory summary
    void computeMemory();

    //! Compute extents of variable over all fields
    //!   \param[in]  VAR    Vector of DataIndex
    //!   \param[in]  nvar   Number of elements to check
    //!   \param[out] min    Minimum value
    //!   \param[out] max    Maximum value
    void getVarLimits(const vector<DataIndex>& VAR,
                      int Nvar,
                      Real& varmin,
                      Real& varmax);

    //! Compute extents of vector over all fields
    //!   \param[in]  VEC    Vector of DataIndex pointing to a CVector
    //!   \param[in]  nvar   Number of elements to check
    //!   \param[out] min    Minimum value
    //!   \param[out] max    Maximum value
    void getVecLimits(const vector<DataIndex>& VEC,
                      int Nvar,
                      Vector& vecmin,
                      Vector& vecmax);

    //! Write the screen report
    //!   \param[out] ofs    Output stream
    //!   \param[in]  Ninc   Increment
    void writeReport(ostream& ofs, const int Ninc);

    //! Write the header for the screen report
    //!   \param[out] ofs    Output stream
    void writeReportHeader(ostream& ofs);

    //! Calculate the kinetic energy for output statistics
    Real calcKE();

    //! Calculate residuals
    void calcResiduals();

    //! Driver to calculate the minimum stable time-step
    void calcTimeStep(Real soltime);

    //! Write the glob file
    void writeGlobFile(const int step,
                       const bool field,
                       const bool statfield,
                       const bool history,
                       const bool restart);
    //@}

    //**************************************************************************
    //! \name Startup and projection functions
    //@{
    //! Check external edge range
    void checkExternalEdgeRange();

    //! Compute the initial prescribed-divergence velocity fields
    void startupProj();

    //! Project edge velocities to div-free subspace
    //!   \param[in] time  Current time-level for velocity field
    //!   \param[in] alpha = -1 for restart projection, +1 for normal projection
    //!   \param[in] field = multiphase velocity field to project
    void projectEdgeVelocity(const Real time,
                 const Real alpha,
                 const int field);

    //! Project element velocities to a div-free subspace
    //!   \param[in] alpha = -1 for restart projection, +1 for normal projection
    //!   \param[in] field = multiphase velocity field to project
    void projectElemVelocity(const Real alpha, const int field);

    //! Compute the mixture momentum field
    void mixMomentum(const vector<DataIndex>& VELOCITY,
                     const vector<DataIndex>& DUALEDGE_VELOCITY,
                     const vector<DataIndex>& VF,
                     const vector<DataIndex>& EDGE_VF,
                     const vector<DataIndex>& DENS,
                     const vector<DataIndex>& EDGE_DENS);

    //! Compute the initial pressure field
    void initPressure();

    //! Save the previous-time state/solution vector
    void saveStateVector();

    //! Calculate the partial momentum at element centers
    void calcPartialMom();

    //! Calculate the partial momentum on external edges
    void calcEdgePartialMom(Real* af);

    //! Setup basic operators
    void setupOperators();

    //! Compute the div-Free velocity field and update velocity and pressure
    void updateVelocityandPressure();

    //! Update the current-time pressure (while keeping the pressure at time n)
    void updatePressure(const Real alpha);
    //@}

    //**************************************************************************
    //! \name Gradient operators
    //@{
    //! Update the dual-edge velocity gradient
    void updateVelocityGradient(Real time);
    //@}

    //**************************************************************************
    //! \name Material evaluation/update
    //@{
    //! Calculate the turbulent viscosity for a given model
    void calcTurbulentViscosity();

    //! Initialize the material state
    void initMaterialState();

    //! Setup the assembled ghost element volumes for G-G smoothing
    void setGhostAssemVolume();

    //! Update the density (for an EOS)
    void updateDensity();

    //! Update the material state
    void updateMaterialState();

    //! Update the fluid molecular viscosity
    void updateMolecularViscosity();

    //! Update the turbulent viscosity and effective edge viscosity
    void updateTurbViscosity();

    //! Update the edge thermal conductivity
    void updateEdgeThermalConductivity();
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCMultiField(const CCMultiField&);
    CCMultiField& operator=(const CCMultiField&);
    //@}

    //**************************************************************************
    //! \name Registration for all variables and communications buffers
    //@{
    //! Register primary primitive variables and their output delegates
    void registerPrimVar();

    //! Register derived output delegates ONLY
    void registerDerivedVar();
    //@}

    //**************************************************************************
    //! \name Linear Algebra
    //@{
    //! Initialize the linear equation solvers, matrices and vectors
    void initLinearSolvers();

    //! Initialize the solver for the multiple momentum equations
    void initLinearSolverMom(Category* const pcat,
                             const LASolverFactory* const factory,
                             const int Nel_glob,
                             LASolverStatus* const status);

    //! Initialize the linear equations solver for transport equations
    void initLinearSolverTrans(Category* const pcat,
                               const LASolverFactory* const factory,
                               const int Nel_glob,
                               LASolverStatus* const status);
    //@}

    //**************************************************************************
    //! \name Transport Equations
    //@{
    //! Initialize the primary flow variables
    void initFlowVars();

    //! Initialize the transport equation solvers
    void initTransportSolvers();

    //! Initialize the transport equation DataIndexes
    void initTransportVars(int field);

    //! Setup momentum exchange computations
    void setupMomentumExchange();

    //! Solve energy transport equations
    void solveEnergyTransport();

    //! Solve species transport equations
    void solveSpeciesTransport() {}

    //! Solve turbulence transport equations
    void solveTurbulenceTransport();

    //! Solve volume fraction transport equations
    void solveVolFracTransport();

    //! Solve momemtum transport equations
    void solveMomentumTransport();

    //! Pre-compute momentum exchange force coefficients (more precisely, the
    //! full force modulo relative velocity) for different momentum exchange
    //! force types, e.g., drag, lift, etc.
    //!   \param[in] Force       Force type, e.g., Drag, Lift, etc.
    //!   \param[in] bo          Block-offset indexing physical variables
    //!   \param[in] bs          Block size in strip-mining
    //!   \param[in] fieldForce  Field force, e.g., m_fieldDrag, m_fieldLift
    //!   \param[out] coeff      Computed force coefficients
    template< typename Force >
    void mxCoeff(int bo,
                 int bs,
                 vector< Force >& fieldForce,
                 vector< vector<Real*> >& coeff) {
      typedef typename Force::second_type::size_type size_type;
      memset(coeff[0][0], 0, Ndim*Ndim*m_Nfield*m_Nfield * bs * sizeof(Real));
      for (int j=0; j<m_Nfield; j++) {
        Force& f = fieldForce[j];
        size_type numforces = f.first.size();
        for (size_type k=0; k<numforces; ++k) {
          f.first[k]->calcForceOp(j, f.second[k], bo, bs, coeff);
        }
      }
    }

    //! Precompute lift coefficients (full force modulo relative velocity)
    //!   \param[in] bo    Pre-computed block-offset indexing physical variables
    //!   \param[in] bs    Block size in strip-mining
    void liftCoeff(int bo, int bs);

    //! Explicit momentum-exchange predictor
    //!   \param[in] bo    Pre-computed block-offset indexing physical variables
    //!   \param[in] bs    Block size in strip-mining
    //!   \param[in] V     Cell volume raw pointer
    void explicitMomentumExchange(int bo,
                                  int bs,
                                  const Real* const V);

    //! Add mass terms to LHS and RHS of momentum-exchange solve
    //!   \param[in] bo    Pre-computed block-offset indexing physical variables
    //!   \param[in] bs    Block size in strip-mining
    void mass(int bo, int bs);

    //! Add momentum drag forces to LHS and RHS of momentum exchange solve
    //!   \param[in] bo    Pre-computed block-offset indexing physical variables
    //!   \param[in] bs    Block size in strip-mining
    void drag(int bo, int bs);

    //! Add momentum lift forces to LHS and RHS of momentum exchange solve
    //!   \param[in] bo    Pre-computed block-offset indexing physical variables
    //!   \param[in] bs    Block size in strip-mining
    void lift(int bo, int bs);

    //! Execute momentum exchange linear solve
    //!   \param[in] bo    Pre-computed block-offset indexing physical variables
    //!   \param[in] bs    Block size in strip-mining
    void momentumExchangeSolve(int bo, int bs);

    //! Apply momentum exchange terms (drag, lift, etc.)
    void momentumExchange();

    //! Apply body force terms to partial acceleration for one field
    void applyBodyForces(const int field, CVector& rhs);
    //@}

    //**************************************************************************
    //! \name Solution drivers and related
    //@{
    //! Advance the solution
    void advanceSolution();

    //! Apply Picard iteration based on 2nd-order projection
    void picardSolve();

    //! 2nd-order Projection solve (MAC's projection-2)
    void projectionSolve();
    //@}

    //**************************************************************************
    //! \name Boundary Conditions
    //!@{
    //! Echo boundary conditions
    void echoBCs(ostream& ofs);

    //! Echo all prescribed Dirichlet conditions
    void echoDirichletBCs(ostream& ofs);

    //! Query memory used by boundary conditions in bytes
    Real getBCMemory();

    //! Setup the velocity boundary condition flag  -- used for LS gradient
    CBoolVector setupVelBCFlag();

    //! Echo the heat sources
    void echoHeatSources(ostream &ofs);

    //! Echo the body foces
    void echoBodyForces(ostream &ofs);

    //! Echo the momentum drag forces
    void echoDrag(ostream& ofs);

    //! Echo the momentum lift forces
    void echoLift(ostream& ofs);
    //@}

    //**************************************************************************
    //! \name Variable Registration
    //@{
    //! Initialize all DataIndex values to UNREGISTERED
    void initDataIndex();

    //! Register material data
    void registerMaterialData();

    //! Register field variable and three output delegates for all fields
    //!   \param[in] id          DataIndex vector where variable IDs will be put
    //!   \param[in] number      Number of items to register
    //!   \param[in] size        Size of one item
    //!   \param[in] basename    Variable name, appended by _ and field ID
    //!   \param[in] centering   Variable centering
    //!   \param[in] type        Variable type
    //!   \param[in] layout      Memory layout
    //!   \param[in] plot        Whether it is possible to plot the variable
    //!   \param[in] restart     Whether the variable is saved to restart file
    //!   \param[in] fieldElmFun Elem-ctr field delegate function pointer
    //!   \param[in] histFun     History delegate wrapper function pointer
    //!   \param[in] fieldNodFun Node-ctr field delegate wrapper function ptr
    void registerNFieldVar(vector<DataIndex>& id,
                           const size_t number,
                           const size_t size,
                           const string basename,
                           const VariableCentering centering = NO_CTR,
                           const VariableType type = GENERIC_VAR,
                           const MemoryLayout layout = LINEAR_ARRAY,
                           const bool plot = false,
                           const bool restart = false,
                           const OutputDelegateWrapperCall fieldElmFun = 0,
                           const OutputDelegateWrapperCall histFun = 0,
                           const OutputDelegateWrapperCall fieldNodFun = 0);

    //! Free field variable for all fields
    //!   \param[in] id          DataIndex vector of variable IDs to be freed
    void freeVariable(vector<DataIndex>& id);

    //! Register field variable and three output delegates for a single field
    //!   \param[in] id          DataIndex where variable ID will be put
    //!   \param[in] number      Number of items to register
    //!   \param[in] size        Size of one item
    //!   \param[in] name        Variable name
    //!   \param[in] centering   Variable centering
    //!   \param[in] type        Variable type
    //!   \param[in] layout      Memory layout
    //!   \param[in] plot        Whether it is possible to plot the variable
    //!   \param[in] restart     Whether the variable is saved to restart file
    //!   \param[in] fieldElmFun Elem-ctr field delegate function pointer
    //!   \param[in] histFun     History delegate wrapper function pointer
    //!   \param[in] fieldNodFun Node-ctr field delegate wrapper function ptr
    void registerFieldVar(DataIndex& ID,
                          const size_t number,
                          const size_t size,
                          const string basename,
                          const VariableCentering centering = NO_CTR,
                          const VariableType type = GENERIC_VAR,
                          const MemoryLayout layout = LINEAR_ARRAY,
                          const bool plot = false,
                          const bool restart = false,
                          const OutputDelegateWrapperCall fieldElmFun = 0,
                          const OutputDelegateWrapperCall histFun = 0,
                          const OutputDelegateWrapperCall fieldNodFun = 0);

    //! Register three output delegates for all fields
    //!   \param[in] basename    Variable name, appended by _ and field ID
    //!   \param[in] type        Variable type
    //!   \param[in] fieldElmFun Elem-ctr field delegate function pointer
    //!   \param[in] histFun     History delegate wrapper function pointer
    //!   \param[in] fieldNodFun Node-ctr field delegate wrapper function ptr
    void registerNDelegate(const string basename,
                           const VariableType type = GENERIC_VAR,
                           const OutputDelegateWrapperCall fieldElmFun = 0,
                           const OutputDelegateWrapperCall histFun = 0,
                           const OutputDelegateWrapperCall fieldNodFun = 0);
    //@}

    //**************************************************************************
    //! \name Timer Registration
    //@{
    //! Register step, aka physics, timers
    void registerTimers();
    //@}

    //**************************************************************************
    //! \name DataIndex Declarations
    //@{
    //! Per-field scalar quantities
    vector<DataIndex> VOLFRAC;          //!< Volume fractions
    vector<DataIndex> VOLFRACN;         //!< Volume fractions at time n
    vector<DataIndex> EXTEDGE_VOLFRAC;  //!< External-edge volume fractions
    vector<DataIndex> EXTEDGE_VOLFRACN; //!< External-edge volume fracs, time n

    vector<DataIndex> GHOST_VAR;        //!< Generic ghost variable

    vector<DataIndex> DENSITY;          //!< Micro-Density at time n+1
    vector<DataIndex> DENSITYN;         //!< Micro-Density at time n
    vector<DataIndex> DENSITYNM1;       //!< Micro-Density at time n-1
    vector<DataIndex> EXTEDGE_DENSITY;  //!< External-edge density at time n+1
    vector<DataIndex> EXTEDGE_DENSITYN; //!< External-edge density at time n

    vector<DataIndex> MACRODENSITY;     //!< Macro-density at time n+1
    vector<DataIndex> MACRODENSITYN;    //!< Macro-density at time n
    vector<DataIndex> MACRODENSITYNM1;  //!< Macro-density at time n-1

    vector<DataIndex> TEMPERATURE;      //!< Temperature at time n+1
    vector<DataIndex> TEMPERATUREN;     //!< Temperature at time n
    vector<DataIndex> TEMPERATURENM1;   //!< Temperature at time n-1

    vector<DataIndex> ENTHALPY;         //!< Enthalpy at time n+1
    vector<DataIndex> ENTHALPYN;        //!< Enthalpy at time n
    vector<DataIndex> ENTHALPYNM1;      //!< Enthalpy at time n-1

    vector<DataIndex> INTENERGY;        //!< Internal energy at time n+1
    vector<DataIndex> INTENERGYN;       //!< Internal energy at time n
    vector<DataIndex> INTENERGYNM1;     //!< Internal energy at time n-1

    vector<DataIndex> DUALEDGE_VEL;     //!< Dual-Edge velocity at time n+1
    vector<DataIndex> DUALEDGE_VELN;    //!< Dual-Edge velocity at time n

    // Storage for material property update/evaluation
    vector<DataIndex> ELEM_MOL_VISCOSITY; //!< Element-level viscosity at n+1
    DataIndex ELEM_MOL_VISCOSITYN;        //!< Element-level viscosity at n
    DataIndex ELEM_TURB_VISCOSITY;        //!< Turbulent viscosity
    DataIndex EDGE_VISCOSITY;             //!< Modified wall viscosity at n+1
    DataIndex EDGE_VISCOSITYN;            //!< Modified wall viscosity at n
    DataIndex EDGE_CONDUCTIVITY;          //!< Effective edge conductivity at n+1
    DataIndex EDGE_SCALED_CONDUCTIVITY;   //!< Effective scaled edge conductivity at n+1

    //! Per-field vector quantities
    vector<DataIndex> VEL;              //!< Velocity at time n+1
    vector<DataIndex> VELN;             //!< Velocity at time n
    vector<DataIndex> VELNN;            //!< A copy of velocity at time n
    vector<DataIndex> GHOST_VEL;        //!< Ghost velocity
    vector<DataIndex> VEL_BC;           //!< Buffer for velocity BCs for proj
//#####DEBUG -- TEMPORARY REMOVE THIS
// Jozsef need to remove this + memory allocation
    vector<DataIndex> DRAG;             //!< Drag force
    vector<DataIndex> LIFT;             //!< Lift force
//#####DEBUG -- TEMPORARY REMOVE THIS

    //! Per-field temporary work storage for edge and element variables
    vector<DataIndex> ELEM_TENSOR;      //!< Work array for elem tensor

    //! Mixture quantities
    DataIndex MIXMOM;                   //!< Mixture momentum
    DataIndex DUALEDGE_MIXMOM;          //!< Dual-edge mixture momentum

    //! Multi-field specific vectors
    DataIndex ELEM_GRADIENT;            //!< Elem-centered gradient at time n+1
    DataIndex EDGE_GRADIENT;            //!< Edge-centered gradient at time n+1

    //! Scratch
    DataIndex TMP3;
    DataIndex TMP4;
    DataIndex TMP5;
    DataIndex TMP_VEC2;
    //@}

    //! Number of material/fluid fields present
    int m_Nfield;

    //! Block size for momentum exchange
    int MX_BLKSIZE;

    //! Local storage for lapack factorization in momentum exchange
    int* m_ipiv;

    //! Timers
    TimerIndex VOLFRAC_TIME;

    //! BC containers
    CCMFBCMap<CCINSDensityBC>          m_densityBCs;
    CCMFBCMap<CCINSHeatFluxBC>         m_heatFluxBCs;
    CCMFBCMap<CCINSMassFlowBC>         m_massFlowBCs;
    CCMFBCMap<CCINSMassFluxBC>         m_massFluxBCs;
    CCMFBCMap<CCINSSpeciesMassFracBC>  m_massFracBCs;
    CCMFBCMap<CCINSSymmVelBC>          m_symmVelBCs;
    CCMFBCMap<CCINSTemperatureBC>      m_temperatureBCs;
    CCMFBCMap<CCINSTractionBC>         m_tractionBCs;
    CCMFBCMap<CCINSVelocityBC>         m_velocityBCs;
    CCMFBCMap<CCMFVolFracBC>           m_volFracBCs;
    CCMFBCMap<CCINSVolumeFlowBC>       m_volumeFlowBCs;

    //! Body forces. The net body force is the sum of all body forces.
    CCMFForceMap<CCINSBodyForce>       m_body_force;
    CCMFForceMap<CCINSBoussinesqForce> m_boussinesq_force;
    CCMFForceMap<CCINSGravityForce>    m_gravity_force;

    //! Momentum exchange
    bool m_MomEx;                                     //!< Do momentum exchange

    //! Drag:
    //!< Pairs of <drag-pointers, field-id-pairs> for all fields
    CCMFSourceMap< CCMFDrag, pair<int,int> > m_drag;  //!< Used for accounting
    vector< Drag > m_fieldDrag;                       //!< Used for computation
    vector< vector<Real*> > m_dragCoeff;              //!< Drag coefficients

    //! Lift:
    //!< Pairs of <lift-pointers, field-id-pairs> for all fields
    CCMFSourceMap< CCMFLift, pair<int,int> > m_lift;  //!< Used for accounting
    vector< Lift > m_fieldLift;                       //!< Used for computation
    vector< vector<Real*> > m_liftCoeff;              //!< Lift coefficients

    vector< Real* > m_mxRHS;         //!< RHS in momentum-exchange linear system
    vector< vector<Real*> > m_mxLHS; //!< LHS in momentum-exchange linear system

    //! Empty porous drag object for momentum interface only.
    vector<CCINSPorousDrag*> m_porous_drag;

    //! Volumetric heat sources
    CCMFSourceMap<CCINSHeatSource> m_heat_source;

    //! Surface chemistry interfaces
    CCMFSourceMap<CCINSSurfChem> m_surfchem;

    //! Simple IC's
    ScalMap<CCINSICPackage> m_ics;

    //! Transport solvers
    vector<CCMFMomentum*> m_Momentum;
    vector<CCINSEnergy*>  m_Energy;
    vector<CCMFVolFrac*>  m_VolFrac;

    CCINSTurbulence* m_Turbulence;

    //! Momentum equation vectors of unknowns
    vector<LAVector*> m_momentum_x;
    vector<LAVector*> m_momentum_y;
    vector<LAVector*> m_momentum_z;

    //! Vector of transport variable DataIndex array structs
    vector<CCINSTransportVar> m_di;

    // matrix for volume fraction solve
    LASolver* m_volfrac_solver;
    LAMatrix* m_volfrac_A;
};

}
#endif // CCMultiField_h
