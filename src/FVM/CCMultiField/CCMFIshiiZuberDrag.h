//******************************************************************************
//! \file    src/FVM/CCMultiField//CCMFIshiiZuberDrag.h
//! \author  Jozsef Bakosi, Mark A. Christon
//! \date    Thu May 30 11:50:35 2013
//! \brief   Ishii-Zuber momentum drag for multiphase flow
//******************************************************************************
#ifndef CCMFIshiiZuberDrag_h
#define CCMFIshiiZuberDrag_h

#include <HydraTypes.h>
#include <CCMFDrag.h>

namespace Hydra {

//! Ishii-Zuber drag momentum exchange model for multiphase flow
class CCMFIshiiZuberDrag : public CCMFDrag {

  public:

    //! \name Constructor/Destructor
    //@{
    //! Constructor
    //!   \param[in]  param    Drag parameters, see struct DragParam
    //!   \param[in]  pair     Field ID pair drag acts between
    //!   \param[in]  nfield   Total number of fields
    //!   \param[in]  ndim     Number of spatial dimensions
    //!   \details Note that nfield and ndim are references as they do not have
    //!          the correct values at parsing (when this constructor is called)
    CCMFIshiiZuberDrag(const DragParam& param,
                       int pair,
                       const int& nfield,
                       const int& ndim) :
                       CCMFDrag(param, pair, nfield, ndim) {}
    virtual ~CCMFIshiiZuberDrag() {}
    //@}

    //! Return string describing drag type (for echo)
    virtual string type() const { return "Ishii-Zuber"; }

    //! Echo one-liner info
    virtual void echo(ostream& ofs) const {
      CCMFDrag::echo(ofs);
      ofs << "  " << setw(14) << right << "n/a"
          << "  " << setw(10) << "computed";
    }

    //! Compute and return drag force operator:
    //! i.e., 3/8 * bubble_radius * volfrac * rho * drag_coeff * abs(rel_vel)
    //!   \param[in]     id1    Field id 1
    //!   \param[in]     id2    Field id 2
    //!   \param[in]     bo     Block offset to access physical variable
    //!   \param[in]     bs     Block size
    //!   \param[inout]  D      Computed force operator
    virtual void calcForceOp(int /* id1 */,
                             int /* id2 */,
                             int /* bo */,
                             int /* bs */,
                             vector< vector<Real*> >& /* D */) { }

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCMFIshiiZuberDrag(const CCMFIshiiZuberDrag&);
    CCMFIshiiZuberDrag& operator=(const CCMFIshiiZuberDrag&);
    //@}

};

}

#endif
