//******************************************************************************
//! \file    src/FVM/CCMultiField/CCMFConstantLift.h
//! \author  Jozsef Bakosi, Mark A. Christon
//! \date    Thu May 30 11:50:35 2013
//! \brief   Constant momentum lift for multiphase flow
//******************************************************************************
#ifndef CCMFConstantLift_h
#define CCMFConstantLift_h

#include <iomanip>

#include <HydraTypes.h>
#include <CCMFLift.h>

namespace Hydra {

//! Constant lift momentum exchange model for multiphase flow
class CCMFConstantLift : public CCMFLift {

  public:

    //! \name Constructor/Destructor
    //@{
    //!   \param[in]  param    Lift parameters, see struct LiftParam
    //!   \param[in]  pair     Field ID pair lift acts between
    //!   \param[in]  nfield   Total number of fields
    //!   \param[in]  ndim     Number of spatial dimensions
    //!   \details Note that nfield and ndim are references as they do not have
    //!          the correct values at parsing (when this constructor is called)
    CCMFConstantLift(const LiftParam& param,
                     int pair,
                     const int& nfield,
                     const int& ndim) : CCMFLift(param, pair, nfield, ndim),
                                        Cl(param.coeff) {}
    virtual ~CCMFConstantLift() {}
    //@}

    //! Return string describing lift type (for echo)
    virtual std::string type() const { return "Constant"; }

    //! Echo one-liner info
    virtual void echo(ostream& ofs) const {
      CCMFLift::echo(ofs);
      ofs << "  " << right << setw(10) << Cl;
    }

    //! Compute and return lift force operator:
    //! i.e., lift_coeff * volfrac * rho * vel x vorticity
    //!   \param[in]     id1    Field id 1
    //!   \param[in]     id2    Field id 2
    //!   \param[in]     bo     Block offset to access physical variable
    //!   \param[in]     bs     Block size
    //!   \param[inout]  L      Computed force operator
    virtual void calcForceOp(int id1,
                             int id2,
                             int bo,
                             int bs,
                             vector< vector<Real*> >& L);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCMFConstantLift(const CCMFConstantLift&);
    CCMFConstantLift& operator=(const CCMFConstantLift&);
    //@}

    const Real Cl;              //!< Constant lift coefficient

};

}

#endif
