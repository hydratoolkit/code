//******************************************************************************
//! \file    src/FVM/CCMultiField/CCMFBCMap.h
//! \author  Jozsef Bakosi, Mark A. Christon
//! \date    Fri Sep 28 11:50:34 2011
//! \brief   Map of vectors of boundary conditions
//******************************************************************************
#ifndef CCMFBCMap_h
#define CCMFBCMap_h

#include <VecMap.h>

namespace Hydra {

//! Map of vectors of pointer to CCINS BC types
template<class T>
class CCMFBCMap : public VecMap<int, T> {

    using VecMap<int, T>::m_cit;
    using VecMap<int, T>::m_mapvec;
    typedef typename VecMap<int, T>::vector_type vector_type;

  public:

    //! Add a boundary condition to the map
    //!   \param[in] bc     BCPackage to insert
    //!   \param[in] field  vector<int> with field ids this bc relates to
    void addBC(const BCPackage& bc,
               const vector<int>& field) {
      const int nfields = field.size();
      for (int i=0; i<nfields; i++)
        m_mapvec[field[i]].push_back(new T(bc,0));
    }

    //! Echo one liner info for boundary condition
    //!   \param[in]  bc       Boundary condition to echo
    //!   \param[out] ofs      Output stream to echo to
    void echoBC(T* const bc,
                const int field,
                ostream& ofs) {
      string bctype = "NULL";
      switch(bc->getType()) {
      case BC_CONSTANT:
        bctype = "CONSTANT";
        break;
      case BC_TIME_DEPENDENT:
        bctype = "TIME DEPENDENT";
        break;
      case BC_SPACE_DEPENDENT:
        bctype = "SPACE DEPENDENT";
        break;
      case BC_SPACETIME_DEPENDENT:
        bctype = "SPACE-TIME DEPENDENT";
        break;
      case BC_USER_DEFINED:
        bctype = "USER-DEFINED";
        break;
      }
      ofs << "\t"
          << setw(10) << bc->getId()      << "  "
          << setw( 5) << field+1          << "  "
          << setw(23) << bctype           << "  "
          << setw( 5) << bc->getTableId() << "  "
          << setw(10) << bc->getAmp()     << " "
          << setw( 5) << bc->getFieldId()
          << endl;
    }

    //! Echo one liner info for boundary condition
    //!   \param[in]  dirLabel Direction label
    //!   \param[in]  bc       Boundary condition to echo
    //!   \param[out] ofs      Output stream to echo to
    void echoBC(const string& dirLabel,
                T* const bc,
                const int field,
                ostream& ofs) {
      string bctype = "NULL";
      switch(bc->getType()) {
      case BC_CONSTANT:
        bctype = "CONSTANT";
        break;
      case BC_TIME_DEPENDENT:
        bctype = "TIME DEPENDENT";
        break;
      case BC_SPACE_DEPENDENT:
        bctype = "SPACE DEPENDENT";
        break;
      case BC_SPACETIME_DEPENDENT:
        bctype = "SPACE-TIME DEPENDENT";
        break;
      case BC_USER_DEFINED:
        bctype = "USER-DEFINED";
        break;
      }
      ofs << "\t"
          << setw(10) << bc->getId()      << "  "
          << setw( 5) << field+1          << "  "
          << setw( 4) << dirLabel         << "  "
          << setw(23) << bctype           << "  "
          << setw( 5) << bc->getTableId() << "  "
          << setw(10) << bc->getAmp()     << " "
          << setw( 5) << bc->getFieldId()
          << endl;
    }

    //! Echo all boundary conditions
    //!   \param[out] ofs      Output stream to echo to
    void echoBCs(ostream& ofs) {
      for (m_cit=m_mapvec.begin(); m_cit!=m_mapvec.end(); m_cit++) {
        const vector_type* v = &m_cit->second;
        const int numvec = v->size();
        for (int i=0; i<numvec; i++)
          echoBC((*v)[i], m_cit->first, ofs);
      }
    }

    //! Echo all boundary conditions (for vector types)
    //!   \param[out] ofs      Output stream to echo to
    void echoVecBCs(ostream& ofs) {
      for (m_cit=m_mapvec.begin(); m_cit!=m_mapvec.end(); m_cit++) {
        const vector_type* v = &m_cit->second;
        const int numvec = v->size();
        for (int i=0; i<numvec; i++) {
          string labeldir = " ";
          switch((*v)[i]->getDirection()) {
            case GLOBAL_XDIR: labeldir = "X"; break;
            case GLOBAL_YDIR: labeldir = "Y"; break;
            case GLOBAL_ZDIR: labeldir = "Z"; break;
          }
          echoBC(labeldir, (*v)[i], m_cit->first, ofs);
        }
      }
    }

    //! Check external edge range
    //!   \param[in]  mesh    Reference to mesh object
    //!   \param[in]  num     Number of external edges to check (e.g. Nedge_ext)
    //!   \param[out] surfid  SurfID if the edge falls onto an internal edge
    void checkExternalEdgeRange(UnsMesh& mesh, int num, int& surfid) {
      for (m_cit=m_mapvec.begin(); m_cit!=m_mapvec.end(); m_cit++) {
        const vector_type* v = &m_cit->second;
        const int numvec = v->size();
        for (int i=0; i<numvec; i++) {
          surfid = (*v)[i]->checkExternalEdges(mesh,num);
          if (surfid >= 0) break;
        }
      }
    }

};

}

#endif
