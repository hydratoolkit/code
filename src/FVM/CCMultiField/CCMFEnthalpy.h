//******************************************************************************
//! \file    src/FVM/CCMultiField/CCMFEnthalpy.h
//! \author  Jozsef Bakosi, Mark A. Christon
//! \date    Mon Jan 6 12:46:22 2011
//! \brief   Multifield Enthalpy transport class
//******************************************************************************
#ifndef CCMFEnthalpy_h
#define CCMFEnthalpy_h

#include <CCINSEnthalpy.h>

namespace Hydra {

class CCMFEnthalpy : public CCINSEnthalpy {

  public:

    //! \name Constructor/Destructor
    //@{
             CCMFEnthalpy(UnsMesh& mesh,
                          Control& control,
                          DualEdgeGradOp& edgeGradOp,
                          CCINSErrors& errors,
                          fileIO& io,
                          CCINSTransportVar& di,
                          vector<CCINSTemperatureBC*>& tempBCs,
                          vector<CCINSHeatFluxBC*>& heatfluxbc,
                          vector<CCINSHeatSource*>& heat_source,
                          vector<CCINSSurfChem*>& surfchem,
                          LASolver& transport_solver,
                          LAMatrix& transport_A,
                          LAVector& transport_b,
                          LAVector& transport_x,
                          CCINSTurbulence& turbulence,
                          CCINSAdapter& adapter);
    virtual ~CCMFEnthalpy();
    //@}

    //! Solve generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);

    virtual void assembleSystem(const CCINSIncParm& incParm);

   /*! Form transport Rhs function
    *    \param[in]     incParm Increment parameter functions
    *    \param[in,out] rhs     Pointer to the rhs vector
    */
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs=0);

    /**************************************************************************/

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCMFEnthalpy(const CCMFEnthalpy&);
    CCMFEnthalpy& operator=(const CCMFEnthalpy&);
    //@}

};

}

#endif // CCMFEnthalpy_h
