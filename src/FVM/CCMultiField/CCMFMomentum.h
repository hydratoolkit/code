//******************************************************************************
//! \file    src/FVM/CCMultiField/CCMFMomentum.h
//! \author  Jozsef Bakosi
//! \date    Mon Jan 14 12:46:22 2011
//! \brief   Solve the momentum transport equations for multi-field
//******************************************************************************
#ifndef CCMFMomentum_h
#define CCMFMomentum_h

#include <CCINSMomentum.h>

namespace Hydra {

class Category;
class CCINSBodyForce;
class CCINSGravityForce;
class CCINSSymmVelBC;
class CCINSVelocityBC;
class CCINSTractionBC;
class CCINSPassiveOutflowBC;
class CCINSPressureOutflowBC;
class CCINSMassFlowBC;
class CCINSMassFluxBC;
class CCINSVolumeFlowBC;
class CCINSPorousDrag;
class CCMFDrag;
class CCINSTurbulence;

  //! Solve the momentum transport equations
  class CCMFMomentum : public CCINSMomentum {

  public:

    //! \name Constructor/Destructor
    //@{
    CCMFMomentum(UnsMesh& mesh,
                 Control& control,
                 DualEdgeGradOp& edgeGradOp,
                 CCINSErrors& errors,
                 fileIO& io,
                 CCINSTransportVar& di,
                 map<int,int>& transportSpecies,
                 vector<CCINSSymmVelBC*>& symmvelbc,
                 vector<CCINSVelocityBC*>& velocitybc,
                 vector<CCINSTractionBC*>& tractionbc,
                 vector<CCINSPassiveOutflowBC*>& passiveoutflowbc,
                 vector<CCINSPressureOutflowBC*>& pressureoutflowbc,
                 vector<CCINSMassFlowBC*>& massflowbc,
                 vector<CCINSMassFluxBC*>& massfluxbc,
                 vector<CCINSVolumeFlowBC*>& volumeflowbc,
                 vector<CCINSBodyForce*>& body_force,
                 vector<CCINSBoussinesqForce*>& boussinesq_force,
                 vector<CCINSGravityForce*>& gravity_force,
                 vector<CCINSPorousDrag*>& porous_drag,
                 LASolver& momentum_solver,
                 LAMatrix& momentum_A,
                 LAVector& momentum_diagA,
                 LAVector& momentum_diagA_temp,
                 LAVector& momentum_bx,
                 LAVector& momentum_by,
                 LAVector& momentum_bz,
                 LAVector& momentum_x,
                 LAVector& momentum_y,
                 LAVector& momentum_z,
                 CCINSTurbulence& turbulence,
                 CCINSAdapter& adapter);
    virtual ~CCMFMomentum() {}
    //@}

    //! Virtual form transport Rhs function
    //!   \param[in] CCINSIncParm Time increment parameters
    //!   \param[in] SourceMode   Time-weighting/time-level for source terms
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs=0);

    //! Solve momentum equations
    //!   \param[in] incParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);

    //! Assemble one momentum equation to fill matrix
    //!   \param[in] incParm Time increment parameters
    virtual void assembleSystem(const CCINSIncParm& incParm, const int dim);

    //! Assemble direction independent part of momentum equations
    //!   \param[in] incParm Time increment parameters
    Real* assembleInitial(const CCINSIncParm& incParm);

    //! Assemble edge gradient operator for momentum equations
    //!   \param[in] incParm Time increment parameters
    void assembleEdgeGradOp(const CCINSIncParm& incParm, const int dim,
                            Real* aedgemu, LAVector* m_momentum_comp,
                            LAVector* m_momentum_bcomp);

    //! Apply body force terms to momentum
    void applyBodyForces(const Real thetaFn, const Real thetaFnp1,
                         const Real* arho, CVector& rhs,
                         const FVMCCINS::SourceMode mode);

    //! Apply body force terms to partial acceleration
    void applyBodyForces(CVector& rhs);

  protected:

    //! Form transport Rhs function
    //!   \param[in] incParm Reference to time increment parameters
    //!   \param[in,out] rhs Pointer to the rhs vector
    void m_formRhs(const CCINSIncParm& incParm,
                   const FVMCCINS::SourceMode mode);

    //! Add the momentum advection terms to the RHS
    //!   \param[out] rhs          Reference to the rhs vector
    //!   \param[in]  DUALEDGE_VEL Data index for face velocity vector
    //!   \param[in]  VEL          Data index for cell-centered velocity vector
    //!   \param[in]  mode         Mode of advection operator treatment
    void addAdvectiveRhs(CVector& rhs,
                         DataIndex DUALEDGE_VEL,
                         DataIndex VEL,
                         FVMCCINS::SourceMode mode=FVMCCINS::TIME_LEVEL_WEIGHTED);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCMFMomentum(const CCMFMomentum&);
    CCMFMomentum& operator=(const CCMFMomentum&);
    //@}

    //! Equation names for error reporting
    vector<string> m_eqname;

    //! Prefix for creation of an error message
    vector<string> m_warn_prefix;

    //! Error codes for convergence error reporting
    vector<CCINSSolverError> m_error_codes;

    //! x,y, and z component of the momentum solution
    vector<LAVector*> m_momentum_comp;

    //! x,y, and z component of the momentum rhs
    vector<LAVector*> m_momentum_bcomp;
  };

}

#endif // CCMFMomentum_h
