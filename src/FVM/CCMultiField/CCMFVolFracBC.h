//******************************************************************************
//! \file    src/FVM/CCMultiField/CCMFVolFracBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Volume fraction BCs
//******************************************************************************
#ifndef CCMFVolFracBC_h
#define CCMFVolFracBC_h


#include <CCSideSetBC.h>

namespace Hydra {

// Forward declarations
struct BCPackage;
class UnsMesh;


//! Temperature boundary condition based on surfaces
class CCMFVolFracBC : public CCSideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCMFVolFracBC(const BCPackage& bc, int offset);
    virtual ~CCMFVolFracBC();
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCMFVolFracBC(const CCMFVolFracBC&);
    CCMFVolFracBC& operator=(const CCMFVolFracBC&);
    //@}

};

}

#endif // CCMFVolFracBC_h
