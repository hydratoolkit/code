//******************************************************************************
//! \file    src/FVM/CCMultiField/CCMFMomentumExchange.h
//! \author  Jozsef Bakosi, Mark A. Christon
//! \date    Thu May 30 11:50:35 2013
//! \brief   Base class for momentum exchange terms
//******************************************************************************
#ifndef MomentumExchange_h
#define MomentumExchange_h

#include <HydraTypes.h>
#include <Source.h>

namespace Hydra {

//! Enum facilitating the parsing and associating a field ID as disperse field
enum DisperseID { SMALLER=0, LARGER=1 };

//! Base class for momentum exchange source terms
class MomentumExchange : public Source {

  public:

    //! \name Constructor/Destructor
    //@{
    MomentumExchange(int setId,
                     int tblid,
                     Real amp,
                     const int& nfield,
                     const int& ndim) :
       Source(tblid, amp, setId), m_Nfield(nfield), m_Ndim(ndim) {}
    virtual ~MomentumExchange() {}
    //@}

  protected:

    const int& m_Nfield;                //!< Total number of fields
    const int& m_Ndim;                  //!< Number of spatial dimensions

  private:

    //! Don't permit copy or assignment operators
    //@{
    MomentumExchange(const MomentumExchange&);
    MomentumExchange& operator=(const MomentumExchange&);
    //@}

};

}

#endif
