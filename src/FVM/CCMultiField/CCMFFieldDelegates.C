//******************************************************************************
//! \file    src/FVM/CCMultiField/CCMFFieldDelegates.C
//! \author  Jozsef Bakosi, Mark A. Christon
//! \date    Mon Sep 24 13:22:00 2012
//! \brief   Field delegates for multifield flow
//******************************************************************************

#include <sstream>

using namespace std;

#include <CCMultiField.h>
#include <CCINSDensityBC.h>
#include <CCINSVelocityBC.h>
#include <CCMFVolFracBC.h>
#include <CCINSTemperatureBC.h>

namespace Hydra {

// Class wrappers for output delegates

OUTPUT_DELEGATE_IMPL(CCMultiField,
                     writeNodeDensityField,
                     CCMultiFieldNodeDensityField)

OUTPUT_DELEGATE_IMPL(CCMultiField,
                     writeNodeVelocityVectorField,
                     CCMultiFieldNodeVelocityVectorField)

OUTPUT_DELEGATE_IMPL(CCMultiField,
                     writeElemVorticityVectorField,
                     CCMultiFieldElemVorticityVectorField)

OUTPUT_DELEGATE_IMPL(CCMultiField,
                     writeNodeVorticityVectorField,
                     CCMultiFieldNodeVorticityVectorField)

OUTPUT_DELEGATE_IMPL(CCMultiField,
                     writeNodeVolFracField,
                     CCMultiFieldNodeVolFracField)

OUTPUT_DELEGATE_IMPL(CCMultiField,
                     writeNodeTemperatureField,
                     CCMultiFieldNodeTemperatureField)

OUTPUT_DELEGATE_IMPL(CCMultiField,
                     writeNodeIntEnergyField,
                     CCMultiFieldNodeIntEnergyField)

OUTPUT_DELEGATE_IMPL(CCMultiField,
                     writeNodeEnthalpyField,
                     CCMultiFieldNodeEnthalpyField)

OUTPUT_DELEGATE_IMPL(CCMultiField,
                     writeElemMixtureDensityField,
                     CCMultiFieldElemMixtureDensityField)

OUTPUT_DELEGATE_IMPL(CCMultiField,
                     writeNodeMixtureDensityField,
                     CCMultiFieldNodeMixtureDensityField)

OUTPUT_DELEGATE_IMPL(CCMultiField,
                     writeElemMixtureVelocityVectorField,
                     CCMultiFieldElemMixtureVelocityVectorField)

OUTPUT_DELEGATE_IMPL(CCMultiField,
                     writeNodeMixtureVelocityVectorField,
                     CCMultiFieldNodeMixtureVelocityVectorField)

OUTPUT_DELEGATE_IMPL(CCMultiField,
                     writeNodeDragVectorField,
                     CCMultiFieldNodeDragVectorField)

OUTPUT_DELEGATE_IMPL(CCMultiField,
                     writeNodeLiftVectorField,
                     CCMultiFieldNodeLiftVectorField)

}

using namespace Hydra;

int
CCMultiField::fieldID(const string& name)
/*******************************************************************************
Routine: fieldID - get field ID from multi-field variable name
Author : J. Bakosi
*******************************************************************************/
{
  string::size_type found = name.find_last_of("_");
  if (found == string::npos) {
    p0cout << endl
         << "\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
         << endl
         << "\tField ID cannot be determined for " << name
         << "\n\t name must end with an underscore + field ID." << endl
         << "\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
         << endl;
  }
  stringstream ss(name.substr(found+1));
  int field;
  ss >> field;
  return field-1;  // field IDs start from 0, but presented to the user from 1
}

void
CCMultiField::projectVelToNodes(Real* evar,
                                const vector<CCINSVelocityBC*>& bc,
                                const GlobalDirection dir,
                                Real* nvar)
/*******************************************************************************
Routine: projectVelToNodes - Project elem velocity component to nodes, appl. BCs
Author : J. Bakosi
*******************************************************************************/
{
  Real* work  = mesh->getVariable<Real>(TMP1);
  int*  iflag = mesh->getVariable<int>(TMP2);

  memset(nvar, 0, Nnp*sizeof(Real));
  memset(work, 0, Nnp*sizeof(Real));
  memset(iflag,0, Nnp*sizeof(int ));

  formVolProjectionID(evar, nvar, work);

  int nbc = bc.size();
  for (int i=0; i<nbc; ++i) {
    if (bc[i]->getDirection() == dir)
      bc[i]->zeroInvDistOp(*mesh, nvar, work, iflag);
  }

  zeroInvDistOpProcBoundaryNodes(nvar, work, iflag);

  for (int i=0; i<nbc; ++i) {
    if (bc[i]->getDirection() == dir)
      bc[i]->formInvDistOp(*mesh, m_time, nvar, work);
  }

  solveProjID(nvar, work, SEND_BUF, RECV_BUF);
}

void
CCMultiField::writeNodeDensityField(const OutputDelegateKey& key,
                                    int plnum, int varId)
/*******************************************************************************
Routine: writeNodeDensityField - write out nodal density w. BC's enforced
Author : J. Bakosi
*******************************************************************************/
{
  int field = fieldID(key.variable);
  Real* node_rho = mesh->getVariable<Real>(TMP2);

  projectToNodes(DENSITY[field], TMP4, TMP1, SEND_BUF, RECV_BUF,
                 m_densityBCs.find(field), m_time, TMP2);

  // Write the field
  string name = key.variable;
  io->writeNodeScalarField(mesh, plnum, varId, name, node_rho);
}

void
CCMultiField::writeNodeVelocityVectorField(const OutputDelegateKey& key,
                                           int plnum, int varId)
/*******************************************************************************
Routine: writeNodeVelocityVectorField - write out nodal velocity
Author : J. Bakosi
*******************************************************************************/
{
  CVector elem_vel  = mesh->getCVector(key.vid);
  CVector node_vel = mesh->getCVector(TMP_VEC1, Nnp);

  int field = fieldID(key.variable);

  projectVelToNodes(
    elem_vel.X, m_velocityBCs.find(field), GLOBAL_XDIR, node_vel.X);
  projectVelToNodes(
    elem_vel.Y, m_velocityBCs.find(field), GLOBAL_YDIR, node_vel.Y);
  projectVelToNodes(
    elem_vel.Z, m_velocityBCs.find(field), GLOBAL_ZDIR, node_vel.Z);

  // Write out the field
  string name = key.variable;
  io->writeNodeVectorField(mesh, plnum, varId, name, node_vel);
}

void
CCMultiField::writeElemVorticityVectorField(const OutputDelegateKey& key,
                                            int plnum, int varId)
/*******************************************************************************
Routine: writeElemVorticityVectorField - write out elem vorticity
Author : J. Bakosi
*******************************************************************************/
{
  int field = fieldID(key.variable);

  CVector vort = mesh->getCVector(TMP_VEC1,Nel);
  CTensor vgrad = mesh->getCTensor(ELEM_TENSOR[field]);

  // Compute the element-centered vorticity field
  calcVorticity(vort, vgrad);

  // Write the field
  string name = key.variable;
  io->writeElemVectorField(mesh, plnum, varId, name, vort);
}

void
CCMultiField::writeNodeVorticityVectorField(const OutputDelegateKey& key,
                                            int plnum, int varId)
/*******************************************************************************
Routine: writeNodeVorticityVectorField - write out node vorticity
Author : J. Bakosi
*******************************************************************************/
{
  int field = fieldID(key.variable);

  CVector vort     = mesh->getCVector(ELEM_RHS,Nel);
  CVector node_var = mesh->getCVector(TMP_VEC1,Nnp);
  Real*   work     = mesh->getVariable<Real>(TMP1);
  CTensor vgrad    = mesh->getCTensor(ELEM_TENSOR[field]);

  // Compute the element-centered vorticity field
  calcVorticity(vort, vgrad);

  memset(node_var.X, 0, Ndim*Nnp*sizeof(Real));

  // For x component!
  memset(work, 0, Nnp*sizeof(Real));
  formVolProjectionID(vort.X, node_var.X, work);
  solveProjID(node_var.X,work,SEND_BUF,RECV_BUF);

  // For y component!
  memset(work, 0, Nnp*sizeof(Real));
  formVolProjectionID(vort.Y, node_var.Y, work);
  solveProjID(node_var.Y,work,SEND_BUF,RECV_BUF);

  // For z component!
  memset(work, 0, Nnp*sizeof(Real));
  formVolProjectionID(vort.Z, node_var.Z, work);
  solveProjID(node_var.Z,work,SEND_BUF,RECV_BUF);

  // Write the field
  string name = key.variable;
  io->writeNodeVectorField(mesh, plnum, varId, name, node_var);
}

void
CCMultiField::writeNodeVolFracField(const OutputDelegateKey& key,
                                    int plnum, int varId)
/*******************************************************************************
Routine: writeNodeVolFracField - write out nodal volume fraction
Author : J. Bakosi
*******************************************************************************/
{
  Real* vf   = mesh->getVariable<Real>(key.vid);
  Real* nvar = mesh->getVariable<Real>(TMP_VEC1);
  Real* work = mesh->getVariable<Real>(TMP1);
  int* iflag = mesh->getVariable<int>(TMP2);

  int field = fieldID(key.variable);

  memset(nvar, 0, Nnp*sizeof(Real));
  memset(work, 0, Nnp*sizeof(Real));
  formVolProjectionID(vf, nvar, work);

  vector<CCMFVolFracBC*>& vbc = m_volFracBCs.find(field);
  int nbc = vbc.size();
  for (int i=0; i<nbc; ++i) {
    vbc[i]->zeroInvDistOp(*mesh, nvar, work, iflag);
  }
  zeroInvDistOpProcBoundaryNodes(nvar, work, iflag);
  for (int i=0; i<nbc; ++i) {
    vbc[i]->formInvDistOp(*mesh, m_time, nvar, work);
  }
  solveProjID(nvar, work, SEND_BUF, RECV_BUF);

  // Write out the field
  string name = key.variable;
  io->writeNodeScalarField(mesh, plnum, varId, name, nvar);
}

void
CCMultiField::writeNodeTemperatureField(const OutputDelegateKey& key,
                                        int plnum, int varId)
/*******************************************************************************
Routine: writeNodeTemperatureField - write out nodal temperature
Author : J. Bakosi
*******************************************************************************/
{
  Real* temp = mesh->getVariable<Real>(key.vid);
  Real* nvar = mesh->getVariable<Real>(TMP_VEC1);
  Real* work = mesh->getVariable<Real>(TMP1);
  int* iflag = mesh->getVariable<int>(TMP2);

  int field = fieldID(key.variable);

  memset(nvar,  0, Nnp*sizeof(Real));
  memset(work,  0, Nnp*sizeof(Real));
  memset(iflag, 0, Nnp*sizeof(int ));

  formVolProjectionID(temp, nvar, work);
  formEnergySurfProjID(TEMPERATURE[field],
                       EDGE_CONDUCTIVITY,
                       m_temperatureBCs.find(field),
                       m_heatFluxBCs.find(field),
                       FVMCCINS::TOTEMPERATURE,
                       nvar,
                       work,
                       iflag);
  solveProjID(nvar, work, SEND_BUF, RECV_BUF);

  // Write out the field
  string name = key.variable;
  io->writeNodeScalarField(mesh, plnum, varId, name, nvar);
}

void
CCMultiField::writeNodeIntEnergyField(const OutputDelegateKey& key,
                                      int plnum, int varId)
/*******************************************************************************
Routine: writeNodeIntEnergyField - write out nodal internal energy
Author : J. Bakosi
*******************************************************************************/
{
  Real* temp = mesh->getVariable<Real>(key.vid);
  Real* nvar = mesh->getVariable<Real>(TMP_VEC1);
  Real* work = mesh->getVariable<Real>(TMP1);
  int* iflag = mesh->getVariable<int>(TMP2);

  int field = fieldID(key.variable);

  memset(nvar,  0, Nnp*sizeof(Real));
  memset(work,  0, Nnp*sizeof(Real));
  memset(iflag, 0, Nnp*sizeof(int ));

  formVolProjectionID(temp, nvar, work);
  formEnergySurfProjID(TEMPERATURE[field],
                       EDGE_CONDUCTIVITY,
                       m_temperatureBCs.find(field),
                       m_heatFluxBCs.find(field),
                       FVMCCINS::TOINTENERGY,
                       nvar,
                       work,
                       iflag);
  solveProjID(nvar, work, SEND_BUF, RECV_BUF);

  // Write out the field
  string name = key.variable;
  io->writeNodeScalarField(mesh, plnum, varId, name, nvar);
}

void
CCMultiField::writeNodeEnthalpyField(const OutputDelegateKey& key,
                                     int plnum, int varId)
/*******************************************************************************
Routine: writeNodeEnthalpyField - write out nodal enthalpy
Author : J. Bakosi
*******************************************************************************/
{
  Real* temp = mesh->getVariable<Real>(key.vid);
  Real* nvar = mesh->getVariable<Real>(TMP_VEC1);
  Real* work = mesh->getVariable<Real>(TMP1);
  int* iflag = mesh->getVariable<int>(TMP2);

  int field = fieldID(key.variable);

  memset(nvar,  0, Nnp*sizeof(Real));
  memset(work,  0, Nnp*sizeof(Real));
  memset(iflag, 0, Nnp*sizeof(int ));

  formVolProjectionID(temp, nvar, work);
  formEnergySurfProjID(TEMPERATURE[field],
                       EDGE_CONDUCTIVITY,
                       m_temperatureBCs.find(field),
                       m_heatFluxBCs.find(field),
                       FVMCCINS::TOENTHALPY,
                       nvar,
                       work,
                       iflag);
  solveProjID(nvar, work, SEND_BUF, RECV_BUF);

  // Write out the field
  string name = key.variable;
  io->writeNodeScalarField(mesh, plnum, varId, name, nvar);
}

void
CCMultiField::writeElemMixtureDensityField(const OutputDelegateKey& key,
                                           int plnum, int varId)
/*******************************************************************************
Routine: writeElemMixtureDensityField - write out elem mixture density
Author : J. Bakosi
*******************************************************************************/
{
  // Compute mixture density
  Real* mixrho = mesh->getVariable<Real>(TMP2);
  memset(mixrho, 0, Nel*sizeof(Real));
  for (int j=0; j<m_Nfield; ++j) {
    Real* rho = mesh->getVariable<Real>(DENSITY[j]);
    Real* vf = mesh->getVariable<Real>(VOLFRAC[j]);
    for (int i=0; i<Nel; ++i) {
      mixrho[i] += vf[i]*rho[i];
    }
  }

  // Write the field
  string name = key.variable;
  io->writeElemScalarField(mesh, plnum, varId, name, mixrho);
}

void
CCMultiField::writeNodeMixtureDensityField(const OutputDelegateKey& key,
                                           int plnum, int varId)
/*******************************************************************************
Routine: writeNodeMixtureDensityField - write out node mixture density
Author : J. Bakosi
*******************************************************************************/
{
  // Compute mixture density

  Real* mixrho = mesh->getVariable<Real>(TMP_VEC1);
  memset(mixrho, 0, Nnp*sizeof(Real));

  for (int j=0; j<m_Nfield; ++j) {
    // Project elem density and volfrac fields to nodes applying BCs
    projectToNodes(DENSITY[j], TMP4, TMP1, SEND_BUF, RECV_BUF,
                   m_densityBCs.find(j), m_time, TMP2);
    projectToNodes(VOLFRAC[j], TMP4, TMP1, SEND_BUF, RECV_BUF,
                   m_volFracBCs.find(j), m_time, TMP3);
    // Compute mixture density in nodes
    Real* rho = mesh->getVariable<Real>(TMP2);
    Real* vf  = mesh->getVariable<Real>(TMP3);
    for (int i=0; i<Nnp; ++i) {
      mixrho[i] += vf[i]*rho[i];
    }
  }

  // Write the field
  string name = key.variable;
  io->writeNodeScalarField(mesh, plnum, varId, name, mixrho);
}

void
CCMultiField::writeElemMixtureVelocityVectorField(const OutputDelegateKey& key,
                                                  int plnum, int varId)
/*******************************************************************************
Routine: writeElemMixtureVelocityVectorField - write out elem mixture velocity
Author : J. Bakosi
*******************************************************************************/
{
  // Compute mixture density
  Real* mixrho = mesh->getVariable<Real>(TMP2);
  memset(mixrho, 0, Nel*sizeof(Real));
  for (int j=0; j<m_Nfield; ++j) {
    Real* rho = mesh->getVariable<Real>(DENSITY[j]);
    Real* vf = mesh->getVariable<Real>(VOLFRAC[j]);
    for (int i=0; i<Nel; ++i) {
      mixrho[i] += vf[i]*rho[i];
    }
  }

  // Compute mixture velocity
  CVector mixvel = mesh->getCVector(TMP_VEC1);
  CVector mixmom = mesh->getCVector(MIXMOM);
  for (int i=0; i<Nel; ++i) {
    mixvel.X[i] = mixmom.X[i] / mixrho[i];
    mixvel.Y[i] = mixmom.Y[i] / mixrho[i];
    mixvel.Z[i] = mixmom.Z[i] / mixrho[i];
  }

  // Write the field
  string name = key.variable;
  io->writeElemVectorField(mesh, plnum, varId, name, mixvel);
}

void
CCMultiField::writeNodeMixtureVelocityVectorField(const OutputDelegateKey& key,
                                                  int plnum, int varId)
/*******************************************************************************
Routine: writeNodeMixtureVelocityVectorField - write out node mixture velocity
Author : J. Bakosi
*******************************************************************************/
{
  // Compute mixture density in nodes

  // PRESSUREN is used as a temporary nodal scalar here, it will be shortly
  // overwritten in saveOldState() anyway.
  Real* mixrho = mesh->getVariable<Real>(TMP5);
  memset(mixrho, 0, Nnp*sizeof(Real));

  for (int j=0; j<m_Nfield; ++j) {
    // Project elem density and volfrac fields to nodes applying BCs
    projectToNodes(DENSITY[j], TMP4, TMP1, SEND_BUF, RECV_BUF,
                   m_densityBCs.find(j), m_time, TMP2);
    projectToNodes(VOLFRAC[j], TMP4, TMP1, SEND_BUF, RECV_BUF,
                   m_volFracBCs.find(j), m_time, TMP3);
    // Compute mixture density in nodes
    Real* rho = mesh->getVariable<Real>(TMP2);
    Real* vf  = mesh->getVariable<Real>(TMP3);
    for (int i=0; i<Nnp; ++i) {
      mixrho[i] += vf[i]*rho[i];
    }
  }

  // Compute mixture velocity in nodes

  CVector node_vel = mesh->getCVector(TMP_VEC1, Nnp);
  CVector mixvel = mesh->getCVector(TMP_VEC2, Nnp);
  memset(mixvel.X, 0, Ndim*Nnp*sizeof(Real));

  for (int j=0; j<m_Nfield; ++j) {
    // Project elem velocity to nodes applying BCs
    CVector elem_vel = mesh->getCVector(VEL[j]);
    projectVelToNodes(elem_vel.X, m_velocityBCs.find(j), GLOBAL_XDIR, node_vel.X);
    projectVelToNodes(elem_vel.Y, m_velocityBCs.find(j), GLOBAL_YDIR, node_vel.Y);
    projectVelToNodes(elem_vel.Z, m_velocityBCs.find(j), GLOBAL_ZDIR, node_vel.Z);
    // Project elem density and volfrac to nodes applying BCs
    projectToNodes(DENSITY[j], TMP4, TMP1, SEND_BUF, RECV_BUF,
                   m_densityBCs.find(j), m_time, TMP2);
    projectToNodes(VOLFRAC[j], TMP4, TMP1, SEND_BUF, RECV_BUF,
                   m_volFracBCs.find(j), m_time, TMP3);
    // Compute mixture momentum in nodes
    Real* rho = mesh->getVariable<Real>(TMP2);
    Real* vf  = mesh->getVariable<Real>(TMP3);
    for (int i=0; i<Nnp; ++i) {
      Real vr = vf[i]*rho[i];
      mixvel.X[i] += vr*node_vel.X[i];
      mixvel.Y[i] += vr*node_vel.Y[i];
      mixvel.Z[i] += vr*node_vel.Z[i];
    }
  }

  // Compute mixture velocity in nodes
  for (int i=0; i<Nnp; ++i) mixvel.X[i] /= mixrho[i];
  for (int i=0; i<Nnp; ++i) mixvel.Y[i] /= mixrho[i];
  for (int i=0; i<Nnp; ++i) mixvel.Z[i] /= mixrho[i];

  // Write out the field
  string name = key.variable;
  io->writeNodeVectorField(mesh, plnum, varId, name, mixvel);
}

void
CCMultiField::writeNodeDragVectorField(const OutputDelegateKey& key,
                                       int plnum, int varId)
/*******************************************************************************
Routine: writeNodeDragVectorField - write out nodal drag
Author : J. Bakosi
*******************************************************************************/
{
  CVector elem_drag  = mesh->getCVector(key.vid);
  CVector node_drag = mesh->getCVector(TMP_VEC1, Nnp);
  Real*   work     = mesh->getVariable<Real>(TMP1);

  memset(node_drag.X, 0, Ndim*Nnp*sizeof(Real));

  // For x component!
  memset(work, 0, Nnp*sizeof(Real));
  formVolProjectionID(elem_drag.X, node_drag.X, work);
  solveProjID(node_drag.X,work,SEND_BUF,RECV_BUF);

  // For y component!
  memset(work, 0, Nnp*sizeof(Real));
  formVolProjectionID(elem_drag.Y, node_drag.Y, work);
  solveProjID(node_drag.Y,work,SEND_BUF,RECV_BUF);

  // For z component!
  memset(work, 0, Nnp*sizeof(Real));
  formVolProjectionID(elem_drag.Z, node_drag.Z, work);
  solveProjID(node_drag.Z,work,SEND_BUF,RECV_BUF);

  // Write out the field
  string name = key.variable;
  io->writeNodeVectorField(mesh, plnum, varId, name, node_drag);
}

void
CCMultiField::writeNodeLiftVectorField(const OutputDelegateKey& key,
                                       int plnum, int varId)
/*******************************************************************************
Routine: writeNodeLiftVectorField - write out nodal lift
Author : J. Bakosi
*******************************************************************************/
{
  CVector elem_lift  = mesh->getCVector(key.vid);
  CVector node_lift = mesh->getCVector(TMP_VEC1, Nnp);
  Real*   work     = mesh->getVariable<Real>(TMP1);

  memset(node_lift.X, 0, Ndim*Nnp*sizeof(Real));

  // For x component!
  memset(work, 0, Nnp*sizeof(Real));
  formVolProjectionID(elem_lift.X, node_lift.X, work);
  solveProjID(node_lift.X,work,SEND_BUF,RECV_BUF);

  // For y component!
  memset(work, 0, Nnp*sizeof(Real));
  formVolProjectionID(elem_lift.Y, node_lift.Y, work);
  solveProjID(node_lift.Y,work,SEND_BUF,RECV_BUF);

  // For z component!
  memset(work, 0, Nnp*sizeof(Real));
  formVolProjectionID(elem_lift.Z, node_lift.Z, work);
  solveProjID(node_lift.Z,work,SEND_BUF,RECV_BUF);

  // Write out the field
  string name = key.variable;
  io->writeNodeVectorField(mesh, plnum, varId, name, node_lift);
}
