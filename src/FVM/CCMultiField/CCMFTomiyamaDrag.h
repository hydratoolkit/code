//******************************************************************************
//! \file    src/FVM/CCMultiField//CCMFTomiyamaDrag.h
//! \author  Jozsef Bakosi, Mark A. Christon
//! \date    Thu May 30 11:50:35 2013
//! \brief   Tomiyama momentum drag for multiphase flow
//******************************************************************************
#ifndef CCMFTomiyamaDrag_h
#define CCMFTomiyamaDrag_h

#include <HydraTypes.h>

namespace Hydra {

//! Tomiyama drag momentum exchange model for multiphase flow
class CCMFTomiyamaDrag : public CCMFDrag {

  public:

    //! \name Constructor/Destructor
    //@{
    //!   \param[in]  param    Drag parameters, see struct DragParam
    //!   \param[in]  pair     Field ID pair drag acts between
    //!   \param[in]  nfield   Total number of fields
    //!   \param[in]  ndim     Number of spatial dimensions
    //!   \details Note that nfield and ndim are references as they do not have
    //!          the correct values at parsing (when this constructor is called)
    CCMFTomiyamaDrag(const DragParam& param,
                     int pair,
                     const int& nfield,
                     const int& ndim) :
      CCMFDrag(param, pair, nfield, ndim),
      m_surfaceTension(param.st) {}
    virtual ~CCMFTomiyamaDrag() {}
    //@}

    //! Return string describing drag type (for echo)
    virtual string type() const { return "Tomiyama"; }

    //! Echo one-liner info
    virtual void echo(ostream& ofs) const {
      CCMFDrag::echo(ofs);
      ofs << "  " << setw(14) << right << m_surfaceTension
          << "  " << setw(10) << "computed";
    }

    //! Compute and return drag force operator:
    //! i.e., 3/8 * bubble_radius * volfrac * rho * drag_coeff * abs(rel_vel)
    //!   \param[in]     id1    Field id 1
    //!   \param[in]     id2    Field id 2
    //!   \param[in]     bo     Block offset to access physical variable
    //!   \param[in]     bs     Block size
    //!   \param[inout]  D      Computed force operator
    virtual void calcForceOp(int /* id1 */,
                             int /* id2 */,
                             int /* bo */,
                             int /* bs */,
                             vector< vector<Real*> >& /* D */) {}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCMFTomiyamaDrag(const CCMFTomiyamaDrag&);
    CCMFTomiyamaDrag& operator=(const CCMFTomiyamaDrag&);
    //@}

    const Real m_surfaceTension;
};

}

#endif
