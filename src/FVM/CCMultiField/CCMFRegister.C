//******************************************************************************
//! \file    src/FVM/CCMultiField/CCMFRegister.C
//! \author  Mark A. Christon
//! \date    Thu Nov 14 12:46:23 2012
//! \brief   CCMFRegister variable registration and related registration methods
//******************************************************************************
#include <iostream>

using namespace std;

#include <CCMultiField.h>
#include <macros.h>
#include <DualEdgeGradOp.h>
#include <DualEdgeLSOp.h>
#include <DualEdgeHybridLSOp.h>
#include <UnsPhysicsFieldDelegates.h>
#include <CCMFFieldDelegates.h>
#include <CCINSHistoryDelegates.h>
#include <CCINSSurfaceDelegates.h>

using namespace Hydra;

void
CCMultiField::freeVariable(vector<DataIndex>& id)
/*******************************************************************************
Routine: freeVariable - Free multifield variable
Author : J. Bakosi
*******************************************************************************/
{
  assert(id.size() == static_cast<unsigned int>(m_Nfield));
  for (int i=0; i<m_Nfield; i++)
    mesh->freeVariable(id[i]);
}

void
CCMultiField::initDataIndex()
/*******************************************************************************
Routine: initDataIndex - initialize all DataIndex values to unregistered
Author : Mark A. Christon
*******************************************************************************/
{
  // Storage for material property update/evaluation
  ELEM_MOL_VISCOSITYN  = UNREGISTERED;
  ELEM_TURB_VISCOSITY  = UNREGISTERED;
  EDGE_VISCOSITY       = UNREGISTERED;
  EDGE_VISCOSITYN      = UNREGISTERED;
  EDGE_CONDUCTIVITY    = UNREGISTERED;
  EDGE_SCALED_CONDUCTIVITY = UNREGISTERED;

  // Mixture quantities
  MIXMOM          = UNREGISTERED;
  DUALEDGE_MIXMOM = UNREGISTERED;

  // Multi-field specific vectors
  ELEM_GRADIENT = UNREGISTERED;
  EDGE_GRADIENT = UNREGISTERED;

  // Scratch variables
  TMP1     = UNREGISTERED;
  TMP2     = UNREGISTERED;
  TMP3     = UNREGISTERED;
  TMP4     = UNREGISTERED;
  TMP5     = UNREGISTERED;
  TMP_VEC1 = UNREGISTERED;
  TMP_VEC2 = UNREGISTERED;

  // Initialize all of the multifield DataIndex values
  for (int i=0; i<m_Nfield; i++) {
    VOLFRAC.push_back(UNREGISTERED);
    VOLFRACN.push_back(UNREGISTERED);
    EXTEDGE_VOLFRAC.push_back(UNREGISTERED);
    EXTEDGE_VOLFRACN.push_back(UNREGISTERED);

    GHOST_VAR.push_back(UNREGISTERED);

    DENSITY.push_back(UNREGISTERED);
    DENSITYN.push_back(UNREGISTERED);
    DENSITYNM1.push_back(UNREGISTERED);
    EXTEDGE_DENSITY.push_back(UNREGISTERED);
    EXTEDGE_DENSITYN.push_back(UNREGISTERED);

    MACRODENSITY.push_back(UNREGISTERED);
    MACRODENSITYN.push_back(UNREGISTERED);
    MACRODENSITYNM1.push_back(UNREGISTERED);

    TEMPERATURE.push_back(UNREGISTERED);
    TEMPERATUREN.push_back(UNREGISTERED);
    TEMPERATURENM1.push_back(UNREGISTERED);

    ENTHALPY.push_back(UNREGISTERED);
    ENTHALPYN.push_back(UNREGISTERED);
    ENTHALPYNM1.push_back(UNREGISTERED);

    INTENERGY.push_back(UNREGISTERED);
    INTENERGYN.push_back(UNREGISTERED);
    INTENERGYNM1.push_back(UNREGISTERED);

    DUALEDGE_VEL.push_back(UNREGISTERED);
    DUALEDGE_VELN.push_back(UNREGISTERED);

    VEL.push_back(UNREGISTERED);
    VELN.push_back(UNREGISTERED);
    VELNN.push_back(UNREGISTERED);
    GHOST_VEL.push_back(UNREGISTERED);
    VEL_BC.push_back(UNREGISTERED);
//#####DEBUG -- TEMPORARY REMOVE THIS
    DRAG.push_back(UNREGISTERED);
    LIFT.push_back(UNREGISTERED);
//#####DEBUG -- TEMPORARY REMOVE THIS

    // Storage for material property update/evaluation
    ELEM_MOL_VISCOSITY.push_back(UNREGISTERED);

    ELEM_TENSOR.push_back(UNREGISTERED);
  }
}

void
CCMultiField::registerData()
/*******************************************************************************
Routine: registerData - register data for incompressible/low-mach Navier-Stokes
Author : Mark A. Christon
*******************************************************************************/
{
  // Register the data for the unstructured-grid base class too
  CCclaw::registerData();

  // Register the primary variables
  registerPrimVar();

  // Register material data
  registerMaterialData();

  // Register the variables for turbulence models
  registerTurbVar();

  // Register the variables for deformable mesh
  registerDeformVar();

  // Register output delegates for derived variables
  registerDerivedVar();

  // Register derived output
  registerDerivedTurbVar();

  // Register the element/edge send and receive buff
  if (Nproc > 1) {
    ElemCommunicator& elem_comm = mesh->getElemComm();
    if (elem_comm.getNcomm() > 0) {
      registerCommBuff();
    }
  }

  // Register all the other stuff...
  NODE_RHS    = mesh->registerVariable(Nnp,
                                       sizeof(Real),
                                       "NODE_RHS",
                                       NODE_CTR,
                                       SCALAR_VAR,
                                       LINEAR_ARRAY,
                                       false,
                                       false);

  io->registerDelegate("NODE_RHS",
                       NODE_CTR,
                       SCALAR_VAR,
                       FIELD_DELEGATE,
                       this,
                       UnsPhysicsScalarField);

  NODEEQ_VAR  = mesh->registerVariable(m_Nnp_eq,
                                       sizeof(Real),
                                       "NODEEQ_VAR");

  // Register as a column-vector
  ELEM_GRADIENT = mesh->registerVariable(Nel,
                                         Ndim*sizeof(Real),
                                         "ELEM_GRADIENT",
                                         ELEMENT_CTR,
                                         VECTOR_VAR,
                                         COLUMN_MAJOR_ARRAY,
                                         false,
                                         false);

  ELEM_GHOST_GRAD = mesh->registerVariable(Nedge_ext,
                                           Ndim*sizeof(Real),
                                           "ELEM_GHOST_GRAD",
                                           ELEMENT_CTR,
                                           VECTOR_VAR,
                                           COLUMN_MAJOR_ARRAY,
                                           false,
                                           false);

  // Gradient Operators
  EDGE_LSMAT    = mesh->registerVariable(Nedge,
                                         sizeof(LSmatrix),
                                         "EDGE_LSMAT");

  EDGE_GRADIENT = mesh->registerVariable(Nedge,
                                         Ndim*sizeof(Real),
                                         "EDGE_GRADIENT",
                                         DUAL_EDGE_CTR,
                                         VECTOR_VAR,
                                         COLUMN_MAJOR_ARRAY,
                                         false,
                                         false);

  EDGE_2NDORDERFLAG = mesh->registerVariable(Nedge,
                                             sizeof(int),
                                             "EDGE_2NDORDERFLAG");

  registerNFieldVar(ELEM_TENSOR, Nel, sizeof(Tensor), ELEM_TENSOR_BASENAME,
                    ELEMENT_CTR, TENSOR_VAR, COLUMN_MAJOR_ARRAY);

  ELEM_RHS    = mesh->registerVariable(Nel,
                                       Ndim*sizeof(Real),
                                       "ELEM_RHS",
                                       ELEMENT_CTR,
                                       VECTOR_VAR,
                                       COLUMN_MAJOR_ARRAY,
                                       false,
                                       false);

  ELEM_DIAG   = mesh->registerVariable(Nel,
                                       sizeof(Real),
                                       "ELEM_DIAG");

  ELEMEQ_VAR  = mesh->registerVariable(Nel,
                                       sizeof(Real),
                                       "ELEMEQ_VAR");

  int nsurf = mesh->getNsdsets();
  if(nsurf > 0) {
    SURFACE_BCVOLUME = mesh->registerVariable(nsurf,
                                              sizeof(Real),
                                              "SURFACE_BCVOLUME",
                                              NO_CTR,
                                              SCALAR_VAR,
                                              LINEAR_ARRAY,
                                              false,
                                              true);

    io->registerDelegate("FVOL",
                         FACE_CTR,
                         SCALAR_VAR,
                         HISTORY_DELEGATE,
                         this,
                         CCINSFlowSurfaceFillVolumeHistory);
  }

  // Scratch arrays, all sized as the maximum of (number of edges, number of
  // points, number of elements, size of momentum exchange matrix)
  int mxA = Ndim * Ndim * m_Nfield * m_Nfield * MX_BLKSIZE;
  int maxSize = amax(mxA, amax(Nedge, amax(Nnp, Nel)));
  TMP1 = mesh->registerVariable(maxSize, sizeof(Real), "TMP1");
  TMP2 = mesh->registerVariable(maxSize, sizeof(Real), "TMP2");
  TMP3 = mesh->registerVariable(maxSize, sizeof(Real), "TMP3");
  TMP4 = mesh->registerVariable(maxSize, sizeof(Real), "TMP4");
  TMP5 = mesh->registerVariable(maxSize, sizeof(Real), "TMP5");

  TMP_VEC1 = mesh->registerVariable(maxSize,
                                    Ndim*sizeof(Real),
                                    "TMP_VEC1",
                                    NODE_CTR,
                                    VECTOR_VAR,
                                    COLUMN_MAJOR_ARRAY,
                                    false,
                                    false);

  TMP_VEC2 = mesh->registerVariable(maxSize,
                                    Ndim*sizeof(Real),
                                    "TMP_VEC2",
                                    NODE_CTR,
                                    VECTOR_VAR,
                                    COLUMN_MAJOR_ARRAY,
                                    false,
                                    false);

  // Allocate error accumulator
  m_errors = new CCINSErrors(*mesh, *io);

  // Temporary data, for error accumulation category
  m_errors->setTmpWorkArrays(TMP2, TMP_VEC1);

//#####DEBUG
#ifdef DEBUG
  mesh->dumpVariables(cout);
#endif
//#####DEBUG
}

void
CCMultiField::registerDerivedVar()
/*******************************************************************************
Routine: registerDerivedVar - register the derived output delegates
Author : J. Bakosi
*******************************************************************************/
{
  Category* pcat = control->getCategory(FVM_CC_NAVIERSTOKES_CATEGORY);
  int tmodel = pcat->getOptionVal(FVMCCINSControl::TURBULENCE_MODEL);

  // !!! ONLY register delegates for variables that are NEVER registered !!!

  // Field delegates
  registerNDelegate(VORTICITY_BASENAME, VECTOR_VAR,
                    CCMultiFieldElemVorticityVectorField,
                    CCINSFlowElemVorticityHistory,
                    CCMultiFieldNodeVorticityVectorField);

  registerNDelegate(ENSTROPHY_BASENAME, SCALAR_VAR,
                    CCINSFlowElemEnstrophyField,
                    CCINSFlowElemEnstrophyHistory,
                    CCINSFlowNodeEnstrophyField);

  io->registerDelegate("MIXDENSITY",
                       ELEMENT_CTR,
                       SCALAR_VAR,
                       FIELD_DELEGATE,
                       this,
                       CCMultiFieldElemMixtureDensityField);

  io->registerDelegate("MIXDENSITY",
                       NODE_CTR,
                       SCALAR_VAR,
                       FIELD_DELEGATE,
                       this,
                       CCMultiFieldNodeMixtureDensityField);

  io->registerDelegate("MIXVEL",
                       ELEMENT_CTR,
                       VECTOR_VAR,
                       FIELD_DELEGATE,
                       this,
                       CCMultiFieldElemMixtureVelocityVectorField);

  io->registerDelegate("MIXVEL",
                       NODE_CTR,
                       VECTOR_VAR,
                       FIELD_DELEGATE,
                       this,
                       CCMultiFieldNodeMixtureVelocityVectorField);

  io->registerDelegate("HELICITY",
                       ELEMENT_CTR,
                       SCALAR_VAR,
                       FIELD_DELEGATE,
                       this,
                       CCINSFlowElemHelicityField);

  io->registerDelegate("HELICITY",
                       NODE_CTR,
                       SCALAR_VAR,
                       FIELD_DELEGATE,
                       this,
                       CCINSFlowNodeHelicityField);

  // Q-Criteria
  io->registerDelegate("VGINV2",
                       ELEMENT_CTR,
                       SCALAR_VAR,
                       FIELD_DELEGATE,
                       this,
                       CCINSFlowElemQCriteriaField);

  io->registerDelegate("VGINV2",
                       NODE_CTR,
                       SCALAR_VAR,
                       FIELD_DELEGATE,
                       this,
                       CCINSFlowNodeQCriteriaField);

  io->registerDelegate("QCRITERIA",
                       ELEMENT_CTR,
                       SCALAR_VAR,
                       FIELD_DELEGATE,
                       this,
                       CCINSFlowElemQCriteriaField);

  io->registerDelegate("QCRITERIA",
                       NODE_CTR,
                       SCALAR_VAR,
                       FIELD_DELEGATE,
                       this,
                       CCINSFlowNodeQCriteriaField);

  // Surface delegates
  io->registerDelegate("TRACTION",
                       FACE_CTR,
                       VECTOR_VAR,
                       SURFACE_DELEGATE,
                       this,
                       CCINSFlowSurfTractionVectorField);

  io->registerDelegate("STRACTION",
                       FACE_CTR,
                       VECTOR_VAR,
                       SURFACE_DELEGATE,
                       this,
                       CCINSFlowSurfShearTractionVectorField);

  io->registerDelegate("NTRACTION",
                       FACE_CTR,
                       VECTOR_VAR,
                       SURFACE_DELEGATE,
                       this,
                       CCINSFlowSurfNormalTractionVectorField);

  io->registerDelegate("WALLSHEAR",
                       FACE_CTR,
                       SCALAR_VAR,
                       SURFACE_DELEGATE,
                       this,
                       CCINSFlowSurfWallShearField);

  io->registerDelegate("SURFAREA",
                       FACE_CTR,
                       SCALAR_VAR,
                       SURFACE_DELEGATE,
                       this,
                       CCINSFlowSurfAreaField);

  // Turbulence-specific surface output variables
  if (tmodel != NO_TURBMODEL) {
    io->registerDelegate("YPLUS",
                         FACE_CTR,
                         SCALAR_VAR,
                         SURFACE_DELEGATE,
                         this,
                         CCINSFlowSurfYplusField);

    if (tmodel == RNG_KE || tmodel == SST_KW ||
        tmodel == KSGS   || tmodel == LDKM_KSGS) {
      io->registerDelegate("YSTAR",
                           FACE_CTR,
                           SCALAR_VAR,
                           SURFACE_DELEGATE,
                           this,
                           CCINSFlowSurfYstarField);
    }

    io->registerDelegate("VARYPLUS",
                         FACE_CTR,
                         SCALAR_VAR,
                         SURFACE_DELEGATE,
                         this,
                         CCINSFlowSurfVarYplusField);
  }

  // Temperature-equation specific surface output variables
  int etype = pcat->getOptionVal(FVMCCINSControl::ENERGY_EQ);
   if (etype != ISOTHERMAL) {
    io->registerDelegate("HEATFLUX",
                         FACE_CTR,
                         VECTOR_VAR,
                         SURFACE_DELEGATE,
                         this,
                         CCINSFlowSurfHeatFluxVectorField);

    io->registerDelegate("NHEATFLUX",
                         FACE_CTR,
                         SCALAR_VAR,
                         SURFACE_DELEGATE,
                         this,
                         CCINSFlowSurfNormalHeatFluxField);
   }

  // History delegates
   io->registerDelegate("HELICITY",
                        ELEMENT_CTR,
                        SCALAR_VAR,
                        HISTORY_DELEGATE,
                        this,
                        CCINSFlowElemHelicityHistory);

   io->registerDelegate("AVGPRESS",
                        FACE_CTR,
                        SCALAR_VAR,
                        HISTORY_DELEGATE,
                        this,
                        CCINSFlowAvgSurfacePressureHistory);

   io->registerDelegate("SURFAREA",
                        FACE_CTR,
                        SCALAR_VAR,
                        HISTORY_DELEGATE,
                        this,
                        CCINSFlowSurfaceAreaHistory);

   io->registerDelegate("AVGVEL",
                        FACE_CTR,
                        VECTOR_VAR,
                        HISTORY_DELEGATE,
                        this,
                        CCINSFlowAvgSurfaceVelocityHistory);

   io->registerDelegate("FORCE",
                        FACE_CTR,
                        VECTOR_VAR,
                        HISTORY_DELEGATE,
                        this,
                        CCINSFlowSurfaceForceHistory);

   io->registerDelegate("PRESSFORCE",
                        FACE_CTR,
                        VECTOR_VAR,
                        HISTORY_DELEGATE,
                        this,
                        CCINSFlowSurfacePressureForceHistory);

   io->registerDelegate("VISCFORCE",
                        FACE_CTR,
                        VECTOR_VAR,
                        HISTORY_DELEGATE,
                        this,
                        CCINSFlowSurfaceViscousForceHistory);

  io->registerDelegate("MASSFLOW",
                       FACE_CTR,
                       SCALAR_VAR,
                       HISTORY_DELEGATE,
                       this,
                       CCINSFlowSurfaceMassFlowRateHistory);

  io->registerDelegate("VOLUMEFLOW",
                       FACE_CTR,
                       SCALAR_VAR,
                       HISTORY_DELEGATE,
                       this,
                       CCINSFlowSurfaceVolumeFlowRateHistory);


  // Temperature-equation specific surface output variables
  if (etype != ISOTHERMAL) {
    io->registerDelegate("HEATFLOW",
                         FACE_CTR,
                         SCALAR_VAR,
                         HISTORY_DELEGATE,
                         this,
                         CCINSFlowSurfaceHeatFlowHistory);

    io->registerDelegate("AVGTEMP",
                         FACE_CTR,
                         SCALAR_VAR,
                         HISTORY_DELEGATE,
                         this,
                         CCINSFlowAvgSurfaceTemperatureHistory);
  }
}

void
CCMultiField::registerNDelegate(const string basename,
                                const VariableType type,
                                const OutputDelegateWrapperCall fieldElmFun,
                                const OutputDelegateWrapperCall histFun,
                                const OutputDelegateWrapperCall fieldNodFun)
/*******************************************************************************
Routine: registerNDelegate - Register three output delegates for all fields
Author : J. Bakosi
*******************************************************************************/
{
  for (int i=0; i<m_Nfield; i++) {
    stringstream name;
    name << basename << '_' << i+1;     // fieldIDs in names are one-based
    const string n = name.str();
    if (fieldElmFun)
      io->registerDelegate(n,ELEMENT_CTR,type,FIELD_DELEGATE,this,fieldElmFun);
    if (histFun)
      io->registerDelegate(n,ELEMENT_CTR,type,HISTORY_DELEGATE,this,histFun);
    if (fieldNodFun)
      io->registerDelegate(n,NODE_CTR,type,FIELD_DELEGATE,this,fieldNodFun);
  }
}


void
CCMultiField::registerMaterialData()
/*******************************************************************************
Routine: registerMaterialData - register the material data
Author : Mark A. Christon
*******************************************************************************/
{
  // Data for material property update/evaluation
  registerNFieldVar(ELEM_MOL_VISCOSITY, Nel, sizeof(Real),
                    ELEM_MOL_VISCOSITY_BASENAME);

  // Register only if we have turbulence model present
  if(m_hasTurbModel) {
    ELEM_TURB_VISCOSITY = mesh->registerVariable(Nel,
                                                 sizeof(Real),
                                                 "TURBNU",
                                                 ELEMENT_CTR,
                                                 SCALAR_VAR,
                                                 LINEAR_ARRAY,
                                                 true,
                                                 false);
  }

  EDGE_VISCOSITY = mesh->registerVariable(Nedge,
                                          sizeof(Real),
                                          "EDGE_VISCOSITY");

  // Energy equation
  if (m_hasEnergyEq) {
    EDGE_CONDUCTIVITY = mesh->registerVariable(Nedge,
                                               sizeof(Real),
                                               "EDGE_CONDUCTIVITY");

    EDGE_SCALED_CONDUCTIVITY = mesh->registerVariable(Nedge,
                                               sizeof(Real),
                                               "EDGE_SCALED_CONDUCTIVITY");
  }

  // Energy equation -- variable viscosity
  if (m_variableViscosity) {
    ELEM_MOL_VISCOSITYN = mesh->registerVariable(Nel,
                                                 sizeof(Real),
                                                 "ELEM_MOL_VISCOSITYN");
  }
}

void
CCMultiField::registerFieldVar(DataIndex& ID,
                               const size_t number,
                               const size_t size,
                               const string basename,
                               const VariableCentering centering,
                               const VariableType type,
                               const MemoryLayout layout,
                               const bool plot,
                               const bool restart,
                               const OutputDelegateWrapperCall fieldElmFun,
                               const OutputDelegateWrapperCall histFun,
                               const OutputDelegateWrapperCall fieldNodFun)
/*******************************************************************************
Routine: registerFieldVar - Register variable, output delegates for one field
Author : J. Bakosi
*******************************************************************************/
{
  ID = mesh->registerVariable(number, size, basename, centering, type, layout,
			      plot, restart);

  if (fieldElmFun)
    io->registerDelegate(basename, ELEMENT_CTR, type, FIELD_DELEGATE, this, fieldElmFun);

  if (histFun)
    io->registerDelegate(basename, ELEMENT_CTR, type, HISTORY_DELEGATE, this, histFun);

  if (fieldNodFun)
    io->registerDelegate(basename, NODE_CTR, type, FIELD_DELEGATE, this, fieldNodFun);
}

void
CCMultiField::registerNFieldVar(vector<DataIndex>& id,
                                const size_t number,
                                const size_t size,
                                const string basename,
                                const VariableCentering centering,
                                const VariableType type,
                                const MemoryLayout layout,
                                const bool plot,
                                const bool restart,
                                const OutputDelegateWrapperCall fieldElmFun,
                                const OutputDelegateWrapperCall histFun,
                                const OutputDelegateWrapperCall fieldNodFun)
/*******************************************************************************
Routine: registerNFieldVar - Register variable, output delegates for all fields
Author : J. Bakosi
*******************************************************************************/
{
  for (int i=0; i<m_Nfield; i++) {
    stringstream name;
    name << basename << '_' << i+1;     // fieldIDs in names are one-based
    const string n = name.str();

    id[i] = mesh->registerVariable(number, size, n, centering, type,
				   layout, plot, restart);

    if (fieldElmFun)
      io->registerDelegate(n,ELEMENT_CTR,type,FIELD_DELEGATE,this,fieldElmFun);

    if (histFun)
      io->registerDelegate(n,ELEMENT_CTR,type,HISTORY_DELEGATE,this,histFun);

    if (fieldNodFun)
      io->registerDelegate(n,NODE_CTR,type,FIELD_DELEGATE,this,fieldNodFun);
  }
}

void
CCMultiField::registerPrimVar()
/*******************************************************************************
Routine: registerPrimVar - register the primary, i.e., primitive variables
Author : Mark A. Christon
*******************************************************************************/
{
  Category* pcat = control->getCategory(FVM_CC_NAVIERSTOKES_CATEGORY);

  // Volume fractions
  registerNFieldVar(VOLFRAC, Nel, sizeof(Real), VOLFRAC_BASENAME, ELEMENT_CTR,
                    SCALAR_VAR, LINEAR_ARRAY, true, true, UnsPhysicsScalarField,
                    CCINSFlowElemScalarHistory, CCMultiFieldNodeVolFracField);
  // Volume fractions at time n
  registerNFieldVar(VOLFRACN, Nel, sizeof(Real), VOLFRACN_BASENAME, ELEMENT_CTR,
                    SCALAR_VAR, LINEAR_ARRAY, false, true);
  // External-edge volume fractions
  registerNFieldVar(EXTEDGE_VOLFRAC, Nedge_ext, sizeof(Real),
                    EXTEDGE_VOLFRAC_BASENAME);
  // External-edge volume fractions at time n
  registerNFieldVar(EXTEDGE_VOLFRACN, Nedge_ext, sizeof(Real),
                    EXTEDGE_VOLFRACN_BASENAME);


  // Micro-Densities at time n+1
  registerNFieldVar(DENSITY, Nel, sizeof(Real), DENSITY_BASENAME, ELEMENT_CTR,
                    SCALAR_VAR, LINEAR_ARRAY, true, true, UnsPhysicsScalarField,
                    CCINSFlowElemScalarHistory, CCMultiFieldNodeDensityField);
  // Micro-Densities at time n
  registerNFieldVar(DENSITYN, Nel, sizeof(Real), DENSITYN_BASENAME,
                    ELEMENT_CTR, SCALAR_VAR, LINEAR_ARRAY, false, true);

  // External-edge densities at time n+1
  registerNFieldVar(EXTEDGE_DENSITY, Nedge_ext, sizeof(Real),
                    EXTEDGE_DENSITY_BASENAME);
  // External-edge densities at time n
  registerNFieldVar(EXTEDGE_DENSITYN, Nedge_ext, sizeof(Real),
                    EXTEDGE_DENSITYN_BASENAME);

  // Macro-Densities at time n+1
  registerNFieldVar(MACRODENSITY, Nel, sizeof(Real), MACRODENSITY_BASENAME,
                    ELEMENT_CTR, SCALAR_VAR, LINEAR_ARRAY, true, true,
                    UnsPhysicsScalarField, CCINSFlowElemScalarHistory,
                    CCMultiFieldNodeDensityField);
  // Macro-Densities at time n
  registerNFieldVar(MACRODENSITYN, Nel, sizeof(Real), MACRODENSITYN_BASENAME,
                    ELEMENT_CTR, SCALAR_VAR, LINEAR_ARRAY, false, true);

  // Generic ghost variable for scalar transport
  registerNFieldVar(GHOST_VAR, Nedge_ext, sizeof(Real), GHOSTVAR_BASENAME);


  if (m_hasEnergyEq) {
    // Still need temperature even if the enthalpy or internal energy equation
    // is solved
    // Temperature at time n+1
    registerNFieldVar(TEMPERATURE, Nel, sizeof(Real), TEMPERATURE_BASENAME,
                      ELEMENT_CTR, SCALAR_VAR, LINEAR_ARRAY, true, true,
                      UnsPhysicsScalarField, CCINSFlowElemScalarHistory,
                      CCMultiFieldNodeTemperatureField);

    int etype = pcat->getOptionVal(FVMCCINSControl::ENERGY_EQ);
    if (etype == TEMPERATURE_EQ) {
      // Temperature at time n
      registerNFieldVar(TEMPERATUREN, Nel, sizeof(Real), TEMPERATUREN_BASENAME,
                        ELEMENT_CTR, SCALAR_VAR, LINEAR_ARRAY, false, true);
    } else if (etype == ENTHALPY_EQ) {
      // Enthalpy at time n+1
      registerNFieldVar(ENTHALPY, Nel, sizeof(Real), ENTHALPY_BASENAME,
                        ELEMENT_CTR, SCALAR_VAR, LINEAR_ARRAY, true, true,
                        UnsPhysicsScalarField, CCINSFlowElemScalarHistory,
                        CCMultiFieldNodeEnthalpyField);
      // Enthalpy at time n
      registerNFieldVar(ENTHALPYN, Nel, sizeof(Real), ENTHALPYN_BASENAME,
                        ELEMENT_CTR, SCALAR_VAR, LINEAR_ARRAY, false, true);
    } else if (etype == INTENERGY_EQ) {
      // Internal energy at time n+1
      registerNFieldVar(INTENERGY, Nel, sizeof(Real), INTENERGY_BASENAME,
                        ELEMENT_CTR, SCALAR_VAR, LINEAR_ARRAY, true, true,
                        UnsPhysicsScalarField, CCINSFlowElemScalarHistory,
                        CCMultiFieldNodeIntEnergyField);
      // Internal energy at time n
      registerNFieldVar(INTENERGYN, Nel, sizeof(Real), INTENERGYN_BASENAME,
                        ELEMENT_CTR, SCALAR_VAR, LINEAR_ARRAY, false, true);
    }
  }  // if has energy eq

  // Velocity at time n+1
  registerNFieldVar(VEL, Nel, Ndim*sizeof(Real), VEL_BASENAME, ELEMENT_CTR,
                    VECTOR_VAR, COLUMN_MAJOR_ARRAY, true, true,
                    UnsPhysicsVectorField, CCINSFlowElemVectorHistory,
                    CCMultiFieldNodeVelocityVectorField);
  // Velocity at time n
  registerNFieldVar(VELN, Nel, Ndim*sizeof(Real), VELN_BASENAME,
                    ELEMENT_CTR, VECTOR_VAR, COLUMN_MAJOR_ARRAY, false, true);
  // A copy of velocity at time n
  registerNFieldVar(VELNN, Nel, Ndim*sizeof(Real), VELNN_BASENAME,
                    ELEMENT_CTR, VECTOR_VAR, COLUMN_MAJOR_ARRAY, false, true);

  // Ghost velocity
  registerNFieldVar(GHOST_VEL, Nedge_ext, Ndim*sizeof(Real), GHOST_VEL_BASENAME,
                    DUAL_EDGE_CTR, VECTOR_VAR, COLUMN_MAJOR_ARRAY, false, false);

  // Mixture momentum at time n+1
  registerFieldVar(MIXMOM, Nel, Ndim*sizeof(Real), "MIXMOM",
                   ELEMENT_CTR, VECTOR_VAR, COLUMN_MAJOR_ARRAY, true, true);

  // Mixture dual-edge momentum
  registerFieldVar(DUALEDGE_MIXMOM, Nedge, sizeof(Real), "DUALEDGE_MIXMOM",
                   DUAL_EDGE_CTR, SCALAR_VAR, LINEAR_ARRAY, false, true);

  if(Nproc > 1) {
    // Ghost volume
    registerFieldVar(GHOST_VOL, Nedge_ext, sizeof(Real), GHOST_VOL_BASENAME);

    // Inverse assembled volume for G-G smoother
    registerFieldVar(GHOST_ASSEM_VOL, Nedge_ext, sizeof(Real),
                     GHOST_ASSEM_VOL_BASENAME);
  }

  // Buffer for velocity BCs for projection
  registerNFieldVar(VEL_BC, Nedge_ext, Ndim*sizeof(Real), VEL_BC_BASENAME,
                    DUAL_EDGE_CTR, VECTOR_VAR, COLUMN_MAJOR_ARRAY, false, false);

  // Dual-edge velocity at time n+1
  registerNFieldVar(DUALEDGE_VEL, Nedge, sizeof(Real), DUALEDGE_VEL_BASENAME,
                    DUAL_EDGE_CTR, SCALAR_VAR, LINEAR_ARRAY, false, true);

  // Dual-edge velocity at time n
  registerNFieldVar(DUALEDGE_VELN, Nedge, sizeof(Real), DUALEDGE_VELN_BASENAME,
                    DUAL_EDGE_CTR, SCALAR_VAR, LINEAR_ARRAY, false, true);

  // Divergence
  registerFieldVar(DIVERGENCE, Nel, sizeof(Real), DIV_BASENAME, ELEMENT_CTR,
                   SCALAR_VAR, LINEAR_ARRAY, true, true, UnsPhysicsScalarField,
                   CCINSFlowElemScalarHistory);

  // Pressure at time n+1
  registerFieldVar(PRESSURE, Nnp, sizeof(Real), PRESSURE_BASENAME, NODE_CTR,
                   SCALAR_VAR, LINEAR_ARRAY, true, true,
                   CCINSFlowElemPressureField, CCINSFlowElemPressureHistory,
                   CCINSFlowNodePressureField);

  // Pressure at time n
  registerFieldVar(PRESSUREN, Nnp, sizeof(Real), PRESSUREN_BASENAME, NODE_CTR,
                   SCALAR_VAR, LINEAR_ARRAY, true, true,
                   CCINSFlowElemPressureField, CCINSFlowElemPressureHistory,
                   CCINSFlowNodePressureField);

  // Single Lagrange multiplier (node centered)
  registerFieldVar(LAMBDA, Nnp, sizeof(Real), LAMBDA_BASENAME, NODE_CTR,
                   SCALAR_VAR, LINEAR_ARRAY, true, true,
                   CCINSFlowElemPressureField, CCINSFlowElemScalarHistory,
                   UnsPhysicsScalarField);

  // Drag force
  registerNFieldVar(DRAG, Nel, Ndim*sizeof(Real), DRAG_BASENAME, ELEMENT_CTR,
                    VECTOR_VAR, COLUMN_MAJOR_ARRAY, true, true,
                    UnsPhysicsVectorField, CCINSFlowElemVectorHistory,
                    CCMultiFieldNodeDragVectorField);
  // Lift force
  registerNFieldVar(LIFT, Nel, Ndim*sizeof(Real), LIFT_BASENAME, ELEMENT_CTR,
                    VECTOR_VAR, COLUMN_MAJOR_ARRAY, true, true,
                    UnsPhysicsVectorField, CCINSFlowElemVectorHistory,
                    CCMultiFieldNodeLiftVectorField);
}

void
CCMultiField::registerTimers()
/*******************************************************************************
Routine: registerTimers - register all the timers for a multi-field physics step
Author : J. Bakosi
*******************************************************************************/
{
  // Note: The order in which timers are registered dictates the
  // order in which the timing for each timer is reported.
  // So, try to maintain a graceful order here.

  VOLFRAC_TIME = timer->registerTimer("Volume fractions solution");

  MOMENTUM_TIME = timer->registerTimer("Momentum equations solution");

  if (m_hasEnergyEq) {
    ENERGY_TIME = timer->registerTimer("Energy equations solution");
  }

  Category* pcat = control->getCategory(FVM_CC_NAVIERSTOKES_CATEGORY);
  int tmodel = pcat->getOptionVal(FVMCCINSControl::TURBULENCE_MODEL);
  if (tmodel != NO_TURBMODEL && tmodel != VARMULTISCALE) {
    switch(tmodel) {
    case WALE:
      TURB_TIME = timer->registerTimer("WALE model");
      break;
    case SMAGORINSKY:
      TURB_TIME = timer->registerTimer("Smagorinsky solution");
      break;
    case SPALART_ALLMARAS:
      TURB_TIME = timer->registerTimer("Spalart-Allmaras solution");
      break;
    case RNG_KE:
      TURB_TIME = timer->registerTimer("K-Eps model solution");
      break;
    case SST_KW:
      TURB_TIME = timer->registerTimer("K-Omega solution");
      break;
    case KSGS:
      TURB_TIME = timer->registerTimer("Ksgs solution");
      break;
    case LDKM_KSGS:
      TURB_TIME = timer->registerTimer("LDKM Ksgs solution");
      break;
    }
  }

  VP_UPDATE = timer->registerTimer("Pressure-velocities update");

  PPE_TIME = timer->registerTimer("Pressure-Poisson solution");

  if (tmodel != NO_TURBMODEL && tmodel != VARMULTISCALE) {
    DIST_TIME = timer->registerTimer("Normal distance solution");
  }

  DT_CALC_TIME = timer->registerTimer("Calculate time-step size");

  UPDATE_VELGRAD = timer->registerTimer("Update velocity gradients");

  UPDATE_MATERIAL_STATE = timer->registerTimer("Update materials state");

  MESH_DEFORMATION = timer->registerTimer("Mesh deformation");

  ALE_UPDATE = timer->registerTimer("ALE update procedure");

  EXPORT_TIME = timer->registerTimer("Calculate exported fields");

  WRITE_TIME = timer->registerTimer("Plot, history write");

  RESTART_WRITE_TIME = timer->registerTimer("Restart write");
}
