//******************************************************************************
//! \file    src/FVM/CCMultiField//CCMFDrag.h
//! \author  Jozsef Bakosi
//! \date    Thu May 30 11:50:35 2013
//! \brief   Base class for momentum drag terms for multiphase flow
//******************************************************************************
#ifndef CCMFDrag_h
#define CCMFDrag_h

#include <vector>

#include <HydraTypes.h>
#include <CCMFMomentumExchange.h>
#include <UnsMesh.h>

namespace Hydra {

//! Drag (momentum exchange for multiphase flow) types
enum DragType { CONST_DRAG=0,
                ISHII_ZUBER_DRAG,
                TOMIYAMA_DRAG,
                BOZZANO_DENTE_DRAG,
                WANG_DRAG };

//! Drag parameters bundle
struct DragParam {
  DragType type;                         //!< Drag type
  int setId;                             //!< Material set ID
  DisperseID dispId;                     //!< Association of the disperse ID
  int tblid;                              //!< Load cureve id
  Real amp;                              //!< Amplitude of table
  Real bd;                               //!< Bubble diameter
  Real st;                               //!< Surface tension
  Real coeff;                            //!< (Constant) drag coefficient
  const vector< pair<int,int> >& field;  //!< Field id pairs drag acts between

  //! Initializer constructor
  //!   \param[in] t      Drag type, see enum DragType
  //!   \param[in] sid    Material set id
  //!   \param[in] did    Association of the disperse ID
  //!   \param[in] lid    table id
  //!   \param[in] amp    Amplitude of table
  //!   \param[in] bd     Bubble diameter
  //!   \param[in] st     Surface tension
  //!   \param[in] coeff  (Constant) drag coefficient
  //!   \param[in] field  Field id pairs drag acts between
  DragParam(DragType t,
            int sid,
            DisperseID did,
            int lid,
            Real a,
            Real b,
            Real s,
            Real c,
            const vector< pair<int,int> >& f) : type(t),
                                                setId(sid),
                                                dispId(did),
                                                tblid(lid),
                                                amp(a),
                                                bd(b),
                                                st(s),
                                                coeff(c),
                                                field(f) {}
};

//! Drag DataIndex variables bundle
struct DragVar {
  DataIndex VOLUME;
  const vector< DataIndex >& VOLFRAC;
  const vector< DataIndex >& DENSITY;
  const vector< DataIndex >& VEL;
  const vector< DataIndex >& VELN;

  //! Initializer constructor
  DragVar(DataIndex volume,
          const vector< DataIndex >& volfrac,
          const vector< DataIndex >& density,
          const vector< DataIndex >& vel,
          const vector< DataIndex >& veln) : VOLUME(volume),
                                             VOLFRAC(volfrac),
                                             DENSITY(density),
                                             VEL(vel),
                                             VELN(veln) {}
};

//! Base class for drag momentum exchange terms
class CCMFDrag : public MomentumExchange {

  public:

    //! \name Constructor/Destructor
    //@{
    //!   \param[in]  param    Drag parameters, see struct DragParam
    //!   \param[in]  pair     Field ID pair drag acts between
    //!   \param[in]  nfield   Total number of fields
    //!   \param[in]  ndim     Number of spatial dimensions
    //!   \details Note that nfield and ndim are references as they do not have
    //!          the correct values at parsing (when this constructor is called)
    CCMFDrag(const DragParam& param,
             int pair,
             const int& nfield,
             const int& ndim);
    virtual ~CCMFDrag();
    //@}

    //! Echo one-liner info
    //!   \param[inout]  ofs  output stream to echo to
    virtual void echo(ostream& ofs) const;

    //! Return string describing drag type (for echo)
    //!   \return string describing the type of drag
    virtual string type() const = 0;

    //! Compute and return drag force operator:
    //! i.e., 3/8 * bubble_radius * volfrac * rho * drag_coeff * abs(rel_vel)
    //!   \param[in]     id1    Field id 1
    //!   \param[in]     id2    Field id 2
    //!   \param[in]     bo     Block offset to access physical variable
    //!   \param[in]     bs     Block size
    //!   \param[inout]  D      Computed force operator
    virtual void calcForceOp(int id1,
                             int id2,
                             int bo,
                             int bs,
                             vector< vector<Real*> >& D) = 0;

    //! Fill raw pointers to physical variables
    //!   \param[in] mesh     Mesh object
    //!   \param[in] var      Drag DataIndex variables bundle
    void getVars(UnsMesh* const mesh, const DragVar& var);

    //! Return relative velocity at the current time level
    //!   \param[in]  id1  Field id 1
    //!   \param[in]  id2  Field id 2
    //!   \param[in]  idx  Cell index
    //!   \param[in]  dim  Dimension (i.e., direction) to compute
    //!   \return     Relative velocity between field-pair at current time level
    Real rvel(int id1, int id2, int idx, int dim) const {
      return vel[id1][dim][idx] - vel[id2][dim][idx];
    }

    //! Return relative velocity at the previous time level
    //!   \param[in]  id1  Field id 1
    //!   \param[in]  id2  Field id 2
    //!   \param[in]  idx  Cell index
    //!   \param[in]  dim  Dimension (i.e., direction) to compute
    //!   \return     Relative velocity between field-pair at previous time level
    Real rveln(int id1, int id2, int idx, int dim) const {
      return veln[id1][dim][idx] - veln[id2][dim][idx];
    }

  protected:
    const int m_dispId;               //!< Disperse phase ID
    const int m_contId;               //!< Continuous phase ID
    const Real m_bubbleRad;           //!< Bubble radius

    //! Raw pointers to physical variables required for drag force calculation
    //! DO NOT EVER ATTEMPT TO CALL DELETE ON THESE POINTERS !!!
    Real* V;
    vector< Real* > vf;
    vector< Real* > rho;
    vector< vector<Real*> > vel;
    vector< vector<Real*> > veln;

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCMFDrag(const CCMFDrag&);
    CCMFDrag& operator=(const CCMFDrag&);
    //@}
};

}

#endif
