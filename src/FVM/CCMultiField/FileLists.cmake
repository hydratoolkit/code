# ############################################################################ #
#
# Library: FVMCCMultiField
# File Definition File 
#
# ############################################################################ #

# Source Files

set(FVMCCMultiField_SOURCE_FILES
            CCMultiField.C
            CCMFBC.C
            CCMFConstantDrag.C
            CCMFConstantLift.C
            CCMFContinuity.C
            CCMFDrag.C
            CCMFLift.C
            CCMFDragMap.C
            CCMFLiftMap.C
            CCMFDerivedVar.C
            CCMFFieldDelegates.C
            CCMFMat.C
            CCMFMomentum.C
            CCMFRegister.C
	    CCMFSolve.C
            CCMFTemperature.C
            CCMFIntEnergy.C
            CCMFEnthalpy.C
            CCMFUtil.C
            CCMFVolFrac.C
            CCMFVolFracBC.C
)
