//******************************************************************************
//! \file    src/FVM/CCMultiField/CCMFForceMap.h
//! \author  Jozsef Bakosi
//! \date    Fri Sep 28 11:50:34 2011
//! \brief   Map of vectors of forces
//******************************************************************************
#ifndef CCMFForceMap_h
#define CCMFForceMap_h

#include <VecMap.h>

namespace Hydra {

//! Map of vectors of pointer to CCMF force types
template<class T>
class CCMFForceMap : public VecMap<int, T> {

    using VecMap<int, T>::m_cit;
    using VecMap<int, T>::m_mapvec;
    typedef typename VecMap<int, T>::vector_type vector_type;

  public:
    //! Empty constructor
    CCMFForceMap() {}

    //! Add a force (e.g, body, Boussinesq) to the map
    //!   \param[in] tblid     table id
    //!   \param[in] amp      Amplitude of body force
    //!   \param[in] setid    Exodus class id
    //!   \param[in] en       Unit normal for direction
    //!   \param[in] field    vector<int> with field ids this force relates to
    void addForce(const int tblid,
                  const Real amp,
                  const int setid,
                  const Real* en,
                  const vector<int>& field) {
      const int nfields = field.size();
      for (int i=0; i<nfields; i++)
        m_mapvec[field[i]].push_back(new T(tblid,amp,setid,en));
    }

    //! Echo all forces
    //!   \param[out] ofs      Output stream to echo to
    void echo(ostream& ofs) {
      for (m_cit=m_mapvec.begin(); m_cit!=m_mapvec.end(); m_cit++) {
        const vector_type* v = &m_cit->second;
        const int numvec = v->size();
        for (int i=0; i<numvec; i++)
          echoForce((*v)[i], m_cit->first, ofs);
      }
    }

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCMFForceMap(const CCMFForceMap&);
    CCMFForceMap& operator=(const CCMFForceMap&);
    //@}

    //! Echo one liner info for force
    //!   \param[in]  force    Force to echo
    //!   \param[in]  field    Field id to echo information of
    //!   \param[out] ofs      Output stream to echo to
    void echoForce(T* const force, const int field, ostream& ofs) {
      Real* dir = force->getDir();
      ofs << "\t" << setw( 5) << right << field+1 << "  "
                  << setw( 6) << force->getSetId() << "  "
                  << setw(10) << force->getTableId() << "  "
                  << setw(10) << force->getAmplitude() << "  "
                  << setw(10) << dir[0] << "  " << dir[1] << "  " << dir[2]
                  << endl;
    }
};

}

#endif
