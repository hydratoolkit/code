//******************************************************************************
//! \file    src/FVM/CCMultiField/CCMFFieldDelegates.h
//! \author  Jozsef Bakosi
//! \date    Mon Sep 24 13:22:00 2012
//! \brief   Field output delegates for multifield flow
//******************************************************************************
#ifndef CCMultiFieldFieldDelegates_h
#define CCMultiFieldFieldDelegates_h

#include <CCMultiField.h>

namespace Hydra {

//! Declaration of field delegates available in CCMultiField

OUTPUT_DELEGATE_DECL(CCMultiField,
                     writeNodeDensityField,
                     CCMultiFieldNodeDensityField)

OUTPUT_DELEGATE_DECL(CCMultiField,
                     writeNodeVelocityVectorField,
                     CCMultiFieldNodeVelocityVectorField)

OUTPUT_DELEGATE_DECL(CCMultiField,
                     writeElemVorticityVectorField,
                     CCMultiFieldElemVorticityVectorField)

OUTPUT_DELEGATE_DECL(CCMultiField,
                     writeNodeVorticityVectorField,
                     CCMultiFieldNodeVorticityVectorField)

OUTPUT_DELEGATE_DECL(CCMultiField,
                     writeNodeVolFracField,
                     CCMultiFieldNodeVolFracField)

OUTPUT_DELEGATE_DECL(CCMultiField,
                     writeNodeTemperatureField,
                     CCMultiFieldNodeTemperatureField)

OUTPUT_DELEGATE_DECL(CCMultiField,
                     writeNodeIntEnergyField,
                     CCMultiFieldNodeIntEnergyField)

OUTPUT_DELEGATE_DECL(CCMultiField,
                     writeNodeEnthalpyField,
                     CCMultiFieldNodeEnthalpyField)

OUTPUT_DELEGATE_DECL(CCMultiField,
                     writeElemMixtureDensityField,
                     CCMultiFieldElemMixtureDensityField)

OUTPUT_DELEGATE_DECL(CCMultiField,
                     writeNodeMixtureDensityField,
                     CCMultiFieldNodeMixtureDensityField)

OUTPUT_DELEGATE_DECL(CCMultiField,
                     writeElemMixtureVelocityVectorField,
                     CCMultiFieldElemMixtureVelocityVectorField)

OUTPUT_DELEGATE_DECL(CCMultiField,
                     writeNodeMixtureVelocityVectorField,
                     CCMultiFieldNodeMixtureVelocityVectorField)

OUTPUT_DELEGATE_DECL(CCMultiField,
                     writeNodeDragVectorField,
                     CCMultiFieldNodeDragVectorField)

OUTPUT_DELEGATE_DECL(CCMultiField,
                     writeNodeLiftVectorField,
                     CCMultiFieldNodeLiftVectorField)

}

#endif // CCMultiFieldFieldDelegates.h
