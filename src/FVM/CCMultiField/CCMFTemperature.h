//******************************************************************************
//! \file    src/FVM/CCMultiField/CCMFTemperature.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Multifield temperature transport
//******************************************************************************
#ifndef CCMFTemperature_h
#define CCMFTemperature_h

#include <CCINSTemperature.h>

namespace Hydra {

class CCMFTemperature : public CCINSTemperature {

  public:

    //! \name Constructor/Destructor
    //@{
             CCMFTemperature(UnsMesh& mesh,
                             Control& control,
                             DualEdgeGradOp& edgeGradOp,
                             CCINSErrors& errors,
                             fileIO& io,
                             CCINSTransportVar& di,
                             vector<CCINSTemperatureBC*>& tempBCs,
                             vector<CCINSHeatFluxBC*>& heatfluxbc,
                             vector<CCINSHeatSource*>& heat_source,
                             vector<CCINSSurfChem*>& surfchem,
                             LASolver& transport_solver,
                             LAMatrix& transport_A,
                             LAVector& transport_b,
                             LAVector& transport_x,
                             CCINSTurbulence& turbulence,
                             CCINSAdapter& adapter);
    virtual ~CCMFTemperature();
    //@}

    //! Solve generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void solve(const CCINSIncParm& incParm);

    //! Assemble generic scalar transport equation
    //!   \param[in] CCINSIncParm Time increment parameters
    virtual void assembleSystem(const CCINSIncParm& incParm);

   /*! Form transport Rhs function
    *    \param[in]     incParm Increment parameter functions
    *    \param[in,out] rhs     Pointer to the rhs vector
    */
    virtual void formRhs(const CCINSIncParm& incParm,
                         FVMCCINS::SourceMode srcmode,
                         Real* rhs=0);

    //! Modify the LHS for iteration w. variable Cp
    void modifyLhs(Real* Cp, Real* rho, Real* rhoCp, LAMatrix& M);

    //! Convert temperature to an energy variable, e.g., enthalpy
    //!   \param[in]  temperature  temperature
    //!   \param[out] energyvar    energy variable
    virtual void convertTempVar(Real* /*T*/, Real* /*energyvar*/) {}

    //! Convert energy variable to temperature
    //!   \param[in]  energyvar    energy variable
    //!   \param[out] temperature  temperature
    virtual void convertEnergyVar(Real* /*energyvar*/, Real* /*T*/) {}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCMFTemperature(const CCMFTemperature&);
    CCMFTemperature& operator=(const CCMFTemperature&);
    //@}

};

}

#endif // CCMFTemperature_h

