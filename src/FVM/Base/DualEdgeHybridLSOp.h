//******************************************************************************
//! \file    src/FVM/Base/DualEdgeHybridLSOp.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:11 2011
//! \brief   Dual-edge hybrid least-squares gradient estimator
//******************************************************************************
#ifndef DualEdgeHybridLSOp_h
#define DualEdgeHybridLSOp_h

#include <HydraTypes.h>
#include <DataShapes.h>
#include <DualEdgeLSOp.h>

namespace Hydra {

struct DualEdge;
class  UnsMesh;
class  LAMatrix;
class  LAVector;

//! Hybrid dual-edge least-squares gradient operators
class DualEdgeHybridLSOp : public DualEdgeLSOp {

  public:

    //! \name Constructor/Destructor
    //@{
             DualEdgeHybridLSOp(UnsMesh& mesh,
                                DataIndex GHOST_XC,
                                DataIndex EDGE_LSMAT,
                                DataIndex EDGE_2NDORDERFLAG,
                                DataIndex SEND_BUF,
                                DataIndex RECV_BUF);
    virtual ~DualEdgeHybridLSOp();
    //@}

    //! Add the diffusive terms to the LHS operator
    virtual void addDiffusiveLhs(const bool restrictOp, Real* alpha,
                                 const Real theta, const Real dt,
                                 LAMatrix& K);

    //! Spalart-Allmaras: Add the diffusive operator the LHS matrix
    virtual void addDiffusiveLhs(const Real C1, const Real C2, const Real theta,
                                 const Real dt, Real* alpha, Real* edge_alpha,
                                 LAMatrix& K);

    //! Add the diffusive terms to the RhS
    virtual void addDiffusiveRhs(const bool restrictOp, Real* alpha,
                                 Real theta, Real dt, Real* evar, Real* gvar,
                                 CVector& vgrad, Real* rhs);

    //! Spalart-Allmaras: Add the diffusive operator the LHS matrix
    virtual void addDiffusiveRhs(Real C1, Real C2, Real theta, Real dt,
                                 Real* alpha, Real* edge_alpha, Real* evar,
                                 Real* gvar, CVector& grad, Real* rhs);

    //! Add the viscous terms to the RHS of the momentum equations
    virtual void addDiffusiveRhs(Real theta, Real dt, Real* alpha,
                                 CVector& veln, CVector& gvel, CTensor& vgrad,
                                 CVector& rhs);

    //! Adjust external edge contribution to LHS due to BC
    virtual void adjustLhs(const bool restrictOp, Real* alpha, Real theta,
                           Real dt, bool* var_bcflag, LAMatrix& K);

    //! Adjust external edge contribution to LHS due to BC
    virtual void adjustLhs(Real* alpha, Real theta, Real dt,
                           bool* var_bcflag, Real* koffdiag1, Real* koffdiag2,
                           LAMatrix& K);

    //! Spalart-Allmaras: Adjust the diffusive operator for BC's in the LHS
    virtual void adjustLhs(Real C1, Real C2, Real theta, Real dt, Real* alpha,
                           Real* edge_alpha, bool* bcflag,
                           DataIndex GHOST_NU, LAMatrix& K);

    //! Adjust edge contribution to RHS due to BC & LS method
    virtual void adjustRhs(const bool restrictOp,
                           Real* alpha, Real theta, Real dt, bool* vel_bcflag,
                           Real* gvel, Real* rhs, LAVector& b);

    //! Spalart-Allmaras: Add the diffusive operator the LHS matrix
    virtual void adjustRhs(Real C1, Real C2, Real theta,
                           Real dt, Real* alpha, Real* edge_alpha,
                           bool* bcflag, Real* gvar, Real* rhs,
                           DataIndex GHOST_NU, LAVector& b);

    //! Setup the least-squares gradient class
    virtual void setup(int& mxElrow, int& mxElrowOff,
                       vector<int>& d_nnz, vector<int>& o_nnz);

    virtual void setup(int& mxElrow, int& mxElrowOff,
                       vector<int>& d_nnz, vector<int>& o_nnz,
                       bool* bcflag);

    virtual void setup(int& mxElrow, int& mxElrowOff,
                       vector<int>& d_nnz, vector<int>& o_nnz,
                       CBoolVector velbcflag);

  private:

    //! Don't permit copy or assignment operators
    //@{
    DualEdgeHybridLSOp(const DualEdgeHybridLSOp&);
    DualEdgeHybridLSOp& operator=(const DualEdgeHybridLSOp&);
    //@}

};

}

#endif // DualEdgeHybridLSOp_h
