//******************************************************************************
//! \file    src/FVM/Base/DualEdgeLSOp.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:11 2011
//! \brief   Edge-centered least-squares gradient estimator
//******************************************************************************
#ifndef DualEdgeLSOp_h
#define DualEdgeLSOp_h

#include <HydraTypes.h>
#include <DualEdgeGradOp.h>
#include <DataShapes.h>

namespace Hydra {

class  UnsMesh;
struct DualEdge;
class  LAMatrix;
class  LAVector;

#define HAS_LS      -1      // Has second order LS term
#define NO_LS        0      // Has no second order LS term
#define MIN_EDGE_DOT 1.0e-8 // Minimum for normal dot-product test

//! Symetric 4x4 least squares data shaper
struct LSmatrix {
  Real L[10];
};

//! Edge-centered least-squares gradient estimator
class DualEdgeLSOp : public DualEdgeGradOp {

  public:

    //! \name Constructor/Destructor
    //@{
             DualEdgeLSOp(UnsMesh& mesh,
                          DataIndex GHOST_XC,
                          DataIndex EDGE_LSMAT,
                          DataIndex EDGE_2NDORDERFLAG,
                          DataIndex SEND_BUF,
                          DataIndex RECV_BUF);
    virtual ~DualEdgeLSOp();
    //@}

    //! Add the diffusive terms to the LHS operator
    virtual void addDiffusiveLhs(const bool restrictOp, Real* alpha,
                                 const Real theta, const Real dt,
                                 LAMatrix& K);

    //! Spalart-Allmaras: Add the diffusive operator the LHS matrix
    virtual void addDiffusiveLhs(const Real C1, const Real C2,
                                 const Real theta, const Real dt,
                                 Real* alpha, Real* edge_alpha,
                                 LAMatrix& K);

    //! Add the diffusive terms to the RHS
    virtual void addDiffusiveRhs(const bool restrictOp, Real* alpha,
                                 Real theta, Real dt, Real* evar, Real* gvar,
                                 CVector& vgrad, Real* rhs);

    //! Spalart-Allmaras: Add the diffusive operator the LHS matrix
    virtual void addDiffusiveRhs(Real C1, Real C2, Real theta, Real dt,
                                 Real* alpha, Real* edge_alpha, Real* evar,
                                 Real* gvar, CVector& grad, Real* rhs);

    //! Add the viscous terms to the RHS of the single-field momentum equations
    virtual void addDiffusiveRhs(Real theta, Real dt, Real* alpha,
                                 CVector& veln, CVector& gvel, CTensor& vgrad,
                                 CVector& rhs);

    //! Adjust external edge contribution to LHS due to BC
    virtual void adjustLhs(const bool restrictOp, Real* alpha, Real theta,
                           Real dt, bool* var_bcflag, LAMatrix& K);

    //! Adjust external edge contribution to LHS due to BC
    virtual void adjustLhs(Real* alpha, Real theta, Real dt,
                           bool* var_bcflag, Real* koffdiag1, Real* koffdiag2,
                           LAMatrix& K);

    //! Restore the LHS due to adjust
    virtual void restoreLhs(Real* koffdiag1, Real* koffdiag2, LAMatrix& K);

    //! Spalart-Allmaras: Adjust the diffusive operator for BC's in the LHS
    virtual void adjustLhs(Real C1, Real C2, Real theta, Real dt, Real* alpha,
                           Real* edge_alpha, bool* bcflag,
                           DataIndex GHOST_NU, LAMatrix& K);

    //! Adjust edge contribution to RHS due to BC & LS method
    virtual void adjustRhs(const bool restrictOp,
                           Real* alpha, Real theta, Real dt, bool* vel_bcflag,
                           Real* gvel, Real* rhs, LAVector& b);

    //! Spalart-Allmaras: Add the diffusive operator the LHS matrix
    virtual void adjustRhs(Real C1, Real C2, Real theta,
                           Real dt, Real* alpha, Real* edge_alpha,
                           bool* bcflag, Real* gvar, Real* rhs,
                           DataIndex GHOST_NU, LAVector& b);

    //! Assemble the edge gradient
    virtual void assembleGrad(int nvardim, DataIndex EDGEVAR);

    //! Compute a variable gradient at dual-edges
    virtual void calcGrad(const bool restrictOp,
                          Real* elem_var, Real* ghost_var, DataIndex EDGEGRAD);

    //! Compute the edge-gradient with a full LS-model
    virtual void calcGrad(const bool restrictOp,
                          Real* elem_var, Real* ghost_var, CVector& edge_grad);

    //! Calculate the least-squares operator
    virtual void calcGradOper();

    //! Setup the least-squares gradient class
    virtual void setup(int& mxElrow, int& mxElrowOff,
                       vector<int>& d_nnz, vector<int>& o_nnz);

    virtual void setup(int& mxElrow, int& mxElrowOff,
                       vector<int>& d_nnz, vector<int>& o_nnz, bool* bcflag);

    virtual void setup(int& mxElrow, int& mxElrowOff,
                       vector<int>& d_nnz, vector<int>& o_nnz,
                       CBoolVector velbcflag);

    //! Return size of external edge 2nd-order
    int extEdge2ndOrderSize() const {
      return m_ExtEdge2ndOrderSize;
    }

    //! Return external edge 2nd-order flag
    bool extEdgeHas2ndOrder() const {
      return m_ExtEdgeHas2ndOrder;
    }

  protected:

    //! Assemble the LS operator at edges
    void assembleLSOper();

    //! Assemble Least-Squares matrix
    void assemLSMatrix(Real* L, Real dx, Real dy, Real dz);

    //! Solve the inverse LS matrix
    void calcInvLSMat();

    //! Realign  the buffer for momentum/transport matrix assembly
    void realignSubMtxData(int size, int counter, Real *buffer) const;

    //! Set the row size for momentum/transport equations
    void setupElemLSRowSize(int& mxElrow, int& mxElrowOff,
                            vector<int>& d_nnz, vector<int>& o_nnz);

    //! Set up the L-matrix
    void setupLSOper();

    //! Set up the second order flag
    void setupSecondOrderFlag();
    void setupSecondOrderFlag(bool* bcflag);
    void setupSecondOrderFlag(CBoolVector velbcflag);

    //! Size the buffer for external edges
    void setupExtEdgeSecondOrderSize();

    DataIndex GHOST_XC;
    DataIndex EDGE_LSMAT;           //!< edge LS operator
    DataIndex EDGE_2NDORDERFLAG;
    DataIndex SEND_BUF;
    DataIndex RECV_BUF;

    bool m_ExtEdgeHas2ndOrder; // flag indicating external edges are 2nd order

    int m_ExtEdge2ndOrderSize; // Size of external edges with 2nd order term

  private:

    //! Don't permit copy or assignment operators
    //@{
    DualEdgeLSOp(const DualEdgeLSOp&);
    DualEdgeLSOp& operator=(const DualEdgeLSOp&);
    //@}

};

}

#endif // DualEdgeLSOp_h
