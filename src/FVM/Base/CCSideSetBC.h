//******************************************************************************
//! \file    src/FVM/Base/CCSideSetBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:11 2011
//! \brief   Base class for deriving cell-centered side-set boundary conditions
//******************************************************************************
#ifndef CCSideSetBC_h
#define CCSideSetBC_h

#include <SideSetBC.h>

namespace Hydra {

//! Side-Set BC class for cell-centered conservation laws
class CCSideSetBC : public SideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             CCSideSetBC(const BCPackage& bc, int offset);
    virtual ~CCSideSetBC();
    //@}

    //! Set the edge-data for a given BC
    //!   \param[in]  mesh   Reference to the mesh object
    //!   \param[in]  t      Time
    //!   \param[out] gd     Pointer to ghost data array
    void setEdgeBCs(UnsMesh& mesh, Real* gd, const Real t);

    //! Set the edge-data for a given BC
    //!   \param[in]  mesh   Reference to the mesh object
    //!   \param[in]  t      Time
    //!   \param[out] bcflag BC flag
    //!   \param[out] gd     Pointer to ghost data array
    virtual void setEdgeBCs(UnsMesh& mesh, bool* bcflag, Real* gd,
                            const Real t);

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCSideSetBC(const CCSideSetBC&);
    CCSideSetBC& operator=(const CCSideSetBC&);
    //@}

};

}

#endif // CCSideSetBC_h
