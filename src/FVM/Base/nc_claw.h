//******************************************************************************
//! \file    src/FVM/Base/nc_claw.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:11 2011
//! \brief   Virtual base class for node-centered conservation law algorithms
//******************************************************************************

#ifndef NC_CLAW_H
#define NC_CLAW_H

#include <control.h>
#include <fileio.h>
#include <mesh.h>
#include <uns_physics.h>

namespace Hydra {

//! Node-centered C-Law class
class NCclaw : public UNSPhysics {

  public:

    //! \name Constructor/Destructor
    //@{
             NCclaw() {}
    virtual ~NCclaw() {}
    //@}

    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData() {}
    //@}

    //! \name Virtual Physics Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    //! Virtual setup function -- implemented for each specific physics
    virtual void setup() {}

    //! Virtual solver -- implemented for advection-diffusion physics
    void solve() {}

    //! Virtual function to setup initial conditions for advection-diffusion
    virtual void setICs() {}
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    NCclaw(const NCclaw&);
    NCclaw& operator=(const NCclaw&);
    //@}

};

}
#endif // NC_CLAW_H
