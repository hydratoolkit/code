//******************************************************************************
//! \file    src/FVM/Base/DualEdgeGradOp.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:11 2011
//! \brief   Virtual base class for the dual-edge gradients
//******************************************************************************
#include <cmath>

using namespace std;

#include <DualEdgeGradOp.h>
#include <UnsMesh.h>
#include <MPIWrapper.h>

using namespace Hydra;

DualEdgeGradOp::DualEdgeGradOp(UnsMesh& mesh):
  m_mesh(mesh),
  m_elEqIDOffset(0),
  m_Nproc(0),
  m_pid(0),
  m_Nedge(0),
  m_Nedge_ext(0),
  m_Nedge_ifc(0),
  m_Nedge_int(0),
  m_Nedge_ghost(0),
  m_Nel(0)
/*******************************************************************************
Routine: DualEdgeGradOp - constructor
Author : Mark A. Christon
*******************************************************************************/
{}

DualEdgeGradOp::~DualEdgeGradOp()
/*******************************************************************************
Routine: ~DualEdgeGradOp - destructor
Author : Mark A. Christon
*******************************************************************************/
{}

