//******************************************************************************
//! \file    src/FVM/Base/CCclaw.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:11 2011
//! \brief  Virtual base class for cell-centered conservation laws algorithms.
//******************************************************************************
#ifndef CCclaw_h
#define CCclaw_h

#include <Control.h>
#include <fileIO.h>
#include <UnsMesh.h>
#include <UnsPhysics.h>

namespace Hydra {

class EdgeGradOp;

//! Cell-centered C-Law class
class CCclaw : public UnsPhysics {

  public:

    //! \name Constructor/Destructor
    //@{
             CCclaw();
    virtual ~CCclaw();
    //@}

    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData();
    //@}

    //! \name Virtual Physics Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    //! Virtual setup function -- implemented for each specific physics
    virtual void setup();
    //@}

    //! Finalize -- implemented for each specific physics
    virtual void finalize() {}

    // Calculate dual-edge interpolation weights
    void calcEdgeInterp();

    //! Calculate the centroids for ghost data at boundaries
    void setGhostCentroids();

    // Swap centroid coordinates for overlapping sub-domains
    void swapGhostCentroids();

    // Swap ghost data for overlapping sub-domains
    void swapGhostVar(Real* elem_v, Real* ghost_v);

    // Swap ghost data for overlapping sub-domains
    void swapGhostVar(CVector& elem_v, CVector& ghost_v);

    // Swap ghost data for overlapping sub-domains
    void swapGhostVar(CSymTensor& elem_v, CSymTensor& ghost_v);

    // zero LHS and RHS for processor boundary nodes
    void zeroInvDistOpProcBoundaryNodes(Real* node_var, Real* work, int* iflag);

  protected:

    //! Project scalar elem variable to nodes, applying BCs
    //!  \param[in] NVAR    Node variable
    //!  \param[in] EVAR    Elem variable
    //!  \param[in] bc      Vector of boundary conditions
    template<class BC>
    void projectToNodes(const DataIndex EVAR,
                        const DataIndex TMP1,
                        const DataIndex TMP2,
                        const DataIndex SND_BUF,
                        const DataIndex RCV_BUF,
                        const vector<BC*>& bc,
                        const Real time,
                        const DataIndex NVAR)
    {
      Real* node_var = mesh->getVariable<Real>(NVAR);
      Real* elem_var = mesh->getVariable<Real>(EVAR);
      Real* work     = mesh->getVariable<Real>(TMP1);
      int*  iflag    = mesh->getVariable<int>(TMP2);

      // Project the element field to the node grid
      memset(node_var, 0, Nnp*sizeof(Real));
      memset(work,     0, Nnp*sizeof(Real));
      memset(iflag,    0, Nnp*sizeof(int ));

      formVolProjectionID(elem_var, node_var, work);

      // Zero the projected RHS and LHS values
      int nbc = bc.size();
      for (int i=0; i<nbc; ++i)
        bc[i]->zeroInvDistOp(*mesh, node_var, work, iflag);

      // Clear the Rhs and Lhs on processor boundaries
      zeroInvDistOpProcBoundaryNodes(node_var, work, iflag);

      // Compute the Rhs and Lhs of projection at surfaces
      for (int i=0; i<nbc; ++i)
        bc[i]->formInvDistOp(*mesh, time, node_var, work);

      // Solve the inverse-distance projection
      solveProjID(node_var, work, SND_BUF, RCV_BUF);
    }

    //! \name Cell-centered protected data
    //@{
    int m_Nmel;
    int m_e_start;
    int m_e_end;
    int m_Nepe;
    int Nedge;        //!< Total number of dual edges             (local )
    int Nedge_int;    //!< Number of internal dual edges          (local )
    int Nedge_ext;    //!< Number of external dual edges          (local )
    int Nedge_ifc;    //!< Number of external plus IFC dual edges (local )
    int Nedge_ghost;  //!< Number of true ghost edges             (local )

    // Dual-edge gradient operator
    EdgeGradOp* m_edgeGradOp;

    DualEdge* edges;

    DataIndex EDGE_XI; //!< \xi value in the owner element for the edge

    //!< Gradient data
    DataIndex EDGE_LSMAT;           //!< edge least squares operator
    DataIndex EDGE_2NDORDERFLAG;    //!< 2nd-order flag for hybrid op.
    DataIndex EDGE_GRADIENT;        //!< Edge gradient

    //!< Parallel communications data
    DataIndex GHOST_XC;
    DataIndex SEND_BUF;
    DataIndex RECV_BUF;
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CCclaw(const CCclaw&);
    CCclaw& operator=(const CCclaw&);
    //@}

};

}

#endif // CCclaw_h
