//******************************************************************************
//! \file    src/FVM/Base/DualEdgeGradOp.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:11 2011
//! \brief   Virtual base class for the dual-edge gradients
//******************************************************************************
#ifndef DualEdgeGradOp_h
#define DualEdgeGradOp_h

#include <HydraTypes.h>
#include <DataShapes.h>
#include <vector>

namespace Hydra {

struct DualEdge;
class  UnsMesh;
class  LAMatrix;
class  LAVector;

//! Virtual base class for the dual-edge gradients
class DualEdgeGradOp {

  public:


    //! \name Constructor/Destructor
    //@{
             DualEdgeGradOp(UnsMesh& mesh);
    virtual ~DualEdgeGradOp();
    //@}

    //! Add the viscous/diffusive terms to the LHS operator
    virtual void addDiffusiveLhs(const bool restrictOp, Real* alpha,
                                 const Real theta, const Real dt,
                                 LAMatrix& K) = 0;

    //! Spalart-Allmaras: Add the diffusive operator to the LHS operator
    virtual void addDiffusiveLhs(const Real C1, const Real C2,
                                 const Real theta, const Real dt,
                                 Real* alpha, Real* edge_alpha,
                                 LAMatrix& K) = 0;

    //! Add the viscous diffusive terms to the RHS
    virtual void addDiffusiveRhs(const bool restrictOp, Real* alpha,
                                 Real theta, Real dt, Real* evar, Real* gvar,
                                 CVector& vgrad, Real* rhs) = 0;

    //! Spalart-Allmaras: Add the diffusive operator the RHS
    virtual void addDiffusiveRhs(Real C1, Real C2, Real theta, Real dt,
                                 Real* alpha, Real* edge_alpha,
                                 Real* evar, Real* gvar, CVector& grad,
                                 Real* rhs) = 0;

    //! Add the viscous terms to the RHS for the momentum equations
    virtual void addDiffusiveRhs(Real theta, Real dt, Real* alpha,
                                 CVector& veln, CVector& gvel,
                                 CTensor& vgrad, CVector& rhs) = 0;

    //! Adjust external edge contribution to LHS due to BC's
    virtual void adjustLhs(const bool restrictOp, Real* alpha, Real theta,
                           Real dt, bool* var_bcflag, LAMatrix& K) = 0;

    //! Adjust external edge contribution to LHS due to BC's
    virtual void adjustLhs(Real* alpha, Real theta, Real dt, bool* var_bcflag,
                           Real* koffdiag1, Real* koffdiag2, LAMatrix& K) = 0;

    //! Restore the LHS due to BC adjustment
    virtual void restoreLhs(Real* koffdiag1, Real* koffdiag2, LAMatrix& K) = 0;

    //! Spalart-Allmaras: Adjust the diffusive operator for BC's in the LHS
    virtual void adjustLhs(Real C1, Real C2, Real theta, Real dt, Real* alpha,
                           Real* edge_alpha, bool* bcflag,
                           DataIndex GHOST_NU, LAMatrix& K) = 0;

    //! Adjust edge contribution to RHS due to BC & LS method
    virtual void adjustRhs(const bool restrictOp,
                           Real* alpha, Real theta, Real dt, bool* vel_bcflag,
                           Real* gvel, Real* rhs, LAVector& b) = 0;

    //! Spalart-Allmaras: Adjust the diffusive operator the LHS matrix
    virtual void adjustRhs(Real C1, Real C2, Real theta,
                           Real dt, Real* alpha, Real* edge_alpha,
                           bool* bcflag, Real* gvar, Real* rhs,
                           DataIndex GHOST_NU, LAVector& b) = 0;

    //! Assemble the edge gradient
    virtual void assembleGrad(int nvardim, DataIndex EDGEVAR) = 0;

    //! Driver to compute the edge-gradient
    virtual void calcGrad(const bool restrictOp, Real* elem_var, Real* ghost_var,
                          DataIndex EDGEGRAD) = 0;

    //! Compute the edge-gradient with a full LS-model
    virtual void calcGrad(const bool restrictOp, Real* elem_var, Real* ghost_var,
                          CVector& edge_grad) = 0;

    //! Calculate the gradient operator
    virtual void calcGradOper() = 0;

    //! Setup the gradient class
    virtual void setup(int& mxElrow, int& mxElrowOff,
                       vector<int>& d_nnz, vector<int>& o_nnz) = 0;

    virtual void setup(int& mxElrow, int& mxElrowOff,
                       vector<int>& d_nnz, vector<int>& o_nnz,
                       bool* bcflag) = 0;

    virtual void setup(int& mxElrow, int& mxElrowOff,
                       vector<int>& d_nnz, vector<int>& o_nnz,
                       CBoolVector velbcflag) = 0;

    //! Return size of external edge 2nd-order
    virtual int extEdge2ndOrderSize() const = 0;

    //! Return external edge 2nd-order flag
    virtual bool extEdgeHas2ndOrder() const = 0;


  protected:

    UnsMesh& m_mesh;

    //!Some basic data stored for convenience in setup().

    int m_elEqIDOffset;

    int m_Nproc;
    int m_pid;

    int m_Nedge;
    int m_Nedge_ext;
    int m_Nedge_ifc;
    int m_Nedge_int;
    int m_Nedge_ghost;

    int m_Nel;

  private:

    //! Don't permit copy or assignment operators
    //@{
    DualEdgeGradOp(const DualEdgeGradOp&);
    DualEdgeGradOp& operator=(const DualEdgeGradOp&);
    //@}

};

}

#endif // DualEdgeGradOp_h
