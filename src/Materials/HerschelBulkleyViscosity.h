//******************************************************************************
//! \file    src/Materials/HerschelBulkleyViscosity.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   Herschel-Bulkley Viscosity
//******************************************************************************
#ifndef HerschelBulkleyViscosity_h
#define HerschelBulkleyViscosity_h

#include <ViscosityProperty.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Herschel-Bulkley Viscosity
class HerschelBulkleyViscosity: public ViscosityProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             HerschelBulkleyViscosity(DataContainer& mesh,
                                      Real shearViscosityLow,
                                      Real yieldShearStress,
                                      Real k,
                                      Real n);
             HerschelBulkleyViscosity(DataContainer& mesh);
    virtual ~HerschelBulkleyViscosity();
    //@}

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate viscosity at given temperature
    virtual Real evaluate(int gid);

    //! Evaluate derivative of viscosity at given temperature
    virtual Real evaluateDerivative(int gid);

    //! Evaluate viscosity
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Evaluate derivative of viscosity at given temperature
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives);

    //! Return viscosity type
    virtual ViscosityType getType();

    //! Get property format
    virtual PropertyFormat getFormat();

  private:

    //! Don't permit copy or assignment operators
    //@{
    HerschelBulkleyViscosity(const HerschelBulkleyViscosity&);
    HerschelBulkleyViscosity& operator=(const HerschelBulkleyViscosity&);
    //@}

    Real m_mu_zero;
    Real m_tau_zero;
    Real m_k;
    Real m_n;

    DataContainer& m_mesh;
};

}

#endif
