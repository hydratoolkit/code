//******************************************************************************
//! \file    src/Materials/MaterialProperty.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   Base class for material properties
//******************************************************************************
#ifndef MaterialProperty_h
#define MaterialProperty_h
#include <iostream>

#include <HydraTypes.h>

namespace Hydra {

//! Material States
enum MaterialState {
  MAT_STATE = 0,   //!< State at time level n+1 (current)
  MAT_STATEN,      //!< State at time level n
  MAT_STATENM1,    //!< State at time level n-1
  NUM_MAT_STATES
};

//! Property Formats
enum PropertyFormat {
  CONSTANT_PROPERTY = 0,
  POLYNOMIAL_PROPERTY,
  TABULAR_PROPERTY,
  FUNCTIONAL_PROPERTY
};

//! Base class for material properties
class MaterialProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             MaterialProperty();
    virtual ~MaterialProperty();
    //@}

    //**************************************************************************
    //! \name Material property virtual interface
    //!  Virtual methods for each material property
    //@{
    //! Echo material property options
    virtual void echoOptions(ostream& ofs) = 0;

    //! Evaluate property
    virtual Real evaluate(int /*gid*/) {
      cout << "evaluate method not implemented" << endl;
      return 0.0;
    }

    //! Evaluate property
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0) {
      cout << "evaluate method not implemented" << endl;
      for (int i = 0; i < nel; i++) {
        results[gids[i]] = 0.0;
      }
      if (derivatives != 0) {
        for (int j = 0; j < nel; j++) {
          derivatives[gids[j]] = 0.0;
        }
      }
      return false;
    }

    //! Get property format
    virtual PropertyFormat getFormat() = 0;

    //! Set data index where pressure data for pressure-dependent property
    void setPressure(DataIndex pressure,
                     DataIndex pressureN,
                     DataIndex pressureNM1);

    //! Set the state that this property points to for evaluation
    virtual void setState(MaterialState state);

    //! Set data index where temperature data for temperature-dependent property
    virtual void setTemperature(DataIndex temperature) = 0;

    //! Set data index where temperature data for temperature-dependent property
    void setTemperature(DataIndex temperature,
                        DataIndex temperatureN,
                        DataIndex temperatureNM1);

    //! Unset temperature data indexes
    virtual void unsetTemperature();

    //! Set a data index where porosity data for the elements can be found
    virtual void setPorosity(DataIndex porosity);
    //@}

  protected:

    DataIndex m_PRESSURE[NUM_MAT_STATES];    //!< Pressure states
    DataIndex m_TEMPERATURE[NUM_MAT_STATES]; //!< Temperature states

  private:

    //! Don't permit copy or assignment operators
    //@{
    MaterialProperty(const MaterialProperty&);
    MaterialProperty& operator=(const MaterialProperty&);
    //@}

};

}
#endif
