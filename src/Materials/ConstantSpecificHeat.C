//******************************************************************************
//! \file    src/Materials/ConstantSpecificHeat.C
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Specific Heat
//******************************************************************************
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include <ConstantSpecificHeat.h>
#include <PrintUtil.h>

using namespace Hydra;

ConstantSpecificHeat::ConstantSpecificHeat(int shType,
                                           Real specificHeat):
  SpecificHeatProperty(shType),
  m_C(specificHeat)
/*******************************************************************************
Routine: ConstantSpecificHeat - constructor
Author : Mark A. Christon and BTN
*******************************************************************************/
{}

ConstantSpecificHeat::~ConstantSpecificHeat()
/*******************************************************************************
Routine: ~ConstantSpecificHeat - destructor
Author : Mark A. Christon
*******************************************************************************/
{}

void
ConstantSpecificHeat::setTemperature(DataIndex /*temperature*/)
/*******************************************************************************
Routine: setTemperature - set data index for temperature
Author : Mark A. Christon
*******************************************************************************/
{}

void
ConstantSpecificHeat::echoOptions(ostream& ofs)
/*******************************************************************************
Routine: echoOPtions - echo material data
Author : Mark A. Christon
*******************************************************************************/
{
  PrintUtil pu(ofs);

  if (m_type == CONSTANT_PRESSURE){
    pu.printOption("Specific heat at constant pressure", m_C);
  } else if (m_type == CONSTANT_VOLUME){
    pu.printOption("Specific heat at constant volume", m_C);
  }
}

Real
ConstantSpecificHeat::evaluate(int /*gid*/)
/*******************************************************************************
Routine: evaluateCp - evaluate specific heat at constant pressure
Author : Mark A. Christon
*******************************************************************************/
{
  // assert(m_type == CONSTANT_PRESSURE);
  return m_C;
}

bool
ConstantSpecificHeat::evaluate(int nel, const int* gids,
                                       Real* results, Real* derivatives)
/*******************************************************************************
Routine: evaluateCp - evaluate specific heat at constant pressure
Author : Mark A. Christon
*******************************************************************************/
{
  for (int i = 0; i < nel; i++)
    results[gids[i]] = m_C;
  if (derivatives != 0) {
    for (int i = 0; i < nel; i++) derivatives[gids[i]] = 0.0;
  }
  return true;
}

Real
ConstantSpecificHeat::evaluateDerivative(int /*gid*/)
/*******************************************************************************
Routine: evaluateCp - evaluate specific heat derivative at constant pressure
Author : Mark A. Christon
*******************************************************************************/
{
  return 0.0;
}

bool
ConstantSpecificHeat::evaluateDerivative(int nel, const int* gids,
                                                 Real* derivatives)
/*******************************************************************************
Routine: evaluateCp - evaluate specific heat derivative at constant pressure
Author : Mark A. Christon
*******************************************************************************/
{
  for (int i = 0; i < nel; i++) derivatives[gids[i]] = 0.0;
  return true;
}

PropertyFormat
ConstantSpecificHeat::getFormat()
/*******************************************************************************
Routine: getFormat - return material property data format
Author : Mark A. Christon
*******************************************************************************/
{
  return CONSTANT_PROPERTY;
}
