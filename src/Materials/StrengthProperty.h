//******************************************************************************
//! \file    src/Materials/StrengthProperty.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Material strength base class w. virtual interface
//******************************************************************************
#ifndef StrengthProperty_h
#define StrengthProperty_h

#include <DataShapes.h>
#include <MaterialProperty.h>

namespace Hydra {

//! Enumeration listing available material properties
enum StrengthType {
  STRENGTH_ELASTIC=0,
  STRENGTH_JOHNSON_COOK,
  STRENGTH_PRANDTL_REUSS,
  NUM_STRENGTH
};

//! Base class for material strength
class StrengthProperty: public MaterialProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             StrengthProperty(Real E, Real G);
    virtual ~StrengthProperty();
    //@}

    //! Set temperature data index
    //!   \param[in] temperature  Data index for temperature
    virtual void setTemperature(DataIndex temperature);

    virtual Real elasticModulus() {return m_E;}

    virtual Real shearModulus() {return m_G;}

    //! Plastic modulus, i.e., slope of yield-stress vs. strain curve
    //   \param[in] e       internal energy
    //   \param[in] epsp    effective plastic strain
    //   \param[in] epspdot effective plastic strain-rate
    virtual Real plasticModulus(Real e, Real epsp, Real epspdot) = 0;

    // Yield stress
    //   \param[in] e       internal energy
    //   \param[in] epsp    effective plastic strain
    //   \param[in] epspdot effective plastic strain-rate
    virtual Real yieldStress(Real e, Real epsbar, Real epsdot) = 0;

    // Radial return
    //  \param[in] nel     # of elements in element class (block)
    //  \param[in] gids    Global Id's for element storage
    //  \param[in] dt      Time-step
    //  \param[in] e       Specific internal energy
    //  \param[in] epsp    effective plastic strain
    //  \param[in] epspdot effective plastic strain-rate
    //  \param[in] S       deviatoric stress
    virtual void radialReturn(int nel, Real dt, int* gids, Real* e,
                              Real* epsp, Real* epspdot, CSymTensor& S);

    // Update the trial elastic deviatoric stress state
    //  \param[in] nel     # of elements in element class (block)
    //  \param[in] dt      Time-step
    //  \param[in] gids    Global Id's for element storage
    //  \param[in] vgrad   velocity gradient
    //  \param[in] S       deviatoric stress
    virtual void updateDeviatoricStress(int nel, Real dt, int* gids,
                                        CTensor& vgrad, CSymTensor& S);

  protected:

    DataIndex m_temperature; //!< Temperature state for thermal effects

    Real m_E;  //!< Elastic modulus (modulus of elasticity)
    Real m_G;  //!< Shear modulus

  private:

    //! Don't permit copy or assignment operators
    //@{
    StrengthProperty(const StrengthProperty&);
    StrengthProperty& operator=(const StrengthProperty&);
    //@}

};

}

#endif // StrengthProperty_h
