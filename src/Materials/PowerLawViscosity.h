//******************************************************************************
//! \file    src/Materials/PowerLawViscosity.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   Power Law Viscosity
//******************************************************************************
#ifndef PowerLawViscosity_h
#define PowerLawViscosity_h

#include <ViscosityProperty.h>

namespace Hydra {

// Forward declarations
class DataContainer;

class PowerLawViscosity: public ViscosityProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             PowerLawViscosity(DataContainer& mesh,
                               Real minViscosity,
                               Real maxViscosity,
                               Real k,
                               Real n);
             PowerLawViscosity(DataContainer& mesh);
    virtual ~PowerLawViscosity();
    //@}

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate viscosity at given temperature
    virtual Real evaluate(int gid);

    //! Evaluate derivative of viscosity at given temperature
    virtual Real evaluateDerivative(int gid);

    //! Evaluate viscosity
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Evaluate derivative of viscosity at given temperature
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives);

    //! Get property format
    virtual PropertyFormat getFormat();

    //! Return viscosity type
    virtual ViscosityType getType();

  private:

    //! Don't permit copy or assignment operators
    //@{
    PowerLawViscosity(const PowerLawViscosity&);
    PowerLawViscosity& operator=(const PowerLawViscosity&);
    //@}

    Real m_mu_min;
    Real m_mu_max;
    Real m_k;
    Real m_n;

    DataContainer& m_mesh;
};

}

#endif
