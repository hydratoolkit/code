//******************************************************************************
//! \file    src/Materials/ViscosityProperty.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   Viscosity base class
//******************************************************************************
#ifndef ViscosityProperty_h
#define ViscosityProperty_h

#include <MaterialProperty.h>

namespace Hydra {

// Forward declarations
class DataContainer;

enum ViscosityType {
  CONSTANT_VISCOSITY = 0,
  CARREAU_YASUDA_VISCOSITY,
  CROSS_VISCOSITY,
  POWER_LAW_VISCOSITY,
  POWELL_EYRING_VISCOSITY,
  HERSCHEL_BULKEY_VISCOSITY,
  ELLIS_METER_VISCOSITY,
  SUTHERLAND_VISCOSITY,
  TABULAR_VISCOSITY
};

//! Viscosity Base Class
class ViscosityProperty: public MaterialProperty
{

  public:

    //! \name Constructor/Destructor
    //@{
             ViscosityProperty();
    virtual ~ViscosityProperty();
    //@}

    //! Evaluate derivative of viscosity at given temperature
    virtual Real evaluateDerivative(int gid) = 0;

    //! Evaluate derivative of viscosity at given temperature
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives) = 0;

    //! Return viscosity type
    virtual ViscosityType getType() = 0;

    virtual void setVelocityGradient(DataIndex velGrad);

  protected:

    // Velocity gradient data index
    DataIndex m_VELOCITY_GRADIENT;

  private:

    //! Don't permit copy or assignment operators
    //@{
    ViscosityProperty(const ViscosityProperty&);
    ViscosityProperty& operator=(const ViscosityProperty&);
    //@}
};

}

#endif
