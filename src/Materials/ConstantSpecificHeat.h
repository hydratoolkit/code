//******************************************************************************
//! \file    src/Materials/ConstantSpecificHeat.h
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Constant Specific Heat
//******************************************************************************
#ifndef ConstantSpecificHeat_h
#define ConstantSpecificHeat_h

#include <cassert>

#include <SpecificHeatProperty.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Constant Specific Heat
class ConstantSpecificHeat: public SpecificHeatProperty
{

  public:

    //! \name Constructor/Destructor
    //@{
             ConstantSpecificHeat(int shType, Real specificHeat);
    virtual ~ConstantSpecificHeat();
    //@}

    //! Evaluate specific_heat
    virtual Real evaluate(int gid);

    //! Get property format
    virtual bool evaluate(int nel, const int* gids,
                          Real* results, Real* derivatives);

    //! Evaluate derivative
    virtual Real evaluateDerivative(int gid);

    //! Evaluate derivative
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives);


    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Get property format
    virtual PropertyFormat getFormat();

  private:

    //! Don't permit copy or assignment operators
    //@{
    ConstantSpecificHeat(const ConstantSpecificHeat&);
    ConstantSpecificHeat& operator=(const ConstantSpecificHeat&);
    //@}

    Real m_C;
};

}

#endif // ConstantSpecificHeat_h
