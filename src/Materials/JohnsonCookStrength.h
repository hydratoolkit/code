//******************************************************************************
//! \file    src/Materials/JohnsonCookStrength.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Johnson-Cook material strength constitutive model
//******************************************************************************
#ifndef JohnsonCookStrength_h
#define JohnsonCookStrength_h

#include <StrengthProperty.h>

namespace Hydra {

class JohnsonCookStrength : public StrengthProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             JohnsonCookStrength(Real G, Real A, Real B, Real C, Real Tmelt,
                                 Real n, Real m);
    virtual ~JohnsonCookStrength();
    //@}

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Get property format
    virtual PropertyFormat getFormat();

    //! Plastic modulus, i.e., slope of yield-stress vs. strain curve
    //   \param[in] e      internal energy
    //   \param[in] epsp   plastic strain
    //   \param[in] epsdot plastic strain-rate
    virtual Real plasticModulus(Real e, Real epsbar, Real epsdot);

    // Yield stress
    //   \param[in] e      internal energy
    //   \param[in] epsp   plastic strain
    //   \param[in] epsdot plastic strain-rate
    virtual Real yieldStress(Real e, Real epsbar, Real epsdot);

  private:

    //! Don't permit copy or assignment operators
    //@{
    JohnsonCookStrength(const JohnsonCookStrength&);
    JohnsonCookStrength& operator=(const JohnsonCookStrength&);
    //@}

    Real m_A;     //!< Yield stress constant
    Real m_B;     //!< Multiplier for plastic strain in yield stress
    Real m_C;     //!< Multiplier on logarithmic strain ratio
    Real m_Tmelt; //!< Melting temperature
    Real m_n;     //!< Plastic strain exponent in yield stress
    Real m_m;     //!< Temperaturee exponent
    Real m_Tref;  //!< Material reference temperature

};

}

#endif // JohnsonCookStrength_h
