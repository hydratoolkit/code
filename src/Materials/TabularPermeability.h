//******************************************************************************
//! \file    src/Materials/TabularPermeability.h
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Tabular Permeability
//******************************************************************************
#ifndef TabularPermeability_h
#define TabularPermeability_h

#include <PermeabilityProperty.h>
#include <DataShapes.h>
#include <Table.h>

namespace Hydra {

class DataContainer;
struct CSymTensor;

//! Tabular Permeability
class TabularPermeability: public PermeabilityProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             TabularPermeability(DataContainer& mesh, int ctype);
    virtual ~TabularPermeability();
    //@}

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate at current settings
    virtual Real evaluate(int gid);

    //! Evaluate isotropic permeability at current settings
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Evaluate derivative at current settings
    virtual Real evaluateDerivative(int gid);

    //! Evaluate isotropic permeability derivative at current settings
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives);

    //! Evaluate at current settings
    virtual SymTensor evaluateTensor(int gid);

    //! Get property format
    virtual PropertyFormat getFormat();

    //! Method to get Forchheimer constant for inertial drag
    virtual Real getForchheimerConstant();

    //! Set a data index where porosity data for the elements can be found
    virtual void setPorosity(DataIndex porosity);

  private:

    //! Don't permit copy or assignment operators
    //@{
    TabularPermeability(const TabularPermeability&);
    TabularPermeability& operator=(const TabularPermeability&);
    //@}

    Real m_forchheimer;

    DataContainer& m_mesh;

    Table* m_table;
};

}

#endif // TabularPermeability_h
