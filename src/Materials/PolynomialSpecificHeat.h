//******************************************************************************
//! \file    src/Materials/PolynomialSpecificHeat.h
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Polynomial Specific Heat
//******************************************************************************
#ifndef PolynomialSpecificHeat_h
#define PolynomialSpecificHeat_h

#include <cassert>

#include <Material.h>
#include <SpecificHeatProperty.h>
#include <Polynomial.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Polynomial Specific Heat
class PolynomialSpecificHeat: public SpecificHeatProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             PolynomialSpecificHeat(DataContainer& mesh,
                                    SpecificHeatType type,
                                    PropertyIndependentVar idepVar,
                                    const vector<Real> coeff,
                                    Real cmin, Real cmax);
    virtual ~PolynomialSpecificHeat();
    //@}

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate specific heat
    virtual Real evaluate(int gid);

    //! Evaluate specific heat
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Evaluate specific heat derivative
    virtual Real evaluateDerivative(int gid);

    //! Evaluate specific heat derivative
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives);

    //! Get property format
    virtual PropertyFormat getFormat();

  private:

    //! Don't permit copy or assignment operators
    //@{
    PolynomialSpecificHeat(const PolynomialSpecificHeat&);
    PolynomialSpecificHeat& operator=(const PolynomialSpecificHeat&);
    //@}

    DataContainer& m_mesh;

    PropertyIndependentVar m_idepVar;

    Polynomial* m_poly;
};

}
#endif // PolynomialSpecificHeat_h
