//******************************************************************************
//! \file    src/Materials/IdealGasEOS.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Ideal gas EOS
//******************************************************************************
#ifndef IdealGasEOS_h
#define IdealGasEOS_h

#include <EOS.h>

namespace Hydra {

class DataContainer;

class IdealGasEOS: public EOS {

  public:

    //! \name Constructor/Destructor
    //@{
    //! Create a IdealGasEOS
    //!   \param[in] mesh    DataContainer w. independent variables
    //!   \param[in] gamma   Ratio of specific heats
             IdealGasEOS(DataContainer& mesh, Real gamma);
    virtual ~IdealGasEOS();
    //@}

    //! Echo material property options
    //!   \param[in] ofs  Output file stream
    virtual void echoOptions(ostream& ofs);

    //! Evaluate internal energy
    //!   \param[in] T    Temperature
    //!   \return internal energy
    virtual Real evaluateInternalEnergy(Real T);

    //! Evaluate internal energy
    //!   \param[in] p    pressure
    //!   \param[in] rho  density
    //!   \return internal energy
    virtual Real evaluateInternalEnergy(Real p, Real rho);

    //! Evaluate pressure
    //!   \param[in] gid   Global id
    //!   \param[in] e     Internal energy
    //!   \param[in] rho   Density
    //!   \return pressure
    virtual Real evaluatePressure(Real e, Real rho);

    //! Evaluate sound speed
    //!   \param[in] e     Internal energy
    //!   \param[in] rho   Density
    //!   \param[in] p     Pressure
    //!   \return sound speed
    virtual Real evaluateSoundSpeed(Real e, Real rho, Real p);

    //! Evaluate temperature
    //!   \param[in] gid   Global id
    //!   \param[in] e     Internal energy
    //!   \return temperature
    virtual Real evaluateTemperature(int gids, Real e);

    //! Get EOS type
    virtual EOSType getEOSType();

    //! Get property format
    virtual PropertyFormat getFormat();

    //! Set specific heat object
    //!   \param[in] specificHeat   Pointer to specific heat object
    void setSpecificHeat(Real specificHeat);

  private:

    //! Don't permit copy or assignment operators
    //@{
    IdealGasEOS(const IdealGasEOS&);
    IdealGasEOS& operator=(const IdealGasEOS&);
    //@}

    DataContainer& m_mesh;

    Real m_Cv;
};

}
#endif
