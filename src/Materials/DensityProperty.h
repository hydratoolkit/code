//******************************************************************************
//! \file    src/Materials/DensityProperty.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   Abstract base class for Density
//******************************************************************************
#ifndef DensityProperty_h
#define DensityProperty_h

// Begin local includes
#include <MaterialProperty.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Abstract base class for density
class DensityProperty: public MaterialProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             DensityProperty();
    virtual ~DensityProperty();
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    DensityProperty(const DensityProperty&);
    DensityProperty& operator=(const DensityProperty&);
    //@}

};

}

#endif // DensityProperty_h
