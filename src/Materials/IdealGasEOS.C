//******************************************************************************
//! \file    src/Materials/IdealGasEOS.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   Ideal gas EOS
//******************************************************************************
#include <cassert>
#include <cmath>
#include <iostream>

using namespace std;

#include <IdealGasEOS.h>
#include <PrintUtil.h>

using namespace Hydra;

IdealGasEOS::IdealGasEOS(DataContainer& mesh, Real gamma) :
  EOS(gamma, gamma-1.0),
  m_mesh(mesh),
  m_Cv(0.0)
/*******************************************************************************
Routine: IdealGasEOS - constructor
Author : Mark A. Christon
*******************************************************************************/
{}

IdealGasEOS::~IdealGasEOS()
/*******************************************************************************
Routine: ~IdealGasEOS - destructor
Author : Mark A. Christon
*******************************************************************************/
{}

void
IdealGasEOS::echoOptions(ostream& ofs)
/*******************************************************************************
Routine: echoOPtions - echo material data
Author : Mark A. Christon
*******************************************************************************/
{
  PrintUtil pu(ofs);
  pu.printOption("Ideal Gas Equation of State");
  pu.printOption("   Ratio of specific heats", m_gamma);
}

Real
IdealGasEOS::evaluateInternalEnergy(Real T)
/*******************************************************************************
Routine: evaluateInternalEnergy - internal energy as if for an ideal gas

Relative to the reference temperature state

Author : Mark A. Christon
*******************************************************************************/
{
  assert(m_Cv > 0.0);
  return m_Cv*(T - m_Tref);
}

Real
IdealGasEOS::evaluateInternalEnergy(Real p, Real rho)
/*******************************************************************************
Routine: evaluatePressure - pressure for an ideal gas
Author : Mark A. Christon
*******************************************************************************/
{
  return p/((m_gamma - 1.0)*rho);
}

Real
IdealGasEOS::evaluatePressure(Real e, Real rho)
/*******************************************************************************
Routine: evaluatePressure - pressure for an ideal gas
Author : Mark A. Christon
*******************************************************************************/
{
  return (m_gamma - 1.0)*rho*e;
}

Real
IdealGasEOS::evaluateSoundSpeed(Real e, Real /*rho*/, Real /*p*/)
/*******************************************************************************
Routine: evaluateSoundSpeed - sonic velocity for a Gamma-law gas
Author : Mark A. Christon
*******************************************************************************/
{
  return sqrt(m_gamma*(m_gamma-1.0)*e);
}

Real
IdealGasEOS::evaluateTemperature(int /*gid*/, Real e)
/*******************************************************************************
Routine: evaluateTemperature - temperature for an ideal gas
Author : Mark A. Christon
*******************************************************************************/
{
  assert(m_Cv > 0.0);
  return e/m_Cv - m_Tref;
}

PropertyFormat
IdealGasEOS::getFormat()
/*******************************************************************************
Routine: getFormat - return material property data format
Author : Mark A. Christon
*******************************************************************************/
{
  return FUNCTIONAL_PROPERTY;
}

EOSType
IdealGasEOS::getEOSType()
/*******************************************************************************
Routine: getEOSType - return type of EOS
Author : Mark A. Christon
*******************************************************************************/
{
  return IDEAL_GAS_EOS;
}

void
IdealGasEOS::setSpecificHeat(Real specificHeat) {
/*******************************************************************************
Routine: setSpecificHeat - set value for specific heat
Author : Mark A. Christon
*******************************************************************************/
  m_Cv = specificHeat;
}

