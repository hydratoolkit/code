//******************************************************************************
//! \file    src/Materials/EOS.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Equation of state base class w. virtual interface
//******************************************************************************
#ifndef EOS_h
#define EOS_h

#include <MaterialProperty.h>

namespace Hydra {

//! Enumeration listing available equations of state
enum EOSType {
  IDEAL_GAS_EOS = 0,
  STIFFENED_GAS_EOS,
  MIE_GRUNEISEN_EOS,
  NUM_EOS
};

//! Base class for equations of state
class EOS: public MaterialProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             EOS(Real gamma, Real Gamma);
    virtual ~EOS();
    //@}

    //! Set a data index where density for the elements can be found
    //!   \param[in] density
    //!   Data index for internal energy (stored in mesh data container)
    virtual void setDensity(DataIndex density);

    //! Set a data index where internal energy for the elements can be found
    //!   \param[in] energy
    //!   Data index for internal energy (stored in mesh data container)
    virtual void setInternalEnergy(DataIndex energy);

    //! Set reference temperature
    //!   \param[in] Tref  Reference temperature
    void setReferenceTemperature(Real Tref);

    //! Set temperature data index
    //!   \param[in] temperature  Data index for temperature
    virtual void setTemperature(DataIndex temperature);

    //! Evaluate internal energy
    //!   \param[in] T    Temperature
    //!   \return internal energy
    virtual Real evaluateInternalEnergy(Real T) = 0;

    //! Evaluate internal energy
    //!   \param[in] p    pressure
    //!   \param[in] rho  density
    //!   \return internal energy
    virtual Real evaluateInternalEnergy(Real p, Real rho) = 0;

    //! Evaluate pressure
    //!   \param[in] e    Internal energy
    //!   \param[in] rho  Density
    //!   \return pressure
    virtual Real evaluatePressure(Real e, Real rho) = 0;

    //! Evaluate sound speed
    //!   \param[in] e     Internal energy
    //!   \param[in] rho   Density
    //!   \param[in] P     Pressure
    //!   \return sound speed
    virtual Real evaluateSoundSpeed(Real e, Real rho, Real P) = 0;

    //! Evaluate temperature
    //!   \param [in] gid    Global id number
    //!   \param[in] e       Internal energy
    //!   \return temperature
    virtual Real evaluateTemperature(int gids, Real e) = 0;

    //! Get EOS type
    virtual EOSType getEOSType() = 0;

    //! Slope of the Us-Up curve
    //!   \return slope
    virtual Real getUsUpSlope() {return 0.0;}

    //! Get the adiabatic exponent -- ratio of specific heats for ideal gases
    //!   \return gamma
    Real getAdiabaticExponent() {return m_gamma;}

    //! Get the Gruneisen parameter
    //!   \return Gamma
    Real getGruneisenParameter() {return m_Gamma;}

  protected:

    DataIndex m_internalEnergy;
    DataIndex m_density;
    DataIndex m_temperature;

    // For an ideal gas, the Gruneisen parameter,
    // m_Gamma = m_gamma - 1.0, or m_gamma = m_Gamma + 1.0
    Real m_gamma; //!< Adiabatic exponent - ratio of specific heats for ideal gas
    Real m_Gamma; //!< Gruneisen parameter
    Real m_Tref;  //!< Material reference temperature

  private:

    //! Don't permit copy or assignment operators
    //@{
    EOS(const EOS&);
    EOS& operator=(const EOS&);
    //@}

};

}

#endif // EOS_h
