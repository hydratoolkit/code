//******************************************************************************
//! \file    src/Materials/EllisMeterViscosity.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   Ellis-Meter Viscosity
//******************************************************************************
#include <cassert>
#include <string>
#include <vector>

using namespace std;

#include <EllisMeterViscosity.h>
#include <DataContainer.h>
#include <MathOp.h>
#include <PrintUtil.h>

using namespace Hydra;
using namespace Hydra::MathOp;

EllisMeterViscosity::EllisMeterViscosity(DataContainer& mesh,
                                         Real shearViscosityLow,
                                         Real shearViscosityHigh,
                                         Real tauHalf, Real n):
  m_mu_zero(shearViscosityLow),
  m_mu_inf(shearViscosityHigh),
  m_S_half(tauHalf),
  m_n(n),
  m_mesh(mesh)
/*******************************************************************************
Routine: EllisMeterViscosity - constructor
Author : Mark A. Christon
*******************************************************************************/
{}

EllisMeterViscosity::~EllisMeterViscosity()
/*******************************************************************************
Routine: ~EllisMeterViscosity - destructor
Author : Mark A. Christon
*******************************************************************************/
{}

void
EllisMeterViscosity::setTemperature(DataIndex /*temperature*/)
/*******************************************************************************
Routine: setTemperature - set data index for temperature
Author : Mark A. Christon
*******************************************************************************/
{}

void
EllisMeterViscosity::echoOptions(ostream& ofs)
/*******************************************************************************
Routine: echoOPtions - echo material data
Author : Mark A. Christon
*******************************************************************************/
{
  PrintUtil pu(ofs);
  pu.printOption("Ellis-Meter Viscosity");
  pu.printOption("   Shear Viscosity at low shear rates", m_mu_zero);
  pu.printOption("   Shear Viscosity at low shear rates", m_mu_inf);
  pu.printOption("   Tau 1/2", m_S_half);
  pu.printOption("   Flow behavior index", m_n);
}

Real
EllisMeterViscosity::evaluate(int /*gid*/)
/*******************************************************************************
Routine: evaluate - evaluate at given elements and temperatures
Author : Mark A. Christon
*******************************************************************************/
{
  return 0.0;
}

Real
EllisMeterViscosity::evaluateDerivative(int /*gid*/)
/*******************************************************************************
Routine: evaluate - evaluate derivative at given elements and temperatures
Author : Mark A. Christon
*******************************************************************************/
{
  return 0.0;
}

bool
EllisMeterViscosity::evaluate(int nel, const int* gids, Real* results,
                              Real* /*derivatives*/)
/*******************************************************************************
Routine: evaluate - evaluate at given elements and temperatures
Author : Mark A. Christon
*******************************************************************************/
{
  CTensor vgrad = m_mesh.getCTensor(m_VELOCITY_GRADIENT);
  Real mu_diff = m_mu_zero - m_mu_inf;
  Real b = (1.0 - m_n)/m_n;
  for (int i=0; i<nel; i++) {
    int gid = gids[i];
    SymTensor Sij;
    symmetric(gid, vgrad, Sij);
    Real S;
    normL2(2.0, Sij, S);
    Real SoS = S/m_S_half;
    results[gid] = m_mu_inf + mu_diff/(1.0 + pow(SoS, b));
  }
  return true;
}

bool
EllisMeterViscosity::evaluateDerivative(
  int /*nel*/, const int* /*gids*/, Real* /*derivatives*/)
/*******************************************************************************
Routine: evaulateDerivative - evaluate at given elements and temperatures
Author : Mark A. Christon
*******************************************************************************/
{
  return false;
}

PropertyFormat
EllisMeterViscosity::getFormat()
/*******************************************************************************
Routine: getFormat - return material property data format
Author : Mark A. Christon
*******************************************************************************/
{
  return FUNCTIONAL_PROPERTY;
}

ViscosityType
EllisMeterViscosity::getType()
/*******************************************************************************
Routine: getType - return viscosity type
Author : Mark A. Christon
*******************************************************************************/
{
  return ELLIS_METER_VISCOSITY;
}
