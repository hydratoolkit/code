//******************************************************************************
//! \file    src/Materials/ConstantDensity.h
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Constant Density
//******************************************************************************
#ifndef ConstantDensity_h
#define ConstantDensity_h

// Begin local includes
#include <DensityProperty.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Constant Density
class ConstantDensity: public DensityProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             ConstantDensity(Real a);
    virtual ~ConstantDensity();
    //@}

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate density at given temperature
    virtual Real evaluate(int gid);

    //! Evaluate density at given temperature
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Get property format
    virtual PropertyFormat getFormat();

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

  private:

    //! Don't permit copy or assignment operators
    //@{
    ConstantDensity(const ConstantDensity&);
    ConstantDensity& operator=(const ConstantDensity&);
    //@}

    Real m_rho;
};

}

#endif // ConstantDensity_h
