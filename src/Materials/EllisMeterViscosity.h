//******************************************************************************
//! \file    src/Materials/EllisMeterViscosity.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   Ellis-Meter Viscosity
//******************************************************************************
#ifndef EllisMeterViscosity_h
#define EllisMeterViscosity_h

#include <ViscosityProperty.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Ellis-Meter viscosity
class EllisMeterViscosity: public ViscosityProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             EllisMeterViscosity(DataContainer& mesh,
                                 Real shearViscosityLow,
                                 Real shearViscosityHigh,
                                 Real tauHalf,
                                 Real n);
             EllisMeterViscosity(DataContainer& mesh);
    virtual ~EllisMeterViscosity();
    //@}

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate viscosity at given temperature
    virtual Real evaluate(int gid);

    //! Evaluate derivative of viscosity at given temperature
    virtual Real evaluateDerivative(int gid);

    //! Evaluate viscosity
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Evaluate derivative of viscosity at given temperature
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives);

    //! Return viscosity type
    virtual ViscosityType getType();

    //! Get property format
    virtual PropertyFormat getFormat();

  private:

    //! Don't permit copy or assignment operators
    //@{
    EllisMeterViscosity(const EllisMeterViscosity&);
    EllisMeterViscosity& operator=(const EllisMeterViscosity&);
    //@}

    Real m_mu_zero;
    Real m_mu_inf;
    Real m_S_half;
    Real m_n;

    DataContainer& m_mesh;
};

}

#endif
