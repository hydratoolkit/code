//******************************************************************************
//! \file    src/Materials/ConstantConductivity.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   ConstantThermal conductivity
//******************************************************************************
#ifndef ConstantConductivity_h
#define ConstantConductivity_h

#include <DataShapes.h>
#include <ConductivityProperty.h>

namespace Hydra {

// Forward declarations
class DataContainer;
struct CSymTensor;

//! Constant Conductivity
class ConstantConductivity: public ConductivityProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             ConstantConductivity(int ctype, const Real* k);
    virtual ~ConstantConductivity();
    //@}

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate at current settings
    virtual Real evaluate(int gid);

    //! Evaluate isotropic conductivity at current settings
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Evaluate at current settings
    virtual Real evaluateDerivative(int gid);

    //! Evaluate isotropic conductivity derivative at current settings
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives);

    //! Evaluate at current settings
    virtual SymTensor evaluateTensor(int gid);

    //! Get property format
    virtual PropertyFormat getFormat();

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

  private:

    //! Don't permit copy or assignment operators
    //@{
    ConstantConductivity(const ConstantConductivity&);
    ConstantConductivity& operator=(const ConstantConductivity&);
    //@}

    SymTensor m_k;
};

}
#endif // ConstantConductivity_h
