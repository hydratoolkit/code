//******************************************************************************
//! \file    src/Materials/ElasticStrength.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Elastic material strength constitutive model
//******************************************************************************
#ifndef ElasticStrength_h
#define ElasticStrength_h

#include <StrengthProperty.h>

namespace Hydra {

//! Elastic Strength Constitutive Model
class ElasticStrength: public StrengthProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             ElasticStrength(Real E, Real nu);
    virtual ~ElasticStrength();
    //@}

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Get property format
    virtual PropertyFormat getFormat();

    //! Plastic modulus, i.e., slope of yield-stress vs. strain curve
    //   \param[in] e      internal energy
    //   \param[in] epsp   plastic strain
    //   \param[in] epsdot plastic strain-rate
    virtual Real plasticModulus(Real e, Real epsp, Real epsdot);

    // Yield stress
    //   \param[in] e      internal energy
    //   \param[in] epsp   plastic strain
    //   \param[in] epsdot plastic strain-rate
    virtual Real yieldStress(Real e, Real epsp, Real epsdot);

    // Radial return
    //  \param[in] nel     # of elements in element class (block)
    //  \param[in] gids    Global Id's for element storage
    //  \param[in] dt      Time-step
    //  \param[in] e       Specific internal energy
    //  \param[in] epsp    effective plastic strain
    //  \param[in] epspdot effective plastic strain-rate
    //  \param[in] S       deviatoric stress
    virtual void radialReturn(int nel, Real dt, int* gids, Real* e,
                              Real* epsp, Real* epspdot, CSymTensor& S);

  private:

    //! Don't permit copy or assignment operators
    //@{
    ElasticStrength(const ElasticStrength&);
    ElasticStrength& operator=(const ElasticStrength&);
    //@}

    Real m_nu;  //!< Poisson's ratio
    Real m_K;   //!< Bulk modulus derived from E, nu

};

}

#endif // ElasticStrength_h
