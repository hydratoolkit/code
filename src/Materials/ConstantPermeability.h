//******************************************************************************
//! \file    src/Materials/ConstantPermeability.h
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Constant Permeability
//******************************************************************************
#ifndef ConstantPermeability_h
#define ConstantPermeability_h

#include <PermeabilityProperty.h>
#include <DataShapes.h>

namespace Hydra {

class DataContainer;
struct CSymTensor;

//! Constant Permeability
class ConstantPermeability : public PermeabilityProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             ConstantPermeability(int ctype, const Real* k);
    virtual ~ConstantPermeability();
    //@}

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate at current settings
    virtual Real evaluate(int gid);

    //! Evaluate isotropic permeability at current settings
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Evaluate at current settings
    virtual Real evaluateDerivative(int gid);

    //! Evaluate isotropic permeability derivative at current settings
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives);

    //! Evaluate at current settings
    virtual SymTensor evaluateTensor(int gid);

    //! Method to get Forchheimerant for inertial drag
    virtual Real getForchheimerConstant();

    //! Get property format
    virtual PropertyFormat getFormat();

  private:

    //! Don't permit copy or assignment operators
    //@{
    ConstantPermeability(const ConstantPermeability&);
    ConstantPermeability& operator=(const ConstantPermeability&);
    //@}

    SymTensor m_k;
    Real m_forchheimer;
};

}

#endif // ConstantPermeability_h
