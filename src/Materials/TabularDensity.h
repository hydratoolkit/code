//******************************************************************************
//! \file    src/Materials/TabularDensity.h
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Tabular Density
//******************************************************************************
#ifndef TabularDensity_h
#define TabularDensity_h

#include <vector>

#include <DensityProperty.h>
#include <HydraTypes.h>
#include <Material.h>
#include <Table.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Tabular Density
class TabularDensity: public DensityProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             TabularDensity(DataContainer& mesh,
                            PropertyIndependentVar idepVar,
                            const vector<Real> lct,
                            const vector<Real> lcv);
    virtual ~TabularDensity();
    //@}

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate density at given temperature
    virtual Real evaluate(int gid);

    //! Evaluate density at given temperature
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Get property format
    virtual PropertyFormat getFormat();

  private:

    //! Don't permit copy or assignment operators
    //@{
    TabularDensity(const TabularDensity&);
    TabularDensity& operator=(const TabularDensity&);
    //@}

    DataContainer& m_mesh;

    PropertyIndependentVar m_idepVar;

    Table* m_table;
};

}

#endif
