//******************************************************************************
//! \file    src/Materials/TabularExpansion.h
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Tabular coefficient of thermal expansion
//******************************************************************************
#ifndef TabularExpansion_h
#define TabularExpansion_h

#include <vector>

#include <ExpansionProperty.h>
#include <HydraTypes.h>
#include <Material.h>
#include <Table.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Tabular thermal expansion
class TabularExpansion : public ExpansionProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             TabularExpansion(DataContainer& mesh,
                              PropertyIndependentVar idepVar,
                              const vector<Real> lct,
                              const vector<Real> lcv);
    virtual ~TabularExpansion();
    //@}

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate expansion at given temperature
    virtual Real evaluate(int gid);

    //! Evaluate expansion
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Get property format
    virtual PropertyFormat getFormat();

  private:

    //! Don't permit copy or assignment operators
    //@{
    TabularExpansion(const TabularExpansion&);
    TabularExpansion& operator=(const TabularExpansion&);
    //@}

    DataContainer& m_mesh;

    PropertyIndependentVar m_idepVar;

    Table* m_table;
};

}

#endif // TabularExpansion_h
