//******************************************************************************
//! \file    src/Materials/PermeabilityProperty.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   Permeability
//******************************************************************************
#ifndef PermeabilityProperty_h
#define PermeabilityProperty_h

#include <MaterialProperty.h>
#include <DataShapes.h>

namespace Hydra {

class DataContainer;
struct CSymTensor;

enum PermeabilityType {
  ISOTROPIC_PERMEABILITY = 0,
  ORTHOTROPIC_PERMEABILITY,
  ANISOTROPIC_PERMEABILITY
};

//! Base class for permeability
class PermeabilityProperty : public MaterialProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             PermeabilityProperty(int ctype);
    virtual ~PermeabilityProperty();
    //@}

    //! Evaluate derivative at current settings
    virtual Real evaluateDerivative(int gid) = 0;

    //! Evaluate isotropic permeability derivative at current settings
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives) = 0;

    //! Evaluate at current settings
    virtual SymTensor evaluateTensor(int gid) = 0;

    //! Get Forchheimer constant for inertial drag evaluation
    virtual Real getForchheimerConstant() = 0;

    //! Set a data index where porosity data for the elements can be found
    virtual void setPorosity(DataIndex porosity);

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

  protected:

    int m_type;

    DataIndex m_POROSITY;

  private:

    //! Don't permit copy or assignment operators
    //@{
    PermeabilityProperty(const PermeabilityProperty&);
    PermeabilityProperty& operator=(const PermeabilityProperty&);
    //@}

};

}

#endif // PermeabilityProperty_h
