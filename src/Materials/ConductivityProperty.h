//******************************************************************************
//! \file    src/Materials/ConductivityProperty.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   Thermal conductivity
//******************************************************************************
#ifndef ConductivityProperty_h
#define ConductivityProperty_h

#include <MaterialProperty.h>
#include <DataShapes.h>

namespace Hydra {

// Forward declarations
class DataContainer;
struct CSymTensor;

enum ThermalConductivityType {
  ISOTROPIC_CONDUCTIVITY = 0,
  ORTHOTROPIC_CONDUCTIVITY,
  ANISOTROPIC_CONDUCTIVITY
};

//! Base conductivity property
class ConductivityProperty : public MaterialProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             ConductivityProperty(int ctype);
    virtual ~ConductivityProperty();
    //@}

    //! Evaluate derivative at current settings
    virtual Real evaluateDerivative(int gid) = 0;

    //! Evaluate isotropic conductivity derivative at current settings
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives) = 0;

    //! Evaluate at current settings
    virtual SymTensor evaluateTensor(int gid) = 0;

  protected:

    int m_type;

  private:

    //! Don't permit copy or assignment operators
    //@{
    ConductivityProperty(const ConductivityProperty&);
    ConductivityProperty& operator=(const ConductivityProperty&);
    //@}

};

}
#endif // ConductivityProperty_h
