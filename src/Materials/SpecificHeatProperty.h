//******************************************************************************
//! \file    src/Materials/SpecificHeatProperty.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   Specific Heat
//******************************************************************************
#ifndef SpecificHeatProperty_h
#define SpecificHeatProperty_h

#include <cassert>

#include <MaterialProperty.h>

namespace Hydra {

// Forward declarations
class DataContainer;

enum SpecificHeatType {
  CONSTANT_PRESSURE = 0,
  CONSTANT_VOLUME
};

//! Base class for specific heat
class SpecificHeatProperty : public MaterialProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             SpecificHeatProperty(int shType);
    virtual ~SpecificHeatProperty();
    //@}

    //! Evaluate derivative
    virtual Real evaluateDerivative(int gid) = 0;

    //! Evaluate derivative
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives) = 0;

    //! Get property format
    virtual PropertyFormat getFormat() = 0;

  protected:

    int m_type; // Specific heat type

  private:

    //! Don't permit copy or assignment operators
    //@{
    SpecificHeatProperty(const SpecificHeatProperty&);
    SpecificHeatProperty& operator=(const SpecificHeatProperty&);
    //@}

};

}

#endif // SpecificHeatProperty_h
