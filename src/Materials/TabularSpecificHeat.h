//******************************************************************************
//! \file    src/Materials/TabularSpecificHeat.h
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Tabular Specific Heat
//******************************************************************************
#ifndef TabularSpecificHeat_h
#define TabularSpecificHeat_h

#include <cassert>

#include <Material.h>
#include <SpecificHeatProperty.h>
#include <Table.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Tabular Specific Heat
class TabularSpecificHeat: public SpecificHeatProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             TabularSpecificHeat(DataContainer& mesh,
                                 SpecificHeatType type,
                                 PropertyIndependentVar idepVar,
                                 const vector<Real> lct,
                                 const vector<Real> lcv);
    virtual ~TabularSpecificHeat();
    //@}

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate specific heat
    virtual Real evaluate(int gid);

    //! Evaluate specific heat
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Evaluate specific heat derivative
    virtual Real evaluateDerivative(int gid);

    //! Evaluate specific heat derivative
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives);

    //! Get property format
    virtual PropertyFormat getFormat();

  private:

    //! Don't permit copy or assignment operators
    //@{
    TabularSpecificHeat(const TabularSpecificHeat&);
    TabularSpecificHeat& operator=(const TabularSpecificHeat&);
    //@}

    DataContainer& m_mesh;

    PropertyIndependentVar m_idepVar;

    Table* m_table;
};

}
#endif // TabularSpecificHeat_h
