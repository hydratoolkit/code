# ############################################################################ #
#
# Library: Materials
# File Definition File
#
# ############################################################################ #

# Source Files

set(Materials_SOURCE_FILES
    CarreauYasudaViscosity.C
    CarmanKozenyPermeability.C
    ConductivityProperty.C
    ConstantConductivity.C
    ConstantDensity.C
    ConstantExpansion.C
    ConstantPermeability.C
    ConstantSpecificHeat.C
    ConstantViscosity.C
    CrossViscosity.C
    DensityProperty.C
    ElasticStrength.C
    EllisMeterViscosity.C
    EOS.C
    ExpansionProperty.C
    HerschelBulkleyViscosity.C
    IdealGasEOS.C
    JohnsonCookStrength.C
    Material.C
    MaterialProperty.C
    MieGruneisenEOS.C
    PermeabilityProperty.C
    PowellEyringViscosity.C
    PowerLawViscosity.C
    PrandtlReussStrength.C
    SpecificHeatProperty.C
    StiffenedGasEOS.C
    StrengthProperty.C
    SutherlandViscosity.C
    PolynomialConductivity.C
    PolynomialDensity.C
    PolynomialSpecificHeat.C
    PolynomialViscosity.C
    TabularConductivity.C
    TabularDensity.C
    TabularExpansion.C
    TabularPermeability.C
    TabularSpecificHeat.C
    TabularViscosity.C
    ViscosityProperty.C
)
