//******************************************************************************
//! \file    src/Materials/PrandtlReussStrength.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Prandtl-Reuss material strength constitutive model
//******************************************************************************
#ifndef PrandtlReussStrength_h
#define PrandtlReussStrength_h

#include <StrengthProperty.h>

namespace Hydra {

//! Prandtl-Reuss Strength Constitutive Model
class PrandtlReussStrength: public StrengthProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             PrandtlReussStrength(Real G, Real A, Real B, Real n);
    virtual ~PrandtlReussStrength();
    //@}

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Get property format
    virtual PropertyFormat getFormat();

    //! Plastic modulus, i.e., slope of yield-stress vs. strain curve
    //   \param[in] e      internal energy
    //   \param[in] epsp   plastic strain
    //   \param[in] epsdot plastic strain-rate
    virtual Real plasticModulus(Real e, Real epsp, Real epsdot);

    // Yield stress
    //   \param[in] e      internal energy
    //   \param[in] epsp   plastic strain
    //   \param[in] epsdot plastic strain-rate
    virtual Real yieldStress(Real e, Real epsp, Real epsdot);

  private:

    //! Don't permit copy or assignment operators
    //@{
    PrandtlReussStrength(const PrandtlReussStrength&);
    PrandtlReussStrength& operator=(const PrandtlReussStrength&);
    //@}

    Real m_A;  //!< Yield stress constant
    Real m_B;  //!< Multiplier for plastic strain in yield stress
    Real m_n;  //!< Plastic strain exponent in yield stress

};

}

#endif // PrandtlReussStrength_h
