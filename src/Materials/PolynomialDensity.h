//******************************************************************************
//! \file    src/Materials/PolynomialDensity.h
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Polynomial Density
//******************************************************************************
#ifndef PolynomialDensity_h
#define PolynomialDensity_h

#include <vector>

#include <DensityProperty.h>
#include <HydraTypes.h>
#include <Material.h>
#include <Polynomial.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Polynomial Density
class PolynomialDensity: public DensityProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             PolynomialDensity(DataContainer& mesh,
                               PropertyIndependentVar idepVar,
                               const vector<Real> coeff,
                               Real rhomin, Real rhomax);

    virtual ~PolynomialDensity();
    //@}

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate density at given temperature
    virtual Real evaluate(int gid);

    //! Evaluate density at given temperature
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Get property format
    virtual PropertyFormat getFormat();

  private:

    //! Don't permit copy or assignment operators
    //@{
    PolynomialDensity(const PolynomialDensity&);
    PolynomialDensity& operator=(const PolynomialDensity&);
    //@}

    DataContainer& m_mesh;

    PropertyIndependentVar m_idepVar;

    Polynomial* m_poly;
};

}

#endif
