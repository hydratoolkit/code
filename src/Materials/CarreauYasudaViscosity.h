//******************************************************************************
//! \file    src/Materials/CarreauYasudaViscosity.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   Carreau-Yasuda viscosity
//******************************************************************************
#ifndef CarreauYasudaViscosity_h
#define CarreauYasudaViscosity_h

#include <ViscosityProperty.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Carreau - Yasuda viscosity
class CarreauYasudaViscosity: public ViscosityProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             CarreauYasudaViscosity(DataContainer& mesh,
                                    Real shearViscosityLow,
                                    Real shearViscosityHigh,
                                    Real timeConstant,
                                    Real n, Real a);
             CarreauYasudaViscosity(DataContainer& mesh);
    virtual ~CarreauYasudaViscosity();
    //@}

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate viscosity at given temperature
    virtual Real evaluate(int gid);

    //! Evaluate derivative of viscosity at given temperature
    virtual Real evaluateDerivative(int gid);

    //! Evaluate viscosity
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Evaluate derivative of viscosity at given temperature
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives);

    //! Return viscosity type
    virtual ViscosityType getType();

    //! Get property format
    virtual PropertyFormat getFormat();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CarreauYasudaViscosity(const CarreauYasudaViscosity&);
    CarreauYasudaViscosity& operator=(const CarreauYasudaViscosity&);
    //@}

    Real m_mu_zero;
    Real m_mu_inf;
    Real m_lambda;
    Real m_n;
    Real m_a;

    DataContainer& m_mesh;
};

}

#endif
