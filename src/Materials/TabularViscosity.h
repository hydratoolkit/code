//******************************************************************************
//! \file    src/Materials/TabularViscosity.h
//! \author  Mark A. Christon
//! \date    Wed Jul 19 13:54:45 MDT 2017
//! \brief   Tabular viscosity
//******************************************************************************
#ifndef TabularViscosity_h
#define TabularViscosity_h

#include <vector>

#include <HydraTypes.h>
#include <ViscosityProperty.h>
#include <Material.h>
#include <Table.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Tabular Viscosity
class TabularViscosity: public ViscosityProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             TabularViscosity(DataContainer& mesh,
                              PropertyIndependentVar idepVar,
                              const vector<Real> lct,
                              const vector<Real> lcv);
    virtual ~TabularViscosity();
    //@}

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate viscosity at given temperature
    virtual Real evaluate(int gid);

    //! Evaluate derivative of viscosity at given temperature
    virtual Real evaluateDerivative(int gid);

    //! Evaluate viscosity
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Evaluate derivative of viscosity at given temperature
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives);

    //! Return viscosity type
    virtual ViscosityType getType();

    //! Get property format
    virtual PropertyFormat getFormat();

  private:

    //! Don't permit copy or assignment operators
    //@{
    TabularViscosity(const TabularViscosity&);
    TabularViscosity& operator=(const TabularViscosity&);
    //@}

    DataContainer& m_mesh;

    PropertyIndependentVar m_idepVar;

    Table* m_table;
};

}

#endif
