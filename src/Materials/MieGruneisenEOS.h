//******************************************************************************
//! \file    src/Materials/MieGruneisenEOS.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Mie-Gruneisen EOS
//******************************************************************************
#ifndef MieGruneisenEOS_h
#define MieGruneisenEOS_h

#include <EOS.h>

namespace Hydra {

class DataContainer;

class MieGruneisenEOS: public EOS {

  public:

    //! \name Constructor/Destructor
    //@{
    //! Create a MieGruneisenEOS
    //!   \param[in] mesh    DataContainer w. independent variables
    //!   \param[in] Gamma   Gruneisen parameter
    //!   \param[in] c0      Sound speed in undisturbed material
    //!   \param[in] s       Slope of us-up curve
             MieGruneisenEOS(DataContainer& mesh,
                             Real rho0, Real Gamma, Real c0, Real s);
    virtual ~MieGruneisenEOS();
    //@}

    //! Echo material property options
    //!   \param[in] ofs  Output file stream
    virtual void echoOptions(ostream& ofs);

    //! Evaluate internal energy
    //!   \param[in] T    Temperature
    //!   \return internal energy
    virtual Real evaluateInternalEnergy(Real T);

    //! Evaluate internal energy
    //!   \param[in] p    pressure
    //!   \param[in] rho  density
    //!   \return internal energy
    virtual Real evaluateInternalEnergy(Real p, Real rho);

    //! Evaluate pressure
    //!   \param[in] gid   Global id
    //!   \param[in] e     Internal energy
    //!   \param[in] rho   Density
    //!   \return pressure
    virtual Real evaluatePressure(Real e, Real rho);

    //! Evaluate sound speed
    //!   \param[in] e     Internal energy
    //!   \param[in] rho   Density
    //!   \param[in] p     Pressure
    //!   \return sound speed
    virtual Real evaluateSoundSpeed(Real e, Real rho, Real p);

    //! Evaluate temperature
    //!   \param[in] gid   Global id
    //!   \param[in] e     Internal energy
    //!   \return temperature
    virtual Real evaluateTemperature(int gids, Real e);

    //! Get EOS type
    virtual EOSType getEOSType();

    //! Slope of the Us-Up curve
    //!   \return slope
    virtual Real getUsUpSlope() {return m_s;}

    //! Get property format
    virtual PropertyFormat getFormat();

    //! Set specific heat object
    //!   \param[in] specificHeat   Pointer to specific heat object
    void setSpecificHeat(Real specificHeat);

  private:

    //! Don't permit copy or assignment operators
    //@{
    MieGruneisenEOS(const MieGruneisenEOS&);
    MieGruneisenEOS& operator=(const MieGruneisenEOS&);
    //@}

    DataContainer& m_mesh;

    Real m_rho0;  //!< Reference density
    Real m_c0;    //!< Reference longitudinal or bulk sound speed
    Real m_s;     //!< Slope of Us-Up curve
    Real m_Cv;    //!< Specific heat at constant specific volume
};

}
#endif
