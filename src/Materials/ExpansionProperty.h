//******************************************************************************
//! \file    src/Materials/ExpansionProperty.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   Abstract base class for coefficient of thermal expansion
//******************************************************************************
#ifndef ExpansionProperty_h
#define ExpansionProperty_h

#include <MaterialProperty.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Abstract base class for thermal expansion
class ExpansionProperty : public MaterialProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             ExpansionProperty();
    virtual ~ExpansionProperty();
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    ExpansionProperty(const ExpansionProperty&);
    ExpansionProperty& operator=(const ExpansionProperty&);
    //@}

};

}

#endif // ExpansionProperty_h
