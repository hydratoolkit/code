//******************************************************************************
//! \file    src/Materials/ConstantViscosity.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   Constant viscosity
//******************************************************************************
#ifndef ConstantViscosity_h
#define ConstantViscosity_h

#include <ViscosityProperty.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Constant Viscosity
class ConstantViscosity: public ViscosityProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             ConstantViscosity(Real v);
    virtual ~ConstantViscosity();
    //@}

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate viscosity at given temperature
    virtual Real evaluate(int gid);

    //! Evaluate derivative of viscosity at given temperature
    virtual Real evaluateDerivative(int gid);

    //! Evaluate viscosity
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Evaluate derivative of viscosity at given temperature
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives);

    //! Return viscosity type
    virtual ViscosityType getType();

    //! Get property format
    virtual PropertyFormat getFormat();

private:

    //! Don't permit copy or assignment operators
    //@{
    ConstantViscosity(const ConstantViscosity&);
    ConstantViscosity& operator=(const ConstantViscosity&);
    //@}

    Real m_v;
};

}

#endif
