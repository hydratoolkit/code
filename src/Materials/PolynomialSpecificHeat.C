//******************************************************************************
//! \file    src/Materials/PolynomialSpecificHeat.C
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Specific Heat
//******************************************************************************
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include <PolynomialSpecificHeat.h>
#include <DataContainer.h>
#include <PrintUtil.h>

using namespace Hydra;

PolynomialSpecificHeat::PolynomialSpecificHeat(DataContainer& mesh,
                                               SpecificHeatType type,
                                               PropertyIndependentVar idepVar,
                                               const vector<Real> coeff,
                                               Real cmin, Real cmax) :
  SpecificHeatProperty(type),
  m_mesh(mesh),
  m_idepVar(idepVar)
/*******************************************************************************
Routine: PolynomialSpecificHeat - constructor
Author : Mark A. Christon
*******************************************************************************/
{
  m_poly = new Polynomial(9000, coeff, cmin, cmax);
}

PolynomialSpecificHeat::~PolynomialSpecificHeat()
/*******************************************************************************
Routine: ~PolynomialSpecificHeat - destructor
Author : Mark A. Christon
*******************************************************************************/
{
  delete m_poly;
}

void
PolynomialSpecificHeat::echoOptions(ostream& ofs)
/*******************************************************************************
Routine: echoOPtions - echo specific heat data
Author : Mark A. Christon
*******************************************************************************/
{
  PrintUtil pu(ofs);
  if (m_type == CONSTANT_PRESSURE) {
    pu.printOption("Polynomial Specific Heat at Constant Pressure");
    pu.printOption("  Polynomial Degree", m_poly->degree());
    m_poly->echoOptions(ofs);
  }
  else {
    pu.printOption("Polynomial Specific Heat at Constant Volume");
    pu.printOption("  Polynomial Degree", m_poly->degree());
    m_poly->echoOptions(ofs);
  }
}

Real
PolynomialSpecificHeat::evaluate(int /*gid*/)
/*******************************************************************************
Routine: evaluate - evaluate specific heat
Author : Mark A. Christon
*******************************************************************************/
{
#ifdef DONT
  Real cp = 0.0;
  int lid = 0;
  m_poly->evaluate(m_mesh, 1, &lid, &gid, &cp);
  return cp;
#endif
  return 1.0;
}

bool
PolynomialSpecificHeat::evaluate(int nel, const int* gids,
                              Real* results, Real* /*derivatives*/)
/*******************************************************************************
Routine: evaluate - evaluate specific heat
Author : Mark A. Christon
*******************************************************************************/
{
  Real* T = m_mesh.getVariable<Real>(m_TEMPERATURE[0]);
  for (int i = 0; i < nel; i++) {
    results[gids[i]] = m_poly->evaluate(T[gids[i]]);
  }
  return true;
}

Real
PolynomialSpecificHeat::evaluateDerivative(int /*gid*/)
/*******************************************************************************
Routine: evaluateCp - evaluate specific heat derivative
Author : Mark A. Christon
*******************************************************************************/
{
  Real deriv = 0.0;
#ifdef DONT
  Real cp = 0.0;
  int lid = 0;
  m_poly->evaluate(m_mesh, 1, &lid, &gid, &cp, &deriv);
  return deriv;
#endif
  return deriv;
}

bool
PolynomialSpecificHeat::evaluateDerivative(int /*nel*/,
                                        const int* /*gids*/,
                                        Real* /*derivatives*/)
/*******************************************************************************
Routine: evaluateCp - evaluate specific heat derivative at constant pressure
Author : Mark A. Christon
*******************************************************************************/
{
#ifdef DONT
  return m_poly->evaluateDerivative(m_mesh, nel, gids, gids, derivatives);
#endif
  return true;
}

PropertyFormat
PolynomialSpecificHeat::getFormat()
/*******************************************************************************
Routine: getFormat - return material property data format
Author : Mark A. Christon
*******************************************************************************/
{
  return POLYNOMIAL_PROPERTY;
}

void
PolynomialSpecificHeat::setTemperature(DataIndex /*temperature*/)
/*******************************************************************************
Routine: setTemperature - set data index for temperature
Author : Mark A. Christon
*******************************************************************************/
{
}
