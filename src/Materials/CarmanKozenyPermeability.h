//******************************************************************************
//! \file    src/Materials/CarmanKozenyPermeability.h
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Carman-Kozeny Permeability
//******************************************************************************
#ifndef CarmanKozenyPermeability_h
#define CarmanKozenyPermeability_h

#include <PermeabilityProperty.h>
#include <DataShapes.h>

namespace Hydra {

class DataContainer;
struct CSymTensor;

//! Carman Kozeny Permeability
class CarmanKozenyPermeability: public PermeabilityProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             CarmanKozenyPermeability(DataContainer& mesh, int ctype,
                                      Real cozenyconstant, Real poreradius);
    virtual ~CarmanKozenyPermeability();
    //@}

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate at current settings
    virtual Real evaluate(int gid);

    //! Evaluate at current settings
    virtual SymTensor evaluateTensor(int gid);

    //! Evaluate isotropic permeability at current settings
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Evaluate at current settings
    virtual Real evaluateDerivative(int gid);

    //! Evaluate isotropic permeability derivative at current settings
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives);

    //! Method to get Forchheimer constant for inertial drag
    virtual Real getForchheimerConstant();

    //! Get property format
    virtual PropertyFormat getFormat();

  private:

    //! Don't permit copy or assignment operators
    //@{
    CarmanKozenyPermeability (const CarmanKozenyPermeability&);
    CarmanKozenyPermeability& operator= (const CarmanKozenyPermeability&);
    //@}

    SymTensor m_k;
    Real m_kozenyconstant;
    Real m_poreradius;
    Real m_forchheimer;
    DataContainer& m_mesh;
};

}

#endif // CarmanKozenyPermeability_h
