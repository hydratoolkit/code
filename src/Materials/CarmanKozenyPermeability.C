//******************************************************************************
//! \file    src/Materials/CarmanKozenyPermeability.C
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Permeability
//******************************************************************************
#include <cassert>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include <CarmanKozenyPermeability.h>
#include <DataContainer.h>
#include <DataShapes.h>
#include <Exception.h>
#include <PrintUtil.h>

using namespace Hydra;

CarmanKozenyPermeability::CarmanKozenyPermeability(DataContainer& mesh,
                                                   int ctype,
                                                   Real cozenyconstant,
                                                   Real poreradius):
  PermeabilityProperty(ctype),
  m_kozenyconstant(cozenyconstant),
  m_poreradius(poreradius),
  m_mesh(mesh)
/*******************************************************************************
Routine: CarmenKoznePermeabilisty - constructor
Author : Mark A. Christon
*******************************************************************************/
{
  // Permeability tensor
  m_k.XX = 1.E+30;
  m_k.XY = 0.0;
  m_k.YY = 1.E+30;
  m_k.ZX = 0.0;
  m_k.YZ = 0.0;
  m_k.ZZ = 1.E+30;

  // Forchheimer constant
  m_forchheimer = 1.75/sqrt(150.0);
}

CarmanKozenyPermeability::~CarmanKozenyPermeability()
/*******************************************************************************
Routine: CarmenKoznePermeabilisty - constructor
Author : Mark A. Christon
*******************************************************************************/
{}

void
CarmanKozenyPermeability::echoOptions(ostream& ofs)
/*******************************************************************************
Routine: echoOPtions - echo material data
Author : Mark A. Christon
*******************************************************************************/
{
  PrintUtil pu(ofs);
  pu.printOption("Porous Media: Carman-Kozeny model");
  pu.printOption("   Pore Particle/Fiber Radius", m_poreradius);
  pu.printOption("   Carman Kozeny Constant", m_kozenyconstant);
}

Real
CarmanKozenyPermeability::evaluate(int gid)
/*******************************************************************************
Routine: evaluate - evaluate at given elements and temperatures
Author : Mark A. Christon
*******************************************************************************/
{
  // Haul the porosity array using "porosity" DataIndex from the data
  // container "m_mesh"
  Real* epsilon = m_mesh.getVariable<Real>(m_POROSITY);

  // Use the Carman-Kozeny relation to evaluate permeability
  Real eps3 = pow(epsilon[gid],3);
  Real solidFrac2 = (1-epsilon[gid])*(1-epsilon[gid]);
  Real rf = m_poreradius;
  Real kConst = m_kozenyconstant;
  Real permeability = 0.25*rf*rf/(kConst);
  permeability *= eps3/(solidFrac2);

  return permeability;
}

SymTensor
CarmanKozenyPermeability::evaluateTensor(int gid)
/*******************************************************************************
Routine: evaluateTensor - evaluate at given elements and temperatures
Author : Mark A. Christon
*******************************************************************************/
{
  // Haul the porosity array using "porosity" DataIndex from the data
  // container "m_mesh"
  Real* epsilon = m_mesh.getVariable<Real>(m_POROSITY);
  SymTensor perm;

  // Use the Carman-Kozeny relation to evaluate permeability
  Real eps3 = pow(epsilon[gid],3);
  Real solidFrac2 = (1-epsilon[gid])*(1-epsilon[gid]);
  Real rf = m_poreradius;
  Real kConst = m_kozenyconstant;
  Real permeability = 0.25*rf*rf/(kConst);
  permeability *= eps3/(solidFrac2);

  perm.XX = permeability;
  perm.XY = 0.0;
  perm.YY = permeability;
  perm.ZX = 0.0;
  perm.YZ = 0.0;
  perm.ZZ = permeability;

  return perm;
}

bool
CarmanKozenyPermeability::evaluate(int nel,
                                   const int* gids,
                                   Real* results,
                                   Real* derivatives)
/*******************************************************************************
Routine: evaluate - evaluate at given elements and temperatures
Author : Mark A. Christon
*******************************************************************************/
{
  // Haul the porosity array using "porosity" DataIndex from the data
  // container "m_mesh"
  Real* epsilon = m_mesh.getVariable<Real>(m_POROSITY);

  for (int i = 0; i < nel; i++) {
    // Use the Carman-Kozeny relation to evaluate permeability
    Real eps3 = pow(epsilon[gids[i]],3);
    Real solidFrac2 = (1-epsilon[gids[i]])*(1-epsilon[gids[i]]);
    Real rf = m_poreradius;
    Real kConst = m_kozenyconstant;
    Real permeability = 0.25*rf*rf/(kConst);
    permeability *= eps3/(solidFrac2);
    results[gids[i]] = permeability;
  }
  if (derivatives != 0) {
    for (int j = 0; j < nel; j++) derivatives[gids[j]] = 0.0;
  }
  return true;
}

Real
CarmanKozenyPermeability::evaluateDerivative(int /*gid*/)
/*******************************************************************************
Routine: evaulateDerivative - evaluate at given elements and temperatures
Author : Mark A. Christon
*******************************************************************************/
{
  return 0.0;
}

bool
CarmanKozenyPermeability::evaluateDerivative(int nel, const int* gids,
                                                     Real* derivatives)
/*******************************************************************************
Routine: evaulateDerivative - evaluate at given elements and temperatures
Author : Mark A. Christon
*******************************************************************************/
{
  for (int j = 0; j < nel; j++)
    derivatives[gids[j]] = 0.0;
  return true;
}

PropertyFormat
CarmanKozenyPermeability::getFormat()
/*******************************************************************************
Routine: getFormat - return material property data format
Author : Mark A. Christon
*******************************************************************************/
{
  return FUNCTIONAL_PROPERTY;
}

Real
CarmanKozenyPermeability::getForchheimerConstant()
/*******************************************************************************
Routine: getFormat - return material property data format
Author : Mark A. Christon
*******************************************************************************/
{
  return m_forchheimer;
}
