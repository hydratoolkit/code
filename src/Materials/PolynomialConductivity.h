//******************************************************************************
//! \file    src/Materials/PolynomialConductivity.h
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Polynomial Thermal conductivity
//******************************************************************************
#ifndef PolynomialConductivity_h
#define PolynomialConductivity_h

#include <vector>

#include <HydraTypes.h>
#include <ConductivityProperty.h>
#include <DataShapes.h>
#include <Material.h>
#include <Polynomial.h>

namespace Hydra {

// Forward declarations
class DataContainer;
struct CSymTensor;

//! Polynomial Conductivity
class PolynomialConductivity: public ConductivityProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             PolynomialConductivity(DataContainer& mesh,
                                    ThermalConductivityType type,
                                    PropertyIndependentVar idepVar,
                                    const vector<Real> coeff,
                                    Real kmin, Real kmax);

    virtual ~PolynomialConductivity();
    //@}

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate at current settings
    virtual Real evaluate(int gid);

    //! Evaluate isotropic conductivity at current settings
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Evaluate derivative at current settings
    virtual Real evaluateDerivative(int gid);

    //! Evaluate isotropic conductivity derivative at current settings
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives);

    //! Evaluate at current settings
    virtual SymTensor evaluateTensor(int gid);

    //! Get property format
    virtual PropertyFormat getFormat();

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

  private:

    //! Don't permit copy or assignment operators
    //@{
    PolynomialConductivity(const PolynomialConductivity&);
    PolynomialConductivity& operator=(const PolynomialConductivity&);
    //@}

    DataContainer& m_mesh;

    PropertyIndependentVar m_idepVar;

    Polynomial* m_poly;
};

}
#endif // PolynomialConductivity_h
