//******************************************************************************
//! \file    src/Materials/PolynomialViscosity.h
//! \author  Mark A. Christon
//! \date    Wed Jul 19 13:54:45 MDT 2017
//! \brief   Polynomial viscosity
//******************************************************************************
#ifndef PolynomialViscosity_h
#define PolynomialViscosity_h

#include <vector>

#include <HydraTypes.h>
#include <ViscosityProperty.h>
#include <Material.h>
#include <Polynomial.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Polynomial Viscosity
class PolynomialViscosity: public ViscosityProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             PolynomialViscosity(DataContainer& mesh,
                                 PropertyIndependentVar idepVar,
                                 const vector<Real> coeff,
                                 Real mumin, Real mumax);
    virtual ~PolynomialViscosity();
    //@}

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate viscosity at given temperature
    virtual Real evaluate(int gid);

    //! Evaluate derivative of viscosity at given temperature
    virtual Real evaluateDerivative(int gid);

    //! Evaluate viscosity
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Evaluate derivative of viscosity at given temperature
    virtual bool evaluateDerivative(int nel, const int* gids,
                                    Real* derivatives);

    //! Return viscosity type
    virtual ViscosityType getType();

    //! Get property format
    virtual PropertyFormat getFormat();

  private:

    //! Don't permit copy or assignment operators
    //@{
    PolynomialViscosity(const PolynomialViscosity&);
    PolynomialViscosity& operator=(const PolynomialViscosity&);
    //@}

    DataContainer& m_mesh;

    PropertyIndependentVar m_idepVar;

    Polynomial* m_poly;
};

}

#endif
