//******************************************************************************
//! \file    src/Materials/ConstantExpansion.h
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Constant coefficient of thermal expansion
//******************************************************************************
#ifndef ConstantExpansion_h
#define ConstantExpansion_h

#include <ExpansionProperty.h>

namespace Hydra {

// Forward declarations
class DataContainer;

//! Constant Thermal Expansion
class ConstantExpansion : public ExpansionProperty {

  public:

    //! \name Constructor/Destructor
    //@{
             ConstantExpansion(Real beta);
    virtual ~ConstantExpansion();
    //@}

    //! Set a data index where temperature data for the elements can be found
    virtual void setTemperature(DataIndex temperature);

    //! Echo material property options
    virtual void echoOptions(ostream& ofs);

    //! Evaluate expansion at given temperature
    virtual Real evaluate(int gid);

    //! Evaluate expansion
    virtual bool evaluate(int nel, const int* gids, Real* results,
                          Real* derivatives = 0);

    //! Get property format
    virtual PropertyFormat getFormat();

  private:

    //! Don't permit copy or assignment operators
    //@{
    ConstantExpansion(const ConstantExpansion&);
    ConstantExpansion& operator=(const ConstantExpansion&);
    //@}

    Real m_beta;
};

}

#endif // ConstantExpansion_h
