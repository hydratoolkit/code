//******************************************************************************
//! \file    src/Materials/Material.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Material model interface class (bag model)
//******************************************************************************
#ifndef Material_h
#define Material_h

#include <DataContainer.h>
#include <MaterialProperty.h>
#include <ConductivityProperty.h>
#include <DensityProperty.h>
#include <EOS.h>
#include <ExpansionProperty.h>
#include <SpecificHeatProperty.h>
#include <StrengthProperty.h>
#include <ViscosityProperty.h>
#include <PermeabilityProperty.h>

namespace Hydra {

//! Property independent variables
enum PropertyIndependentVar {
  TEMPERATURE_VARIABLE = 0,
  PRESSURE_VARIABLE,
  VELOCITY_VARIABLE,
  POROSITY_VARIABLE,
  SPECIES_VARIABLE,
  STRAINRATE_VARIABLE,
  NUM_INDEPENDENT_VARIABLES
};

//! Material "bag-model" for storing/accessing material properties
class Material
{

  public:

    //! \name Material Property Types
    enum PropertyType {
      CONDUCTIVITY = 0,
      DENSITY,
      EQ_OF_STATE,
      EXPANSION,
      PERMEABILITY,
      SPECIES_DIFFUSIVITY,
      SPECIES_ID,
      SPECIFIC_HEAT_CP,
      SPECIFIC_HEAT_CV,
      STRENGTH,
      VISCOSITY,
      NUM_PROPERTIES
    };

    //! \name Constructor/Destructor
    //@{
    //! Create a complete material with material Id
    //!   \param[in] mesh     Data container with independent variables
    //!   \param[in] id       Material id
             Material(DataContainer& mesh, int id);
    //! Create an empty material - fill in using unpack
    //!   \param[in] mesh     Data container with independent variables
             Material(DataContainer& mesh);
    virtual ~Material();
    //@}

    //**************************************************************************
    //! \name Material model attributes
    //! These methods are for managing the material model
    //@{
    //! Echo material options
    //!   \param[in] output   Output stream
    void echoOptions(ostream& output);

    //! Get the material Id
    //!   \param[in] m_id      material Id
    int getId() {return m_id;}

    //! Set the material Id
    //!   \param[in] m_id      material Id
    void setId(int id) {m_id = id;}

    //! Set the material name
    //!   \param[in] m_name    material name
    void setName(string name) {m_name = name;}

    //! Insert property into material
    //!   \param[in] mtype    Material property type
    //!   \param[in] prop     Pointer to material property
    void insert(PropertyType mtype, MaterialProperty* prop);
    //@}

    //**************************************************************************
    //! \name Material property accessors
    //! These methods are the primary accessors for evaluation of properties.
    //@{
    //! Thermal conductivity
    ConductivityProperty& getConductivity();

    //! Density
    DensityProperty& getDensity();

    //! Thermal expansion (beta)
    ExpansionProperty& getExpansion();

    //! brief Thermal expansion (beta) using a specified state
    //!   \param[in] state     State at which to evaluate property
    ExpansionProperty& getExpansion(MaterialState state);

    //! Equation of State
    EOS& getEOS();

    //! Check EOS to be sure specific heat is set, etc.
    void checkEOS();

    //! Permeability
    PermeabilityProperty& getPermeability();

    //! Specific heat - constant volume or constant pressure
    SpecificHeatProperty& getSpecificHeat(int shType);

    //! Strength model
    StrengthProperty& getStrength();

    //! Viscosity
    ViscosityProperty& getViscosity();

    //! Viscosity at a specified state
    //!   \param[in] state    State at which to evaluate property
    ViscosityProperty& getViscosity(MaterialState state);

    //! Material reference tempreature
    Real RefTemp()  {return m_Tref;}

    //! Set reference temperature
    void setRefTemp(const Real Tref) {m_Tref = Tref;}

    //! Set material to be rigid (CHT)
    void setRigid() {m_rigid = true;}

    //! Set material to be not rigid (CHT)
    void setNotRigid() {m_rigid = false;}

    //! Set velocity of rigid material (CHT)
    void setRigidVel(const Vector rvel) {m_rigidVel = rvel;}

    //! Set x-velocity of rigid material (CHT)
    void setRigidVelX(const Real rvelx) {m_rigidVel.X = rvelx;}

    //! Set y-velocity of rigid material (CHT)
    void setRigidVelY(const Real rvely) {m_rigidVel.Y = rvely;}

    //! Set z-velocity of rigid material (CHT)
    void setRigidVelZ(const Real rvelz) {m_rigidVel.Z = rvelz;}

    //! Get velocity of rigid material (CHT)
    Vector& getRigidVel() {return m_rigidVel;}

//#####DEBUG -- CLEANUP
    //! Set solutal expansion coefficient
    void setSolutalExpansionCoeff(const Real beta_c) {m_beta_c = beta_c;}

    //! Get solutal expansion coefficient
    Real getSolutalExpansionCoeff() {return m_beta_c;}

    //! Set diffusivity
    void setDiffusivity(const Real diffusivity) {m_diffusivity = diffusivity;}

    //! Get diffusivity
    Real getDiffusivity() {return m_diffusivity;}

    //! Set species reference concentration
    void setSpeciesRefConc(const Real cref) {m_cref = cref;}

    //! Get solutal expansion coefficient
    Real getSpeciesRefConc() {return m_cref;}

//#####DEBUG -- CLEANUP
    //@}

    //**************************************************************************
    //! \name Material query methods
    //! Methods used to query the material class for specific properties and
    //! attributes.
    //@{
    //! Query if property is defined
    //!   \param[in] property     Property type
    bool hasProperty(PropertyType property) {
      return m_property[property] != 0;
    }

    //! Query material to see if it has a strength model
    //!   \return true/false
    virtual bool hasMaterialStrength();

    //! Query if material depends on any independent variables
    //!   \param[in] variable     Variable to test for dependence
    //!   \param[in] mtype        Material property type
    bool isDependent(PropertyIndependentVar variable, PropertyType mtype );

    //! Query fluid status of material (CHT)
    bool isFluid() {return !m_rigid;}

    //! Query rigid status of material (CHT)
    bool isRigid() {return m_rigid;}
    //@}

    //**************************************************************************
    //! \name Material state specification
    //! Define the DataIndex value for registered independent variables
    //! used for material evaluation.
    //@{
    //! Set the data index for internal energy
    //!   \param [in] internalEnergy     Data index for internal energy
    void setInternalEnergy(DataIndex internalEnergy);

    //! Set the data index for porosity data
    //!   \param[in] porosity    Data index for porosity
    void setPorosity(DataIndex porosity);

    //! Set the data indices for pressure data
    //!   \param[in] pressure     Data index for pressure at N+1
    //!   \param[in] pressureN    Data index for pressure at N
    //!   \param[in] pressureNM1  Data index for temperature at N-1
    //!   \param[in] ptype        enum for property type
    void setPressure(DataIndex pressure,
                     DataIndex pressureN,
                     DataIndex pressureNM1,
                     PropertyType ptype);

    //! Set the data index for temperature data
    //!   \param[in] temperature   Data index for temperatures
    //!   \param[in] ptype         enum for property type
    void setTemperature(DataIndex temperature, PropertyType ptype);

    //! Set the data indices for temperature data
    //!   \param[in] temperature     Data index for temperature at N+1
    //!   \param[in] temperatureN    Data index for temperature at N
    //!   \param[in] temperatureNM1  Data index for temperature at N-1
    //!   \param[in] ptype           enum for property type
    void setTemperature(DataIndex temperature,
                        DataIndex temperatureN,
                        DataIndex temperatureNM1,
                        PropertyType ptype);

    //! Set the data index for the velocity gradient
    //!   \param[in] velGrad         Data index for the velocity gradient
    void setVelocityGradient(DataIndex velGrad);

    //! Unset the data indexes for temperature
    void unsetTemperature();
    //@}

    //**************************************************************************
    //! \name Utilitiy methods
    //! Methods used to convert energy to temperature, etc.
    //@{
    //!< Convert temperature to internal energy
    inline Real convTemptoEnergy(const Real &T) {
      return (T-RefTemp())*getSpecificHeat(CONSTANT_VOLUME).evaluate(0);
    }

    //!< Convert internal energy to temperature
    inline Real convertEnergytoTemp(const Real &u) {
      return RefTemp()+u/getSpecificHeat(CONSTANT_VOLUME).evaluate(0);
    }

    //!< Convert enthalpy to temperature
    inline Real convertTemptoEnthalpy(const Real &T) {
      return (T-RefTemp())*getSpecificHeat(CONSTANT_PRESSURE).evaluate(0);
    }

    //!< Convert enthalpy to temperature
    inline Real convertEnthalpytoTemp(const Real &h) {
      return RefTemp() + h/getSpecificHeat(CONSTANT_PRESSURE).evaluate(0);
    }
    //@}

    //**************************************************************************
    //! \name Factory methods for properties
    //! These functions are used to add properties to the material.
    //@{

    // Conductivity
    //! Add a constant conductivity
    void addConstantConductivity(int ctype, const Real* k);

    //! Add a Polynomial Conductivity
    void addPolynomialConductivity(ThermalConductivityType type,
                                   PropertyIndependentVar idepVar,
                                   vector<Real> coeff, Real kmin, Real kmax);

    //! Add a Tabular Conductivity
    void addTabularConductivity(ThermalConductivityType type,
                                PropertyIndependentVar idepVar,
                                vector<Real> lct, vector<Real> lcv);

    // Density
    //! Add a constant density
    void addConstantDensity(Real rho);

    //! Add a Polynomial Density
    void addPolynomialDensity(PropertyIndependentVar idepVar,
                              vector<Real> coeff, Real rhomin, Real rhomax);

    //! Add a Tabular Density
    void addTabularDensity(PropertyIndependentVar idepVar,
                           vector<Real> lct, vector<Real> lcv);

    // Equation of State (EOS)
    //! Add an ideal gas EOS
    void addIdealGasEOS(Real gamma);

    //! Add a Mie-Gruneisen EOS
    void addMieGruneisenEOS(Real rho0, Real Gamma, Real c0, Real s);

    //! Add a stiffened gas EOS
    void addStiffenedGasEOS(Real gamma, Real pinf);

    // Permeability
    //! Add a constant permeability
    void addConstantPermeability(int ctype, const Real* k);

    // Specific Heat
    //! Add a constant specific heat
    void addConstantSpecificHeat(int shType, Real value);

    //! Add a Polynomial Specific Heat
    void addPolynomialSpecificHeat(SpecificHeatType type,
                                   PropertyIndependentVar idepVar,
                                   vector<Real> coeff, Real cmin, Real cmax);

    //! Add a Tabular Specific Heat
    void addTabularSpecificHeat(SpecificHeatType type,
                                PropertyIndependentVar idepVar,
                                vector<Real> lct, vector<Real> lcv);

    // Material Strength Models
    void addElasticStrength(Real E, Real nu);

    void addJohnsonCookStrength(Real G, Real A, Real B, Real C, Real Tmelt,
                                Real n, Real m);

    void addPrandtlReussStrength(Real G, Real A, Real B, Real n);

    // Thermal Expansion
    //! Add a constant thermal expansion coefficient
    void addConstantThermalExpansion(Real beta);

    //! Add a Tabular Thermal Expansion
    void addTabularThermalExpansion(PropertyIndependentVar idepVar,
                                    vector<Real> lct, vector<Real> lcv);

    // Viscosity
    //! Add a constant viscosity
    void addConstantViscosity(Real mu);

    //! Add a Careau-Yasuda viscosity
    void addCarreauYasudaViscosity(Real shearViscosityLow,
                                   Real shearViscosityHigh,
                                   Real timeConstant,
                                   Real n,
                                   Real a);

    //! Add a Cross viscosity
    void addCrossViscosity(Real shearViscosityLow,
                           Real shearViscosityHigh,
                           Real timeConstant,
                           Real n);

    //! Add a Ellis-Meter viscosity
    void addEllisMeterViscosity(Real shearViscosityLow,
                                Real shearViscosityHigh,
                                Real tauHalf,
                                Real n);

    //! Add a HerschelBulkley viscosity
    void addHerschelBulkleyViscosity(Real shearViscosityLow,
                                     Real yieldShearStress,
                                     Real k,
                                     Real n);


    //! Add a Powell-Eyring viscosity
    void addPowellEyringViscosity(Real shearViscosityLow,
                                  Real shearViscosityHigh,
                                  Real timeConstant);

    //! Add a Power Law viscosity
    void addPowerLawViscosity(Real minViscosity,
                              Real maxViscosity,
                              Real k,
                              Real n);

    //! Add a Sutherland's Law viscosity
    void addSutherlandViscosity(Real mu_zero,
                                Real T_zero,
                                Real S);

    //! Add a Polynomial Viscosity
    void addPolynomialViscosity(PropertyIndependentVar idepVar,
                                vector<Real> coeff, Real mumin, Real mumax);

    //! Add a Tabular Viscosity
    void addTabularViscosity(PropertyIndependentVar idepVar,
                             vector<Real> lct, vector<Real> lcv);
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    Material(const Material&);
    Material& operator=(const Material&);
    //@}

    bool m_rigid;          //!< Indicates material is rigid, i.e., a solid
    int  m_id;             //!< Material id
    Real m_Tref;           //!< Reference temperature
//#####DEBUG -- CLEANUP
    Real m_diffusivity;    //!< Diffusivity
    Real m_beta_c;         //!< Solutal expansion coefficient
    Real m_cref;           //!< Species reference concentration
//#####DEBUG -- CLEANUP
    Vector m_rigidVel;     //!< Rigid material velocity for CHT

    //! Material property objects
    MaterialProperty* m_property[NUM_PROPERTIES];

    DataContainer& m_mesh; //!< Data container for variable prop's

    string m_name;         //!< Name for the material

};

}
#endif // Material_h
