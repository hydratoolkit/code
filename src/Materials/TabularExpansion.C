//******************************************************************************
//! \file    src/Materials/TabularExpansion.C
//! \author  Mark A. Christon
//! \date    Thu Jul 20 10:13:32 MDT 2017
//! \brief   Tabular coefficient of thermal expansion
//******************************************************************************
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include <TabularExpansion.h>
#include <DataContainer.h>
#include <PrintUtil.h>

using namespace Hydra;

TabularExpansion::TabularExpansion(DataContainer& mesh,
                                   PropertyIndependentVar idepVar,
                                   const vector<Real> lct,
                                   const vector<Real> lcv) :
  m_mesh(mesh),
  m_idepVar(idepVar)
/*******************************************************************************
Routine: TabularExpansion - constructor
Author : Mark A. Christon
*******************************************************************************/
{
  m_table = new Table(9000, lct, lcv);
}

TabularExpansion::~TabularExpansion()
/*******************************************************************************
Routine: ~TabularExpansion - destructor
Author : Mark A. Christon
*******************************************************************************/
{
  delete m_table;
}

void
TabularExpansion::setTemperature(DataIndex /*temperature*/)
/*******************************************************************************
Routine: setTemperature - set data index for temperature
Author : Mark A. Christon
*******************************************************************************/
{
}

void
TabularExpansion::echoOptions(ostream& ofs)
/*******************************************************************************
Routine: echoOPtions - echo material data
Author : Mark A. Christon
*******************************************************************************/
{
  PrintUtil pu(ofs);
  pu.printOption("Tabular Thermal Expansion");
  pu.printOption("  Number of Points", m_table->size());
  pu.printOption("  Minimum Temperature value", m_table->getMinTime());
  pu.printOption("  Minimum Thermal Expansion value", m_table->getMinVar());
  pu.printOption("  Maximum Temperature value", m_table->getMaxTime());
  pu.printOption("  Maximum Thermal Expansion value", m_table->getMaxVar());

  vector<string> columns;
  columns.push_back("Temperature");
  columns.push_back("Expansion");
  m_table->echoOptions(columns, ofs);
}

Real
TabularExpansion::evaluate(int /*gid*/)
/*******************************************************************************
Routine: evaluate - evaluate at given elements and temperatures
Author : Mark A. Christon
*******************************************************************************/
{
#ifdef DONT
  assert(m_table != 0);
  Real beta = 0.0;
  int lid = 0;
  m_table->evaluate(m_mesh, 1, &lid, &gid, &beta);
  return beta;
#endif
  return 1.0;
}

bool
TabularExpansion::evaluate(int nel, const int* gids, Real* results,
                           Real* /*derivatives*/)
/*******************************************************************************
Routine: evaluate - evaluate at given elements and temperatures
Author : Mark A. Christon
*******************************************************************************/
{
  Real* T = m_mesh.getVariable<Real>(m_TEMPERATURE[0]);
  for (int i = 0; i < nel; i++) {
    results[gids[i]] = m_table->lookup(T[gids[i]]);
  }
  return true;
}

PropertyFormat
TabularExpansion::getFormat()
/*******************************************************************************
Routine: getFormat - return material property data format
Author : Mark A. Christon
*******************************************************************************/
{
  return TABULAR_PROPERTY;
}
