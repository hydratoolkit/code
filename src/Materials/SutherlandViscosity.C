//******************************************************************************
//! \file    src/Materials/SutherlandViscosity.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:12 2011
//! \brief   Sutherland viscosity
//******************************************************************************
#include <cassert>
#include <string>
#include <vector>

using namespace std;

#include <SutherlandViscosity.h>
#include <DataContainer.h>
#include <PrintUtil.h>

using namespace Hydra;

SutherlandViscosity::SutherlandViscosity(DataContainer& mesh,
                                         Real mu_zero,
                                         Real T_zero,
                                         Real S):
  m_mu_zero(mu_zero),
  m_T_zero(T_zero),
  m_S(S),
  m_mesh(mesh)
/*******************************************************************************
Routine: SutherlandViscosity - constructor
Author : Mark A. Christon
*******************************************************************************/
{}

SutherlandViscosity::~SutherlandViscosity()
/*******************************************************************************
Routine: SutherlandViscosity - constructor
Author : Mark A. Christon
*******************************************************************************/
{}

void
SutherlandViscosity::setTemperature(DataIndex /*temperature*/)
/*******************************************************************************
Routine: setTemperature - set data index for temperature
Author : Mark A. Christon
*******************************************************************************/
{}

void
SutherlandViscosity::echoOptions(ostream& ofs)
/*******************************************************************************
Routine: echoOPtions - echo material data
Author : Mark A. Christon
*******************************************************************************/
{
  PrintUtil pu(ofs);
  pu.printOption("Sutherland Viscosity");
  pu.printOption("   Viscosity at reference temperature", m_mu_zero);
  pu.printOption("   Reference temperature", m_T_zero);
  pu.printOption("   Sutherland temperature", m_S);
}

Real
SutherlandViscosity::evaluate(int /*gid*/)
/*******************************************************************************
Routine: evaluate - evaluate at given elements and temperatures
Author : Mark A. Christon
*******************************************************************************/
{
  return 0.0;
}

Real
SutherlandViscosity::evaluateDerivative(int /*gid*/)
/*******************************************************************************
Routine: evaulateDerivative - evaluate at given elements and temperatures
Author : Mark A. Christon
*******************************************************************************/
{
  return 0.0;
}

bool
SutherlandViscosity::evaluate(int nel, const int* gids,
                              Real* results, Real* /*derivatives*/)
/*******************************************************************************
Routine: evaluate - evaluate at given elements and temperatures
Author : Mark A. Christon
*******************************************************************************/
{
  Real* T = m_mesh.getVariable<Real>(m_TEMPERATURE[0]);
  for (int i=0; i<nel; i++) {
    int gid = gids[i];
    Real Trat = T[gid]/m_T_zero;
    results[gid] = m_mu_zero*pow(Trat, 1.5)*(m_T_zero + m_S)/(T[gid] + m_S);
  }

  return true;
}

bool
SutherlandViscosity::evaluateDerivative(int /*nel*/, const int* /*gids*/,
                                        Real* /*derivatives*/)
/*******************************************************************************
Routine: evaulateDerivative - evaluate at given elements and temperatures
Author : Mark A. Christon
*******************************************************************************/
{
  return false;
}

PropertyFormat
SutherlandViscosity::getFormat()
/*******************************************************************************
Routine: getFormat - return material property data format
Author : Mark A. Christon
*******************************************************************************/
{
  return FUNCTIONAL_PROPERTY;
}

ViscosityType
SutherlandViscosity::getType()
/*******************************************************************************
Routine: getType - return viscosity type
Author : Mark A. Christon
*******************************************************************************/
{
  return SUTHERLAND_VISCOSITY;
}
