//******************************************************************************
//! \file    src/FEM/MultiscaleLagrangianDynamics/MultiscaleLagrangianDynamics.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:35:24 2011
//! \brief   VMS Multiscale Lagrangian Hydro
//******************************************************************************

#ifndef MultiscaleLagrangianDynamics_h
#define MultiscaleLagrangianDynamics_h

#include <vector>

#include <UnsPhysics.h>
#include <UserIC.h>

namespace Hydra {

//! Array dimensions for cache blocks
#define NDIM    3
#define MAXNPE  8

//! The Boundary list data structure holds boundary element/side data
struct Blist {
  //@{
  int  Nel;             //!< Number of elements in sideset
  DataIndex EL_LIST;    //!< List of elements in the sideset
  DataIndex SIDE_LIST;  //!< List of sides for each element in the sideset
  //@}
};

//! Multiscale Dynamics class
class MultiscaleDynamics : public UnsPhysics {

  public:

    //! \name Constructor/Destructor
    //@{
             MultiscaleDynamics();
    virtual ~MultiscaleDynamics() {}
    //@}

    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData();
    //@}

    //! \name Virtual Physics Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    virtual void setup();

    virtual void initialize();

    virtual void solve();

    virtual void finalize() {}

    virtual void setupMergedSetBCs() {}
    //@}

    //! Set velocity IC's
    void setVelocityICs(Real uic, Real vic, Real wic);

    //! Use user specified initial conditions
    void addUserIC() { user_ic.push_back(new UserIC); }
    //@}

    // Setup thermodynamic variables
    void initEandRho();
    void initThermoVars();

    //! Setup the boundary lists
    void setupBlists(UnsHashMap& hmap);

    //! Setup element groups
    void setupGroups();

    //! Form the lumped mass matrix
    void formMass();
    void formMass2D();
    void formMass3D();

    //! Calculate the element volumes
    void curVolume();
    void curVol2D();
    void curVol3D();

    //! Calculate internal forces
    void calcResidual();

    //! Quad-4 workset functions
    void gatherQ4Nodes(Quad4* el, Real* x, Real* y);
    void gatherQ4Y(Real* uold, Real* unew, Real* vold, Real* vel,
                   Real* p, Real* pold);
    void calcQ4Lengths();
    void calcQ4Deriv(int k);
    void calcQ4Rese(int k);
    void calcQ4FluxJacobians();
    void calcQ4Yprime();
    void calcQ4nuDC();
    void calcQ4YFJ(int k);
    void calcQ4Nu();
    void calcQ4BoundaryFlux();
    void assyQ4Resid(Real* resv, Real* resp, Real* nu_p, Real* V0);
    void updateRp();
    void gatherQ4Vel(Real* unew, Real* vel, Real* dvel);
    void calcQ4J(int k);
    void updateQ4Rp(int k);
    void calcQ4Mpp(int k);
    void assyQ4RM(Real* rmpp, Real* resp);

    void calcInitVisc() {cout << "!!!!!!!!!!!!!!!!!!!!!!!!" << endl
                              << "No initial viscosity yet" << endl
                              << "!!!!!!!!!!!!!!!!!!!!!!!!" << endl;}

    void calcTimeStep(Real time);
    void calcKE();

    //! \name Virtual Functions for I/O
    //! These functions are implemented for each specific physics
    //! to permit custom options to be coded for reading and writing
    //! restart files, time-history data, bc's, etc.
    //@{
    virtual void writeSolving();

    virtual void echoOptions(ostream& ofs);

    void echoBCs(ostream& ofs);

    void echoBlists(ostream& ofs);

    void VelocityLimits(Real* vel,
                        Real& v1min, Real& v1max,
                        Real& v2min, Real& v2max);

    //! Write the header for the global time-history data file
    void globHeader(ofstream& globf);

    //! Write the global time history data at each step
    void writeGlobal(ofstream& globf, Real time, Real ie, Real ke);

    //@}

    //! \name Local Functions for I/O
    //! These functions are implemented for this specific physics
    //! to permit custom options to be coded for output of plot variables
    //@{
    void setupPlotVar();    //!< Setup the output plot variables
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    MultiscaleDynamics(const MultiscaleDynamics&);
    MultiscaleDynamics& operator=(const MultiscaleDynamics&);
    //@}

    //! \name Data registered for MultiscaleDynamics
    //@{
    DataIndex CURVOL;       //!< Volume in "current" configuration
    DataIndex DENSITY;      //!< Density distribution
    DataIndex REF_DENSITY;  //!< reference Density distribution (for IC's)
    DataIndex INT_ENERGY;   //!< Internal energy
    DataIndex NU_P;         //!< Artificial viscosity for output
    DataIndex DISP_OLD;     //!< displ. coordinates at t_n
    DataIndex DISP_NEW;     //!< displ. coordinates at iteration (i+1)
    DataIndex VELI;         //!< velocity at iterate (i)
    DataIndex DVEL;         //!< velocity increment at iterate (i)
    DataIndex VEL_OLD;      //!< velocity at t_n
    DataIndex P;            //!< pressure at iterate (i)
    DataIndex DP;           //!< pressure increment at iterate (I)
    DataIndex P_OLD;        //!< pressure at t_n
    DataIndex RV;           //!< Residual, velocity at iteration (i)
    DataIndex RP;           //!< Residual, Pressure at iteration (i)
    DataIndex RLM;          //!< lumped mass (velocity)
    DataIndex RIMVV;        //!< inverse lumped mass (velocity)
    DataIndex RMPP;         //!< lumped mass (pressure)


    //debug vectors
    DataIndex RVX, RVY;         //!< lumped mass (pressure)



    // Group lists
    DataIndex P1_ELG_START; //!< Start of tri/tet  Element Groups
    DataIndex P1_ELG_END;   //!< End   of tri/tet  Element Groups
    DataIndex Q1_ELG_START; //!< Start of quad/hex Element Groups
    DataIndex Q1_ELG_END;   //!< End   of quad/hex Element Groups

    // Output variables
    DataIndex MATX;         //!< X-material coordinates (current configuration)
    DataIndex MATY;         //!< Y-material coordinates (current configuration)
    DataIndex MATZ;         //!< Z-material coordinates (current configuration)
    DataIndex DISPLX;       //!< X-displ. coordinates at n+1
    DataIndex DISPLY;       //!< Y-displ. coordinates at n+1
    DataIndex DISPLZ;       //!< Z-displ. coordinates at n+1 (not used for 2D)
    DataIndex VELX;         //!< X-velocity at n+1
    DataIndex VELY;         //!< Y-velocity at n+1
    DataIndex VELZ;         //!< Z-velocity at n+1
    //@}

    // Local variables
    // Real rho_0, gam, ogm1;

    // Cache-block data for now
    int Nelg_p1;            //!< # of element groups - tri3/tet4
    int Nelg_q1;            //!< # of element groups - quad4/hex8
    int Nnpe;               //!< # of nodes per element in current group
    int Nmel;               //!< # of elements in the current group
    int e_start;            //!< start of the element group
    int e_end;              //!< end of the element group

    int nd[MAXNPE][BLKSIZE];
    int gid[BLKSIZE], matid[BLKSIZE];

    //! Nodal coordinates, derivatives at the element level in the element group
    Real   xe[MAXNPE][BLKSIZE],   ye[MAXNPE][BLKSIZE],   ze[MAXNPE][BLKSIZE];
    Real dNdx[MAXNPE][BLKSIZE], dNdy[MAXNPE][BLKSIZE], dNdz[MAXNPE][BLKSIZE];

    Real xi[NDIM][BLKSIZE], eta[NDIM][BLKSIZE], zeta[NDIM][BLKSIZE];
    Real he[NDIM][BLKSIZE];

    Real ev[BLKSIZE], evi[BLKSIZE];

    //! Inverse jacobian -- relative to parent element
    Real i11[BLKSIZE], i12[BLKSIZE], i21[BLKSIZE], i22[BLKSIZE];

    //! Data at element level in the element group
    Real uolde[NDIM][MAXNPE][BLKSIZE], unewe[NDIM][MAXNPE][BLKSIZE];
    Real volde[NDIM][MAXNPE][BLKSIZE],  vele[NDIM][MAXNPE][BLKSIZE];
    Real dvele[NDIM][MAXNPE][BLKSIZE];
    Real       polde[MAXNPE][BLKSIZE],          pe[MAXNPE][BLKSIZE];

    //! Velocity/Pressure residual at the element level in the element group
    Real  rv[NDIM][MAXNPE][BLKSIZE], rp[MAXNPE][BLKSIZE];
    Real  mlpp[MAXNPE][BLKSIZE];

    //! Viscosity
    Real nu_avg[BLKSIZE], nu_art[BLKSIZE];

    //! All arrays are dimensioned NDIM+1 unless otherwise noted
    Real rwork1[NDIM+1];
    Real Yprime[NDIM+1][BLKSIZE];

    Real Yp[NDIM+1][BLKSIZE]; // the vector Y=[v_1,v_2,p]^T @ iterate (i):
                             // the second index represent 0=fcn, 1=x-der., 2=y-der.
    Real Ym[NDIM+1][BLKSIZE]; // the vector Y=[v_1,v_2,p]^T @ time t_n
                             // the second index represent 0=fcn, 1=x-der., 2=y-der.
    Real Yc[NDIM+1][NDIM+1][BLKSIZE]; // the vector Y=[v_1,v_2,p]^T @ time t_{n+1/2}
                             // the second index represent 0=fcn, 1=x-der., 2=y-der.

    Real   A0c[NDIM+1][NDIM+1][BLKSIZE];
    Real   A1c[NDIM+1][NDIM+1][BLKSIZE];
    Real   A2c[NDIM+1][NDIM+1][BLKSIZE];
    Real   A3c[NDIM+1][NDIM+1][BLKSIZE];

    // Deformation gradient F, same notation as before
    Real  Fc[NDIM][NDIM][BLKSIZE];

    // The Jacobian of F, i.e.
    // of the map from original to current configuration
    Real Jp[BLKSIZE];
    Real Jm[BLKSIZE];
    Real Jc[BLKSIZE];

    // Vector offsets for indexing u and v
    int UVOFF[NDIM];

    // Boundary lists
    Blist p1list;
    Blist q1list;

    // Time steps
    Real dtn;   //<! delta-t at n
    Real dto2;  //<! delta-t at n divided by 2
    Real dtnm;  //<! delta-t at n-1
    // Remove this

    // Energies
    Real kin_energy;
    Real int_energy;

    // Simple IC's
    Real vel_ic[3];

    //! \name User defined initial conditions (should only use one)
    //@{
    vector<UserIC *> user_ic;
    //@}
};

}
#endif // MultiscaleLagrangianDynamics_h
