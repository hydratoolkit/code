//******************************************************************************
//! \file src/FEM/MultimaterialLagrangianDynamics/MultimaterialLagrangianDynamics.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:35:07 2011
//! \brief   Multimaterial Lagrangian hydro
//******************************************************************************
#ifndef MultimaterialLagrangianDynamics_h
#define MultimaterialLagrangianDynamics_h

#include <vector>

#include <MaterialInsert.h>
#include <MaterialState.h>
#include <UnsPhysics.h>
#include <ConductionBCs.h>

namespace Hydra {

//! Move to element_ops.h or some such thing?
struct BmatrixQ41PT {
  Real bx1;
  Real bx2;
  Real by1;
  Real by2;
};

//! Multimaterial Lagrangian hydro class
class MultimaterialLagrangian : public UnsPhysics {

  public:

    //! \name Constructor/Destructor
    //@{
             MultimaterialLagrangian();
    virtual ~MultimaterialLagrangian() {}
    //@}

    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData();
    //@}

    //! \name Virtual Physics Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    virtual void setup();

    virtual void initialize();

    virtual void solve();

    virtual void finalize() {}

    virtual void setupMergedSetBCs() {}
    //@}

    //! Setup element groups
    void setupGroups();

    //! Initialize the initial multimaterial parameters
    void initMultiMatVar();

    void addMaterial(MultiMaterialVar *mmvar,
                     int id,
                     Material* mat,
                     Real vfrac,
                     Real p,
                     Real e,
                     Real rho);

    void deleteMaterial(MultiMaterialVar *mmvar, int id);

    void avgDensity(MultiMaterialVar *mmvar, Real* density, int Nel);

    void avgInternalEnergy(MultiMaterialVar *mmvar, Real *intenergy, int Nel);

    void avgPressure(MultiMaterialVar *mmvar, Real* pressure, int Nel);

    Real avgSoundSpeed(MultiMaterialVar mmvar);

    void avgSoundSpeed(MultiMaterialVar *mmvar, Real* soundspeed, int Nel);

    //! \name Multimaterial Lagrangian specific material insertion functions.
    //! These are only implemented in this physics class for now.
    //@{
    //! Add a material insertion object
    void addMaterialInsert() {
      cout << "!!!!! Can't add base material insert object !!!!!"<< endl;
    }

    void addBoxInsert(BoxInsert* matbox) {material_inserts.push_back(matbox);}

    void addCircleInsert() {
      cout << "!!!!! Can't add circle insert object !!!!!"<< endl;
    }

    //! Number of insertion objects
    int numMaterialInserts() {return material_inserts.size();}

    //! Echo the material insertion options
    void echoMaterialInserts(ostream& ofs);
    //@}

    //! Form the lumped mass matrix
    void formMass();

    //! Initialize an EOS -- simple place holder for now
    void initEOS();

    //! Update an EOS -- simple place holder for now
    void updateEOS();
    void updateDensity();
    void predictIntEnergy();
    void correctIntEnergy();

    //! Calculate the element volumes
    void curVolume();
    void curVol2D();
    void curVol3D();

    //! Calculate internal forces
    void calcInternalForce(Real* Fx, Real* Fy);
    void gatherNodesQ4(Quad4* el);
    void gatherNodeVecQ4(Quad4* el, DataIndex idx, DataIndex idy, bool delta);
    void gatherBmatrixQ4();
    void calcBmatrixQ4();
    void integrate();
    void calcTimeStep(Real time);
    void calcArtViscosity();
    void calcArtViscQ4();
    void calcKE();
    void calcHGShape( );
    void calcHourGlassForce( Real ehg );

    //! \name Virtual Functions for I/O
    //! These functions are implemented for each specific physics
    //! to permit custom options to be coded for reading and writing
    //! restart files, time-history data, bc's, etc.
    //@{
    virtual void writeSolving();

    virtual void echoOptions(ostream& ofs);

    void echoBCs(ostream& ofs);

    //! Write the header for the global time-history data file
    void globHeader(ofstream& globf);

    //! Write the global time history data at each step
    void writeGlobal(ofstream& globf, Real time, Real ie, Real ke);

    //@}

  protected:

    //! \name Data registered for Lagrangian dynamics
    //! Only temperature and heat source term need to be registered
    //! for conduction.
    //@{
    DataIndex VOLN;          //!< Volume at time n
    DataIndex VOLNP1;        //!< Volume at time n+1
    DataIndex DENSITY;       //!< Density
    DataIndex PRESSURE;      //!< Pressure
    DataIndex INT_ENERGY;    //!< Internal energy
    DataIndex ART_VISC;      //!< Artificial viscosity
    DataIndex MATX;          //!< X-material coordinates at n+1
    DataIndex MATY;          //!< Y-material coordinates at n+1
    DataIndex MATZ;          //!< Z-material coordinates at n+1 (3D only)
    DataIndex DISPLX;        //!< X-displ. coordinates at n+1
    DataIndex DISPLY;        //!< Y-displ. coordinates at n+1
    DataIndex DISPLZ;        //!< Z-displ. coordinates at n+1 (3D only)
    DataIndex VELX;          //!< X-velocity at n+1/2
    DataIndex VELY;          //!< Y-velocity at n+1/2
    DataIndex VELZ;          //!< Z-velocity at n+1/2  (not used for 2D)
    DataIndex VELX_OLD;      //!< X-velocity at n-1/2
    DataIndex VELY_OLD;      //!< Y-velocity at n-1/2
    DataIndex VELZ_OLD;      //!< Z-velocity at n-1/2  (not used for 2D)
    DataIndex FX;            //!< X-force at n
    DataIndex FY;            //!< Y-force at n
    DataIndex FZ;            //!< Z-force at n    (not used for 2D)
    DataIndex FXN;           //!< X-force at n+1  (not used)
    DataIndex FYN;           //!< Y-force at n+1  (not used)
    DataIndex FZN;           //!< Z-force at n+1  (not used)
    DataIndex RIMX;          //!< X-inverse lumped mass w. EBC's imposed
    DataIndex RIMY;          //!< Y-inverse lumped mass w. EBC's imposed
    DataIndex RIMZ;          //!< Z-inverse lumped mass w. EBC's imposed
    DataIndex LUMPED_MASS;   //!< Lumped mass matrix
    DataIndex EL_MASS;       //!< Element mass
    DataIndex MMVAR;         //!< Multimaterial variables
    DataIndex BMATRIX_Q41PT; //!< B-matrix

    DataIndex ELG_START;     //!< Start of Element Groups
    DataIndex ELG_END;       //!< End   of Element Groups

    DataIndex SEND_BUFFER;   //!< Send buffer for nodal communicator messages
    DataIndex RECV_BUFFER;   //!< Recieve buffer for nodal communicator
    //@}

    // Cache-block data for now
    int Nelg;               //!< # of element groups

    int Nmel, e_start, e_end;
    int   n1[BLKSIZE],  n2[BLKSIZE],  n3[BLKSIZE],  n4[BLKSIZE];
    int   n5[BLKSIZE],  n6[BLKSIZE],  n7[BLKSIZE],  n8[BLKSIZE];
    int matid[BLKSIZE];

    Real  x1[BLKSIZE],  x2[BLKSIZE],  x3[BLKSIZE],  x4[BLKSIZE];
    Real  x5[BLKSIZE],  x6[BLKSIZE],  x7[BLKSIZE],  x8[BLKSIZE];
    Real x13[BLKSIZE], x24[BLKSIZE];
    Real x17[BLKSIZE], x28[BLKSIZE], x35[BLKSIZE], x46[BLKSIZE];

    Real  y1[BLKSIZE] , y2[BLKSIZE] , y3[BLKSIZE] , y4[BLKSIZE];
    Real  y5[BLKSIZE] , y6[BLKSIZE] , y7[BLKSIZE] , y8[BLKSIZE];
    Real y13[BLKSIZE], y24[BLKSIZE];
    Real y17[BLKSIZE], y28[BLKSIZE], y35[BLKSIZE], y46[BLKSIZE];

    Real  z1[BLKSIZE] , z2[BLKSIZE] , z3[BLKSIZE] , z4[BLKSIZE];
    Real  z5[BLKSIZE] , z6[BLKSIZE] , z7[BLKSIZE] , z8[BLKSIZE];
    Real z13[BLKSIZE], z24[BLKSIZE];
    Real z17[BLKSIZE], z28[BLKSIZE], z35[BLKSIZE], z46[BLKSIZE];

    Real fx1[BLKSIZE], fx2[BLKSIZE], fx3[BLKSIZE], fx4[BLKSIZE];
    Real fx5[BLKSIZE], fx6[BLKSIZE], fx7[BLKSIZE], fx8[BLKSIZE];

    Real fy1[BLKSIZE], fy2[BLKSIZE], fy3[BLKSIZE], fy4[BLKSIZE];
    Real fy5[BLKSIZE], fy6[BLKSIZE], fy7[BLKSIZE], fy8[BLKSIZE];

    Real fz1[BLKSIZE], fz2[BLKSIZE], fz3[BLKSIZE], fz4[BLKSIZE];
    Real fz5[BLKSIZE], fz6[BLKSIZE], fz7[BLKSIZE], fz8[BLKSIZE];

    Real bx1[BLKSIZE], bx2[BLKSIZE], bx3[BLKSIZE], bx4[BLKSIZE];
    Real bx5[BLKSIZE], bx6[BLKSIZE], bx7[BLKSIZE], bx8[BLKSIZE];

    Real by1[BLKSIZE], by2[BLKSIZE], by3[BLKSIZE], by4[BLKSIZE];
    Real by5[BLKSIZE], by6[BLKSIZE], by7[BLKSIZE], by8[BLKSIZE];

    Real bz1[BLKSIZE], bz2[BLKSIZE], bz3[BLKSIZE], bz4[BLKSIZE];
    Real bz5[BLKSIZE], bz6[BLKSIZE], bz7[BLKSIZE], bz8[BLKSIZE];

    Real ev[BLKSIZE], evi[BLKSIZE];

    Real hgsv1[BLKSIZE], hgsv2[BLKSIZE], hgsv3[BLKSIZE], hgsv4[BLKSIZE];

    Real q[BLKSIZE];

  private:

    //! Don't permit copy or assignment operators
    //@{
    MultimaterialLagrangian(const MultimaterialLagrangian&);
    MultimaterialLagrangian& operator=(const MultimaterialLagrangian&);
    //@}

    //! Screwed up dt's
    Real dtn;   //<! delta-t at n
    Real dtnm;  //<! delta-t at n-1
    Real dtmid; //<! for velocity update

    //! Energies
    Real kin_energy;
    Real int_energy;

    //! Material insertion objects
    vector<MaterialInsert*> material_inserts;
};

}
#endif // MultimaterialLagrangianDynamics_h
