//******************************************************************************
//! \file    src/FEM/MultimaterialLagrangianDynamics/MaterialState.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:35:07 2011
//! \brief   Material state structure
//******************************************************************************
#ifndef MaterialState_h
#define MaterialState_h

#include <vector>

namespace Hydra {

//! Expandable material state structure to handle cell-by-cell variations
struct MultiMaterialState {
  int  matid;
  Material* mat;   //!< Material pointers (not owned)
  Real volFrac;
  Real pressure;
  Real intEnergy;
  Real density;
};

//! Multimaterial structure for data at the element level
struct MultiMaterialVar {
  int  Nmat;           //!< # of materials present in the element = mat.size()
  bool hasStrength;    //!< Default is false for hydro limit
  Real voidFrac;       //!< voidFrac = 1.0 by default until material is added
  vector<MaterialState> state;
};

}
#endif // MaterialState_h
