//******************************************************************************
//! \file    src/FEM/MultimaterialLagrangianDynamics/MaterialInsert.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:35:07 2011
//! \brief   Class to insert multiple material into cells
//******************************************************************************

#ifndef MaterialInsert_h
#define MaterialInsert_h

#include <UnsMesh.h>
#include <MaterialState.h>
#include <CCError.h>

namespace Hydra {

enum FillType {
  ALL_OUTSIDE = 0,
  ALL_INSIDE,
  MIXED };

//! Base clase for material insertion
class MaterialInsert {

  public:

    //! \name Constructor/Destructor
    //@{
             MaterialInsert() {}
    virtual ~MaterialInsert() {}
    //@}

    virtual void insertMaterial(UnsMesh* /*mesh*/,
                                MultiMaterialVar* /*mmvar*/) {
      cout << "Base InsertMaterial not implemented" << endl;
    }

    virtual void echoInsert(ostream& /*ofs*/) {
      cout << "Base echoInsert not implemented" << endl;
    }

    int  getId() {return id;}

    void addMaterial(MultiMaterialVar *mmvar,
                     int id,
                     Material* mat,
                     Real vfrac,
                     Real p,
                     Real e,
                     Real rho);

  protected:

    int id;        // insertion object id
    int matid;     // material id to be inserted
    int maxlevels; // maximum number of refinement levels

  private:

    //! Don't permit copy or assignment operators
    //@{
    MaterialInsert(const MaterialInsert&);
    MaterialInsert& operator=(const MaterialInsert&);
    //@}

};

class BoxInsert : public MaterialInsert {

  public:

    //! \name Constructor/Destructor
    //@{
             BoxInsert();
             BoxInsert(int insert_id, int insert_matid, int insert_maxlevels,
                       Real* insert_box);
    virtual ~BoxInsert() {}
    //@}

    virtual void insertMaterial(UnsMesh* mesh, MultiMaterialVar* mmvar);

    virtual void echoInsert(ostream& ofs);

    FillType insideBox(Real* x, Real* y);

    // FillType insideBox(Real* x, Real* y, Real* z) {}

    Real calcMatVolume(Real* x, Real* y, Real V, Eltype etype);

    void splitQuad(Quad4R* q);

    void writeGMVMesh(Quad4R* q4r);

  private:

    //! Don't permit copy or assignment operators
    //@{
    BoxInsert(const BoxInsert&);
    BoxInsert& operator=(const BoxInsert&);
    //@}

    Real box[2][3];  // xmin, xmax, ymin, ymax, zmin, zmax

};

class CircleInsert : public MaterialInsert {

  public:

    //! \name Constructor/Destructor
    //@{
             CircleInsert() {}
    virtual ~CircleInsert() {}
    //@}

    virtual void insertMaterial(UnsMesh* /*mesh*/,
                                MultiMaterialVar* /*mmvar*/) {}

    virtual void echoInsert(ostream& /*ofs*/) {}

  private:

    //! Don't permit copy or assignment operators
    //@{
    CircleInsert(const CircleInsert&);
    CircleInsert& operator=(const CircleInsert&);
    //@}

    Real radius;
    Real xc[3];   //! Location
};

}
#endif // MaterialInsert_h
