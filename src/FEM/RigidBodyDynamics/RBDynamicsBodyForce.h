//******************************************************************************
//! \file    src/FEM/RigidBodyDynamics/RBDynamicsBodyForce.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:35:42 2011
//! \brief   Body forces for rigid body dynamics
//******************************************************************************

#ifndef RBDynamicsBodyForce
#define RBDynamicsBodyForce

#include <BodyForce.h>

namespace Hydra {

//! Rigid-Body Dyanamics forces
class RBDynBodyForce : public BodyForce {

  public:

    //! \name Constructor/Destructor
    //@{
    RBDynBodyForce() {
      //initialize everything to zero or -1.
      force_amp[0] = 0;
      force_amp[1] = 0;
      force_amp[2] = 0;

      force_tblid[0] = -1;
      force_tblid[1] = -1;
      force_tblid[2] = -1;

      mom_amp[0] = 0;
      mom_amp[1] = 0;
      mom_amp[2] = 0;

      mom_tblid[0] = -1;
      mom_tblid[1] = -1;
      mom_tblid[2] = -1;
    }

    virtual ~RBDynBodyForce(){}

    //! functions for setting the scaler ampiltude of each force table.
    //@{
    void setXForceAmp(Real amp) {force_amp[0] = amp;}
    void setYForceAmp(Real amp) {force_amp[1] = amp;}
    void setZForceAmp(Real amp) {force_amp[2] = amp;}
    //@}

    //! functions for setting the IDs of each force table.
    //@{
    void setXForceID(int id) {force_tblid[0] = id;}
    void setYForceID(int id) {force_tblid[1] = id;}
    void setZForceID(int id) {force_tblid[2] = id;}
    //@}

    //! functions for setting the scaler ampiltude of each moment table.
    //@{
    void setXMomAmp(Real amp) {mom_amp[0] = amp;}
    void setYMomAmp(Real amp) {mom_amp[1] = amp;}
    void setZMomAmp(Real amp) {mom_amp[2] = amp;}
    //@}

    //! functions for setting the IDs of each moment table.
    //@{
    void setXMomID(int id) {mom_tblid[0] = id;}
    void setYMomID(int id) {mom_tblid[1] = id;}
    void setZMomID(int id) {mom_tblid[2] = id;}
    //@}

    //! functions for getting the scaler aplitude of each force table.
    //@{
    Real getXForceAmp() {return force_amp[0];}
    Real getYForceAmp() {return force_amp[1];}
    Real getZForceAmp() {return force_amp[2];}
    //@}

    //! functions for getting the IDs of each force table.
    //@{
    int getXForceID() {return force_tblid[0];}
    int getYForceID() {return force_tblid[1];}
    int getZForceID() {return force_tblid[2];}
    //@}

    //! functions for getting the scaler aplitude of each moment table.
    //@{
    Real getXMomAmp() {return mom_amp[0];}
    Real getYMomAmp() {return mom_amp[1];}
    Real getZMomAmp() {return mom_amp[2];}
    //@}

    //! functions for getting the IDs of each moment table.
    //@{
    int getXMomID() {return mom_tblid[0];}
    int getYMomID() {return mom_tblid[1];}
    int getZMomID() {return mom_tblid[2];}
    //@}

  private:

    Real force_amp[3];   //!< scalar factors to multiply the force by.
    int  force_tblid[3]; //!< IDs of force tables.
    Real mom_amp[3];     //!< scalar factors to multiply the force by
    int  mom_tblid[3];   //!< IDs of moment tables
};

}
#endif
