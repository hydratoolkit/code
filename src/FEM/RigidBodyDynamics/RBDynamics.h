//******************************************************************************
//! \file    src/FEM/RigidBodyDynamics/RBDynamics.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:35:42 2011
//! \brief   Rigid body dynamics
//******************************************************************************

#ifndef RBDynamics_h
#define RBDynamics_h

#include <vector>

#include <UnsPhysics.h>
#include <RBDynamicsBodyForce.h>

namespace Hydra {

//! Size of state arrays for Rigid body formulation
#define STATE_SIZE 13
#define COORD   0
#define QUATRN  3
#define LMOM    7
#define AMOM    10

//! Rigid-Body Dynamics class
class RBDynamics : public UnsPhysics {

  public:

    //! \name Constructor/Destructor
    //@{
             RBDynamics();
    virtual ~RBDynamics();
    //@}

    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData();
    //@}

    //! \name Virtual Physics Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    virtual void setup();

    virtual void initialize();

    virtual void solve();

    virtual void finalize() {}

    virtual void setupMergedSetBCs() {}
    //@}

    //! Form the lumped mass matrix
    void calcMassProp();

    //! Calculate Xcg and Icg for 2D
    void calcXcgIcg2d();

    //! Calculate Xcg and Icg for 3D
    void calcXcgIcg3d();

    //! Calculate the inverse CG intertia tensor
    void calcIcginv();

    //! (re-)Normalize quaternion
    void normalizeQuaternion(Real* Y);

    //! Generic matrix-vector multiply function
    void matVec(Real A[3][3], Real x[3], Real b[3]);

    //! Generic matrix-matrix multiply function
    void matMat(Real A[3][3], Real B[3][3], Real C[3][3]);

    //! Form the rotation matrix from the quaternion
    void formRotMat();

    //! Calculate Ydot vector
    void calcYdot(Real* Y, Real t);

    //! Post-process to get nodal coordinates & velocities
    void calcNodalResults();

    //! Calculate the kinetic energies
    void calcKE();

    //! Calculate a stable time-step (not yet implemented)
    void calcTimeStep();

    //! Integrator
    void integrate(Real t, Real dtn);

    //! Calculate derived variables from state vector
    void calcDerivedVars();

    //! Calculate body forces
    void calcForces(Real t, Real &fx, Real &fy, Real &fz,
                    Real &mx, Real &my, Real &mz);

    //! \name Virtual Functions for I/O
    //! These functions are implemented for each specific physics
    //! to permit custom options to be coded for reading and writing
    //! restart files, time-history data, bc's, etc.
    //@{
    virtual void writeSolving();

    virtual void echoOptions(ostream& ofs);

    void echoBCs(ostream& ofs);

    void echoICs(ostream& ofs);

    //! Write the header for the global time-history data file
    void globHeader(ofstream& globf);

    //! Write the global time history data at each step
    void writeGlobal(ofstream& globf, Real time, Real t_ke, Real r_ke);

    //@}

    //! Add a body force to the vector of body forces
    void addBodyForce(RBDynBodyForce* force);

    //! Check the inertial tensor
    void checkITensor();

    //! Set IC's
    void setICs(Real* vic, Real* ric);

  protected:

    //! \name Data registered for rigid-body dynamics
    //@{
    DataIndex DISPX;
    DataIndex DISPY;
    DataIndex DISPZ;
    DataIndex VELX;
    DataIndex VELY;
    DataIndex VELZ;
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    RBDynamics(const RBDynamics&);
    RBDynamics& operator=(const RBDynamics&);
    //@}

    //! \name mass and Inertia tensor about CG and CG coordinates
    //@{
    Real mass, massinv;
    Real Xcg[3];
    Real Icg[3][3], Icginv[3][3], I[3][3], Iinv[3][3];
    //@}

    //! Rotation tensors
    Real R[3][3], RT[3][3];

    //! Linear and angular momentum
    Real P[3], L[3];

    //! Quaternion (4-vector): q[0] = s, q[1] - q[3] = v.
    Real q[4];

    //! \name displacement, velocity, and rotational velocity about CG
    //@{
    Real   pos_cg[3];
    Real   vel_cg[3];
    Real omega_cg[3];
    //@}

    //! \name Storage for ODE
    //@{
    Real Yn[STATE_SIZE], Ytemp[STATE_SIZE];
    Real Y1[STATE_SIZE], Y2[STATE_SIZE], Y3[STATE_SIZE];
    Real Ydot[STATE_SIZE];
    //@}

    //! \name Energies
    //@{
    Real kin_energy;
    Real tran_energy;
    Real rot_energy;
    //@}

    //! \name Linear and rotational velocity IC's
    //@{
    Real vel_ic[3];
    Real rot_ic[3];
    //@}

    //! \name the force object pointer
    //@{
    vector<RBDynBodyForce*> body_forces;
    //@}
};

}
#endif // RBDynamics_h
