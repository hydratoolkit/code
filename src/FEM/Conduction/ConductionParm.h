//******************************************************************************
//! \file    src/FEM/Conduction/ConductionParm.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Defined parameters, i.e., the penalty multiplier
//******************************************************************************
#ifndef ConductionParm_h
#define ConductionParm_h

namespace Hydra {

#define CONDUCTION_PENALTY_MULTIPLIER 1.0e+15

}

#endif // ConductionParm_h
