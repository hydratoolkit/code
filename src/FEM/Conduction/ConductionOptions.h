//******************************************************************************
//! \file    src/FEM/Conduction/ConductionOptions.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Solution options for Conduction
//******************************************************************************
#ifndef ConductionOptions_h
#define ConductionOptions_h

namespace Hydra {

//! \brief ConductionOptions provides the solution options
namespace ConductionOptions {

//! Flags for solution method
enum SolutionMethod {
  STEADY_STATE,
  TRANSIENT
};

//! Flags for time step control
enum TimeStepType {
  FIXED_TIME_STEP,      //!< Fixed time-step size
  ADAPTIVE_TIME_STEP    //!< Adaptive, e.g., using TR-BDF2
};

//! Flags for time integration
enum TimeInt {
  BDF2_INT,
  THETA_INT
};

} // end of namespace ConductionOptions

}

#endif // ConductionOptions_h
