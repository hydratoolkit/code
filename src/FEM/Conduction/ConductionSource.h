//******************************************************************************
//! \file    src/FEM/Conduction/ConductionSource.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:39:27 2011
//! \brief   Volumetric heat source terms for the FEM heat conduction.
//******************************************************************************
#ifndef ConductionSource_h
#define ConductionSource_h

#include <HydraTypes.h>
#include <Source.h>

namespace Hydra {

class UnsMesh;

//! Volumetric heat source terms
class ConductionSource : public Source {

  public:

    //! \name Constructor/Destructors
    //@{
             ConductionSource();
             ConductionSource(int tblid, Real amp, int setid);
    virtual ~ConductionSource();
    //@}

    //! Apply the heat source
    void apply(UnsMesh& mesh,
               const Real timen, const Real timenp1, const Real dt,
               const Real thetaFn, const Real thetaFnp1,
               const Real* vol, Real* rhs);

  private:

    //! Don't permit copy or assignment operators
    //@{
    ConductionSource(const ConductionSource&);
    ConductionSource& operator=(const ConductionSource&);
    //@}

};

}
#endif
