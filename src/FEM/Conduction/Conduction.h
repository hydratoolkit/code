//******************************************************************************
//! \file    src/FEM/Conduction/Conduction.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:34:20 2011
//! \brief   FEM Heat conduction
//******************************************************************************

#ifndef Condcution_h
#define Condcution_h

#include <vector>

#include <UnsPhysics.h>
#include <ConductionBCs.h>
#include <ConductionSource.h>

namespace Hydra {

//! Struct for IC's
struct ConductionICPackage {
  Real temp;
  ConductionICPackage() : temp (0.0) {}
 };

class LASolver;
class LAVector;
class LAMatrix;


//! Conduction heat transfer class
class Conduction : public UnsPhysics {

  public:

    //! \name Constructor/Destructor
    //@{
             Conduction();
    virtual ~Conduction() {}
    //@}

    //**************************************************************************
    // Virtual public interface

    //! Check the health of the code during solution phase
    virtual bool codeStatus() {return true;}

    //! Echo physics-specific options
    virtual void echoOptions(ostream& ofs);

    //! Finalize the physics
    virtual void finalize();

    //! Initialize the physics
    virtual void initialize();

    //! Register all solution variables
    virtual void registerData();

    //! Allocate and setup physics specific variables
    virtual void setup();

    //! Setup variables that can be migrated for dynamics load-balancing
    virtual void setupMigrationVars(vector<DataIndex>& elem_var,
                                    vector<DataIndex>& node_var);

    //! Solve the physics problem
    virtual void solve();

    //! Write a message identifying the physics solver
    virtual void writeSolving();

    //! Setup the internal set Id for BC's with a merged set Id
    virtual void setupMergedSetBCs();

    //**************************************************************************
    // Boundary Conditions and Initial Conditions

    //! Add a heat flux boundary condition to list of heat flux BC's
    void addHeatFluxBC(const BCPackage& bc);

    //! Load a heat flux BC given an id 'i'
    HeatFluxBC* getHeatFluxBC(int i) {return m_heatfluxbc[i];}

    //! How many heat flux bc's are loaded
    int numHeatFluxBC() {return m_heatfluxbc.size();}

    //! Add a convection boundary condition
    void addConvectionBC(const BCPackage& bc, Real Tamb);

    //! Load a convection boundary condtion
    ConvectionBC* getConvectionBC(int i) {return m_convectionbc[i];}

    //! How many convection bc's are loaded
    int numConvectionBC() {return m_convectionbc.size();}

    //! Add a temperature boundary condition
    void addTemperatureBC(const BCPackage& bc);

    //! Setup unique node lists in temperature BC where needed
    void setupTemperatureBCs();

    //! Load a temperature boundary condtion
    TemperatureBC* getTemperatureBC(int i) {return m_temperaturebc[i];}

    //! How many temperature bc's are loaded
    int numTemperatureBC() {return m_temperaturebc.size();}

    //! Set the initial conditions
    void setICs(ConductionICPackage& ics);

    //**************************************************************************
    // Heat conduction specific methods

    //! Time weight
    void setTheta(Real parm) {thetak = parm;}

    //! Mass type
    void setMass(int mass) {masstype = mass;}

    //! Compute/collect the memory summary
    void computeMemory();

    //! Print memory allocated in detail
    //!   \param[in] ofs      stream to print memory info to
    void printMemory(ostream& ofs);

  private:

    //! Don't permit copy or assignment operators
    //@{
    Conduction(const Conduction&);
    Conduction& operator=(const Conduction&);
    //@}

    //**************************************************************************
    // Boundary condition and source methods

    //! Apply BC's to RHS vector
    void applyBCs();

    //! Apply BC's to LHS operator
    void applyBCsLHS(Real* Adiag);

    //**************************************************************************
    // Heat conduction specific methods

    //! Form the M+K and M-K operators
    void formMK(bool BDF2, bool first_pass);
    void formMK(bool BDF2, Real alpha, Real wtmpk, Real wtmmk, Hex8* ec);
    void formMK(bool BDF2, Real alpha, Real wtmpk, Real wtmmk, Quad4* ec);
    void formMK(bool BDF2, Real alpha, Real wtmpk, Real wtmmk, Tet4* ec);

    //! Form the steady-state operators
    void formK();
    void formK(Hex8*    ec);
    void formK(Pyr5*    ec);
    void formK(Quad4*   ec);
    void formK(Tet4*    ec);
    void formK(Wedge6*  ec);

    //! Initialize the field variables
    void initFieldVars();

    //! Set the increment solution parameter
    void setIncParam();

    //! Steady-state solve
    void solveSteadyState();

    //! Transient solve
    void solveTransient();

    //**************************************************************************
    // Linear algebra related functions

    //! Finalize all the solvers
    void finalLinearSolvers();

    //! Initialize the linear equation solvers, matrices and vectors
    void initLinearSolvers();

    //! Map global node Id's to equation numbers
    void mapNodeEqNumber();

    //! Mark the nodes associated with nodal penalty conditions
    void markNodePenalty(int* nproc, int* nodebc, int* send_buf, int* recv_buf);

    //! Initialize matrices etc. for steady state solve
    //! (this cannot happen in ::solve or memory will be undercounted)
    void initSteadyState();

    //**************************************************************************
    // Material evaluation/update

    //! Update the material state
    void updateMaterialState();

    //**************************************************************************
    // Output related functions

    // Echo BC's
    void echoBCs(ostream& ofs);

    // Echo Heat Sources
    void echoHeatSources(ostream& ofs);

    // Echo IC's
    void echoICs(ostream& ofs);

    // Echo time integrator options
    void echoTimeInt(ostream& ofs);

    //! Query memory used by boundary conditions in bytes
    Real getBCMemory();

    //! Write the screen report
    void writeReport(ostream& ofs, int Ninc);

    //! Write the screen report header
    void writeReportHeader(ostream& ofs);

    //**************************************************************************
    // Registration for all variables, communications buffers, timers

    //! Register all timers for this physics
    void registerTimers();

    //**************************************************************************
    //* Data members

    //! \name Data registered for advection-diffusion
    //! Only temperature and heat source term need to be registered
    //! for conduction.
    //@{
    DataIndex TEMPERATURE;
    DataIndex TNM1;
    DataIndex Q;
    DataIndex CAPACITANCE;
    //@}

    //! \name Data required for linear algegra
    //@{
    DataIndex NODEEQ_MAP; // equation number to global node number mapping
    DataIndex NODEEQ_VAR;
    DataIndex TEMP_ELEM;   // Scratch array

    //@}

    DataIndex SEND_BUF;    // Nodal send buffer
    DataIndex RECV_BUF;    // Nodal recv buffer


    // Time step data
    Real m_dt;
    Real m_totalTime;

    // The increment parameters
    int m_Ninc;
    Real m_time;
    Real m_time_np1;

    bool m_isRestart;      //!< Indicate current step is a restart

    //! Time weights
    Real m_thetaKn;   // thetak
    Real m_thetaKnp1; // thetak
    Real thetak;

    int m_ttyi;       // Interval for screen prints

    //! \name Linear Solver
    //@{
    bool m_convergence;
    bool m_A_set;
    LASolver* m_solver;
    LAMatrix* m_A;
    LAVector* m_b;
    LAVector* m_x;
    LAVector* m_C;

    int m_mxNdrow;        //!< maximum node row size in processor
    int m_mxNdrowOff;     //!< maximum node row size off processor
    vector<int> m_d_nnz;  //!< per row maximum node row size in processor
    vector<int> m_o_nnz;  //!< per row maximum node row size off processor

    int m_nodeEqStart;    //!< First number of the node equation number
    int m_nodeEqEnd;      //!< Last number of the node equation number
    int m_Nnp_eq;         //!< number of node equation number
    //@}

    int masstype;

    //! Timers
    TimerIndex WRITE_TIME;

    //! \name STL vectors of BC objects
    //@{
    vector<HeatFluxBC*> m_heatfluxbc;

    vector<ConvectionBC*> m_convectionbc;

    vector<TemperatureBC*> m_temperaturebc;

    vector<ConductionSource*> m_heatsource;
    //@}

    //! Simple IC's
    ConductionICPackage m_ics;

};

}
#endif // Condcution_h
