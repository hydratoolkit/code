//******************************************************************************
//! \file    src/FEM/Conduction/ConductionBCs.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:34:20 2011
//! \brief   Heat conduction BC's
//******************************************************************************
#ifndef ConductionBCs_h
#define ConductionBCs_h

#include <HydraTypes.h>
#include <UnsMesh.h>
#include <NodeSetBC.h>
#include <SideSetBC.h>
#include <BCPackage.h>
#include <LAMatrix.h>

namespace Hydra {

//! Convection boundary condition based on sidesets
class ConvectionBC : public SideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             ConvectionBC(const BCPackage& bc, Real Tambient);
    virtual ~ConvectionBC() {}
    //@}

    //! Integrate convective heat flux over the surface and assemble into {Q}
    void apply(UnsMesh* mesh, Real* q, Real t, Real dt);

    //! Integrate convective heat flux and assemble into LHS operators
    void applyLHS(UnsMesh* mesh, Real* mpk, Real t, Real dt);

    //! Set the ambient temperature, T_infinity
    void setTamb(Real val) {Tamb = val;}

    //! Get the ambient temperature, T_infinity
    Real getTamb()         {return Tamb;}

  protected:

  private:

    //! Don't permit copy or assignment operators
    //@{
    ConvectionBC(const ConvectionBC&);
    ConvectionBC& operator=(const ConvectionBC&);
    //@}

    Real Tamb;  //! Ambient temperature
};


//! Heat flux boundary condition for sidesets
class HeatFluxBC : public SideSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
             HeatFluxBC(const BCPackage& bc);
    virtual ~HeatFluxBC() {}
    //@}

    //! Integrate the heat flux over the surface and assemble into {Q}
    void apply(UnsMesh* mesh, Real* q, Real t, Real dt);

  protected:

  private:

    //! Don't permit copy or assignment operators
    //@{
    HeatFluxBC(const HeatFluxBC&);
    HeatFluxBC& operator=(const HeatFluxBC&);
    //@}

};

//! Temperature boundary condition based on sidesets
class TemperatureBC : public NodeSetBC {

  public:

    //! \name Constructor/Destructor
    //@{
    //! Construct a node-based temperature boundary condition
    //!   \param[in] bc   BC package
             TemperatureBC(const BCPackage& bc);
    virtual ~TemperatureBC();
    //@}

    //! Apply BC
    //!   \param[in] mesh        Data mesh
    //!   \param[in] Nproc       Number of processors
    //!   \param[in] nodeEqStart Equation number start
    //!   \param[in] nodeEqEnd   Equation number end
    //!   \param[in] nodeEqMap   Node to equation map
    //!   \param[in] A           Operator
    //!   \param[in] rhs         RHS
    //!   \param[in] t           Time
    //!   \param[in] scale       Scale factor
    void apply(UnsMesh& mesh, int Nproc, int nodeEqStart, int nodeEqEnd,
               const int* nodeEqMap, LAMatrix* A, Real* rhs,
               Real t, Real scale);

    //! Apply BC to LHS
    //!   \param[in] mesh        Data mesh
    //!   \param[in] Nproc       Number of processors
    //!   \param[in] nodeEqStart Equation number start
    //!   \param[in] nodeEqEnd   Equation number end
    //!   \param[in] nodeEqMap   Node to equation map
    //!   \param[in] Adiag       Operator diagonal
    //!   \param[in] dirty       Bool flag to avoid redundant BC application
    void applyLHS(UnsMesh& mesh, int Nproc, int nodeEqStart,
                  int nodeEqEnd, const int* nodeEqMap, Real* Adiag,
                  bool* dirty);

    //! Mark the prescribed boundary condition
    //!   \param[in] mesh   Data mesh
    //!   \param[in] kthBc  k-th boundary condition, the last one always wins.
    //!   \param[in] nodeBc An array to mark node (output)
    void markNodePenalty(UnsMesh& mesh, int kthBc, int* nodeBc);

  private:

    //! Don't permit copy or assignment operators
    //@{
    TemperatureBC(const TemperatureBC&);
    TemperatureBC& operator=(const TemperatureBC&);
    //@}

    Real m_penalty; //!< BC penalty factor
};

}
#endif // ConductionBCs_h
