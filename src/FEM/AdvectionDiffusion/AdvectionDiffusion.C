//******************************************************************************
//! \file    src/FEM/AdvectionDiffusion/AdvectionDiffusion.C
//! \\author  Mark A. Christon
//! \date    Thu Jul 14 12:33:32 2011
//! \brief   FEM Advection-Diffusion
//******************************************************************************

#include <cstring>
#include <cstdlib>

#include <cmath>
#include <iomanip>
#include <iostream>

using namespace std;

#include <AdvectionDiffusion.h>
#include <Control.h>
#include <FEMAdvectionDiffusionControl.h>
#include <IterativeSolverControl.h>
#include <OutputControl.h>
#include <MPIWrapper.h>
#include <ArrayLimits.h>
#include <UnsPhysicsFieldDelegates.h>

using namespace Hydra;
using namespace Hydra::ArrayLimits;


void
AdvectionDiffusion::solve()
/*******************************************************************************
Routine: solve - driver to solve thermal conduction problems
Author : Mark A. Christon

This driver assumes a fixed time-step and constant in time properties.
*******************************************************************************/
{
  p0cout << endl << "\tSolving time-dependent advection-diffusion problem"
         << endl << endl;

  Category* ocat  = control->getCategory(OUTPUT_CATEGORY);
  Category* pcat  = control->getCategory(FEM_ADVECTION_DIFFUSION_CATEGORY);
  Category* eqcat = pcat->getCategory(FEMAdvectionDiffusionControl::NODE_SOLVER_CAT);

  int Nsteps = control->getOptionVal(Control::NSTEPS);
  int itchk  = eqcat->getOptionVal(IterativeSolverControl::ITCHK);
  int itmax  = eqcat->getOptionVal(IterativeSolverControl::ITMAX);
  int iprt   = 0;

  int plti   = ocat->getOptionVal(OutputControl::PLTI);
  int plnum  = ocat->getOptionVal(OutputControl::PLNUM);

  int lun    = 6;
  int itknt  = 0;

  Real fmin, fmax;

  Real eps   = eqcat->getParamVal(IterativeSolverControl::EPS);

  Real dt    = control->getParamVal(Control::DELTAT);
  Real t     = control->getParamVal(Control::START_TIME);

  Real* mpk  = mesh->getVariable<Real>(MPK);
  Real* mmk  = mesh->getVariable<Real>(MMK);
  int*  mkj  = mesh->getVariable<int>(MKJ);
  Real* temp = mesh->getVariable<Real>(TEMPERATURE);
  Real* q    = mesh->getVariable<Real>(Q);
  Real* r    = mesh->getVariable<Real>(R);
  Real* p    = mesh->getVariable<Real>(PP);
  Real* ap   = mesh->getVariable<Real>(AP);
  Real* z    = mesh->getVariable<Real>(Z);
  Real* rmi  = mesh->getVariable<Real>(W);

  bool recordedMemoryFootprint = false;

  // Apply BC's
  applyNodalBC(TEMP, TEMPERATURE, 0, 1.0);

  // Write out the plot data
  calcStreamFunc();
  calcVorticity(TMP, Z);
  io->selectInstPlotOutput();
  io->writePlot(plnum, 0, t, control, mesh);

  // Write the time-history data
  writeTHdata(t);

  getVarLimits(temp, fmin, fmax, Nnp);
  p0cout << setiosflags( ios::scientific | ios::showpoint) << setprecision(2);
  p0cout << "\t Step    Time       Tmin       Tmax  " << endl;
  p0cout << "\t=====  ========  =========  =========" << endl;
  p0cout << "\t" << setw(5) << 0 << "  " << t << "   "
         << fmin << "  " << fmax << endl;

  int plotcnt = 0;
  for (int i=0; i<Nsteps; i++) {

    // Form the operators -- fixed in time
    formMK();

    // Complete the right-hand-side terms
    FORTRAN(smvb) (mmk, temp, q, mkj, Nnp, mxNdrow);

    // Apply BC's
    applyPenaltyLHS(mpk, TEMP);
    applyPenaltyRHS(mpk, q, TEMP, 1.0);

    // Solve the linear system
    FORTRAN(dscga) (mpk, mkj, temp, q, p, r, ap, z, rmi, Nnp, mxNdrow, itchk,
                    itmax, eps, iprt, lun, itknt );

    t += dt;
    plotcnt++;

    // Calculate your error norms as required
    if (t == 50.0) {
      p0cout << "Calculating Error Norms ..." << endl;
      calcErrorNorms(t);
    }

    // Write out a state
    if(plotcnt == plti || i+1 == Nsteps) {
      calcStreamFunc();
      calcVorticity(TMP, Z);
      io->selectInstPlotOutput();
      io->writePlot(plnum, i+1, t, control, mesh);
      plotcnt = 0;
    }

    // Write the time-history data
    writeTHdata(t);

    // compute the memeory footprint (only in the first step)
    if (!recordedMemoryFootprint) {
      computeMemory();
      recordedMemoryFootprint = true;
    }

    getVarLimits(temp, fmin, fmax, Nnp);
    p0cout << "\t" << setw(5) << i+1 << "  " << t << "   "
           << fmin << "  " << fmax << endl;
  }
  // compute the memeory footprint (in case there are zero steps)
  if (!recordedMemoryFootprint) {
    computeMemory();
    recordedMemoryFootprint = true;
  }

  // Save the final plot number, simulation time
  ocat->setOptionVal(OutputControl::PLNUM, plnum);
  control->setParamVal(Control::START_TIME, t);

}

AdvectionDiffusion::AdvectionDiffusion()
/*******************************************************************************
Routine: AdvectionDiffusion() - constructor
Author : Mark A. Christon
*******************************************************************************/
{
  // Initialize DataIndex values
  TEMPERATURE = UNREGISTERED;
  Q   = UNREGISTERED;
  MPK = UNREGISTERED;
  MMK = UNREGISTERED;
  MKJ = UNREGISTERED;
  PP  = UNREGISTERED;
  AP  = UNREGISTERED;
  W   = UNREGISTERED;
  R   = UNREGISTERED;
  Z   = UNREGISTERED;
  TMP = UNREGISTERED;

  // Default time weight
  thetak = 0.5;

  // Default mass
  masstype = LUMPED;
}

void
AdvectionDiffusion::formMK()
/*******************************************************************************
Routine: formMK - calculate & assemble the conduction operators
Author : Mark A. Christon
*******************************************************************************/
{
  // Setup the mass matrix
  Real alpha;
  switch(masstype) {
    case LUMPED:
      alpha = 0.0;
      break;

    case CONSISTENT:
      alpha = 1.0;
      break;

    case HIGH_ORDER:
      alpha = 0.5;
      break;

    default:
      alpha = 1.0;
      break;
  }

  Real     dt = control->getParamVal(Control::DELTAT);
  Real  wtmpk = dt*thetak;
  Real  wtmmk = dt*(1.0 - thetak);

  // Zero the matrix & RHS
  Real* mpk = mesh->getVariable<Real>(MPK);
  Real* mmk = mesh->getVariable<Real>(MMK);
  Real*   q = mesh->getVariable<Real>(Q);
  memset(mpk, 0, Nnp*mxNdrow*sizeof(Real));
  memset(mmk, 0, Nnp*mxNdrow*sizeof(Real));
  memset(q, 0, Nnp*sizeof(Real));

  // Loop over element classes
  int Nec = mesh->numElementClass();
  for (int i=0; i<Nec; i++) {
    Element* ec = mesh->getElementClass(i);
    Eltype etype = ec->getEltype();
    switch(etype) {
    case TRI3:
      cout << "!!! form M+K TRI3 not implemented !!!" << endl;
      break;
    case QUAD4: {
      Quad4* q4ec = reinterpret_cast<Quad4*>(ec);
      formMK(dt, alpha, wtmpk, wtmmk, q4ec);
      }
      break;
    case HEX8:
      cout << "!!! form M+K HEX8 not implemented !!!" << endl;
      break;
    case PYR5:
      cout << "!!! form M+K PRY5 not implemented !!!" << endl;
      break;
    case TET4:
      cout << "!!! form M+K TET4 not implemented !!!" << endl;
      break;
    case WEDGE6:
      cout << "!!! form M+K WEDGE6 not implemented !!!" << endl;
      break;
    case BEAM2:
    case POLY2D:
    case POLY3D:
    case NUM_ELEMENT_TYPES:
      cout << "!!! Invalid element type found !!!" << endl;
      break;
    }
  }
}

void
AdvectionDiffusion::formMK(Real dt, Real alpha, Real wtmpk, Real wtmmk,
                           Quad4* ec)
/*******************************************************************************
Routine: formMK - calculate & assemble the operators
Author : Mark A. Christon
*******************************************************************************/
{
  int  n0, n1, n2, n3;

  Real j11, j12, j21, j22;
  Real i11, i12, i21, i22;
  Real dNdx[4], dNdy[4];
  Real Me[4][4];
  Real Mle[4];
  Real Ke[4][4];
  Real C[2][4];

  Real*    x = mesh->getX();
  Real*    y = mesh->getY();
  Real* velx = mesh->getVariable<Real>(VELX);
  Real* vely = mesh->getVariable<Real>(VELY);
  Real* temp = mesh->getVariable<Real>(TEMPERATURE);
  Real*    q = mesh->getVariable<Real>(Q);

  Real*  mpk = mesh->getVariable<Real>(MPK);
  Real*  mmk = mesh->getVariable<Real>(MMK);
  int*   mkj = mesh->getVariable<int>(MKJ);

  Real xm1, xm2, xm3, xm4, xm5, xm6, xm7, xm8;
  Real rho, Cp, k11, k12, k22;
  Real ev, evi, ue, ve;
  Real dto2 = 0.5*dt;
  Real o4th = 0.25;

  int nel  = ec->getNel();
  int nnpe = ec->getNnpe();

  Quad4IX* q4 = ec->getQuad4Connectivity();

  Quad4QuadSet& q4qs = ec->getQuad4QuadSet();

  int Nqpt = q4qs.getNumVolPts();

  int mid = ec->getMatId();
  Material* mat = mesh->getMaterial(mid);

  // The 'usual' quadrature and assembly process
  for (int n=0; n<nel; n++) {

    // Zero the element operators
    memset(Me, 0, sizeof(Me));
    memset(Mle, 0, sizeof(Mle));
    memset(Ke, 0, sizeof(Ke));
    memset(C, 0, sizeof(C));

    n0 = q4[n].ix[0];
    n1 = q4[n].ix[1];
    n2 = q4[n].ix[2];
    n3 = q4[n].ix[3];

    // Centroid velocities
    ue = o4th*(velx[n0] + velx[n1] + velx[n2] + velx[n3]);
    ve = o4th*(vely[n0] + vely[n1] + vely[n2] + vely[n3]);

    // Load the material properties
    rho = mat->Density();
    Cp = mat->SpecificHeat();
    mat->Conductivity(k11, k12, k22);

    // Account for BTD
    k11 += dto2*ue*ue;
    k12 += dto2*ue*ve;
    k22 += dto2*ve*ve;

    // Form the element-level operators
    for (int k=0; k<Nqpt; k++) {
      const Real* N    = q4qs.getVsfArray(0, k);
      const Real* dNdr = q4qs.getVsfArray(1, k);
      const Real* dNds = q4qs.getVsfArray(2, k);

      // Jacobian
      j11 = dNdr[0]*x[n0] + dNdr[1]*x[n1] + dNdr[2]*x[n2] + dNdr[3]*x[n3];
      j12 = dNds[0]*x[n0] + dNds[1]*x[n1] + dNds[2]*x[n2] + dNds[3]*x[n3];
      j21 = dNdr[0]*y[n0] + dNdr[1]*y[n1] + dNdr[2]*y[n2] + dNdr[3]*y[n3];
      j22 = dNds[0]*y[n0] + dNds[1]*y[n1] + dNds[2]*y[n2] + dNds[3]*y[n3];

      ev  = j11*j22 - j12*j21;
      if (ev < 0.0) {
        p0cout << "Error in quadrature at Gauss point " << k
               << " ev = " << ev << endl;
      }
      evi = 1.0/ev;

      // Mass matrix -- symmetric terms only
      Real rcpv = rho*Cp*ev;
      Me[0][0] += rcpv*N[0]*N[0];
      Me[0][1] += rcpv*N[0]*N[1];
      Me[0][2] += rcpv*N[0]*N[2];
      Me[0][3] += rcpv*N[0]*N[3];
      Me[1][1] += rcpv*N[1]*N[1];
      Me[1][2] += rcpv*N[1]*N[2];
      Me[1][3] += rcpv*N[1]*N[3];
      Me[2][2] += rcpv*N[2]*N[2];
      Me[2][3] += rcpv*N[2]*N[3];
      Me[3][3] += rcpv*N[3]*N[3];

      // Inverse Jacobian
      i11 =  evi*j22;
      i12 = -evi*j12;
      i21 = -evi*j21;
      i22 =  evi*j11;

      // dN/dx, dN/dy operators
      dNdx[0] = dNdr[0]*i11 + dNds[0]*i21;
      dNdx[1] = dNdr[1]*i11 + dNds[1]*i21;
      dNdx[2] = dNdr[2]*i11 + dNds[2]*i21;
      dNdx[3] = dNdr[3]*i11 + dNds[3]*i21;
      dNdy[0] = dNdr[0]*i12 + dNds[0]*i22;
      dNdy[1] = dNdr[1]*i12 + dNds[1]*i22;
      dNdy[2] = dNdr[2]*i12 + dNds[2]*i22;
      dNdy[3] = dNdr[3]*i12 + dNds[3]*i22;

      // C-operators for advective terms
      C[0][0] += dNdx[0]*ev;
      C[0][1] += dNdx[1]*ev;
      C[0][2] += dNdx[2]*ev;
      C[0][3] += dNdx[3]*ev;
      C[1][0] += dNdy[0]*ev;
      C[1][1] += dNdy[1]*ev;
      C[1][2] += dNdy[2]*ev;
      C[1][3] += dNdy[3]*ev;

      // Conductivity operator -- symmetric conductivity tensor
      xm1 = k11*dNdx[0] + k12*dNdy[0];
      xm2 = k12*dNdx[0] + k22*dNdy[0];
      xm3 = k11*dNdx[1] + k12*dNdy[1];
      xm4 = k12*dNdx[1] + k22*dNdy[1];
      xm5 = k11*dNdx[2] + k12*dNdy[2];
      xm6 = k12*dNdx[2] + k22*dNdy[2];
      xm7 = k11*dNdx[3] + k12*dNdy[3];
      xm8 = k12*dNdx[3] + k22*dNdy[3];

      // Symmetric terms only
      Ke[0][0] += ( dNdx[0]*xm1 + dNdy[0]*xm2 )*ev;
      Ke[0][1] += ( dNdx[0]*xm3 + dNdy[0]*xm4 )*ev;
      Ke[0][2] += ( dNdx[0]*xm5 + dNdy[0]*xm6 )*ev;
      Ke[0][3] += ( dNdx[0]*xm7 + dNdy[0]*xm8 )*ev;
      Ke[1][1] += ( dNdx[1]*xm3 + dNdy[1]*xm4 )*ev;
      Ke[1][2] += ( dNdx[1]*xm5 + dNdy[1]*xm6 )*ev;
      Ke[1][3] += ( dNdx[1]*xm7 + dNdy[1]*xm8 )*ev;
      Ke[2][2] += ( dNdx[2]*xm5 + dNdy[2]*xm6 )*ev;
      Ke[2][3] += ( dNdx[2]*xm7 + dNdy[2]*xm8 )*ev;
      Ke[3][3] += ( dNdx[3]*xm7 + dNdy[3]*xm8 )*ev;
    }

    // Setup the lumped mass matrix (symmetric mass)
    Mle[0] = Me[0][0] + Me[0][1] + Me[0][2] + Me[0][3];
    Mle[1] = Me[0][1] + Me[1][1] + Me[1][2] + Me[1][3];
    Mle[2] = Me[0][2] + Me[1][2] + Me[2][2] + Me[2][3];
    Mle[3] = Me[0][3] + Me[1][3] + Me[2][3] + Me[3][3];

    // Assemble the operators
    setColumnIds(mkj, q4[n].ix, Nnp, nnpe);

    for (int i=0; i<nnpe; i++) {
      int ndi = q4[n].ix[i];
      for (int j=0; j<nnpe; j++) {
        if (i == j) {
          mpk[ndi] += alpha*Me[i][j] + (1.0-alpha)*Mle[i] + wtmpk*Ke[i][j];
          mmk[ndi] += alpha*Me[i][j] + (1.0-alpha)*Mle[i] - wtmmk*Ke[i][j];
        }
        else if (j > i) {
          int jc = col_id[i][j];
          mpk[Nnp*jc + ndi] += alpha*Me[i][j] + wtmpk*Ke[i][j];
          mmk[Nnp*jc + ndi] += alpha*Me[i][j] - wtmmk*Ke[i][j];
        }
        else if (j < i) {
          int jc = col_id[i][j];
          mpk[Nnp*jc + ndi] += alpha*Me[j][i] + wtmpk*Ke[j][i];
          mmk[Nnp*jc + ndi] += alpha*Me[j][i] - wtmmk*Ke[j][i];
        }
      }
    }

    // Setup the right-hand-side advective terms -- centroid advection!!
    Real qe = dt*o4th*rho*Cp*(
      ue*(C[0][0]*temp[n0]+C[0][1]*temp[n1]+C[0][2]*temp[n2]+C[0][3]*temp[n3]) +
      ve*(C[1][0]*temp[n0]+C[1][1]*temp[n1]+C[1][2]*temp[n2]+C[1][3]*temp[n3]));

    q[n0] -= qe;
    q[n1] -= qe;
    q[n2] -= qe;
    q[n3] -= qe;

  } // End of the element loop
}

void
AdvectionDiffusion::initialize()
/*******************************************************************************
Routine: initialize - setup temperature initial conditions
Author : Mark A. Christon
*******************************************************************************/
{
  if (control->getFlagVal(Control::DORESTART)) return;

  Real* x    = mesh->getX();
  // Real* y    = mesh->getY();
  Real* velx = mesh->getVariable<Real>(VELX);
  Real* vely = mesh->getVariable<Real>(VELY);
  Real* temp = mesh->getVariable<Real>(TEMPERATURE);

#define GAUSSIAN_PUFF

// Baptista-1
// IC's for Gaussian puff in constant stream
#ifdef GAUSSIAN_PUFF
  Real x0 = 0.5;
  Real sigma = 0.05;
  for (int i=0; i<Nnp; i++) {
    velx[i] = 1.0;
    vely[i] = 0.0;
    Real dx = x[i] - x0;
    temp[i] = exp(-(dx*dx)/(2.0*sigma*sigma));
  }
#endif

// Baptista-2
// Rotating Gaussian cone
#ifdef GAUSSIAN_CONE
  Real omega = PI/100.0;
  Real sigma = 10.0;
  Real x0    = 50.0;
  Real y0    = 0.0;
  for (int i=0; i<Nnp; i++) {
    velx[i] = -omega*y[i];
    vely[i] =  omega*x[i];
    Real dx = x[i] - x0;
    Real dy = y[i] - y0;
    temp[i] = exp(-(dx*dx + dy*dy)/(2.0*sigma*sigma));
  }
#endif

// Baptista-3
// Simple Riemann problem
#ifdef BAPTISTA3
  for (int i=0; i<Nnp; i++) {
    velx[i] = 1.0;
    vely[i] = 0.0;
    temp[i] = 0.0;
  }
#endif

// Baptista-4
#ifdef BAPTISTA4
  for (int i=0; i<Nnp; i++) {
    velx[i] = sin(2.0*PI*x[i]);
    vely[i] = cos(PI*x[i]+PI);
    temp[i] = 0.0;
  }
#endif

// Baptista-5
#ifdef BAPTISTA5
  Real x0 = 0.5;
  Real y0 = 0.5;
  Real sigma = 0.10;
  for (int i=0; i<Nnp; i++) {
    velx[i] = 0.32*PI*sin(4.0*PI*x[i])*sin(4.0*PI*y[i]);
    vely[i] = 0.32*PI*cos(4.0*PI*x[i])*cos(4.0*PI*y[i]);
    temp[i] = 0.0;
    Real dx = x[i] - x0;
    Real dy = y[i] - y0;
    temp[i] = exp(-(dx*dx + dy*dy)/(2.0*sigma*sigma));
  }
#endif
}

void AdvectionDiffusion::registerData()
/*******************************************************************************
Routine: registerData() - register conduction specific data
Author : Mark A. Christon
*******************************************************************************/
{
  int nw  = Nnp*mxNdrow;

  TEMPERATURE = mesh->registerVariable(Nnp,
                                       sizeof(Real),
                                       "TEMPERATURE",
                                       NODE_CTR,
                                       SCALAR_VAR,
                                       LINEAR_ARRAY,
                                       true,
                                       true);

  io->registerDelegate("TEMPERATURE",
                       NODE_CTR,
                       SCALAR_VAR,
                       FIELD_DELEGATE,
                       this,
                       UnsPhysicsScalarField);

  Q           = mesh->registerVariable(Nnp,
                                       sizeof(Real),
                                       "Q",
                                       NODE_CTR,
                                       SCALAR_VAR,
                                       LINEAR_ARRAY,
                                       true,
                                       true);

  io->registerDelegate("Q",
                       NODE_CTR,
                       SCALAR_VAR,
                       FIELD_DELEGATE,
                       this,
                       UnsPhysicsScalarField);

  MPK = mesh->registerVariable(nw,
                               sizeof(Real),
                               "M+K",
                               NO_CTR);

  MMK = mesh->registerVariable(nw,
                               sizeof(Real),
                               "M-K",
                               NO_CTR);

  MKJ = mesh->registerVariable(nw,
                               sizeof(int),
                               "MKJ",
                               NO_CTR);

  PP  = mesh->registerVariable(Nnp,
                               sizeof(Real),
                               "PP",
                               NO_CTR);

  AP  = mesh->registerVariable(Nnp,
                               sizeof(Real),
                               "AP",
                               NO_CTR);

  W   = mesh->registerVariable(Nnp,
                               sizeof(Real),
                               "W",
                               NO_CTR);

  R   = mesh->registerVariable(Nnp,
                               sizeof(Real),
                               "R",
                               NO_CTR);

  Z   = mesh->registerVariable(Nnp,
                               sizeof(Real),
                               "Z",
                               NO_CTR);

  TMP = mesh->registerVariable(Nnp,
                               sizeof(Real),
                               "TMP",
                               NO_CTR);

  // Register the data for the base class too
  FlowPhysics::registerData();

}

void
AdvectionDiffusion::setup()
/*******************************************************************************
Routine: setup - setup for advection-diffusion
Author : Mark A. Christon
*******************************************************************************/
{
  // Calculate the nodal 1/2-bandwidth and row size
  calcNodalBandwidth();
  ostream& outf = io->outputStream(ASCII_OUTPUT);
  echoNodalStats(cout);
  echoNodalStats(outf);

  // Setup the base-class  -- also calls registerData()
  FlowPhysics::setup();

  // Setup the MKJ data structures
  setupNodeIndex(MKJ);

  // Initialize quadrature sets as required
  int Nec = mesh->numElementClass();
  for (int n = 0; n < Nec; n++) {
    Element* ec = mesh->getElementClass(n);
    Eltype etype = ec->getEltype();
    switch(etype) {
    case QUAD4: {
      Quad4* q4ec = reinterpret_cast<Quad4*>(ec);
      Quad4QuadSet& q4qs = q4ec->getQuad4QuadSet();
      q4qs.initialize(4,2);
      }
      break;
    case HEX8: {
      Hex8* h8ec = reinterpret_cast<Hex8*>(ec);
      Hex8QuadSet& h8qs = h8ec->getHex8QuadSet();
      h8qs.initialize(8,4);
      }
      break;
    case PYR5: {
      Pyr5* p5ec = reinterpret_cast<Pyr5*>(ec);
      Pyr5QuadSet& p5qs = p5ec->getPyr5QuadSet();
      p5qs.initialize(5,1);
      }
      break;
    case WEDGE6: {
      Wedge6* w6ec = reinterpret_cast<Wedge6*>(ec);
      Wedge6QuadSet& w6qs = w6ec->getWedge6QuadSet();
      w6qs.initialize(2,1);
      }
      break;
    case BEAM2:
    case POLY2D:
    case POLY3D:
    case TET4:
    case TRI3:
    case NUM_ELEMENT_TYPES:
      cout << "!!! Invalid element type found !!!" << endl;
      break;
    }
  }

  // Align output delegates with field, history, surface output requests
  io->setupOutputDelegates(control, mesh, PLOT);

  // Setup the time-history file labels
  writeTHhead();
}

void
AdvectionDiffusion::writeTHhead()
/*******************************************************************************
Routine: writeTHhead - variable names for time history data
Author : Mark A. Christon
*******************************************************************************/
{
}

void
AdvectionDiffusion::writeTHdata(Real /*time*/)
/*******************************************************************************
Routine: writeTHdata() - write out nodal time-history data
Author : Mark A. Christon
*******************************************************************************/
{
}

void
AdvectionDiffusion::writeSolving()
/*******************************************************************************
Routine: writeSolving() - write solution type
Author : Mark A. Christon
*******************************************************************************/
{
  ofstream& ofs = io->getOut();
  if (pid == 0) {
    cout << endl
         << "\tSolving an advection-diffusion problem ..."
         << endl;

    ofs  << endl
         << "\tSolving an advection-diffusion problem ..."
         << endl;
  }
}

void
AdvectionDiffusion::echoBCs(ostream& ofs)
/*******************************************************************************
Routine: echoBCs - echo all BC's
Author : Mark A. Christon
*******************************************************************************/
{
  // Echo the nodal BC's first
  echoNodalBCs(ofs);
}

void
AdvectionDiffusion::echoOptions(ostream& ofs)
/*******************************************************************************
Routine: echoOptions - echo all solver options
Author : Mark A. Christon
*******************************************************************************/
{
  echoBCs(ofs);
}

void
AdvectionDiffusion::calcErrorNorms(Real t)
/*******************************************************************************
Routine: calcErrorNorms - calculate your error Norms
Author : Mark A. Christon
*******************************************************************************/
{
  Real l1err = 0.0;
  Real l2err = 0.0;

  // You program the error norms here
  for (int i=0; i<Nnp; i++) {
    l1err += 0.0;
    l2err += 0.0;
  }

  p0cout << endl;
  p0cout << "Time     = " << t << endl;
  p0cout << "L1 Error = " << l1err << endl;
  p0cout << "L2 Error = " << l2err << endl;
  p0cout << endl;

}
