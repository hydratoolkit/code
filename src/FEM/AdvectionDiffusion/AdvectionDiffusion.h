//******************************************************************************
//! \file    src/FEM/AdvectionDiffusion/AdvectionDiffusion.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:33:32 2011
//! \brief   FEM Advection-Diffusion
//******************************************************************************

#ifndef AdvectionDiffusion_h
#define AdvectionDiffusion_h

#include <FlowPhysics.h>

namespace Hydra {

//! Advection-Diffusion class
class AdvectionDiffusion : public FlowPhysics {

  public:

    //! \name Constructor/Destructor
    //@{
             AdvectionDiffusion();
    virtual ~AdvectionDiffusion() {}
    //@}

    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData();
    //@}

    //! \name Virtual Physics Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    virtual void setup();

    virtual void initialize();

    virtual void solve();

    virtual void finalize() {}
    //@}

    //! Form the M+K and M-K operators
    void formMK();
    void formMK(Real dt, Real alpha, Real wtmpk, Real wtmmk, Quad4* ec);

    //! \name Virtual Functions for I/O
    //! These functions are implemented for each specific physics
    //! to permit custom options to be coded for reading and writing
    //! restart files, time-history data, bc's, etc.
    //@{
    virtual void writeTHhead();

    virtual void writeTHdata(Real time);

    virtual void writeSolving();

    virtual void echoOptions(ostream& ofs);

    void echoBCs(ostream& ofs);
    //@}

    // You program this to compute the error norms that you desire
    void calcErrorNorms(Real t);

    //! Time weight
    void setTheta(Real parm) {thetak = parm;}

    //! Mass type
    void setMass(int mass) {masstype = mass;}

    //! Setup the internal set Id for BC's with a merged set Id
    virtual void setupMergedSetBCs() {}

  protected:

    //! \name Data registered for advection-diffusion
    //! Only temperature and heat source term need to be registered
    //! for advection-diffusion -- FlowPhysics takes care of all
    //! the other flow parameters.
    //@{
    DataIndex TEMPERATURE;
    DataIndex Q;
    //@}

    //! \name Data required for linear algegra
    //@{
    DataIndex MPK; //!< DataIndex for the M+K operator
    DataIndex MMK; //!< DataIndex for the M-K operator
    DataIndex MKJ; //!< DataIndex for the compressed row storage column pointers
    //@}

    //! \name Temporary storage for required for the C-G algorithm
    //! These DataIndices are used for the temporary storage required
    //! for the C-G solution algorithm for nodal variables.
    //! Note that TMP is simply a 'temporary' variable.
    //@{
    DataIndex PP;  //!< Search direction p
    DataIndex AP;  //!< For the Ap product
    DataIndex W;   //!< The W scaling matrix -- really the diagonal of A
    DataIndex R;   //!< The residucal vector r
    DataIndex Z;   //!< The pseudo-residucal in PCCG
    DataIndex TMP; //!< Just a temporary vector
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    AdvectionDiffusion(const AdvectionDiffusion&);
    AdvectionDiffusion& operator=(const AdvectionDiffusion&);
    //@}

    //! Time weight
    Real thetak;

    //! Mass type
    int masstype;

};

}
#endif // AdvectionDiffusion_h
