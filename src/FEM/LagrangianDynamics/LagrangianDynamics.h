//******************************************************************************
//! \file    src/FEM/LagrangianDynamics/LagrangianDynamics.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:34:50 2011
//! \brief   DYNA-style 2-D Lagrangian Hydro
//******************************************************************************
#ifndef LagrangianDynamics_h
#define LagrangianDynamics_h

#include <vector>

#include <UnsPhysics.h>
#include <ArtificialViscosity.h>
#include <HourglassControl.h>
#include <LagrangianWorkSet.h>
#include <LagrangianQuad4WS.h>

namespace Hydra {

//! Lagrangian Dynamics class
class LagrangianDynamics : public UnsPhysics {

  public:

    //! \name Constructor/Destructor
    //@{
             LagrangianDynamics();
    virtual ~LagrangianDynamics();
    //@}

    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData();
    //@}

    /*! \name Virtual Physics Functions
        These functions are pure virtual functions that need to
        be implemented for each specific type of physics being solved
        with the framework.
     */
    //@{
    virtual void setup();

    virtual void initialize();

    virtual void solve();

    virtual void finalize() {}

    virtual void setupMergedSetBCs() {}
    //@}

    /*! \name Artificial viscosity & Hourglass control
        Manage artificial viscosity types and options
     */
    //@{
    bool addArtificialViscosity(bool allblocks, int Id, Real linear,
                                Real quadratic, vector<int>& classIds);

    ArtificialViscosity* getArtificialViscosity(int classId);

    void echoArtificialViscosity(ostream& ofs);

    bool addHourglassControl(bool allblocks, int Id, Real viscous,
                             Real stiff, HGType type, vector<int>& classIds);

    HourglassControl* getHourglassControl(int classId);

    void echoHourglassControl(ostream& ofs);
    //@}

    //! Setup element groups
    void setupGroups();

    //! Form the lumped mass matrix
    void formMass();

    //! Initialize an EOS -- simple place holder for now
    void initEOS();

    //! Update an EOS -- simple place holder for now
    void updateEOS();
    void updateDensity();
    void updateIntEnergy();
    void correctIntEnergy();

    //! Calculate the element volumes
    void curVolume();
    void curVol2D();
    void curVol3D();

    //! Calculate internal forces
    void calcInternalForce(CVector& F);
    void integrate();
    void calcTimeStep(Real time);
    Real calcTimeStep(Real dtmin, Real* e, Real* rho,
                      CVector& coord, CVector& vel, Quad4* ec);
    void calcKE();

    /*! \name Virtual Functions for I/O
        These functions are implemented for each specific physics
        to permit custom options to be coded for reading and writing
        restart files, time-history data, bc's, etc.
     */
    //@{
    virtual void writeSolving();

    virtual void echoOptions(ostream& ofs);

    void echoBCs(ostream& ofs);

    //! Write the header for the global time-history data file
    void globHeader(ofstream& globf);

    //! Write the global time history data at each step
    void writeGlobal(ofstream& globf, Real time, Real ie, Real ke);
    //@}

    //! \name Functions for managing WorkSets
    //! These functions are implemented for each specific element type
    //! to permit custom options to be coded for fast low-level calculations
    //@{
    void addWorkSet(Eltype type);
    int  numWorkSets() {return m_worksets.size();}
    LagrangianWorkSet* getWorkSet(Eltype type);
    //@}

    // Set CFL
    void setCFL(Real parm) {CFL = parm;}

    //! Set velocity IC's
    void setVelocityICs(Real uic, Real vic, Real wic);

  protected:

    //! \name Data registered for Lagrangian dynamics
    //! Only temperature and heat source term need to be registered
    //! for conduction.
    //@{
    DataIndex VOLN;         //!< Volume at time n
    DataIndex VOLNP1;       //!< Volume at time n+1
    DataIndex DENSITY;      //!< Density
    DataIndex PRESSURE;     //!< Pressure
    DataIndex INT_ENERGY;   //!< Internal energy
    DataIndex ART_VISC_P;   //!< Artificial viscosity for output
    DataIndex ART_VISC_M;   //!< Artificial viscosity for output
    DataIndex MATX;         //!< X-material coordinates at n+1
    DataIndex MATY;         //!< Y-material coordinates at n+1
    DataIndex MATZ;         //!< Z-material coordinates at n+1 (not used for 2D)
    DataIndex DISPLX;       //!< X-displ. coordinates at n+1
    DataIndex DISPLY;       //!< Y-displ. coordinates at n+1
    DataIndex DISPLZ;       //!< Z-displ. coordinates at n+1 (not used for 2D)
    DataIndex VELX;         //!< X-velocity at n+1/2
    DataIndex VELY;         //!< Y-velocity at n+1/2
    DataIndex VELZ;         //!< Z-velocity at n+1/2  (not used for 2D)
    DataIndex VELX_OLD;     //!< X-velocity at n-1/2
    DataIndex VELY_OLD;     //!< Y-velocity at n-1/2
    DataIndex VELZ_OLD;     //!< Z-velocity at n-1/2  (not used for 2D)
    DataIndex FX;           //!< X-force at n
    DataIndex FY;           //!< Y-force at n
    DataIndex FZ;           //!< Z-force at n    (not used for 2D)
    DataIndex FXN;          //!< X-force at n+1  (not used)
    DataIndex FYN;          //!< Y-force at n+1  (not used)
    DataIndex FZN;          //!< Z-force at n+1  (not used)
    DataIndex RIMX;         //!< X-inverse lumped mass w. EBC's imposed
    DataIndex RIMY;         //!< Y-inverse lumped mass w. EBC's imposed
    DataIndex RIMZ;         //!< Z-inverse lumped mass w. EBC's imposed
    DataIndex LUMPED_MASS;  //!< Lumped mass matrix
    DataIndex EL_MASS;      //!< Element mass

    DataIndex ELG_START;    //!< Start of Element Groups
    DataIndex ELG_END;      //!< End   of Element Groups

    DataIndex SEND_BUFFER; //!< Send buffer for nodal communicator messages
    DataIndex RECV_BUFFER; //!< Recieve buffer for nodal communicator
    //@}

    //! Cache-block data for now
    int Nelg;  //!< # of element groups

    int Nmel, e_start, e_end;
    int   n1[BLKSIZE],  n2[BLKSIZE],  n3[BLKSIZE],  n4[BLKSIZE];
    int   n5[BLKSIZE],  n6[BLKSIZE],  n7[BLKSIZE],  n8[BLKSIZE];
    int matid[BLKSIZE];

    Real  x1[BLKSIZE],  x2[BLKSIZE],  x3[BLKSIZE],  x4[BLKSIZE];
    Real  x5[BLKSIZE],  x6[BLKSIZE],  x7[BLKSIZE],  x8[BLKSIZE];
    Real x13[BLKSIZE], x24[BLKSIZE];
    Real x17[BLKSIZE], x28[BLKSIZE], x35[BLKSIZE], x46[BLKSIZE];

    Real  y1[BLKSIZE] , y2[BLKSIZE] , y3[BLKSIZE] , y4[BLKSIZE];
    Real  y5[BLKSIZE] , y6[BLKSIZE] , y7[BLKSIZE] , y8[BLKSIZE];
    Real y13[BLKSIZE], y24[BLKSIZE];
    Real y17[BLKSIZE], y28[BLKSIZE], y35[BLKSIZE], y46[BLKSIZE];

    Real  z1[BLKSIZE] , z2[BLKSIZE] , z3[BLKSIZE] , z4[BLKSIZE];
    Real  z5[BLKSIZE] , z6[BLKSIZE] , z7[BLKSIZE] , z8[BLKSIZE];
    Real z13[BLKSIZE], z24[BLKSIZE];
    Real z17[BLKSIZE], z28[BLKSIZE], z35[BLKSIZE], z46[BLKSIZE];

    Real fx1[BLKSIZE], fx2[BLKSIZE], fx3[BLKSIZE], fx4[BLKSIZE];
    Real fx5[BLKSIZE], fx6[BLKSIZE], fx7[BLKSIZE], fx8[BLKSIZE];

    Real fy1[BLKSIZE], fy2[BLKSIZE], fy3[BLKSIZE], fy4[BLKSIZE];
    Real fy5[BLKSIZE], fy6[BLKSIZE], fy7[BLKSIZE], fy8[BLKSIZE];

    Real fz1[BLKSIZE], fz2[BLKSIZE], fz3[BLKSIZE], fz4[BLKSIZE];
    Real fz5[BLKSIZE], fz6[BLKSIZE], fz7[BLKSIZE], fz8[BLKSIZE];

    Real bx1[BLKSIZE], bx2[BLKSIZE], bx3[BLKSIZE], bx4[BLKSIZE];
    Real bx5[BLKSIZE], bx6[BLKSIZE], bx7[BLKSIZE], bx8[BLKSIZE];

    Real by1[BLKSIZE], by2[BLKSIZE], by3[BLKSIZE], by4[BLKSIZE];
    Real by5[BLKSIZE], by6[BLKSIZE], by7[BLKSIZE], by8[BLKSIZE];

    Real bz1[BLKSIZE], bz2[BLKSIZE], bz3[BLKSIZE], bz4[BLKSIZE];
    Real bz5[BLKSIZE], bz6[BLKSIZE], bz7[BLKSIZE], bz8[BLKSIZE];

    Real ev[BLKSIZE], evi[BLKSIZE];

    Real hgsv1[BLKSIZE], hgsv2[BLKSIZE], hgsv3[BLKSIZE], hgsv4[BLKSIZE];

    Real q[BLKSIZE];

  private:

    //! Don't permit copy or assignment operators
    //@{
    LagrangianDynamics(const LagrangianDynamics&);
    LagrangianDynamics& operator=(const LagrangianDynamics&);
    //@}

    //! Screwed up dt's
    Real dtn;   //<! delta-t at n
    Real dtnm;  //<! delta-t at n-1
    Real dtmid; //<! for velocity update

    //! CFL
    Real CFL;

    //! Energies
    Real kin_energy;
    Real int_energy;

    //! Simple IC's
    Real vel_ic[3];

    //! Use a typedef map for fast index to worksets.
    //! worksets is the map used to lookup worksets by element type
    typedef map<int, LagrangianWorkSet*> wsMap;
    wsMap m_worksets;

    //! Map classId's to artificial viscosity
    typedef map<int, int> artViscMap;
    artViscMap m_avId;

    vector<ArtificialViscosity*> m_av;

    //! Map classId's to hourglass control
    typedef map<int, int> hourGlassMap;
    hourGlassMap m_hgId;

    vector<HourglassControl*> m_hg;

};

}
#endif // LagrangianDynamics_h
