project(FEMLagrangianDynamics CXX)

# Include configuration/headers/libraries common to all code
INCLUDE(${HYDRA_SOURCE_DIR}/HydraCommon.cmake)
INCLUDE(${HYDRA_SOURCE_DIR}/HydraDefine.cmake)

include_directories(${HYDRA_SOURCE_DIR}/Base
                    ${HYDRA_SOURCE_DIR}/Control
                    ${HYDRA_SOURCE_DIR}/DataMesh
                    ${HYDRA_SOURCE_DIR}/FEM/LagrangianDynamics
                    ${HYDRA_SOURCE_DIR}/IO
                    ${HYDRA_SOURCE_DIR}/Materials
)

INCLUDE(HydraAddLibrary)
HYDRA_ADD_LIBRARY(FEMLagrangianDynamics)

INSTALL(TARGETS FEMLagrangianDynamics
        RUNTIME DESTINATION bin COMPONENT Runtime
        LIBRARY DESTINATION lib COMPONENT Runtime
        ARCHIVE DESTINATION lib COMPONENT Development
)
