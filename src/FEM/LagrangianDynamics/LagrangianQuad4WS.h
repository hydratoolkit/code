//******************************************************************************
//! \file    src/FEM/LagrangianDynamics/LagrangianQuad4WS.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:34:50 2011
//! \brief   FEM Lagrangian dynamics 4-node quadrilateral workset
//******************************************************************************
#ifndef LagrangianQuad4WS_h
#define LagrangianQuad4WS_h

#include <LagrangianWorkSet.h>
#include <Quad4.h>
#include <Control.h>
#include <HourglassControl.h>

namespace Hydra{

class Material;


//! FEM Lagrangian Dynamics 4-node quadrilateral workset
class LagrangianQuad4WS {

  public:

    //! \name Constructor/Destructor
    //@{
             LagrangianQuad4WS() {}
    virtual ~LagrangianQuad4WS() {}
    //@}

    //! \name WorkSet Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    //! Initialize -- implemented for each specific physics
    virtual int initialize(Element* ec);

    //! Gather node number for the workset
    virtual void gatherNodes();

    //! Gather nodal coordinates
    virtual void gatherCoordinates(CVector& coord, bool delta);

    //! Gather velocities
    virtual void gatherVelocity(CVector& vel, bool delta);

    //! Calculate the artificial viscosity forces
    virtual void calcArtificialVisc(Real linear,
                                    Real quadratic,
                                    Real *e,
                                    Real *rho,
                                    Real* QP,
                                    Material* mat);

    //! Calculate hourglass forces
    virtual void calcHourGlassForce(Real ehg,
                                    Real *e,
                                    Real* rho,
                                    Real* volume,
                                    Material* mat,
                                    HGType type);

    //! Calculate the B-matrix
    virtual void calcBMatrix();

    //! Assemble the forces
    virtual void assembleForces(Real* p, CVector& F);
    //@}

  protected:

    //! Cache-block data for now
    int m_Nel;     //!< # of elements in class
    int m_Nelg;    //!< # of element groups
    int m_Nmel;    //!< Number of elements in group
    int m_e_start; //!< Start element in group
    int m_e_end;   //!< End element in group

  private:

    //! Don't permit copy or assignment operators
    //@{
    LagrangianQuad4WS(const LagrangianQuad4WS&);
    LagrangianQuad4WS& operator=(const LagrangianQuad4WS&);
    //@}

    int   n1[BLKSIZE],  n2[BLKSIZE],  n3[BLKSIZE],  n4[BLKSIZE];

    Real  x1[BLKSIZE],  x2[BLKSIZE],  x3[BLKSIZE],  x4[BLKSIZE];
    Real  y1[BLKSIZE],  y2[BLKSIZE],  y3[BLKSIZE],  y4[BLKSIZE];

    Real x13[BLKSIZE], x24[BLKSIZE];
    Real y13[BLKSIZE], y24[BLKSIZE];

    Real vx1[BLKSIZE], vx2[BLKSIZE], vx3[BLKSIZE], vx4[BLKSIZE];
    Real vy1[BLKSIZE], vy2[BLKSIZE], vy3[BLKSIZE], vy4[BLKSIZE];

    Real vx13[BLKSIZE], vx24[BLKSIZE];
    Real vy13[BLKSIZE], vy24[BLKSIZE];

    Real fx1[BLKSIZE], fx2[BLKSIZE], fx3[BLKSIZE], fx4[BLKSIZE];
    Real fy1[BLKSIZE], fy2[BLKSIZE], fy3[BLKSIZE], fy4[BLKSIZE];

    Real bx1[BLKSIZE], bx2[BLKSIZE], bx3[BLKSIZE], bx4[BLKSIZE];
    Real by1[BLKSIZE], by2[BLKSIZE], by3[BLKSIZE], by4[BLKSIZE];

    Real ev[BLKSIZE], evi[BLKSIZE];

    Real hgsv1[BLKSIZE], hgsv2[BLKSIZE], hgsv3[BLKSIZE], hgsv4[BLKSIZE];

    Real q[BLKSIZE];

    //! QUAD4 element class (pointer held, not owned)
    Quad4* q4;

    //! QUAD4 connectivity (pointer held, not owned)
    Quad4IX* el;

    //! Global Id's
    int* gid;
};

}
#endif // LagrangianQuad4WS_h
