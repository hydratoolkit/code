//******************************************************************************
//! \file    src/FEM/LagrangianDynamics/LagrangianWorkSet.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:34:50 2011
//! \brief   Base class for Lagrangian dynamics worksets
//******************************************************************************
#ifndef LagrangianWorkSet_h
#define LagrangianWorkSet_h

#include <Element.h>

namespace Hydra {

class Control;
class Material;


//! Base class for Lagrangian dynamics worksets
class LagrangianWorkSet {

  public:

    //! \name Constructor/Destructor
    //@{
             LagrangianWorkSet() {}
    virtual ~LagrangianWorkSet() {}
    //@}

    //! \name WorkSet Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    //! Initialize -- implemented for each specific physics
    virtual int initialize(Element* ec) = 0;

    //! Set the current workset group
    void setGroup(int group);

    //! Gather node number for the current group in the workset
    virtual void gatherNodes() = 0;

    //! Gather nodal coordinates
    virtual void gatherCoordinates(CVector& coord, bool delta) = 0;

    //! Gather velocities
    virtual void gatherVelocity(CVector& vel, bool delta) = 0;

    //! Calculate the artificial viscosity forces
    virtual void calcArtificialVisc(Real linear,
                                    Real quadratic,
                                    Real *e,
                                    Real *rho,
                                    Real* QP,
                                    Material* mat) = 0;

    //! Calculate hourglass forces
    virtual void calcHourGlassForce(Real ehg,
                                    Real *e,
                                    Real* rho,
                                    Real* volume,
                                    Material* mat,
                                    int type);

    //! Calculate the B-matrix
    virtual void calcBMatrix() = 0;

    //! Assemble the forces
    virtual void assembleForces(Real* p, CVector& F) = 0;
    //@}

  protected:

    //! Cache-block data for now
    int m_Nel;     //!< # of elements in class
    int m_Nelg;    //!< # of element groups
    int m_Nmel;    //!< Number of elements in group
    int m_e_start; //!< Start element in group
    int m_e_end;   //!< End element in group

  private:

    //! Don't permit copy or assignment operators
    //@{
    LagrangianWorkSet(const LagrangianWorkSet&);
    LagrangianWorkSet& operator=(const LagrangianWorkSet&);
    //@}

};

}
#endif // LagrangianWorkSet_h
