//! \file    src/NKA/nonlinear_krylov_accelerator.h
//! \brief   Non-linear Krylov Accelerator interface

#ifndef NKA_h
#define NKA_h

/* nonlinear_krylov_accelerator.h */

namespace Hydra {

#define EOL -1  /* end-of-list marker */

struct nka_state {
  int subspace;       /* boolean: a nonempty subspace */
  int pending;        /* contains pending vectors -- boolean */
  int vlen;           /* vector length */
  int mvec;           /* maximum number of subspace vectors */
  double vtol;        /* vector drop tolerance */
  /* Subspace storage */
  double **v;   /* correction vectors */
  double **w;   /* function difference vectors */
  double **h;   /* matrix of w vector inner products */
  /* Linked-list organization of the vector storage. */
  int first;  /* index of first subspace vector */
  int last;   /* index of last subspace vector */
  int free;   /* index of the initial vector in free storage linked list */
  int *next;  /* next index link field */
  int *prev;  /* previous index link field in doubly-linked subspace v */
  /* Dot product function pointer */
  double (*dp)(int, double *, double *);
};

typedef struct nka_state* NKA;

NKA nka_init (int, int, double, double (*dp)(int, double *, double *));
void nka_delete (NKA);
void nka_accel_update (NKA, double *);
void nka_cache_update (NKA, double *);
void nka_restart (NKA);
void nka_relax (NKA);
int nka_num_vec (NKA);
int nka_max_vec (NKA);
int nka_vec_len (NKA);
double nka_vec_tol (NKA);
double dot_product (int len, double *a, double *b);

}

#endif // NKA_h
