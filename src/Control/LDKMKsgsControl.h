//******************************************************************************
//! \file    src/Control/LDKMKsgsControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief    LDKM K-SGS controls
//******************************************************************************
#ifndef LDKMKsgsControl_h
#define LDKMKsgsControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! LDKM K-SGS Model Control Category
class LDKMKsgsControl : public Category {

  public:

    enum Flag {NO_FLAGS};

    enum Option {NO_OPTIONS};

    enum Param {C_NU,
                C_EPS,
                KAPPA,
                B,
                YP11,
                J1,
                J2,
                J3,
                PRANDTL,
                SCHMIDT,
                DEFAULT_NORMAL_DISTANCE
    };

    enum Category{NO_CATEGORIES};

    //! \name Constructor/Destructor
    //@{
             LDKMKsgsControl(string title);
    virtual ~LDKMKsgsControl() {}
    //@}

    void addCategory(int type, string title = "");

  private:

    //! Don't permit copy or assignment operators
    //@{
    LDKMKsgsControl(const LDKMKsgsControl&);
    LDKMKsgsControl& operator=(const LDKMKsgsControl&);
    //@}

};

}

#endif
