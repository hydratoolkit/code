//******************************************************************************
//! \file    src/Control/MeshDeformationControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   Deforming mesh controls
//******************************************************************************
#ifndef MeshDeformationControlControl_h
#define MeshDeformationControlControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! Mesh Deformation Control
class MeshDeformationControl : public Category {

  public:

    enum Flag {NO_FLAG};

    enum Option {ITMAX};

    enum Param {NO_PARAM};

    enum Category{NO_CATEGORY};

    //! \name Constructor/Destructor
    //@{
             MeshDeformationControl(string title);
    virtual ~MeshDeformationControl() {}
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    MeshDeformationControl(const MeshDeformationControl&);
    MeshDeformationControl& operator=(const MeshDeformationControl&);
    //@}
};

}

#endif
