//******************************************************************************
//! \file    src/Control/STDKEControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   Standard k-epsilon controls
//******************************************************************************
#ifndef STDKEControl_h
#define STDKEControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! Standard k-epsilon Model Control Category
class STDKEControl : public Category {

  public:

    enum Flag {NO_FLAGS};

    enum Option {YPLUS_ITMAX,  //!< Max. iterations for y+ calculation
                 WALLFN_TYPE   //!< Wall function type
    };

    enum Param {C_MU,
                C_EPS1,
                C_EPS2,
                SIGMA_K,
                SIGMA_EPS,
                PRANDTL,
                SCHMIDT,
                KAPPA,
                B,
                YP11,
                J1,
                J2,
                J3,
                ALPHA,
                EPS_LIMITER_1,
                EPS_LIMITER_2,
                DEFAULT_NORMAL_DISTANCE
    };

    enum Category{NO_CATEGORIES};

    //! \name Constructor/Destructor
    //@{
             STDKEControl(string title);
    virtual ~STDKEControl() {}
    //@}

    void addCategory(int type, string title = "");

  private:

    //! Don't permit copy or assignment operators
    //@{
    STDKEControl(const STDKEControl&);
    STDKEControl& operator=(const STDKEControl&);
    //@}

};

}

#endif
