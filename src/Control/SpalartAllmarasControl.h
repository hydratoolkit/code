//******************************************************************************
//! \file    src/Control/SpalartAllmarasControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   Spalart-Allmaras controls
//******************************************************************************
#ifndef SpalartAllmarasControl_h
#define SpalartAllmarasControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! Spalart-Allmaras Model Control Category
class SpalartAllmarasControl : public Category {

  public:

    enum Flag {DETACHED_EDDY_MODE,
               CURVATURE_CORRECTION};

    enum Option {NO_OPTIONS};

    enum Param {C_B1,
                C_B2,
                C_V1,
                C_V2,
                C_W1,
                C_W2,
                C_W3,
                SIGMA,
                C_DES,
                KAPPA,
                B,
                YP11,
                J1,
                J2,
                J3,
                PRANDTL,
                SCHMIDT,
                DEFAULT_NORMAL_DISTANCE
    };

    enum Category{NO_CATEGORIES};

    //! \name Constructor/Destructor
    //@{
             SpalartAllmarasControl(string title);
    virtual ~SpalartAllmarasControl() {}
    //@}

    void addCategory(int type, string title = "");

  private:

    //! Don't permit copy or assignment operators
    //@{
    SpalartAllmarasControl(const SpalartAllmarasControl&);
    SpalartAllmarasControl& operator=(const SpalartAllmarasControl&);
    //@}

};

}

#endif
