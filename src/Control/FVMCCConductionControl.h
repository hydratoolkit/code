//******************************************************************************
//! \file    src/Control/FVMCCConductionControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   Cell-centered FVM heat conduction controls
//******************************************************************************
#ifndef FVMCCConductionControl_h
#define FVMCCConductionControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! FVM Conduction Control Category
class FVMCCConductionControl : public Category {

  public:

    enum Flag {
      NO_FLAG //!< No flags used for this category
    };

    enum Option {
      TIME_INTEGRATOR, //!< Time integrator type
      TIMESTEP_TYPE    //!< Timestep type: fixed, adaptive, etc.
    };

    enum Param {
      THETAK          //!< Time-weight for theta method
    };

    enum Category{
      EQ_SOLVER_CAT    //!< Equation solver category
    };

    //! \name Constructor/Destructor
    //@{
             FVMCCConductionControl(string title);
    virtual ~FVMCCConductionControl() {}
    //@}

    void addCategory(int type, string title = "");

  private:

    //! Don't permit copy or assignment operators
    //@{
    FVMCCConductionControl(const FVMCCConductionControl&);
    FVMCCConductionControl& operator=(const FVMCCConductionControl&);
    //@}

};

}

#endif
