//******************************************************************************
//! \file    src/Control/LoadBalanceControl.C
//! \author  Nathan Barnett
//! \date    Dec 7, 2011
//! \brief   Load Balance controls
//******************************************************************************
#include <iostream>

using namespace std;

#include <LoadBalanceControl.h>

using namespace Hydra;

LoadBalanceControl::LoadBalanceControl(string title)
/*******************************************************************************
Routine: LoadBalanceControl - constructor
Author : Nathan Barnett, Mark A. Christon
*******************************************************************************/
{
  // Section title
  m_title = "L O A D  B A L A N C E  O P T I O N S";
  if (title.size() > 0) m_title = title;

  // Flags
  addFlag(DIAGNOSTICS, true, false, "Write load balance diagnostics");

  // Options
  addOption(STATIC_LB_METHOD, true, true, RCB,
            "Static Load Balance Method", "RCB" );

  //Parameters
  // None at this time
}

