//******************************************************************************
//! \file    src/Control/FVMCCINSControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   Controls for FVM Cell-centered incompressible Navier-Stokes
//******************************************************************************
#ifndef FVMCCINSControl_h
#define FVMCCINSControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! Hybrid FEM/FVM Navier-Stokes Control Category
class FVMCCINSControl : public Category {

  public:

    enum Flag {
      NLSOLVER_DIAG,      //!< Flag to output non-linear convergence history
      NLSOLVER_CONV,      //!< Flag to control convergence file output
      TRIM_LAST_DT,       //!< "trim" last time step, to fit "end time"
      SUBCYCLE_PRESSURE,  //!< Apply subcycle logic for solving pressure
      TIMESTEP_CONTROL,   //!< Specify time-step control for non-linear
      UPDATE_FLOW         //!< Specify energy only vs. full update
    };

    enum Option {
      DEFORMING_MESH,     //!< Toggle mesh deformation algorithm on/off
      ENERGY_EQ,          //!< Type of energy equation to solve
      SPECIES_EQ,          //!< Type of species transport equation to solve
      TURBULENCE_MODEL,   //!< Type of turbulence model to use
      GRADIENT_TYPE,      //!< Least-squares, hybrid least-squares, etc.
      INIT_PRESSURE_SOLVES, //!< No. of initial pressure solves
      PSINT,                //!< Subsycle interval for pressure solves
      TIME_INTEGRATOR,      //!< Time integrator type
      TIMESTEP_TYPE,        //!< Timestep type: fixed, fixed cfl, etc.
      IDTCHK,               //!< Interval to check time-step
      ENERGY_ITMAX,         //!< Max. iterations for energy equation
      // -- Non-linear solution solver --
      SOLUTION_STRATEGY,    //!< Projection, Picard, NewtonKrylov, etc.
      NLSOLVER_ITMAX,       //!< Max. no. of non-linear iterations
      NKA_NVEC,             //!< Number of NKA vectors
      ERROR_NORM,           //!< Error norm type
      PRECONDITIONING       //!< None, Projection, etc.
    };

    enum Param {
      EPSLAM,            //!< Tolerance for initial projection
      DIVEPS,            //!< Div tolerance
      EPS_DISTANCE,      //!< Convergence criteria: normal-distance
      EPS_P0,            //!< Convergence criteria: initial projection/pressure
      CFL_INITIAL,       //!< Initial CFL
      CFL_LIMIT,         //!< Maximum allowable CFL
      CFL_DELTATMAX,     //!< Maximum allowable time step
      CFL_DTSCALE,       //!< scale factor for time step
      THETAA,            //!< Advective time-weight
      THETAK,            //!< Diffusive time-weight
      THETAP,            //!< Pressure weight -- always 1.0
      THETAF,            //!< Force/source time-weight
      ENERGY_EPS,        //!< Convergence for temp-dependent spec. heat
      MAX_VOLUME_ERROR,  //!< Maximum volume error
      // -- Non-linear solver --
      NLSOLVER_EPS,      //!< Convergence criteria for non-linear solver
      NKA_DROP_TOL,      //!< NKA drop tolerance for Krylov subspace
      // -- Exact Termination time
      TRIM_DT_TOL        //!< Exact termination time tolerance
    };

    // Note: Keep turbulence model categories contiguous in this enum (MAC)
    enum Category{
      WALE_CAT,
      SMAGORINSKY_CAT,
      SPALART_ALLMARAS_CAT,
      RLZ_KE_CAT,
      RNG_KE_CAT,
      STD_KE_CAT,
      NL_KE_CAT,
      SST_KW_CAT,
      KSGS_CAT,
      LDKM_KSGS_CAT,
      MESH_DEFORM_CAT,
      PPE_SOLVER_CAT,
      MOMENTUM_SOLVER_CAT,
      TRANSPORT_SOLVER_CAT,
      FSI_CONTROL_CAT
    };

    //! \name Constructor/Destructor
    //@{
             FVMCCINSControl(string title);
    virtual ~FVMCCINSControl() {}
    //@}

    void addCategory(int type, string title = "");

  private:

    //! Don't permit copy or assignment operators
    //@{
    FVMCCINSControl(const FVMCCINSControl&);
    FVMCCINSControl& operator=(const FVMCCINSControl&);
    //@}

};

}

#endif
