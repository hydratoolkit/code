//******************************************************************************
//! \file    src/Control/SmagorinskyControl.h
//! \author  Jozsef Bakosi
//! \date    Wed Dec 21 17:42:00 2011
//! \brief   Smagorinsky controls
//******************************************************************************
#ifndef SmagorinskyControl_h
#define SmagorinskyControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! Smagorinsky Model Control Category
class SmagorinskyControl : public Category {

  public:

    enum Option {NO_OPTIONS};

    enum Param {C_S,
                PRANDTL,
                SCHMIDT,
                DEFAULT_NORMAL_DISTANCE
    };

    enum Category{NO_CATEGORIES};

    //! \name Constructor/Destructor
    //@{
             SmagorinskyControl(string title);
    virtual ~SmagorinskyControl() {}
    //@}

    void addCategory(int type, string title = "");

  private:

    //! Don't permit copy or assignment operators
    //@{
    SmagorinskyControl(const SmagorinskyControl&);
    SmagorinskyControl& operator=(const SmagorinskyControl&);
    //@}

};

}

#endif
