//******************************************************************************
//! \file    src/Control/FSIControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   The category holds FSI specific controls
//******************************************************************************
#ifndef FSIControl_h
#define FSIControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

class FSIControl : public Category {

  public:

    enum Flag {NO_FLAG
    };

    enum Option {NO_OPTION
    };

    enum Param {PENALTY_SCALE
    };

    enum Category{NO_CATEGORY
    };

    //! \name Constructor/Destructor
    //@{
             FSIControl(string title);
    virtual ~FSIControl() {}
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    FSIControl(const FSIControl&);
    FSIControl& operator=(const FSIControl&);
    //@}
};

}

#endif
