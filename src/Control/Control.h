//******************************************************************************
//! \file    src/Control/Control.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   The Control class holds all the relevant analysis options
//******************************************************************************
#ifndef Control_h
#define Control_h

#include <Category.h>
#include <OutputId.h>

#include <vector>

namespace Hydra {

//! The Control class holds all the relevant analysis options and parameters.
class Control : public Category {

  public:

    enum Flag {DORESTART=0,  //!< Perform restart using dump file
               PRECHECK,     //!< Perform a model precheck and load-balance
               CATALYST,     //!< Use Catalyst
               STATS,        //!< Whether to collect statistics
               STATINIT      //!< Whether collection of statistics started
    };

    enum Option {SOLTYP=0,   //!< physics selection
                 MSHTYP,     //!< mesh type (ASCII for now)
                 NSTEPS,     //!< number of timesteps or increments
                 NFIELDS,    //!< Number of fields
                 EXECNTL_INT //!< interval to check the execution control file
    };

    enum Param {DELTAT,           //!< time step size
                START_TIME,       //!< start time
                TERMINATION_TIME  //!< Termination time
    };

    //! \name Constructor/Destructor
    //@{
             Control();
    virtual ~Control();
    //@}

    //! Add a control category
    void addCategory(int type, string titlearg = "");

    string getTitle() {return title;}

    void setTitle(char* cbuf) {title = cbuf;}

    //! \name Catalyst category functions
    //! Manage the set of Catalyst scripts (i.e. pipelines) and their field
    //! dependencies. There can be more than a single script that is
    //! executed by Catalyst. Each Catalyst script has a list of node, side
    //! and cell centered variables that are needed by it.
    //@{
    void addCatalystScript(string& script, vector<string>& nodeVars,
                           vector<string>& elemVars,
                           vector<pair<int,pair<int,string> > >& sideVars);

    vector<string> getCatalystScripts() {return m_catalystScripts;}
    vector<vector<string> > getCatalystNodeVars() {return m_catalystNodeVars;}
    vector<vector<string> > getCatalystElemVars() {return m_catalystElemVars;}
    vector<vector<OutputId*> > getCatalystSideVars() {return m_catalystSideVars;}
    //@}

    //! \name Plot variable functions
    //! Manage the lists of requested plot, history, surface variables
    //@{
    void addElemHistVar(char* name, int userId, int Id, Vector& pt,
                        bool useCoord);
    void addNodeHistVar(char* name, int userId, int Id, Vector& pt,
                        bool useCoord);
    void addSurfHistVar(char* name, int userId, int Id);

    void addElemPlotInstVar(char* name) {m_elemPlotInstVar.push_back(name);}
    void addNodePlotInstVar(char* name) {m_nodePlotInstVar.push_back(name);}
    void addSurfPlotInstVar(char* name, int userId, int Id);

    void addElemPlotStatVar(char* name) {m_elemPlotStatVar.push_back(name);}
    void addNodePlotStatVar(char* name) {m_nodePlotStatVar.push_back(name);}
    void addSurfPlotStatVar(char* name, int userId, int Id);

    vector<OutputId*> getNodeHistVar() {return m_nodeHistVar;}
    vector<OutputId*> getElemHistVar() {return m_elemHistVar;}
    vector<OutputId*> getSurfHistVar() {return m_surfHistVar;}

    vector<string>    getNodePlotVar() {return m_nodePlotVar;}
    vector<string>    getElemPlotVar() {return m_elemPlotVar;}
    vector<OutputId*> getSurfPlotVar() {return m_surfPlotVar;}
    //@}

    //! \name Echo control object
    //! The data echo functions for the Control object
    //@{
    void echoControl(ostream& ofs);
    //@}

    //! \name Switches between variable vectors
    //@{
    void selectCatalystVars();  //<! Switch to the Catalyst variables list
    void selectInstVars();      //<! Switch to the instantaneous variables list
    void selectStatVars();      //<! Switch to the statistics variables list
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    Control(const Control&);
    Control& operator=(const Control&);
    //@}

    string title;  //!< Analysis title from cntl file goes here

    //! \name Requested output variables by name
    //@{
    vector<OutputId*> m_elemHistVar;     //!< Names of elem history vars
    vector<OutputId*> m_nodeHistVar;     //!< Names of node history vars
    vector<OutputId*> m_surfHistVar;     //!< Names of surf history vars

    vector<string>    m_elemPlotInstVar; //!< Names of elem plot inst vars
    vector<string>    m_nodePlotInstVar; //!< Names of node plot inst vars
    vector<OutputId*> m_surfPlotInstVar; //!< Names of surf plot inst vars

    vector<string>    m_elemPlotStatVar; //!< Names of elem stat plot vars
    vector<string>    m_nodePlotStatVar; //!< Names of node stat plot vars
    vector<OutputId*> m_surfPlotStatVar; //!< Names of surf stat plot vars

    vector<string>    m_elemPlotVar;     //!< Pointer to current elem plot vars
    vector<string>    m_nodePlotVar;     //!< Pointer to current node plot vars
    vector<OutputId*> m_surfPlotVar;     //!< Pointer to current surf plot vars

    vector<string>          m_catalystScripts;     //!< Catalyst script list
    vector<vector<string> > m_catalystNodeVars;    //!< Catalyst nodeVars list
    vector<vector<string> > m_catalystElemVars;    //!< Catalyst elemVars list
    vector<vector<OutputId*> > m_catalystSideVars; //!< Catalyst sideVars list
    //@}

};

}

#endif // Control_h
