//******************************************************************************
//! \file    src/Control/OutputControl.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   Output controls
//******************************************************************************
#include <iostream>

using namespace std;

#include <OutputControl.h>

using namespace Hydra;

OutputControl::OutputControl(string title)
/*******************************************************************************
Routine: OutputControl - constructor
Author : Mark A. Christon
*******************************************************************************/
{
  // Section title
  m_title = "O U T P U T  O P T I O N S";
  if (title.size() > 0) m_title = title;

  // Flags

  // Options
  addOption(PRTLEV, true,  true,  PARMS_ONLY, "Print level", "PARMS_ONLY");
  addOption(PRTI, true, false, 10, "Interval for printing");
  addOption(TTYI, true, false, 10, "Interval for screen output");
  addOption(THTI, true, false,  1, "Interval for instantaneous time history");
  addOption(PLTI, true, false, 20, "Interval for instantaneous plot output");
  addOption(DUMP, true, false, 0, "Interval for restart dumps");
  addOption(DUMPTYPE, true, true, BINARY_DUMP, "Dump file format", "BINARY");
  addOption(PLTYPE, true, true, EXODUSII, "Plot file format", "EXODUS");
  addOption(PLT_FTYPE, true, true, 1, "Plot file type", "SERIAL");
  addOption(PLNUM, false, false, 1, "Plot number for instantaneous output");
  addOption(PSNUM, false, false, 1, "Plot number for statistics output");
  addOption(THNUM, false, false, 1, "Plot number for time history output");

  //Parameters
  addParam(PLOT_FREQ, true, 0.0, "Time interval for instantaneous plot output");
  addParam(HIST_FREQ, true, 0.0, "Time interval for instantaneous time history");
  addParam(STAT_STARTTIME, true, 0.0, "Start time for collecting statistics");
  addParam(STAT_ENDTIME, true, 1.0, "End time for collecting statistics");
  addParam(STAT_PLOTWINSIZE, true, 0.1, "Time window size for outputing field statistics");
}

