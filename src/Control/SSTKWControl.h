//******************************************************************************
//! \file    src/Control/SSTKWControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   SST K-W controls
//******************************************************************************
#ifndef SSTKWControl_h
#define SSTKWControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! SST k-omega Model Control Category
class SSTKWControl : public Category {

  public:

    enum Flag {NO_FLAGS};

    enum Option {NO_OPTIONS};

    enum Param {A1,
                BETAS,
                KAPPA,
                YP11,
                B,
                SIGMA_K1,
                SIGMA_K2,
                SIGMA_OMEGA1,
                SIGMA_OMEGA2,
                BETA1,
                BETA2,
                GAMMA1,
                GAMMA2,
                J1,
                J2,
                J3,
                PRANDTL,
                SCHMIDT,
                DEFAULT_NORMAL_DISTANCE
    };

    enum Category{NO_CATEGORIES};

    //! \name Constructor/Destructor
    //@{
             SSTKWControl(string title);
    virtual ~SSTKWControl() {}
    //@}

    void addCategory(int type, string title = "");

  private:

    //! Don't permit copy or assignment operators
    //@{
    SSTKWControl(const SSTKWControl&);
    SSTKWControl& operator=(const SSTKWControl&);
    //@}

};

}

#endif
