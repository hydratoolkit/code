//******************************************************************************
//! \file    src/Control/RLZKEControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   Control category for Realizable k-e model
//******************************************************************************
#ifndef RLZKEControl_h
#define RLZKEControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! Realizable k-epsilon Model Control Category
class RLZKEControl : public Category {

  public:

    enum Flag {NO_FLAGS};

    enum Option {YPLUS_ITMAX,  //!< Max. iterations for y+ calculation
                 WALLFN_TYPE   //!< Wall function type
    };

    enum Param {C_MU,
                C_EPS1,
                C_EPS2T,
                C_EPS3,
                C_MU_MAX,
                C1_MAX,
                C2,
                A0,
                SIGMA_K,
                SIGMA_EPS,
                PRANDTL,
                SCHMIDT,
                KAPPA,
                B,
                YP11,
                J1,
                J2,
                J3,
                BETA,
                LAMBDA0,
                ALPHA,
                EPS_LIMITER_1,
                EPS_LIMITER_2,
                DEFAULT_NORMAL_DISTANCE
    };

    enum Category{NO_CATEGORIES};

    //! \name Constructor/Destructor
    //@{
             RLZKEControl(string title);
    virtual ~RLZKEControl() {}
    //@}

    void addCategory(int type, string title = "");

  private:

    //! Don't permit copy or assignment operators
    //@{
    RLZKEControl(const RLZKEControl&);
    RLZKEControl& operator=(const RLZKEControl&);
    //@}

};

}

#endif
