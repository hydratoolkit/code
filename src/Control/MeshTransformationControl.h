//******************************************************************************
//! \file    src/Control/MeshTransformationControl.h
//! \author  Markus Berndt, Mark A. Christon
//! \date
//! \brief   Mesh modification controls
//******************************************************************************
#ifndef MeshTransformationControl_h
#define MeshTransformationControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! Mesh Transformation Control Category
class MeshTransformationControl : public Category {

  public:

    enum Flag {DO_MESHTRANS=0
    };

    enum Option {
    };

    enum Param {SCALEX=0,
                SCALEY,
                SCALEZ,
                TRANSX,
                TRANSY,
                TRANSZ
    };

    //! \name Constructor/Destructor
    //@{
             MeshTransformationControl(string title);
    virtual ~MeshTransformationControl() {};
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    MeshTransformationControl(const MeshTransformationControl&);
    MeshTransformationControl& operator=(const MeshTransformationControl&);
    //@}

};

}

#endif
