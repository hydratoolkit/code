//******************************************************************************
//! \file    src/Control/WALEControl.h
//! \author  Jozsef Bakosi
//! \date    Mon Jan 2 11:58:00 2012
//! \brief   WALE turbulence model controls
//******************************************************************************
#ifndef WALEControl_h
#define WALEControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! Wall-Adapted LES Model Control Category
class WALEControl : public Category {

  public:

    enum Option {NO_OPTIONS};

    enum Param {C_W,
                PRANDTL,
                SCHMIDT,
                DEFAULT_NORMAL_DISTANCE
    };

    enum Category{NO_CATEGORIES};

    //! \name Constructor/Destructor
    //@{
             WALEControl(string title);
    virtual ~WALEControl() {}
    //@}

    void addCategory(int type, string title = "");

  private:

    //! Don't permit copy or assignment operators
    //@{
    WALEControl(const WALEControl&);
    WALEControl& operator=(const WALEControl&);
    //@}

};

}

#endif
