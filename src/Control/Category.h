//******************************************************************************
//! \file    src/Control/Category.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   Base class for physics-specific analysis options
//******************************************************************************
#ifndef Category_h
#define Category_h

#include <string>
#include <fstream>
#include <map>
#include <vector>

#include <HydraTypes.h>

namespace Hydra {

//! These enumerations control all the basic aspects of the code.
//@{
enum PhysicsType {
  FEM_ADVECTION_DIFFUSION=0, //!< FEM Time-dependent scalar advection
  FEM_CONDUCTION,            //!< FEM Time-dependent heat conduction
  FEM_NAVIERSTOKES,          //!< FEM Time-dependent scalar advection
  FEM_LAGRANGIAN,            //!< FEM Lagrangian hydrodynamics
  FEM_MS_LAGRANGIAN,         //!< FEM Multiscale hydrodynamics
  FEM_MULTIMAT_LAGRANGIAN,   //!< FEM Multimaterial Lagrangian hydro
  FEM_RIGID_DYNAMICS,        //!< FEM Rigid-body dynamics
  FVM_CC_ADVECTION,          //!< FVM Cell-centered advection
  FVM_CC_BURGERS,            //!< FVM Cell-centered Burgers' eq.
  FVM_CC_CONDUCTION,         //!< FVM Cell-centered heat conduction
  FVM_CC_DUMMY,              //!< FVM Cell-centered dummy physics
  FVM_CC_EULER,              //!< FVM Cell-centered Euler eq.s
  FVM_CC_FRONT,              //!< FVM Front Tracking harness
  FVM_CC_NAVIERSTOKES,       //!< FVM Cell-centered Navier-Stokes
  FVM_CC_LAGRANGIAN,         //!< FVM Cell-centered Lagrangian
  FVM_CC_LSET,               //!< FVM Cell-centered level sets
  FVM_CC_MULTIFIELD,         //!< FVM Node-centered multifield
  FVM_NC_ADVECTION,          //!< FVM Node-centered advection
  FVM_NC_EULER,              //!< FVM Node-centered Euler eq.s
  NUM_PHYSICS_TYPES          //!< Number of physics
};

//! FEM ONLY and Physics specific -- MOVE THIS ???
enum MassType {
  LUMPED=0,     //!< FEM row-sum lumped mass matrix
  CONSISTENT,   //!< FEM consistent mass matrix
  HIGH_ORDER    //!< FEM high-order mass matrix
};

//! Formulation for energy equation
enum EnergyType {
  ISOTHERMAL,      //!< Isothermal physics
  TEMPERATURE_EQ,  //!< Energy equation cast in terms of temperature
  ENTHALPY_EQ,     //!< Energy equation cast in terms of enthalpy
  INTENERGY_EQ     //!< Energy equation cast in terms of internal energy
};

//! Formulation for species transport equation
enum SpeciesType {
  NO_SPECIES_EQ,   //!< No species transport
  BINARY           //!< Binary species transport
};

//! Edge gradient types
enum EdgeGradType {
  EDGE_LS,          //!< Edge-based least-squares gradient
  EDGE_HYBRID_LS,   //!< Hybrid edge-based least-squares gradient
  EDGE_GG           //!< Edge-based Green-Gauss gradient
};

//! Mesh deformation algorithm
enum MeshDeformType {
  NO_DEFORM,       //!< No mesh deformation algorithm
  SPRING_DEFORM,   //!< Spring analogy relaxation scheme
  ELLIPTIC_DEFORM, //!< Elliptic relaxation scheme
  INTERP_DEFORM    //!< Interpolate the mesh
};

//! Input mesh file format
enum MeshType {
  ASCII_MESH=0,   //!< Hydra Toolkit ASCII mesh file format
  EXODUS_MESH,    //!< Exodus-II mesh
  NUM_MESH_TYPES  //!< Number of input mesh formats
};

//! Data echo type
enum PrintType {
  PARMS_ONLY=0,  //!< Print parameters only
  RESULTS,       //!< Print parameters + selected results
  VERBOSE        //!< Print in verbose mode
};

//! Plot file format
enum PlotType {
  GMV_ASCII=0,     //!< GMV ASCII
  GMV_BINARY,      //!< GMV Binary -- not implemented
  EXODUSII,        //!< CDF  Exodus-II file - 32bit
  EXODUSII64,      //!< CDF  Exodus-II file - 64bit
  EXODUSII_HDF5,   //!< HDF5 Exodus-II file - 32bit
  EXODUSII64_HDF5, //!< HDF5 Exodus-II file - 64bit
  VTK_ASCII,       //!< ASCII VTK File
  NUM_PLOT_TYPES   //!< Number of plot file formats
};

//! Plot file type for parallel
enum PlotFileType {
  DISTRIBUTED = 0,      //<! One file per processor
  SERIAL,               //<! Serialized by fan-in
  COLLATED,             //<! Collated via MPI-IO
  NUM_PARALLELFILETYPES //<! No. of parallel file types
};

//! Time history file type
enum HistoryType {
  ASCII_HIST=0,   //!< ASCII format
  NUM_HIST_TYPES  //!< Number of time-history file types
};

//! Restart file (dump file) type
enum DumpType {
  BINARY_DUMP=0,  //!< Binary dump file
  NUM_DUMP_TYPES  //!< Number of restart file types
};

//! Load Balancing
enum LoadBalanceType {
  RCB,              //!< Recursive coordinate bisection
  RIB,              //!< Recursive inertial bisection
  SFC,              //!< Space-filling curve
  HG,               //!< Hypergraph
  SFC_AND_HG,       //!< Space-filling curve followed by hypergraph
  ADAPTIVE          //!< Adaptive load balancing
};

//! Linear equation solver combinations
enum EqSolver {
  NATIVE_JPCG = 0,    //!< Basic native Jacobi CG
  NATIVE_SSORCG,      //!< Native SSOR CG
  NATIVE_ACONJUGATE,  //!< Native A-conjugate projection CG
  NATIVE_FGMRES,      //!< Native FGMRES
  PETSC_JPCG,         //!< PETSc Jacobi CG
  PETSC_SSORCG,       //!< PETSc SSOR CG
  PETSC_ACONJUGATE,   //!< PETSc A-Conjugate projection CG
  PETSC_AMG,          //!< PETSc AMG w. CG
  PETSC_GMRES,        //!< PETSc GMRES
  PETSC_FGMRES,       //!< PETSc FGMRES
  PETSC_ILUGMRES,     //!< PETSc ILU w. GMRES
  PETSC_ILUFGMRES     //!< PETSc ILU w. FGMRES
};

//! Option values for PETSc AMG preconditioners
enum AMGPC {
  AMG_ML=0,  //!< Sandia's ML preconditioner
  AMG_HYPRE  //!< LLNL's Hypre preconditioner
};

enum AMGCycleId {
  AMG_V=0,             //!< AMG V-cycle
  AMG_W,               //!< AMG W-cycle
  NUM_AMG_CYCLE_TYPES  //!< Number of AMG cycles
};

//! AMG cycle string
const string AMGCycle[NUM_AMG_CYCLE_TYPES] = {"V", "W"};

//! Option values for PETSc AMG/ML preconditioners
enum MLSolver {
  CG=0,   //!< CG solver
  BCGS,   //!< Bi-CG stabilized
  FGMRES  //!< FGMRES
};

//! ML smoother options
enum MLSmoother {
  AMG_CHEBYCHEV,  //!< Chebychev polynomial
  AMG_SSOR,       //!< SSOR
  AMG_ICC,        //!< Incomplet Cholesky w. zero-fill
  AMG_ILU         //!< ILU w. zero-fill
  };

//! Option values for PETSc AMG Hypre preconditioners
 enum HYPREType {
   HYPRE_BOOMERAMG=0,
   HYPRE_PARASAILS,
   HYPRE_EULCLID,
   HYPRE_PILUT,
   HYPRE_HYBRID
 };

//! Option values for Hypre parallel coarsening algorithm
//! This list should probably be extended as more Hypre coarsening algorithms
//! become available (i.e. when moving to a new PETSc version).
enum HYPRECoarsenId {
  HYPRE_CLJP=0,
  HYPRE_RUGE_STEUBEN,
  HYPRE_MODIFIED_RUGE_STEUBEN,
  HYPRE_FALGOUT,
  HYPRE_PMIS,
  HYPRE_HMIS,
  NUM_HYPRE_COARSENS
};

//! The strings here must be as defined in HYPREBoomerAMGCoarsenType[] in
//! <PETSC_SOURCE>/src/ksp/pc/impls/hypre/hypre.c, and in the same order as in
//! the enum HYPRECoarsenId.
const string HYPRECoarsen[NUM_HYPRE_COARSENS] = {
  "CLJP",
  "Ruge-Stueben",
  "modifiedRuge-Stueben",
  "Falgout",
  "PMIS",
  "HMIS"
};

//! Option values for Hypre smoother
//! This list should probably be extended as more Hypre smoothers become
//! available (i.e. when moving to a new PETSc version).
enum HYPRESmootherId {
  HYPRE_JACOBI=0,
  HYPRE_SEQ_GS,
  HYPRE_HYB_GS,
  HYPRE_BACK_HYB_GS,
  HYPRE_HYB_SGS,
  HYPRE_GE,
  NUM_HYPRE_SMOOTHERS
};

//! The strings here must be as defined in HYPREBoomerAMGRelaxType[] in
//! <PETSC_SOURCE>/src/ksp/pc/impls/hypre/hypre.c, and in the same order as in
//! the enum HYPRESmootherId.
const string HYPRESmoother[NUM_HYPRE_COARSENS] = {
  "Jacobi",
  "sequential-Gauss-Seidel",
  "SOR/Jacobi",
  "backward-SOR/Jacobi",
  "symmetric-SOR/Jacobi",
  "Gaussian-elimination"
};

//! Option values for Hypre interpolation operator
//! This list should probably be extended as more Hypre interpolation operators
//! become available (i.e. when moving to a new PETSc version).
enum HYPREInterpId {
  HYPRE_CLASSICAL=0,
  HYPRE_DIRECT,
  HYPRE_MULTIPASS,
  HYPRE_MULTIPASS_WTS,
  HYPRE_EXT_I,
  HYPRE_EXT_I_CC,
  HYPRE_STANDARD,
  HYPRE_STANDARD_WTS,
  HYPRE_FF,
  HYPRE_FF1,
  NUM_HYPRE_INTERPS
};

//! The strings here must be as defined in HYPREBoomerAMGInterpType[] in
//! <PETSC_SOURCE>/src/ksp/pc/impls/hypre/hypre.c, and in the same order as in
//! the enum HYPREInterpId.
const string HYPREInterp[NUM_HYPRE_INTERPS] = {
  "classical",
  "direct",
  "multipass",
  "multipass-wts",
  "ext+i",
  "ext+i-cc",
  "standard",
  "standard-wts",
  "FF",
  "FF1"
};

//! Turbulence model types
enum TurbulenceModel {
  NO_TURBMODEL,
  WALE,
  SMAGORINSKY,
  VARMULTISCALE,
  KSGS,
  LDKM_KSGS,
  SPALART_ALLMARAS,
  KEPSZETAF,
  RLZ_KE,
  RNG_KE,
  STD_KE,
  NL_KE,
  SST_KW
};

//! Wall Treatment
enum WallTreatment {
  NO_WALLFUNCTION,
  COMPOUND,
  SCALABLE,
  TWOLAYER
};

//! Control categories - controls the order of output!!
enum ControlCategory{
  LOAD_BALANCE_CATEGORY=0,
  MESHTRANS_CATEGORY,
  ANALYSIS_CATEGORY,
  OUTPUT_CATEGORY,
  DEFORMING_MESH_CATEGORY,
  FEM_ADVECTION_DIFFUSION_CATEGORY,
  FEM_CONDUCTION_CATEGORY,
  FEM_NAVIERSTOKES_CATEGORY,
  FEM_LAGRANGIAN_CATEGORY,
  FEM_RIGIDBODYDYNAMICS_CATEGORY,
  FSI_CONTROL_CATEGORY,
  FVM_CC_CONDUCTION_CATEGORY,
  FVM_CC_NAVIERSTOKES_CATEGORY,
  FVM_CC_LAGRANGIAN_CATEGORY,
  LINEAR_ALGEBRA_CATEGORY,
  INCREMENTCONTROL_CATEGORY,
  ITERATIVESOLVER_CATEGORY,
  NUM_CONTROL_CATEGORIES
};

struct ControlFlag {
  bool echo;    //<! Echo on or off
  bool value;   //<! Value for the flag
  string name;  //<! String to be printed on data echo
};

struct ControlOption {
  bool echo;    //<! Echo on or off
  bool tag;     //<! True to write a string label, false for numeric value
  int  value;   //<! Value for the option
  string label; //<! Label name
  string name;  //<! String to be printed on data echo
};

struct ControlParam {
  bool echo;   //<! Echo on or off
  Real value;  //<! Value for the parameter
  string name; //<! String to be printed on data echo
};

//! Control Category Base Class
class Category {

  public:

    //! \name Constructor/Destructor
    //@{
             Category();
    virtual ~Category();
    //@}

    void echoOptions(ostream& ofs);

    void echoCategories(ostream& ofs);

    virtual void addFlag(int key, bool echo, bool value, string name);

    virtual bool getFlagVal(int key) {return m_flags[key]->value;}

    virtual void setFlagEcho(int key, bool eflag);

    virtual void setFlag(int key, bool value);

    virtual void addOption(int key, bool echo, bool tag, int value,
                           string name, string label = "");

    virtual string getOptionLabel(int key) {return m_options[key]->label;}

    virtual int getOptionVal(int key) {return m_options[key]->value;}

    virtual void setOptionEcho(int key, bool eflag);

    virtual void setOptionVal(int key, int value);

    virtual void setOptionVal(int key, int value, string label);

    virtual void addParam(int key, bool echo, Real value, string name);

    virtual void setParamEcho(int key, bool eflag);

    virtual Real getParamVal(int key) {return m_params[key]->value;}

    virtual void setParamVal(int key, Real value);

    ControlOption* getOption(int key) {return m_options[key];}

    ControlParam* getParam(int key) {return m_params[key];}

    string& getTitle() {return m_title;}

    //**************************************************************************
    //! \name  Manipulate categories
    //@{
    virtual void addCategory(int type, string title = "");

    virtual void delCategory(int type);

    virtual Category* getCategory(int type) {return m_catlist[type];}

    const Category& getCategoryRef(int type) {return *m_catlist[type];}

    int  numCategories() {return m_catlist.size();}
    //@}

  protected:

    string m_title;

    typedef map<int, ControlFlag*> FlagMap;
    FlagMap m_flags;

    typedef map<int, ControlOption*> OptionMap;
    OptionMap m_options;

    typedef map<int, ControlParam*> ParamMap;
    ParamMap m_params;

    typedef map<int, Category*> catMap;
    catMap m_catlist;

  private:

    //! Don't permit copy or assignment operators
    //@{
    Category(const Category&);
    Category& operator=(const Category&);
    //@}

};

}

#endif
