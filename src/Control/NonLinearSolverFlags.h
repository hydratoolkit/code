//******************************************************************************
//! \file    src/Control/NonLinearSolverFlags.h
//! \author  Robert N. Nourgaliev
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Options for non-linear solvers
//******************************************************************************
#ifndef NonLinearSolverFlags_h
#define NonLinearSolverFlags_h

namespace Hydra {

//! \brief NLSVRS provides nonlinear solver options
namespace NLSVRS {

//! Flags for preconditioning strategies
enum PrecondStrategy {
  NO_PRECOND=0,     // No preconditioning
  PHYSICS_BASED,    // Physics-based preconditioning
  PETSC_BASED_EXP,  // Expensive PETSs-based Jacobian evaluation
  PETSC_BASED_CLR   // Coloring PETSs-based Jacobian evaluation
};

//! Flags for SNES types
enum SNESTypes {
  SNES_TRUSTREGION=0, // Trust region method
  SNES_LSCUBIC,       // Line search with cubic backtracking
  SNES_DAMPLINE,      // Basic damping line search
  SNES_SECANTLINE,    // Secant line search
  SNES_CPSECANTLINE   // Critical point secant line search
};

//! Flags for Matrix-Free method
enum MFreeTypes {
  DENIS_SCHNABEL=0,  //!< Denis & Schnabel
  PERNICE_WALKER // Method of Pernice and Walker
};

//! Orthogonalization options
enum Orthogonalization {
  CLASSICAL_GRAM_SCHM=0, //!< Unmodified (classical) Gram-Schmidt method
  MODIFIED_GRAM_SCHM     //!< Modified Gram-Schmidt method
};

//! GS orthogonalization refinement options
 enum Refinement {
   NO_REFINEMENT=0, //!< No refinement
   IF_NEEDED,       //!< If needed
   ALWAYS           //!< Always
 };

} // end of NLSVRS namespace

} // end of Hydra namespace

#endif // NonLinearSolverFlags_h
