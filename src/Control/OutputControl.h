//******************************************************************************
//! \file    src/Control/OutputControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   Output controls
//******************************************************************************
#ifndef OutputControl_h
#define OutputControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! Output Control Category
class OutputControl : public Category {

  public:

    enum Flag {NONE
    };

    enum Option {PRTLEV = 0, //!< 0 - parm's only, 1 - results, 2 - verbose
                 TTYI,       //!< Interval for screen reports of min./max.
                 PRTI,       //!< print interval
                 THTI,       //!< time-history interval for instantaneous data
                 PLTI,       //!< plot interval for instantaneous data
                 DUMP,       //!< restart dump interval
                 DUMPTYPE,   //!< dump file type
                 PLTYPE,     //!< plot file type (EXODUS-II by default)
                 PLT_FTYPE,  //!< plot file type in parallel
                 PLNUM,      //!< plot # for instantaneous plot file
                 PSNUM,      //!< plot # for statistics plot file
                 THNUM       //!< history # of time history file
    };

    enum Param {PLOT_FREQ=0,       //!< Plot frequency (time)
                HIST_FREQ,         //!< Time-history frequency (time)
                STAT_STARTTIME,    //!< Start time for statistics
                STAT_ENDTIME,      //!< End time for statistics
                STAT_PLOTWINSIZE   //!< Statistics window size
    };

    //! \name Constructor/Destructor
    //@{
             OutputControl(string title);
    virtual ~OutputControl() {};
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    OutputControl(const OutputControl&);
    OutputControl& operator=(const OutputControl&);
    //@}

};

}

#endif
