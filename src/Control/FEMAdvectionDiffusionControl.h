//******************************************************************************
//! \file    src/Control/FEMAdvectionDiffusionControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   FEM Advection-Diffusion controls
//******************************************************************************
#ifndef FEMAdvectionDiffusionControl_h
#define FEMAdvectionDiffusionControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! FEM Advection-Diffusion Control Category
class FEMAdvectionDiffusionControl : public Category {

  public:

    enum Option {NO_OPTIONS
    };

    enum Param {NO_PARAMS
    };

    enum Category{NODE_SOLVER_CAT
    };

    //! \name Constructor/Destructor
    //@{
             FEMAdvectionDiffusionControl(string title);
    virtual ~FEMAdvectionDiffusionControl() {}
    //@}

    void addCategory(int type, string title = "");

  private:

    //! Don't permit copy or assignment operators
    //@{
    FEMAdvectionDiffusionControl(const FEMAdvectionDiffusionControl&);
    FEMAdvectionDiffusionControl& operator=(const FEMAdvectionDiffusionControl&);
    //@}

};

}

#endif
