//******************************************************************************
//! \file    src/Control/FVMCCLagrangianControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   Controls for FVM Cell-centered Lagrangian physics
//******************************************************************************
#ifndef FVMCCLagrangianControl_h
#define FVMCCLagrangianControl_h

/*! \file FVMCCLagrangianControl.h
    \brief
 */

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! Order or RK time integrator
enum FVMCCLTimeInt {
  FVMCCL_RK1=1,
  FVMCCL_RK2,
  FVMCCL_RK3
};

//! Riemann solver type
enum FVMCCLRiemannSolver {
  FVMCCL_ACOUSTIC,
  FVMCCL_VERTEX_LLF
};

//! Spatial order, i.e., reconstruction
enum FVMCCLSpatialOrder {
  FVMCCL_FIRST_ORDER=1,
  FVMCCL_SECOND_ORDER
};

//! Node solver type -- either Maire's or Burton's
enum FVMCCLNodeSolver {
  FVMCCL_MAIRE_METHOD,
  FVMCCL_BURTON_METHOD,
  FVMCCL_MAC_METHOD
};

//! Gradient methods
enum FVMCCLGradientMethod {
  FVMCCL_LEAST_SQUARES,
  FVMCCL_MIMETIC
};


//! Limiter Options
 enum FVMCCLLimterOptions {
   FVMCCL_VENKATAKRISHNAN,
   FVMCCL_BARTH_JESPERSON,
   FVMCCL_UNLIMITED
};

//! Velocity Limiter methods
enum FVMCCLLimterMethod {
  FVMCCL_VELOCITY_MAGNITUDE,
  FVMCCL_VELOCITY_PROJECTION,
  FVMCCL_NORMAL_LIMITER
};

//! FVM Cell-Centered Lagrangian Hydro Control Category
class FVMCCLagrangianControl : public Category {

  public:

    enum Flag {
      NO_FLAG
    };

    enum Option {
      TIME_INTEGRATOR,     //!< Time integrator type
      GRADIENT_METHOD,     //!< Gradient method, e.g., least-squares
      NODE_RIEMANN_SOLVER, //!< Nodal Riemann solver
      MAX_RIEMANN_ITER,    //!< Maximum number of Riemann iterations
      SPATIAL_ORDER,       //!< Spatial order
      NODE_SOLVER,         //!< Node solver type (Maire or Burton)
      LIMITER_OPTION,      //!< Limit gradients based on the choices
      VEL_LIMITER_OPTION   //!< Velocity limiter
    };

    enum Param {
      CFL_INITIAL,       //!< Initial CFL
      CFL_LIMIT,         //!< Maximum allowable CFL
      CFL_DTSCALE,       //!< scale factor for time step
      MAX_VOLUME_CHANGE, //!< Maximum allowable volume/area change
      DT_INITIAL         //!< Initial time step
    };

    //! \name Constructor/Destructor
    //@{
             FVMCCLagrangianControl(string title);
    virtual ~FVMCCLagrangianControl() {}
    //@}

    void addCategory(int type, string title = "");

  private:

    //! Don't permit copy or assignment operators
    //@{
    FVMCCLagrangianControl(const FVMCCLagrangianControl&);
    FVMCCLagrangianControl& operator=(const FVMCCLagrangianControl&);
    //@}

};

}

#endif
