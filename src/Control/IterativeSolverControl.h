//******************************************************************************
//! \file    src/Control/IterativeSolverControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   Iterative solver controls
//******************************************************************************
#ifndef IterativeSolverControl_h
#define IterativeSolverControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! Iterative Solver Control Category
class IterativeSolverControl : public Category {

  public:

    enum Flag {CONVERGENCE,
               DIAGNOSTICS,
               HYPRE_NODAL
    };

    enum Option {TYPE,
                 ITMAX,
                 ITCHK,
                 NUM_RESTART,
                 MAX_PHI,
                 KSP_METHOD,
                 NUM_LEVELS,
                 MAX_COARSE_SIZE,
                 AMG_CYCLE,
                 AMG_SMOOTH_UP,
                 AMG_SMOOTH_DOWN,
                 AMG_SMOOTHER,
                 AMG_PC,
                 HYPRE_TYPE,
                 HYPRE_COARSEN_TYPE,
                 HYPRE_SMOOTHER,
                 HYPRE_SMOOTHER_UP,
                 HYPRE_SMOOTHER_DN,
                 HYPRE_SMOOTHER_CO,
                 PMAX_ELEMENTS,
                 AGG_NUM_LEVELS,
                 AGG_NUM_PATHS,
                 GS_ORTHOGONALIZATION,
                 CGS_REFINE,
                 INTERP_TYPE
    };

    enum Param {EPS,
                ANORM_TOL,
                ZERO_PIVOT,
                TRUNC_FACTOR,
                STRONG_THRESHOLD,
                MAX_ROWSUM
    };

    //! \name Constructor/Destructor
    //@{
             IterativeSolverControl(string title);
    virtual ~IterativeSolverControl() {}
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    IterativeSolverControl(const IterativeSolverControl&);
    IterativeSolverControl& operator=(const IterativeSolverControl&);
    //@}

};

}

#endif
