//******************************************************************************
//! \file    src/Control/LoadBalanceControl.h
//! \author  Nathan Barnett
//! \date    Dec 7, 2011
//! \brief   Load Balance Controls
//******************************************************************************
#ifndef LoadBalanceControl_h
#define LoadBalanceControl_h

#include <string>
#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! Load Balance Control Category
class LoadBalanceControl : public Category {

  public:

    enum Flag {
      DIAGNOSTICS       //!< Turn on/off diagnostics
    };

    enum Option {
      STATIC_LB_METHOD  //!< Type of static load balancing to use
    };

    //! \name Constructor/Destructor
    //@{
             LoadBalanceControl(string title);
    virtual ~LoadBalanceControl() {};
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    LoadBalanceControl(const LoadBalanceControl&);
    LoadBalanceControl& operator=(const LoadBalanceControl&);
    //@}

};

}

#endif

