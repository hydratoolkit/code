//******************************************************************************
//! \file    src/Control/FEMINSControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   FEM heat conduction controls
//******************************************************************************
#ifndef FEMINSControl_h
#define FEMINSControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! FEM Incompressible Navier-Stokes Control Category
class FEMINSControl : public Category {

  public:

    enum Option {MASS_MATRIX
    };

    enum Param {NONE

    };

    enum Category{NODE_SOLVER_CAT,
                  PPE_SOLVER_CAT
    };

    //@{
    //! \name Constructor/Destructor
             FEMINSControl(string title);
    virtual ~FEMINSControl() {}
    //@}

    void addCategory(int type, string title = "");

  private:

    //! Don't permit copy or assignment operators
    //@{
    FEMINSControl(const FEMINSControl&);
    FEMINSControl& operator=(const FEMINSControl&);
    //@}

};

}

#endif
