//******************************************************************************
//! \file    src/Control/FEMConductionControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   FEM heat conduction controls
//******************************************************************************
#ifndef FEMConductionControl_h
#define FEMConductionControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! FEM Conduction Control Category
class FEMConductionControl : public Category {

  public:

    enum Flag {
      NO_FLAG //!< No flags used for this category
    };

    enum Option {
      SOLUTION_METHOD, //!< Steady or transient (default STEADY_STATE)
      TIME_INTEGRATOR, //!< Time integrator type
      TIMESTEP_TYPE,   //!< Timestep type: fixed, adaptive, etc.
      MASS_MATRIX,     //!< Mass matrix approximation, e.g., lumped
      PHASE_CHANGE     //!< Perform phase change
    };

    enum Param {
      THETAK           //!< Time-weight for theta method
    };

    enum Category{
      NODE_SOLVER_CAT
    };


    //! \name Constructor/Destructor
    //@{
             FEMConductionControl(string title);
    virtual ~FEMConductionControl() {}
    //@}

    void addCategory(int type, string title = "");

  private:

    //! Don't permit copy or assignment operators
    //@{
    FEMConductionControl(const FEMConductionControl&);
    FEMConductionControl& operator=(const FEMConductionControl&);
    //@}

};

}

#endif
