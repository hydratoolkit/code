//******************************************************************************
//! \file    src/Control/NLKEControl.h
//! \author  Ben Magolan, Mark A. Christon
//! \date    July, 2014
//! \brief   Nonlinear k-epsilon controls
//******************************************************************************
#ifndef NLKEControl_h
#define NLKEControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! Nonlinear k-epsilon Model Control Category
class NLKEControl : public Category {

  public:

    enum Flag {QUADRATIC_KE,   //!< Quadratic Reynolds Stress model (Default)
               CUBIC_KE,       //!< Cubic Reynolds Stress model
               DEBUG_STD_KE    //!< Debug mode which enables STD_KE model
    };

    enum Option {YPLUS_ITMAX,  //!< Max. iterations for y+ calculation
                 WALLFN_TYPE   //!< Wall function type
    };

    enum Param {C_MU,
                C_EPS1,
                C_EPS2,
                C_MU_MAX,
                SIGMA_K,
                SIGMA_EPS,
                KAPPA,
                B,
                YP11,
                J1,
                J2,
                J3,
                ALPHA,
                EPS_LIMITER_1,
                EPS_LIMITER_2,
                PRANDTL,
                SCHMIDT,
                DEFAULT_NORMAL_DISTANCE,
                CNL1,
                CNL2,
                CNL3,
                CNL6,
                CNL7,
                A0,
                A1,
                A2,
                A3
    };

    enum Category{NO_CATEGORIES};

    //! \name Constructor/Destructor
    //@{
             NLKEControl(string title);	
    virtual ~NLKEControl() {}
    //@}

    void addCategory(int type, string title = "");

  private:

    //! Don't permit copy or assignment operators
    //@{
    NLKEControl(const NLKEControl&);
    NLKEControl& operator=(const NLKEControl&);
    //@}

};

}

#endif
