//******************************************************************************
//! \file    src/Control/RNGKEControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   Control category for RNG k-e model
//******************************************************************************
#ifndef RNGKEControl_h
#define RNGKEControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! RNG k-epsilon Model Control Categor
class RNGKEControl : public Category {

  public:

    enum Flag {NO_FLAGS};

    enum Option {YPLUS_ITMAX,  //!< Max. iterations for y+ calculation
                 WALLFN_TYPE   //!< Wall function type
    };

    enum Param {C_MU,
                C_EPS1,
                C_EPS2T,
                SIGMA_K,
                SIGMA_EPS,
                BETA,
                PRANDTL,
                SCHMIDT,
                KAPPA,
                B,
                YP11,
                J1,
                J2,
                J3,
                LAMBDA0,
                ALPHA,
                EPS_LIMITER_1,
                EPS_LIMITER_2,
                DEFAULT_NORMAL_DISTANCE
    };

    enum Category{NO_CATEGORIES};

    //! \name Constructor/Destructor
    //@{
             RNGKEControl(string title);
    virtual ~RNGKEControl() {}
    //@}

    void addCategory(int type, string title = "");

  private:

    //! Don't permit copy or assignment operators
    //@{
    RNGKEControl(const RNGKEControl&);
    RNGKEControl& operator=(const RNGKEControl&);
    //@}

};

}

#endif
