//******************************************************************************
//! \file    src/Control/IncrementControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:59:42 2011
//! \brief   Solution increment control
//******************************************************************************
#ifndef IncrementControl_h
#define IncrementControl_h

#include <string>

#include <HydraTypes.h>
#include <Category.h>

namespace Hydra {

//! Solution Increment Control Category
class IncrementControl : public Category {

  public:

    enum Flag {NO_FLAGS
    };

    enum Option {NO_OPTIONS
    };

    enum Param {NO_PARAM
    };

    //! \name Constructor/Destructor
    //@{
             IncrementControl(string title);
    virtual ~IncrementControl() {}
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    IncrementControl(const IncrementControl&);
    IncrementControl& operator=(const IncrementControl&);
    //@}

};

}

#endif
