//******************************************************************************
//! \file    src/Multiphysics/PhysicsManager.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:28 2011
//! \brief   Physics manager for multiphysics problems
//******************************************************************************
#ifndef PhysicsManager_h

#include <map>

#include <Control.h>
#include <UnsMesh.h>
#include <Physics.h>

namespace Hydra {

struct physicsPackage {
  //! \name  Physics  package
  //! This structure holds a mesh, control and physics object for analysis.
  //@{
  int      m_type;     //!< Physics/solution type
  Control* m_control;  //!< Control
  UnsMesh* m_mesh;     //!< Mesh
  Physics* m_physics;  //!< Physics
  //@}
};


class PhysicsManager {

  public:
    //! \name Constructor/Destructor
    //@{
             PhysicsManager() {}
    virtual ~PhysicsManager() {}
    //@}

    //! \name Physics Manager functions
    //! These functions used to manage Physics, Control, Mesh objects
    //! implemented for a specific physics problem.
    //@{
    void addPhysics(int type, UnsMesh* mesh, Control* control);

    int numPhysics() {return m_physics.size();}

    physicsPackage* getPhysicsPackage() {return m_physics[m_activeId];}
    physicsPackage* getPhysicsPackage(int id) {return m_physics[id];}

    Physics* getPhysics() {return m_physics[m_activeId]->m_physics;}
    Physics* getPhysics(int Id) {return m_physics[Id]->m_physics;}

    int getType() {return m_physics[m_activeId]->m_type;}
    int getType(int Id) {return m_physics[Id]->m_type;}

    void finalize();
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    PhysicsManager(const PhysicsManager&);
    PhysicsManager& operator=(const PhysicsManager&);
    //@}

    //! Use a typedef map for fast index to the physicsMap.
    //! m_physics is the real map used to lookup physics by id
    typedef map<int, physicsPackage*> physicsMap;
    physicsMap m_physics;

    //! Currently active physics -- typically only one for now
    int m_activeId;

};

}

#endif // PhysicsManager_h
