//******************************************************************************
//! \file    src/InterfaceReconstruction/InterfaceReconstruction.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Virtual base class for interface reconstruction for volume-tracking
//******************************************************************************
#ifndef InterfaceReconstruction_h
#define InterfaceReconstruction_h

#include <HydraTypes.h>
#include <Interface.h>

namespace Hydra {

//! Interface reconstruction object
class InterfaceReconstruction : public Interface {
  public:
    /*! \name Constructor/Destructor
     */
    //@{
             InterfaceReconstruction() {}
    virtual ~InterfaceReconstruction() {}
    //@}


    /*! \name InterfaceReconstruction access functions
        The InterfaceReconstruction class is used to setup physics-specific ...
     */
    //@{
    void finalize();
    void initialize();
    //@}

  private:
    //! Don't permit copy or assignment operators
    //@{
    InterfaceReconstruction(const InterfaceReconstruction&);
    InterfaceReconstruction& operator=(const InterfaceReconstruction&);
    //@}

};

}

#endif // Interface_h
