#
# Usage:
#
# INCLUDE(PrintVariable)
# :
# :
# set(foo to some list)
# PRINT_VARIABLE(foo)
#
# Running cmake will print out
#
# => foo=to;some;list
#
MACRO(PRINT_VARIABLE var)
  MESSAGE(STATUS "==> ${var}=${${var}}")
ENDMACRO(PRINT_VARIABLE var)
