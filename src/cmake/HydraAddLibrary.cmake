#
# Usage:
#
# HYDRA_ADD_LIBRARY(<lib> [STATIC|SHARED|MODULE] [EXCLUDE_FROM_ALL])
#
# 
INCLUDE(MessageHandler)

MACRO(HYDRA_ADD_LIBRARY lib_name)

  INCLUDE(${CMAKE_CURRENT_SOURCE_DIR}/FileLists.cmake)

  
  IF(NOT DEFINED ${lib_name}_SOURCE_FILES)
    FATAL_MESSAGE("Can not add ${lib_name} to the build. No source files found.")
  ENDIF(NOT DEFINED ${lib_name}_SOURCE_FILES)

  ADD_LIBRARY(${lib_name} ${ARGN} ${${lib_name}_SOURCE_FILES})

ENDMACRO(HYDRA_ADD_LIBRARY lib_name)
