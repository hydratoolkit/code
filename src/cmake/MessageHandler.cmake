#
# 
# MessageHandler.cmake
#
#  Collection of macros for printing out useful error, warning and fatal messages
#
#
#
#
MACRO(ERROR_MESSAGE mess)
  message(SEND_ERROR "HYDRA CMAKE ERROR: ${mess}")
ENDMACRO(ERROR_MESSAGE mess)

MACRO(WARN_MESSAGE mess)
  message(WARNING "HYDRA CMAKE WARNING: ${mess}") 
ENDMACRO(WARN_MESSAGE mess)

MACRO(FATAL_MESSAGE mess)
  message(FATAL_ERROR "HYDRA CMAKE FATAL ERROR: ${mess}")
ENDMACRO(FATAL_MESSAGE mess)
