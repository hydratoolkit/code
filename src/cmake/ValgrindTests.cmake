add_hydra_test("FEM_Advection-Diffusion_Test-1_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FEM/AdvectionDiffusion/2D/basic" 1 ON)
add_hydra_test("FEM_Conduction_2D_box_w._heat_flux_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FEM/Conduction/2D/box" 1 ON)
add_hydra_test("FEM_Conduction_3D_8-block_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FEM/Conduction/3D/8block" 1 ON)
add_hydra_test("FEM_Navier-Stokes_2D_vortex_shedding_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FEM/IncNavierStokes/2D/vortex_shedding" 1 ON)
add_hydra_test("FEM_Rigid_Body_Dynamics_3D_cone_test_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FEM/RigidBodyDynamics/3D/cone" 1 ON)
add_hydra_test("FVM_Euler_2D_Emery_problem_-_normal_BCs_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FVM/CCEuler/2D/emery" 1 ON)
add_hydra_test("FVM_Advection_tri-mesh1_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FVM/CCAdvection/2D/basic" 1 ON)
add_hydra_test("FVM_INS_2D_Coarse_BFS_k-e_model_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/bfs" 1 ON)
add_hydra_test("FVM_INS_2D_body-force_test-a_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/body_force" 1 ON)
add_hydra_test("FVM_INS_2D_heat_source_test_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/heat_source" 1 ON)
add_hydra_test("FVM_INS_2D_Re=10000_Spalart-Allmaras_coarse_mesh_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding" 1 ON)
add_hydra_test("FVM_INS_2D_Cylinder_flow_Spalart-Allmaras_DES_model:_internal_energy_formulation_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding/Int.Energy" 1 ON)
add_hydra_test("FVM_INS_2D_Cylinder_flow_Smagorinksy_model_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding" 1 ON)
add_hydra_test("FVM_INS_2D_Cylinder_flow_WALE_model:_internal_energy_formulation_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding/Int.Energy" 1 ON)
add_hydra_test("FVM_INS_2D_Cylinder_flow:_inst_and_stat_output_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding" 1 ON)
add_hydra_test("FVM_INS_2D_Euler_time-dependent_velocity_BC_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/euler_vbc" 1 ON)
add_hydra_test("FVM_MultiField_Startup_Machinery_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FVM/CCMultiPhase/2D/vortex_shedding" 1 ON)
add_hydra_test("FVM_CCLagrangian_Maire_Sod_test_a_4P" "Valgrind Tests"
  "${HYDRA_REGRESSION_DIR}/FVM/CCLagrangian/2D/sod" 1 ON)
