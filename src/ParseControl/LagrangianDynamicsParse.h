//******************************************************************************
//! \file    src/ParseControl/LagrangianDynamicsParse.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   FEM Lagrangian dynamics specific parsing
//******************************************************************************
#ifndef LagrangianDynamicsParse_h
#define LagrangianDynamicsParse_h

namespace Hydra {

// Lagrangian Dynamics Analysis keywords
typedef bool
(*Dynamics_Keyword_Function)(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

typedef struct {
  const char* name;
  Dynamics_Keyword_Function func;
} Dynamics_Keyword;

// Lagrangian Dynamics
bool parseDynamics(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);

// Lagrangian Dynamics Keywords
bool parseHourGlass(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseArtificialViscosity(TokenStream* ts, UnsMesh* mesh, Control *control,
                              PhysicsManager&);

}

#endif // LagrangianDynamicsParse_h
