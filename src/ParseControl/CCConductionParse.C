//******************************************************************************
//! \file    src/ParseControl/CCConductionParse.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   Cell-centered heat conduction
//******************************************************************************

#include <string>
#include <vector>

using namespace std;

#include <HydraTypes.h>
#include <Exception.h>
#include <fileIO.h>
#include <TokenStream.h>
#include <PhysicsManager.h>
#include <ParseControl.h>
#include <ParseMaterial.h>
#include <ParseUtil.h>

#include <Category.h>
#include <FVMCCConductionControl.h>
#include <IterativeSolverControl.h>

#include <CCConduction.h>
#include <CCConductionParse.h>

namespace Hydra {

// Setup the CCEuler-specific list of keywords
#define KEYWORD(name,func) {name,func},
CCConduction_Keyword ccconduction_list[] = {
#include <AnalysisKeywords.def>
#include <CCConductionKeywords.def>
};
#undef KEYWORD

bool parseCCConduction(TokenStream* ts, fileIO* /*io*/, UnsMesh* mesh,
                       Control *control, PhysicsManager& physman)
/*******************************************************************************
Routine: parseCCConduction - parse the cell-centered advection physics options
Author : Mark A. Christon
*******************************************************************************/
{
  bool  isread = false;
  char* token;

  // Create the physics object
  physman.addPhysics(FVM_CC_CONDUCTION, mesh, control);

  // Set default controls
  setCCConductionDefaults(control);

  // Analysis keywords
  typedef map<const char*,
              bool(*)(TokenStream*, UnsMesh*, Control*, PhysicsManager&),
              parseComparitor > AnalysisParseFunctions;
  AnalysisParseFunctions analysis;

  // Add the physics-specific keyword-function pairs
  int Nkw = sizeof(ccconduction_list)/sizeof(CCConduction_Keyword);
  for (int i=0; i<Nkw; i++) {
    analysis.insert(
            AnalysisParseFunctions::value_type(ccconduction_list[i].name,
                                               ccconduction_list[i].func));
  }

  // Parse the remainder of the cc_conduction -- end block
  while (!isread) {
    token = ts->getToken();
    // Parse non-comment lines
    if (analysis.find(token) != analysis.end()) {
      isread = analysis[token](ts, mesh, control, physman);
    } else {
      ts->handleError();
      throw Exception("Problem parsing cc_conduction -- end");
    }
  } // end of the while

  return false;
}

bool
parseCCConductionICs(TokenStream* ts, UnsMesh* /*mesh*/, Control* /*control*/,
                     PhysicsManager& physman)
/*******************************************************************************
Routine: parseCCConductionICs - parse the initial -- end block
Author : Mark A. Christon

initial
  temp   val
end

Aliases: init

*******************************************************************************/
{
  char* token;
  CCConductionICPackage ics;

  // Parse the initial -- end block
  bool  isread = false;
  while (!isread) {
    token = ts->getToken();
    if (strncasecmp("temp", token, 4) == 0 ) {
      token = ts->getToken();
      getTokenParam(token, ics.temp);
    } else if (strncasecmp("temperature", token, 11) == 0 ) {
      token = ts->getToken();
      getTokenParam(token, ics.temp);
    }
    else if (strncasecmp("end", token, 3) == 0 ) {
      isread = true;
    } else {
      ts->handleError();
      throw Exception("Problem parsing initial -- end");
    }
  } // end of the while

  // IC's parsed now, store off in physics
  static_cast<CCConduction*>(physman.getPhysics())->setICs(ics);

  return false;
}

bool
parseCCCTSBC(TokenStream* ts, UnsMesh* mesh, Control* /*control*/,
             PhysicsManager& physman)
/*******************************************************************************
Routine: parseCCCTBC -- Parse temperature BC's
Author : Mark A. Christon

temperature
  sideset setid amp val
end

Aliases: tbc, temperaturebc

*******************************************************************************/
{
  char* token;

  bool isread = false;
  while (!isread) {
    int ssid    = -1;
    int intid   = -1;
    int tblid   = -1;
    int fieldid = -1;
    Real amp    = 0.0;
    token = ts->getToken();
    // Parse non-comment lines
    if (strncasecmp("sideset", token, 7) == 0) {
      token = ts->getToken();
      sscanf(token, "%d", &ssid);
      token = ts->getToken();
      sscanf(token, "%d", &tblid);
      token = ts->getToken();
      getTokenParam(token, amp);
      // Scan for the internal sideset #
      bool set = mesh->checkUserSidesetId(ssid, intid);
      if (!set) {
        ts->handleError();
        throw Exception("Problem parsing side set Id");
      }
      // Add a Temperature BC
      BCPackage bc;
      bc.m_setId = ssid;
      bc.m_intId = intid;
      bc.m_centering = FACE_CTR;
      bc.m_tblid = tblid;
      bc.m_amp = amp;
      bc.m_fieldId = fieldid;
      CCConduction* con = static_cast<CCConduction*>(physman.getPhysics());
      con->addTemperatureBC(bc);
    } else if (strncasecmp("end", token, 3) == 0) {
      isread = true;
    } else {
      ts->handleError();
      throw Exception("Problem parsing Temperature BC");
    }
  } // end of the while
  return false;
}

void
setCCConductionDefaults(Control* control)
/*******************************************************************************
Routine: setCCConductionDefaults -- setup default controls
Author : Mark A. Christon
*******************************************************************************/
{
  control->setOptionVal(Control::SOLTYP, FVM_CC_CONDUCTION, "FVM - HT");

  // Add Conduction base category
  control->addCategory(FVM_CC_CONDUCTION_CATEGORY);

  Category* pcat = control->getCategory(FVM_CC_CONDUCTION_CATEGORY);

  // Equation solver defaults
  Category* eqcat = pcat->getCategory(FVMCCConductionControl::EQ_SOLVER_CAT);

  eqcat->setOptionVal(IterativeSolverControl::TYPE, PETSC_FGMRES,
                      "PETSC_FGMRES");

  // Restart size = 30 is the PETSc default
  eqcat->addOption(IterativeSolverControl::NUM_RESTART, true, false, 30,
                   "Number of restart vectors");

}

} // End of Hydra namespace
