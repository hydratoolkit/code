//******************************************************************************
//! \file    src/ParseControl/CCIncNavierStokesParse.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   Parsing interface for CCIncNavierStokes
//******************************************************************************
#ifndef CCIncNavierStokesParse_h
#define CCIncNavierStokesParse_h

namespace Hydra {

// CC Incompressible Navier Stokes Analysis keywords
typedef bool
(*CCINS_Keyword_Function)(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

typedef struct {
  const char* name;
  CCINS_Keyword_Function func;
} CCINS_Keyword;

// CC Incompressible Navier Stokes parse functions
bool parseCCIncNavierStokes(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);

// Boundary conditions
bool parseCCINSBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSDispBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseHeatFluxBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSHSTAT(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSMassFlowBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSMassFluxBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSPressureOutflowBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSPassiveOutflowBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSPeriodicSideBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSPVDepBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSSPECIES(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSSpeciesDiffusionFluxBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSSymmVelBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseSurfChem(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSTractionBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSVelBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSVolumeFlowBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

// Initial Conditions
bool parseCCINSBlockICs(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSICs(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

// Model Options
bool parseCCINSENERGY(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSMOMSOL(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSPPESOL(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSSOLMETHOD(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSTINT(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSTRNSOL(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

// Source terms, body forces, drag forces
bool parseCCINSBODYFORCE(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSBOUSSFORCE(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSGravityForce(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSHEATSOURCE(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSPOROUSDRAG(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

// Turbulence model options
bool parseCCINSTMODEL(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCINSTURB(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseRLZKEOpt(TokenStream*, Category* cat);
bool parseRNGKEOpt(TokenStream*, Category* cat);
bool parseSTDKEOpt(TokenStream*, Category* cat);
bool parseNLKEOpt(TokenStream*, Category* cat);
bool parseSAOpt(TokenStream*, Category* cat);
bool parseSMAGOpt(TokenStream*, Category* cat);
bool parseSSTKWOpt(TokenStream*, Category* cat);
bool parseKSGSOpt(TokenStream*, Category* cat);
bool parseLDKMKSGSOpt(TokenStream*, Category* cat);
bool parseWALEOpt(TokenStream*, Category* cat);

// Defaults for the CC Incompressible Navier-Stokes
void setCCINSDefaults(Control*);

}

#endif // CCIncNavierStokesParse_h
