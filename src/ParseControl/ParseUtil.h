//******************************************************************************
//! \file    src/ParseControl/ParseUtil.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   Utility parsing functiosn
//******************************************************************************
#ifndef ParseUtil_h
#define ParseUtil_h

#include <cstring>

namespace Hydra {

// Forward declarations
class PhysicsManager;
class TokenStream;

// Utility functions

//! Check parameter bounds
//!   \param[in] ts     TokenStream pointer
//!   \param[in] val    Real parameter to check
//!   \param[in] minval Minimal value (>= minval)
//!   \param[in] maxval Minimal value (< maxval)
//!   \param[in] error  String describing error condition
void checkParamBounds(TokenStream* ts, const Real val, const Real minval,
                      const Real maxval, string error);

//! Check minimal value for a parameter
//!   \param[in] ts     TokenStream pointer
//!   \param[in] val    Integer parameter to check
//!   \param[in] minval Minimal value
//!   \param[in] error  String describing error condition
void checkParamMin(TokenStream* ts, const int val, const int minval,
                   string error);

//! Check minimal value for a parameter
//!   \param[in] ts     TokenStream pointer
//!   \param[in] val    Real parameter to check
//!   \param[in] minval Minimal value
//!   \param[in] error  String describing error condition
void checkParamMin(TokenStream* ts, const Real val, const Real minval,
                   string error);

//! Check positivity of a parameter value (param >= 0)
//!   \param[in] ts     TokenStream pointer
//!   \param[in] val    integera parameter to check
//!   \param[in] minval Minimal value
//!   \param[in] error  String describing error condition
void checkParamPos(TokenStream* ts, const int val, string error);

//! Check positivity of a parameter value (param >= 0.0)
//!   \param[in] ts     TokenStream pointer
//!   \param[in] val    Real parameter to check
//!   \param[in] minval Minimal value
//!   \param[in] error  String describing error condition
void checkParamPos(TokenStream* ts, const Real val, string error);

//! Get a real parameter from the token stream
//!   \param[in]  token  char* token
//!   \param[out] val    int  parameter
void getTokenParam(char* token, int &val);

//! Get a real parameter from the token stream
//!   \param[in]  token  char* token
//!   \param[out] val    Real parameter
void getTokenParam(char* token, Real &val);


} // End of Hydra namespace

#endif // ParseUtil_h
