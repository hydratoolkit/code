//******************************************************************************
//! \file    src/ParseControl/CCAdvectionParse.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   Cell-Centered Advection specific parsing
//******************************************************************************
#ifndef CCAdvectionParse_h
#define CCAdvectionParse_h

namespace Hydra {

// CC Advection Analysis keywords
typedef bool
(*CCAdvection_Keyword_Function)(TokenStream*, UnsMesh*, Control*,
                                PhysicsManager&);

typedef struct {
  const char* name;
  CCAdvection_Keyword_Function func;
} CCAdvection_Keyword;

// DG/FVM CCAdvection, CCBurgers and CCLset
bool parseCCAdvection(TokenStream*, fileIO*, UnsMesh*, Control*,
                      PhysicsManager&);
bool parseCCBurgers(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCLset(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);

bool parseTemperatureIC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseTBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

}

#endif // CCAdvectionParse_h
