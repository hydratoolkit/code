//******************************************************************************
//! \file    src/ParseControl/LagrangianDynamicsParse.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:47 2011
//! \brief   FEM Lagrangian dynamics specific parsing
//******************************************************************************
#include <string>
#include <vector>

using namespace std;

#include <HydraTypes.h>
#include <Exception.h>
#include <fileIO.h>
#include <TokenStream.h>
#include <PhysicsManager.h>
#include <ParseControl.h>
#include <ParseMaterial.h>
#include <ParseUtil.h>

#include <LagrangianDynamics.h>
#include <LagrangianDynamicsParse.h>

namespace Hydra {

// Setup the Navier-Stokes specific list of keywords
#define KEYWORD(name,func) {name,func},
Dynamics_Keyword dynamics_list[] = {
#include <AnalysisKeywords.def>
#include <LagrangianDynamicsKeywords.def>
};
#undef KEYWORD


bool
parseDynamics(TokenStream* ts, fileIO* /*io*/, UnsMesh* mesh,
              Control *control, PhysicsManager& physman)
/*******************************************************************************
Routine: parseDynamics - parse the Lagrangian hydro physics options
Author : Mark A. Christon
*******************************************************************************/
{
  bool  isread = false;
  char* token;

  // Create the physics object
  physman.addPhysics(FEM_LAGRANGIAN, mesh, control);
  control->setOptionVal(Control::SOLTYP, FEM_LAGRANGIAN, "FEM - LD");

  // Analysis keywords
  typedef map<const char*,
              bool(*)(TokenStream*, UnsMesh*, Control*, PhysicsManager&),
              parseComparitor> AnalysisParseFunctions;
  AnalysisParseFunctions analysis;

  // Add the physics-specific keyword-function pairs
  int Nkw = sizeof(dynamics_list)/sizeof(Dynamics_Keyword);
  for (int i=0; i<Nkw; i++) {
    analysis.insert(
        AnalysisParseFunctions::value_type(dynamics_list[i].name,
                                           dynamics_list[i].func));
  }

  // Parse the remainder of the analzye -- end block
  while (!isread) {
    token = ts->getToken();
    // Parse non-comment lines
    if (analysis.find(token) != analysis.end()) {
      isread = analysis[token](ts, mesh, control, physman);
    } else {
      ts->handleError();
      throw Exception("Problem parsing control file");
    }
  } // end of the while

  return false;
}

bool
parseArtificialViscosity(TokenStream* ts, UnsMesh* /*mesh*/,
                         Control* /*control*/, PhysicsManager& physman)
/*******************************************************************************
Routine: parseArtificialViscosity - parse the art_visc -- end block
Author : Mark A. Christon

OLD:
art_visc
  linear    lvalue
  quadratic qvalue
end

Block-by-block assignment                       All blocks
art_visc                                        art_visc
  id        id      (used internally only)        id        id
  linear    lvalue                                linear    lvalue
  quadratic qvalue                                quadratic qvalue
  block     b1                                    all_blocks
  block     b2                                   end
  block     b3
  ...
  block     bn
end
*******************************************************************************/
{
  char* token;
  bool  isread = false;
  bool  allblocks = true;
  int   Id = -1;          // Required
  Real  linear = 0.40;    // Dyna-style
  Real  quadratic = 1.00; // Dyna-style
  vector<int> classIds;

  // Parse the art_visc -- end block
  while (!isread) {
    token = ts->getToken();
    // Parse non-comment lines
    if (strncasecmp("id", token, 2) == 0) {
      token = ts->getToken();
      sscanf(token, "%d", &Id);
    }
    else if (strncasecmp("linear", token, 6) == 0 ) {
      token = ts->getToken();
      getTokenParam(token, linear);
    }
    else if (strncasecmp("quadratic", token, 9) == 0 ) {
      token = ts->getToken();
      getTokenParam(token, quadratic);
    }
    else if (strncasecmp("all_blocks", token, 10) == 0 ) {
      allblocks = true;
    }
    else if (strncasecmp("block", token, 10) == 0 ) {
      int bid;
      token = ts->getToken();
      sscanf(token, "%d", &bid);
      classIds.push_back(bid);
    }
    else if (strncasecmp("end", token, 3) == 0 ) {
      isread = true;
    }
    else {
      ts->handleError();
      throw Exception("Problem parsing art_visc -- end");
    }
  } // end of the while

  // Must either specify all blocks or a list of block Id's
  if (!allblocks && classIds.size() == 0) {
      ts->handleError();
      throw Exception("Block Id's not specified");
  }
  if (Id == -1) {
    ts->handleError();
    throw Exception("Artificial viscosity Id not set");
  }

  LagrangianDynamics* ld =
    reinterpret_cast<LagrangianDynamics*>(physman.getPhysics());

  bool err = false;
  err = ld->addArtificialViscosity(allblocks, Id, linear, quadratic, classIds);

  classIds.clear();

  // Error trap
  if (err) {
    ts->handleError();
    throw Exception("Problem with artificial viscosity definition");
  }

  return false;
}

bool
parseHourGlass(TokenStream* ts, UnsMesh* /*mesh*/, Control* /*control*/,
               PhysicsManager& physman)
/*******************************************************************************
Routine: parseHourGlass - parse the hourglass control -- end block
Author : Mark A. Christon

Block-by-block assignment                       All blocks
hourglass                                       hourglass
  id        id      (used internally only)        id        id
  type      none | fb | pisces                    type      type
  viscous   viscous_coef                          viscous   stiff_coef
  stiff     stiff_coef                            stiff     stiff_coef
  block     b1                                    all_blocks
  block     b2                                   end
  block     b3
  ...
  block     bn
end
*******************************************************************************/
{
  char* token;
  bool  isread = false;
  bool  allblocks = true;
  int   Id = -1;        // Required
  Real  viscous = 0.10;
  Real  stiff   = 0.10;
  HGType type = FB_STAB;
  vector<int> classIds;

  // Parse the hourglass -- end block
  while (!isread) {
    token = ts->getToken();
    // Parse non-comment lines
    if (strncasecmp("id", token, 2) == 0) {
      token = ts->getToken();
      sscanf(token, "%d", &Id);
    }
    else if (strncasecmp("type", token, 4) == 0 ) {
      token = ts->getToken();
      if (strncasecmp("none", token, 4) == 0) {
        type = NO_STAB;
      } else if (strncasecmp("dyna", token, 2) == 0) {
        type = H_STAB;
      } else if (strncasecmp("fb", token, 2) == 0) {
        type = FB_STAB;
      } else if (strncasecmp("pisces", token, 6) == 0) {
        type = PISCES_STAB;
      }
    }
    else if (strncasecmp("viscous", token, 7) == 0 ) {
      token = ts->getToken();
      getTokenParam(token, viscous);
    }
    else if (strncasecmp("stiff", token, 5) == 0 ) {
      token = ts->getToken();
      getTokenParam(token, stiff);
    }
    else if (strncasecmp("all_blocks", token, 10) == 0 ) {
      allblocks = true;
    }
    else if (strncasecmp("block", token, 10) == 0 ) {
      int bid;
      token = ts->getToken();
      sscanf(token, "%d", &bid);
      classIds.push_back(bid);
    }
    else if (strncasecmp("end", token, 3) == 0 ) {
      isread = true;
    } else {
      ts->handleError();
      throw Exception("Problem parsing hourglass -- end");
    }
  } // end of the while

  // Must either specify all blocks or a list of block Id's
  if (!allblocks && classIds.size() == 0) {
      ts->handleError();
      throw Exception("Block Id's not specified");
  }
  if (Id == -1) {
    ts->handleError();
    throw Exception("Hourglass control Id not set");
  }

  LagrangianDynamics* ld =
    reinterpret_cast<LagrangianDynamics*>(physman.getPhysics());

  bool err = false;
  err = ld->addHourglassControl(allblocks, Id, viscous, stiff, type, classIds);

  classIds.clear();

  // Error trap
  if (err) {
    ts->handleError();
    throw Exception("Problem with hourglass control definition");
  }

  return false;
}

}
