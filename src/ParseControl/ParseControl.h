//******************************************************************************
//! \file    src/ParseControl/ParseControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   Parsing interface for control files
//******************************************************************************
#ifndef ParseControl_h
#define ParseControl_h

#include <cstring>

namespace Hydra {

// Forward declarations
class PhysicsManager;

struct parseComparitor {
  bool operator()(const char* s1, const char* s2) const {
    return strcmp(s1, s2) < 0;
  }
};

// Keyword signatures & structs
// Main keywords
typedef bool
(*Main_Keyword_Function)(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);

typedef struct {
  const char* name;
  Main_Keyword_Function func;
} Main_Keyword;

// Control file parsing functions
// Individual keywords are parseKEY()
// General functions that parse keyword -- end are parseName()

void parseControl(fileIO*, UnsMesh*, Control*, PhysicsManager&);

// Main (Global) Keywords
bool parseEXIT(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);
bool parseTITLE(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);

// Equation Solver Keywords
bool parseNodeSolver(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

// Generic Analysis Keywords
bool parseCatalyst(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCFL(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseDELTAT(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseDUMP(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseEND(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseExeControl(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseHistVar(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseLoadBalance(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseTable(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseNSTEPS(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parsePlotVar(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parsePlotStatVar(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parsePLTYPE(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseFILETYPE(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parsePLTF(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parsePLTI(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parsePRTI(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parsePRTLEV(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseStatistics(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseTHTF(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseTHTI(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseTERM(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseTTYI(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

// FEM Generic Keywords
bool parseMASS(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseTHETAK(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

// Generic IC Keywords
bool parseICs(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

// Mesh Transformation Keywords
bool parseMergeSidesets(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseMeshTransform(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

// Multifield parsing
bool parseFieldIdPair(TokenStream* ts,
                      char*& token,
                      vector< pair<int,int> >& phase,
                      const int nfields);

bool parseFieldIdRange(TokenStream* ts,
                       char*& token,
                       vector<int>& phase,
                       const int nfields);

void processFieldIDPair(TokenStream* ts,
                        string& str,
                        size_t middle,
                        int nfields,
                        int& id1,
                        int& id2);

}

#endif // ParseControl_h
