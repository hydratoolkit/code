//******************************************************************************
//! \file    src/ParseControl/CCDummyParse.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   Dummy physics keyword parsing -- for testing
//******************************************************************************

#ifndef CCDummyParse_h
#define CCDummyParse_h

namespace Hydra {

// Dummy Physics Analysis keywords
typedef bool
(*CCDummy_Keyword_Function)(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

typedef struct {
  const char* name;
  CCDummy_Keyword_Function func;
} CCDummy_Keyword;

// DG/FVM CCDummy
bool parseCCDummy(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);

}

#endif // CCDummyParse_h
