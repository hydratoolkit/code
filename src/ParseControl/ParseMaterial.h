//******************************************************************************
//! \file    src/ParseControl/ParseMaterial.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   Parse material properties
//******************************************************************************
#ifndef ParseMaterial_h
#define ParseMaterial_h

#include <cstring>

namespace Hydra {

// Forward declarations
class PhysicsManager;

// Parse the material -- end keyword block
bool parseMaterial(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

// Parse individual material properties
void parseConductivity(TokenStream*, Material*);
void parseDensity(TokenStream*, Material*);
void parseMaterialVelocity(TokenStream*, Material*);
void parsePermeability(TokenStream*, Material*);
void parseSpecificHeat(TokenStream*, Material*);
void parseStrength(TokenStream*, Material*);
void parseThermalExpansion(TokenStream*, Material*);
void parseViscosity(TokenStream*, Material*);
void parseEOS(TokenStream*, Material*);

// Parase material sets
bool parseMaterialSet(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

}

#endif // ParseMaterial_h
