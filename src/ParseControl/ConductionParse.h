//******************************************************************************
//! \file    src/ParseControl/ConductionParse.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   Conduction specific parsing
//******************************************************************************
#ifndef ConductionParse_h
#define ConductionParse_h

namespace Hydra {

// Conduction Analysis keywords
typedef bool
(*Conduction_Keyword_Function)(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

typedef struct {
  const char* name;
  Conduction_Keyword_Function func;
} Conduction_Keyword;

bool parseConduction(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);
bool parseConductionICs(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseConvectionBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

bool parseTemperatureBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

void setConductionDefaults(Control* control);

}

#endif // ConductionParse_h
