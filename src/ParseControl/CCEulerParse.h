//******************************************************************************
//! \file    src/ParseControl/CCEulerParse.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   CCEuler-specific keyword parsing methods
//******************************************************************************
#ifndef CCEulerParse_h
#define CCEulerParse_h

namespace Hydra {

// CC Euler Analysis keywords
typedef bool
(*CCEuler_Keyword_Function)(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

typedef struct {
  const char* name;
  CCEuler_Keyword_Function func;
} CCEuler_Keyword;

// DG/FVM CC_Euler
bool parseCCEuler(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);
bool parseEulerBodyForce(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCEulerIntegrator(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseDirichletBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseInitBlock(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseIntTracker(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseUserIC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseZeroVel(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

}

#endif // CCEulerParse_h
