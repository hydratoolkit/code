//******************************************************************************
//! \file    src/ParseControl/AdvectionDiffusionParse.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   FEM Advection-Diffusion specific parsing
//******************************************************************************

#ifndef AdvectionDiffusionParse_h
#define AdvectionDiffusionParse_h

namespace Hydra {

//! Advection Analysis keywords
typedef bool
(*Advection_Keyword_Function)(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

//! Advection-Diffusion Keyword/Function Association
typedef struct {
  const char* name;
  Advection_Keyword_Function func;
} Advection_Keyword;

bool parseAdvection(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);

void setAdvectionDiffusionDefaults(Control* control);

}

#endif // AdvectionDiffusionParse_h
