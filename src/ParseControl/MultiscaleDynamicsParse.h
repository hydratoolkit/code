//******************************************************************************
//! \file    src/ParseControl/MultiscaleDynamicsParse.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   Multiscale Lagrangian Dynamics Parsing
//******************************************************************************
#ifndef MultiscaleDynamicsParse_h
#define MultiscaleDynamicsParse_h

namespace Hydra {

// Multiscale Dynamics Analysis keywords
typedef bool
(*MSDynamics_Keyword_Function)(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

typedef struct {
  const char* name;
  MSDynamics_Keyword_Function func;
} MSDynamics_Keyword;

bool parseMSDynamics(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);

bool parseMSDynamicsUserIC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

}

#endif // MultiscaleDynamicsParse_h
