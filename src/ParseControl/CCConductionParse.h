//******************************************************************************
//! \file    src/ParseControl/CCConductionParse.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   Cell-centered heat conduction
//******************************************************************************
#ifndef CCConductionParse_h
#define CCConductionParse_h

namespace Hydra {

// CC Advection Analysis keywords
typedef bool
(*CCConduction_Keyword_Function)(TokenStream*, UnsMesh*, Control*,
                                 PhysicsManager&);

typedef struct {
  const char* name;
  CCConduction_Keyword_Function func;
} CCConduction_Keyword;

// DG/FVM CCConduction
bool parseCCConduction(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);

bool parseCCConductionICs(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCCTSBC(TokenStream* , UnsMesh* , Control* , PhysicsManager&);

void setCCConductionDefaults(Control* control);

}

#endif // CCConductionParse_h
