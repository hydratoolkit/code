//******************************************************************************
//! \file    src/ParseControl/RBDynamicsParse.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   Parsing interface for RBDynamics
//******************************************************************************

#ifndef RBDynamicsParse_h
#define RBDynamicsParse_h

#include <RBDynamics.h>

namespace Hydra {

// Rigid Body Dynamics Analysis keywords
typedef bool
(*RBDynamics_Keyword_Function)(TokenStream*, UnsMesh*, Control*,
                               PhysicsManager&);

typedef struct {
  const char* name;
  RBDynamics_Keyword_Function func;
} RBDynamics_Keyword;

// Rigid Body Dynamics Keywords
bool parseRBDynamics(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);
bool parseRBDynBFs(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseRBDynICs(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

}

#endif // RBDynamicsParse_h
