//******************************************************************************
//! \file    src/ParseControl/CCLagrangianParse.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   Lagrangian dynamics specific parsing
//******************************************************************************
#ifndef CCLagrangianParse_h
#define CCLagrangianParse_h

namespace Hydra {

// CC Lagrangian Analysis keywords
typedef bool
(*CCLagrangian_Keyword_Function)(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

typedef struct {
  const char* name;
  CCLagrangian_Keyword_Function func;
} CCLagrangian_Keyword;

// FVM CC_Lagrangian
bool parseCCLagrangian(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);
// Solver Options
bool parseCCLNodeSolver(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCLRiemannSolver(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCLSpatialOrder(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCLTimeInt(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCLGradientLimiter(TokenStream* ts, UnsMesh* mesh, Control* control,
                              PhysicsManager& physman);

// Initial Conditions
bool parseCCLICs(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCLBlockICs(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCLPredefICs(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

// Boundary Conditions
bool parseCCLPressureBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCLVelocityBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseCCLZeroVelBC(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

// Solver defaults
void setCCLagrangianDefaults(Control*);

}

#endif // CCLagrangianParse_h
