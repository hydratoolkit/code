//******************************************************************************
//! \file    src/ParseControl/MultimaterialLagrangianParse.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   Multimaterial Lagrangian specific parsing
//******************************************************************************
#ifndef MultimaterialLagrangianParse_h
#define MultimaterialLagrangianParse_h

namespace Hydra {

// reuse Lagrangian Dynamics Analysis keywords
typedef bool
(*Multimaterial_Keyword_Function)(TokenStream*, UnsMesh*, Control*,
                                  PhysicsManager&);

typedef struct {
  const char* name;
  Multimaterial_Keyword_Function func;
} Multimaterial_Keyword;

// Multimaterial Lagrangian hydro
bool parseMultimaterialLagrangian(TokenStream*, fileIO*, UnsMesh*, Control*,
                                  PhysicsManager&);

// reuse Lagrangian Dynamics parsing for these keywords
bool parseHourGlass(TokenStream*, UnsMesh*, Control*, PhysicsManager&);
bool parseArtificialViscosity(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

// Multimaterial Lagrangian hydro only keywords
bool parseBoxInsert(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

}

#endif // MultimaterialLagrangianParse_h
