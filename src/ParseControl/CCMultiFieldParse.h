//******************************************************************************
//! \file    src/ParseControl/CCMultiFieldParse.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   Parse functions specific to multifield flows
//******************************************************************************
#ifndef CCMultiFieldParse_h
#define CCMultiFieldParse_h

namespace Hydra {

// CC Euler Analysis keywords
typedef bool
(*CCMP_Keyword_Function)(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

typedef struct {
  const char* name;
  CCMP_Keyword_Function func;
} CCMP_Keyword;

// CC Multiphase parse functions
bool parseCCMultiField(TokenStream*, fileIO*, UnsMesh*, Control*,
                       PhysicsManager&);

bool parseCCMFNFields(TokenStream* ts, UnsMesh* mesh, Control *control,
                      PhysicsManager& physman);

void setCCMultiFieldDefaults(Control*);

// Drag model parsers
bool parseCCMFConstDrag(TokenStream* ts, UnsMesh* mesh, Control* control,
                        PhysicsManager& physman);
bool parseCCMFIshiiZuberDrag(TokenStream* ts, UnsMesh* mesh, Control* control,
                             PhysicsManager& physman);
bool parseCCMFTomiyamaDrag(TokenStream* ts, UnsMesh* mesh, Control* control,
                           PhysicsManager& physman);

// Lift model parsers
bool parseCCMFConstLift(TokenStream* ts, UnsMesh* mesh, Control* control,
                        PhysicsManager& physman);
}

#endif // CCMultiField_h
