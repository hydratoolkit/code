//******************************************************************************
//! \file    src/ParseControl/CCFrontParse.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:58:46 2011
//! \brief   Frontier keywords
//******************************************************************************
#ifndef CCFrontParse_h
#define CCFrontParse_h

namespace Hydra {

// CC Euler Analysis keywords
typedef bool
(*CCFront_Keyword_Function)(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

typedef struct {
  const char* name;
  CCFront_Keyword_Function func;
} CCFront_Keyword;

// DG/FVM CC_Euler
bool parseCCFront(TokenStream*, fileIO*, UnsMesh*, Control*, PhysicsManager&);
bool parseFrontTracker(TokenStream*, UnsMesh*, Control*, PhysicsManager&);

}

#endif // FVM_CC_EULER_PARSE_H
