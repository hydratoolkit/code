//******************************************************************************
//! \file    src/Base/Timer.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Simple class for registration-based timers.
//******************************************************************************

#ifndef Timer_h
#define Timer_h

#ifdef WIN32

#else
#include <unistd.h>
#include <sys/times.h>
#endif // End of WIN32

#include <string>

#include <HydraTypes.h>

namespace Hydra {

#define MAX_TIMERS 1024

//! Timer is the basic timer object in the Hydra Toolkit
class Timer {

  public:

    //! \name Constructor/Destructor
    //@{
            Timer();
   virtual ~Timer() {}
   //@}

   //! \name Timer() is the basic tiiming object in HYDRA.
   //! The Timer object is used to register, store and report
   //! code timings for critical code sections.
   //!
   //! The TimerIndex is a simple handle for retrieving timing data without
   //! carrying raw pointers around.  The timer register keeps track
   //! of the CPU time usage in the code and can be queried from
   //! anywhere in the code.
   //@{
   //! Register timer objects using this function
   TimerIndex registerTimer(const char *label);

   //! Release a timer
   void freeTimer(TimerIndex tid);

   //! Free ALL data/objects
   void freeAllTimers();

   void startTimer(TimerIndex vid);

   void stopTimer(TimerIndex vid);

   //! Return the time for a given a Timer
   Real getTime(TimerIndex vid);

   //! Lookup a timer by name
   TimerIndex findTimer(string vname);

   //! Get the string for a timer
   const char* getName(TimerIndex vid) {return name[vid].c_str();}

   //! Report all timers
   void reportTimers(ostream& ofs);

   void reportCycleTime(ostream& ofs, TimerIndex vid, int Nel, int Nstep);

   //! Dump a list of all registered timers
   void dumpTimers();
   //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    Timer(const Timer&);
    Timer& operator=(const Timer&);
    //@}

    Real     time[MAX_TIMERS];
    Real   tstart[MAX_TIMERS];
    string   name[MAX_TIMERS];  //!< Timer names
    bool    inuse[MAX_TIMERS];  //!< Currently inuse (or not)

    Real seconds_per_tick;
#ifndef WIN32
    struct tms time_info;
#endif
};

}

#endif  // Timer_h
