//******************************************************************************
//! \file    src/Base/BodyForce.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Base class for all other body forces across physics
//******************************************************************************
#ifndef BodyForce_h
#define BodyForce_h

#include <HydraTypes.h>

namespace Hydra {

//! Body force base class
class BodyForce {

  public:

    BodyForce() {}

    BodyForce(const int tblid,
              const Real amp,
              const int setid,
              const Real* en) : m_tblid(tblid), m_setid(setid), m_amp(amp) {
      m_en[0] = en[0];
      m_en[1] = en[1];
      m_en[2] = en[2];
    }

    virtual ~BodyForce() {}

    int  getTableId() const     {return m_tblid;}
    void setTableId(int tblid)  {m_tblid = tblid;}

    int  getSetId() const       {return m_setid;}
    void setSetId(int setid)    {m_setid = setid;}

    Real getAmplitude() const   {return m_amp;}
    void setAmplitude(Real amp) {m_amp = amp;}

    Real* getDir()              {return m_en;}

  protected:

    int m_tblid;  //!< table Id
    int m_setid;  //!< Element set id (Exodus block) for the body force
    Real m_amp;   //!< Amplitude for forcing function
    Real m_en[3]; //!< Direction vector for body force

 private:

    //! Don't permit copy or assignment operators
    //@{
    BodyForce(const BodyForce&);
    BodyForce& operator=(const BodyForce&);
    //@}
};

}

#endif // BodyForce_h
