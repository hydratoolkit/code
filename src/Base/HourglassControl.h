//******************************************************************************
//! \file    src/Base/HourglassControl.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Base class to hold hourglass control parameters
//******************************************************************************
#ifndef HourglassControl_h
#define HourglassControl_h

#include <HydraTypes.h>

namespace Hydra {

enum HGType {NO_STAB=0, H_STAB, FB_STAB, PISCES_STAB};

//! Base hourglass control class
class HourglassControl {

  public:

    //! \name Constructor/Destructor
    //@{
             HourglassControl(int Id, HGType type,
                              Real viscosity, Real stiffness);
    virtual ~HourglassControl() {}

    int getId() {return m_Id;}

    HGType getType() {return m_type;}

    Real getViscosity() {return m_viscosity;}

    Real getStiffness() {return m_stiffness;}
    //@}

  protected:

    int m_Id;

    Real m_viscosity;
    Real m_stiffness;

    HGType m_type;

  private:

    //! Don't permit copy or assignment operators
    //@{
    HourglassControl(const HourglassControl&);
    HourglassControl& operator=(const HourglassControl&);
    //@}

};

}

#endif // HourglassControl_h
