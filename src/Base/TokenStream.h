//******************************************************************************
//! \file    src/Base/TokenStream.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Token stream for parsing
//******************************************************************************

#ifndef TokenStream_h
#define TokenStream_h

#include <fstream>
#include <string>
#include <vector>

#include <HydraTypes.h>

namespace Hydra {

//! Maximum number of buffered tokens
#define MAX_PARSE_HIST 10

//! Token stream class for parsing -- tokenize an control file
class TokenStream {

  public:

    //! \name Constructor/Destructor
    //@{
    //! Open an ifstream
             TokenStream(const string&);

    //! Close the ifstream
    virtual ~TokenStream() {ifs.close();}
    //@}

    int   numLinesParsed() {return lines_parsed;}
    char* getToken();
    char* getCurrToken() {return const_cast<char*>(token.c_str());}
    char* getPrevToken() {return const_cast<char*>(prev_token.c_str());}
    char* getNextLine();
    bool  iscomment(const string&);
    void  handleError();

  private:

    //! Don't permit copy or assignment operators
    //@{
    TokenStream(const TokenStream&);
    TokenStream& operator=(const TokenStream&);
    //@}

    void  getLine(istream& is, string& t);

    ifstream ifs;

    string filename;

    string cbuf;
    string token;
    string prev_token;
    string::iterator current_pos;

    int lines_parsed;
    vector<string> parse_hist;
};

}

#endif // TokenStream_h
