//******************************************************************************
//! \file    src/Base/Source.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Base class for volumetric source terms
//******************************************************************************
#ifndef Source_h
#define Source_h

#include <iostream>
#include <ostream>

#include <HydraTypes.h>
#include <SourcePackage.h>
#include <SourceTypes.h>

namespace Hydra {

//! Base class for volumetric source terms
class Source {

  public:

    //! \name Constructor/Destructor
    //@{
             Source();
             Source(SourcePackage& sp);
             Source(int tblid, Real amp, int setid) :
               m_tblid(tblid), m_fieldid(-1), m_setid(setid), m_amp(amp) {}
    virtual ~Source() {}
    //@}

    //**************************************************************************
    //! \name Virtual Soruce Interface
    //! These functions are virtual and are implemented for each specific
    //! type of source condition.
    //@{
    //! Echo one liner info about Source (virtual, def below is the default)
    virtual void echo(ostream& ofs) const;

    //! Set a user-defined source value
    virtual Real setUserValue() {
      return 0.0;
      cout << "No default implementation of Source::setUserValue()" << endl;
    }
    //@}

    //! Accessors
    int  getSetId() const       {return m_setid;}
    Real getAmplitude() const   {return m_amp;}
    int  getFieldId()           {return m_fieldid;} //!< Get the field Id
    int  getTableId() const     {return m_tblid;}
    SourceType getType()        {return m_type;}    //!< Get the source type

    //! Set functions
    void setSetId(int setid)    {m_setid = setid;}
    void setAmplitude(Real amp) {m_amp = amp;}
    void setFieldId(int id)     {m_fieldid = id;}   //!< Set the field Id
    void setTableId(int tblid)  {m_tblid = tblid;}


  protected:

    //! \name Source data
    //! This data is used in the inhereted physics-specific source terms.
    //! So, the amplitude is interpreted in each of the physics-specific
    //! cases accordingly.  For example, CCINSHeatSource, the
    //! amplitude (amp) represents the volumetric heat source amplitude
    //! or Q.
    //@{
    int  m_tblid;      //!< table Id
    int  m_fieldid;    //!< field Id
    int  m_setid;      //!< Element set id for application of the source terms
    Real m_amp;        //!< Amplitude for forcing function
    SourceType m_type; //!< BC type
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    Source(const Source&);
    Source& operator=(const Source&);
    //@}
};

}

#endif
