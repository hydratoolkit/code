//******************************************************************************
//! \file    src/Base/PrintUtil.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Pretty printing utilities
//******************************************************************************
#ifndef PrintUtil_h
#define PrintUtil_h

#include <fstream>
#include <string>

#include <HydraTypes.h>
#include <DataShapes.h>

namespace Hydra {

//! Define the line-lenght for output w. "..." fill
#define PRINT_LENGTH 55

//! Pretty-printing class
class PrintUtil {

  public:

    //! \name Constructor/Destructor
    //@{
             PrintUtil(ostream& ofs);
    virtual ~PrintUtil() {}
    //@}

    //! \name Banner, heading, etc. functions
    //@{
    void printBanner(const string executable,
                     const string version,
                     const string osname,
                     const string compiler,
                     const string buildType,
                     const string buildDate,
                     const string delimiter);

    void printFiles(const string date, string* fnames);

    void printHeader(const string header);

    void printSubHeader(const string header);

    void printOption(const string name);

    void printOption(const string name, bool val);

    void printOption(const string name, int val);

    void printOption(const string name, int val1, int val2);

    void printOption(const string name, int val1, Real val2);

    void printOption(const string name, Real val);

    void printOption(const string name, string val);

    void printOption(const string name, Vector& vec);
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    PrintUtil(const PrintUtil&);
    PrintUtil& operator=(const PrintUtil&);
    //@}

    ostream& m_ofs;

};

}

#endif // printUtil_h
