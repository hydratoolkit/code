//******************************************************************************
//! \file    src/Base/SourcePackage.h
//! \author  Mark A. Christon
//! \date    Thu Nov 30 17:18:19 MST 2017
//! \brief   Package for instantiating Source objects
//******************************************************************************
#ifndef SourcePackage_h
#define SourcePackage_h

#include <HydraTypes.h>
#include <SourceTypes.h>

namespace Hydra {

//! Struct for temporarily holding source term information read in the parser
struct SourcePackage {
  int m_setId;           //!< mesh entity set Id in user space
  int m_intId;           //!< Internal sideset Id
  int m_tblId;           //!< table id, -1 if empty
  int m_fieldId;         //!< Field index
  int m_centering;       //!< Centering
  int m_dir;             //!< Dir.: x=GLOBAL_XDIR, y=GLOBAL_YDIR, z=GLOBAL_ZDIR
  Real m_amp;            //!< amplitude - BC magnitude from keyword data line
  bool m_userBC;         //!< Flag indicating user-defined boundary condition

  SourcePackage(): m_setId(-1), m_intId(-1), m_tblId(-1), m_fieldId(-1),
                   m_centering(-1), m_dir(-1), m_userBC(false) {}

};

}

#endif
