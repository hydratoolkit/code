//******************************************************************************
//! \file    src/Base/ArtificialViscosity.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Base class for Q-style artificial viscosity
//******************************************************************************
#ifndef ArtificialViscosity_h
#define ArtificialViscosity_h

namespace Hydra {

//! Base class for artificial viscosity used in hydro methods
class ArtificialViscosity {
  public:
             ArtificialViscosity(int Id, Real linear, Real quadratic);
    virtual ~ArtificialViscosity() {}

    int getId() {return m_Id;}

    Real getLinear()    {return m_linear;   }
    Real getQuadratic() {return m_quadratic;}


  protected:

    int m_Id;

    Real m_linear;
    Real m_quadratic;

  private:
    //! Don't permit copy or assignment operators
    //@{
    ArtificialViscosity(const ArtificialViscosity&);
    ArtificialViscosity& operator=(const ArtificialViscosity&);
    //@}

};

}

#endif // ArtificialViscosity_h
