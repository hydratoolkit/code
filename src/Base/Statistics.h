//******************************************************************************
//! \file    src/Base/Statistics.h
//! \author  Jozsef Bakosi, Mark A. Christon
//! \date    Wed Feb 01 15:15:00 2012
//! \brief   Base class for field, surface, and history statistics
//******************************************************************************
#ifndef Statistics_h
#define Statistics_h

#include <vector>
#include <set>
#include <map>
#include <HydraTypes.h>
#include <AccumulatorKey.h>
#include <OutputDelegateKey.h>
#include <UnsMesh.h>
#include <Control.h>

namespace Hydra {

//! Macro to declare statistics accumulator wrappers
#define ACCUMULATOR_DECL(NAME) \
void NAME(void* obj, const AccumulatorKey& key, Real dt);

//! Macro to define statistics accumulators wrappers
#define ACCUMULATOR_IMPL(CLASS, METHOD, NAME) \
void NAME(void* obj, const AccumulatorKey& key, Real dt) \
{ \
  static_cast<CLASS*>(obj)->METHOD(key, dt); \
}

// Macro to declare statistics accumulators class-members
#define ACCUMULATOR_MEMB(NAME) \
void NAME(const AccumulatorKey& key, Real dt); \

//! Accumulator id
typedef int AccumulatorId;

//! Statistics delegate
struct StatisticsDelegate {
  vector<AccumulatorId> vecAccId;

  // constructor
  StatisticsDelegate(vector<AccumulatorId> init_vecAccId) :
                     vecAccId(init_vecAccId) {}

  // echo for debug
  void echo() {
    p0cout << "\tStatisticsDelegate:\n\t" <<
              "    vecAccId = ";
    vector<AccumulatorId>::iterator it;
    for (it=vecAccId.begin(); it!=vecAccId.end(); it++) {
      if (it!=vecAccId.end()-1) {
        p0cout << *it << ", ";
      } else {
        p0cout << *it;
      }
    }
    p0cout << endl;
  }
};

//! Statistics accumulator delegate
struct AccumulatorDelegate {
  AccumulatorKey key;     // accumulator key
  MemoryLayout layout;    // accumulator memory layout
  VariableType vtype;     // accumulator variable type
  bool plot;              // accumulator variable plottable

  // constructor
  AccumulatorDelegate(AccumulatorKey keyarg,
                      MemoryLayout layoutarg,
                      VariableType vtypearg,
                      bool plotarg) : key(keyarg),
                                      layout(layoutarg),
                                      vtype(vtypearg),
                                      plot(plotarg) {}
#ifdef STAT_DEBUG
  void echo() {
    p0cout << "\tAccumulatorDelegate:\n\t";
    key.echo("  ");
    p0cout << "\t          layout = " << this->layout << "\n\t" <<
              "           vtype = " << this->vtype << "\n\t" <<
              "            plot = " << this->plot << endl;
  }
#endif // STAT_DEBUG
};

//! Map of statistics delegates
typedef map<OutputDelegateKey, StatisticsDelegate> StatisticsDelegateMap;

//! Map of accumulator delegates
typedef map<AccumulatorId, AccumulatorDelegate> AccumulatorDelegateMap;

//! Set of accumulators
typedef set<AccumulatorKey> AccumulatorSet;

//! Map of accumulator variable indices and associated output delegate keys
typedef map<OutputDelegateKey, vector<DataIndex> > AccumulatorVarIdMap;

//! Map instantaneous variable dataindices to their means' dataindices
typedef map<DataIndex, DataIndex> InstMeanPairMap;

//! Enum for deciding where accumulator functions can reside
//! (This is used by the given physics, so it only needs to know whether the
//! accumulator function needs to be called from the physics or from a
//! statistics class.)
enum AccumulatorFuncOwner { STATISTICS = 0,     // in a statistics class
                            PHYSICS };          // in a physics class

// Statistics base class
class Statistics {

  public:

    //! \name Constructor/Destructor
    //@{
             Statistics();
    virtual ~Statistics();
    //@}

    //! Accumulate all accumulators
    void accumulate(const Real dt);

    //! Divide by sum of dt
    void divSumdt(Real* scalar, int number);
    void divSumdt(Real* scalar, int number, Real numerator);

#ifdef STAT_DEBUG
    //! Echo properties of accumulator delegates (known to code)
    void echoAccumulatorDelegates(const AccumulatorDelegateMap& map);
#endif //STAT_DEBUG

    //! Echo accumulators (based on user requests)
    void echoAccumulators();

    //! Echo accumulator variable indices (and associated output delegate keys)
    void echoAccumulatorVarIds();

    //! Echo map of instantaneous variables and their means
    void echoInstMeanPairs();

    //! Echo properties of statistics delegates (known to code)
    void echoStatisticsDelegates(const StatisticsDelegateMap& d);

    //! Get list of accumulators for a statistics delegate
    vector<DataIndex> getAccumulators(const OutputDelegateKey& key);

    //! Return mean variable index for an instantaneous var index
    DataIndex getMean(DataIndex instId);

    //! Get the sum of dt
    Real getSumdt() { return m_sumdt; }

    //! Set the sum of dt (at restart)
    void setSumdt(Real sumdt) { m_sumdt = sumdt; }

    //! Set the initial time mark for statistics collection
    Real setTimeMark(Real statStart,
                     bool restart,
                     Real time,
                     Real statPlotWinsize);

    //! Initialize all accumulators
    void initialize();

    //! Insert an instantaneous-mean variable index pair
    void insertVarIds(DataIndex inst, DataIndex mean);

  protected:

    //! Accumulate product for vector component w/ indexing
    void accumulateProduct(Real* prod,
                           const Real* const inst,
                           DataIndex meanId,
                           int component,
                           const int* const el_list,
                           int number);

    //! Accumulate product for central moment with indexing
    void accumulateProduct(Real* prod,
                           const Real* const inst,
                           DataIndex meanId,
                           const int* const el_list,
                           int number);

    //! Accumulate product for ordinary moment with indexing
    void accumulateProduct(Real* prod,
                           const Real* const inst,
                           const int* const el_list,
                           int number);

    //! Accumulate product for vector component for central moment
    void accumulateProduct(Real* prod,
                           const Real* const inst,
                           DataIndex meanId,
                           int component,
                           int number);

    //! Accumulate product for central moment
    void accumulateProduct(Real* prod,
                           const Real* const inst,
                           DataIndex mean,
                           int number);

    //! Accumulate product for ordinary moment
    void accumulateProduct(Real* prod,
                           const Real* const inst,
                           int number);

    //! Accumulate sum
    void accumulateSum(Real* sum,
                       const Real* const inst,
                       int number,
                       Real dt);

    //! Accumulators
    AccumulatorSet m_accumulators;
    //! Accumulator variable indices
    AccumulatorVarIdMap m_accumulatorIds;
    //! Map instantaneous varIds to that of their means
    InstMeanPairMap m_means;
    //! Object pointer to mesh
    UnsMesh* m_mesh;

  private:

    //! Don't permit copy or assignment operators
    //@{
    Statistics(const Statistics&);
    Statistics& operator=(const Statistics&);
    //@}

    int m_pid;    //<! Processor id
    Real m_sumdt; //<! Sum of dt
};

}

#endif // Statistics.h
