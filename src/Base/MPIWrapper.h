//******************************************************************************
//! \file    src/Base/MPIWrapper.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Simple MPI wrapper for generic interface
//******************************************************************************

#ifndef MPIWrapper_h
#define MPIWrapper_h

#ifdef USE_MPI
#include <mpi.h>
#endif

#include <HydraTypes.h>

namespace Hydra {

// Forward declarations
class MPIWrapper;

// This is a global variable, for now.  g_comm is set in MPIWrapper.C.
extern MPIWrapper *g_comm;

#ifdef USE_MPI
typedef MPI_Request CommRequest;
typedef MPI_Status  CommStatus;
typedef MPI_Comm    CommType;
#else
typedef int CommRequest;
typedef int CommStatus;
typedef int CommType;
#endif

// Define p0cout to control output in parallel
#define p0cout if (g_comm->getPid() == 0) cout
#define p0cerr if (g_comm->getPid() == 0) cerr

//! MPIWrapper wraps the MPI message-passing functions.
class MPIWrapper
{
  //! The communicator
  CommType m_comm;

  public:

    //! \name Constructor/Destructor
    //@{
    //! Constructor:  Arguments are those from main()
    MPIWrapper(int* argc, char*** argv);

    //! Constructor:  Duplicate an existing communicator without init
    MPIWrapper(CommType oldcomm);

    virtual ~MPIWrapper() {}
    //@}

    //! Generic message-passing functions to wrap MPI calls
    //@{
    void  blockingProbe(int source, int tag, CommStatus* status);

    void  blockingSend(int mtype, char* data, int len, int pid);

    void  blockingSend(int mtype, int* data, int len, int pid);

    void  blockingSend(int mtype, long* data, int len, int pid);

    void  blockingSend(int mtype, Real *data, int len, int pid);

    void  broadcast(char* value, int length, int pid);

    void  broadcast(int* value, int length, int pid);

    void  broadcast(Real* value, int length, int pid);

    //! Duplicates the underlying comm object
    void  duplicate(CommType *newcom);

    void  freeCommunicator(CommType *oldcom);

    void  finalMP();

    void  gather(int* sendbuf, int sendcnt, int* recvbuf, int recvcnt, int pid);

    int   getCount(CommStatus* status);

    int   getPid();

    int   getNproc();

    uint  globalMax(uint value);

    int   globalMax(int value);

    int*  globalMax(int* value, int* work, int length);

    Real  globalMax(Real value);

    Real* globalMax(Real* value, Real* work, int length);

    uint  globalMin(uint value);

    int   globalMin(int value);

    int*  globalMin(int* value, int* work, int length);

    Real  globalMin(Real value);

    Real* globalMin(Real* value, Real* work, int length);

    uint  globalSum(uint value);

    int   globalSum(int value);

    void  globalSum(int* value, int* work, int length);

    Real  globalSum(Real value);

    void globalSum(Real* value, Real* work, int length);

    CommRequest nonblockingReceive(int mtype, char* data, int len, int pid);

    CommRequest nonblockingReceive(int mtype, int* data, int len, int pid);

    CommRequest nonblockingReceive(int mtype, Real* data, int len, int pid);

    CommRequest nonblockingReceive(int mtype, bool* data, int len, int pid);

    CommRequest nonblockingSend(int mtype, bool* data, int len, int pid);

    CommRequest nonblockingSend(int mtype, char* data, int len, int pid);

    CommRequest nonblockingSend(int mtype, int* data, int len, int pid);

    CommRequest nonblockingSend(int mtype, Real* data, int len, int pid);

    int receive(bool* data, int len, int mtype);

    int receive(char* data, int len, int mtype);

    int receive(int* data, int len, int mtype);

    int receive(long* data, int len, int mtype);

    int receive(float* data, int len, int mtype);

    int receive(Real* data, int len, int mtype);

    int receive(bool* data, int len, int src_pid, int mtype);

    int receive(char* data, int len, int src_pid, int mtype);

    int receive(int* data, int len, int src_pid, int mtype);

    int receive(Real* data, int len, int src_pid, int mtype);

    int statusSize();

    void syncup();

    int testMessage(CommRequest request);

    int wait(CommRequest* request);

    void waitAll(int count, CommRequest* requests);

    int waitAny(int count, CommRequest* requests, int &index);
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    MPIWrapper(const MPIWrapper&);
    MPIWrapper& operator=(const MPIWrapper&);
    //@}

};

}

#endif // MPIWrapper_h
