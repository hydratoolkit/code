//******************************************************************************
//! \file    src/Base/AccumulatorKey.h
//! \author  Jozsef Bakosi, Mark A. Christon
//! \date    Thu Mar 1 11:13:34 2012
//! \brief   Accumulator Key
//******************************************************************************
#ifndef AccumulatorKey_h
#define AccumulatorKey_h

#include <string>
#include <iostream>
#include <MPIWrapper.h>

namespace Hydra {

struct AccumulatorKey;

//! Typedef for statistics accumulator wrapper functions
typedef void (*AccumulatorFunc)(void* obj,
                                const AccumulatorKey&,
                                const Real dt);

//! Statistics Accumulator key
struct AccumulatorKey {
  DataIndex accId;         // accumulator index
  DataIndex varId;         // accumulator variable data index
  string name;             // accumulator name
  bool cenMom;             // true if central moment, false if ordinary moment
  int Id;                  // local (on-proc.) node, element or internal set Id
  void* cPtr;              // owner class of accumulator function
  AccumulatorFunc aFunc;   // accumulator wrapper function pointer
  vector<DataIndex> inst;  // vector of instantaneous variables needed

  //! "<" operator for comparator in accumulator map
  bool operator < (const AccumulatorKey& key) const {
    // test on everything except accId & varId
    if (name < key.name) {
      return true;
    } else if (name == key.name && cenMom < key.cenMom) {
      return true;
    } else if (name == key.name && cenMom == key.cenMom && Id < key.Id) {
      return true;
    } else if (name == key.name && cenMom == key.cenMom && Id == key.Id &&
               cPtr < key.cPtr) {
      return true;
    } else if (name == key.name && cenMom == key.cenMom && Id == key.Id &&
               cPtr == key.cPtr && aFunc < key.aFunc) {
      return true;
    } else if (name == key.name && cenMom == key.cenMom && Id == key.Id &&
               cPtr == key.cPtr && aFunc == key.aFunc && inst < key.inst) {
      return true;
    } else {
      return false;
    }
  }

#ifdef STAT_DEBUG
  void echo(const string prefix) const {
    p0cout << prefix << "AccumulatorKey : " <<
                        "(accId = " << this->accId << "), " <<
                        "(varId = " << this->varId << "), " <<
                        "name = " << this->name << ", " <<
                        "cenMom = " << this->cenMom << ", " <<
                        "Id = " << this->Id << ", " <<
                        "cPtr = " << this->cPtr << ", " <<
                        "aFunc = " << (void*)(this->aFunc) << ", " <<
                        "inst = ";
    vector<DataIndex>::const_iterator it;
    for (it=inst.begin(); it!=inst.end(); it++) {
      if (it!=inst.end()-1) {
        p0cout << *it << ",";
      } else {
        p0cout << *it;
      }
    }
    p0cout << endl;
  }
#endif // STAT_DEBUG
};

}

#endif
