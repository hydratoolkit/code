//******************************************************************************
//! \file    src/Base/UnsPhysics.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Base class for deriving unstructured-mesh physics
//!
//! The UnsPhysics class is used to develop a physics-specific solver and
//! a specific solution algorithm using an unstructured mesh and
//! associated spatial discretization.
//******************************************************************************
#ifndef UnsPhysics_h
#define UnsPhysics_h

#include <Control.h>
#include <fileIO.h>
#include <UnsMesh.h>
#include <DualEdge.h>
#include <Physics.h>

namespace Hydra {

#define MAXNNPE    8    //!< Max. no. of nodes per element
#define MAXNVQPT   8    //!< Max. no. of volume quadrature points
#define MAXNVSSQPT 1000 //!< Max. no of super-sample volume quadrature points
#define MAXNSQPT   4    //!< Max. no. of surface quadrature points
#define MAXNSPE    6    //!< Max. number of surfaces per element
#define MAXQDIM    3    //!< Max. dimensionality for quadrature (3-D)

//! Hash table for surface extraction
typedef struct _HashTab {
   unsigned int hashv;
   struct _HashTab *next;
   struct _HashTab *prev;
} HashTab;

//! Virtual base class used to derive the solution algorithms for a given physics
class UnsPhysics : public Physics {

  public:

    //! \name Constructor/Destructor
    //@{
             UnsPhysics();
    virtual ~UnsPhysics() {}
    //@}

    //**************************************************************************
    //! \name Virtual UnsPhysics Interface
    //! These functions are virtual and are implemented for each specific
    //! type of physics being solved with the toolkit.
    //@{
    //! Virtual setup function -- implemented for each specific physics
    virtual void setup();

    //! \brief Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    virtual void registerData();

    //! \brief Virtual Read Restart
    //! Each physics that is implemented will require its own
    //! restart reader. The restart reader function is implemented
    //! for each physics class to enable custom restart readers.
    virtual void readRestart();

    //! \brief Virtual Object Storage
    //! Each physics based on a specific mesh type can store off
    //! it's mesh pointer, control pointer and i/o pointer
    virtual void setObj(UnsMesh* mesh, Control* control, fileIO* io,
                        Timer* timer);

    //! No setObj(StrMesh*, ...) in UnsPhysics
    virtual void setObj(StrMesh*, Control*, fileIO*, Timer*) {
      cout << "!!! No setObj(StrMesh*, ...) in UnsPhysics !!!" << endl;
    }

    //! \brief Virtual data migration
    //! Each physics that is implemented will require identification
    //! of the variables that will be migrated during dynamic
    //! load-balancing operations.
    virtual void setupMigrationVars(vector<DataIndex>& elem_var,
                                    vector<DataIndex>& node_var);

    //! \brief Report memory for a specific physics
    //! Each physics implements it's own memory report based on the allocation
    //! required for the specific discretization and algorithm.
    virtual void reportMemory();

    //! \brief Compute a detailed measurement of memory allocation
    //! Each physics implemments it's own measurement of memory allocation.
    virtual void computeMemory();

    //! \brief Print a detailed summary of memory allocation
    //! Each physics implements it's own output format of memory allocation.
    virtual void printMemory(ostream& ofs);
    //@}
    //**************************************************************************

    //**************************************************************************
    //! \name Linear Algebra Utility Functions
    //! These utiliy functions are used to setup the storage
    //! for the nodal linear algebra and for the FEM assembly process.
    //@{
    //! \brief Calculate the reverse connectivity
    //! Calculate the reverese connectivity using the element connectivity.
    //! \param[in] nRevcount
    //!   Number of reverse connectivity entries
    //! \param[out] elNode
    //! \param [out] locElem
    //!  \param [out] revConn
    void calcReverseConnectivity(int nRevcount, int* elNode, int*locElem,
                                 int* revConn);

    //! \brief Calculate the size of the reverse connectivity
    //! \param[in] elNode
    int calcReverseConnectivitySize(int* elNode);

    //! Compute the nodal 1/2-bandwidth and associated parameters
    void calcNodalBandwidth();

    //! \brief Calculate the maximum number of non-zero values for diagonal and
    //! off-diagonal submatrices on each processor.
    //!
    //! These values are passed to the Petsc function MatCreateMPIAIJ:
    //! Creates a sparse parallel matrix in AIJ format (the default parallel
    //! PETSc format). For good matrix assembly performance the user should
    //! preallocate the matrix storage by setting the parameters d_nz (or d_nnz)
    //! and o_nz (or o_nnz). By setting these parameters accurately, performance
    //! can be increased by more than a factor of 50." Here d_nz is the max
    //! number of non-zero entrie in a row in the diagonal block submatrix and
    //! o_nz is the analog for off-diagnal submatrices.
    //!
    //! \param[in]  nodeEqStart
    //!   Lowest equation id for which this proc is responsible
    //! \param[in]  nodeEqEnd
    //!   Highest equation id for which this proc is responsible
    //! \param[in]  elNode
    //!   Array that gives the number of elements attached to the given node
    //! \param[in]  locElem
    //!   Starting location in the reverse connection lookup array
    //! \param[in]  revConn
    //!   Array that gives the elements attached to the given node
    //! \param[out] mxNdrow
    //!   Max number of nonzeros per row in the DIAGONAL portion of local
    //!   submatrix
    //! \param[out] mxNdrowOff
    //!   Max number of nonzeros per row in the OFF-DIAGONAL portion of local
    //!   submatrix
    void calcNodeRowSize(int nodeEqStart, int nodeEqEnd, int* elNode,
                         int* locElem, int* revConn,
                         int& mxNdrow, int& mxNdrowOff, DataIndex NODEEQ_MAP);

    //! \brief Calculate the maximum number of non-zero values for diagonal and
    //! off-diagonal submatrices on each processor.
    //!
    //! These values are passed to the Petsc function MatCreateMPIAIJ:
    //! Creates a sparse parallel matrix in AIJ format (the default parallel
    //! PETSc format). For good matrix assembly performance the user should
    //! preallocate the matrix storage by setting the parameters d_nz (or d_nnz)
    //! and o_nz (or o_nnz). By setting these parameters accurately, performance
    //! can be increased by more than a factor of 50." Here d_nz is the max
    //! number of non-zero terms on a row in the diagonal submatrix and o_nz
    //! is the analog for off-diagnal submatrices.
    //!
    //! \param[in]  nodeEqStart
    //!   Lowest equation id for which this proc is responsible
    //! \param[in]  nodeEqEnd
    //!   Highest equation id for which this proc is responsible
    //! \param[in]  elNode
    //!   Array that gives the number of elements attached to the given node
    //! \param[in]  locElem
    //!   Starting location in the reverse connection lookup array
    //! \param[in]  revConn
    //!   Array that gives the elements attached to the given node
    //! \param[out] mxNdrow
    //!   Max number of nonzeros per row in the DIAGONAL portion of local
    //!   submatrix
    //! \param[out] mxNdrowOff
    //!   Max number of nonzeros per row in the OFF-DIAGONAL portion of local
    //!   submatrix
    //! \param[out] d_nnz
    //!   number of nonzeros per row in the DIAGONAL
    //! \param[out] o_nnz
    //!   number of nonzeros per row in the OFF-DIAGONAL
    void calcNodeRowSize(int nodeEqStart, int nodeEqEnd, int* elNode,
                         int* locElem, int* revConn,
                         int& mxNdrow, int& mxNdrowOff,
                         vector<int>& d_nnz, vector<int>& o_nnz,
                         DataIndex NODEEQ_MAP);

    //! Echo nodal statistics, e.g., nodal 1/2 bandwidth
    void echoNodalStats(ostream& ofs);

    //! Setup the edge-to-node connectivity
    void EdgeToNode();

    //! Setup nodal equation numbers
    void setupNodeEqNumber(int& nodeEqStart, int& nodeEqEnd, int& Nnp_eq,
                           int* nproc, int* nodePbc, int* nodeeqmap);

    //! Assign processor ownership to boundary conditions
    void assignNodeBCOwnership(int* send_buf, int* recv_buf, int* nproc,
                               int* nodebc);

    //! Initialize processor boundary condition ownership array
    void initNodeBCOwnership(int* nproc, int* nodebc);

    //! Map equation variables to nodal variables
    void mapEqVarToNdVar(int nodeEqStart, int nodeEqEnd, int* nodeeqmap,
                         Real* eqVar, DataIndex NDVAR,
                         DataIndex SEND_BUF, DataIndex RECV_BUF);

    //! Map nodal variables to equation variables
    void mapNdVarToEqVar(int nodeEqStart, int nodeEqEnd,int* nodeeqmap,
                         Real* eqVar, DataIndex NDVAR);
    //@}
    //**************************************************************************

    //**************************************************************************
    //! \name Topology Extraction/Setup Functions
    //! These utiliy functions are used to calculate the adjacency
    //! between elements and setup the relevant face data structures.
    //@{

    //! Compute the dual-edge data for all element classes
    void calcDualEdgeData();

    //! Setup the element-to-element adjacency hash map
    void setupElementAdjacency(UnsHashMap& hmap);

    //! Setup the dual-edge data structures
    void setupDualEdges(UnsHashMap& hmap);
    //@}
    //**************************************************************************

    //**************************************************************************
    //! \name Output Field Delegates & Utility Routines
    //! These functions are used to interpolate, lift, and output field data.
    //@{
    //! Interpolate a scalar nodal field to get element-centroid values
    void interpElemField(DataIndex node_field, DataIndex elem_field);

    //! Interpolate a scalar nodal field to get element-centroid values
    void interpElemField(Real* nvar, Real* evar);

    //! Form LHS/RHS to project element field to node field
    void formVolProjectionID(Real* elem_var,
                             Real* node_var,
                             Real* inverse_distance_wf);

    //! Form LHS/RHS to project element field to node field accounting for IFCs
    void formVolProjectionIFC(Real* elem_var,
                              Real* node_var,
                              Real* inverse_distance_wf);

    //! Solve the inverse-distance projection
    void solveProjID(Real* node_var, Real* inverse_distance_wf,
                     DataIndex SEND_BUF, DataIndex RECV_BUF);

    //! Write out the MPI processor rank
    void writeElemProcessorIdField(const OutputDelegateKey& key, int plnum,
                                   int varId);

    //! Write out a scalar field
    void writeScalarField(const OutputDelegateKey& key, int plnum, int varId);

    //! Write out a symmetric tensor field
    void writeSymTensorField(const OutputDelegateKey& key, int plnum, int varId);

    //! Write out a vector field
    void writeVectorField(const OutputDelegateKey& key, int plnum, int varId);
    //@}
    //**************************************************************************

    //**************************************************************************
    //! \name Utilility functions common to all unstructured mesh physics
    //! These functions are shared accross all physics, and provide basic
    //! operation such as computing and checking the volume.
    //@{
    //! Compute element centroids
    void calcCentroid();

    //! Calculate the element volumes
    Real calcTotalVolume();

    //! Calculate the volume of each element
    void calcVolume(DataIndex VOL);

    //! Check element volumes to be sure that there are no malformed elements
    void checkVolume();
    //@}
    //**************************************************************************


  protected:

    //**************************************************************************
    //! \name Objects used - NOT owned
    //@{
    UnsMesh* mesh;     //!< The UnsMesh is an unstructured DataMesh
    Control* control;  //!< The Control object holds all of the control info.
    fileIO*  io;       //!< The fileIO object is used for all I/O operations
    Timer*   timer;    //!< The Timer object for all timing data
    //@}
    //**************************************************************************

    //**************************************************************************
    //! \name Basic mesh information
    //! The number of dimensions, number of elements and number of nodes.
    //@{
    int Ndim;      //!< Number of space dimensions
    int Nel;       //!< Number of elements
    int Nnp;       //!< Number of nodes
    //@}
    //**************************************************************************

    //**************************************************************************
    //! \name Nodal statistics (assumes 1-DOF per node)
    //! These variables are used to keep track of topology
    //! related aspects of the nodes in a mesh
    //@{
    int  Nodebw;   //!< Nodal 1/2-bandwidth
    int  mxNeln;   //!< Max. elements connected to a node
    int  mxNedn;   //!< Max. edges connected to a node
    int  mxNd;     //!< Node where max. occurs
    int  mxNdrow;  //!< Maximum nodal row size
    //@}
    //**************************************************************************

    DataIndex PROCID; //!< Processor MPI Rank [Nel]
    DataIndex VOLUME; //!< Volume             [Nel]

    vector<Real> m_memory; //<! vector to hold memory footprint information

  private:

    //! Don't permit copy or assignment operators
    //@{
    UnsPhysics(const UnsPhysics&);
    UnsPhysics& operator=(const UnsPhysics&);
    //@}

};

}

#endif // UnsPhysics_h
