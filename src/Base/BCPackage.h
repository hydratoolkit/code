//******************************************************************************
//! \file    src/Base/BCPackage.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Package for instantiating BC objects
//******************************************************************************
#ifndef BCPackage_h
#define BCPackage_h

#include <HydraTypes.h>
#include <SetTypes.h>

namespace Hydra {

//! Struct for temporarily holding BC information read in the parser
struct BCPackage {
  int m_setId;           //!< mesh entity set Id in user space
  int m_intId;           //!< Internal sideset Id
  int m_tblid;           //!< table id, -1 if empty
  int m_fieldId;         //!< Field index
  int m_centering;       //!< Centering
  int m_dir;             //!< Dir.: x=GLOBAL_XDIR, y=GLOBAL_YDIR, z=GLOBAL_ZDIR
  int m_offset;          //!< Offset of component for field variable in FSI
  Real m_amp;            //!< amplitude - BC magnitude from keyword data line
  bool m_rotation;       //!< Flag to indicate the rotation matrix is active
  Real m_r[3][3];        //!< Rotation matrix
  MeshSetType m_settype; //!< BC is defined on a set of these mesh entities
  bool m_userBC;         //!< Flag indicating user-defined boundary condition

  BCPackage(): m_setId(-1), m_intId(-1), m_tblid(-1), m_fieldId(-1),
               m_centering(-1), m_dir(-1), m_offset(-1),
               m_rotation(false), m_settype(UNDEFINED_SET), m_userBC(false)
  {
    m_r[0][0] = m_r[0][1] = m_r[0][2] =
    m_r[1][0] = m_r[1][1] = m_r[1][2] =
    m_r[2][0] = m_r[2][1] = m_r[2][2] = 0.0;
  }
};

}

#endif
