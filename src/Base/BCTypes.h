//******************************************************************************
//! \file    src/Base/BCTypes.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Boundary condition types
//******************************************************************************
#ifndef BCTypes_h
#define BCTypes_h

#include <HydraTypes.h>

namespace Hydra {

enum BCType {
    BC_CONSTANT = 0,
    BC_TIME_DEPENDENT,
    BC_SPACE_DEPENDENT,
    BC_SPACETIME_DEPENDENT,
    BC_USER_DEFINED
};

}

#endif // BCTypes_h

