//******************************************************************************
//! \file    src/Base/SetBC.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Base class for mesh entity set BC's (either node or face sets)
//******************************************************************************
#include <cassert>
#include <cmath>
#include <iomanip>
#include <set>

using namespace std;

#include <SetBC.h>

using namespace Hydra;

SetBC::SetBC(const BCPackage& bc, int offset)
/*******************************************************************************
Routine: SetBC - base class for BC's on sidesets
Author : Mark A. Christon
******************************************************************************/
{
  m_ssid = bc.m_setId;             //!< sideset Id in user space
  m_settype = bc.m_settype;        //!< mesh entity type for the mesh entity set
  m_set = bc.m_intId;              //!< Internal sideset Id
  m_tblid = bc.m_tblid;            //!< table Id, -1 if empty
  m_fieldid = bc.m_fieldId;        //!< Field index
  m_amp = bc.m_amp;                //!< Amplitude
  m_centering = bc.m_centering;    //!< Centering
  m_offset = offset;               //!< Element offset
  m_hasNodeList = false;           //!< Has unique node set if true
  m_Nnp = 0;                       //!< Number of unique nodes (not always set)
  UNIQUE_NODE_LIST = UNREGISTERED; //!< Data index for unique node list

  // Set the boundary condition type
  m_type = BC_USER_DEFINED;
  if (!bc.m_userBC) {
    m_type = BC_CONSTANT;
    if (bc.m_fieldId == -1 && bc.m_tblid > -1)
      m_type = BC_TIME_DEPENDENT;
    else if (bc.m_fieldId > -1 && bc.m_tblid == -1)
      m_type = BC_SPACE_DEPENDENT;
    else if (bc.m_fieldId > -1 && bc.m_tblid > -1)
      m_type = BC_SPACETIME_DEPENDENT;
  }
}

void
SetBC::echo(ostream& ofs)
/*******************************************************************************
Routine: echo - echo basic set BC data
Author : Mark A. Christon
*******************************************************************************/
{
  string bctype = "NULL";
  switch(m_type) {
  case BC_CONSTANT:
    bctype = "CONSTANT";
    break;
  case BC_TIME_DEPENDENT:
    bctype = "TIME DEPENDENT";
    break;
  case BC_SPACE_DEPENDENT:
    bctype = "SPACE DEPENDENT";
    break;
  case BC_SPACETIME_DEPENDENT:
    bctype = "SPACE-TIME DEPENDENT";
    break;
  case BC_USER_DEFINED:
    bctype = "USER-DEFINED";
    break;
  }
  ofs << "\t"
      << setw(10) << m_ssid    << "  "
      << setw(23) << bctype    << "  "
      << setw( 5) << m_tblid   << "  "
      << setw(10) << m_amp     << "  "
      << setw( 5) << m_fieldid
      << endl;
}

void
SetBC::echo(ostream& ofs, string& dirLabel)
/*******************************************************************************
Routine: echo - echo basic set BC data with direction label
Author : Mark A. Christon
*******************************************************************************/
{
  string bctype = "NULL";
  switch(m_type) {
  case BC_CONSTANT:
    bctype = "CONSTANT";
    break;
  case BC_TIME_DEPENDENT:
    bctype = "TIME DEPENDENT";
    break;
  case BC_SPACE_DEPENDENT:
    bctype = "SPACE DEPENDENT";
    break;
  case BC_SPACETIME_DEPENDENT:
    bctype = "SPACE-TIME DEPENDENT";
    break;
  case BC_USER_DEFINED:
    bctype = "USER-DEFINED";
    break;
  }
  ofs << "\t"
      << setw(10) << m_ssid    << "  "
      << setw( 4) << dirLabel  << "  "
      << setw(23) << bctype    << "  "
      << setw( 5) << m_tblid   << "  "
      << setw(10) << m_amp     << "  "
      << setw( 5) << m_fieldid
      << endl;
}

int*
SetBC::getUniqueNodeList()
/*******************************************************************************
Routine: getUniqueNodeList - Get list of unique nodes
Author : Mark A. Christon
*******************************************************************************/
{
  assert(UNIQUE_NODE_LIST != UNREGISTERED);
  return getVariable<int>(UNIQUE_NODE_LIST);
}

void
SetBC::setupUniqueNodeList(UnsMesh& mesh)
/*******************************************************************************
Routine: setupUniqueNodeList - Setup the unique node list for a surface bc
Author : Mark A. Christon
*******************************************************************************/
{
  if (m_centering == NODE_CTR) {
    Nodeset* nodeset = mesh.getNodesets();
    int n  = nodeset[m_set].Nnp;
    if (n > 0) {
      UNIQUE_NODE_LIST = registerVariable(n,
                      sizeof(int),
                      "UNIQUE_NODE_LIST");

      int* nlist = mesh.getVariable<int>(nodeset[m_set].NODE_LIST);
      int* nodes = mesh.getVariable<int>(UNIQUE_NODE_LIST);
      memcpy(nodes, nlist, n*sizeof(int));
    }
    m_hasNodeList = true;
    m_Nnp = n;

  } else if (m_centering == FACE_CTR) {

    // This is intended primarily for nodal DOF that use a surface set
    // to apply a prescribed boundary condition.

    // Haul out the surface data for the current set
    Sideset* surf = mesh.getSidesets();
    int nel  = surf[m_set].Nel;

    if (nel == 0) return;

    int  conn[4];
    int lconn[4];

    const int* el_list = mesh.getVariable<const int>(surf[m_set].EL_LIST);
    const int* side_list = mesh.getVariable<const int>(surf[m_set].SIDE_LIST);

    // Map from global element Id to class
    int* cidmap = mesh.getElementClassMap();

    set<int> unique;

    // Trip over all elements in the surface set
    int Ndim = mesh.getNdim();
    for (int i = 0; i < nel; i++) {
      int el  = el_list[i];
      int sd  = side_list[i];
      int cid = cidmap[el];
      Element* ec=mesh.getElementClass(cid);
      int nnpf;
      if (Ndim == TWOD) {
        nnpf = ec->getNodesPerEdge(sd);
      } else {
        nnpf = ec->getNodesPerFace(sd);
      }
      int lid = el - ec->getElementOffset();
      ec->getFaceConnectivity(lid, sd, conn, lconn);
      for (int j = 0; j < nnpf; j++)
    unique.insert(conn[j]);
    }

    m_hasNodeList = true;
    m_Nnp = unique.size();
    if (m_Nnp > 0) {
      UNIQUE_NODE_LIST = registerVariable(m_Nnp,
                      sizeof(int),
                      "UNIQUE_NODE_LIST");
      int* node_list = getVariable<int>(UNIQUE_NODE_LIST);
      int nd = 0;
      set<int>::const_iterator iter;
      for (iter = unique.begin(); iter != unique.end(); iter++, nd++)
    node_list[nd] = *iter;
    }
  }
  else {
    assert("Unknown bc centering in setupUniqueNodeList" == 0);
  }
}
