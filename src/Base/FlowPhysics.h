//******************************************************************************
//! \file    src/Base/FlowPhysics.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Virtual base class used to construct the solution algorithms for
//!          a given flow physics, e.g., advection-diffusion, Navier-Stokes
//******************************************************************************
#ifndef FlowPhysics_h
#define FlowPhysics_h

#include <Control.h>
#include <fileIO.h>
#include <UnsMesh.h>
#include <UnsPhysics.h>

namespace Hydra {

//! Flow physics class for 2-D FEM flow problems
class FlowPhysics : public UnsPhysics {

  public:
             FlowPhysics() {}
    virtual ~FlowPhysics() {}

    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData();
    //@}

    //! \name Virtual Flow Physics Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    //! Virtual setup function -- implemented for each specific physics
    virtual void setup();
    //@}

    //! \name Stream function & vorticity
    //! These functions are used to compute the stream-function and
    //! vorticity for graphics output.
    //@{
    void calcEdge2D();     //!< Compute the edge order for the stream function

    void calcStreamFunc(); //!< Traverse the edges to compute the stream func.

    void calcVorticity(DataIndex TMP, DataIndex Z);  //!< Compute the vorticity
    //@}

  protected:

    //! \name Data registered for flow physics
    //! All of the basic variables for flow problems such as
    //! advection-diffusion and Navier-Stokes is registered here.
    //@{
    DataIndex VELX;      //!< Y-velocity component
    DataIndex VELY;      //!< X-velociity component
    DataIndex PSI;       //!< Stream function
    DataIndex PSI_EL;    //!< Element, edge for stream function computation
    DataIndex PSI_EDGE;  //!< Edges for stream function (PSI)
    DataIndex VORTICITY; //!< Vorticity (z-component)
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    FlowPhysics(const FlowPhysics&);
    FlowPhysics& operator=(const FlowPhysics&);
    //@}

    int Npsi;  //!< No. of 2-D elements for stream function

};

}

#endif // FlowPhysics_h
