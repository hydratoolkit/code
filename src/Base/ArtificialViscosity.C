//******************************************************************************
//! \file    src/Base/ArtificialViscosity.C
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Base class for artificial viscosity methods
//******************************************************************************

#include <cmath>
#include <iostream>
#include <cstdlib>

using namespace std;

#include <HydraTypes.h>
#include <ArtificialViscosity.h>

using namespace Hydra;

ArtificialViscosity::ArtificialViscosity(int Id, Real linear, Real quadratic)
/*******************************************************************************
Routine: ArtificialViscosity - constructor
Author : Mark A. Christon
*******************************************************************************/
{
  m_Id = Id;
  m_linear = linear;
  m_quadratic = quadratic;
}
