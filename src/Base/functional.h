//******************************************************************************
//! \file       functional.h
//! \author     Nathan Barnett
//! \date       Nov 21, 2011
//! \brief      Provides functors and the like for operations on stl containers
//******************************************************************************
#ifndef FUNCTIONAL_H
#define FUNCTIONAL_H

namespace Hydra {

//! \brief Package two unary functors together with an operator functor.
//!
//! An example would be creating the expression f(i){ return (i<5 && i>1); }
//! to be used in an stl algorithm or otherwise
template <class OP1, class OP2, class OP3>
class compose1_t :
    public std::unary_function<typename OP2::argument_type, typename OP1::result_type>
{
private:
  OP1 op1; // process: op1(op2(x),op3(x))
  OP2 op2;
  OP3 op3;
public:
  // constructor
  compose1_t(const OP1& o1, const OP2& o2, const OP3& o3)
      : op1(o1), op2(o2), op3(o3) {
  }

  // function call
  typename OP1::result_type
  operator()(const typename OP2::argument_type& x) const {
    return op1(op2(x),op3(x));
  }
};

//! Convenience function for the compose1 adapter
template <class OP1, class OP2, class OP3>
inline compose1_t<OP1,OP2,OP3>
compose1(const OP1& o1, const OP2& o2, const OP3& o3) {
  return compose1_t<OP1,OP2,OP3>(o1,o2,o3);
}

//! Translate an stl array using a table for lookup values and storing the
//! results in-place
template<class T>
struct translate{
  translate(T* translationArray) : m_translationArray(translationArray) {}
  T operator() (unsigned int i) {return m_translationArray[i];}
private:
  T* m_translationArray;
};

} // end of Hydra namespace

#endif // FUNCTIONAL_H

