//******************************************************************************
//! \file    src/Base/DataContainerEntry.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   DataContainer entry for variable registration
//******************************************************************************
#ifndef DATACONTAINERENTRY_H
#define DATACONTAINERENTRY_H

#include <string>

namespace Hydra {

//! Define value for unregistered data index
#define UNREGISTERED -1

//! Maximum number of entry dimensions
#define MAX_DIMS 2

enum VariableCentering {NO_CTR = 0,      //!< Generic data type, not plottable
                        DUAL_EDGE_CTR,   //!< Dual edge, i.e., face
                        FACE_CTR,        //!< Same as dual edge
                        EDGE_CTR,        //!< Unique element edges
                        ELEMENT_CTR,     //!< Element-centered
                        NODE_CTR,        //!< Node-centered
                        NUM_VARIABLE_CENTERING
};

enum VariableType {SCALAR_VAR = 0,  //!< Scalar quantity
                   VECTOR_VAR,
                   ANTITENSOR_VAR,
                   SYMTENSOR_VAR,
                   TENSOR_VAR,
                   QUATERNION_VAR,
                   SET_VAR,         //!< Node/element/side set/etc.
                   GENERIC_VAR,     //!< Non-specific variable type
                   NUM_VARIABLE_TYPE
};

// Layout for multi-dimensional arrays
enum MemoryLayout {LINEAR_ARRAY = 0,
                   COLUMN_MAJOR_ARRAY,
                   ROW_MAJOR_ARRAY,
                   NUM_MEMORY_LAYOUT
};

//! Data container entry holds the data associated with a registered variable
 class DataContainerEntry {

    friend class DataContainer;

  public:

    //! \name Constructor/Destructor
    //@{
    DataContainerEntry(size_t number,
                       size_t size,
                       string name,
                       VariableCentering centering,
                       VariableType type,
                       MemoryLayout layout,
                       bool plot,
                       bool restart,
                       bool mapped,
                       void* ptr) :
      m_plot(plot),
      m_restart(restart),
      m_mapped(mapped),
      m_number(number),
      m_size(size),
      m_type(type),
      m_centering(centering),
      m_layout(layout),
      m_ptr(ptr)
        { m_name = name; }

      ~DataContainerEntry() {}
    //@}

  private:

      //! Don't permit copy or assignment operators
      //@{
      DataContainerEntry(const DataContainerEntry&);
      DataContainerEntry& operator=(const DataContainerEntry&);
      //@}

      bool              m_plot;      //!< Variable can be plotted
      bool              m_restart;   //!< Write to restart (or not)
      bool              m_mapped;    //!< Mapped relative to another DataIndex
      size_t            m_number;    //!< # items per dim.
      size_t            m_size;      //!< Object/Data sizes
      VariableType      m_type;      //!< Variable classification
      VariableCentering m_centering; //!< Variable grid
      MemoryLayout      m_layout;    //!< Memory layout
      string            m_name;      //!< Object/Data names
      void*             m_ptr;       //!< Pointer to data/object
  };


//! Temporary DataContainerEntry class
class DataContainerTemporary {

  friend class DataContainer;

  private:

    //! \name Constructor/Destructor
    //@{
    DataContainerTemporary(size_t nbytes,
                           const char filename[],
                           int lineno,
                           void* ptr) :
      m_nbytes(nbytes),
      m_lineno(lineno),
      m_filename(filename),
      m_ptr(ptr) {}

    ~DataContainerTemporary() { }
    //@}

    //! Don't permit copy or assignment operators
    //@{
    DataContainerTemporary(const DataContainerTemporary&);
    DataContainerTemporary& operator=(const DataContainerTemporary&);
    //@}

    size_t m_nbytes;
    int    m_lineno;
    string m_filename;
    void*  m_ptr;
};

}

#endif // DATACONTAINERENTRY_H
