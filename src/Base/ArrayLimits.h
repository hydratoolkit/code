//******************************************************************************
//! \file    src/Base/ArrayLimits.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Compute min/max for arrays of various data shapes
//******************************************************************************

#ifndef ArrayLimits_h
#define ArrayLimits_h

#include <DataShapes.h>

namespace Hydra {

//! ArrayLimits provides a convenient interface for array limits
namespace ArrayLimits {

  //! Min/Max bounding scalar values
  //! \param[in]  var     Vector of scalars whose limits are computed
  //! \param[out] varmin  Minimum
  //! \param[out] varmax  Maximum
  //! \param[in]  Nvar    Number of items in the vector
  void getVarLimits(const Real* const var,
                    Real& varmin,
                    Real& varmax,
                    const int Nvar);

  //! Min/Max bounding vector values
  //! \param[in]  var     Vector of Vectors whose limits are computed
  //! \param[out] varmin  Vector of minima
  //! \param[out] varmax  Vector of maxima
  //! \param[in]  Nvar    Number of items in the vector
  void getVarLimits(const CVector& var,
                    Vector& varmin,
                    Vector& varmax,
                    const int Nvar);

  //! Min/Max bounding tensor values
  //! \param[in]  var     Vector of Tensors whose limits are computed
  //! \param[out] varmin  Tensor of minima
  //! \param[out] varmax  Tensor of maxima
  //! \param[in]  Nvar    Number of items in the vector
  void getVarLimits(const CTensor& var,
                    Tensor& varmin,
                    Tensor& varmax,
                    const int Nvar);

  //! Max scalar value
  //! \param[in]  var     Vector of scalars whose maximum is computed
  //! \param[out] varmax  Maxium
  //! \param[in]  Nvar    Number of items in the vector
  void getVarMax(const Real* const var,
                 Real& varmax,
                 const int Nvar);

  //! Min scalar value
  //! \param[in]  var     Vector of scalars whose maximum is computed
  //! \param[out] varmin  Minimum
  //! \param[in]  Nvar    Number of items in the vector
  void getVarMin(const Real* const var,
                 Real& varmin,
                 const int Nvar);

} // namespace ArrayLimits

} // namespace Hydra

#endif // ArrayLimits_h
