//******************************************************************************
//! \file    src/Base/Table.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   The table class provides simple data interpolation in tabular data
//******************************************************************************

#ifndef Table_h
#define Table_h

#include <string>
#include <vector>
#include <HydraTypes.h>

namespace Hydra {

// Table class for interplation of tabular data
class Table {

  public:

    //! \name Constructor/Destructor
    //@{
             Table() {}
             Table(int tblid, const vector<Real> lct,
                              const vector<Real> lcv);
             Table(int tblid, const vector<Real> lct,
                              const vector< vector<Real> > lcv);
    virtual ~Table();
    //@}

    //! \name table interface
    //@{
    int  Id()             {return id;  } //!< Return the table id
    int  size()           {return Npts;} //!< Return the table size
    Real getMinTime()     {return tmin;} //!< Return minimum time value
    Real getMaxTime()     {return tmax;} //!< Return maximum time value
    Real getMinVar()      {return vmin;} //!< Return minimum variable value
    Real getMaxVar()      {return vmax;} //!< Return maximum variable value

    Real lookup(Real t);                 //!< Lookup data
    Real calcDeriv(Real t);              //!< Calculate derivative at time=t

    //! Echo table options
    void echoOptions(vector<string>& columns, ostream& ofs);
    //@}

  protected:

  private:

    //! Don't permit copy or assignment operators
    //@{
    Table(const Table&);
    Table& operator=(const Table&);
    //@}

    int   id;    //!< Table id
    int   Npts;  //!< Number of points
    Real  value; //!< table value returned from lookup function
    Real  tmin;  //!< Minimum time value for table
    Real  tmax;  //!< Maximum time value for table
    Real  vmin;  //!< Minimum variable value for table
    Real  vmax;  //!< Maximum variable value for table
    Real  dtmin; //!< Minimum time step in the table

    vector<Real> time;  //!< Array of independent variables
    vector<Real> val;   //!< Array of dependent   variables

    vector< vector<Real> > valn;
};

}

#endif // Table_h
