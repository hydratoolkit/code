//******************************************************************************
//! \file    src/Base/SetBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Base class used to derive physics-specific set based BC's
//******************************************************************************
#ifndef SetBC_h
#define SetBC_h

#include <HydraTypes.h>
#include <BCPackage.h>
#include <BCTypes.h>
#include <UnsMesh.h>

namespace Hydra {

//! This is the base class for all sideset-based boundary conditions
class SetBC : public DataContainer {

  public:

    //! \name Constructor/Destructor
    //@{
             SetBC() {}
             SetBC(const BCPackage& bc, int offset);
    virtual ~SetBC() {}
    //@}

    //**************************************************************************
    //! \name Virtual SetBC Interface
    //! These functions are virtual and are implemented for each specific
    //! type of set-based boundary condition.
    //@{
    //! Echo one liner for a set-based BC
    virtual void echo(ostream& ofs);

    //! Echo one liner for a set-based BC w. direction, e.g., vector component
    virtual void echo(ostream& ofs, string& dirLabel);

    //! Setup the unique node list
    virtual void setupUniqueNodeList(UnsMesh& mesh);

    //! Set a user-defined BC value
    virtual Real setUserValue() {
      cout << "No default implementation of SetBC::setUserValue()" << endl;
      return 0.0;
    }
    //@}

    //! \name SetBC access functions
    //! The base SetBC class is setup to use a mesh entity set id,
    //! a time (or parameter based) table identified by
    //! a table id and with an associated amplitude.
    //! The amplitude is generic and is used as an amplitude
    //! multiplier on all tables constant or otherwise.
    //@{
    void setId(int id)        {m_ssid = id;}    //!< Set the mesh entity set Id
    void setSet(int id)       {m_set = id;}     //!< Set the internal set Id
    void setSetType(MeshSetType settype) {      //!< Set the mesh entity type
      m_settype = settype;
    }
    void setTableId(int id)   {m_tblid = id;}    //!< Set the table Id
    void setFieldId(int id)   {m_fieldid = id;}  //!< Set the field Id
    void setAmp(Real val)     {m_amp  = val;}    //!< Set table amplitude
    void setNumber(int num)   {m_set  = num;}    //!< Set the internal id
    void setType(BCType type) {m_type = type;}   //!< Set the BC type

    BCType getType()   {return m_type;}          //!< Get the BC type
    int  getId()       {return m_ssid;}          //!< Get the mesh entity set Id
    MeshSetType getSetType() {return m_settype;} //!< Get the mesh entity type
    int  getSet()      {return m_set; }          //!< Get the internal set Id
    int  getFieldId()  {return m_fieldid;}       //!< Get the field Id
    int  getTableId()  {return m_tblid;}         //!< Get the table Id
    Real getAmp()      {return m_amp; }          //!< Get table amplitude
    //@}

    //! Get number of nodes in unique node list
    int getNumberUniqueNodes() const {return m_Nnp;}

    //! Get pointer to list of unique nodes
    int* getUniqueNodeList();

  protected:

    //! \name SetBC data
    //! This data is used in the inhereted physics-specific boundary
    //! conditions.  So, the amplitude is interpreted in each of
    //! the physics-specific cases accordingly.  For example,
    //! in the HeatFluxBC, the amplitude (amp) represents the
    //! heat flux amplitude or q_n.  In the convection bc,
    //! it represents the convective heat transfer coefficient.
    //@{
    int m_ssid;            //!< internal sideset Id
    MeshSetType m_settype; //!< mesh entity type for the mesh entity set
    int m_set;             //!< user sideset Id
    int m_tblid;           //!< table id
    int m_fieldid;         //!< field Id
    Real m_amp;            //!< amplitude
    BCType m_type;         //!< BC type
    int m_centering;       //!< Centering on the grid, e.g., NODE_CTR
    int m_offset;          //!< Offset for elements

    //! Unique node list when required for some BC's
    bool m_hasNodeList;    //!< Indicates a unique node list is present
    int m_Nnp;             //!< Number of unique nodes in the list
    DataIndex UNIQUE_NODE_LIST; //<! DataIndex for the unique nodes
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    SetBC(const SetBC&);
    SetBC& operator=(const SetBC&);
    //@}

};

}

#endif // SetBC_h
