//******************************************************************************
//! \file    src/Base/SourceTypes.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Source types
//******************************************************************************
#ifndef SourceTypes_h
#define SourceTypes_h

#include <HydraTypes.h>

namespace Hydra {

enum SourceType {
  SRC_CONSTANT = 0,
  SRC_TIME_DEPENDENT,
  SRC_SPACE_DEPENDENT,
  SRC_SPACETIME_DEPENDENT,
  SRC_USER_DEFINED
};

}

#endif // SourceTypes_h

