//******************************************************************************
//! \file    src/Base/PeriodicSideBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   PeriodicSideBC is used to derive physics-specific periodic BC's
//******************************************************************************
#ifndef PeriodicSideBC_h
#define PeriodicSideBC_h

#include <BCTypes.h>
#include <HydraTypes.h>
#include <DataContainer.h>
#include <UnsMesh.h>

namespace Hydra {

//! This is the base class for all sideset-based periodic BC's
class PeriodicSideBC : public DataContainer {

  public:

    //! Transformation types
    enum TransformType {
      TRANSLATE,   //!< Perform translation to match sets
      ROTATE       //!< Perform rotation to match sets
    };

    //! \name Constructor/Destructor
    //@{
             PeriodicSideBC(int master_ssid, int master_set,
                            int slave_ssid,  int slave_set,
                            TransformType transform, int offset);
    virtual ~PeriodicSideBC() {}
    //@}

    //! \name PeriodicSideBC access functions
    //! The base PeriodicSideBC class is setup to use a master and a slave
    //! sideset.  The internal and external Id's for the master and slave
    //! are stored.
    //@{
    //! Get the master side-set Id (user)
    int getMasterId() {return m_master_ssid;}

    //! Get the master side-set Id (internal)
    int getMasterSet() {return m_master_set;}

    //! Get the slave side-set Id (user)
    int getSlaveId() {return m_slave_ssid;}

    //! Get the slave side-set Id (internal)
    int getSlaveSet() {return m_slave_set;}
    //@}

    //! Setup the unique node lists for master/slave sidesets
    virtual void setupUniqueNodeList(UnsMesh& mesh);

    //! Setup the unique node lists in a sideset
    int setupSidesetNodeList(UnsMesh& mesh, int setId,
                             DataIndex& UNIQUE_NODE_LIST,
                             string listname);

    void matchSets(UnsMesh& mesh);

    //! Get number of nodes in unique node list
    int getNumberUniqueNodes() const {return m_master_Nnp;}

    //! Driver to check the external edges for master/slave sidesets
    int checkExternalEdges(UnsMesh& mesh, int extNedge);

    //! Check the external edges, return false if interior edge exists
    int checkSidesetExternalEdges(UnsMesh& mesh, int extNedge, int setId);

    //! Calculate the sideset parameters: centroid, hmin, displacement
    void calcSetParam(UnsMesh& mesh);

  protected:

    //! \name PeriodicSideBC data
    //! This data is used in the inhereted physics-specific boundary
    //! conditions.
    //@{
    BCType m_type;     //!< Boundary condition type
    int m_master_ssid; //!< master internal sideset id
    int m_master_set;  //!< master internal sideset number
    int m_slave_ssid;  //!< slave  internal sideset id
    int m_slave_set;   //!< slave  internal sideset number
    int m_centering;   //!< Centering on the grid, e.g., NODE_CTR
    int m_offset;      //!< Offset for elements

    //! Unique node list when required for some BC's
    bool m_hasNodeList;  //!< Indicates that unique node lists are in use
    int m_master_Nnp;    //!< Number of nodes in the unique master list
    int m_slave_Nnp;     //!< Number of nodes in the unique slave list

    Real m_h_min;        //!< Minimum length scale on sets
    Real m_eps;          //!< Tolerance for node matching

    Vector m_master_cent; //! Centroid for master surface
    Vector m_slave_cent;  //! Centroid for slave  surface
    Vector m_disp;        //! Displacement for translational periodicity

    TransformType m_transform; //!< Transformation type

    DataIndex MASTER_UNIQUE_NODE_LIST;
    DataIndex SLAVE_UNIQUE_NODE_LIST;
    DataIndex MASTER_SLAVE_LIST;  //!< Master Id as a function of Slave Id?
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    PeriodicSideBC(const PeriodicSideBC&);
    PeriodicSideBC& operator=(const PeriodicSideBC&);
    //@}

};

}

#endif // PeriodicSideBC_h
