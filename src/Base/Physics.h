//******************************************************************************
//! \file    src/Base/Physics.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Physics base class for deriving physics-specific solvers
//******************************************************************************
#ifndef Physics_h
#define Physics_h

#include <map>

#include <Control.h>
#include <fileIO.h>
#include <Timer.h>
#include <StrMesh.h>
#include <UnsMesh.h>

namespace Hydra {

//! Virtual base class used to construct solution algorithms for a given physics
class Physics {

  public:

    //! \name Constructor/Destructor
    //@{
             Physics() {}   //!< Constructor for Physics class
    virtual ~Physics() {}   //!< Destructor  for Physics class
    //@}

    //**************************************************************************
    //! \name Virtual Physics Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    //! Setup -- implemented for each specific physics
    virtual void setup() {
      cout << "!!! No default physics setup !!!" << endl; }

    //! Initialize -- implemented for each specific physics
    virtual void initialize() {
      cout << "!!! No default physics initialize" << endl; }

    //! Finalize -- implemented for each specific physics
    virtual void finalize() {
      cout << "!!! No default physics finalize !!!" << endl; }

    //! Solve -- implemented for each specific physics
    virtual void solve() {
      cout << "!!! No default physics solve !!!" << endl; }
    //@}

    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData() {
      cout << "!!! No default data registration !!!" << endl; }
    //@}

    //! \name Virtual Memory Report
    //! Each physics that is implemented will require its own
    //! function that collects and report its memory usage.
    //@{
    virtual void reportMemory() {
      cout << "!!! No default memory report !!!" << endl; }
    //@}

    //! \name Virtual Object Storage
    //! Each physics based on a specific mesh type can store off
    //! it's mesh pointer, control pointer, i/o pointer and timer pointer
    //@{
    virtual void setObj(UnsMesh* mesh, Control* control, fileIO* io,
                        Timer* timer) = 0;

    virtual void setObj(StrMesh* mesh, Control* control, fileIO* io,
                        Timer* timer) = 0;
    //@}

    //! \name Virtual Functions for I/O
    //! These functions are implemented for each specific physics
    //! to permit custom options to be coded for reading and writing
    //! restart files, time-history data, bc's, etc.
    //@{
    virtual void writeSolving() {
      cout << "!!! No default writeSolving !!!" << endl;}

    virtual void echoOptions(ostream& ofs) {
      ofs << "!!! No default echoOptions !!!" << endl;}

    //! \name Virtual Read Restart
    //! Each physics that is implemented will require its own
    //! restart reader. The restart reader function is implemented
    //! for each physics class to enable custom restart readers.
    //@{
    virtual void readRestart() {
      cout << "!!! No default readRestart !!!" << endl;}
    //@}

    //! \brief Virtual function for defining variables for data migration
    //! Each physics that is implemented will require identification
    //! of the variables that will be migrated during dynamic
    //! load-balancing operations.
    //@{
    virtual void setupMigrationVars(vector<DataIndex>& elem_var,
                                    vector<DataIndex>& node_var) = 0;

    //! \brief Virtual setup merged-set BC's
    //! Each physics that is implemented will require a final step
    //! to resolve the internal set Id associated with a BC and a merged
    //! sideset or nodeset.
    virtual void setupMergedSetBCs() = 0;
    //@}
    //**************************************************************************

  protected:

    //! \name Parallel data stored for convenience
    //{@
    int pid;    //!< processor number (rank) in the communicator
    int Nproc;  //!< No. of processors
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    Physics(const Physics&);
    Physics& operator=(const Physics&);
    //@}

};

}

#endif // Physics_h
