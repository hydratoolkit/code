//******************************************************************************
//! \file    src/Base/DataContainer.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Base-class data container for deriving meshes, element classes, etc.
//******************************************************************************

#ifndef DataContainer_h
#define DataContainer_h

#include <cstring>
#include <stdint.h>

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

#include <HydraTypes.h>
#include <DataContainerEntry.h>
#include <DataShapes.h>
#include <MPIWrapper.h>

namespace Hydra {

//! Macros that insert file and line number information into temp data allocs
#define allocTempChar(n)  allocChar(n*sizeof(char), __FILE__, __LINE__)
#define allocTempFloat(n) allocFloat(n*sizeof(float), __FILE__, __LINE__)
#define allocTempInt(n)   allocInt(n*sizeof(int), __FILE__, __LINE__)
#define allocTempReal(n)  allocReal(n*sizeof(Real), __FILE__, __LINE__)
#define allocTempData(n)  allocData(n, __FILE__, __LINE__)

#define MAX_MEMORY_ENTRIES   32768
#define MAX_TEMPORARY_ENTRIES  128

//! DataContainer is the basic data storage/management class in HYDRA.
class DataContainer {

  public:

    //! \name Constructor/Destructor
    //@{
             DataContainer();
    virtual ~DataContainer();
    //@}

    //! \name Variable registration
    //! All objects and variables are registered in data containers
    //! using the registerVariable function.
    //!
    //! The DataIndex is a simple handle for retrieving the data without
    //! carrying raw pointers around.  The data register keeps track
    //! of the total memory usage in the code and can be queried from
    //! anywhere in the code.
    //@{
    //! Dump the data container entries
    void dumpEntries(ostream& ofs);

    //! Dump a list of all registered data
    void dumpVariables(ostream& ofs);

    //! Dump a list of all registered data on a given processor
    void dumpVariables(ostream& ofs, int pid);

    //! Dump a list of all registered data
    void dumpMPVariables(ostream& ofs);

    //! Free data/objects by DataIndex
    void freeVariable(DataIndex& vid);

    //! Free ALL data/objects
    void freeAllVariables();

    //! Lookup a variable by name
    DataIndex findVariable(string vname);

    //! Return the VariableCentering given a DataIndex
    VariableCentering getCentering(DataIndex vid);

    //! Return the memory layout
    MemoryLayout getLayout(DataIndex vid);

    //! Return a variable name given a DataIndex
    const char* getName(DataIndex vid);

    //! Number of data items
    int getNumber(DataIndex vid);

    //! Number of registered items
    int getNumberRegistered();

    //! Get size (in bytes) of data object based on type
    int getSizeOfType(VariableType type);

    //! Return the size in bytes of the data object
    size_t getSize(DataIndex vid);
    //@}

    //! \name Column-oriented data shapers
    //@{
    CVector getCVector(DataIndex vid);

    CVector getCVector(DataIndex vid, int stride);

    Real* getCVectorComp(DataIndex vid, int comp);

    CBoolVector getCBoolVector(DataIndex vid, int stride);

    CAntiTensor getCAntiTensor(DataIndex vid);

    CSymTensor getCSymTensor(DataIndex vid);

    CTensor getCTensor(DataIndex vid);

    Real* getCTensorComp(DataIndex vid, int comp);

    CQuaternion getCQuaternion(DataIndex vid);
    //@}

    //! \name Row-oriented data shapers -- use is currently discouraged
    //@{
    RVector* getRVector(DataIndex vid);

    RAntiTensor* getRAntiTensor(DataIndex vid);

    RSymTensor* getRSymTensor(DataIndex vid);

    RTensor* getRTensor(DataIndex vid);

    RQuaternion* getRQuaternion(DataIndex vid);
    //@}

    //! \name Array data shapers
    //@{
    //! Return a pointer given a DataIndex
    //! \param[in]  V    Type of variables as the memory is accessed
    //! \param[in]  vid  DataIndex
    //! \return          Raw pointer of type V to memory
    template<class V>
      V* getVariable(DataIndex vid) {
      if (vid < 0 || vid >= MAX_MEMORY_ENTRIES) {
        cout << endl
             << "\tDataContainer::getVariable(...) " <<  endl;
        cout << "\tProcessor: id = " << g_comm->getPid() << endl;
        cout << "\tError in data registration: id = " << vid
             << " invalid !!!" << endl;
        dumpVariables(cout);
        assert (vid >= 0);
        exit(0);
      }

      if (m_entry[vid]->m_ptr == 0) {
        cout << endl
             << "\tDataContainer::getVariable(...) " <<  endl;
        cout << "\tProcessor: id = " << g_comm->getPid() << endl;
        cout << "\tBad pointer encountered: id = " << vid << endl;
        dumpVariables(cout);
        exit(0);
      }

      return static_cast<V*>((m_entry[vid]->m_ptr));
    }

    //! Return a logically 2D array with strides s1 x s2 on top of a DataIndex
    //! \author J. Bakosi
    //! \param[in]  T      Type of variables as the memory is accessed
    //! \param[in]  vid    DataIndex
    //! \param[in]  s1     Major stride
    //! \param[in]  s2     Minor stride
    //! \param[in]  start  Start from a specified offset
    //! \param[in]  vid    DataIndex
    //! \return            Logically s1 x s2 dimensional array of raw pointers of
    //!                    type V to memory
    //! \details WARNING: This data shaper is not intended for general use on
    //!          large arrays with arbitrary indexing. For example, if all of the
    //!          strides are known at compile-time and a simpler struct can be
    //!          used with a constant, pre-defined layout, use those already
    //!          available in Base/DataShapes.h instead. The dynamic shaping in
    //!          this routine is really only justified if the strides are only
    //!          known at run-time. Also, the use of this shaper and its indexing
    //!          will be most memory-efficient if the sizes of the strides are in
    //!          increasing order, i.e., the last being the largest, e.g.,
    //!          s1 = 4, s2 = very_large. In other words,
    //!            *  THINK HARD BEFORE YOU ATTEMPT TO USE THIS ROUTINE,
    //!            *  EXPLORE ALTERNATIVES, and
    //!            *  EVEN IF YOU DECIDE TO USE IT, DO NOT CALL THIS ROUTINE
    //!               WHERE PERFORMANCE IS PARAMOUNT, e.g., try to call it once,
    //!               in a setup/initialization stage only.
    template<typename T>
      vector<T*> getShapedArray(DataIndex vid, int s1, int s2, size_t start = 0) {
      assert(s1 > 0);
      assert(s2 > 0);
      if ((start + s1*s2)*sizeof(T) > getNumber(vid)*getSize(vid)) {
        cout << endl
             << "\tDataContainer::getShapedArray(...) " <<  endl;
        cout << "\tProcessor: id = " << g_comm->getPid() << endl;
        cout << "\tAttempt to shape array to larger footprint than available "
          "behind id = " << vid << " !!!" << endl;
        dumpVariables(cout);
      }

      T* f = getVariable<T>(vid) + start;
      vector<T*> F(s1);
      for (int i=0; i<s1; ++i) {
        F[i] = f + i*s2;
      }
      return F;
    }

    //! Return a logically 3D array with strides s1 x s2 x s3 on top of a
    //! DataIndex
    //! \author J. Bakosi
    //! \param[in]  T    Type of variables as the memory is accessed
    //! \param[in]  vid  DataIndex
    //! \param[in]  s1   Major stride
    //! \param[in]  s2   Minor stride
    //! \param[in]  s3   Even more minor stride
    //! \param[in]  vid  DataIndex
    //! \return          Logically s1 x s2 x s3 dimensional array of raw pointers
    //!                  of type V to memory
    //! \details WARNING: This data shaper is not intended for general use on
    //!          large arrays with arbitrary indexing. For example, if all of the
    //!          strides are known at compile-time and a simpler struct can be
    //!          used with a constant, pre-defined layout, use those already
    //!          available in Base/DataShapes.h instead. The dynamic shaping in
    //!          this routine is really only justified if the strides are only
    //!          known at run-time. Also, the use of this shaper and its indexing
    //!          will be most memory-efficient if the sizes of the strides are in
    //!          increasing order, i.e., the last being the largest, e.g.,
    //!          s1 = 4, s2 = 4, s3 = very_large. In other words,
    //!            *  THINK HARD BEFORE YOU ATTEMPT TO USE THIS ROUTINE,
    //!            *  EXPLORE ALTERNATIVES, and
    //!            *  EVEN IF YOU DECIDE TO USE IT, DO NOT CALL THIS ROUTINE
    //!               WHERE PERFORMANCE IS PARAMOUNT, e.g., try to call it once,
    //!               in a setup/initialization stage only.
    template<typename T>
      vector< vector<T*> > getShapedArray(DataIndex vid, int s1, int s2, int s3) {
      assert(s1 > 0);
      if (static_cast<size_t>(s1)*s2*s3*sizeof(T) > getNumber(vid)*getSize(vid)) {
        cout << endl
             << "\tDataContainer::getShapedArray(...) " <<  endl;
        cout << "\tProcessor: id = " << g_comm->getPid() << endl;
        cout << "\tAttempt to shape array to larger footprint than available "
          "behind id = " << vid << " !!!" << endl;
        dumpVariables(cout);
      }

      vector< vector<T*> > F(s1);
      for (int i=0; i<s1; ++i) {
        F[i] = getShapedArray<T>(vid, s2, s3, static_cast<size_t>(i)*s2*s3);
      }
      return F;
    }
    //@}

    //! Return the VariableType given a DataIndex
    VariableType getType(DataIndex vid);

    //! Map data/objects to existing registered variable
    DataIndex mapVariable(size_t number, //!< # of data objects
                          size_t size,   //!< size of object in bytes
                          size_t offset, //!< offset into existing data
                          DataIndex vid, //!< DataIndex for existing memory
                          string name,   //!< label for data
                          VariableCentering centering = NO_CTR,
                          VariableType type = GENERIC_VAR,
                          MemoryLayout layout = LINEAR_ARRAY,
                          bool plot = false,
                          bool restart = false);

    //! Number of entries in container
    int numEntries() {return MAX_MEMORY_ENTRIES;}

    //! Check to see if a variable can be output for plotting
    bool plotVariable(DataIndex vid);

    //! Check to see if a variable is registered
    bool registeredVariable(DataIndex vid);

    //! Check to see if a variable is marked for restart
    bool restartVariable(DataIndex vid);

    //! Register data/objects using this function
    DataIndex registerVariable(size_t number, //!< # of data objects
                               size_t size,   //!< size of object in bytes
                               string name,   //!< label for data
                               VariableCentering centering = NO_CTR,
                               VariableType type = GENERIC_VAR,
                               MemoryLayout layout = LINEAR_ARRAY,
                               bool plot = false,
                               bool restart = false);

    //! Query registered memory usage statistics in bytes
    Real getRegisteredMemory();

    //! Query temporary memory usage statistics in bytes
    Real getTemporaryMemory();

    //! Query DataContainer total memory usage in bytes
    Real getMemory();

    //! Report memory usage statistics
    void reportMemory(ostream& ofs);

    //! Swap the memory associated with two DataIndices, V1 and V2
    void swapMemory(DataIndex V1, DataIndex V2);
    //@}

    //! \name Temporary memory management
    //! Allocate data for temporary use.  Do not call these functions directly,
    //! use the allocTemp* macros defined in the header instead.
    //!
    //! allocTempChar(n) -- allocates char(n*sizeof(char), __FILE__, __LINE__)
    //! allocTempInt(n)  -- allocates integers
    //! allocTempReal(n) -- allocates Reals
    //! allocTempData(n) -- allocates void*
    //{@
    //! Free temporary character data
    void freeTempData(char*  ptr);

    //! Free temporary integer data
    void freeTempData(int*   ptr);

    //! Free temporary Real data
    void freeTempData(Real*  ptr);

    //! Free temporary void data
    void freeTempData(void*  ptr);

    //! Internal allocator with file name & line number tracked
    char* allocChar(size_t nchars, const char filename[], int lineno){
     return
       reinterpret_cast<char*>(allocData(nchars*sizeof(char), filename,lineno));
   }

   float* allocFloat(size_t nfloat, const char filename[], int lineno) {
     return
      reinterpret_cast<float*>(allocData(nfloat*sizeof(float),filename,lineno));
   }

   //! Internal allocator with file name & line number tracked
   int* allocInt(size_t nints, const char filename[], int lineno) {
     return
       reinterpret_cast<int*>(allocData(nints*sizeof(int), filename, lineno));
   }

   //! Internal allocator with file name & line number tracked
   Real* allocReal(size_t nreals, const char filename[], int lineno) {
     return
       reinterpret_cast<Real*>(allocData(nreals*sizeof(Real),filename, lineno));
   }

   //! Internal allocator with file name & line number tracked
   void*  allocData(uint64_t Nbytes, const char filename[], int lineno);
   //@}

  protected:

    //! Processor Id, # of Processors
    int m_pid;
    int m_Nproc;

  private:

    //! \name Don't permit copy or assignment operators
    //@{
    DataContainer(const DataContainer&);
    DataContainer& operator=(const DataContainer&);
    //@}

    //! List of data container entries
    DataContainerEntry* m_entry[MAX_MEMORY_ENTRIES];

    //! List of temporary storage objects
    DataContainerTemporary* m_temp[MAX_TEMPORARY_ENTRIES];

    //! \name Counters of total allocated/deallocated storage in bytes
    //@{
    Real m_total_allocated;
    Real m_total_deallocated;
    //@}
};

}

#endif  // DataContainer_h
