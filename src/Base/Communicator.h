//******************************************************************************
//! \file    src/Base/Communicator.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Base class for unstructured mesh communications
//******************************************************************************
#ifndef Communicator_h
#define Communicator_h

#include <DataContainer.h>

namespace Hydra {

//! Processor Map structure
struct ProcMap {
  int st; //!< Starting ordinal Id on given processor
  int en; //!< Ending ordinal Id on given processor
};

//! This is the base class for all communications objects
class Communicator : public DataContainer {
  public:

    //! \name Constructor/Destructor
    //@{
             Communicator() {}
    virtual ~Communicator() {}
    //@}

    //@{
    //! Get the communicator Id
    int  getID()  {return m_Id; }
    //@}

  protected:

    //! Data is used in the inhereted communicators
    int m_Id;  //!< communicator id

  private:

    //! Don't permit copy or assignment operators
    //@{
    Communicator(const Communicator&);
    Communicator& operator=(const Communicator&);
    //@}
};

}

#endif // Communicator_h
