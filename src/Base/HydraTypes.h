//******************************************************************************
//! \file    src/Base/HydraTypes.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Defines all parameters and types for Hydra, e.g., Real is double.
//******************************************************************************
#ifndef HydraTypes_h
#define HydraTypes_h

#include <cmath>
#include <limits>

namespace Hydra {

//! typedefs to allow easy modification of word size for a particular
//! application or computer

typedef unsigned int uint;  //!< Unsigned integers
#ifdef DBL
typedef double Real;
#else
typedef float  Real;
#endif

typedef int DataIndex;       //!< Data type for variable registration
typedef int TimerIndex;      //!< Data type for timer registration

//! Cache block size, other basic constants used in the code
const int BLKSIZE   = IBSZ;  //!< Blocksize for cache-blocks
const int MAXNB     = 6;     //!< Maximum number of neighbors attached by edges
const int MAXCHR    = 1024;  //!< Maximum character length
const int MAXMSG    = 2048;  //!< Maximum number of characters in a warning
const int TTY_WIDTH = 72;    //!< Width of screen output for errors/warnings

const Real PI = 4.0*atan(1.0); //!< Pi definition

//! Dimensionality
enum Dimension {
  ONED   = 1,
  TWOD   = 2,
  THREED = 3
};

//! Global coordinate directions
enum GlobalDirection {
  GLOBAL_XDIR = 1,
  GLOBAL_YDIR = 2,
  GLOBAL_ZDIR = 3
};

//! FORTRAN macro to screen linkage rules
#if defined LC_FLINK
#define FORTRAN(subname) subname##_
#elif defined LCFLINK
#define FORTRAN(subname) subname
#elif defined GNU
#define FORTRAN(subname) subname##_
#endif

}
#endif  // HydraTypes_h
