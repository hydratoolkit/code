//******************************************************************************
//! \file    src/Base/Interface.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Base class for deriving interface tracking or reconstruction
//******************************************************************************

#ifndef Interface_h
#define Interface_h

#include <HydraTypes.h>

namespace Hydra {

//! Base class for deriving interface tracking or reconstruction
class Interface {

  public:

    //! \name Constructor/Destructor
    //@{
             Interface() {}
    virtual ~Interface() {}
    //@}

    //! \name Interface access functions
    //! The base Interface class is used to derive physics-specific
    //! interface options.
    //@{
    int  getId()  {return id; } //!< Get the interface id
    //@}

  protected:

    //! \name Interface data
    //! This data is used in the inhereted physics-specific interface
    //! objects.
    int   id;  //!< interface id

  private:

    //! Don't permit copy or assignment operators
    //@{
    Interface(const Interface&);
    Interface& operator=(const Interface&);
    //@}
};

}

#endif // Interface_h
