//******************************************************************************
//! \file    src/Base/NodeSetBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Base class for deriving physics-specific nodeset-based BC's
//******************************************************************************
#ifndef NodeSetBC_h
#define NodeSetBC_h

#include <BCPackage.h>
#include <SetBC.h>

namespace Hydra {

//! Nodeset BC base class
class NodeSetBC : public SetBC {

  public:
    //! \name Constructor/Destructor
    //@{
             NodeSetBC() {}
             NodeSetBC(const BCPackage& bc);
    virtual ~NodeSetBC() {}
    //@}

    //! Get the nodeset for this boundary condition. Note that
    //! the nodeset may be derived from a sideset, or instead
    //! may be a nodeset defined in the mesh.
    //! \param[in] mesh   The mesh
    int* getNodeSet(UnsMesh& mesh);

    //! Get the local size of the nodeset for this boundary
    //! condition. Note that the nodeset may be derived from a
    //! sideset, or instead may be a nodeset defined in the mesh.
    //! \param[in] mesh   The mesh
    int getNodeSetSize(UnsMesh& mesh);

  protected:

  private:

    //! Don't permit copy or assignment operators
    //@{
    NodeSetBC(const NodeSetBC&);
    NodeSetBC& operator=(const NodeSetBC&);
    //@}

};

}

#endif // NodeSetBC_h
