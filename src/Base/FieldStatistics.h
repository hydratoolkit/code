//******************************************************************************
//! \file    src/Base/FieldStatistics.h
//! \author  Jozsef Bakosi, Mark A. Christon
//! \date    Wed Feb 01 15:45:00 2012
//! \brief   Field statistics
//******************************************************************************
#ifndef FieldStatistics_h
#define FieldStatistics_h

#include <fileIO.h>
#include <Physics.h>
#include <UnsMesh.h>
#include <Statistics.h>
#include <UnsPhysics.h>

namespace Hydra {

// Declarations of generic field accumulator wrapper functions
ACCUMULATOR_DECL(FieldStatisticsAccumulateScalar)
ACCUMULATOR_DECL(FieldStatisticsAccumulateVector)
ACCUMULATOR_DECL(FieldStatisticsAccumulateTensor)
ACCUMULATOR_DECL(FieldStatisticsAccumulateSymTensor)
ACCUMULATOR_DECL(FieldStatisticsAccumulateDyad)

class FieldStatistics : public Statistics {

  public:

    //! \name Constructor/Destructor
    //@{
             FieldStatistics(UnsMesh* mesh,
                             fileIO* io,
                             UnsPhysics* unsPhysics,
                             vector<DataIndex>& tmpScalars);
    virtual ~FieldStatistics();
    //@}

    //! Member function declarations of generic field accumulator functions
    ACCUMULATOR_MEMB(accumulateScalar)
    ACCUMULATOR_MEMB(accumulateVector)
    ACCUMULATOR_MEMB(accumulateTensor)
    ACCUMULATOR_MEMB(accumulateSymTensor)
    ACCUMULATOR_MEMB(accumulateDyad)

    //! Get variable index based on accumulator index for a field
    DataIndex getFieldVarId(AccumulatorId accId);

    //! Register a field accumulator
    DataIndex registerAccumulator(AccumulatorId accId,
                                  const OutputDelegateKey& odKey,
                                  const AccumulatorDelegateMap& accDelegates);

    //! Setup field statistics
    void setupStatistics(const StatisticsDelegateMap& statDelegates,
                         const bool* const availAccumulators,
                         AccumulatorDelegateMap& accDelegates);

  private:

    //! Don't permit copy or assignment operators
    //@{
    FieldStatistics(const FieldStatistics&);
    FieldStatistics& operator=(const FieldStatistics&);
    //@}

    //! Data indices to elem-centered scratch scalars
    vector<DataIndex> m_tmpScalars;

    //! Object pointer to fileIO
    fileIO* m_io;

    //! Object pointer to UnsPhysics
    UnsPhysics* m_unsPhysics;
};

}

#endif // FieldStatistics.h
