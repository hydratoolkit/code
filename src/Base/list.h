//******************************************************************************
//! \file    src/Base/list.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Provides linked-list functions used for the PPE connectivity.
//******************************************************************************
#ifndef LIST_H
#define LIST_H

// list.h - Definitions for list operations.
//
// 11/27/95 - Added NEXT and PREV macros; fixed bug in UNLINK
//            for case where the unlinked element is first in
//            the list ("prev" pointer for _new_ first element
//            was not being reset to NULL).
//
// 08/02/96 - Same fix as UNLINK (above) for DELETE.
//
// These routines assume that the next and prev members of an element
// are set to NULL by default.

//! Increment a list pointer to the next list member.
#define NEXT( elem ) ( (elem) = (elem)->next )

//! Decrement a list pointer to the previous list member.
#define PREV( elem ) ( (elem) = (elem)->prev )

//! Set the next, prev pointers of an element to NULL.
#define INIT_PTRS( elem ) { (elem)->next = NULL; (elem)->prev = NULL; }

//! Insert an element at the beginning of a list.
#define INSERT( elem, list ) { if ( (list) != NULL )  \
                                  { (elem)->next = (list);  \
                                    (list)->prev = (elem); }  \
                               (list) = (elem); }

//! Insert an element before another in a list.
#define INSERT_BEFORE( new_elem, elem ) { if ( (elem)->prev != NULL ) {  \
                 (elem)->prev->next = (new_elem);  \
                 (new_elem)->prev = (elem)->prev; }  \
                 (new_elem)->next = (elem); (elem)->prev = (new_elem); }

//! Insert an element after another in a list.
#define INSERT_AFTER( new_elem, elem ) { if ( (elem)->next != NULL ) {  \
                 (elem)->next->prev = (new_elem);  \
                 (new_elem)->next = (elem)->next; }  \
                 (new_elem)->prev = (elem); (elem)->next = (new_elem); }

//! Append an element or list of elements to another.
#define APPEND( elem, list ) { if ( (list) == NULL ) (list) = (elem);  \
                 else {  (elem)->prev = (list);  \
                         while( (elem)->prev->next != NULL )  \
                            (elem)->prev = (elem)->prev->next;  \
                         (elem)->prev->next = (elem);  } }

//! Unlink an element from a list.
#define UNLINK( elem, list ) { if ( (list) == (elem) )  \
                                  (list) = (elem)->next;  \
                               else  \
                                  (elem)->prev->next = (elem)->next;  \
                               if ( (elem)->next != NULL )  \
                                  (elem)->next->prev = (elem)->prev;  \
                               (elem)->next = NULL; (elem)->prev = NULL; }

//! Delete an element from a list.
//! DOES NOT HANDLE DYNAMICALLY ALLOCATED DATA IN THE ELEMENT.
#define DELETE( elem, list ) { if ( (list) == (elem) )  \
                                  (list) = (elem)->next;  \
                               else  \
                                  (elem)->prev->next = (elem)->next;  \
                               if ( (elem)->next != NULL )  \
                                  (elem)->next->prev = (elem)->prev;  \
                               (elem)->next = NULL; (elem)->prev = NULL; }

//! Delete all of the elements in a list.
#define DELETE_LIST( list ) { if ( (list) != NULL ) {  \
                       while ( (list)->next != NULL )  \
                       { (list) = (list)->next; delete ( (list)->prev ); }  \
                       delete ( (list) ); (list) = NULL; } }


#endif // LIST_H
