//******************************************************************************
//! \file    src/Base/SetTypes.h
//! \author  Markus Berndt, Mark A. Christon
//! \date
//! \brief   declaration of the different mesh entities
//*****************************************************************************
#ifndef SetTypes_h
#define SetTypes_h

namespace Hydra {

#define UNDEFINED_SET_ID -1

enum MeshSetType {
  UNDEFINED_SET,  //!< Undefined set
  NODE_SET,       //!< Node set
  EDGE_SET,       //!< Dual-edge set
  FACE_SET,       //!< Side set, e.g., face-centered
  ELEM_SET        //!< ELement set
};

}

#endif
