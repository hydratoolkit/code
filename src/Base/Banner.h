//******************************************************************************
//! \file    src/Base/Banner.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Print out the code name in 'banner' format
//******************************************************************************

#ifndef Banner_h
#define Banner_h

#include <HydraTypes.h>

namespace Hydra {

#define SMALL_FONT   1
#define MEDIUM_FONT  2
#define LARGE_FONT   3

#define LEFT_JUSTIFIED    1
#define CENTER_JUSTIFIED  2
#define RIGHT_JUSTIFIED   3

class Banner;
ostream& operator<<(ostream&, const Hydra::Banner&);

//! Pretty-print code banner
class Banner {

  public:

    Banner(const string& name,
           const int fontSize = MEDIUM_FONT,
           const int format = CENTER_JUSTIFIED) :
           m_name(name), m_fontSize(fontSize), m_format(format) {};

    ~Banner() {};

    friend ostream& operator<<(ostream&, const Hydra::Banner&);

  private:

    //! Don't permit copy or assignment operators
    //@{
    Banner(const Banner&);
    Banner& operator=(const Banner&);
    //@}

    string m_name;       //!< String to be converted
    int    m_fontSize;   //!< Banner font size
    int    m_format;     //!< Banner justification

}; // Banner_h

}

#endif
