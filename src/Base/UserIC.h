//******************************************************************************
//! \file    src/Base/UserIC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Virtual base class for implementing user-defined initial conditions
//******************************************************************************

#ifndef UserIC_h
#define UserIC_h

#include <HydraTypes.h>
#include <UnsMesh.h>

namespace Hydra {

//! Virtual base class for implementing user-defined initial conditions
class UserIC {

  public:

    //! \name Constructor/Destructor
    //@{
             UserIC() {}
    virtual ~UserIC() {}
    //@}

    //! Setup IC's using a mesh and set of DataIndices
    virtual void setICs(UnsMesh* /*mesh*/, vector<DataIndex>& /*vars*/) {
      cout << "!!! No default user setICs() function !!!" << endl;
    }

    //! Return a name for the IC
    virtual string getName() {return "No-Name";}

  private:

    //! Don't permit copy or assignment operators
    //@{
    UserIC(const UserIC&);
    UserIC& operator=(const UserIC&);
    //@}

};

}

#endif // USERIC_H
