//******************************************************************************
//! \file    src/Base/Block.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Derive physics-specific block objects for prescribing IC's, etc.
//******************************************************************************

#ifndef Block_h
#define Block_h

#include <HydraTypes.h>

namespace Hydra {

//! This is the base class for all block-based conditions
class Block {

  public:
    //! \name Constructor/Destructor
    //@{
             Block() {}
    virtual ~Block() {}
    //@}

    //! \name Block access functions
    //! The base Block class is used to derive physics-specific
    //! block options.
    //@{
    void setId(int id) {m_userId = id;   } //!< Set the block user Id
    int  getId()       {return m_userId; } //!< Get the block id
    //@}

  protected:

    //! \name Block data
    //! This data is used in the inhereted physics-specific block-based
    //! objects.
    int m_userId;  //!< User block id

  private:

    //! Don't permit copy or assignment operators
    //@{
    Block(const Block&);
    Block& operator=(const Block&);
    //@}
};

//! Block Velocity Initial Conditions
class BlockVelIC : public Block {

  public:

    //! \name Constructor/Destructor
    //@{
             BlockVelIC() {}
    virtual ~BlockVelIC() {}
    //@}


    //! \name BlockVelIC access functions
    //! The BlockVelIC class is used to setup physics-specific
    //! block initial velocity conditions.
    //@{
    void setVelIC(Real* vel) {velic[0] = vel[0];
                              velic[1] = vel[1];
                              velic[2] = vel[2];}

    Real* getVelIC() {return velic;}
    Real  getVelx()  {return velic[0];}
    Real  getVely()  {return velic[1];}
    Real  getVelz()  {return velic[2];}
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    BlockVelIC(const BlockVelIC&);
    BlockVelIC& operator=(const BlockVelIC&);
    //@}

    Real velic[3];  //!< Vector of velocity IC's
};

}

#endif // Block_h
