//******************************************************************************
//! \file    src/Base/MathOp.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Math Op's on vectors, tensors, etc.
//******************************************************************************
#ifndef MathOp_h
#define MathOp_h

#include <DataShapes.h>

namespace Hydra {

//! MathOp provides math operations using vectors and tensors
namespace MathOp {

  //****************************************************************************
  // ADDITION
  //! Add two vectors C = A + B
  void add(Vector& A, Vector& B, Vector& C);

  //! Add two column vectors C = A + B
  void add(int start, int Nsize, CVector& A, CVector& B, CVector& C);

  //! Add two tensors Cij = Aij + Bij
  void add(Tensor& A, Tensor& B, Tensor& C);

  //! Add two column tensors Cij = Aij + Bij
  void add(int start, int Nsize, CTensor& A, CTensor& B, CTensor& C);

  //! Add two symmetric tensors Cij = Aij + Bij
  void add(SymTensor& A, SymTensor& B, SymTensor& C);

  //! Add two symmetric column-tensors Cij = Aij + Bij
  void add(int start, int Nsize, CSymTensor& A, CSymTensor& B, CSymTensor& C);

  //! Add two anti-tensors Cij = Aij + Bij
  void add(AntiTensor& A, AntiTensor& B, AntiTensor& C);

  //! Add two column anti-tensors Cij = Aij + Bij
  void add(int start, int Nsize, CAntiTensor& A, CAntiTensor& B, CAntiTensor& C);

  //! Add a symmmetric tensor and an anti-tensor Cij = Aij + Bij
  void add(SymTensor& A, AntiTensor& B, Tensor& C);

  //! Add a symmetric column tensor and a column anti-tensor Cij = Aij + Bij
  void add(int start, int Nsize, CSymTensor&  A, CAntiTensor& B, CTensor& C);

  //****************************************************************************
  // SHIFT
  //! Add a scalar to each component of a vector C = A + SxI
  void shift(Vector& A, const Real S, Vector& C);

  //! Add a scalar to each component of a column vector C = A + SxI
  void shift(int start, int Nsize, CVector& A, const Real S, CVector& C);

  //! Add a scalar to each component of a tensor  Cij = Aij + SxIij
  void shift(Tensor& A, const Real S, Tensor& C);

  //! Add a scalar to each component of a column tensor Cij = Aij + SxIij
  void shift(int start, int Nsize, CTensor& A, const Real S, CTensor& C);

  //! Add a scalar to each component of a sym-tensor  Cij = Aij + SxIij
  void shift(SymTensor& A, const Real S, SymTensor& C);

  //! Add a scalar to each component of a solumn sym-tensor  Cij = Aij + SxIij
  void shift(int start, int Nsize, CSymTensor& A, const Real S, CSymTensor& C);

  //! Add a scalar to each component of an anti-tensor  Cij = Aij + SxIij
  void shift(AntiTensor& A, const Real S, Tensor& C);

  //! Add a scalar to each component of a column anti-tensor  Cij = Aij + SxIij
  void shift(int start, int Nsize, CAntiTensor& A, const Real S, CTensor& C);

  //****************************************************************************
  // SCALE
  //! Multiply a scalar to each component of a vector C =SxA
  void scale(Vector& A, const Real S, Vector& C);

  //! Multiply a scalar to each component of a column vector C =SxA
  void scale(int start, int Nsize, CVector& A, const Real S, CVector& C);

  //! Multiply a scalar to each component of a tensor  Cij = SxAij
  void scale(Tensor& A, const Real S, Tensor& C);

  //! Multiply a scalar to each component of a column tensor  Cij = SxAij
  void scale(int start, int Nsize, CTensor& A, const Real S, CTensor& C);

  //! Multiply a scalar to each component of a sym-tensor  Cij = SxAij
  void scale(SymTensor& A, const Real S, SymTensor& C);

  //! Multiply a scalar to each component of a column sym-tensor  Cij = SxAij
  void scale(int start, int Nsize, CSymTensor& A, const Real S, CSymTensor& C);

  //! Multiply a scalar to each component of an anti- tensor  Cij = SxAij
  void scale(AntiTensor& A, const Real S, AntiTensor& C);

  //! Multiply a scalar to each component of a column anti-tensor  Cij = SxAij
  void scale(int start, int Nsize, CAntiTensor& A, const Real S, CAntiTensor& C);

  //****************************************************************************
  // IDENTITY
  //! Create identity tensor which can be scaled by 2 factors
  void identity(Real& A, Real& B, Tensor& C);

  //****************************************************************************
  // INNER PRODUCT
  //! Compute the dot product between two vectors S =<A|B>
  void innerProduct(Vector& A, Vector& B, Real& S);

  //! Compute the dot product between two column vectors S =<A|B>
  void innerProduct(int start, int Nsize, CVector& A, CVector& B, Real* S);

  //! Compute the double product of two tensors S =Aij:Bij
  void innerProduct(Tensor& A, Tensor& B, Real& S);

  //! Compute the double product of two column tensors S =Aij:Bij
  void innerProduct(int start, int Nsize, CTensor& A, CTensor& B, Real* S);

  //! Compute the double product of two sym-tensors S =Aij:Bij
  void innerProduct(SymTensor& A, SymTensor& B, Real& S);

  //! Compute the double product of two column sym-tensors S =Aij:Bij
  void innerProduct(int start, int Nsize, CSymTensor& A, CSymTensor& B, Real* S);

  //! Compute the double product of two anti-tensors S =Aij:Bij
  void innerProduct(AntiTensor& A, AntiTensor& B, Real& S);

  //! Compute the double product of two column anti-tensors S =Aij:Bij
  void innerProduct(int start, int Nsize, CAntiTensor& A, CAntiTensor& B, Real* S);

  //! Compute the double product of a sym-tensor and a tensor S =Aij:Bij
  void innerProduct(SymTensor& A, Tensor& B, Real& S);

  //! Compute the double product of a column sym-tensor and tensors S =Aij:Bij
  void innerProduct(int start, int Nsize, CSymTensor& A, CTensor& B, Real* S);

  //! Compute the double product of tensor and sym-tensor S =Aij:Bij
  void innerProduct(Tensor& A, SymTensor& B, Real& S);

  //! Compute the double product of column tensor and sym-tensor S =Aij:Bij
  void innerProduct(int start, int Nsize, CTensor& A, CSymTensor& B, Real* S);

  //! Compute the double product of anti-tensor and tensor S =Aij:Bij
  void innerProduct(AntiTensor& A, Tensor& B, Real& S);

  //! Compute the double product of column anti-tensor and tensor S =Aij:Bij
  void innerProduct(int start, int Nsize, CAntiTensor& A, CTensor& B, Real* S);

  //! Compute the double product of tensor and anti-tensor S =Aij:Bij
  void innerProduct(Tensor& A, AntiTensor& B, Real& S);

  //! Compute the double product of column tensor and anti-tensor S =Aij:Bij
  void innerProduct(int start, int Nsize, CTensor& A, CAntiTensor& B, Real* S);

  //! Compute the double product of sym-tensor an anti-tensor S =Aij:Bij
  void innerProduct(SymTensor& A, AntiTensor& B, Real& S);

  //! Compute the double product of column sym-tensor and anti-tensor S =Aij:Bij
  void innerProduct(int start, int Nsize, CSymTensor& A, CAntiTensor& B, Real* S);

  //! Compute the double product of anti and sym-tensor S =Aij:Bij
  void innerProduct(AntiTensor& A, SymTensor& B, Real& S);

  //! Compute the double product of column anti and sym-tensor S =Aij:Bij
  void innerProduct(int start, int Nsize, CAntiTensor& A, CSymTensor& B, Real* S);

  //****************************************************************************
  // PRODUCT
  //! Compute the product of two tensors: Sij = Aik Bkj
  void product(int idx, CTensor& A, CTensor& B, Tensor& S);

  //! Compute the product of two STANDARD tensors: Sij = Aik Bkj
  void product(Tensor& A, Tensor& B, Tensor& S);

  //! Compute the product of two SYMMETRIC tensors: Sij = Aik Bkj
  void product(SymTensor& A, SymTensor& B, Tensor& S);

  //! Compute the product of two ANTI-SYMMETRIC tensors: Sij = Aik Bkj
  void product(AntiTensor& A, AntiTensor& B, Tensor& S);

  //! Compute the product of SYM AND ANTI tensors: Sij = Aik Bkj
  void product(SymTensor& A, AntiTensor& B, Tensor& S);

  //! Compute the product of ANTI AND SYM tensors: Sij = Aik Bkj
  void product(AntiTensor& A, SymTensor& B, Tensor& S);

  //****************************************************************************
  // OUTER PRODUCT

  //****************************************************************************
  // CROSS PRODUCT
  //! Cross product between two vectors Ci = Eijk Aj Bk
  void crossProduct(Vector& A, Vector& B, Vector& C);

  //! Cross product between two column vectors Ci = Eijk Aj Bk
  void crossProduct(int start, int Nsize, CVector& A, CVector& B, CVector& C);

  //! Angle between two vectors
  Real angle(Vector& A, Vector &B);

  //****************************************************************************
  // NORM
  // Compute the L-2 norm of a vector |A| = <A|A>^1/2
  void normL2(Vector& A, Real& L2N);

  //! Compute the L-2 norm of a column vector |A| = <A|A>^1/2
  void normL2(int start, int Nsize, CVector& A, Real* L2N);

  //! Compute the L-2 norm of a tensor |Aij| = <AijAij>^1/2
  void normL2(const Real S, Tensor& A, Real& L2N);

  //! Compute the L-2 norm of a column tensor |Aij| = <SxAijAij>^1/2
  void normL2(int start, int Nsize, const Real S, CTensor& A, Real* L2N);

  //! Compute the L-2 norm of a sym-tensor |Aij| = <SxAijAij>^1/2
  void normL2(const Real S, SymTensor& A, Real& L2N);

  //! Compute the L-2 norm of a column sym-tensor |Aij| = <SxAijAij>^1/2
  void normL2(int start, int Nsize, const Real S, CSymTensor& A, Real* L2N);

  //! Compute the L-2 norm of an anti-tensor |Aij| = <SxAijAij>^1/2
  void normL2(const Real S, AntiTensor& A, Real& L2N);

  //! Compute the L-2 norm of a column anti-tensor |Aij| = <SxAijAij>^1/2
  void normL2(int start, int Nsize, const Real S, CAntiTensor& A, Real* L2N);

  //****************************************************************************
  // SYMMETRIC AND ANTI-SYMMETRIC TENSORS
  //! Compute the anti-symmetric tensor Rij = 1/2 ( Aij - Aji )
  void antisymmetric(Tensor& A, AntiTensor& R);

  //! Compute the symmetric column tensor Rij = 1/2 ( Aij - Aji )
  void antisymmetric(int start, int Nsize, CTensor& A, CAntiTensor& R);

  //! Compute the symmetric tensor Rij = 1/2 ( Aij - Aji )
  void antisymmetric(int loc, CTensor& A, AntiTensor& R);

  //! Compute the symmetric (Sij) and anti-symmetric (Rij) tensors
  void decomposeSymmAnti(Tensor& A, SymTensor& S, AntiTensor& R);

  //! Compute the symmetric (Sij) and anti-symmetric (Rij) tensors
  void decomposeSymmAnti(int start, int Nsize, CTensor& A, CSymTensor& S,
                         CAntiTensor& R);

  //! Compute the symmetric (Sij) and anti-symmetric (Rij) tensors
  void decomposeSymmAnti(int loc, CTensor& A, SymTensor& S, AntiTensor& R);

  //! Compute the symmetric tensor Sij = 1/2 ( Aij + Aji )
  void symmetric(Tensor& A, SymTensor& S);

  //! Compute the symmetric tensor Sij = 1/2 ( Aij + Aji )
  void symmetric(int start, int Nsize, CTensor& A, CSymTensor& S);

  //! Compute the symmetric tensor Sij = 1/2 ( Aij + Aji )
  void symmetric(int loc, CTensor& A, SymTensor& S);

  //! Compute the symmetric tensor Sij = 1/2 ( Aij + Aji )
  void symmetric(int loc, CTensor& A, Tensor& S);

  //****************************************************************************
  // DETERMINANT
  //! Compute the determinant of a matrix
  void determinant(Tensor& A, Real& Det);

  //! Compute the determinant of a matrix
  void determinant(int start, int Nsize, CTensor& A, Real* Det);

  //! Compute the determinant of a matrix
  void determinant(SymTensor& A, Real& Det);

  //! Compute the determinant of a matrix
  void determinant(int start, int Nsize, CSymTensor& A, Real* Det);

  //****************************************************************************
  // TRANSPOSE
  //! Compute the transpose of a matrix Cij = Aji
  void transpose(Tensor& A, Tensor& C);

  //! Compute the transpose of a matrix Cij = Aji
  void transpose(int start, int Nsize, CTensor& A, CTensor& C);

  //! Compute the transpose of a matrix Cij = Aji
  void transpose(AntiTensor& A, AntiTensor& C);

  //! Compute the transpose of a matrix Cij = Aji
  void transpose(int start, int Nsize, CAntiTensor& A, CAntiTensor& C);

  //! Compute the transpose of a matrix Cij = Aji
  void transpose(SymTensor& A, SymTensor& C);

  //! Compute the transpose of a matrix Cij = Aji
  void transpose(int start, int Nsize, CSymTensor& A, CSymTensor& C);

  //****************************************************************************
  // TRACE
  //! Compute the trace of a tensor sigma = Aii
  void trace(Tensor& A, Real& sigma);

  //! Compute the trace of a tensor sigma = Aii
  void trace(int start, int Nsize, CTensor& A, Real* sigma);

  //! Compute the trace of a tensor sigma = Aii
  void trace(SymTensor& A, Real& sigma);

  //! Compute the trace of a tensor sigma = Aii
  void trace(int start, int Nsize, CSymTensor& A, Real* sigma);

  //****************************************************************************
  // DIAGONA
  //! Compute the diagonal of a tensor Ci = Aij*Delta_ij
  void diagonal(Tensor& A, Vector& C);

  //! Compute the diagonal of a tensor Ci = Aij*Delta_ij
  void diagonal(int start, int Nsize, CTensor& A, CVector& C);

  //! Compute the diagonal of a tensor Ci = Aij*Delta_ij
  void diagonal(SymTensor& A, Vector& C);

  //! Compute the diagonal of a tensor Ci = Aij*Delta_ij
  void diagonal(int start, int Nsize, CSymTensor& A, CVector& C);

  //****************************************************************************
  // INVARIANTS

  //****************************************************************************
  // DEVIATOR TENSOR
  //! Compute the deviator of a tensor: Dij = Aij - (Akk/3)Delta_ij
  void deviator(Tensor& A, Tensor& D);

  //! Compute the symmetric deviator of a tensor:
  //! Sij = (Aij+Aji)/2 - (Akk/3)Delta_ij
  void symDeviator(Tensor& A, SymTensor& S);

  //****************************************************************************
  // SPECIAL OPERATIONS: G NEW NAME? G = I - F^-1

  //! Compute G tensor from deformation gradient
  void calcG(int start, int Nsize, CTensor& F, CTensor& G );

  //! Compute G tensor from deformation gradient
  void calcG(Tensor& F, Tensor& G);

  //****************************************************************************
  // TRANSFORM INTO NEW DATA TYPES

  //! Extract a Tensor from a Column Tensor
  void CTtoTensor(int id, CTensor& A, Tensor& C);

  //! Convert a Symmetric Tensor to a standard tensor
  void SymToTensor(SymTensor& A, Tensor& C);

  //! Convert an Anti-Symmetric Tensor to a standard tensor
  void AntiToTensor(AntiTensor& A, Tensor& C);

}

} // Hydra namespace
#endif // MathOp_h
