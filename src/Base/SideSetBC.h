//******************************************************************************
//! \file    src/Base/SideSetBC.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   SideSetBC is used to derive physics-specific sideset based BC's
//******************************************************************************
#ifndef SideSetBC_h
#define SideSetBC_h

#include <HydraTypes.h>
#include <BCPackage.h>
#include <DataContainer.h>
#include <UnsMesh.h>
#include <SetBC.h>

namespace Hydra {

//! This is the base class for all sideset-based boundary conditions
class SideSetBC : public SetBC {

  public:
    //@{
             SideSetBC() {}
             SideSetBC(const BCPackage& bc, int offset);
    virtual ~SideSetBC() {}
    //@}

    //! Check the external edges, return false if interior edge exists
    int checkExternalEdges(UnsMesh& mesh, int extNedge);

    //! Project surface data to nodes
    //! \param[in]  mesh     Reference to the mesh object
    //! \param[in]  t        Time
    //! \param[out] node_var Pointer to node variables
    //! \param[out] inverse_distance_wf inverse distance weighting function
    virtual void formInvDistOp(UnsMesh& mesh, const Real t, Real* node_var,
                               Real* inverse_distance_wf);

    //! Insert a nodal value for a BC into a nodal array
    virtual void insertNodalValue(UnsMesh& mesh, Real bcval, Real* var);

    //! Zero inverse distance operator for the surface
    void zeroInvDistOp(UnsMesh& mesh, Real* nvar, Real* invDistWF, int* iflag);

  protected:

    //! Convert default BC value into implementation-specific value
    //! \param[in] bcval Default BC value
    //! \param[in] gid   Element global id
    //! \return boundary condition value
    Real getBCvalue(Real& bcval, const int /*gid*/) {return bcval;}

    //! \name Data presented for the user-defined function
    //@{
    int m_elemId;      //!< Element Id for user access
    int m_edgeId;      //!< Dual-Edge Id for user access
    Real m_time;       //!< Time for velocity evaluation
    Vector m_coord;    //!< Dual-Edge face centroid coordinates
    Material* m_mat;   //!< Material class for user access
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    SideSetBC(const SideSetBC&);
    SideSetBC& operator=(const SideSetBC&);
    //@}

};

}

#endif // SideSetBC_h
