//******************************************************************************
//! \file    src/Base/QuadratureSet.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   Abstract base class for quadrature sets
//!
//! The derived quadrature sets are meant to be paired with an element
//! topology to form the overall quadrature methods for numerical integration.
//******************************************************************************
#ifndef QuadratureSet_h
#define QuadratureSet_h

#include <HydraTypes.h>

namespace Hydra {

// Abstract base class for quadrature sets
class QuadratureSet {

  public:

    //! \name Constructor/Destructor
    //@{
             QuadratureSet() {m_initialized = false;}
    virtual ~QuadratureSet() {}
    //@}

    //! Finalize phase
    virtual void finalize() = 0;

    //! Number of quadrature points for volume integration
    int getNumVolPts() {return m_Nqpt_vol;}

    //! Number of quadrature points for surface integration
    int getNumSurfPts() {return m_Nqpt_surf;}

    //! Initialize the quadrature set according to the quadrature rule
    virtual void initialize(int Nqpt_vol, int Nqpt_surf) = 0;

    //! Is the quadrature set initialized
    bool isInitialized() {return m_initialized;}

  protected:

    bool m_initialized; // Inidicates quadrature set is initialized

    int m_Nqpt_vol;     // No. of volume quadrature points
    int m_Nqpt_surf;    // No. of volume quadrature points

  private:

    //! Don't permit copy or assignment operators
    //@{
    QuadratureSet(const QuadratureSet&);
    QuadratureSet& operator=(const QuadratureSet&);
    //@}

};

}

#endif
