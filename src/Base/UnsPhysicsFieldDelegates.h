//******************************************************************************
//! \file    src/Base/UnsPhysicsFieldDelegates.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Field delegates for plot data
//******************************************************************************

#include <UnsPhysics.h>

namespace Hydra {

//! macro delarations for the field delegates

OUTPUT_DELEGATE_DECL(UnsPhysics,
                     writeScalarField,
                     UnsPhysicsScalarField)

OUTPUT_DELEGATE_DECL(UnsPhysics,
                     writeVectorField,
                     UnsPhysicsVectorField)

OUTPUT_DELEGATE_DECL(UnsPhysics,
                     writeElemProcessorIdField,
                     UnsPhysicsElemProcessorIdField)

OUTPUT_DELEGATE_DECL(UnsPhysics,
                     writeSymTensorField,
                     UnsPhysicsSymTensorField)

}

