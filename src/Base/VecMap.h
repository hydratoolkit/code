//******************************************************************************
//! \file    src/Base/VecMap.h
//! \author  Jozsef Bakosi, Mark A. Christon
//! \date    Fri Sep 28 11:50:34 2011
//! \brief   Map of vectors of boundary conditions, body forces, etc.
//******************************************************************************
#ifndef VecMap_h
#define VecMap_h

#include <iostream>
#include <vector>
#include <map>

#include <HydraTypes.h>

namespace Hydra {

//! Map of vectors of T*, where T are types of boundary conditions, forces, etc.
template<typename Key, class T>
class VecMap {

  public:

    typedef vector<T*> vector_type;
    typedef map<Key, vector_type> map_type;
    typedef typename map_type::size_type size_type;

    //! \name Constructor/Destructor
    //@{
    //! Constructor
             VecMap() {}
    virtual ~VecMap() {}
    //@}

    //! Init for Key = int
    //! \param[in]  f        field
    //! \param[in]  v        vector to insert to
    void init(int f, const vector_type& v, int, int) {
      m_mapvec.insert( make_pair(f, v) );
    }

    //! Init for Key = pair<int,int>(i,j) for half-sym storage i<j
    //! \param[in]  f        field
    //! \param[in]  v        vector to insert to
    //! \param[in]  nfields  number of fields
    void init(int f, const vector_type& v, int nfields, pair<int,int>) {
      for (int j=0; j<nfields; j++)
        if (j<f) m_mapvec.insert( make_pair(Key(j,f), v) );
    }

    //! Fill/initialize the map with nfields empty vectors
    //! \param[in]  nfields  Number of field-entries to create
    void initialize(const int nfields) {
      m_Nfield = nfields;       // Store own copy of number of fields
      vector_type emptyVec;     // empty vector of T*s
      for (int i=0; i<nfields; i++)
        init(i, emptyVec, nfields, Key());
    }

    //! Free VecMap
    void clear() {
      if (!m_mapvec.size()) return;
      for (m_cit=m_mapvec.begin(); m_cit!=m_mapvec.end(); m_cit++) {
        const vector_type* v = &m_cit->second;
        const int numvec = v->size();
        for (int i=0; i<numvec; i++)
          delete (*v)[i];
      }
      m_mapvec.clear();
    }

    //! Return number of fields
    int fields() const {
      return m_mapvec.size();
    }

    //! Return number of entries (of all fields in the map)
    size_type size() {
      size_type num = 0;
      for (m_cit=m_mapvec.begin(); m_cit!=m_mapvec.end(); m_cit++) {
        num += (m_cit->second).size();
      }
      return num;
    }

    //! Query memory used
    //! \param[out] mem     Accumulate memory usage in bytes in mem
    void memory(Real& mem) {
      for (m_cit=m_mapvec.begin(); m_cit!=m_mapvec.end(); m_cit++) {
        const vector_type* v = &m_cit->second;
        const int numvec = v->size();
        for (int i=0; i<numvec; i++)
          mem += (*v)[i]->getMemory();
      }
    }

    //! Find a vector for a given field
    //! \param[in]  field   Field id
    vector_type& find(const Key& field) {
      if ((m_it = m_mapvec.find(field)) == m_mapvec.end()) {
        cout << "ERROR: Field " << field << " not found in find() of VecMap"
             << endl;
        return m_mapvec.begin()->second;
      } else {
        return m_it->second;
      }
    }

    //! Remove duplicate entries (keep the latest)
    //! Needs custom comparator dereferencing the pointer to work properly -JB
    void pack() {
      for (m_it=m_mapvec.begin(); m_it!=m_mapvec.end(); m_it++) {
        vector_type* v = &m_it->second;
        sort(v->begin(), v->end());
        reverse(v->begin(), v->end());
        unique(v->begin(), v->end());
      }
    }

    //! Const accessor to mapvec
    const map_type& getMap() const { return m_mapvec; }

  protected:

    map_type m_mapvec;
    typename map_type::const_iterator m_cit;
    typename map_type::iterator m_it;

    int m_Nfield;               //! Store own copy of number of fields

  private:

    //! Don't permit copy or assignment operators
    //@{
    VecMap(const VecMap&);
    VecMap& operator= (const VecMap&);
    //@}
};

}

#endif
