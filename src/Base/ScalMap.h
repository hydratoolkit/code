//******************************************************************************
//! \file    src/Base/ScalMap.h
//! \author  Jozsef Bakosi
//! \date    Fri Sep 28 11:50:34 2011
//! \brief   Map of scalars of type T
//******************************************************************************
#ifndef ScalMap_h
#define ScalMap_h

#include <UnsPhysics.h>

namespace Hydra {

//! Map of scalar of T, where T is a type of initial conditions, etc.
template<class T>
class ScalMap {

  public:

    typedef T scalar_type;
    typedef map<int,scalar_type> map_type;
    typedef typename map_type::size_type size_type;
    typedef typename map<int,scalar_type>::const_iterator const_iterator;
    typedef typename map<int,scalar_type>::iterator iterator;

    //! \name Constructor/Destructor
    //@{
             ScalMap() {};
    virtual ~ScalMap() {};
    //@}

    //! Fill/initialize the map with nfields empty scalars
    //! \param[in]  nfields  Number of field-entries to create
    void initialize(const int nfields) {
      scalar_type emptyScal;   // fields of scalar T will be uninitialized!
      for (int i=0; i<nfields; i++)
        m_mapscal.insert(pair<int,scalar_type>(i, emptyScal));
    }

    //! Set entry for all fields
    //! \param[in] s      Reference to object to insert
    //! \param[in] field  vector<int> with field ids this scalar relates to
    void set(const scalar_type& s, const vector<int>& field) {
      const int nfields = field.size();
      for (int i=0; i<nfields; i++)
        m_mapscal[field[i]] = s;
    }

    //! const map begin/end accessors
    const_iterator begin() { return m_mapscal.begin(); }
    const_iterator   end() { return m_mapscal.end(); }

    //! Free ScalMap
    void clear() { m_mapscal.clear(); }

    //! Return number of fields
    int fields() { return m_mapscal.size(); }

    //! Return number of entries (alias, since fields = size in map of scalars)
    int size() { return m_mapscal.size(); }

    //! Safe [] operator picking out a scalar for a given field
    //! \param[in] field  Requested field id
    scalar_type& operator[] (const int field) {
      if ((m_it = m_mapscal.find(field)) == m_mapscal.end()) {
        cout << "ERROR: Field " << field
             << " not found in operator [] of ScalMap" << endl;
        return m_mapscal.begin()->second;
      } else {
        return m_it->second;
      }
    }

  private:

    //! Don't permit copy or assignment operators
    //@{
    ScalMap(const ScalMap&);
    ScalMap& operator= (const ScalMap&);
    //@}

    map_type m_mapscal;
    iterator m_it;

};

}

#endif
