//******************************************************************************
//! \file    src/Base/InterfaceTracker.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Implements interface tracking using the FronTier code
//******************************************************************************

#ifndef InterfaceTracker_h
#define InterfaceTracker_h

#include <Interface.h>
#include <UnsMesh.h>

#ifdef FRONTIER
// This is done in the build process for FronTier
#include "FronTier.h"
#define float double
#ifndef TWOD
#define TWOD
#endif
#include "FronTier.h"
#undef float

#endif

namespace Hydra {

#define GV_PATH_LENGTH 128

//! This class implements interface tracking using Frontier
class InterfaceTracker : public Interface {
  public:
    //! \name Constructor/Destructor
    //@{
             InterfaceTracker() {}
    virtual ~InterfaceTracker() {}
    //@}

    //! \name InterfaceTracker access functions
    //! The InterfaceTracker class is used to setup physics-specific ...
    //@{
    void setup(UnsMesh *mesh); // called from physics setup function
    //@}


    //! \name 2-D InterfaceTracker example interfaces
    //! These functions create different interfaces with FronTier for
    //! testing purposes
    //@{
    void createCircle(Real x_cen, Real y_cen, Real radius, int num_pts);
    void createEllipse(Real x_radius, Real y_radius, Real x_cen, Real y_cen);
    void createLine(Real x0, Real y0, Real x1, Real y1, int num_pts);
    void setBoundary();
    //@}

    // 3D
    //! \name 3-D InterfaceTracker example interfaces
    //! These functions create different interfaces with FronTier for
    //! testing purposes
    //@{
    void createSphere(Real x0, Real y0, Real z0, Real radius);
    //@}

    //! \name Dynamic interface movement and tracking routines
    //! These functions move the interface
    //@}

    //! \name Functions for IO
    //! These routines output the interface information
    //@{
    //! Geometry output function - output for GeomView
    void createGeomViewFile(char *dirname, char *intfc_name);

    //! Create Geomview files from a timestep
    void outputGeomViewTimeStep();
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    InterfaceTracker(const InterfaceTracker&);
    InterfaceTracker& operator=(const InterfaceTracker&);
    //@}

#ifdef FRONTIER
    void initializeFrontier(int dim, Real lower[], Real upper[],
                            Real grid_size);
    void initializeInterface();
    void getBoundingBox(UnsMesh *mesh,
                        Real &min_x, Real &max_x,
                        Real &min_y, Real &max_y,
                        Real &min_z, Real &max_z);

     F_INIT_DATA Init;
     Front front;
     RECT_GRID comp_grid;
     RECT_GRID top_grid;
     char geomview_dirname[GV_PATH_LENGTH];
     char geomview_intfc_name[GV_PATH_LENGTH];
#endif

};

}

#endif // InterfaceTracker_h
