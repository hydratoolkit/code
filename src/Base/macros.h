//******************************************************************************
//! \file    src/Base/macros.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Define general-use macros in Hydra
//******************************************************************************
#ifndef macros_h
#define macros_h

//! min/max definitions
#define amin(a, b) ((a) >= (b) ? (b) : (a))
#define amax(a, b) ((a) >= (b) ? (a) : (b))

#endif // macros_h
