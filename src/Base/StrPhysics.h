//******************************************************************************
//! \file    src/Base/StrPhysics.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:35 2011
//! \brief   Base physics class for deriving structured-mesh physics
//******************************************************************************

#ifndef StrPhysics_h
#define StrPhysics_h

#include <Control.h>
#include <fileIO.h>
#include <StrMesh.h>
#include <Physics.h>

namespace Hydra {

//! Base class used to construct the solution algorithms for a given phsyics
class StrPhysics : public Physics {

  public:

    //! \name Constructor/Destructor
    //@{
             StrPhysics();     //!< Constructor for StrPhysics class
    virtual ~StrPhysics() {}   //!< Destructor  for StrPhysics class
    //@}

    //! \name Virtual Read Restart
    //! Each physics that is implemented will require its own
    //! restart reader. The restart reader function is implemented
    //! for each physics class to enable custom restart readers.
    //@{
    virtual void readRestart();
    //@}

    //! \name Virtual Object Storage
    //! Each physics based on a specific mesh type can store off
    //! it's mesh pointer, control pointer and i/o pointer
    //@{
    virtual void setObj(StrMesh*, Control*, fileIO*, Timer*) {
      cout << "!!! StrPhysics::setObj() not implemented !!!" << endl;
    }

    //! No setObj(UnsMesh*) in StrPhysics
    virtual void setObj(UnsMesh*, Control*, fileIO*, Timer*) {
      cout << "!!! No setObj(UnsMesh*) in StrPhysics(UnsMesh*, ...) !!!" << endl;
    }
    //@}

    //! \name Virtual StrPhysics Functions
    //! These functions are pure virtual functions that need to
    //! be implemented for each specific type of physics being solved
    //! with the framework.
    //@{
    //! Virtual setup function -- implemented for each specific physics
    virtual void setup();
    //@}

    //! \name Virtual Data Registration
    //! Each physics that is implemented will require its own
    //! storage and specific variables.  The data registration
    //! function is implemented for each physics class to enable
    //! custom definition of variables for each algorithm that's
    //! implemented.
    //@{
    virtual void registerData() {}
    //@}

  protected:

    //! \name Objects used - NOT owned
    //@{
    StrMesh* mesh;     //!< The finite element mesh
    Control* control;  //!< The Control object holds all of the control info.
    fileIO*  io;       //!< The fileIO object is used for all I/O operations
    Timer*   timer;    //!< The Timer object for all timing data
    //@}

  private:

    // Don't permit copy or assignment operators
    //! Don't permit copy or assignment operators
    //@{
    StrPhysics(const StrPhysics&);
    StrPhysics& operator=(const StrPhysics&);
    //@}

};

}

#endif // StrPhysics_h
