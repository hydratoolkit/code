//******************************************************************************
//! \file    src/Base/DataShapes.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Data shaper for physics-oriented arrays
//******************************************************************************

#ifndef DataShapes_h
#define DataShapes_h

#include <HydraTypes.h>

namespace Hydra {

//! Data Shape component definition
#define SCALAR_COMPONENTS 1
#define VECTOR_COMPONENTS 3
#define TENSOR_COMPONENTS 9
#define SYMTENSOR_COMPONENTS 6
#define ANTITENSOR_COMPONENTS 6
#define QUATERNION_COMPONENTS 4

//******************************************************************************
//! \name Basic data shapes
//! These simple algebraic types are simply to be used as a convience for
//! typing first and second-rank tensors
//@{
//! Vector (1rst-Rank Tensor)
struct Vector {
  Real X;
  Real Y;
  Real Z;
};

//! 2nd-Rank Anti-Symmetric Cartesian Tensor
struct AntiTensor {
  Real XY;
  Real YZ;
  Real ZX;
};

//! 2nd-Rank Symmetric Cartesian Tensor
struct SymTensor {
  Real XX;
  Real YY;
  Real ZZ;
  Real XY;
  Real YZ;
  Real ZX;
};

//! 2nd-Rank Cartesian Tensor
struct Tensor {
  Real XX;
  Real YY;
  Real ZZ;
  Real XY;
  Real YZ;
  Real ZX;
  Real YX;
  Real ZY;
  Real XZ;
};

//! Quaternion 4-Vector
struct Quaternion {
  Real X;
  Real Y;
  Real Z;
  Real Q;
};
//@}

//******************************************************************************
//! \name Column oriented data shapes
//! Column Data Shapers for applications that use the DataMesh object
//! These algebraic types are meant to be used with variable registration
//! for data intensive operations in the flow solver.
//@{
//!Vector (1rst-Rank Tensor)
struct CVector {
  int   N;
  Real* X;
  Real* Y;
  Real* Z;
};

//! 2nd-Rank Anti-Symmetric Cartesian Tensor
struct CAntiTensor {
  int    N;
  Real *XY;
  Real *YZ;
  Real *ZX;
};

//! 2nd-Rank Symmetric Cartesian Tensor
struct CSymTensor {
  int    N;
  Real *XX;
  Real *YY;
  Real *ZZ;
  Real *XY;
  Real *YZ;
  Real *ZX;
};

//! 2nd-Rank Cartesian Tensor
struct CTensor {
  int    N;
  Real *XX;
  Real *YY;
  Real *ZZ;
  Real *XY;
  Real *YZ;
  Real *ZX;
  Real *YX;
  Real *ZY;
  Real *XZ;
};

//! Quaternion 4-Vector
struct CQuaternion {
  int   N;
  Real *X;
  Real *Y;
  Real *Z;
  Real *Q;
};

//! Bool Vector
struct CBoolVector{
  int   N;
  bool *X;
  bool *Y;
  bool *Z;
};
//@}

//******************************************************************************
//! \name
//! Row Data Shapers for CFD applications that use the DataMesh object
//! These algebraic types are meant to be used with variable registration
//! for data intensive operations in the flow solver.
//@{
//! Vector (1rst-Rank Tensor)
struct RVector {
  Real X;
  Real Y;
  Real Z;
};

//! 2nd-Rank Anti-Symmetric Cartesian Tensor
struct RAntiTensor {
  Real XY;
  Real YZ;
  Real ZX;
};

//! 2nd-Rank Symmetric Cartesian Tensor
struct RSymTensor {
  Real XX;
  Real YY;
  Real ZZ;
  Real XY;
  Real YZ;
  Real ZX;
};

//! 2nd-Rank Cartesian Tensor
struct RTensor {
  Real XX;
  Real YY;
  Real ZZ;
  Real XY;
  Real YZ;
  Real ZX;
  Real YX;
  Real ZY;
  Real XZ;
};

//! Quaternion 4-Vector
struct RQuaternion {
  Real X;
  Real Y;
  Real Z;
  Real Q;
};
//@}

}

#endif // DataShapes_h
