//******************************************************************************
//! \file    src/Base/Polynomial.h
//! \author  Mark A. Christon
//! \date    Thu Sep 17 16:21:12 MDT 2020
//! \brief   The polynomial class provides a polynomial function interface
//******************************************************************************

#ifndef Polynomial_h
#define Polynomial_h

#include <string>
#include <vector>
#include <HydraTypes.h>

namespace Hydra {

// Polynomial class for interplation of tabular data
class Polynomial {

  public:

    //! \name Constructor/Destructor
    //@{
             Polynomial() {}
             Polynomial(int pid, const vector<Real> coeff, Real minv, Real maxv);
    virtual ~Polynomial();
    //@}

    //! \name polynomial interface
    //@{
    int  Id()             {return id;  } //!< Return the polynomial id
    int  degree()         {return Ndeg;} //!< Return the polynomial degree

    Real evaluate(Real x);  //!< Evaluate the polynomial

    //! Echo polynomial options
    void echoOptions(ostream& ofs);
    //@}

  protected:

  private:

    //! Don't permit copy or assignment operators
    //@{
    Polynomial(const Polynomial&);
    Polynomial& operator=(const Polynomial&);
    //@}

    bool hasBounds;  //!< Bounds are available for min/max
    int   id;        //!< Polynomial id
    int Ndeg;        //!< Polynomial degree
    Real pmin;       //!< Minimum bounding value
    Real pmax;       //!< Maximum bounding value
    vector<Real> a;  //!< Array of polynomial coefficients
};

}

#endif // Polynomial_h
