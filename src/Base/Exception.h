//******************************************************************************
//! \file    src/Base/Exception.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Exception handler class to handle a message
//******************************************************************************
#ifndef Exception_h
#define Exception_h

#include <string>

namespace Hydra {

//! Exception Handler Interface
class Exception {

  public:

    explicit Exception(const string& error) : m_error(error) {}

    const string& getError() const { return m_error; }

  private:

    const string m_error;

};

}

#endif // Exception_h
