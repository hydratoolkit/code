# General macros
# Defines         | Meaning
#-----------------|-------------------------------------------------------------
# -DDBL           | double-precision for Real
# -DEXODUS_IO     | turn on Exodus-II output
# -DMPC           | build for a message-passing architecture
# -DUSE_MPI       | turn on MPI for message passing
# -DUSE_NX        | turn on NX for message passing
# -DSTAT_DEBUG    | turn on statistics debugging (extra output on statistics)
# -DUSE_FENV      | turn on floating point error trapping
# -DUSE_CATALYST  | Use the ParaView/Catalyst library
# -DUSE_PARMETIS  | Use the Metis/ParMetis libraries
# -DFRONTIER      | Activate the FontTier front-tracking code
#
# Debugging macros
# Defines         | Meaning
#-----------------|-------------------------------------------------------------
# -DDEBUG         | conditionally compiled debug statements
# -DSTAT_DEBUG    | turn on statistics debugging (extra tty output on statistics)
# -DSURFCHEM_DEBUG| conditionally compiled debugging for surface chemistry
#
# Defines         | Meaning
#-----------------|-------------------------------------------------------------
# -DHYDRA_F77BLAS | turn on old F77 BLAS routines in LinearAlgebra/util.F
#
# Control linking fortran
# Defines         | Meaning
#-----------------|-------------------------------------------------------------
# -DHYDRAF77_BLAS | -- activate F77_BLAS routines
#
# Control linking fortran
# Defines         | Meaning
#-----------------|-------------------------------------------------------------
# -DLC_FLINC      | #define FORTRAN(subname) subname##_
# -DLCFLINK       | #define FORTRAN(subname) subname
# -DGNU           | #define FORTRAN(subname) subname##_
#
# Cache Blocking
# Defines         | Meaning
#-----------------|-------------------------------------------------------------
# -DIBSZ=32       | Set's the cache block size to 32 for various physics
#
# MPI Specific
# Defines         | Meaning
#-----------------|-------------------------------------------------------------
# -DOMPI_SKIP_MPICXX, -DMPIPP_H -- skip the C++ interface
#
# Platform specific
# Defines         | Meaning
#-----------------|-------------------------------------------------------------
# -DMACOS         | Turn on MacOSX specific timing routines

# Basic defines
# add_definitions(-DDBL -DGNU -DMPC -DUSE_MPI -DOMPI_SKIP_MPICXX -DMPIPP_H -DEXODUS_IO -DIBSZ=32)
add_definitions(-DDBL -DGNU -DMPC -DUSE_MPI -DOMPI_SKIP_MPICXX -DMPIPP_H -DEXODUS_IO -DIBSZ=64)

# Append for MACOSX -- performance.C only
if (${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
  add_definitions(-DMACOSX)
endif (${CMAKE_SYSTEM_NAME} MATCHES "Darwin")

