//******************************************************************************
//! \file    src/DataMesh/Quad4QuadSet.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   4-node Quadrilateral element quadrature set
//******************************************************************************
#ifndef Quad4QuadSet_h
#define Quad4QuadSet_h

#include <QuadratureSet.h>

namespace Hydra {

//! 4-node Quadrilateral element quadrature set
class Quad4QuadSet : public QuadratureSet {

  public:

    //! \name Constructor/Destructor
    //@{
             Quad4QuadSet();
    virtual ~Quad4QuadSet();
    //@}

    //! Initialize the quadrature set according to the quadrature rule
    virtual void initialize(int Nqpt_vol, int Nqpt_surf);

    //! Finalize phase
    virtual void finalize();

    int getNumVolPts() {return m_Nqpt_vol;}

    int getNumSurfPts() {return m_Nqpt_surf;}

    const Real* getVsfArray(int qdim, int kpt) const {
      return &m_vsf[qdim][kpt][0];
    }

  protected:

    int m_Nqpt_vol;  // No. of volume quadrature points
    int m_Nqpt_surf; // No. of volume quadrature points

  private:

    //! Don't permit copy or assignment operators
    //@{
    Quad4QuadSet(const Quad4QuadSet&);
    Quad4QuadSet& operator=(const Quad4QuadSet&);
    //@}

    Real     sf(Real xi, Real eta, int node);
    Real   sfxi(Real xi, Real eta, int node);
    Real  sfeta(Real xi, Real eta, int node);

    //! Shape functions, derivatives and quadrature weights
    //! vsf[Nqdim][Nqpt][Nnpe]
    Real m_vsf[3][4][4];

    //! Natural coordinates
    //! qpt[Nqpt][Ndim]
    Real m_vqpt[4][2];

    //! Weights for 2x2x2 quadrature are 1.0... so ignore for now (MAC)

};

}

#endif
