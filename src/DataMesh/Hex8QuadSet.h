//******************************************************************************
//! \file    src/DataMesh/Hex8QuadSet.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   8-node hex quadrature set
//******************************************************************************
#ifndef Hex8QuadSet_h
#define Hex8QuadSet_h

#include <QuadratureSet.h>

namespace Hydra {

//! Hex-8 Quadrature Set
class Hex8QuadSet : public QuadratureSet {

  public:

    //! \name Constructor/Destructor
    //@{
             Hex8QuadSet();
    virtual ~Hex8QuadSet();
    //@}

    //! Initialize the quadrature set according to the quadrature rule
    virtual void initialize(int Nqpt_vol, int Nqpt_surf);

    //! Finalize phase
    virtual void finalize();

    int getNumVolPts() {return m_Nqpt_vol;}

    int getNumSurfPts() {return m_Nqpt_surf;}

    const Real* getVsfArray(int qdim, int kpt) const {
      return &m_vsf[qdim][kpt][0];
    }

  protected:

    int m_Nqpt_vol;  // No. of volume quadrature points
    int m_Nqpt_surf; // No. of volume quadrature points

  private:

    //! Don't permit copy or assignment operators
    //@{
    Hex8QuadSet(const Hex8QuadSet&);
    Hex8QuadSet& operator=(const Hex8QuadSet&);
    //@}

    Real     sf(Real xi, Real eta, Real zeta, int node);
    Real   sfxi(Real xi, Real eta, Real zeta, int node);
    Real  sfeta(Real xi, Real eta, Real zeta, int node);
    Real sfzeta(Real xi, Real eta, Real zeta, int node);

    //! Shape functions, derivatives and quadrature weights
    //! vsf[Nqdim][Nqpt][Nnpe]
    Real m_vsf[4][8][8];

    //! Natural coordinates
    //! qpt[Nqpt][Ndim]
    Real m_vqpt[8][3];

    //! Weights for 2x2x2 quadrature are 1.0... so ignore for now (MAC)

};

}

#endif
