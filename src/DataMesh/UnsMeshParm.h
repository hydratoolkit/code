//******************************************************************************
//! \file    src/DataMesh/UnsMeshParm.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   Struct used ot initialize mesh parameters
//******************************************************************************
#ifndef UnsMeshParm_h
#define UnsMeshParm_h

namespace Hydra {

struct UnsMeshParm {
  //!  \name Unstructured Mesh Parameters
  //@{
  int Ndim;           //!< Dimension of mesh
  // --- MPC specific ---
  int Nnp_g;          //!< Number of nodes               (global)
  int Nel_g;          //!< Total Number of elements      (global)
  int Nel_poly2d_g;   //!< Number of POLY2D elements     (global)
  int Nel_quad4_g;    //!< Number of QUAD4  elements     (global)
  int Nel_tri3_g;     //!< Number of TRI3   elements     (global)
  int Nel_hex8_g;     //!< Number of HEX8   elements     (global)
  int Nel_poly3d_g;   //!< Number of POLY3D elements     (global)
  int Nel_pyr5_g;     //!< Number of PYR5   elements     (global)
  int Nel_tet4_g;     //!< Number of TET4   elements     (global)
  int Nel_wedge6_g;   //!< Number of WEDGE6 elements     (global)
  int Nnp_int;        //!< Number of internal nodes      (local )
  int Nnp_brdr;       //!< Number of border nodes        (local )
  int Nnp_ext;        //!< Off-processor ghost nodes     (local )
  int Nel_int;        //!< Number of internal elements   (local )
  int Nel_brdr;       //!< Number of border elements     (local )
  int Nel_ext;        //!< Off-processor ghost elements  (local )
  // --- MPC specific ---
  int Nnp;            //!< Number of nodes               (local )
  int Nel;            //!< Total Number of elements      (local )
  int Nel_poly2d;     //!< Number of POLY2D elements     (local )
  int Nel_quad4;      //!< Number of QUAD4  elements     (local )
  int Nel_tri3;       //!< Number of TRI3   elements     (local )
  int Nel_hex8;       //!< Number of HEX8   elements     (local )
  int Nel_poly3d;     //!< Number of POLY3D elements     (local )
  int Nel_pyr5;	      //!< Number of PYR5   elements     (local )
  int Nel_tet4;	      //!< Number of TET4   elements     (local )
  int Nel_wedge6;     //!< Number of WEDGE6 elements     (local )
  int Nedge;          //!< Total number of dual edges    (local )
  int Nedge_int;      //!< Number of internal dual edges (local )
  int Nedge_ext;      //!< Number of external dual edges (local )
  int Nedge_ghost;    //!< Number of true ghost edges    (local )
  int Neblock;        //!< Number of element blocks      (global)
  int Nel_sets;       //!< No. of element sets           (global)
  int Nnd_sets;       //!< Number of nodesets            (global)
  int Nsd_sets;       //!< Number of sidesets            (global)
  //@}
};

}

#endif
