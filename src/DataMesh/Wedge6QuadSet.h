//******************************************************************************
//! \file    src/DataMesh/Wedge6QuadSet.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   6-node wedge element quadrature set
//******************************************************************************
#ifndef Wedge6QuadSet_h
#define Wedge6QuadSet_h

#include <QuadratureSet.h>

namespace Hydra {

//! 6-Node Wedge Element Quadrature Set
class Wedge6QuadSet : public QuadratureSet {

  public:

    //! \name Constructor/Destructor
    //@{
             Wedge6QuadSet();
    virtual ~Wedge6QuadSet();
    //@}

    //! Initialize the quadrature set according to the quadrature rule
    virtual void initialize(int Nqpt_vol, int Nqpt_surf);

    //! Finalize phase
    virtual void finalize();

    int getNumVolPts() {return m_Nqpt_vol;}

    int getNumSurfPts() {return m_Nqpt_surf;}

    const Real* getVsfArray(int qdim, int kpt) const {
      return &m_vsf[qdim][kpt][0];
    }

  protected:

    int m_Nqpt_vol;  //!< No. of volume quadrature points
    int m_Nqpt_surf; //!< No. of volume quadrature points

  private:

    //! Don't permit copy or assignment operators
    //@{
    Wedge6QuadSet(const Wedge6QuadSet&);
    Wedge6QuadSet& operator=(const Wedge6QuadSet&);
    //@}

    Real     sf(Real xi, Real eta, Real zeta, int node);
    Real   sfxi(Real xi, Real eta, Real zeta, int node);
    Real  sfeta(Real xi, Real eta, Real zeta, int node);
    Real sfzeta(Real xi, Real eta, Real zeta, int node);

    //! Shape functions, derivatives and quadrature weights
    //! vsf[Nqdim][Nqpt][Nnpe]
    Real m_vsf[4][2][6];

    //! Natural coordinates
    //! qpt[Nqpt][Ndim]
    Real m_vqpt[2][3];

    //! Weights for basic quadrature are 1.0... so ignore for now (MAC)

};

}

#endif
