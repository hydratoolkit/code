//******************************************************************************
//! \file    src/DataMesh/DualEdge.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   Dual edge structure
//******************************************************************************
#ifndef DualEdge_h
#define DualEdge_h

namespace Hydra {

//! \name DualEdgeType
//! This enum defines the various dual-edge types in the dual-edge mesh
//! for construction of least-squares gradients
//!
//! INTERIOR_EDGES can be purely interior, or align at an interface (IFC)
//! EXTERIOR_EDGES can be only external boundary edges
//! INTERPOC_EDGES are at processor boundaries, but can also be an IFC
enum DualEdgeType {
  INTERIOR_EDGE,  //!< Interior dual-edge
  EXTERIOR_EDGE,  //!< External dual-edge (only on the domain boundary)
  INTERPROC_EDGE  //!< Interprocessor dual-edge -- shared between proc's
};

//! \name DualEdge structure
//! This structure holds the relevant information for dual-edges used in
//! the C-Law formulation
struct DualEdge {
  bool ifc;          //!< True for an edge with an interface constraint (solid-owned)
  bool ifcbc;        //!< True for an edge with an interface BC (fluid-owned)
  int ecid1;         //!< Element class id
  int gid1;          //!< Global element id (zero based)
  int side1;         //!< Side number
  int mid1;          //!< Material id for side-1
  int ecid2;         //!< Element class id
  int gid2;          //!< Global element id (zero based)
  int side2;         //!< Side number
  int mid2;          //!< Material id for side-2 (zero for ghost data)
  Real en[3];        //!< Surface normal
  Real xs[3];        //!< Surface centroid
  Real gamma;        //!< Surface area (or length in 2D)
  DualEdgeType type; //!< Type of dual-edge: INTERIOR, EXTERIOR or INTERPROC
};

//! \name DualCutEdge
//! This structure holds all relevant data for cut-edges used in the
//! pressure-based and density-based flow solvers
struct DualCutEdge {
  bool has_cut;    //!< Face is cut by a fluid-solid interface
  int  src_gid;    //!< Global element id for source data for ghost data
  Real xi[3];      //!< Fluid-solid interface location
  Real ni[3];      //!< Normal to the interface at xi
  Real xe[3];      //!< Exact fluid-solid interface locations (error cals)
  Real xp[3];      //!< Mirrored location of the ghost centroid in fluid
};

}

#endif // DualEdge_h
