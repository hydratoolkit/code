//******************************************************************************
//! \file    src/DataMesh/Wedge6.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   6-node wedge element
//******************************************************************************
#ifndef Wedge6_h
#define Wedge6_h

#include <Element.h>
#include <Wedge6QuadSet.h>

namespace Hydra {

//! 6-Node wedge element
#define NDIM_WEDGE6        3   //!< Spatial dimensions
#define NNPE_WEDGE6        6   //!< No. of nodes per element
#define NUM_WEDGE6_EDGES   9   //!< No. of edges per element
#define NNPE_WEDGE6_EDGES  2   //!< No. of nodes per Wedge6 edge
#define NUM_WEDGE6_FACES   5   //!< No. of faces per element
#define NNPE_WEDGE6_FACES  4   //!< No. of nodes per Wedge6 face (Maximum)

struct Wedge6IX {
  int ix[NNPE_WEDGE6];  //!< Connectivity
};

struct Wedge6Element {
  int ix[NNPE_WEDGE6];  //!< Connectivity
  int mat;              //!< Material Id
  int oid;              //!< Ordinal Id
  int label;            //!< User label
};

struct Wedge6Bmatrix {
  Real X[NNPE_WEDGE6];
  Real Y[NNPE_WEDGE6];
  Real Z[NNPE_WEDGE6];
};

struct Wedge6DualEdgeIds {
  int eid[NUM_WEDGE6_FACES];  //!< Dual-mesh edge Id's -- one per face
};

//! 6-node wedge element
class Wedge6: public Element {

  public:

    //! \name Constructor/Destructor
    //@{
             Wedge6(int ecid, int nel);
    virtual ~Wedge6();
    //@}

    //! Setup dual-grid adjacency for ghost
    virtual int addGhostAdjacency(UnsHashMap& hmap, int ecid, int is,
                                  int matid, int proc, int* ghost_conn);

    //! Calculate the B-matrix
    virtual int calcBmatrix(const CVector& coord, Real* V);

    //! Calculate the element centroid
    virtual void calcCentroid(const CVector& coord, CVector& cent);

    //! Calculate the dual-edge data
    virtual void calcDualEdgeData(DualEdge* edge, const CVector& coord);

    //! Calculate the element quality metrics
    virtual void calcMetrics(CVector& /* coord */);

    //! Calculate surface integral and assemble to global array
    virtual void surfaceIntegral(int gid, int side, Real wt, CVector& coord,
                                 Real* rhs);

    //! Calculate the element volume
    virtual bool calcVolume(const CVector& coord, Real* v);

    //! Echo the nodal connectivity
    virtual void echoConnectivity(ostream& ofs);

    //! Return the number of edges per element
    virtual int getEdgesPerElement() {return NUM_WEDGE6_EDGES;}

    //! Get the edge connectivity, i.e., 2-nodes
    virtual void getEdgeConnectivity(int lid, int eid, int& n1, int& n2);

    //! Get the face connectivity
    virtual void getFaceConnectivity(int lid, int face, int* conn, int* lconn);

    //! Return the number of nodes per edge given an edgeId
    virtual int getNodesPerEdge(int /*edgeId*/) const {
      return NNPE_WEDGE6_EDGES;
    }

    //! Return the number of nodes per face give a face Id
    virtual int getNodesPerFace(int faceId) const {
      return m_nnpeFace[faceId];
    }

    //! Quadrature-set is initialized and ready to use?
    virtual bool hasQuadSet() {return m_w6qs.isInitialized();}

    //! Inside element test
    virtual bool inElement(int lid, const Vector pt, CVector& coord);

    //! Register the B-matrix
    virtual void registerBmatrix();

    //! Set the dual edge information in a given element
    virtual void registerDualEdgeIds();

    //! Set the dual edge information in a given element
    virtual void setDualEdgeId(int lid, int side, int eid);

    //! Get the global dual-edge ID for a given global element ID & side
    virtual int getDualEdgeId(int gid, int side);

    //! Setup dual-grid adjacency
    virtual int setupAdjacency(UnsHashMap& hmap, int ecid);

    //! Setup edge-edge list for ghosts
    virtual void setupGhostEdgeList(UnsHashMap& hmap, int proc,
                                    int* el_conn, int& curlen, int* local_list);

    //! Set the size of the Hex8 element for communications
    virtual int sizeofElement() {return sizeof(Wedge6Element);}

    //**************************************************************************
    //! \name Element specific return types
    //{@

    //! Shape the B-matrix for direct calculation
    Wedge6Bmatrix* getWedge6Bmatrix();

    //! Return dual-edge Id's for element class
    Wedge6DualEdgeIds* getWedge6DualEdgeIds() {
      return reinterpret_cast<Wedge6DualEdgeIds*>(getDualEdgeIds());
    }

    //! Return element connectivity shaped for Wedge6
    Wedge6IX* getWedge6Connectivity() {
      return reinterpret_cast<Wedge6IX*>(getConnectivity());
    };


    //! Return quadrature set -- may not be initialized
    Wedge6QuadSet& getWedge6QuadSet() {return m_w6qs;}

    //}@
    /**************************************************************************/

  private:

    //! Don't permit copy or assignment operators
    //@{
    Wedge6(const Wedge6&);
    Wedge6& operator=(const Wedge6&);
    //@}

    //! Typically initialized with registration of the B-matrix
    Wedge6QuadSet m_w6qs;

    static int m_edges[NUM_WEDGE6_EDGES][NNPE_WEDGE6_EDGES]; //<! Local edges
    static int m_faces[NUM_WEDGE6_FACES][NNPE_WEDGE6_FACES]; //<! Local faces
    static int m_nnpeFace[NUM_WEDGE6_FACES];  //<! # of nodes per local face

};

}

#endif
