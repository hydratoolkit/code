//******************************************************************************
//! \file    src/DataMesh/Poly2D.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   2-D polygonal element
//******************************************************************************
#ifndef Poly2D_h
#define Poly2D_h

#include <Element.h>
#include <FaceConnectivity.h>

namespace Hydra {

//! 2-D polygonal element
#define NDIM_POLY2D        2  //!< Spatial dimensions
#define NNPE_POLY2D        10 //!< No. of nodes per element (maximum)
#define NUM_POLY2D_EDGES   10 //!< No. of edges per element (maximum)
#define NNPE_POLY2D_EDGES  2  //!< No. of nodes per poly2d edge
#define NUM_POLY2D_FACES   1  //!< No. of faces per element
#define NNPE_POLY2D_FACES  10 //!< No. of nodes per poly2d face (maximum)

//! Note that the maximum number of nodes per polygon is set at an
//! upper limit of 10 for the load balancer.  This will be resolved
//! in a later update. (MAC)

struct Poly2DElement {
  int ix[NNPE_POLY2D]; // Connectivity
  int mat;             // Material Id
  int oid;             // Ordinal Id
  int label;           // User label
  int instance;        // Instance parent
};

//! 2-D polygonal element
class Poly2D : public Element {

  public:

    //! \name Constructor/Destructor
    //@{
             Poly2D(int ecid, int nel);
    virtual ~Poly2D();
    //@}

    //! Setup dual-grid adjacency for ghost
    virtual int addGhostAdjacency(UnsHashMap& hmap, int ecid, int is,
                                  int matid, int proc, int* ghost_conn);

    //! Calculate the B-matrix
    virtual int calcBmatrix(const CVector& coord, Real* V);

    //! Calculate the element centroid
    virtual void calcCentroid(const CVector& coord, CVector& cent);

    //! Calculate the dual-edge data
    virtual void calcDualEdgeData(DualEdge* edge, const CVector& coord);

    //! Calculate the element quality metrics
    virtual void calcMetrics(CVector& /* coord */);

    //! Calculate the element volume
    virtual bool calcVolume(const CVector& coord, Real* v);

    //! Echo the nodal connectivity
    virtual void echoConnectivity(ostream& ofs);

    //! Return the number of edges per element
    virtual int getEdgesPerElement() {return NUM_POLY2D_EDGES;}

    //! Get the edge connectivity, i.e., 2-nodes
    virtual void getEdgeConnectivity(int lid, int eid, int& n1, int& n2);

    //! Get the face connectivity
    virtual void getFaceConnectivity(int lid, int face, int* conn, int* lconn);

    //! Return the number of nodes per edge given an edgeId
    virtual int getNodesPerEdge(int /*edgeId*/) const {
      return NNPE_POLY2D_EDGES;
    }

    //! Return the number of nodes per face (edge in 2D) given a face Id
    virtual int getNodesPerFace(int /*faceID*/) const {
      return NNPE_POLY2D_EDGES;
    }

    //! Quadrature-set is initialized and ready to use?
    virtual bool hasQuadSet() {return false;}

    //! Inside element test
    virtual bool inElement(int lid, const Vector pt, CVector& coord);

    //! Register data for this class, e.g. materials, global ids, connectivities
    virtual void registerData();

    //! Register the B-matrix
    virtual void registerBmatrix();

    //! Set the dual edge information in a given element
    virtual void registerDualEdgeIds();

    //! Set the dual edge information in a given element
    virtual void setDualEdgeId(int lid, int side, int eid);

    //! Get the global dual-edge ID for a given global element ID & side
    virtual int getDualEdgeId(int gid, int side);

    //! Setup dual-grid adjacency
    virtual int setupAdjacency(UnsHashMap& hmap, int ecid);

    //! Setup edge-edge list for ghosts
    virtual void setupGhostEdgeList(UnsHashMap& hmap, int proc, int* el_conn,
                                    int& curlen, int* local_list);

    //! Set the size of the Poly2D element for communications
    virtual int sizeofElement() {return sizeof(Poly2DElement);}

    //! Calculate surface integral and assemble to global array
    virtual void surfaceIntegral(int gid,
                                 int side,
                                 Real wt,
                                 CVector& coord,
                                 Real* rhs);

    //! Return the number of nodes per element for a given element
    virtual int getNnpe(int lid) const {return m_connect[lid].ix.size();}

    //!The function will return integer connectivity and the number of nodes
    //! for the given lid
    virtual int* getNodeConnectivity(const int lid, int& nnpe);

    //**************************************************************************
    //! \name Element specific return types
    //{@

    //! Add a polygonal element to the class
    void addElement() {m_connect.push_back(faceConnect());}

    //! Return the "face" connectivity for an element in the class
    faceConnect& getFaceConnectivity(int lid) {return m_connect[lid];}

    //}@
    /**************************************************************************/

  private:

    //! Don't permit copy or assignment operators
    //@{
    Poly2D(const Poly2D&);
    Poly2D& operator=(const Poly2D&);
    //@}

    //! The variable "m_dualEdgeIds" is the analog of the "int* m_dualEdgeIDs"
    //! defined in the Element class (in Element.h)
    vector<dualEdgeId>  m_dualEdgeIds;  //<! Dual edge Id's for the class
    vector<faceConnect> m_connect;      //<! Element connectivity

};

}

#endif // Poly2D_h
