//******************************************************************************
//! \file    src/DataMesh/Tri3.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   3-node linear element
//******************************************************************************
#ifndef Tri3_h
#define Tri3_h

#include <Element.h>

namespace Hydra {

//! 3-Node tri element
#define NDIM_TRI3        2  //!< Spatial dimensions
#define NNPE_TRI3        3  //!< No. of nodes per element
#define NUM_TRI3_EDGES   3  //!< No. of edges per element
#define NNPE_TRI3_EDGES  2  //!< No. of nodes per tri3 edge
#define NUM_TRI3_FACES   1  //!< No. of faces per element
#define NNPE_TRI3_FACES  3  //!< No. of nodes per tri3 face

//! Connectivity
struct Tri3IX {
  int ix[NNPE_TRI3];
};

struct Tri3Bmatrix {
  Real Bx[NDIM_TRI3][NNPE_TRI3];
  Real By[NDIM_TRI3][NNPE_TRI3];
};

struct Tri3Element {
  int ix[NNPE_TRI3];   //!< Connectivity
  int mat;             //!< Material Id
  int oid;             //!< Ordinal Id
  int label;           //!< User label
};

struct Tri3DualEdgeIds {
  int eid[NUM_TRI3_EDGES];  //!< Dual-mesh edge Id's -- one per face
};

//! 3-node triangular element
class Tri3 : public Element {

  public:

    //! \name Constructor/Destructor
    //@{
             Tri3(int ecid, int nel);
    virtual ~Tri3();
    //@}

    //! Setup dual-grid adjacency for ghost
    virtual int addGhostAdjacency(UnsHashMap& hmap, int ecid, int is,
                                  int matid, int proc, int* ghost_conn);

    //! Calculate the B-matrix
    virtual int calcBmatrix(const CVector& coord, Real* V);

    //! Calculate the element centroid
    virtual void calcCentroid(const CVector& coord, CVector& cent);

    //! Calculate the dual-edge data
    virtual void calcDualEdgeData(DualEdge* edge, const CVector& coord);

    //! Calculate the element quality metrics
    virtual void calcMetrics(CVector& /* coord */);

    //! Calculate surface integral and assemble to global array
    virtual void surfaceIntegral(int gid, int side, Real wt, CVector& coord,
                                 Real* rhs);

    //! Calculate the element volume
    virtual bool calcVolume(const CVector& coord, Real* v);

    //! Echo the nodal connectivity
    virtual void echoConnectivity(ostream& ofs);

    //! Return the number of edges per element
    virtual int getEdgesPerElement() {return NUM_TRI3_EDGES;}

    //! Get the edge connectivity, i.e., 2-nodes
    virtual void getEdgeConnectivity(int lid, int eid, int& n1, int& n2);

    //! Get the face connectivity
    virtual void getFaceConnectivity(int lid, int face, int* conn, int* lconn);

    //! Return the number of nodes per edge given an edgeId
    virtual int getNodesPerEdge(int /*edgeId*/) const {
      return NNPE_TRI3_EDGES;
    }

    //! Return the number of nodes per face given a face Id
    virtual int getNodesPerFace(int /*faceId*/) const {
      return NNPE_TRI3_FACES;
    }

    //! Quadrature-set is initialized and ready to use?
    virtual bool hasQuadSet() {return false;}

    //! Inside element test
    virtual bool inElement(int lid, const Vector pt, CVector& coord);

    //! Register the B-matrix
    virtual void registerBmatrix();

    //! Register dual edge Id's
    virtual void registerDualEdgeIds();

    //! Set the dual edge information in a given element
    virtual void setDualEdgeId(int lid, int side, int eid);

    //! Get the global dual-edge ID for a given global element ID & side
    virtual int getDualEdgeId(int gid, int side);

    //! Setup dual-grid adjacency
    virtual int setupAdjacency(UnsHashMap& hmap, int ecid);

    //! Setup edge-edge list for ghosts
    virtual void setupGhostEdgeList(UnsHashMap& hmap, int proc, int* el_conn,
                                    int& curlen, int* local_list);

    //! Set the size of the Tri3 element for communications
    virtual int sizeofElement() {return sizeof(Tri3Element);}

    //**************************************************************************
    //! \name Element specific return types
    //{@

    //! Shape the B-matrix for direct calculation
    Tri3Bmatrix* getTri3Bmatrix();

    //! Return dual-edge Id's for element class
    Tri3DualEdgeIds* getTri3DualEdgeIds() {
      return reinterpret_cast<Tri3DualEdgeIds*>(getDualEdgeIds());
    }

    //! Return element connectivity shaped for Tri3
    Tri3IX* getTri3Connectivity() {
      return reinterpret_cast<Tri3IX*>(getConnectivity());
    };

    //}@
    /**************************************************************************/

  private:

    //! Don't permit copy or assignment operators
    //@{
    Tri3(const Tri3&);
    Tri3& operator=(const Tri3&);
    //@}

    static int m_edges[NUM_TRI3_EDGES][NNPE_TRI3_EDGES]; //<! Local edges
};

}

#endif // Tri3_h
