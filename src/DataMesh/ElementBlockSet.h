//******************************************************************************
//! \file    src/DataMesh/ElementBlockSet.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   Element set - a collection of element classes (aka blocks)
//******************************************************************************
#ifndef ElementBlockSet_h
#define ElementBlockSet_h

#include <vector>

namespace Hydra {

//! Element Block or Set
class ElementBlockSet {

  public:

    //! \name Constructor/Destructor
    //@{
             ElementBlockSet(int id, const vector<int>& classIds) :
               m_Id(id), m_classIds(classIds) {}
    virtual ~ElementBlockSet() {}
    //@}

    int numElementClass() {return m_classIds.size();}

    int getClassId(int i) {return m_classIds[i];}


  protected:

    int         m_Id;       //<! Id for set
    vector<int> m_classIds; //<! List of class Id's in set


  private:

    //! Don't permit copy or assignment operators
    //@{
    ElementBlockSet(const ElementBlockSet&);
    ElementBlockSet& operator=(const ElementBlockSet&);
    //@}

};

}

#endif // ElementBlockSet_h
