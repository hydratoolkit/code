//******************************************************************************
//! \file    src/DataMesh/Quad4.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief  4-node Quadrilateral element
//******************************************************************************
#ifndef Quad4_h
#define Quad4_h

#include <Element.h>
#include <Quad4QuadSet.h>

namespace Hydra {

// 4-Node quad element
#define NDIM_QUAD4       2  //!< Spatial dimensions
#define NNPE_QUAD4       4  //!< No. of nodes per element
#define NUM_QUAD4_EDGES  4  //!< No. of edges per element
#define NNPE_QUAD4_EDGES 2  //!< No. of nodes per quad4 edge
#define NUM_QUAD4_FACES  1  //!< No. of faces per element
#define NNPE_QUAD4_FACES 4  //!< No. of nodes per quad4 face

struct Quad4IX {
  int ix[NNPE_QUAD4];   //!< Connectivity
};

struct Quad4Element {
  int ix[NNPE_QUAD4]; //!< Connectivity
  int mat;            //!< Material Id
  int oid;            //!< Ordinal Id
  int label;          //!< User label
  int instance;       //!< Instance parent
};

struct Quad4Bmatrix {
  Real Bx[NDIM_QUAD4][NNPE_QUAD4];
  Real By[NDIM_QUAD4][NNPE_QUAD4];
};

struct Quad4DualEdgeIds {
  int eid[NUM_QUAD4_EDGES];  //!< Dual-mesh edge Id's -- one per edge
};

//! 4-node quadrilateral element
class Quad4 : public Element {


  public:

    //! \name Constructor/Destructor
    //@{
             Quad4(int ecid, int nel);
    virtual ~Quad4();
    //@}

    //! Setup dual-grid adjacency for ghost
    virtual int addGhostAdjacency(UnsHashMap& hmap, int ecid, int is,
                                  int matid, int proc, int* ghost_conn);

    //! Calculate the B-matrix
    virtual int calcBmatrix(const CVector& coord, Real* V);

    //! Calculate the element centroid
    virtual void calcCentroid(const CVector& coord, CVector& cent);

    //! Calculate the dual-edge data
    virtual void calcDualEdgeData(DualEdge* edge, const CVector& coord);

    //! Calculate the element quality metrics
    virtual void calcMetrics(CVector& /* coord */);

    //! Calculate the element volume
    virtual bool calcVolume(const CVector& coord, Real* v);

    //! Echo the nodal connectivity
    virtual void echoConnectivity(ostream& ofs);

    //! Return the number of edges per element
    virtual int getEdgesPerElement() {return NUM_QUAD4_EDGES;}

    //! Get the edge connectivity, i.e., 2-nodes
    virtual void getEdgeConnectivity(int lid, int eid, int& n1, int& n2);

    //! Get the face connectivity
    virtual void getFaceConnectivity(int lid, int face, int* conn, int* lconn);

    //! Return the number of nodes per edge given an edgeId
    virtual int getNodesPerEdge(int /*edgeId*/) const {
      return NNPE_QUAD4_EDGES;
    }

    //! Return the number of nodes per face (edge in 2D) given a face Id
    virtual int getNodesPerFace(int /*faceID*/) const {
      return NNPE_QUAD4_EDGES;
    }

    //! Quadrature-set is initialized and ready to use?
    virtual bool hasQuadSet() {return false;}

    //! Inside element test
    virtual bool inElement(int lid, const Vector pt, CVector& coord);

    //! Register the B-matrix
    virtual void registerBmatrix();

    //! Set the dual edge information in a given element
    virtual void registerDualEdgeIds();

    //! Set the dual edge information in a given element
    virtual void setDualEdgeId(int lid, int side, int eid);

    //! Get the global dual-edge ID for a given global element ID & side
    virtual int getDualEdgeId(int gid, int side);

    //! Setup dual-grid adjacency
    virtual int setupAdjacency(UnsHashMap& hmap, int ecid);

    //! Setup edge-edge list for ghosts
    virtual void setupGhostEdgeList(UnsHashMap& hmap, int proc, int* el_conn,
                                    int& curlen, int* local_list);

    //! Set the size of the Hex8 element for communications
    virtual int sizeofElement() {return sizeof(Quad4Element);}

    //! Calculate surface integral and assemble to global array
    virtual void surfaceIntegral(int gid, int side, Real wt, CVector& coord,
                                 Real* rhs);

    //**************************************************************************
    //! \name Element specific return types
    //{@

    //! Shape the B-matrix for direct calculation
    Quad4Bmatrix* getQuad4Bmatrix();

    //! Return dual-edge Id's for element class
    Quad4DualEdgeIds* getQuad4DualEdgeIds() {
      return reinterpret_cast<Quad4DualEdgeIds*>(getDualEdgeIds());
    }

    //! Return element connectivity shaped for Tri3
    Quad4IX* getQuad4Connectivity() {
      return reinterpret_cast<Quad4IX*>(getConnectivity());
    };

    //! Return quadrature set -- may not be initialized
    Quad4QuadSet& getQuad4QuadSet() {return m_q4qs;}

    //}@
    /**************************************************************************/

  private:

    //! Don't permit copy or assignment operators
    //@{
    Quad4(const Quad4&);
    Quad4& operator=(const Quad4&);
    //@}

    //! Typically initialized with registration of the B-matrix
    Quad4QuadSet m_q4qs;

    static int m_edges[NUM_QUAD4_EDGES][NNPE_QUAD4_EDGES]; //!< Local edge array
    static int m_faces[NUM_QUAD4_FACES][NNPE_QUAD4_FACES]; //!< Local face array
};

}

#endif // Quad4_h
