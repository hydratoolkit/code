//******************************************************************************
//! \file    src/DataMesh/UnsMesh.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   Unstructured DataMesh
//******************************************************************************
#ifndef UnsMesh_h
#define UnsMesh_h

#include <fstream>
#include <map>
#include <set>
#include <string>
#include <vector>

#include <Control.h>
#include <ElemCommunicator.h>
#include <NodeCommunicator.h>
#include <DualEdgeCommunicator.h>
#include <DataContainer.h>
#include <CoordinateTypes.h>
#include <Element.h>
#include <Beam2.h>
#include <Poly2D.h>
#include <Quad4.h>
#include <Tri3.h>
#include <Hex8.h>
#include <Tet4.h>
#include <Poly3D.h>
#include <Pyr5.h>
#include <Wedge6.h>
#include <ElementBlockSet.h>
#include <MaterialSet.h>
#include <UnsMeshParm.h>
#include <Material.h>
#include <SetTypes.h>
#include <Table.h>

namespace Hydra {

struct ElementSet {
  //! \name  ElementSet data structure
  //! The ElementSet data structure holds element sets for the mesh.
  //@{
  int  id;             //!< Set Id
  int  Nel;            //!< Number of elements in set
  DataIndex ELEM_LIST; //!< list for the set
  string* name;        //!< Name for the element set
  //@}
};

struct Nodeset {
  //! \name  Nodeset data structure
  //! The Nodeset data structure is used to hold input nodeset data for the mesh.
  //@{
  int  id;             //!< Nodeset Id
  int  Nnp;            //!< Number of nodes in nodeset
  DataIndex NODE_LIST; //!< Node list for nodeset
  string* name;        //!< Name for the nodeset
  //@}
};

struct Sideset {
  //! \name  Sideset data structure
  //! The Sideset data structure is used to hold input sideset data for the mesh.
  //@{
  int  id;                 //!< Sideset Id
  int  Nel;                //!< Number of elements in sideset
  int  Nnp;                //!< Number of nodes in sideset
  int  Ndf;                //!< Number of distribution factors
  DataIndex EL_LIST;       //!< List of elements in the sideset
  DataIndex SIDE_LIST;     //!< List of sides for each element in the sideset
  DataIndex DUALEDGE_LIST; //!< List of dual edges (global indices) in sideset
  string *name;            //!< Name for the sideset
  //@}
};

struct Field {
  //! \name Field holds spatially varying information of various types
  //@{
  int setId;                   //!< Set or surface Id
  VariableCentering centering; //!< EDGE_CTR, FACE_CTR, etc.
  VariableType fieldType;      //!< SCALAR_VAR, VECTOR_VAR, etc.
  string* fieldName;           //!< Field name
  DataIndex FIELD;             //!< Field data
  //@}
};

struct MergeSet {
  //! \name Merge set data structure
  int userId;         //!< User Id for the merged set
  string name;        //!< Name for the merged set
  vector<int> setIds; //!< User Id's of the sets to be merged
};

//! \brief Extra data sent with ghost connectivity
//! The order in the send buffer * is as follows:
//!   connectivity offset (point to connectivity),
//!   element class number, material id and connectivity.
//!   the length for offset, class number and material id is the
//!   number of sending
enum SendGhostDim{OFFSET = 0,
                  ELEMENTCLASS = 1,
                  MATERIALID = 2,
                  EXTRASENDING = 3};

//! \brief UnsMesh holds the basic mesh data, sidesets, nodesets, etc.
//! The mesh object holds basic mesh information, sidesets, nodesets, and data
//! This is the so-called "data-mesh" model where the mesh object basically
//! serves data to the physics object.
class UnsMesh : public DataContainer {

  public:

    //! \name Constructor/Destructor
    //@{
             UnsMesh();
    virtual ~UnsMesh();
    //@}

    //**************************************************************************
    //! \name Data registration for primary mesh entities
    //@{
    //! Register centroid coordinates
    void registerCentroidCoordinates();

    //! Register nodal coordinates
    void registerCoordinates(int ndim, int nnp);

    //! Register all of the variables/objects for the mesh
    void registerData();

    //! Register the dual-edges
    void registerDualEdges();

    //! Register nodesets
    void registerSets(int nnd_sets, int nsd_sets);
    //@}

    //**************************************************************************
    //! \name Local-to-global mapping, element class mapping
    //@{
    //! Map the element connectivity for processor-local numbering
    void mapConnectivity();

    //! Generate the element to class map
    void mapElemClasses();
    //@}

    //**************************************************************************
    //! \name Data echo functions
    //@{
    //! Print out the dual-edge summary
    void echoDualEdgeParm(ostream& ofs);

    //! Print out the mesh parameters
    void echoMeshParms(ostream& ofs);

    //! Print out the element connectivity
    void echoConnectivity(ostream& ofs);

    //! Print out the nodal coordinates
    void echoCoord(ostream& ofs);

    //! Print out the nodeset data
    void echoNodesets(ostream& ofs);

    //! Print out the sideset data
    void echoSidesets(ostream& ofs);

    //! Print out the IFC sideset data
    void echoIFCSidesets(ostream& ofs);

    //! Print out the node and element communicators
    void echoCommLists(ostream& ofs);
    //@}

    //**************************************************************************
    //! \name Utility functions
    //@{
    //! Find closest node to a point in an element
    int closestNode(int Id, const Vector& pt);

    //! Find Element given a point coordinate
    int findElement(const Vector& pt);

    //! Find closest node to a given point coordinate
    void findNode(const Vector& pt, int& nodeId, int& userId);

    //! Is a point in the element
    bool inElement(int Id, const Vector& pt);

    //! Return the status of the local-numbering for parallel
    // false: in ordinal numbering space (zero-based)
    // true : in the local numbering space (zero-based)
    bool localStatus() {return isLocal;}
    //@}

    //! Store the mesh title
    void setTitle(char* cbuf) {title = cbuf;}

    //! Retrieve a character string for the mesh title
    const char* getTitle()   {return title.c_str();}

    //! Retrieve the C++ string for that holds the mesh title
    string Title()  {return title;}

    //! Store the mesh coord names
    void setXcoordName(char* cbuf) {if (strlen(cbuf) > 0) xcoord_name = cbuf;}
    void setYcoordName(char* cbuf) {if (strlen(cbuf) > 0) ycoord_name = cbuf;}
    void setZcoordName(char* cbuf) {if (strlen(cbuf) > 0) zcoord_name = cbuf;}

    //! Retrieve a character string for the mesh coord names
    const char* getXcoordName()   {return xcoord_name.c_str();}
    const char* getYcoordName()   {return ycoord_name.c_str();}
    const char* getZcoordName()   {return zcoord_name.c_str();}

    //! Retrieve the C++ string for that holds coord names
    string XcoordName()  {return xcoord_name;}
    string YcoordName()  {return ycoord_name;}
    string ZcoordName()  {return zcoord_name;}

    //! Set all the relevant mesh parameters
    void setMeshParm(int Nnp, int Nel);

    void setMeshParm(UnsMeshParm meshparm);

    void setGlobalMeshParm(UnsMeshParm meshparm);

    void setLocalMeshParm(UnsMeshParm meshparm);

    void setEdgeParm(int nedge, int nedge_int, int nedge_ext,
                     int nedge_ext_ifc, int nedge_ghost, int nedge_ghost_ifc);

    //! \name Mesh Parameter Functions
    //! These functions are used to access the basic parameters that
    //! describe the mesh, e.g., the number of nodes, number of elements, etc.
    //! Note: These functions return the processor-local values of the
    //! number of nodes, elements, etc.
    //@{
    int   getNdim()             {return Ndim;            }
    int   getNnp()              {return Nnp;             }
    int   getGlobalNnp()        {return Nnp_g;           }
    int   getNel()              {return Nel;             }
    int   getGlobalNel()        {return Nel_g;           }
    int   getNel_int()          {return Nel_int;         }
    int   getNel_brdr()         {return Nel_brdr;        }
    int   getNel_poly2d()       {return Nel_poly2d;      }
    int   getNel_quad4()        {return Nel_quad4;       }
    int   getNel_tri3()         {return Nel_tri3;        }
    int   getNel_hex8()         {return Nel_hex8;        }
    int   getNel_poly3d()       {return Nel_poly3d;      }
    int   getNel_pyr5()         {return Nel_pyr5;        }
    int   getNel_tet4()         {return Nel_tet4;        }
    int   getNel_wedge6()       {return Nel_wedge6;      }
    int   getNeblock()          {return Neblock;         }
    int   getNedge()            {return Nedge;           }
    int   getNedge_int()        {return Nedge_int;       }
    int   getNedge_ext()        {return Nedge_ext;       }
    int   getNedge_ifc()        {return Nedge_ifc;}
    int   getNedge_ghost()      {return Nedge_ghost;     }
    int   getNedge_ghost_ifc()  {return Nedge_ghost_ifc; }
    int   getNelsets()          {return Nel_sets;        }
    int   getNndsets()          {return Nnd_sets;        }
    int   getNsdsets()          {return Nsd_sets;        }
    int   getNelOffset()        {return NelOffset;       }

    //! Get the size of the object in bytes
    int getSizeOf(DataIndex vid);

    //**************************************************************************
    //! \name Mesh Transformation Functions
    //! These functions are used to change the dimension and
    //! the arrangement of mesh
    //@{
    //! Calculate the mesh bounding box
    void calcBbox();

    //! Get the mesh bounding box (for utilities)
    void getBbox(Vector& xmin, Vector& xmax);

    //! Set the mesh bounding box (for utilities)
    void setBbox(Vector& xmin, Vector& xmax);

    //! Calculate the mesh quality metrics
    void calcMetrics();

    //! Mesh transformation
    void transform(Control* control);

    //! Mesh Scaling
    void scale(Category* cat);

    //! Mesh Translation
    void translate(Category* cat);
    //@}

    //**************************************************************************
    //! \name Field Functions
    //! Handle fields for spatially-varying BC's, source terms, etc.
    //@{
    Field& getField(int id) {return m_fields[id];}

    int getNfield() {return m_fields.size();}
    //@}

    //**************************************************************************
    //! \name Nodeset and Sideset Functions
    //! These functions are used to access the nodeset and sideset
    //! data for the application of BC's.
    //@{
    Nodeset* getNodesets() {return getVariable<Nodeset>(NODESETS);}

    Sideset* getSidesets() {return getVariable<Sideset>(SIDESETS);}

    //! Check a user Nodeset Id
    bool checkUserNodesetId(int userId, int& locId);

    //! Check a user Sideset Id
    bool checkUserSidesetId(int userId, int& locId);

    //! Create a number of new side sets
    void createNewSidesets(int n);

    //! Add a merged side set to the mesh
    void addMergedSideset(int userId, string& name, const vector<int>& setIds);

    //! Add new side set to mesh
    void addSideset(int userId, string& name,
                    const vector<int>& new_el_list,
                    const vector<int>& new_sd_list);

    //! Add new side set to mesh
    void addSideset(const vector<int>& new_el_list,
                    const vector<int>& new_sd_list,
                    int& locId, int& userId);

    //! Get a new unique side set id
    int getNewSidesetId();

    //! Setup the merged side sets in the mesh
    void setupMergedSidesets();

    //! Setup all the internal set Id's for output plot requests
    void setupOutputSetIds(Control* control);
    //@}

    //**************************************************************************
    //! \name IFC functions
    //! The functions used to access the IFC sideset lists.
    //@{
    int numIFCs() {return m_ifcSets.size();}

    int numIFCBCs() {return m_ifcbcSets.size();}

    vector<int>& getIFCSets() {return m_ifcSets;}

    vector<int>& getIFCBCSets() {return m_ifcbcSets;}

    int* getInactiveIFCNodes() {return getVariable<int>(INACTIVE_IFC_NODES);}
    //@}

    //**************************************************************************
    //! \name Element and Material sets
    //! The functions used to access element, material, multimaterial sets
    //! during parsing, setup, remesh/remap operations.
    //@{
    ElementSet* getElementsets() { return getVariable<ElementSet>(ELEMSETS); }

    int numElementBlockSets() {return m_elembsets.size();}

    ElementBlockSet* getElementBlockSet(int i) {return m_elembsets[i];}

    int numMaterialSets() {return m_matsets.size();}

    bool addMaterialSet(int Id, int matId, int fieldId, CoordinateType type,
                        vector<int>& classIds);

    //! Get a material set by Id
    MaterialSet* getMaterialSet(int id, int fieldid=0) {
      return m_matsets[make_pair(id,fieldid)];
    }

    void echoMaterialSets(ostream& ofs);
    //@}

    //! \name Basic variables registered by the datamesh
    //! These functions provide access to the nodal coordinates
    //@{
    Real* getX()  {return getCVector(NODE_COORD).X; }
    Real* getY()  {return getCVector(NODE_COORD).Y; }
    Real* getZ()  {return getCVector(NODE_COORD).Z; }
    Real* getXC() {return getCVector(ELEM_COORD).X; }
    Real* getYC() {return getCVector(ELEM_COORD).Y; }
    Real* getZC() {return getCVector(ELEM_COORD).Z; }

    CVector getNodeCoord() {return getCVector(NODE_COORD); }

    CVector getCentCoord() {return getCVector(ELEM_COORD); }
    //@}

    //! \name Manage element classes
    //! These functions are used to manage element classes
    //@{
    void addElementClass(Eltype etype, int ecid, int nel);

    //! Check element class Id's for a valid class/block Id
    //!   \param[in]  Input element block Id (set Id)
    bool checkClassId(const int id);

    //! Get the internal class Id given a user Id
    //!  \param[in]  Input elment block Id in user space (1-based)
    //!  \return internal class Id
    int getClassId(const int userId);

    Element* getElementClass(int i) {return elclass[i];}

    set<int> getElementClassIds() {return elclassIds;}

    int* getElementClassMap() {
      return getVariable<DataIndex>(CLASS_MAP);
    }

    //! Query UnsMesh total memory usage in bytes
    Real getMemory();

    int  numElementClass() {return elclass.size();}

    void setupElementClassIds();
    //@}

    //**************************************************************************
    //! \name Dual-Edge interface
    //! Accessors for the dual-edge data structures
    //@{
    DualEdge* getDualEdges() {
      return getVariable<DualEdge>(DUAL_EDGES);
    }
    //@}

    //**************************************************************************
    //! \name Primal edge interface
    //! Accessors for the primal mesh edge data
    //@{
    //! Setup the unique primal edge list
    void setupUniquePrimalEdges();

    // Get the number of unique primal edges
    int numUniquePrimalEdges() {return m_uniquePrimalEdges.size();}

    // Get the list of unique primal edges
    vector< pair<int,int> >& getUniquePrimalEdges() {
      return m_uniquePrimalEdges;
    }
    //@}

    //! \name Material model interface
    //! These functions provide access to the material parameters for
    //! fluid/thermal problems.  More sophisticated functions may
    //! be implemented, e.g., for temperature-dependent properties.
    //@{
    void addMaterial(int matid);
    void addMaterial(int matid, Material* mat);

    void addJCMaterial(int matid, Material* mat);
    void addJCMaterial(int matid, Real* JCdata, Real* MGdata);

    void addPRMaterial(int matid, Material* mat);
    void addPRMaterial(int matid, Real* PRdata, Real* MGdata);

    int  numMaterials()                 {return materials.size();}
    Material* getMaterial(int matid)    {return materials[matid];}
    vector<int> getMatids()             {return matids;          }
    map<int, Material*>& getMaterials() {return materials;       }

    void assignMaterials();
    void echoMaterials(ostream& ofs);
    //@}

    //**************************************************************************
    //! \name table interface
    //@{
    void addTable(int id, vector<Real> lct, vector<Real> lcv);
    int  numTables()           {return tables.size();}
    Table* getTable(int tblid)  {return tables[tblid]; }
    vector<int> getTableIds()  {return tableids;     }
    void echoTables(ostream& ofs);
    //@}

    //**************************************************************************
    //! \name Message-passing related functions
    //@{
    void registerGlobalMaps();
    void registerGlobalMaps(int nel, int nnp);

    void setProcId();

    //! \name Local-to-global maps
    int* getElemMap()  {return getVariable<int>(ELEM_MAP);}
    int* getNodeMap()  {return getVariable<int>(NODE_MAP);}

    DataIndex getElemMapId() const {return ELEM_MAP;}

    DataIndex getNodeMapId() const {return NODE_MAP;}

    //! \name Communicators
    NodeCommunicator&     getNodeComm()     {return m_nodecomm;}

    ElemCommunicator&     getElemComm()     {return m_elemcomm;}

    DualEdgeCommunicator& getDualEdgeComm() {return m_dualedgecomm;}

    void markGhostElemConn(int* comm_len, int* comm_buf);

    void markRigidNodes();

    void packGhostElemConn(int* ecmap, int* ecidmap, int* send_offset);

    void renumberProcessorLocal();

    void renumberConnectivityLocal(map<int,int>& nd_global2local);

    void renumberElemSetsLocal(map<int,int>& el_global2local);

    void renumberNodeSetsLocal(map<int,int>& nd_global2local);

    void renumberSideSetsLocal(map<int,int>& el_global2local);

    void sendGhostElemConn(int* send_offset);

    void setupCommunicators();

    void setupDualEdgeCommunicator(UnsHashMap& hmap);

    void setupElementCommunicator(ProcMap* proc_map, int* xadj, int* adjncy);

    void setupGhostElemAdjacency(UnsHashMap& hmap);

    void setupGhostElemConn(int* ecmap, int* ecidmap);

    //! Setup the dual-edge data for IFC's
    void setupIFCEdges();

    //! Setup the side-sets for IFC's
    void setupIFCSideSets();

    void setupNodeCommunicator(int* eptr, int* eind);

    void setupProcMap(int* elem_dist, ProcMap* proc_map);

    //! Sets up the dual edge list in the side set data structure
    void setupSidesetDualEdgeLists();

    //! Setup variables registered by the mesh to be migrated
    void setupMigrationVars(vector<DataIndex>& elem_var,
                            vector<DataIndex>& node_var);

    //! Set the offset of global element number in the class
    void setGlobalElementIds();

    //! Setup the canonical element distribution for ParMETIS
    void setupCanonicalDist(int* elem_dist);

    //! Compute the distributed graph used to setup communicators
    void calcDistributedGraph(int* elem_dist,
                              const vector<int> &eptr,
                              const vector<int> &eind,
                              vector<int> &xadj,
                              vector<int> &adjncy);

    //! Check remotepid's partition for connectivity
    bool doPartitionsConnect(const int remotepid,
                             const vector<int>& noderanges,
                             const vector<Real>& boundingbox);

    //! Setup graph connectivity used for load balancing/communicators
    void setupGraphConnectivity(int* eind, int* eptr);


    //! Swap ghost elements to setup all required mapping
    void swapGhostElemConn();

    //! Query memory used by element classes in bytes
    Real getElementClassMemory();

  private:

    //! Don't permit copy or assignment operators
    //@{
    UnsMesh(const UnsMesh&);
    UnsMesh& operator=(const UnsMesh&);
    //@}

    //**************************************************************************
    //! \name Mesh parameters
    //@{
    string title;         //!< Mesh title
    int meshtype;         //!< Mesh type
    int Ndim;             //!< Dimension of mesh
    // --- MPC specific ---
    int Nnp_g;            //!< Number of nodes                          (global)
    int Nel_g;            //!< Total Number of elements                 (global)
    int Nel_poly2d_g;     //!< Number of POLY2D elements                (global)
    int Nel_quad4_g;      //!< Number of QUAD4  elements                (global)
    int Nel_tri3_g;       //!< Number of TRI3   elements                (global)
    int Nel_hex8_g;       //!< Number of HEX8   elements                (global)
    int Nel_poly3d_g;     //!< Number of POLY3D elements                (global)
    int Nel_pyr5_g;       //!< Number of PYR5   elements                (global)
    int Nel_tet4_g;       //!< Number of TET4   elements                (global)
    int Nel_wedge6_g;     //!< Number of WEDGE6 elements                (global)
    int Nnp_int;          //!< Number of internal nodes                 (local )
    int Nnp_brdr;         //!< Number of border nodes                   (local )
    int Nnp_ext;          //!< Off-processor ghost nodes                (local )
    int Nel_int;          //!< Number of internal elements              (local )
    int Nel_brdr;         //!< Number of border elements                (local )
    int Nel_ext;          //!< Off-processor ghost elements             (local )
    // --- MPC specific ---
    int Nnp;              //!< Number of nodes                          (local )
    int Nel;              //!< Total Number of elements                 (local )
    int Nel_poly2d;       //!< Number of POLY2D elements                (local )
    int Nel_quad4;        //!< Number of QUAD4  elements                (local )
    int Nel_tri3;         //!< Number of TRI3   elements                (local )
    int Nel_hex8;         //!< Number of HEX8   elements                (local )
    int Nel_poly3d;       //!< Number of POLY3D elements                (local )
    int Nel_pyr5;         //!< Number of PYR5   elements                (local )
    int Nel_tet4;         //!< Number of TET4   elements                (local )
    int Nel_wedge6;       //!< Number of WEDGE6 elements                (local )
    int Neblock;          //!< Number of mesh blocks                    (global)
    int Nedge;            //!< Total number of dual edges               (local )
    int Nedge_int;        //!< Number of internal dual edges            (local )
    int Nedge_ext;        //!< Number of external dual edges            (local )
    int Nedge_ifc;        //!< Number of external plus IFC dual edges   (local )
    int Nedge_ghost;      //!< Number of true ghost dual edges          (local )
    int Nedge_ghost_ifc;  //!< Number of ghost dual edges that are IFCs (local )
    int Nel_sets;         //!< No. of element sets                      (global)
    int Nnd_sets;         //!< Number of nodesets                       (global)
    int Nsd_sets;         //!< Number of sidesets                       (global)

    int NelOffset;        //!< Starting global element number for each processor
    //@}

    //**************************************************************************
    //! \name Coordinates -- dimensions are [Nnp]
    //@{
    DataIndex NODE_COORD;
    DataIndex ELEM_COORD;
    //@}

    //! Map from global element Id to element class
    DataIndex CLASS_MAP;

    //**************************************************************************
    //! \name Message passing data structures
    //@{
    DataIndex ELEM_MAP; //<! Global element number from local element number
    DataIndex NODE_MAP; //<! Global node    number from local node    number
    DataIndex PROC_ID;  //<! Processor/rank ID

    NodeCommunicator     m_nodecomm;
    ElemCommunicator     m_elemcomm;
    DualEdgeCommunicator m_dualedgecomm;
    //@}

    //**************************************************************************
    //! \name Node-sets, side-sets, element sets, etc.
    //@{
    DataIndex NODESETS;
    DataIndex NODEBCS;

    //! Sidesets for bc's
    DataIndex SIDESETS;

    //! Element sets -- may not be used in the future
    DataIndex ELEMSETS;

    //! List of IFC and IFC BC side-set Id's (internal Id's)
    vector<int> m_ifcSets;
    vector<int> m_ifcbcSets;

    //! Inactive interior nodes for IFC's
    DataIndex INACTIVE_IFC_NODES;

    typedef map<int, ElementBlockSet*> ElemBsetMap;
    ElemBsetMap m_elembsets; //<! Element block sets

    typedef map<pair<int,int>, MaterialSet*> MaterialSetMap;
    MaterialSetMap m_matsets; //<! Material sets

    vector<MergeSet*> mergedSidesets;  //!< vector of merged sidesets
    //@}

    //**************************************************************************
    //! \name Dual-edge structures
    //@{
    DataIndex DUAL_EDGES;
    DataIndex DUAL_CUT_EDGES;
    //@}

    //**************************************************************************
    //! \name Primal edge structures
    //@{
    vector< pair<int,int> > m_uniquePrimalEdges;
    //@}

    //! Element classes -- stored generically
    vector<Element*> elclass;

    //! Set of element class Id's
    set<int> elclassIds;

    //! Use a typedef map for fast index to materials
    //! matmap is the real map used to lookup materials by id
    typedef map<int, Material*> matmap;
    matmap materials;

    //! Vector of material Id's, aka, block Id's in Exodus-II
    vector<int> matids;

    //! Use a typedef map for fast index to tables
    //! lcmap is the real map used to lookup tables by Id
    typedef map<int, Table*> lcmap;
    lcmap tables;

    //! Vector of table id's
    vector<int> tableids;

    //**************************************************************************
    //! \name Fields
    //@{
    vector<Field> m_fields;
    //@}

    //! Indicate node/element numbers are processor-local or ordinal
    bool isLocal;

    //! C++ strings that holds the mesh coordinate names
    string xcoord_name;
    string ycoord_name;
    string zcoord_name;

    //! Global mesh bounding box
    Vector m_xmin_g;
    Vector m_xmax_g;

    //! Local mesh bounding box
    Vector m_xmin;
    Vector m_xmax;

  };

}

#endif // UnsMesh_h
