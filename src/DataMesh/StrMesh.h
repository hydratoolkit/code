//******************************************************************************
//! \file    src/DataMesh/StrMesh.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   Abstract base class for structure-meshes
//******************************************************************************

#ifndef StrMesh_h
#define StrMesh_h

namespace Hydra {

//! Mesh holds basic mesh data, sidesets, nodesets, etc.
//! The mesh object holds basic mesh information, sidesets, nodesets, and data
//! This is the so-called "data-mesh" model where the mesh object basically
//! serves data to the physics object.
class StrMesh : public DataContainer {

  public:

    //! \name Constructor/Destructor
    //@{
             StrMesh();
    virtual ~StrMesh();
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    StrMesh(const StrMesh&);
    StrMesh& operator=(const StrMesh&);
    //@}

};

}

#endif // StrMesh_h
