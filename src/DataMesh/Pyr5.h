//******************************************************************************
//! \file    src/DataMesh/Pyr5.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   5-node Pyramid element
//******************************************************************************
#ifndef Pyr5_h
#define Pyr5_h

#include <Element.h>
#include <Pyr5QuadSet.h>

namespace Hydra {

#define NDIM_PYR5        3   //!< Spatial dimensions
#define NNPE_PYR5        5   //!< No. of nodes per element
#define NUM_PYR5_EDGES   8   //!< No. of edges per element
#define NNPE_PYR5_EDGES  2   //!< No. of nodes per Pyr5 edge
#define NUM_PYR5_FACES   5   //!< No. of faces per element
#define NNPE_PYR5_FACES  4   //!< No. of nodes per Pyr5 face (Maximum)

struct Pyr5IX {
    int ix[NNPE_PYR5];  //!< Connectivity
};

struct Pyr5Element {
    int ix[NNPE_PYR5];  //!< Connectivity
    int mat;            //!< Material Id
    int oid;            //!< Ordinal Id
    int label;          //!<!< User label
};

struct Pyr5Bmatrix {
    Real X[NNPE_PYR5];
    Real Y[NNPE_PYR5];
    Real Z[NNPE_PYR5];
}__attribute__((aligned(16)));

struct Pyr5DualEdgeIds {
    int eid[NUM_PYR5_FACES];  //!< Dual-mesh edge Id's -- one per face
};


//! 5-node pyramid element
class Pyr5: public Element {

  public:

    //! \name Constructor/Destructor
    //@{
             Pyr5(int ecid, int nel);
    virtual ~Pyr5();
    //@}

    //! Setup dual-grid adjacency for ghost
    virtual int addGhostAdjacency(UnsHashMap& hmap, int ecid, int is,
                                  int matid, int proc, int* ghost_conn);

    //! Calculate the B-matrix
    virtual int calcBmatrix(const CVector& coord, Real* V);

    //! Calculate the element centroid
    virtual void calcCentroid(const CVector& coord, CVector& cent);

    //! Calculate the dual-edge data
    virtual void calcDualEdgeData(DualEdge* edge, const CVector& coord);

    //! Calculate the element quality metrics
    virtual void calcMetrics(CVector& /* coord */);

    //! Calculate surface integral and assemble to global array
    virtual void surfaceIntegral(int gid, int side, Real wt, CVector& coord,
                                 Real* rhs);

    //! Calculate the element volume
    virtual bool calcVolume(const CVector& coord, Real* v);

    //! Echo the nodal connectivity
    virtual void echoConnectivity(ostream& ofs);

    //! Return the number of edges per element
    virtual int getEdgesPerElement() {return NUM_PYR5_EDGES;}

    //! Get the edge connectivity, i.e., 2-nodes
    virtual void getEdgeConnectivity(int lid, int eid, int& n1, int& n2);

    //! Get the face connectivity
    virtual void getFaceConnectivity(int lid, int face, int* conn, int* lconn);

    //! Return the number of nodes per edge given an edgeId
    virtual int getNodesPerEdge(int /*edgeId*/) const {
      return NNPE_PYR5_EDGES;
    }

    //! Return the number of nodes per face give a face Id
    virtual int getNodesPerFace(int faceId) const {
      return m_nnpeFace[faceId];
    }

    //! Quadrature-set is initialized and ready to use?
    virtual bool hasQuadSet() {return m_p5qs.isInitialized();}

    //! Inside element test
    virtual bool inElement(int lid, const Vector pt, CVector& coord);

    //! Register the B-matrix
    virtual void registerBmatrix();

    //! Set the dual edge information in a given element
    virtual void registerDualEdgeIds();

    //! Set the dual edge information in a given element
    virtual void setDualEdgeId(int lid, int side, int eid);

    //! Get the global dual-edge ID for a given global element ID & side
    virtual int getDualEdgeId(int gid, int side);

    //! Setup dual-grid adjacency
    virtual int setupAdjacency(UnsHashMap& hmap, int ecid);

    //! Setup edge-edge list for ghosts
    virtual void setupGhostEdgeList(UnsHashMap& hmap, int proc,
                                    int* el_conn, int& curlen, int* local_list);

    //! Set the size of the Hex8 element for communications
    virtual int sizeofElement() {return sizeof(Pyr5Element);}

    //**************************************************************************
    //! \name Element specific return types
    //{@

    //! Shape the B-matrix for direct calculation
    Pyr5Bmatrix* getPyr5Bmatrix();

    //! Return dual-edge Id's for element class
    Pyr5DualEdgeIds* getPyr5DualEdgeIds() {
      return reinterpret_cast<Pyr5DualEdgeIds*>(getDualEdgeIds());
    }

    //! Return quadrature set -- may not be initialized
    Pyr5IX* getPyr5Connectivity() {
      return reinterpret_cast<Pyr5IX*>(getConnectivity());
    };

    //! Return quadrature set -- may not be initialized
    Pyr5QuadSet& getPyr5QuadSet() {return m_p5qs;}

    //}@
    /**************************************************************************/

  private:

    //! Don't permit copy or assignment operators
    //@{
    Pyr5(const Pyr5&);
    Pyr5& operator=(const Pyr5&);
    //@}

    //! Typically initialized with registration of the B-matrix
    Pyr5QuadSet m_p5qs;

    static int m_edges[NUM_PYR5_EDGES][NNPE_PYR5_EDGES]; //<! Local edges
    static int m_faces[NUM_PYR5_FACES][NNPE_PYR5_FACES]; //<! Local face array
    static int m_nnpeFace[NUM_PYR5_FACES];  //<! # of nodes per local face
};

}

#endif
