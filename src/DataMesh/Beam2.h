//******************************************************************************
//! \file    src/DataMesh/Beam2.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   Simple 2-node beam element
//******************************************************************************
#ifndef Beam2_h
#define Beam2_h

#include <Element.h>

namespace Hydra {

#define NNPE_BEAM2       2  //!< No. of nodes per 2-node beam element

// 2-Node element
struct Beam2Element {
  int ix[NNPE_BEAM2];  //!< Node-to-element connectivity
  int mat;             //!< Material Id
  int oid;             //!< User Id
  int lable;           //!< User label
};

//! 2-node 1-D element (for testing purposes)

class Beam2 : public Element {

  public:

    //! \name Constructor/Destructor
    //@{
             Beam2(int ecid, int nel);
    virtual ~Beam2();
    //@}

    //! Calculate the element grid quality metrics
    virtual void calcMetrics(CVector& /* coord */);

  protected:

  private:

    //! Don't permit copy or assignment operators
    //@{
    Beam2(const Beam2&);
    Beam2& operator=(const Beam2&);
    //@}

};

}

#endif // Beam2_h
