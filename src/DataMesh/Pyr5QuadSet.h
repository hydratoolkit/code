//******************************************************************************
//! \file    src/DataMesh/Pyr5QuadSet.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   5-node Pyramid quadrature set
//******************************************************************************
#ifndef Pyr5QuadSet_h
#define Pyr5QuadSet_h

#include <QuadratureSet.h>

namespace Hydra {

//! 5-Node Pyramid Quadrature Set
class Pyr5QuadSet : public QuadratureSet {

  public:

    //! \name Constructor/Destructor
    //@{
             Pyr5QuadSet();
    virtual ~Pyr5QuadSet();
    //@}

    //! Initialize the quadrature set according to the quadrature rule
    virtual void initialize(int Nqpt_vol, int Nqpt_surf);

    //! Finalize phase
    virtual void finalize();

    int getNumVolPts() {return m_Nqpt_vol;}

    int getNumSurfPts() {return m_Nqpt_surf;}

    const Real* getVsfArray(int qdim, int kpt) const {
      return &m_vsf[qdim][kpt][0];
    }

    const Real* getVolWeights() const {return m_vwt;}

  protected:

    int m_Nqpt_vol;  // No. of volume quadrature points
    int m_Nqpt_surf; // No. of volume quadrature points

  private:

    //! Don't permit copy or assignment operators
    //@{
    Pyr5QuadSet(const Pyr5QuadSet&);
    Pyr5QuadSet& operator=(const Pyr5QuadSet&);
    //@}

    Real     sf(Real xi, Real eta, Real zeta, int node);
    Real   sfxi(Real xi, Real eta, Real zeta, int node);
    Real  sfeta(Real xi, Real eta, Real zeta, int node);
    Real sfzeta(Real xi, Real eta, Real zeta, int node);

    //! Shape functions, derivatives and quadrature weights
    //! vsf[Nqdim][Nqpt][Nnpe]
    Real m_vsf[4][5][5];

    //! Natural coordinates
    //! qpt[Nqpt][Ndim]
    Real m_vqpt[5][3];

    //! Quadrature weights
    //! wt[Nqpt]
    Real m_vwt[5];


};

}

#endif
