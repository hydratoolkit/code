//******************************************************************************
//! \file    src/DataMesh/MaterialSet.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   A collection of element classes (aka blocks) w. same mat's
//******************************************************************************
#ifndef MaterialSet_h
#define MaterialSet_h

#include <vector>

#include <ElementBlockSet.h>
#include <CoordinateTypes.h>

namespace Hydra {

//! Material Set
class MaterialSet : public ElementBlockSet {

  public:

    //! \name Constructor/Destructor
    //@{
             MaterialSet(int Id, int matId, int fieldId,
                         const vector<int>& classIds, CoordinateType type);
    virtual ~MaterialSet() {}
    //@}

    int getMatId() {return m_matId;}

    int getFieldId() {return m_fieldId;}

    CoordinateType getCoordType() {return m_type;}


  protected:

    CoordinateType m_type;

  private:

    //! Don't permit copy or assignment operators
    //@{
    MaterialSet(const MaterialSet&);
    MaterialSet& operator=(const MaterialSet&);
    //@}

    int m_matId;
    int m_fieldId;

};

}

#endif // MaterialSet_h
