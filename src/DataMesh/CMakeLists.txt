project(DataMesh CXX)

# Include configuration/headers/libraries common to all code
INCLUDE(${HYDRA_SOURCE_DIR}/HydraCommon.cmake)
INCLUDE(${HYDRA_SOURCE_DIR}/HydraDefine.cmake)

include_directories(${HYDRA_SOURCE_DIR}/Base
                    ${HYDRA_SOURCE_DIR}/Control
                    ${HYDRA_SOURCE_DIR}/DataMesh
                    ${HYDRA_SOURCE_DIR}/IO
                    ${HYDRA_SOURCE_DIR}/Materials
                    ${PARMETIS_INCLUDE_DIR}
)

include(HydraAddLibrary)
HYDRA_ADD_LIBRARY(DataMesh)

INSTALL(TARGETS DataMesh
        RUNTIME DESTINATION bin COMPONENT Runtime
        LIBRARY DESTINATION lib COMPONENT Runtime
        ARCHIVE DESTINATION lib COMPONENT Development
)
