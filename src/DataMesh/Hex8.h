//******************************************************************************
//! \file    src/DataMesh/Hex8.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   Hex-8 element
//******************************************************************************
#ifndef Hex8_h
#define Hex8_h

#include <Element.h>
#include <Hex8QuadSet.h>

namespace Hydra {

//! 8-Node hex element
#define NDIM_HEX8        3   // Spatial dimensions
#define NNPE_HEX8        8   //!< No. of nodes per element
#define NUM_HEX8_EDGES   12  //!< No. of edges per element
#define NNPE_HEX8_EDGES  2   //!< No. of nodes per hex8 edge
#define NUM_HEX8_FACES   6   //!< No. of faces per element
#define NNPE_HEX8_FACES  4   //!< No. of nodes per hex8 face

struct Hex8IX {
  int ix[NNPE_HEX8];  // Connectivity
};

struct Hex8Element {
  int ix[NNPE_HEX8];  // Connectivity
  int mat;            // Material Id
  int oid;            // Ordinal Id
  int label;          // User label
  int instance;       // Instance parent
};

struct Hex8Bmatrix {
  Real X[NNPE_HEX8];
  Real Y[NNPE_HEX8];
  Real Z[NNPE_HEX8];
};

struct Hex8DualEdgeIds {
  int eid[NUM_HEX8_FACES];  // Dual-mesh edge Id's -- one per face
};

//! 8-node hex element
class Hex8 : public Element {

  public:

    //! \name Constructor/Destructor
    //@{
             Hex8(int ecid, int nel);
    virtual ~Hex8();
    //@}

    //! Setup dual-grid adjacency for ghost
    virtual int addGhostAdjacency(UnsHashMap& hmap, int ecid, int is,
                                  int matid, int proc, int* ghost_conn);

    //! Calculate the B-matrix
    virtual int calcBmatrix(const CVector& coord, Real* V);

    //! Calculate the element centroid
    virtual void calcCentroid(const CVector& coord, CVector& cent);

    //! Calculate the dual-edge data
    virtual void calcDualEdgeData(DualEdge* edge, const CVector& coord);

    //! Calculate element quality metrics
    virtual void calcMetrics(CVector& /* coord */);

    //! Calculate surface integral and assemble to global array
    virtual void surfaceIntegral(int gid, int side, Real wt, CVector& coord,
                                 Real* rhs);

    //! Calculate the element volume
    virtual bool calcVolume(const CVector& coord, Real* v);

    //! Echo the nodal connectivity
    virtual void echoConnectivity(ostream& ofs);

    //! Return the number of edges per element
    virtual int getEdgesPerElement() {return NUM_HEX8_EDGES;}

    //! Get the edge connectivity, i.e., 2-nodes
    virtual void getEdgeConnectivity(int lid, int eid, int& n1, int& n2);

    //! Get the face connectivity
    virtual void getFaceConnectivity(int lid, int face, int* conn, int* lconn);

    //! Return the number of nodes per edge given an edgeId
    virtual int getNodesPerEdge(int /*edgeId*/) const {
      return NNPE_HEX8_EDGES;
    }

    //! Return the number of nodes per face give a face Id
    virtual int getNodesPerFace(int /*faceId*/) const {
      return NNPE_HEX8_FACES;
    }

    //! Quadrature-set is initialized and ready to use?
    virtual bool hasQuadSet() {return m_h8qs.isInitialized();}

    //! Inside element test
    virtual bool inElement(int lid, const Vector pt, CVector& coord);

    //! Register the B-matrix
    virtual void registerBmatrix();

    //! Set the dual edge information in a given element
    virtual void registerDualEdgeIds();

    //! Set the dual edge information in a given element
    virtual void setDualEdgeId(int lid, int side, int eid);

    //! Get the global dual-edge ID for a given global element ID & side
    virtual int getDualEdgeId(int gid, int side);

    //! Setup dual-grid adjacency
    virtual int setupAdjacency(UnsHashMap& hmap, int ecid);

    //! Setup edge-edge list for ghosts
    virtual void setupGhostEdgeList(UnsHashMap& hmap, int proc,
                                    int* el_conn, int& curlen, int* local_list);

    //! Set the size of the Hex8 element for communications
    virtual int sizeofElement() {return sizeof(Hex8Element);}

    //**************************************************************************
    //! \name Element specific return types
    //{@

    //! Shape the B-matrix for direct calculation
    Hex8Bmatrix* getHex8Bmatrix();

    //! Return dual-edge Id's for element class
    Hex8DualEdgeIds* getHex8DualEdgeIds() {
      return reinterpret_cast<Hex8DualEdgeIds*>(getDualEdgeIds());
    }

    //! Return element connectivity shaped for Tri3
    Hex8IX* getHex8Connectivity() {
      return reinterpret_cast<Hex8IX*>(getConnectivity());
    };

    //! Return quadrature set -- may not be initialized
    Hex8QuadSet& getHex8QuadSet() {return m_h8qs;}

    //}@
    /**************************************************************************/

  protected:

  private:

    //! Don't permit copy or assignment operators
    //@{
    Hex8(const Hex8&);
    Hex8& operator=(const Hex8&);
    //@}

    //! Quadrature set initialized with registration of the B-matrix
    Hex8QuadSet m_h8qs;

    static int m_edges[NUM_HEX8_EDGES][NNPE_HEX8_EDGES]; //<! Local edges
    static int m_faces[NUM_HEX8_FACES][NNPE_HEX8_FACES]; //<! Local faces
};

}

#endif // Hex8_h
