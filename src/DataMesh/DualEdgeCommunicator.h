//******************************************************************************
//! \file    src/DataMesh/DualEdgeCommunicator.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   dual-edge communicator
//******************************************************************************

#ifndef DualEdgeCommunicator_h
#define DualEdgeCommunicator_h

#include <fstream>

#include <Communicator.h>

namespace Hydra {

//! Dual Edge Communicator
class DualEdgeCommunicator : public Communicator {
  public:

    //! Communication tags
    enum CommTag {GHOST_TAG = 1101};

    //! \name Constructor/Destructor
    //@{
             DualEdgeCommunicator();
    virtual ~DualEdgeCommunicator();
    //@}

    void assembleEdgeColumnVar( int nVarDim, int stride,
                                Real* var_edge, Real* recv_buf);

    void assembleEdgeMinVar(int* var_edge, int* recv_buf);

    void assembleEdgeRowVar(bool* var_edge, bool* recv_buf);

    void assembleEdgeRowVar(int* var_edge, int* recv_buf);

    void assembleEdgeRowVar(int nVarDim, Real* var_edge, Real* recv_buf);

    void echoCommLists(ostream& ofs);

    void gatherEdgeColumnVar(int nVarDim, int stride, Real* var_edge,
                             Real* send_buf);

    void gatherEdgeRowVar(bool* var_edge, bool* send_buf);

    void gatherEdgeRowVar(int* var_edge, int* send_buf);

    void gatherEdgeRowVar(int nVarDim, Real* var_edge, Real* send_buf);


    //! \name NodeCommunicator access functions
    //@{
    int  getNedge() {return m_Nedge;}
    int  getNcomm() {return m_Ncomm;}

    int* getLocalList();
    int* getProcList();
    int* getCommPid();
    int* getCommLen();

    DataIndex getLocalListIndex()  {return LOCAL_LIST; }
    DataIndex getProcListIndex()   {return PROC_LIST;  }

    void registerData();

    void setId(int comm_id)   {m_Id = comm_id;  }

    void sendEdgeVar(bool* send_buf, bool* recv_buf );

    void sendEdgeVar(int* send_buf, int* recv_buf );

    void sendEdgeVar(int nVarDim, Real* send_buf, Real* recv_buf );

    void setNcomm(int ncomm ) {m_Ncomm = ncomm;}

    void setNedge(int comm_edge) {m_Nedge = comm_edge;}

    void swapEdgeColumnVar(int nVarDim, int stride,
                           Real* var_edge, Real* send_buf, Real* recv_buf);

    void swapEdgeMinVar(int* var_edge, int* send_buf, int* recv_buf);

    void swapEdgeRowVar(bool* var_edge, bool* send_buf, bool* recv_buf);

    void swapEdgeRowVar(int* var_edge, int* send_buf, int* recv_buf);

    void swapEdgeRowVar(int nVarDim, Real* var_edge,
                        Real* send_buf, Real* recv_buf);
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    DualEdgeCommunicator(const DualEdgeCommunicator&);
    DualEdgeCommunicator& operator=(const DualEdgeCommunicator&);
    //@}

    //@{
    int m_pid;             //<! Processor Id (aka MPI rank)
    int m_Nproc;           //<! No. of processors
    int m_Nedge;           //<! No. of edges for communcation
    int m_Ncomm;           //<! No. of processors for communication

    DataIndex COMM_PID;    //<! List of processor id's to communcicate with
    DataIndex COMM_LEN;    //<! Message lengths assuming 1-DOF
    DataIndex LOCAL_LIST;  //<! List of local  edgeents for communication
    DataIndex PROC_LIST;   //<! List of processors to talk with
    DataIndex REQUESTS;    //<! Communications requests -- non-blocking mode
    //@}

};

}

#endif // DualEdgeCommunicator_h
