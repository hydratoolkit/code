//******************************************************************************
//! \file    src/DataMesh/ElemCommunicator.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   Element communicator
//******************************************************************************
#ifndef ElemCommunicator_h
#define ElemCommunicator_h

#include <fstream>
#include <set>

#include <Communicator.h>

namespace Hydra {

//! Simple linked list entry for constructing element communicator
typedef struct _ElemList {
  int oid;
  struct _ElemList *next;
  struct _ElemList *prev;
} ElemList;

//! Structure for ghost and edge mapping
struct EdgeGhost {
  int eid;     //<! edge id
  int ghoid;   //<! ghost index into the receive list
};

//! Element communicator
class ElemCommunicator : public Communicator {

  public:

    //! \name Constructor/Destructor
    //@{
             ElemCommunicator();
    virtual ~ElemCommunicator();
    //@}

    enum CommTag {GHOST_TAG = 1201};

    void echoCommLists(ostream& ofs);

    void registerData();

    void setupCommLists(int Nel, ProcMap* proc_map, int* xadj, int* adjncy,
                        set<int>& el_onproc);

    //! \name ElemCommunicator access functions
    //@{
    int  getNcomm() {return m_Ncomm;}

    int  getNelSend() {return m_Nel_send;}

    int  getNelRecv() {return m_Nel_recv;}

    void setNelSend(int comm_nel) {m_Nel_send = comm_nel;}

    void setNelRecv(int comm_nel) {m_Nel_recv = comm_nel;}

    void setNcomm(int ncomm ) {m_Ncomm = ncomm;}

    int* getLocalSendList() {
      return getVariable<int>(LOCAL_SEND_LIST);
    }

    int* getLocalRecvList() {
      return getVariable<int>(LOCAL_RECV_LIST);
    }

    int* getCommPid() {
      return getVariable<int>(COMM_PID);
    }

    int* getCommSendLen() {
      return getVariable<int>(COMM_SEND_LEN);
    }

    int* getCommRecvLen() {
      return getVariable<int>(COMM_RECV_LEN);
    }

    void registerEdgeToGhost(int nedge_ghost);

    EdgeGhost* getEdgeToGhost() {
      return getVariable<EdgeGhost>(EDGE_TO_GHOST);
    }

    Real* getSendRealBuffer() {
      return getVariable<Real>(COMM_SEND_BUF);
    }

    Real* getRecvRealBuffer() {
      return getVariable<Real>(COMM_RECV_BUF);
    }

    int*  getSendIntBuffer() {
      return getVariable<int>(COMM_SEND_BUF);
    }

    int*  getRecvIntBuffer() {
      return getVariable<int>(COMM_RECV_BUF);
    }

    void registerCommBuffers(int sendVarDim, int datasize);

    void freeCommBuffers();

    void gatherGhostColumnVar(int nVarDim, int stride,
                              Real* var_elem, Real* send_buf);

    void scatterGhostColumnVar(int nedge_ghost,
                               int nVarDim, int stride_gt,
                               Real* recv_buf,  Real* var_gt);

    void swapGhostColumnVar(int nedge_ghost,
                            int sendVarDim, int nVarDim, int stride_el,
                            int stride_gt, Real* var_elem, Real* var_gt,
                            Real* send_buf, Real* recv_buf );

    void gatherGhostRowVar(int sendVarDim, int nVarDim,
                           Real* var_elem, Real* send_buf);

    void scatterGhostRowVar(int nedge_ghost, int sendVarDim,
                            int nVarDim, Real* recv_buf, Real* var_gt);

    void swapGhostRowVar(int nedge_ghost, int sendVarDim, int nVarDim,
                         Real* var_elem, Real* var_gt,
                         Real* send_buf, Real* recv_buf);

    void sendGhostVar(int sendVarDim, Real* send_buf, Real* recv_buf);

    DataIndex getLocalSendIndex() {return LOCAL_SEND_LIST;}

    DataIndex getLocalRecvIndex() {return LOCAL_RECV_LIST;}

    void setId(int comm_id)  {m_Id  = comm_id;}
    //@}


  private:

    //! Don't permit copy or assignment operators
    //@{
    ElemCommunicator(const ElemCommunicator&);
    ElemCommunicator& operator=(const ElemCommunicator&);
    //@}

    //@{
    int m_Nel_send;            //<! No. of elements for send
    int m_Nel_recv;            //<! No. of elements for recv
    int m_Ncomm;               //<! No. of processors for communication
    int m_pid;                 //<! Processor Id (aka MPI rank)
    int m_Nproc;               //<! No. of processors

    DataIndex COMM_PID;        //<! List of processor id's to communcicate with
    DataIndex COMM_SEND_LEN;   //<! Message lengths assuming 1-DOF
    DataIndex COMM_RECV_LEN;   //<! Message lengths assuming 1-DOF
    DataIndex COMM_SEND_BUF;   //<! sending buffer for communication
    DataIndex COMM_RECV_BUF;   //<! receive buffer for communication
    DataIndex REQUESTS;        //<! Communications requests -- non-blocking mode
    DataIndex EDGE_TO_GHOST;   //<! Mappping between edge and ghost for receive

    //! Send Lists: [Nel_send]
    //! This is the list of processor-local global element id's that are to
    //! be used to gather data for sending to other processors.
    //! The global id's are not local to a specific element class.
    DataIndex LOCAL_SEND_LIST;

    //! Recv Lists: [Nel_recv]
    //! This is the list of ordinal element id's after load-balancing.
    //! The id's are ordered according to the new global distribution of
    //! elements after load-balaning.
    //! The List of ordinal id's are not local to a specific element class.
    DataIndex LOCAL_RECV_LIST;
    //@}

};

}

#endif // ElemCommunicator_h
