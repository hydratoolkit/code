//******************************************************************************
//! \file    src/DataMesh/Poly3D.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   3-D polyhedral element
//******************************************************************************
#ifndef Poly3D_h
#define Poly3D_h

#include <Element.h>
#include <FaceConnectivity.h>

namespace Hydra {

//! POLY3D hex element
#define NDIM_POLY3D        3   //!< Spatial dimensions
#define NNPE_POLY3D        64  //!< No. of nodes per element (maximum)
#define NUM_POLY3D_EDGES   12  //!< No. of edges per element
#define NNPE_POLY3D_EDGES  2   //!< No. of nodes per poly3d edge
#define NUM_POLY3D_FACES   12  //!< No. of faces per element (maximum)
#define NNPE_POLY3D_FACES  10  //!< No. of nodes per poly3d face (maximum)

//! Note that the maximum number of nodes per polyhedron is set at an
//! upper limit of 64 for the load balancer.  This will be resolved
//! in a later update. (MAC)

struct Poly3DElement {
  int ix[NNPE_POLY3D];  //!< Connectivity
  int mat;              //!< Material Id
  int oid;              //!< Ordinal Id
  int label;            //!< User label
  int instance;         // Instance parent
};

struct elemConnect {
  int Nnpe;        //!< Number of nodes per element
  vector<int> Id;  //!< Integer connectivity for the faces of a polyhedron
};

//! 3-D polyhedral element
class Poly3D : public Element {

  public:

    //! \name Constructor/Destructor
    //@{
             Poly3D(int ecid, int nel);
    virtual ~Poly3D();
    //@}

    //! Setup dual-grid adjacency for ghost
    virtual int addGhostAdjacency(UnsHashMap& hmap, int ecid, int is,
                                  int matid, int proc, int* ghost_conn);

    //! Calculate the B-matrix
    virtual int calcBmatrix(const CVector& coord, Real* V);

    //! Calculate the element centroid
    virtual void calcCentroid(const CVector& coord, CVector& cent);

    //! Calculate the dual-edge data
    virtual void calcDualEdgeData(DualEdge* edge, const CVector& coord);

    //! Calculate surface integral and assemble to global array
    virtual void surfaceIntegral(int gid, int side, Real wt, CVector& coord,
                                 Real* rhs);

    //! Calculate the element quality metrics
    virtual void calcMetrics(CVector& /* coord */);

    //! Calculate the element volume
    virtual bool calcVolume(const CVector& coord, Real* v);

    //! Echo the nodal connectivity
    virtual void echoConnectivity(ostream& ofs);

    //! Return the number of edges per element
    virtual int getEdgesPerElement() {return NUM_POLY3D_EDGES;}

    //! Get the edge connectivity, i.e., 2-nodes
    virtual void getEdgeConnectivity(int lid, int eid, int& n1, int& n2);

    //! Get the face connectivity
    virtual void getFaceConnectivity(int lid, int face, int* conn, int* lconn);

    //! Return the number of nodes per edge given an edgeId
    virtual int getNodesPerEdge(int /*edgeId*/) const {
      return NNPE_POLY3D_EDGES;
    }

    //! Return the number of nodes per face give a face Id
    virtual int getNodesPerFace(int /*faceId*/) const {
      return NNPE_POLY3D_FACES;
    }

    //! Quadrature-set is initialized and ready to use?
    virtual bool hasQuadSet() {return false;}

    //! Inside element test (stub for now)
    virtual bool inElement(int /*lid*/,const Vector /*pt*/, CVector& /*coord*/){
      return false;
    }

    //! Register the B-matrix
    virtual void registerBmatrix();

    //! Set the dual edge information in a given element
    virtual void registerDualEdgeIds();

    //! Set the dual edge information in a given element
    virtual void setDualEdgeId(int lid, int side, int eid);

    //! Get the global dual-edge ID for a given global element ID & side
    virtual int getDualEdgeId(int gid, int side);

    //! Setup dual-grid adjacency
    virtual int setupAdjacency(UnsHashMap& hmap, int ecid);

    //! Setup edge-edge list for ghosts
    virtual void setupGhostEdgeList(UnsHashMap& hmap, int proc,
                                    int* el_conn, int& curlen, int* local_list);

    //! Set the size of the Poly3D element for communications
    virtual int sizeofElement() {return sizeof(Poly3DElement);}

    //! Return the number of nodes per element for a given element
    int getNnpe(int lid) const {return m_elemConnect[lid].Nnpe;}

    //! Number of faces per element as a function of local element id
    int getFacesPerElement(const int lid) const {
      return m_elemConnect[lid].Id.size();
    }

    //! The function will return integer connectivity and the number of nodes
    //! for the given lid
    virtual int* getNodeConnectivity(const int lid, int& nnpe);

    //**************************************************************************
    //! \name Element specific return types
    //{@

    //! Add a polygonal element to the class
    void addElement() {m_elemConnect.push_back(elemConnect());}

    //! Return the connectivity for an element in the class
    elemConnect& getElemConnectivity(int lid) {return m_elemConnect[lid];}

    //}@
    /**************************************************************************/

  private:

    //! Don't permit copy or assignment operators
    //@{
    Poly3D(const Poly3D&);
    Poly3D& operator=(const Poly3D&);
    //@}

    vector<dualEdgeId>  m_dualEdgeIds;  //<! Dual edge Id's for the class
    vector<faceConnect> m_faceConnect;  //<! Node-to-face connectivity
    vector<elemConnect> m_elemConnect;  //<! Face-to-element connectivity
};

}

#endif // Poly3D_h
