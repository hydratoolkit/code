//******************************************************************************
//! \file    src/DataMesh/Tet4.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   4-node Tet element
//******************************************************************************
#ifndef Tet4_h
#define Tet4_h

#include <Element.h>
#include <Tet4QuadSet.h>

namespace Hydra {

//! 4-Node tet element
#define NDIM_TET4        3   //!< Spatial dimensions
#define NNPE_TET4        4   //!< No. of nodes per element
#define NUM_TET4_EDGES   6   //!< No. of edges per element
#define NNPE_TET4_EDGES  2   //!< No. of nodes per tet4 edge
#define NUM_TET4_FACES   4   //!< No. of faces per element
#define NNPE_TET4_FACES  3   //!< No. of nodes per tet4 face

struct Tet4IX {
  int ix[NNPE_TET4];  //!< Connectivity
};

struct Tet4Element {
  int ix[NNPE_TET4];  //!< Connectivity
  int mat;            //!< Material Id
  int oid;            //!< Ordinal Id
  int label;          //!< User label
};

struct Tet4Bmatrix {
  Real X[NNPE_TET4];
  Real Y[NNPE_TET4];
  Real Z[NNPE_TET4];
};

struct Tet4DualEdgeIds {
  int eid[NUM_TET4_FACES];  //!< Dual-mesh edge Id's -- one per face
};

//! 4-node tet element
class Tet4: public Element {

  public:

    //! \name Constructor/Destructor
    //@{
             Tet4(int ecid, int nel);
    virtual ~Tet4();
    //@}

    //! Setup dual-grid adjacency for ghost
    virtual int addGhostAdjacency(UnsHashMap& hmap, int ecid, int is,
                                  int matid, int proc, int* ghost_conn);

    //! Calculate the B-matrix
    virtual int calcBmatrix(const CVector& coord, Real* V);

    //! Calculate the element centroid
    virtual void calcCentroid(const CVector& coord, CVector& cent);

    //! Calculate the dual-edge data
    virtual void calcDualEdgeData(DualEdge* edge, const CVector& coord);

    //! Calculate the element quality metrics
    virtual void calcMetrics(CVector& /* coord */);

    //! Calculate surface integral and assemble to global array
    virtual void surfaceIntegral(int gid, int side, Real wt, CVector& coord,
                                 Real* rhs);

    //! Calculate the element volume
    virtual bool calcVolume(const CVector& coord, Real* v);

    //! Echo the nodal connectivity
    virtual void echoConnectivity(ostream& ofs);

    //! Return the number of edges per element
    virtual int getEdgesPerElement() {return NUM_TET4_FACES;}

    //! Get the edge connectivity, i.e., 2-nodes
    virtual void getEdgeConnectivity(int lid, int eid, int& n1, int& n2);

    //! Get the face connectivity
    virtual void getFaceConnectivity(int lid, int face, int* conn, int* lconn);

    //! Return the number of nodes per edge given an edgeId
    virtual int getNodesPerEdge(int /*edgeId*/) const {
      return NNPE_TET4_EDGES;
    }

    //! Return the number of nodes per face give a face Id
    virtual int getNodesPerFace(int /*faceID*/) const {
      return NNPE_TET4_FACES;
    }

    //! Quadrature-set is initialized and ready to use?
    virtual bool hasQuadSet() {return false;}

    //! Inside element test
    virtual bool inElement(int lid, const Vector pt, CVector& coord);

    //! Register the B-matrix
    virtual void registerBmatrix();

    //! Set the dual edge information in a given element
    virtual void registerDualEdgeIds();

    //! Set the dual edge information in a given element
    virtual void setDualEdgeId(int lid, int side, int eid);

    //! Get the global dual-edge ID for a given global element ID & side
    virtual int getDualEdgeId(int gid, int side);

    //! Setup dual-grid adjacency
    virtual int setupAdjacency(UnsHashMap& hmap, int ecid);

    //! Setup edge-edge list for ghosts
    virtual void setupGhostEdgeList(UnsHashMap& hmap, int proc,
                                    int* el_conn, int& curlen, int* local_list);

    //! Set the size of the Hex8 element for communications
    virtual int sizeofElement() {return sizeof(Tet4Element);}

    //**************************************************************************
    //! \name Element specific return types
    //{@

    //! Shape the B-matrix for direct calculation
    Tet4Bmatrix* getTet4Bmatrix();

    //! Return dual-edge Id's for element class
    Tet4DualEdgeIds* getTet4DualEdgeIds() {
      return reinterpret_cast<Tet4DualEdgeIds*>(getDualEdgeIds());
    }

    //! Return element connectivity shaped for Tet4
    Tet4IX* getTet4Connectivity() {
      return reinterpret_cast<Tet4IX*>(getConnectivity());
    };

    //}@
    /**************************************************************************/

  private:

    //! Don't permit copy or assignment operators
    //@{
    Tet4(const Tet4&);
    Tet4& operator=(const Tet4&);
    //@}

    //! There is no quadrature set currently used with TET4
    Tet4QuadSet m_t4qs;

    static int m_edges[NUM_TET4_EDGES][NNPE_TET4_EDGES]; //<! Local edges
    static int m_faces[NUM_TET4_FACES][NNPE_TET4_FACES]; //<! Local faces
};

}

#endif // Tet4_h
