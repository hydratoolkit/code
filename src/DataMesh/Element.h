//******************************************************************************
//! \file    src/DataMesh/Element.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   Base class for deriving elements
//******************************************************************************
#ifndef Element_h
#define Element_h

#include <fstream>
#include <string>
#include <map>
#include <vector>

#include <DataContainer.h>
#include <DataShapes.h>
#include <DualEdge.h>

namespace Hydra {

//! Undefined node number for variable-topology faces on PYR5
#define UNDEFINED_NODE -1

//! Maximum quantities over all element types
#define MAXELEM_FACES 6

//! Element types by topology
enum Eltype {BEAM2=0,          //!< 2-node beam
             POLY2D,           //!< 2-D polygonal
             QUAD4,            //!< 4-node quadrilateral
             TRI3,             //!< 3-node triangle
             HEX8,             //!< 8-node hex
             POLY3D,           //!< 3-D polyhedral
             PYR5,             //!< 5-node pyramid
             TET4,             //!< 4-node tetrahedra
             WEDGE6,           //!< 6-node wedge
             UNKNOWN_ELEMENT,  //!< Unknown type
             NUM_ELEMENT_TYPES //!< # of element types
};


//! Hash table for determining element adjacency, etc.
struct UnsHash {
  int nel;             //! Number of elements on the dual-edge
  int gid1;            //! Element global Id at node-1
  int lid1;            //! Element lodal Id Id at node-1
  int side1;           //! Element side at node-1
  int ecid1;           //! Element class Id at node-1
  Eltype etype1;       //! Element class type at node-1
  int gid2;            //! Element global Id at node-2
  int lid2;            //! Element local-Id at node-2
  int side2;           //! Element side at node-2
  int ecid2;           //! Element class Id at node-2
  Eltype etype2;       //! Element class type at node-2
  bool ifc;            //! True if there is an IFC owned on the solid material
  bool ifcbc;          //! True for fluid-owned IFC with an associated BC
  // constructor - requires setting of only specific values
UnsHash(int nelarg,
        int gid1arg, int lid1arg, int side1arg, int ecid1arg, Eltype etype1arg,
        int gid2arg=-1, int side2arg=-1, int lid2arg=-1, int ecid2arg=-1, Eltype etype2arg=UNKNOWN_ELEMENT,
        bool ifcarg=false, bool ifbcarg=false) :
  nel(nelarg),
    gid1(gid1arg), lid1(lid1arg), side1(side1arg), ecid1(ecid1arg), etype1(etype1arg),
    gid2(gid2arg), lid2(lid2arg), side2(side2arg), ecid2(ecid2arg), etype2(etype2arg),
    ifc(ifcarg), ifcbc(ifbcarg){}
};

typedef map<vector<int>, UnsHash> UnsHashMap;

//! Abstract base class for elements
class Element : public DataContainer {

  public:

    //! \name Constructor/Destructor
    //@{
             Element(Eltype etype, int ecid, int nel, int nnpe, int nfpe);
    virtual ~Element();
    //@}

    //! \name Virtual element interface
    //@{

    //! Setup dual-grid adjacency for ghost
    virtual int addGhostAdjacency(UnsHashMap& hmap, int ecid, int is,
                                  int matid, int proc, int* recv_buf) = 0;

    //! Calculate the B-matrix -- this should really be done on physics-side
    virtual int calcBmatrix(const CVector& coord, Real* V) = 0;

    //! Calculate the element centroid
    virtual void calcCentroid(const CVector& coord, CVector& cent) = 0;

    //! Calculate the dual-edge data
    virtual void calcDualEdgeData(DualEdge* edge, const CVector& coord) = 0;

    //! Calculate element quality metrics
    virtual void calcMetrics(CVector& /* coord */) = 0;

    //! Calculate surface area on a side
    virtual Real calcSurfaceArea(int /*gid*/, int /*side*/, CVector& /*coord*/){
      cout << "!!! Surface area not implemented in base class !!!" << endl;
      return 0.0;
    }

    //! Calculate surface integral and assemble to global array
    virtual void surfaceIntegral(int gid, int side, Real wt, CVector& coord,
                                 Real* rhs) = 0;

    //! Calculate the element volume
    virtual bool calcVolume(const CVector& coord, Real* v) = 0;

    //! Echo the nodal connectivity
    virtual void echoConnectivity(ostream& ofs) = 0;

    //! Return the number of edges per element
    virtual int getEdgesPerElement() = 0;

    //! Get the edge connectivity, i.e., 2-nodes
    virtual void getEdgeConnectivity(int lid, int eid, int& n1, int& n2) = 0;

    //! Get the face connectivity (misnomer for 2-D elements)
    virtual void getFaceConnectivity(int lid, int face, int* conn, int* lconn) = 0;

    //! Return the number of nodes per edge given an edgeId
    virtual int getNodesPerEdge(int edgeId) const = 0;

    //! Return the number of nodes per face given a faceId
    virtual int getNodesPerFace(int faceId) const = 0;

    //! Quadrature-set is initialized and ready to use?
    virtual bool hasQuadSet() = 0;

    //! Inside element test
    virtual bool inElement(int lid, const Vector pt, CVector& coord) = 0;

    //! Register the B-matrix
    virtual void registerBmatrix() = 0;

    //! Register data for this class, e.g. materials, global ids, connectivities
    virtual void registerData();

    //! Register dual edges
    virtual void registerDualEdgeIds() = 0;

    //! Set the dual-edge information in a given element
    virtual void setDualEdgeId(int lid, int side, int eid) = 0;

    //! Get the global dual-edge ID for a given global element ID & side
    virtual int getDualEdgeId(int gid, int side) = 0;

    //! Setup dual-grid adjacency
    virtual int setupAdjacency(UnsHashMap& htab, int ecid) = 0;

    //! Get ghost edge list
    virtual void setupGhostEdgeList(UnsHashMap& hmap, int proc,
                                    int* el_conn,
                                    int& curlen, int* local_list) = 0;

    //! Size the element for data migration
    virtual int sizeofElement() = 0;
    //@}

    //! This function is re-defined below without the lid argument
    virtual int getNnpe(int /*lid*/) const {return m_nnpe;}

    //! The function will return integer connectivity and the number of nodes
    //! for the given lid
    virtual int* getNodeConnectivity(const int lid, int& Nnpe);

    //! \name Element data migration
    //@{
    //! Finalize element migration
    void finalizeMigration();

    //! Return the "new" number of elements during load-balancing
    int getNewNel() const { return m_nel_new; }

    //! Setup migration plan for an element class
    void setupMigration(int* procid, int* elem_send, int* elem_recv);

    //! Unpack elements after data migration
    void unpackElements(int nrecv);

    //@}

    //! Echo the element metrics for the clas
    void echoElemMetrics(ostream& ofs);

    //! Free the B-matrix
    void freeBmatrix();

    //! Free dual edges
    void freeDualEdgeIds();

    //! contiguous B-matrix
    Real* getBmatrix();

    //! Return a pointer to the array of connectivities
    int* getConnectivity() {
      return getVariable<int>(CONNECTIVITY);
    }

    //! Return pointer to new connectivities for load-balancing
    int* getNewConnectivity() {
      return getVariable<int>(NEW_CONNECTIVITY);
    }

    //! Return the DataIndex for the connectivity
    DataIndex getConnectivityID() {return CONNECTIVITY;}

    //! Return a vector of node Ids that belong to the element specified by gid
    vector<int> getNodeIds(int gid);

    //! Return pointer to the dual-edge list
    int* getDualEdgeIds() {return m_dualEdgeIDs;}

    //! Return the element type identifier for this class
    Eltype getEltype() const {return m_eltype;}

    //! Return the element id offset for this class
    int getElementOffset() const {return m_elementOffset;}

    //! Number of faces per element
    int getFacesPerElement() const {return m_nfpe;}

    //! Number of faces per element as a function of local element id
    int getFacesPerElement(const int /*lid*/) const {return m_nfpe;}

    //! Return a pointer to the array of global ids for this class
    int* getGlobalIds() {return m_globalIDs;}

    //! Return the DataIndex for the global id's
    DataIndex getGlobalID() {return GLOBAL_IDS;}

    //! Return the element class id
    int getId() {return m_id;}

    //! Return a pointer to the array of serial ids
    int* getSerialIds() {
      return getVariable<int>(SERIAL_IDS);
    }

    //! Return the DataIndex for the ordinal id's
    DataIndex getSerialID() {return SERIAL_IDS;}

    //! Return the number of elements
    int getNel() const {return m_nel;}

    //! Return the number of nodes per element for the class
    int getNnpe() const {return m_nnpe;}

    //! Get the material Id
    int getMatId(int fieldid=0);

    //! Get number of materials associated with fields in the element class
    int getNmat() {
      return m_fmap.size();
    }

    //! Set the material Id
    void setMatId(int fieldid, int mid);

    //! Access the receive buffer of elements
    char* getRecvBuffer() {
      return getVariable<char>(ELEM_RECV_BUF);
    }

    //! Return a pointer to the array of user labels
    int* getUserLabels() {
      return getVariable<int>(USER_LABELS);
    }

    //! B-matrix created?
    bool hasBmatrix() {return m_bmatrix;}

    //! Pack elements up for migration
    char* packElements(int* procid, int* send_offset, int* send_list);

    //! Set the element id offset for this class
    void setElementOffset(int eoff) { m_elementOffset = eoff; }

    //! Set the number of elements in the class
    void setNel(int nel) {m_nel = nel;}

    // Get the user name
    string& getName() {return m_name;}

    // Get the user name
    string& getUserName() {return m_uname;}

    //! Set the user name -- default case
    void setUserName();

    //! Set the user name
    void setUserName(string& uname);


  protected:

    //! Returns true if all elements of the vector >= 0
    bool leZero(const vector<int>& v);

    //! Insert a hash entry for element adjacency
    int insertHashEntry(UnsHashMap& hmap,
                        const vector<int>& key,
                        int ecid,
                        Eltype type,
                        int gid,
                        int lid,
                        int side);

    //! Insert a hash entry for ghost element adjacency
    int insertGhostHashEntry(UnsHashMap& hmap,
                             const vector<int>& key,
                             int ecid, int matid, int is, int proc);

    //! Get the ghost edge from ghost element
    void getGhostEdgeList(UnsHashMap& hmap,
                          const vector<int>& key,
                          int proc, int& curlen, int* local_list );

    DataIndex GLOBAL_IDS;
    DataIndex SERIAL_IDS;
    DataIndex USER_LABELS;
    DataIndex CONNECTIVITY;
    DataIndex DUAL_EDGE_IDS;
    DataIndex BMATRIX;

    //! NOTE: The following pointers are cached for performance reasons.  Care
    //! must be taken that these pointers don't become stale in the event that
    //! the data index is reset, e.g. global ids after a load balance.

    int* m_globalIDs;   //!< Cache pointer for performance
    int* m_dualEdgeIDs; //!< Cache pointer for performance

    //! \name Data used for element migration
    //{@
    int m_nel_new;
    int m_nel_send;
    int m_nel_recv;
    int m_cur_el;

    DataIndex NEW_SERIAL_IDS;
    DataIndex NEW_CONNECTIVITY;
    DataIndex NEW_USER_LABELS;
    DataIndex ELEM_SEND_BUF;
    DataIndex ELEM_RECV_BUF;
    //}@

    //! \name Basic data for the class
    //@{
    bool m_bmatrix;       //!< Indicates equipped with B-matrix

    int m_id;             //!< Element class Id, aka block Id in Exodus-II
    int m_nel;            //!< No. of elements
    int m_nnpe;           //!< No. of nodes per element
    int m_nfpe;           //!< No. of faces per element
    int m_elementOffset;  //!< Element offset for elements in this class

    Eltype m_eltype;      //!< Element type, e.g., quad, hex, etc.
    string m_name;        //!< Element type in string form
    string m_uname;       //!< User name for the element class

    Real minAspect;       //!< Min. aspect ratio
    Real maxAspect;       //!< Max. aspect ratio
    Real minSkew;         //!< Min. skeweness
    Real maxSkew;         //!< Max. skewness

    typedef map<int,int> FieldMaterialMap;
    FieldMaterialMap m_fmap;  //!< Field material map for global material Id's
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    Element(const Element&);
    Element& operator=(const Element&);
    //@}

};

}

#endif // Element_h
