//******************************************************************************
//! \file    src/DataMesh/CoordinateTypes.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   Basic coordinate types for different reference frames
//******************************************************************************
#ifndef CoordinateTypes_h
#define CoordinateTypes_h

namespace Hydra {

//! Cordinate Types: Eulerian, Lagrangian, ALE
enum CoordinateType {EULERIAN_COORD = 0,
                     LAGRANGIAN_COORD,
                     ALE_COORD
};

}

#endif // CoordinateTypes_h
