//******************************************************************************
//! \file    src/DataMesh/NodeCommunicator.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   Communicator for node centered data
//******************************************************************************
#ifndef NodeCommunicator_h
#define NodeCommunicator_h

#include <fstream>
#include <set>

#include <Communicator.h>
#include <DataShapes.h>

namespace Hydra {

//! Node communicator
class NodeCommunicator : public Communicator {

  public:
    //! \name Communication tags
    //@{
    enum CommTag {SCALAR_TAG = 1000,
                  VECTOR_TAG = 2000,
                  BLOCK_TAG  = 3000 };
    //@}

    //! \name Constructor/Destructor
    //@{
             NodeCommunicator();
    virtual ~NodeCommunicator();
    //@}

    void assembleVector(int* data, int* send_buffer, int* recv_buffer);

    void assembleVector(Real* data, Real* send_buffer, Real* recv_buffer);

    void assembleCVector(CVector& data, Real* send_buffer, Real* recv_buffer);

    void assembleVector(Real* data, Real* send_buf, Real* recv_buf, int Ndof);

    void calcDist(int* send_list, int* recv_list, int* eptr, int* eind,
                  int* ghost_comm_pid, int* comm_send_len,
                  int* ghost_send_list, set<int>& nd_onproc);

    void echoCommLists(ostream& ofs);

    //! \name NodeCommunicator access functions
    //@{
    int  getNnp()        {return m_Nnp;  }
    int  getNcomm()      {return m_Ncomm;}

    int* getLocalList();
    int* getProcList();
    int* getCommPid();
    int* getCommLen();

    DataIndex getLocalListIndex()  {return LOCAL_LIST; }
    DataIndex getProcListIndex()   {return PROC_LIST;  }

    void pack(int* send_list, int* send_offset,
              int* send_buf, int* eptr, int* eind,
              int* ghost_comm_pid, int* comm_send_len,
              int* ghost_send_list, set<int>& nd_onproc);

    void registerData();

    void renumberLocal(map<int,int>& nd_global2local);

    void setId(int comm_id)   {m_Id  = comm_id; }

    void setNnp(int comm_nnp) {m_Nnp = comm_nnp;}

    void setupCommLists(int Ncomm, int* eptr, int* eind,
                        int* ghost_comm_pid, int* comm_send_len,
                        int* ghost_send_list, set<int>& nd_onproc);

    void swapNodeVector(int* data, int* send_buf, int* recv_buf);

    void unpack(int* send_list, int* recv_list, int* recv_offset,
                int* recv_buf,  int* ghost_comm_pid, set<int>& nd_onproc);

    void zeroProcBoundaryNodes(int* iflag, Real* var1, Real* var2);
    //@}

  private:

    //! Don't permit copy or assignment operators
    //@{
    NodeCommunicator(const NodeCommunicator&);
    NodeCommunicator& operator=(const NodeCommunicator&);
    //@}

    //! \name MPI Communicator Data -- use g_comm for now
    //@{
    //! MPI_Comm node_comm;
    //!
    //! node_list_id's = node_cmap_ids!
    //! Nnp            = node_cmap_node_cnts (same as msglen in GILA)!
    //! node_list      = node_ids   -- global node numbers for communication!
    //! node_proc_list = node_proc_ids!

    int m_Nnp;             //<! No. of nodes for communcation
    int m_Ncomm;           //<! No. of processors for communication
    int m_pid;             //<! Processor Id (aka MPI rank)
    int m_Nproc;           //<! No. of processors

    DataIndex COMM_PID;    //<! List of processor id's to communcicate with
    DataIndex COMM_LEN;    //<! Message lengths assuming 1-DOF
    DataIndex LOCAL_LIST;  //<! List of local  nodes for communication
    DataIndex PROC_LIST;   //<! List of processors to communicate with
    DataIndex REQUESTS;    //<! Communications requests -- non-blocking mode
    //@}

};

}

#endif // NodeCommunicator_h
