//******************************************************************************
//! \file    src/DataMesh/Tet4QuadSet.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   4-node Tet quadrature set
//******************************************************************************
#ifndef Tet4QuadSet_h
#define Tet4QuadSet_h

#include <QuadratureSet.h>

namespace Hydra {

//! 4-Node Tet Quadrature Set
class Tet4QuadSet : public QuadratureSet {

  public:

    //! \name Constructor/Destructor
    //@{
             Tet4QuadSet();
    virtual ~Tet4QuadSet();
    //@}

    //! Initialize the quadrature set according to the quadrature rule
    virtual void initialize(int Nqpt_vol, int Nqpt_surf);

    //! Finalize phase
    virtual void finalize();

    int getNumVolPts() {return m_Nqpt_vol;}

    int getNumSurfPts() {return m_Nqpt_surf;}

    Real* getVsfArray(int qdim, int kpt) {
      return &vsf[qdim][kpt][0];
    }

  protected:

    int m_Nqpt_vol;  //!< No. of volume quadrature points
    int m_Nqpt_surf; //!< No. of volume quadrature points

  private:

    //! Don't permit copy or assignment operators
    //@{
    Tet4QuadSet(const Tet4QuadSet&);
    Tet4QuadSet& operator=(const Tet4QuadSet&);
    //@}

    Real     sf4(Real xi, Real eta, Real zeta, int node);
    Real   sf4xi(Real xi, Real eta, Real zeta, int node);
    Real  sf4eta(Real xi, Real eta, Real zeta, int node);
    Real sf4zeta(Real xi, Real eta, Real zeta, int node);

    //! Shape functions, derivatives and quadrature weights
    //! sf[Nqdim][Nqpt][Nnpe]
    Real vsf[4][4][4];

    //! Natural coordinates
    //! qpt[Nqpt][Ndim]
    Real vqpt[4][3];

};

}

#endif
