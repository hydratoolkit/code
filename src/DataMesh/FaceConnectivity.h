//******************************************************************************
//! \file    src/DataMesh/FaceConnectivity.h
//! \author  Mark A. Christon
//! \date    Thu Jul 14 12:32:02 2011
//! \brief   Face Connectivity for polygonal/polyhedra elements
//******************************************************************************
#ifndef FaceConnectivity_h
#define FaceConnectivity_h

#include <vector>

namespace Hydra {

//! Poly Face Connectivity
struct faceConnect {
  vector<int> ix;  // Integer connectivity for the face of a polygon/polyhedron
};

//! Poly Edge Connectivity
struct dualEdgeId {
  vector<int> Id;  // Integer dual-edge Id for polygonal/polyhedral elements
};

}

#endif // Face connectivity

