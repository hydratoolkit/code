//******************************************************************************
//! \file    src/Chemistry/SurfChemInfo.h
//! \author  Balu Nadiga, Mark A. Christon
//! \date    Thu Jul 14 11:50:34 2011
//! \brief   Struct for instantiating SurfChem objects
//******************************************************************************
#ifndef SurfChemInfo_h
#define SurfChemInfo_h

#include <vector>

#include <HydraTypes.h>

namespace Hydra {

//! Flags for different types of surface chemistry models
enum SurfChemType {
  NATIVE
};

//! Flags for different types of wall inputs to surface chemistry models
enum SurfChemWallInput {
  NONE,
  TKE,
  WALL_SHEAR
};

//! Struct for temporarily holding info read in the parser
struct SurfChemInfo{
  SurfChemType type;      //!< NATIVE
  SurfChemWallInput wallinput; //!< tke or wall_shear
  Real startTime;         //!< When should surf chem be activated
  Real endTime;           //!< When should surf chem be turned off
  Real couplingInterval;  //!< Time interval at which surf chem model is called
  bool intss;             //!< Turn surf chem on all internal sidesets or not
  int ssid;               //!< sideset id
  int intId;              //!< internal sideset id
  int tblid;              //!< table id, -1 if constant
  Real amp;               //!< amplitude - SurfChem magnitude

  //! Set defaults
 SurfChemInfo(): type(NATIVE), wallinput(NONE),
    startTime(0.), endTime(0.), couplingInterval(1e10),
    intss(false), ssid(-1), intId(-1), tblid(-1), amp(0.)
  {}
};

}

#endif
