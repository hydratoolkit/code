//******************************************************************************
//! \file    src/Chemistry/SurfChem.h
//! \author  Balu Nadiga, Mark A. Christon
//! \date    Thu Jul 14 12:46:22 2011
//! \brief   Base class for surface chemistry
//******************************************************************************
#ifndef SurfChem_h
#define SurfChem_h

#include <HydraTypes.h>
#include <SurfChemInfo.h>
#include <DataContainer.h>
#include <UnsMesh.h>

namespace Hydra {

class fileIO;

//! Base class for surface chemistry
class SurfChem : public DataContainer {

  public:

    //! Constructor for SurfChem
    //! \name Constructor/Destructor
    //@{
             SurfChem(const SurfChemInfo& msci);
    virtual ~SurfChem();
    //@}

    //! Method to get chemistry type
    SurfChemType getType() const {return m_type;}

    //! Method to get INTERNAL sideset
    int getSet() const {return m_intId;}

    //! Method to get MESH sideset
    int getId() const {return m_ssId;}

    //! Method to get start time
    Real getStart() const {return m_startTime;}

    //! Method to get endTime
    Real getEnd() const {return m_endTime;}

    //! Method to get call interval
    Real getCouplingInterval() const {return m_couplingInterval;}

    //! Method to get time when surface chemistry is to be called next
    Real getNextCallTime() const {return m_nextCallTime;}

    //! Method to get wall input
    SurfChemWallInput getWallInput() const {return m_wallinput;}

    virtual void echoInput(ostream& ofs) =0;

  protected:

    SurfChemType m_type;             //!< surface chemistry model type
    Real m_startTime;       //!< When should surf chem be activated
    Real m_endTime;         //!< When should surf chem be turned off
    Real m_couplingInterval;//!< hydra and surf chem coupling interval
    Real m_lastCallTime;    //!< When sc was last called. Initialized large
    Real m_nextCallTime;    //!< When should surf chem be turned off
    int m_intId;            //!< Internal sideset Id
    int m_ssId;             //!< User sideset Id
    SurfChemWallInput m_wallinput; //!< wall input to chem model
    bool m_initialized;     //!< Whether initialized

  private:

    //! Don't permit copy or assignment operators
    //@{
    SurfChem(const SurfChem&);
    SurfChem& operator=(const SurfChem&);
    //@}

  };

}

#endif
