Steps to using/setting up valgrind w. Hydra

o Compile with CMAKE_BUILD_TYPE = DEBUG

o Linux to generate suppresion information

  valgrind --tool=memcheck --leak-check=full -q \
            --num-callers=20 --gen-suppressions=all \
            --log-file=VG $HYDRA -malloc off \
            -i poiseuille.exo -c poiseuille.cntl


  where $HYDRA points to the hydra executable


o Linux w. suppresion file

  valgrind --tool=memcheck --leak-check=full -q \
            --num-callers=20 --suppressions=<file> \
            --log-file=VG $HYDRA -malloc off \
            -i poiseuille.exo -c poiseuille.cntl


  where $HYDRA points to the hydra executable, and
  <file> is the suppression file, e.g., linux_gcc.supp

