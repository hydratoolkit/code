#!/usr/bin/env perl
#
use strict;
use warnings;
use Getopt::Long;

my $pvfile=pop @::ARGV;
die "File $pvfile does not exist!" unless -e $pvfile;

open(PVD, "$pvfile") or die "Can not open $pvfile: $!";

# Create the label index map
my $first_line = <PVD>;
$first_line =~ s:"::g;
my $save_ptr=tell PVD;
my @labels=split(",",$first_line);
chomp @labels;
my %label_map;
my $idx=0;
foreach my $label ( @labels  ) {
  #print "$label\n";
  $label_map{$label}=$idx;
  $idx++;
}

# Radius scaling (m)
my $R=0.070;

# Scaling for the velocities (m/s)
#my $U_BLK=0.5847;
#my $U_CL=0.6840;
my $U_CL=0.86;
my $U_BLK=$U_CL/1.11;

# Loop through and dump out velocities
if ( (exists $label_map{"<velocity>:0"}) &&
     (exists $label_map{"<velocity>:1"}) &&
     (exists $label_map{"<velocity>:2"}) ) {

  printf("#R=$R\n");
  printf("#U_BLK=$U_BLK\n");
  printf("#y/R,z/R,U/U_BLK,V/U_CL,W_CL\n");

  while(<PVD>) {
    my @data=split(",");
    my $x = $data[$label_map{"Points:0"}];
    my $y = $data[$label_map{"Points:1"}];
    my $z = $data[$label_map{"Points:2"}];
  
    my $vx = $data[$label_map{"<velocity>:0"}];
    my $vy = $data[$label_map{"<velocity>:1"}];
    my $vz = $data[$label_map{"<velocity>:2"}];
  
    #my $sum = $vx*$vx + $vy*$vy + $vz*$vz;
    #my $vmag = sqrt($sum);
    #printf("%1.5e %1.5e %1.5e %1.5e %1.5e\n",
    #           $y/$R, $z/$R, $vx/$U_BLK, $vy/$U_CL, $vz/$U_CL);
    printf("%1.5e %1.5e %1.5e %1.5e %1.5e\n",
               $y/$R, $z/$R, $vx, $vy, $vz);
 
  }

  seek PVD,$save_ptr,0;

}

  

# Temp scaling (*C)
my $T_hi=36.0;
my $T_lo=19.0;

if ( exists $label_map{"<temperature>"} ) {
  printf("\n\n\n\n"); 
  printf("\n#T_hi=$T_hi T_lo=$T_lo\n");
  printf("#\n#\n");
  printf("# y/R, z/R, T/T*\n");
  
  while ( <PVD> ) {
    my @data=split(",");
    my $x = $data[$label_map{"Points:0"}];
    my $y = $data[$label_map{"Points:1"}];
    my $z = $data[$label_map{"Points:2"}];
    my $T = $data[$label_map{"<temperature>"}];
    printf("%1.5e %1.5e %1.5e\n",
              $y/$R, $z/$R, ( ($T-$T_lo)/($T_hi - $T_lo)));
  }

}


# Now close the file
close(PVD);

exit(0);

