The source code search tool "ack" is worth checking out.  See hxxp://beyondgrep.com<http://beyondgrep.com> to download, etc.

% ack foo   <-----recursively searches for "foo" through directory tree within .h and .C files (and within other files ack recognizes)

I have attached a tar file containing a ".ackrc" file that you can put in your home directory as well as a file containing aliases that you can add to your .cshrc or .bashrc file if you want to narrow searching within header files and C files.  For example:

% ackh foo    <-----same search restricted to *.h files
% ackc foo    <-----same search restricted to *.C files

Alan
---------------------------------------------------------------------
Alan Stagg
Modeling and Simulation Group
Computational Sciences and Engineering Division
Oak Ridge National Laboratory
PO Box 2008
Oak Ridge, TN 37831-6085
(865) 576-0055
---------------------------------------------------------------------

