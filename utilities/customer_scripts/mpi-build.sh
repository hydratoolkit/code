#!/bin/bash
#
# Westinghouse MPI build and benchmark logger
#
# Usage:
#  Variables HYDRA, INTEL, SKAMPI_DISTRIBUTION_FILE and MPIROOT 
#  should be altered to match the directory and file locations
#  correct for the system. Once set, run the script and montior
#  progress through the build-mpi-*.log file. This script 
#  builds OpenMPI if the MPIROOT directory does not exist. 
#  After this check, the PATH, LD_LIBRARY_PATH variables are
#  set to use the MPIROOT installation and the simple
#  MPI benchmark SkaMPI is run. The number of processors
#  for the benchmark is TEST_PROCS. Default is 8. The SkaMPI
#  benchmark should run quickly (<10 mintues). It is an 
#  additional sanity check that the MPI is sane.
#
#  

# Define the location of the Hydra-TH distribution
export HYDRA=/local/scratch/lpritch/CASL/wec/hydra-th-20130826

# Intel compiler variables
# Installation location (INTEL)
# architecture 32-bit (ia32) or 64-bit (intel64)
export INTEL=/ccs/opt/x86_64/intel-13.0/composer_xe_2013.0.079
export INTEL_ARCH=intel64

# MPI installation, if this directory does not exist will build OpenMPI 
# distribution defined in the MPI_DISTRIBUTION_FILE and install in this
# directory.
export MPIROOT=/scratch/lpritch/CASL/wec/openmpi

# File use to build OpenMPI. Should be openmpi-x.y.z.tar.gz, default is the OpenMPI
# found in HYDRA
MPI_DISTRIBUTION_FILE=${HYDRA}/tpl/tarballs/openmpi-1.6.5.tar.gz

# Directory to build MPI
MPI_BUILD_DIR=${HYDRA}/build/intel/mpi-build

# SkaMPI benchmark distribution file
SKAMPI_DISTRIBUTION_FILE=/local/scratch/lpritch/CASL/wec/skampi.tar.gz

# Directory to build MPI
SKAMPI_BUILD_DIR=${HYDRA}/build/intel/skampi-build

# Make threads (-j n)
MAKE_THREADS=4

# Test threads (-j n)
TEST_PROCS=8

# Build log file, need full path because there are cd commands
DATE_STAMP=`date +%y%m%d-%H%M`
LOG_FILE=`pwd`/build-mpi-${DATE_STAMP}.log
START_TIME=`date +%s`

# Simple functions
log_header ()
{
  host=`hostname`
  os=`uname -m`
  user=`whoami`
  date_now=`date`
  echo "================================================================================" | tee ${LOG_FILE}
  echo "Machine: $host" | tee -a ${LOG_FILE}
  echo "OS:      $os" | tee -a ${LOG_FILE}
  echo "Builder: ${user}" | tee -a ${LOG_FILE}
  echo "Date:    $date_now" | tee -a ${LOG_FILE}
  echo "" | tee -a ${LOG_FILE}
  echo "Important variables:" | tee -a ${LOG_FILE}
  echo "  HYDRA=${HYDRA}" | tee -a ${LOG_FILE}
  echo "  INTEL=${INTEL}" | tee -a ${LOG_FILE}
  echo "  MPIROOT=${MPIROOT}" | tee -a ${LOG_FILE}
  echo "  CMAKE_BINARY_DIR=${CMAKE_BINARY_DIR}" | tee -a ${LOG_FILE}
  echo "================================================================================" | tee -a ${LOG_FILE}

}

elapsed_time () 
{
  now=`date +%s`
  elap_secs=`echo ${now}-${START_TIME} | bc`
  elap_hrs=`echo $elap_secs/3600 | bc`
  elap_mins=`echo \(${elap_secs}-3600*${elap_hrs}\)/60 | bc`
  remain=`echo \(${elap_secs}-3600*${elap_hrs}-${elap_mins}*60\) | bc`
  echo | tee -a ${LOG_FILE}
  echo "ELAPSED TIME (h:m:s) ${elap_hrs}:${elap_mins}:${remain}" | tee -a ${LOG_FILE}
  echo | tee -a ${LOG_FILE}

}

log_footer ()
{
  echo "================================================================================" | tee -a ${LOG_FILE}
  echo "MPI Build and Benchmark Complete" | tee -a ${LOG_FILE}
  echo "MPI installed in ${MPIROOT}" | tee -a ${LOG_FILE}
  echo "================================================================================" | tee -a ${LOG_FILE}
}
  

exit_now ()
{
  echo "ERROR: '${command}' failed ($status)"
  elapsed_time
  exit 1
}

do_it ()
{
  echo  | tee -a ${LOG_FILE}
  echo "----> $command" | tee -a ${LOG_FILE}
  { $command 2>&1 ; echo $?>.pipestatus.$$ ; } | tee -a ${LOG_FILE}
  status=`cat .pipestatus.$$`
  rm .pipestatus.$$
  if [[ ${status} -ne 0 ]] ; then
    exit_now
  fi
  echo  | tee -a ${LOG_FILE}
  command=
}

dump_env ()
{
  command="printenv"
  do_it
}

dump_variable ()
{
  a=$1
  echo "" | tee -a ${BUILD_DIR}
  echo "----> $a=${!a}" | tee -a ${LOG_FILE}
  echo "" | tee -a ${BUILD_DIR}
}

set_env_variable ()
{
  echo "----> export $1=$2" | tee -a ${LOG_FILE}
  export $1=$2

}

prepend_env_path ()
{
  p=$1
  v=$2
  if [[ ! -z "$p" ]] ; then
    set_env_variable $p ${v}:${!p}
  else
    set_env_variable $p $v
  fi
}

make_directory () 
{
  if [[ ! -d "${1}" ]] ; then
    command="mkdir -p $1"
    do_it
  fi
}


########################################
# Build script begins here
clear   

# --- Create the log file will clobber
log_header


# --- Initalizing the environment

# All environment variables
dump_env

# Variables of interest
dump_variable PATH
dump_variable LD_LIBRARY_PATH
dump_variable LD_RUN_PATH
# --- Intel setup

# Source the compiler scripts, can not use command/do_it to preserve changes
# I echo the command to teh log so we can see the output correctly
intel_compiler_script=${INTEL}/bin/compilervars.sh
command="echo source ${intel_compiler_script} ${INTEL_ARCH}"
do_it
source ${intel_compiler_script} ${INTEL_ARCH}
if [[ "$?" -ne 0 ]] ; then
  command="source ${intel_compiler_script} ${INTEL_ARCH}" # So the error message is correct
  exit_now
fi

mkl_compiler_script=${INTEL}/mkl/bin/mklvars.sh
command="echo source ${mkl_compiler_script} ${INTEL_ARCH}"
do_it
source ${mkl_compiler_script} ${INTEL_ARCH}
if [[ "$?" -ne 0 ]] ; then
  command="source ${mkl_compiler_script} ${INTEL_ARCH}" # So the error message is correct
  exit_now
fi

# Dump out the variables we need set for the compilers
dump_variable PATH
dump_variable LD_LIBRARY_PATH
dump_variable LD_RUN_PATH
dump_variable MKLROOT

# Check the compilers
command="which icc"
do_it
command="icc -V"
do_it
command="which icpc"
do_it
command="icpc -V"
do_it
command="which ifort"
do_it
command="ifort -V"
do_it

# --- MPI setup

# Build MPI if MPIROOT is not found
if [[ ! -d "${MPIROOT}" ]] ; then
  save_dir=`pwd`
  make_directory ${MPI_BUILD_DIR}
  cd ${MPI_BUILD_DIR} || (command="cd ${MPI_BUILD_DIR}" ; exit_now ; )
  command="tar zxf ${MPI_DISTRIBUTION_FILE}"
  do_it
  mpi_dir=`echo ${MPI_DISTRIBUTION_FILE} | sed 's/.*\/\(.*\)\.tar\.gz$/\1/'`
  make_directory 'build'
  cd 'build' || (command="cd build" ; exit_now ; )
  command="../${mpi_dir}/configure --prefix=${MPIROOT} --enable-shared --enable-static CC=icc CXX=icpc FC=ifort"
  do_it
  command="make -j ${MAKE_THREADS}"
  do_it
  command="make check"
  do_it
  command="make install"
  do_it
  cd ${save_dir}
fi

# Set environment variables 
prepend_env_path PATH ${MPIROOT}/bin
prepend_env_path LD_LIBRARY_PATH ${MPIROOT}/lib
prepend_env_path LD_RUN_PATH ${MPIROOT}/lib

# Check the wrappers
command='which mpicc'
do_it
command='mpicc --showme'
do_it
command='which mpicxx'
do_it
command='mpicxx --showme'
do_it
command='which mpif90'
do_it
command='mpif90 --showme'
do_it

# --- SkaMPI

# Will cd and want to save the original directory
save_dir=`pwd`

# Prep the build 
make_directory ${SKAMPI_BUILD_DIR}
cd ${SKAMPI_BUILD_DIR} || ( command="cd $SKAMPI_BUILD_DIR" ; exit_now )

# Unpack the distribution file
command="tar xf ${SKAMPI_DISTRIBUTION_FILE}"
do_it

# Change the directory. May nee to change this directory name
SKAMPI_VERSION=5.0.4-r0355
cd skampi-${SKAMPI_VERSION} || ( command="cd skampi-${SKAMPI_VERSION}" ; exit_now )

# Build SkaMPI
command='make'
do_it

# Check the skampi binary
command='ldd skampi'
do_it

# Run collective benchmark
command="mpirun -np ${TEST_PROCS} skampi -i ski/skampi_coll.ski"
do_it

# Run point to point benchmark
command="mpirun -np ${TEST_PROCS} skampi -i ski/skampi_pt2pt.ski"
do_it

# Run one sided benchmark
#HUNGcommand="mpirun -np ${TEST_PROCS} skampi -i ski/skampi_onesided.ski"
#HUNGdo_it

# Run extra benchmark
command="mpirun -np ${TEST_PROCS} skampi -i ski/skampi_misc.ski"
do_it

# --- Finish up
cd ${save_dir} || ( command="${save_dir}" ; exit_now )

log_footer

elapsed_time


