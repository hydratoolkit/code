#!/bin/bash

#
# Westinghouse Hydra-TH build logger
#
#  Usage:
#   Variables HYDRA, CMAKE_BINARY_DIR, INTEL, and MPIROOT
#   should be altered by the user. Once set the script will
#   build OpenMPI (if neccessary), all the Hydra-TH TPLs
#   and the Hydra-TH binary. Once the binary is built, the
#   parallel and serial regression test suites are launched.
#   Both STDOUT and STDERR pipes are directed to a build log
#   file located in the directory where the script is executed.
#   Test results are stored under the directory $HYDRA/test_results_*
#
#  Notes for Developers:
#   o Use command="some command" ; do_it to log all commands and
#     output. The do_it function does check the return status
#     of the command and calls exit_now if return != 0.
#   o The function do_it creates hidden files .pipestatus.* that store
#     the command return status. They are deleted once the status is read.
#   o The exit_now function also uses the last definition of 'command'
#     to generate an error message. Can be called outside of do_it, but
#     'command' must be set correctly to provide an accurate error message. 
#   o command/do_it has limitations.
#     - The 'tee' spawns a sub-shell. Any 'export' or 'cd' command
#       will *not* hold in the parent shell. To work around this,
#       use set_env_variable and prepend_env_path to alter environment
#       variables. Use the '||' for simple commands that do need
#       logging such as cd, e.g.  cd foo || (command='cd foo' ; exit_now)
#     - Chained commands,e.g. 'cd build;make install' will not work.
#     - Commands containing pipes;e.g. 'ls -l | grep imc'  will not work.
#   o Function order matters! build_log_header MUST come before any function
#     that 'tee's to the log file. This function will clobber any existing
#     log file.  
#   o Build directories are not removed to reduce build times on repeat
#     attempts. If the Makefile is still there, then 'make rebuild_cache'
#     regenerates the build system but does not remove built targets.
#       
#       
#   
#

# Define the location of the Hydra-TH distribution
export HYDRA=/scratch/lpritch/CASL/wec/hydra-th-20140109

# Build Hydra-TH TPLs deafult is true, set to false to skip the TPL build
BUILD_HYDRATH_TPLS=true

# List of TPLs to skip in the build, use '' is more than one tpl is listed
TPL_SKIP_LIST=

# Location of the CMake binary (www.cmake.org)
CMAKE_BINARY_DIR=/local/packages/cmake/2.8.8/bin

# Intel compiler variables
# Installation location (INTEL)
# architecture 32-bit (ia32) or 64-bit (intel64)
export INTEL=/ccs/opt/x86_64/intel-13.0
export INTEL_ARCH=intel64

# MPI installation, if this directory does not exist will build OpenMPI 
# distribution defined in the MPI_DISTRIBUTION_FILE 
export MPIROOT=/local/scratch/lpritch/CASL/wec/openmpi-install

# File use to build OpenMPI. Should be openmpi-x.y.z.tar.gz, default is the OpenMPI
# found in HYDRA
MPI_DISTRIBUTION_FILE=${HYDRA}/tpl/tarballs/openmpi-1.6.5.tar.gz

# Directory to build MPI
MPI_BUILD_DIR=${HYDRA}/build/intel/mpi-build

# Make threads (-j n)
MAKE_THREADS=4

# Test threads (-j n)
TEST_THREADS=8

# Build log file, need full path because there are cd commands
DATE_STAMP=`date +%y%m%d-%H%M`
#BUILD_LOG=`pwd`/build-hydra-${DATE_STAMP}.log
BUILD_LOG=`pwd`/build-hydra.log
START_TIME=`date +%s`

# Simple functions
build_log_header ()
{
  host=`hostname`
  os=`uname -m`
  user=`whoami`
  date_now=`date`
  echo "================================================================================" | tee ${BUILD_LOG}
  echo "Machine: $host" | tee -a ${BUILD_LOG}
  echo "OS:      $os" | tee -a ${BUILD_LOG}
  echo "Builder: ${user}" | tee -a ${BUILD_LOG}
  echo "Date:    $date_now" | tee -a ${BUILD_LOG}
  echo "" | tee -a ${BUILD_LOG}
  echo "Important variables:" | tee -a ${BUILD_LOG}
  echo "  HYDRA=${HYDRA}" | tee -a ${BUILD_LOG}
  echo "  INTEL=${INTEL}" | tee -a ${BUILD_LOG}
  echo "  MPIROOT=${MPIROOT}" | tee -a ${BUILD_LOG}
  echo "  CMAKE_BINARY_DIR=${CMAKE_BINARY_DIR}" | tee -a ${BUILD_LOG}
  echo "================================================================================" | tee -a ${BUILD_LOG}

}

build_log_echo ()
{
  echo "" | tee -a ${BUILD_LOG}
  echo "MESSAGE:$1" | tee -a ${BUILD_LOG}
  echo "" | tee -a ${BUILD_LOG}
}

elapsed_time () 
{
  now=`date +%s`
  elap_secs=`echo ${now}-${START_TIME} | bc`
  elap_hrs=`echo $elap_secs/3600 | bc`
  elap_mins=`echo \(${elap_secs}-3600*${elap_hrs}\)/60 | bc`
  remain=`echo \(${elap_secs}-3600*${elap_hrs}-${elap_mins}*60\) | bc`
  echo | tee -a ${BUILD_LOG}
  echo "ELAPSED TIME (h:m:s) ${elap_hrs}:${elap_mins}:${remain}" | tee -a ${BUILD_LOG}
  echo | tee -a ${BUILD_LOG}

}

build_log_footer ()
{
  echo "================================================================================" | tee -a ${BUILD_LOG}
  echo "Hydra-TH Build and Test Complete" | tee -a ${BUILD_LOG}
  echo "Binary installed in ${HYDRA}/install/intel/bin" | tee -a ${BUILD_LOG}
  echo "================================================================================" | tee -a ${BUILD_LOG}
}
 

exit_now ()
{
  echo "ERROR: '${command}' failed ($status)"
  elapsed_time
  exit 1
}

do_it ()
{
  echo  | tee -a ${BUILD_LOG}
  echo "----> $command" | tee -a ${BUILD_LOG}
  { $command 2>&1 ; echo $?>.pipestatus.$$ ; } | tee -a ${BUILD_LOG}
  status=`cat .pipestatus.$$`
  rm .pipestatus.$$
  if [[ ${status} -ne 0 ]] ; then
    exit_now
  fi
  echo  | tee -a ${BUILD_LOG}
  command=
}

dump_env ()
{
  command="printenv"
  do_it
}

dump_variable ()
{
  a=$1
  echo "" | tee -a ${BUILD_DIR}
  echo "----> $a=${!a}" | tee -a ${BUILD_LOG}
  echo "" | tee -a ${BUILD_DIR}
}

set_env_variable ()
{
  echo "----> export $1=$2" | tee -a ${BUILD_LOG}
  export $1=$2

}

prepend_env_path ()
{
  p=$1
  v=$2
  if [[ ! -z "$p" ]] ; then
    set_env_variable $p ${v}:${!p}
  else
    set_env_variable $p $v
  fi
}

make_directory () 
{
  if [[ ! -d "${1}" ]] ; then
    command="mkdir -p $1"
    do_it
  fi
}

change_directory ()
{
  save_dir=`pwd`
  echo  | tee -a ${BUILD_LOG}
  echo "---> cd $1" | tee -a ${BUILD_LOG} 
  cd $1 || (command="cd $1" ; exit_now ; )
  echo  | tee -a ${BUILD_LOG}
}

pop_directory ()
{
  if [[ ! -z "${save_dir}" ]] ; then
    build_log_echo "Return to ${save_dir}"
    change_directory ${save_dir}
  else
    build_log_echo "save_dir is undefined ... ignoring pop"
  fi
}
  

# Build script begins here
clear   

# --- Create the build log will clobber
build_log_header


# --- Initalizing the environment
build_log_echo "Initializing the environment"

# All environment variabels
dump_env

# Variables of interest
dump_variable PATH
dump_variable LD_LIBRARY_PATH
dump_variable LD_RUN_PATH

# Memory limits
command="ulimit -a"
do_it

# --- HYDRA verification
command="ls ${HYDRA}"
do_it

# --- CMake setup
prepend_env_path PATH ${CMAKE_BINARY_DIR}
command="which cmake"
do_it
command="cmake --version"
do_it

# --- Intel setup
build_log_echo "Intel compiler setup"

# Source the compiler scripts, can not use command/do_it to preserve changes
# I echo the command to the log so we can see the output correctly
intel_compiler_script=${INTEL}/bin/compilervars.sh
command="echo source ${intel_compiler_script} ${INTEL_ARCH}"
do_it
source ${intel_compiler_script} ${INTEL_ARCH}
if [[ "$?" -ne 0 ]] ; then
  command="source ${intel_compiler_script} ${INTEL_ARCH}" # So the error message is correct
  exit_now
fi

mkl_compiler_script=${INTEL}/mkl/bin/mklvars.sh
command="echo source ${mkl_compiler_script} ${INTEL_ARCH}"
do_it
source ${mkl_compiler_script} ${INTEL_ARCH}
if [[ "$?" -ne 0 ]] ; then
  command="source ${mkl_compiler_script} ${INTEL_ARCH}" # So the error message is correct
  exit_now
fi

# Dump out the variables we need set for the compilers
dump_variable PATH
dump_variable LD_LIBRARY_PATH
dump_variable LD_RUN_PATH
dump_variable MKLROOT

# Check the compilers
command="which icc"
do_it
command="icc -V"
do_it
command="which icpc"
do_it
command="icpc -V"
do_it
command="which ifort"
do_it
command="ifort -V"
do_it

# --- MPI setup
build_log_echo "MPI setup"

# Build MPI if MPIROOT is not found
if [[ ! -d "${MPIROOT}" ]] ; then
  make_directory ${MPI_BUILD_DIR}
  change_directory ${MPI_BUILD_DIR}
  mpi_dir=`echo ${MPI_DISTRIBUTION_FILE} | sed 's/.*\/\(.*\)\.tar\.gz$/\1/'`
  command="tar zxvf ${MPI_DISTRIBUTION_FILE}"
  do_it
  pop_directory
  change_directory ${MPI_BUILD_DIR}/${mpi_dir}
  command="./configure --prefix=${MPIROOT} --enable-shared --enable-static CC=icc CXX=icpc FC=ifort"
  do_it
  command="make -j ${MAKE_THREADS}"
  do_it
  command="make install"
  do_it
  pop_directory
fi

# Set environment variables 
prepend_env_path PATH ${MPIROOT}/bin
prepend_env_path LD_LIBRARY_PATH ${MPIROOT}/lib
prepend_env_path LD_RUN_PATH ${MPIROOT}/lib

# Check the wrappers
command='which mpicc'
do_it
command='mpicc --showme'
do_it
command='which mpic++'
do_it
command='mpic++ --showme'
do_it
command='which mpif90'
do_it
command='mpif90 --showme'
do_it

# --- Build Hydra-TH TPLs
build_log_echo "Build Hydra-TH TPLs"

# Directory definitions related to the TPL build
TPL_SOURCE_DIR=${HYDRA}/tpl
TPL_BUILD_DIR=${HYDRA}/tpl/build/intel
TPL_INSTALL_DIR=${HYDRA}/tpl/install/intel

# Define the TPL build location
make_directory ${TPL_BUILD_DIR}
change_directory ${TPL_BUILD_DIR}

# Configure the TPL build
if [[ -e Makefile ]] ; then
  command="make rebuild_cache"
else
  command="cmake ${TPL_SOURCE_DIR}"
fi
do_it

# Launch the build
command="make -j ${MAKE_THREADS}"
do_it

# Return the original directory
pop_directory

# --- Build Hydra-TH
build_log_echo "Build Hydra-TH binaries"

# Directory definitions related to the Hydra-TH build
HYDRA_SOURCE_DIR=${HYDRA}/code/src
HYDRA_BUILD_DIR=${HYDRA}/build/intel
HYDRA_INSTALL_DIR=${HYDRA}/install/intel

# Define the Hydra-TH build directory
make_directory ${HYDRA_BUILD_DIR}
change_directory ${HYDRA_BUILD_DIR}

# Configure Hydra-TH
HYDRA_CONF_CMD=" \
  cmake \
    -D CMAKE_Fortran_COMPILER:PATH=mpif90 \
    -D CMAKE_C_COMPILER:PATH=mpicc \
    -D CMAKE_CXX_COMPILER:PATH=mpic++ \
    -D HYDRATH:BOOL=ON \
    -D PLATFORM:STRING=wec-intel \
    -D MPI_INCLUDE_DIR:PATH=${MPIROOT}/include \
    -D MPI_LIB_PATH:PATH=${MPIROOT}/path \
    -D CMAKE_INSTALL_PREFIX:PATH=${HYDRA_INSTALL_DIR} \
    -D THIRD_PARTY_PREFIX:PATH=${TPL_INSTALL_DIR} \
    -D CMAKE_PREFIX_PATH:STRING=${HYDRA_INSTALL_DIR} \
    -D CMAKE_BUILD_TYPE:STRING=RELEASE \
    ${HYDRA_SOURCE_DIR}"
if [[ -e Makefile ]] ; then
  command="make rebuild_cache"
else
  command=${HYDRA_CONF_CMD}
fi
do_it

# Build Hydra-TH
command="make -j ${MAKE_THREADS}"
do_it

# Install Hydra-TH
command="make install"
do_it

# Install locations and binaries
HYDRA_INSTALL_DIR=${HYDRA}/install/intel
HYDRA_BINARY=${HYDRA_INSTALL_DIR}/bin/hydra-th
EXODIFF_BINARY=${HYDRA_INSTALL_DIR}/bin/exodiff
command="ls -l ${HYDRA_BINARY}"
do_it
command="ldd ${HYDRA_BINARY}"
do_it
command="${HYDRA_BINARY}"
do_it

# Return to the original directory
pop_directory

# --- Test The Hydra-Th Installation
build_log_echo "Running the tests"

# Define directories associated with Hydra-TH testing
HYDRA_TEST_DIR=${HYDRA}/test
HYDRA_REGRESSION_DIR=${HYDRA_TEST_DIR}/regression
HYDRA_RESULTS_DIR=${HYDRA}/test_results-${DATE_STAMP}

# Run the serial test suite
command="${HYDRA}/code/scripts/rtest.py -j ${TEST_THREADS} --hydra_exe=${HYDRA_BINARY} --exodiff_exe=${EXODIFF_BINARY} --test_dir=${HYDRA_REGRESSION_DIR} --dump_dir=${HYDRA_RESULTS_DIR}/serial --suite=${HYDRA_REGRESSION_DIR}/serial_regression_hydra-th.xml"
do_it

# Run the parallel test suite
command="${HYDRA}/code/scripts/rtest.py -j ${TEST_THREADS} --hydra_exe=${HYDRA_BINARY} --exodiff_exe=${EXODIFF_BINARY} --test_dir=${HYDRA_REGRESSION_DIR} --dump_dir=${HYDRA_RESULTS_DIR}/parallel --suite=${HYDRA_REGRESSION_DIR}/parallel_regression_hydra-th.xml"
do_it

# --- Finalize

# How long did this take
elapsed_time


# Write the footer to the log file
build_log_footer 


