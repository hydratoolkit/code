#!/bin/bash
#
# Westinghouse Run Hydra-TH Logger
#
#  Usage:
#    Using the same logging capabilities found in wec-hydra-th.sh
#    this script runs Hydra-TH in a consistent environment that
#    was used to build Hydra-TH.
#
#    ./wec-run-hydra-th.sh -N n -i mesh_file -c cntl [options]
#   
#  
#  Notes:
#    o See the Notes section in wec-hydra-th.sh on the limitations of
#      command/do_it.
#    o Environment variables HYDRA, HYDRATH_BINARY, INTEL and MPIROOT
#      must be set to correct and useable binaries and locations.
#    o Only required options, mesh and control files, are checked
#      by this script. Other options are handled by the Hydra-TH binary.
#    o Hydra-TH does NOT exit with non-zero in all error cases. Users
#      should inspect the output from the logger AND binary to verify
#      that the run ended successfully.
#  

# Define the location of the Hydra-TH distribution and installed binary
export HYDRA=/casl/hydra
export HYDRATH_BINARY=${HYDRA}/install/intel/bin/hydra-th

# Intel compiler variables
# Installation location (INTEL)
# architecture 32-bit (ia32) or 64-bit (intel64)
export INTEL=/mctprod/hydra/composerxe
export INTEL_ARCH=intel64

# MPI installation
export MPIROOT=/casl/openmpi

# Build log file, need full path because there are cd commands
DATE_STAMP=`date +%y%m%d-%H%M`
RUN_LOG=`pwd`/run-hydra-${DATE_STAMP}.log
START_TIME=`date +%s`

# Simple functions
run_log_header ()
{
  host=`hostname`
  os=`uname -m`
  user=`whoami`
  date_now=`date`
  echo "================================================================================" | tee ${RUN_LOG}
  echo "Machine: $host" | tee -a ${RUN_LOG}
  echo "OS:      $os" | tee -a ${RUN_LOG}
  echo "Builder: ${user}" | tee -a ${RUN_LOG}
  echo "Date:    $date_now" | tee -a ${RUN_LOG}
  echo "" | tee -a ${RUN_LOG}
  echo "Important variables:" | tee -a ${RUN_LOG}
  echo "  HYDRA=${HYDRA}" | tee -a ${RUN_LOG}
  echo "  HYDRATH_BINARY=${HYDRATH_BINARY}" | tee -a ${RUN_LOG}
  echo "  INTEL=${INTEL}" | tee -a ${RUN_LOG}
  echo "  MPIROOT=${MPIROOT}" | tee -a ${RUN_LOG}
  echo "================================================================================" | tee -a ${RUN_LOG}

}

elapsed_time () 
{
  now=`date +%s`
  elap_secs=`echo ${now}-${START_TIME} | bc`
  elap_hrs=`echo $elap_secs/3600 | bc`
  elap_mins=`echo \(${elap_secs}-3600*${elap_hrs}\)/60 | bc`
  remain=`echo \(${elap_secs}-3600*${elap_hrs}-${elap_mins}*60\) | bc`
  echo | tee -a ${RUN_LOG}
  echo "ELAPSED TIME (h:m:s) ${elap_hrs}:${elap_mins}:${remain}" | tee -a ${RUN_LOG}
  echo | tee -a ${RUN_LOG}

}

run_log_footer ()
{
  echo "================================================================================" | tee -a ${RUN_LOG}
  echo "Hydra-TH run complete" | tee -a ${RUN_LOG}
  echo "Binary installed in ${HYDRA}/install/intel/bin" | tee -a ${RUN_LOG}
  echo "================================================================================" | tee -a ${RUN_LOG}
}
  

exit_now ()
{
  echo "ERROR: '${command}' failed ($status)"
  elapsed_time
  exit 1
}

do_it ()
{
  echo  | tee -a ${RUN_LOG}
  echo "----> $command" | tee -a ${RUN_LOG}
  { $command 2>&1 ; echo $?>.pipestatus.$$ ; } | tee -a ${RUN_LOG}
  status=`cat .pipestatus.$$`
  rm .pipestatus.$$
  if [[ ${status} -ne 0 ]] ; then
    exit_now
  fi
  echo  | tee -a ${RUN_LOG}
  command=
}

dump_env ()
{
  command="printenv"
  do_it
}

dump_variable ()
{
  a=$1
  echo "" | tee -a ${BUILD_DIR}
  echo "----> $a=${!a}" | tee -a ${RUN_LOG}
  echo "" | tee -a ${BUILD_DIR}
}

set_env_variable ()
{
  echo "----> export $1=$2" | tee -a ${RUN_LOG}
  export $1=$2

}

prepend_env_path ()
{
  p=$1
  v=$2
  if [[ ! -z "$p" ]] ; then
    set_env_variable $p ${v}:${!p}
  else
    set_env_variable $p $v
  fi
}

make_directory () 
{
  if [[ ! -d "${1}" ]] ; then
    command="mkdir -p $1"
    do_it
  fi
}

print_usage ()
{
cat << EOF
usage: $0 -N n -i mesh_file -c cntl [hydra-th options]

Run hydra-th binary ${HYDRATH_BINARY}

OPTIONS:

  -N <n>          Number of processors to run (Default: 1)

  -i mesh_file    Mesh file (REQUIRED)
  -c cntl         Control file (REQUIRED)

  -o out          Output file (Default: out)
  -p plot         Plot file (Default: plot)
  -g glob         Glob file (Default: glob)
  -h hist         History file (Default: hist)
  -n conv         Convergence file (Default: conv)
  -d dump         Dump (restart) file  (Default: dump)
  -r rest         Restart file (Default: rest)
  -e exec         Execution control file (Default: exec)

See the Hydra-TH user manual for more information
EOF
}

########################################
# Run script begins here
clear   

# --- Create the build log will clobber
run_log_header

# --- Read and process command line argument

# Exit immediately if less than 2 are given
if [ $# -lt 2 ] ; then
  print_usage
  command=$0
  exit_now
fi

# Everything but N is a hydra-th option
hydrath_opts=
num_procs=
mesh_file=
cntl_file=
while getopts "N:i:c:o:p:g:h:n:d:r:e:" OPTION ; do
  case $OPTION in
    i|c|o|p|g|h|n|d|r|e)
      if [[ -z ${hydrath_opts} ]] ; then
        hydrath_opts="-${OPTION} $OPTARG"
      else
        hydrath_opts="$hydrath_opts -${OPTION} $OPTARG"
      fi
      case $OPTION in
	i)
	  mesh_file=${OPTARG}
	  ;;
	c)
	  cntl_file=${OPTARG}
	  ;;
	*)
	  ;; # Do nothing
      esac	  
      ;;

    N)
      num_procs=$OPTARG
      ;;
    ?)
      print_usage
      command=$0
      exit_now
      ;;
  esac
done

# Default to one processor if -N was not used
if [[ -z ${num_procs} ]] ; then
  num_procs=1
fi

# Exit if the mesh file is not defined
if [[ -z ${mesh_file} ]] ; then
  command="Undefined mesh file"
  print_usage
  exit_now
fi

# Exit if the mesh file does not exist
if [[ ! -e ${mesh_file} ]] ; then
  command="${mesh_file} does not exist"
  exit_now
fi

# Exit if the control file is not defined
if [[ -z ${cntl_file} ]] ; then
  command="Undefined control (cntl) file"
  print_usage
  exit_now
fi

# Exit if the control file does not exist
if [[ ! -e ${cntl_file} ]] ; then
  command="${cntl_file} does not exist"
  exit_now
fi

# Echo the hydra-th options to the log
command="echo hydrath_opts=$hydrath_opts"
do_it

# Echo the number of requested processors
command="echo Requested $num_procs processors"      
do_it

# --- Initalizing the environment

# All environment variables
dump_env

# Variables of interest
dump_variable PATH
dump_variable LD_LIBRARY_PATH
dump_variable LD_RUN_PATH

# Memory limits
command='ulimit -a'
do_it

# Check the TMP directory
command="df ${TMPDIR}"
do_it

# --- Memory configuration

# Unlimit the locked memory
# the subshell will not allow this change in the
# parent shell, but echo to the log to track
command='echo ulimit -l unlimited'
do_it
ulimit -l unlimited || (command='ulimit -l unlimited' ; exit_now ;)

# After the change check the limits
command='ulimit -a'
do_it

# --- Intel setup

# Source the compiler scripts, can not use command/do_it to preserve changes
# I echo the command to teh log so we can see the output correctly
intel_compiler_script=${INTEL}/bin/compilervars.sh
command="echo source ${intel_compiler_script} ${INTEL_ARCH}"
do_it
source ${intel_compiler_script} ${INTEL_ARCH}
if [[ "$?" -ne 0 ]] ; then
  command="source ${intel_compiler_script} ${INTEL_ARCH}" # So the error message is correct
  exit_now
fi

mkl_compiler_script=${INTEL}/mkl/bin/mklvars.sh
command="echo source ${mkl_compiler_script} ${INTEL_ARCH}"
do_it
source ${mkl_compiler_script} ${INTEL_ARCH}
if [[ "$?" -ne 0 ]] ; then
  command="source ${mkl_compiler_script} ${INTEL_ARCH}" # So the error message is correct
  exit_now
fi

# Dump out the variables we need set for the compilers
dump_variable PATH
dump_variable LD_LIBRARY_PATH
dump_variable LD_RUN_PATH
dump_variable MKLROOT

# Check the compilers
command="which icc"
do_it
command="icc -V"
do_it
command="which icpc"
do_it
command="icpc -V"
do_it
command="which ifort"
do_it
command="ifort -V"
do_it

# --- MPI setup

# Set environment variables 
prepend_env_path PATH ${MPIROOT}/bin
prepend_env_path LD_LIBRARY_PATH ${MPIROOT}/lib
prepend_env_path LD_RUN_PATH ${MPIROOT}/lib

# Check the MPI launch command mpirun
command='which mpirun'
do_it

# --- Hydra-TH pre-launch checks

# Check the build date
command="ls -l ${HYDRATH_BINARY}"
do_it

# Check the dynamic loader
command="ldd ${HYDRATH_BINARY}"
do_it

# --- Launch Hydra-TH

# Run
command="mpirun -n ${num_procs} ${HYDRATH_BINARY} ${hydrath_opts}"
do_it

# --- Finalize

# How long did this take
elapsed_time

# Write the footer to the log file
run_log_footer




