Nel = 1000;
t = 0.0;
omega = 1.0e+4;
dt = 1.0e-6;

printf("omega = %10.4e\n", omega);

for i=1:Nel+1
# printf("    %10.4e %10.4e\n", t, sin(pi*omega*t))
  printf("    %10.4e %10.4e\n", t, sin(3.14159*omega*t))
  t = t + dt;
end
