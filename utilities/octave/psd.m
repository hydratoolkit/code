data = load("hist_force_9_stationary");

N = length(data(:,2));
#N2 = N/2;
#N4 = N/4;
#N3 = 3*N/4;

# mean
xavg = sum(data(:,2))/N
#avg2 = sum(data(1:N2,2))/N2;
#avg3 = sum(data(1:N3,2))/N3;
yavg = sum(data(:,3))/N

# rms
xrms = 0.0;
yrms = 0.0;
for i = 1 : N
  xrms = xrms + (data(i,2)-xavg)*(data(i,2)-xavg);
  yrms = yrms + (data(i,3)-yavg)*(data(i,3)-yavg);
endfor
xrms = sqrt(xrms/N)
yrms = sqrt(yrms/N)

y = data(:,2);
T = data(N,1);

# FFT
f = fft(y);
nf = linspace(0.1,N,N);

# Welch's method
S = 20; # increase for more smoothing
M = round(N/S);
nw = linspace(0.1,N,2*M);
p = pwelch(y,2*M,[],[],[],'whole');
p = sqrt(p);

# Plot both FFT and Welch
nf = nf*2.0*pi/T;
f = abs(f);
nw = nw*2.0*pi/T;
loglog( nf,f,"-;FFT;", nw,p,"-;Welch;" );

# save FFT
m = zeros(N,2);
m(:,1) = nf';
m(:,2) = f';
mt = zeros(N/2,2);
mt(:,1) = m(1:N/2,1);
mt(:,2) = m(1:N/2,2);
save( "-ascii", "hist_force_9x_stationary.fft", "mt" );

# save Welch
m = zeros(2*M,2);
m(:,1) = nw';
m(:,2) = p';
mt = zeros(M,2);
mt(:,1) = m(1:M,1);
mt(:,2) = m(1:M,2);
save( "-ascii", "hist_force_9x_stationary.welch", "mt" );
