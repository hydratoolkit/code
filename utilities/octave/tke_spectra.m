data = load("hist_vel_2038709_stationary");

N = length(data(:,2))
x = data(:,2);
y = data(:,3);
z = data(:,4);
T = data(N,1);

# mean
xavg = sum(x)/N
yavg = sum(y)/N
zavg = sum(z)/N

# FFT
fx = fft((x-xavg));
fy = fft((y-yavg));
fz = fft((z-zavg));
f = 0.5*real(fx.*conj(fx) + fy.*conj(fy) + fz.*conj(fz));
nf = linspace(0.1,N,N);
loglog( 2.0*pi*nf,f,"-;FFT;");

# Welch's method
S = 20; # increase for more smoothing
M = round(N/S);
nw = linspace(0.1,N,2*M);
p = pwelch(f,2*M,[],[],[],'whole');
p = sqrt(p);

# save FFT
m = zeros(N,2);
m(:,1) = 2.0*pi*nf';
m(:,2) = f';
mt = zeros(N/2-1,2);
mt(:,1) = m(2:N/2,1);
mt(:,2) = m(2:N/2,2);
save( "-ascii", "hist_vel_2038709_stationary.fft", "mt" );

# save Welch
m = zeros(2*M,2);
m(:,1) = nw';
m(:,2) = p';
mt = zeros(M,2);
mt(:,1) = m(1:M,1);
mt(:,2) = m(1:M,2);
save( "-ascii", "hist_vel_2038709_stationary.welch", "mt" );
