#!/usr/bin/env python

_usage = '''Usage: %prog -n numPts

  Load Curve Generator:

'''

import sys
import os
from optparse import OptionParser as OptParser

try:
  import numpy
except:
  print 'NumPy not installed. Please install NumPy'
  raise


class TimestepData:
  def __init__(self,dt,t,data):
    self.dt=dt
    self.t=t
    self.dim=len(data)
    self.data=data

def read_file(infile,tstart):
  import re
  try:
    fh=open(infile)
  except IOError:
    print ('Failed to open %s') % (infile)
    raise
  except:
    raise
  comment=re.compile('^#')
  delims='[\s\t,]+'
  times=[]
  tp = None
  for line in fh:
    if comment.match(line):
      continue
    tmp = re.split(delims,line.rstrip().lstrip())
    t = float(tmp[0])
    data = []
    if t >= tstart:
      if tp is None:
        tp = t
      dt = t - tp
      tp = t
      for item in tmp[1:]:
        data.append(float(item))
      times.append(TimestepData(dt,t,data))
  return times

def compute_time_mean(times):
  try:
    means=numpy.zeros((len(times[0].data)))
  except IndexError:
    print 'No data to average'
    raise
  except:
    raise
  sumdt=0
  for t in times:
    idx=0
    sumdt=sumdt+t.dt
    while idx < t.dim:
      means[idx]+= t.data[idx]*t.dt
      idx=idx+1
  for i in range(0,idx):
    means[i]/=sumdt
  return means

def compute_time_rms(means,times):
  import math
  rms=numpy.zeros((len(times[0].data)))
  sumdt=0
  for t in times:
    i=0
    sumdt+=t.dt
    while i < t.dim:
      rms[i]+= (t.data[i]-means[i])*(t.data[i]-means[i])*t.dt
      i+=1
  for idx in range(0,i):
    rms[idx]/=sumdt
    rms[idx]=math.sqrt(rms[idx])
  return rms

def print_results(means,rms):
  print '{0:5} {1:13} {2:13}'.format('index','mean','rms')
  i=0
  max_i=len(means)
  while i < max_i:
    print '{0:5d} {1:13e} {2:13e}'.format(i,means[i],rms[i])
    i+=1

def main():
  '''Define the command line parser and process the command'''
  parser=OptParser(usage="%prog [options] hist_file",add_help_option=True)
  parser.add_option('--start',dest='tstart',action='store',type="float",
                    default=0.0,metavar='NUM')
  (options,args)=parser.parse_args()

  '''Read the history file'''
  try:
    times=read_file(args[0],options.tstart)
  except IndexError:
    print 'A time history file is required'
    parser.print_help()
    raise
  except:
    raise

  '''Compute timestep weighted means'''
  means=compute_time_mean(times)

  '''Compute timestep weighted rms'''
  rms=compute_time_rms(means,times)

  print_results(means,rms)


if __name__ == '__main__':
  main()

