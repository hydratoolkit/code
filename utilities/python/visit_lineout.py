#!/usr/bin/env python

'''
Utilities for extracting a lineout using Visit.

It is set up to be run as:

  visit -cli -s visit_lineout.py [other visit options]

There are comments in the main section below to run as a stand-alone script,
although this is not working currently on Mac OS.

Written by Rob Lowrie, CCS-2
'''

def getLineout(fileIn, var, p0, p1, n=500, axis=None, time=-1)
    '''
    Gets lineout data.  The data returned is a 2-D array as data[i][j], with
    i=0 corresponding to a spatial coordinate (set from the axis argument), i
    > 1 the depedent variable (set from the var argument) for each time index
    specified, and j the index along the lineout.

    fileIn : plot file
    var    : variable to extract
    p0     : starting coordinate of lineout
    p1     : ending coordinate of lineout
    n      : number of points on the lineout
    axis   : axis along which to plot.  If None, determine automatically 
             from p0,p1.  Otherwise, values may be 0: x-axis, 1: y-axis, 
             2: z-axis, 3: arclength
    time   : time index at which to generate data.  Special values are:
             None: all times, -1: final time
    '''
    # Determine the axis to plot along by inspecting the starting and ending
    # points of the lineout.  If the lineout is aligned with an axis, use that
    # axis, otherwise use arclength.
    if axis is None:
        for i in range(3):
            diff = abs(p1[i] - p0[i])
            if diff > 1.0e-10:
                if axis is None:
                    axis = i
                else:
                    axis = 3
        if axis is None:
            print 'ERROR: Starting and end points appear to be the same.'
            sys.exit(1)
    print 'Using axis =', axis

    # Make the plot
    OpenDatabase(fileIn)
    AddPlot("Pseudocolor", var)

    # Set the time indices to collect
    nTimes = TimeSliderGetNStates()
    if time is None:
        times = range(nTimes)
    elif time == -1:
        times = [nTimes - 1]
    else:
        times = [time]

    # Loop over times and collect the data
    data = [0] * (len(times) + 1) # return value
    first = True # if true, first time thru loop
    for i in range(len(times)):
        SetTimeSliderState(times[i])
        DrawPlots()

        # Define the lineout the first time thru the loop
        if first:
            if axis == 3:
                Lineout(p0, p1, n)
            else:
                DefineScalarExpression("xc", "coord(Mesh)[%d]" % axis)
                Lineout(p0, p1, ("default", "xc"), n)

        # Extract the lineout data
        SetActiveWindow(2)
        SetActivePlots(0)
        vals = GetPlotInformation()["Curve"] # arclength, var
        data[i+1] = vals[1::2] # get var

        # Coordindate axis is always the same, so extract only the first time
        if first:
            if axis == 3:
                data[0] = vals[0::2] # get arclength
            else:
                SetActivePlots(1)
                data[0] = GetPlotInformation()["Curve"][1::2] # get xc

        first = False
    return data

def writeLineout(data, fileOut='lineout.dat', maxcols=6):
    '''
    Outputs data from getLineout.  With maxcols=3 and 4 output times, the data
    output format is:

      x[0] y_time1[0] y_time2[0] y_time3[0]
      x[1] y_time1[1] y_time2[1] y_time3[1]
      x[1] y_time1[2] y_time2[2] y_time3[2]
      ....
      x[N] y_time1[N] y_time2[N] y_time3[N]
      
      x[0] y_time4[0]
      x[1] y_time4[1]
      x[2] y_time4[2]
      ....
      x[N] y_time4[N]
      ....

    Note the blank line between each data block.  This format may be plotted
    with grace as

      xmgrace -nxy fileOut [other xmgrace options]
    
    Arguments:

    data    : data from getLineout
    fileOut : output filename (over-written if exists)
    maxcols : maximum y-columns for each data block
    '''
    outf  = open(fileOut, 'w')
    n     = len(data[0])
    nData = len(data)

    it = 1
    while it < nData:
        if it > 1: outf.write('\n') # blank line between blocks of data
        cols = min(it + maxcols, nData)
        for i in range(n):
            outf.write('%12.5e' % data[0][i])
            for j in range(it, cols):
                outf.write(' %12.5e' % data[j][i])
            outf.write('\n')
        it = cols
    outf.close()

#### Main program ############################################################

if __name__ == '__main__':
    import sys

    # Stand-alone: Add the location of visit.so
    #sys.path.append('/Applications/VisIt.app/Contents/Resources/2.4.1/darwin-x86_64/lib')

    from visit import *

    # Stand-alone: Launch visit
    #LaunchNowin()

    # plot file name
    fileIn  = '../test/regression/FVM/CCIncNavierStokes/2D/vortex_shedding/baseline/c1_plot.exo'

    d = getLineout(fileIn, 'pressure', (-8,0,0), (25,0,0), axis=None, time=None)
    writeLineout(d)

    # exit if no windows are shown
    if ('-nowin' in sys.argv): sys.exit(0)
