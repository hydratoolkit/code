#!/usr/bin/env python

_usage = '''Usage: %prog [options]

  Hydra Plotting Utility

'''

import sys
import os
from optparse import OptionParser as OptParser

try:
    import matplotlib.pyplot as plt
except:
    print 'matplotlib is not installed, please install using pip.'
    raise

try:
    import numpy
except:
    print 'NumPy not installed. Please install NumPy using pip.'
    raise

class TimeSeriesData:
  def __init__(self,data):
    self.dt  = data[1]
    self.t   = data[2]
    self.div = data[3]
    self.ke  = data[4]

def readFile(infile):
    import re
    try:
        fh=open(infile)
    except IOError:
        print ('Failed to open %s') % (infile)
        raise
    except:
        raise
    comment=re.compile('^#')
    delims='[\s\t,]+'
    times=[]
    tp = None
    for line in fh:
        if comment.match(line):
            continue
        tmp = re.split(delims, line.rstrip().lstrip())
        t = float(tmp[0])
        data = []
        for item in tmp[0:5]:
            data.append(float(item))
        times.append(TimeSeriesData(data))
    return times

def main():
    '''Define the command line parser and process the command'''
    parser=OptParser(usage="%prog [options] glob", add_help_option=True)

#   parser.add_option('--start',dest='tstart',action='store',type="float",
#                     default=0.0,metavar='NUM')
    (options,args)=parser.parse_args()

    '''Read the glob time-history file'''
    try:
        times = readFile(args[0])
    except IndexError:
        print 'A time history file is required'
        parser.print_help()
        raise
    except:
        raise

    npts = len(times)

    print 'Number of points = ', npts

    x = numpy.zeros(npts)
    y = numpy.zeros(npts)

    i = 0
    for t in times:
        x[i] = t.t
        y[i] = t.ke
        i += 1

    plt.plot(x, y)
    plt.xlabel('Time [s]')
    plt.ylabel('Kinetic Energy')
    plt.grid(True)
    plt.show()

    i = 0
    for t in times:
        y[i] = t.div
        i += 1

    plt.plot(x, y)
    plt.xlabel('Time [s]')
    plt.ylabel('RMS divergence')
    plt.grid(True)
    plt.show()

if __name__ == '__main__':
  main()
