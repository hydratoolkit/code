(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     23528,        560]
NotebookOptionsPosition[     21914,        500]
NotebookOutlinePosition[     22273,        516]
CellTagsIndexPosition[     22230,        513]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["\<\
Finds the convergence rate for a sequence of three meshes.
Input:
  p1 : result on mesh 1
  p2 : result on mesh 2
  p3 : result on mesh 3 (works best if this is the finest mesh)
  sig2 = (mesh 2 spacing) / (mesh 1 spacing)
  sig3 = (mesh 3 spacing) / (mesh 1 spacing)
  
Output:
  pe = exact result
  r = convergence rate
  c = proportionality constant
  
 For N=1,2,3, solves:
   pN =  pe + c sigN^r
 with sig1 = 1\
\>", "Subsubtitle",
 CellChangeTimes->{{3.529947638009109*^9, 3.529947854118923*^9}, {
  3.529947936620264*^9, 3.529947954465111*^9}, {3.531678002022154*^9, 
  3.53167802989107*^9}}],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{
   RowBox[{"input", ":", " ", 
    RowBox[{"in", " ", "this", " ", "case"}]}], ",", " ", 
   RowBox[{"we", " ", "back", " ", "out", " ", "sig2"}], ",", " ", 
   RowBox[{
   "sig3", " ", "using", " ", "number", " ", "of", " ", "elements"}]}], " ", 
  "*)"}]], "Input",
 CellChangeTimes->{{3.529947978055793*^9, 3.529947982766445*^9}, {
  3.5316780424133587`*^9, 3.531678060716338*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"mesh1", "=", "1049228"}], ";", 
  RowBox[{"mesh2", "=", "2663920"}], ";", 
  RowBox[{"mesh3", "=", "5832718"}], ";"}]], "Input",
 CellChangeTimes->{{3.530894111198764*^9, 3.530894185063365*^9}, 
   3.530894675181343*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"mesh1", "=", "2663920"}], ";", 
  RowBox[{"mesh2", "=", "5832718"}], ";", " ", 
  RowBox[{"mesh3", " ", "=", " ", "12522644"}], ";"}]], "Input",
 CellChangeTimes->{{3.532797379909362*^9, 3.532797402589758*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"p1", "=", "0.7665"}], ";", " ", 
  RowBox[{"p2", "=", "0.7720"}], ";", 
  RowBox[{"p3", "=", "0.7748"}], ";"}]], "Input",
 CellChangeTimes->{{3.532797655381618*^9, 3.532797699384165*^9}, {
  3.532797733850149*^9, 3.532797734185544*^9}, {3.532798024486168*^9, 
  3.5327980536533413`*^9}, {3.532800018974773*^9, 3.532800028941221*^9}, {
  3.532800190244645*^9, 3.5328002195485687`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"sig2", "=", 
   RowBox[{
    RowBox[{"N", "[", 
     RowBox[{"mesh1", "/", "mesh2"}], "]"}], "^", 
    RowBox[{"(", 
     RowBox[{"1", "/", "3"}], ")"}]}]}], ";", 
  RowBox[{"sig3", "=", 
   RowBox[{
    RowBox[{"N", "[", 
     RowBox[{"mesh1", "/", "mesh3"}], "]"}], "^", 
    RowBox[{"(", 
     RowBox[{"1", "/", "3"}], ")"}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.530888051507461*^9, 3.530888086339093*^9}, 
   3.530888238838743*^9, {3.5308897100597897`*^9, 3.530889740035122*^9}, {
   3.530889881238119*^9, 3.5308898855971823`*^9}, {3.530894189959688*^9, 
   3.530894226189877*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"l3", "=", 
  RowBox[{"Log", "[", "sig3", "]"}]}]], "Input",
 CellChangeTimes->{{3.529950443592423*^9, 3.529950451536417*^9}, 
   3.532799291738954*^9}],

Cell[BoxData[
 RowBox[{"-", "0.5159132677625246`"}]], "Output",
 CellChangeTimes->{
  3.529950452878808*^9, 3.529950651551762*^9, 3.530017404431115*^9, 
   3.530887657659244*^9, 3.530887854826757*^9, 3.530888088886343*^9, {
   3.5308882657829103`*^9, 3.530888316182486*^9}, 3.5308883936339273`*^9, 
   3.53088851566*^9, {3.530888714381195*^9, 3.53088872042762*^9}, {
   3.530888759686222*^9, 3.5308887907139807`*^9}, 3.530889211842535*^9, 
   3.530889269388331*^9, {3.530889302604396*^9, 3.530889326014759*^9}, 
   3.530889748417729*^9, 3.5308898526320753`*^9, 3.530889891744857*^9, {
   3.5308942290610037`*^9, 3.5308942375702963`*^9}, 3.530894689780261*^9, 
   3.530894749998769*^9, 3.532797411442824*^9, 3.532797629554326*^9, 
   3.5327977039972887`*^9, 3.532797736858561*^9, 3.532798057368742*^9, 
   3.5327992927650433`*^9, 3.532799443137704*^9, 3.532800207325664*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"l2", "=", 
  RowBox[{"Log", "[", "sig2", "]"}]}]], "Input",
 CellChangeTimes->{{3.52995045503165*^9, 3.52995046245354*^9}, 
   3.532799296170755*^9}],

Cell[BoxData[
 RowBox[{"-", "0.2612281262619538`"}]], "Output",
 CellChangeTimes->{
  3.529950462901972*^9, 3.529950652687169*^9, 3.530017405331709*^9, 
   3.530887658856748*^9, 3.530887855928582*^9, 3.53088809010401*^9, {
   3.530888268504591*^9, 3.530888317116705*^9}, 3.530888394485841*^9, 
   3.530888518746221*^9, 3.53088872141092*^9, {3.530888759746179*^9, 
   3.530888790773119*^9}, 3.53088921189509*^9, 3.530889269447587*^9, {
   3.5308893026578407`*^9, 3.530889326055975*^9}, 3.530889748451914*^9, 
   3.5308898526702347`*^9, 3.53088989178655*^9, {3.530894231015077*^9, 
   3.530894237633527*^9}, 3.530894342883663*^9, 3.530894690748139*^9, 
   3.530894750766295*^9, 3.532797412324046*^9, 3.532797630624058*^9, 
   3.532797704967145*^9, 3.5327977376441727`*^9, 3.532798058236994*^9, 
   3.5327992968946*^9, 3.532799444256669*^9, 3.532800208042532*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{
  "this", " ", "equation", " ", "comes", " ", "from", " ", "reducing", " ", 
   "the", " ", "3", " ", "equations", " ", "to", " ", "a", " ", "single", " ",
    "nonlinear", " ", "equation", " ", "in", " ", "the", " ", "unknown", " ", 
   "pe"}], " ", "*)"}]], "Input",
 CellChangeTimes->{{3.530025972384858*^9, 3.530026000931251*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"eq", " ", "=", " ", 
  RowBox[{
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"p2", "-", "pe"}], ")"}], "/", 
      RowBox[{"(", 
       RowBox[{"p1", "-", "pe"}], ")"}]}], ")"}], "^", "l3"}], " ", "-", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"p3", "-", "pe"}], ")"}], "/", 
      RowBox[{"(", 
       RowBox[{"p1", "-", "pe"}], ")"}]}], ")"}], "^", "l2"}]}]}]], "Input",
 CellChangeTimes->{{3.52994686954809*^9, 3.529946877491343*^9}, {
  3.5299473627821503`*^9, 3.5299473638625813`*^9}, {3.529949210110725*^9, 
  3.529949300520707*^9}, {3.529950424661522*^9, 3.5299504298166637`*^9}, {
  3.529950466023837*^9, 3.5299505268054047`*^9}, {3.530894538315868*^9, 
  3.530894556873226*^9}, {3.5327972971235857`*^9, 3.532797325739643*^9}, {
  3.532797573637535*^9, 3.5327975973493853`*^9}, {3.532799304467959*^9, 
  3.532799360885488*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", 
   SuperscriptBox[
    RowBox[{"(", 
     FractionBox[
      RowBox[{"0.772`", "\[VeryThinSpace]", "-", "pe"}], 
      RowBox[{"0.7665`", "\[VeryThinSpace]", "-", "pe"}]], ")"}], 
    "0.5159132677625246`"]], "-", 
  FractionBox["1", 
   SuperscriptBox[
    RowBox[{"(", 
     FractionBox[
      RowBox[{"0.7748`", "\[VeryThinSpace]", "-", "pe"}], 
      RowBox[{"0.7665`", "\[VeryThinSpace]", "-", "pe"}]], ")"}], 
    "0.2612281262619538`"]]}]], "Output",
 CellChangeTimes->{
  3.529946890241357*^9, 3.529947293798233*^9, 3.529947365021047*^9, 
   3.529947428541445*^9, 3.529948516231338*^9, 3.5299493026080713`*^9, 
   3.529949861153626*^9, 3.5299499747231913`*^9, 3.529950527908017*^9, 
   3.529950653489012*^9, 3.5300174064188957`*^9, 3.53088766391923*^9, 
   3.530887858098928*^9, 3.5308880922269363`*^9, {3.53088827072659*^9, 
   3.530888318888391*^9}, 3.5308883959066963`*^9, 3.53088852013634*^9, {
   3.530888732450643*^9, 3.5308887908661537`*^9}, 3.530889211975102*^9, 
   3.530889269507169*^9, {3.530889302755468*^9, 3.530889326115961*^9}, 
   3.530889748530748*^9, 3.530889852751812*^9, 3.5308898918621283`*^9, 
   3.53089423771168*^9, 3.530894344788527*^9, 3.530894557670912*^9, 
   3.530894692385825*^9, 3.530894752804083*^9, 3.5327974140505466`*^9, 
   3.532797598447021*^9, {3.532797710915085*^9, 3.532797739648872*^9}, 
   3.53279805987479*^9, 3.532799369904635*^9, 3.532799449865365*^9, {
   3.532800209481697*^9, 3.532800226654632*^9}}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{
  "may", " ", "have", " ", "to", " ", "tinker", " ", "with", " ", "the", " ", 
   "initial", " ", "guess", " ", "here", " ", "to", " ", "get", " ", "a", " ",
    "good", " ", "root"}], " ", "*)"}]], "Input",
 CellChangeTimes->{{3.5299478830677643`*^9, 3.529947918939466*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"sol", "=", 
  RowBox[{"FindRoot", "[", 
   RowBox[{
    RowBox[{"{", "eq", "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"pe", ",", "0.775"}], "}"}]}], "]"}], " "}]], "Input",
 CellChangeTimes->{{3.529947016602887*^9, 3.5299470295414267`*^9}, {
  3.529947064644535*^9, 3.529947109856831*^9}, {3.529947209639304*^9, 
  3.529947264779457*^9}, {3.5299473431182003`*^9, 3.529947343182619*^9}, {
  3.529947871421302*^9, 3.529947878051634*^9}, {3.5299486218542147`*^9, 
  3.5299486241007032`*^9}, {3.529949331008996*^9, 3.5299493471316233`*^9}, {
  3.52994994909823*^9, 3.529949979788251*^9}, {3.529950673409532*^9, 
  3.529950674905346*^9}, {3.530017838675891*^9, 3.530017839634469*^9}, {
  3.53088842632934*^9, 3.53088842815279*^9}, {3.530894349199045*^9, 
  3.530894350366713*^9}, {3.530894605179023*^9, 3.530894605969084*^9}, {
  3.5327977158018417`*^9, 3.532797744218163*^9}, {3.532799802207209*^9, 
  3.532799818853333*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"pe", "\[Rule]", "0.7779190363748546`"}], "}"}]], "Output",
 CellChangeTimes->{
  3.5299498660064087`*^9, 3.529949952633671*^9, 3.529949983460747*^9, 
   3.5299505339258223`*^9, {3.5299506550682373`*^9, 3.529950677645948*^9}, 
   3.530017415958085*^9, 3.53001784027594*^9, 3.530887666893044*^9, 
   3.530887861272791*^9, 3.53088809429568*^9, {3.530888272775779*^9, 
   3.5308883202478123`*^9}, 3.530888397596693*^9, 3.530888428797583*^9, 
   3.530888523940238*^9, {3.5308887341547127`*^9, 3.530888790932062*^9}, 
   3.530889212028659*^9, 3.530889269572138*^9, {3.530889302836919*^9, 
   3.5308893261855717`*^9}, 3.530889748597094*^9, 3.530889852833748*^9, 
   3.5308898919443207`*^9, 3.5308942377782907`*^9, 3.5308943508830357`*^9, 
   3.530894559498365*^9, 3.5308946078700237`*^9, 3.5308946937431383`*^9, 
   3.530894754240056*^9, 3.5327974162777452`*^9, 3.5327976008108053`*^9, 
   3.532797745555621*^9, 3.532798061541806*^9, 3.532799377247312*^9, 
   3.5327994517579927`*^9, {3.532799805677331*^9, 3.5327998199778976`*^9}, {
   3.5328002109017878`*^9, 3.532800228308881*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"eq", "/.", "sol"}]], "Input",
 CellChangeTimes->{{3.532799082297271*^9, 3.532799085034458*^9}}],

Cell[BoxData["8.881784197001252`*^-16"], "Output",
 CellChangeTimes->{3.5327990856848993`*^9, 3.532799380265873*^9, 
  3.5327994529716883`*^9, 3.5327998224169893`*^9, 3.5328002298068943`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{"eq", ",", 
   RowBox[{"{", 
    RowBox[{"pe", ",", "0", ",", 
     RowBox[{"2", " ", "p3"}]}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.52994854558389*^9, 3.5299485512537727`*^9}, {
   3.529949431345314*^9, 3.529949464503014*^9}, {3.5299496925352488`*^9, 
   3.529949695957118*^9}, {3.529949904831588*^9, 3.5299499079839897`*^9}, {
   3.530017423573667*^9, 3.530017442766509*^9}, {3.530017793318866*^9, 
   3.5300177949115553`*^9}, {3.530887881110697*^9, 3.530887912205909*^9}, 
   3.5308883400228453`*^9, {3.530889233160424*^9, 3.530889238984647*^9}, {
   3.530894583498529*^9, 3.5308945940496817`*^9}, {3.532797935994775*^9, 
   3.5327979395616207`*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {Hue[0.67, 0.6, 0.6], LineBox[CompressedData["
1:eJwV1Xk81OsewPFfC3XIUmmjEoWoxlBHk9RjRGTJcnAnTbYZY0S2LEXWQbLc
tNEisraII26LyHcSkjWOJUvI1o32i4aT437743k9r/fr832+/z5Krj42bgsp
ikrC8+tm9k+89pl13k/pi64tTDWHLcN0rqQqIRQr/0nidXMIzPQLealiSShq
gcnqTHO4OXchNk/FiVDbQ8s175jDg+3KiZdVfAjV17E04Kk57DUcl0pSiSBU
ima3wYA5UFaHW86qnCeUbIDJP+oWMGxHFceqZBJKOH9vVbUFDD0N43nrFhPq
1pUdMuqWMBI81G60/gmhHFSHZ0ytYONkvkF8JxCKfzKxWdoa+MSRs3d9NaHO
9CZtmbGG5BT2QJpYHaGKFBhlEzbwhS/g1Rs0EkrXb2zfclsIli3UWafzmlBD
Eb3a9nbA1l7RNa/aRqiM0uIH9+1hIE0yrJXZTqhBu0E1WxaYvPgHOhidhEr4
fMJSwQEi+l9VTaq8IRT38XIZRTbMHp0Yt6Khp/3NwrayIah72cpCHfQys55v
WmzwarfkuBmjdUdkPxuywaG+Y1EnH11xO3WnJxt2PXpn+LgAzeipEz1hw3jy
TO1prW5CjVQ03zhyDGz3qTf8o9eD8/1nRosd4X1W5bMGQ7TqytvO5Y4QIm5b
fNUc7SX2c6jGEW61hF/RZqMnbx6Y63GEjy7tTrxQtA1d/pyYEwjiIiabytDp
/omL2U5Q0tKx4aZOL6FsK6eclziDjGu07156H6GSbruk1zpD8RqaTDED/bCk
lNfkDDZN3YVbmOiYsTiddme4zNCakLJBO2U3jL1zBnmZQbfBk2hafVLCnDOo
Vuw7GvsYzWkUG/3dBfRWzxi17H9LKEcGta7ABTzqfRQ4Fv2EWtSXwbzrCvUD
8irFdmjpq6FRD1xBY6qGNncMbbQ3v67MFT4qrjdI80ZrqCw7Ue8K3oF1/Ibz
v9530WQ+ukLAJqVHO9vQIV/vP6dzIDyozWrxvwYIpfXUXkPIgf7EMAdrJzTz
sxr/FQdI1lZuhjvaPyq7qI0DVGN4MOMU+kNutd0oBwRK2zK8rqH1pddKS3Ih
vlEw0d6LzpLbrM3iwqEL8i9s6IOEqvr6e/4MF+JkNAq1dqHPNJ51XOQGVcl7
0mQZaD/jAUUpN9BNYHk2E7RFTOuzTW6gIUhdaWqJNlBV55u4gUTgCi7TGz1r
rhJ9zQ0aWRJi9Pvoc1Pp2w/wYGnXui/Sxejda/6reJgHhnbq3Z9K0SLDuQ1H
eFBhbVJYUI52q63R9eFBoWmcnVoDWoOXKrrBg3/rLczfOI7mbuu59IMHVptm
D0ptfUeoYFlGzWN3kHspt0R/O9ooci2zxh26Tmi+9Kf/6uO2jW3u4FjONX7D
QAfKmS3/7A6e9i3GOYfQ4W8UWCp8iEvKNdnjifZ6mvotlQ/losOmbkXo+Ycy
W856QHimh8TVEvTxv2xepHqAwcGY+vpH6NjNlYH5HlB3qcyUDmjmx+tytR7Q
Sdti9ncL2t3Gab/4cfjKnTG78A3dq9rqmnAcVFtzLJ7tGiJU3oH433I8IdBT
5H+HgdZvZm36jydUi1lcvaSHFgntzGs8wVV3esjDEF0w6T/y3hMyc0xOrbZF
u4xlr6N5wdqgT9k+J9E3DbwrKr1AQmG3SKkEXRlCFL+fgCMPEzdIPUIL25Xp
4t5wx3LQQFSGXikjOirvDQdjziU3P0dP7LcRO+ANgk+9yqGt6IDqvtwr3vAT
Ii3av6J5IspU3we+cOuz4zSHSaST5HF/e1+oVdPIqNk5TKi3vTvcOL6QPn7u
2mIG9kO7Y7x8fcHE91BKtD72jOeFZQm+kB1aFxZujea5hf4h9AW7i7WsoJNo
7ZUupZp+UA5V0rzH6NxW9rNN/nAxWlkirxz3zZdJ/kn3B75RtNgIoDc2vqnQ
9we5BuZPlzr0a6Foh4s/eHcKPxzrRpcse+qd4w9Knyqr7f5GV/9kPNhxEs7K
l4cYkRFCdTiolx4NAL3k6wW+B9D6e/r8fALg+/zpvhvG6Bc60/aCAHAcZez/
bjlCIml3lVMLAkCn+NGCTGfsSkqrpuYCYMyoJF4UhZ5bopt1LxDSn1wo2xyH
8+Iyv80LA8Fmm9/44QT0SLZqYlcgVMrSzfMu4vzdohS6WBCk9hbK2ORgf6Uc
JnQNgoN+d1MLqtFX1y2KVAuGn8PxLzvrcL7Iol2SGQwl9nzRgib0MUvaa4dg
2Kin5sDqwHmD//mKnQ+GafG8DWJj2A2Kr+2eDYb89Fu5jktGCdWlkODXdwrE
69JKlpuOksjYpi9NnSFgQntgMmWB/Ye2Zdt0CCRcru9/Y43+cOPQ5jWhIOUy
J3HrCNqqlPMnKxTkZl05NA98LzOTc34gFJS30+TM4rGnd/SFz50B7kXje7Qk
9O3ZoB7lMMgXOeuvSPm1Xzo71SQM1GsueXWnoQelL+hcCQO640y1+210p+Ff
ZGc4kJTqwJha3F9Q2jMhiIDI6beS/Hp0HrNmRXEEVLF/ZJk1o9XZLx/2RcBB
dY3mFZ2jROhetLVCIRIOV51XzRrF/snpw5XTkcCedOh6tniMCDXVJQsNoiDD
IeBE1tIx/B9ZWhrHomBQmLwodtkY0R9Nfi8KjgJOslDTXG6MRKZYTaUURcFx
VdX4ns3Yn0ySLsVoOMX6xvhhgH5N1ujJCYD5flUezRj3SUkcZe0WgESQ7nKe
GfavuaxpBwGkXxJ8aP8D9w359FflCGD14hfvCWeMeLFnBO9oMfB/IEk9uw==

     "]], LineBox[CompressedData["
1:eJwV1Xk01FscAPBJyyu0KU+kCVFSMXbq6Wt7NfiRpSyJN0xmTAZDmpjEoBKh
tEgpPClrQ6R6hkgiLVJR2kRZwqAGEaX3nT/uuedzvveec8/3e+/3qvoGO/tJ
kUikRByS2eMJL2G8agDk5tT8yvoWB525Ui2NTwaAXz1hvJ8eB5qjGnKlLegq
bQW2ZRwEW9jsTG9Fb8p6/lI1DmY+nGz3f49213Hu64oFZYVVXQtE6I2CRz2M
WHBPMBbbyQwCP/vuZE5sDGS/3m1gsHgQSAM2T68wY6BPI4qrvAzjO8RvvhMx
wK2rnxIpoc+9e5O8IgbOTjvNTtHC9S9YU+7lfHgWyF7eYoP2uf4xVRQNmd0X
N8o6oJU8ej8Jo4Ht2WRNdUYz/n7YcCIapG3WHqjdje4Z7/9rUzRYq3e2lQag
qVnBmdwoqHzrcj41Gc1MGTRXPQwJTrElT1PRcRlzCycjwf1haeOCNPSPwxvi
WiJhvGLhZEwmmq50MComEiipje6hJei0ZA3Wl0NwdfsWJZfn6IlQSvFDHoTd
ZemdbJPs32PtfI0Hlobpto/foF3YlTuO8KBTbZxn9QltZ1ZgZ8EDpRnBO/1R
9BGzSfWaCDh1U+3ycnkRkOSNb3s2hcMhVWmVV+5o1xGPV0sOghb1TuDMHvQ9
ik7TBBfagxjCtT7o69nLxB1cMKiqcz3IQjcJd32+zoVh10PJK3joGcNgXQcu
0JJEU7sz0K+Fm8TnD4D1RHPbxw/ovsJaPSIMxKsOr5n/CX2W49NoEgbZ1htC
KL3on3Xf+Rph8Cs1XjZmGO1F8vL5vR9ubTS3UvuNLto+8/jmfljve6N0r8oQ
1rvk4JjGflj09MyJfl/0xMpig9WhQO07tPURA73De6x2SSjEztr7rXAf+riN
zl6pUPhuaODGDkU7rckd7gmBj5mtaiMxaJXZtQsFIVDKka8cy0ZrhfOpliHg
tPz8l98d6KXH9dgRHEjSjs7o/ISuaZaNCORAA5XpcK8X3Z674bwPBzYfNr4Z
O4x+aZczz5YDar2vo+fOoMlFEYKVHBDfXqEgs2oY10demHckGM56Xvz7T090
1K/ntfpB4JqZ3Gz7D7pbpXFyVRAodPHdoulobfUFjvOD4CKDyfoSgBZdZ3p+
CISsEIPkykg0hU7vOB4IhfHNrd6Z6LQSO+8eNtSUS+3N60JbH6soKQsA/vdx
0bse9KLXJnuyA8DStP/AkgF0Sn6fSkoA1N99Fh8hRhcWLRCzAuBR06Uiu9kj
QErfsl2gFgCtH41GR9TRmlsDmen7YEAmINbEH32EPO+/MywwtkznaLDRtvUf
VI6y4Ej4A285Dtrxh+MFLgvIPau3iA6i95hfEHqwwOVu62hWPDqJXlCnwoIq
zla/P/LQ+8Kc7t/wh5S2pdRXPZJ4bH1HBxPeyYDR/X507vwH614wQdOSrV46
hM5ZQo16wIT7ggZS4jj61FC0SzETft6vfp8p9RXv97/+l3hMoFwOPecsh7Z9
tjlNkQkXdryfV6mLrvy1/gqNAaV1ZenX9NGZ/g6CXQxoMEzQOmOI9u1fW2/L
APFKIwe2qSR+s2KxIQOILyfPkS3QoXUZ6tIM+M23Uo9zRL/XCzO55Qd+ZYUW
RDD60gWXq/J+oCsfEdlRjO7mCp8M0yFRedePMQG6PoXr1UOH7jW6XJkbaPef
ShPv6JCu1x9sUoFWThqybqJjHj3op6vRjARvm1w6tCSa2m5rRieu/TzgSQcO
aUpBMIImn4k0f+4LpYO88sN634DUnJw9/twHWkSjvhwDNPlK6+AjH/g6xJaj
G6FF9HN9932A8tU7hLoZXVWdM1LhAyVjljrLLNFTPcYWF31A8Eu6KN8Jfdrr
QTfdB4oXZ+S85KA/Ldgwe5oGBfrC1PUl6BfrTjONaSDbZLpx2w106+tReV0a
cLzvNPiWoxu+QpMWDYwTKqYzbqMHovhbyTR40FHit6gWXfZ5Dn0uDboSck1H
W9Cd8SfIBf+AYmdyV5UYTYv1yhJ5Q0ISTdfRSIz95M7QvSQv+OxqHR53Ez0p
HBilegJlFTnYjDKK/cVa8R7NA/zG9qwLvYXWXD00kOYGy8zeHivXHwOS7Pq3
W9RcoSCtKi9YiG6Jb5eu2Qn6orPhZebjQFJ3pqmecAGj8qKvs6rRc+ZeC0p0
BtUT67SeWH3HfB2YZXvKCY62dklfuYdW5j0NrnaED4qnM0hmE/j+whuXz3IE
ZU7eUHsdmuol0Et2gDbj6d5qmMT+tFg3ycEeYptvnx20kJhsa2JjDxS/MEtF
azSpzLnXyh4STw9dDqNKTNGmmtrD1qGPOzc4od0Nvqmp20N+Tn1dui9axaxG
f4qAQwtTMvcfRZ8aownzCNDMtSX+jUe3GAddySHg1eY/ppoT0JrltsmXCaD4
83dppUgcHsg4Q0B3XahsV5rEbNCOJsAh3I3nkI9Ov22+zI2A6UXLNSMLJed1
f6bgTED+1Za2gmJ0uwKZbE/A7Jc2unNvoB0tJ3WsCLiz6a8vwv8mJf/lUX8d
AvzqJ8/1C9H8+GmeFgFynhVWCnfRFHOtkxoEsI9rZ4XWTUrqL6haSYDi6kEi
ux69xE7q1Z8ENFTkTT1tkMS3yXxbSkAYsTd/+iF6/vmHCxcSoPpZxXX9Y3Sp
m+nG+QT8D9ROATc=
     "]], 
    LineBox[{{0.7776178033068841, -0.009855043258694539}, {
     0.7777460321032639, -0.004944955658611638}, {0.7782171600081289, 
     0.006997176704522934}, {0.7783762016089875, 0.009738206797807823}}]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesOrigin->{0, 0},
  PlotRange->{{0, 1.5496}, {-0.009855043258694539, 0.009738206797807823}},
  PlotRangeClipping->True,
  PlotRangePadding->{
    Scaled[0.02], 
    Scaled[0.02]}]], "Output",
 CellChangeTimes->{{3.529948548241506*^9, 3.529948551751226*^9}, {
   3.5299494535423737`*^9, 3.5299494657072563`*^9}, 3.529949698756856*^9, 
   3.529949908924073*^9, 3.529949986913602*^9, 3.529950542611451*^9, 
   3.529950657515348*^9, 3.5300174436902246`*^9, 3.530017796104578*^9, 
   3.530887669783718*^9, {3.5308878915527277`*^9, 3.530887912606847*^9}, {
   3.5308882767389917`*^9, 3.530888340880988*^9}, 3.5308884003142548`*^9, 
   3.530888528097536*^9, {3.5308887451080112`*^9, 3.530888791037485*^9}, {
   3.53088921211798*^9, 3.530889239613195*^9}, 3.530889269647743*^9, {
   3.530889302943151*^9, 3.530889326274476*^9}, 3.530889748655737*^9, 
   3.5308898528762074`*^9, 3.530889892003208*^9, 3.5308942378366013`*^9, 
   3.530894354121216*^9, {3.530894563115106*^9, 3.530894594594322*^9}, {
   3.530894695943486*^9, 3.530894710555194*^9}, 3.530894762188958*^9, 
   3.532797427922497*^9, 3.5327979400743093`*^9, 3.532798090926296*^9, 
   3.532799381692202*^9, 3.532799470964225*^9, 3.532799824087914*^9, 
   3.532800231099243*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"c", "=", 
  RowBox[{
   RowBox[{"p1", "-", "pe"}], "/.", "sol"}]}]], "Input",
 CellChangeTimes->{{3.5300178195465117`*^9, 3.530017843988194*^9}}],

Cell[BoxData[
 RowBox[{"-", "0.01141903637485464`"}]], "Output",
 CellChangeTimes->{
  3.5300178452896137`*^9, 3.530887758134098*^9, 3.5308879270004177`*^9, {
   3.530888760020813*^9, 3.530888791081437*^9}, 3.5308892121590843`*^9, 
   3.5308892696880074`*^9, {3.530889302987893*^9, 3.530889326316019*^9}, 
   3.530889748684432*^9, 3.530889852922724*^9, 3.530889892053488*^9, 
   3.530894237887171*^9, 3.53089462172966*^9, 3.530894764994809*^9, 
   3.5327977607695312`*^9, 3.532798098536791*^9, 3.532799384781992*^9, 
   3.532799472506812*^9, 3.5327998259428864`*^9, 3.5328002336371813`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"r", "=", 
  RowBox[{
   RowBox[{"Log", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{"p2", "-", "pe"}], ")"}], "/", "c"}], "/.", "sol"}], "]"}], 
   "/", 
   RowBox[{"Log", "[", "sig2", "]"}], " "}]}]], "Input",
 CellChangeTimes->{{3.530017852671817*^9, 3.530017949099922*^9}}],

Cell[BoxData["2.515457154082316`"], "Output",
 CellChangeTimes->{
  3.5300179512405157`*^9, 3.530887759356102*^9, 3.530887928202675*^9, {
   3.530888760061775*^9, 3.53088879114115*^9}, 3.5308892121992197`*^9, 
   3.530889269729618*^9, {3.530889303046636*^9, 3.5308893263579397`*^9}, 
   3.53088974874006*^9, 3.530889852978622*^9, 3.530889892099481*^9, 
   3.5308942379334497`*^9, 3.5308946235683117`*^9, 3.530894766163252*^9, 
   3.5327977699882402`*^9, 3.532798099735298*^9, 3.5327993863968287`*^9, 
   3.532799474248201*^9, 3.532799827663162*^9, 3.532800234689782*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{"check", " ", "errors"}], " ", "*)"}]], "Input",
 CellChangeTimes->{{3.530023292737705*^9, 3.5300232987185163`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Simplify", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"p1", "-", "pe", "-", "c"}], ",", " ", 
     RowBox[{"p2", "-", "pe", "-", 
      RowBox[{"c", " ", 
       RowBox[{"sig2", "^", "r"}]}]}], ",", 
     RowBox[{"p3", "-", "pe", "-", 
      RowBox[{"c", " ", 
       RowBox[{"sig3", "^", "r"}]}]}]}], "}"}], "/.", "sol"}], "]"}]], "Input",\

 CellChangeTimes->{{3.530023181432376*^9, 3.530023266329344*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0.`", ",", "0.`", ",", "0.`"}], "}"}]], "Output",
 CellChangeTimes->{{3.530023195601019*^9, 3.530023207428129*^9}, {
   3.5300232536017113`*^9, 3.530023267135253*^9}, 3.53088776468381*^9, 
   3.530887930859316*^9, {3.530888760118038*^9, 3.5308887911979322`*^9}, 
   3.530889212256976*^9, 3.530889269803895*^9, {3.530889303103174*^9, 
   3.530889326413721*^9}, 3.5308897487964163`*^9, 3.53088985305099*^9, 
   3.530889892178959*^9, 3.530894238012212*^9, 3.532797786744748*^9, 
   3.5327981014400187`*^9, 3.532799389351651*^9, 3.53279948175385*^9, 
   3.532799830194409*^9, 3.532800236461116*^9}]
}, Open  ]]
}, Open  ]]
},
WindowSize->{740, 867},
WindowMargins->{{Automatic, 792}, {Automatic, 286}},
FrontEndVersion->"8.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (February 23, \
2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 609, 20, 281, "Subsubtitle"],
Cell[1191, 44, 430, 10, 27, "Input"],
Cell[1624, 56, 255, 6, 27, "Input"],
Cell[1882, 64, 245, 5, 27, "Input"],
Cell[2130, 71, 419, 8, 27, "Input"],
Cell[2552, 81, 626, 17, 27, "Input"],
Cell[CellGroupData[{
Cell[3203, 102, 175, 4, 27, "Input"],
Cell[3381, 108, 873, 13, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4291, 126, 173, 4, 27, "Input"],
Cell[4467, 132, 860, 13, 27, "Output"]
}, Open  ]],
Cell[5342, 148, 380, 7, 43, "Input"],
Cell[CellGroupData[{
Cell[5747, 159, 931, 23, 27, "Input"],
Cell[6681, 184, 1499, 30, 60, "Output"]
}, Open  ]],
Cell[8195, 217, 321, 6, 27, "Input"],
Cell[CellGroupData[{
Cell[8541, 227, 946, 17, 27, "Input"],
Cell[9490, 246, 1117, 17, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10644, 268, 119, 2, 27, "Input"],
Cell[10766, 272, 191, 2, 30, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10994, 279, 711, 13, 27, "Input"],
Cell[11708, 294, 7159, 122, 229, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[18904, 421, 169, 4, 27, "Input"],
Cell[19076, 427, 590, 9, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[19703, 441, 323, 10, 27, "Input"],
Cell[20029, 453, 570, 8, 27, "Output"]
}, Open  ]],
Cell[20614, 464, 161, 3, 27, "Input"],
Cell[CellGroupData[{
Cell[20800, 471, 449, 13, 27, "Input"],
Cell[21252, 486, 634, 10, 27, "Output"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
