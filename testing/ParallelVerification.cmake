#
# FVM: Navier-Stokes
#
add_hydra_test(
  "FVM_INS_2D_Porous_Channel"
  "Hydra Hybrid Navier-Stokes Parallel Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/porous_channel" 4 ON)
add_hydra_test(
  "FVM_INS_2D_Beavers-Joseph_Porous_Channel"
  "Hydra Hybrid Navier-Stokes Parallel Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/porous_Beavers-Joseph" 4 ON)
add_hydra_test(
  "FVM_INS_2D_Beavers-Joseph_Porous_Channel_Fully_Implicit_verification"
  "Hydra Hybrid Navier-Stokes Parallel Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/porous_Beavers-Joseph" 4 ON)
add_hydra_test(
  "FVM_INS_2D_Poiseuille_flow_FI-P2"
  "Hydra Hybrid Navier-Stokes Parallel Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/poiseuille" 4 ON)
add_hydra_test(
  "FVM_INS_2D_Poiseuille_flow_FI-P2_small_dtmax"
  "Hydra Hybrid Navier-Stokes Parallel Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/poiseuille" 4 ON)
add_hydra_test(
  "FVM_INS_2D_Poiseuille_flow_FI-P2_large_dtmax"
  "Hydra Hybrid Navier-Stokes Parallel Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/poiseuille" 4 ON)
add_hydra_test(
  "FVM_INS_2D_k-epsilon_grid_turbulence_SI-P2"
  "Hydra Hybrid Navier-Stokes Parallel Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/grid_turbulence" 4 ON)
add_hydra_test(
  "FVM_INS_2D_k-epsilon_grid_turbulence_FI-P2_small_dtmax"
  "Hydra Hybrid Navier-Stokes Parallel Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/grid_turbulence" 4 ON)
add_hydra_test(
  "FVM_INS_2D_k-epsilon_grid_turbulence_FI-P2_large_dtmax"
  "Hydra Hybrid Navier-Stokes Parallel Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/grid_turbulence" 4 ON)
add_hydra_test(
  "FVM_INS_2D_Blasius_solution"
  "Hydra Hybrid Navier-Stokes Parallel Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/blasius" 4 ON)
