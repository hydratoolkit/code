#
# FVM: Navier-Stokes
#
add_hydra_test(
  "FVM_INS_3D_Single_Pin_RNG_k-e_Fully-Implicit"
  "Hydra Hybrid Navier-Stokes Large Regression"
  "${HYDRA_LONG_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/single_pin" 96 ON)
