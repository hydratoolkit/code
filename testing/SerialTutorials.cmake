#
# FEM: Conduction Heat Transfer
#
add_hydra_test(
  "FEM_HT_block-singlemat"
  "Hydra Serial Tutorials"
  "${HYDRA_TUTORIALS_DIR}/FEM/Conduction/material-sets" 1)
add_hydra_test(
  "FEM_HT_block-threemat"
  "Hydra Serial Tutorials"
  "${HYDRA_TUTORIALS_DIR}/FEM/Conduction/material-sets" 1)
#
# FEM: RigidBodyDynamics
#
add_hydra_test(
  "FEM_RBD_Millennium_Falcon"
  "Hydra Serial Tutorials"
  "${HYDRA_TUTORIALS_DIR}/FEM/RigidBodyDynamics/millennium-falcon" 1)
#
# FVM: Navier-Stokes
#
add_hydra_test(
  "FVM_INS_3D_mixing_elbow"
  "Hydra Serial Tutorials"
  "${HYDRA_TUTORIALS_DIR}/FVM/CCIncNavierStokes/elbow" 1)
add_hydra_test(
  "FVM_INS_2D_vortex-shedding"
  "Hydra Serial Tutorials"
  "${HYDRA_TUTORIALS_DIR}/FVM/CCIncNavierStokes/vortex-shedding" 1)
