#
# FVM: Navier-Stokes
#
add_hydra_test(
  "FVM_INS_2D_k-epsilon_vortex_shedding"
  "Hydra Hybrid Navier-Stokes Long Regression"
  "${HYDRA_LONG_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding" 8)
add_hydra_test(
  "FVM_INS_3D_Ahmed_body_RNG_k-e"
  "Hydra Hybrid Navier-Stokes Long Regression"
  "${HYDRA_LONG_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/ahmed" 8)
set_tests_properties(FVM_INS_3D_Ahmed_body_RNG_k-e PROPERTIES TIMEOUT 2400)
add_hydra_test(
  "FVM_INS_3D_Ahmed_body_STD_k-e"
  "Hydra Hybrid Navier-Stokes Long Regression"
  "${HYDRA_LONG_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/ahmed" 8)
set_tests_properties(FVM_INS_3D_Ahmed_body_STD_k-e PROPERTIES TIMEOUT 2400)
add_hydra_test(
  "FVM_INS_3D_Ahmed_body_Spalart-Allmaras"
  "Hydra Hybrid Navier-Stokes Long Regression"
  "${HYDRA_LONG_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/ahmed" 8)
set_tests_properties(FVM_INS_3D_Ahmed_body_Spalart-Allmaras PROPERTIES TIMEOUT 2400)
add_hydra_test(
  "FVM_INS_3D_k-epsilon_lid-driven_cavity"
  "Hydra Hybrid Navier-Stokes Long Regression"
  "${HYDRA_LONG_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/lid_driven_cavity" 8)
add_hydra_test(
  "FVM_INS_3D_full_vessel"
  "Hydra Hybrid Navier-Stokes Long Regression"
  "${HYDRA_LONG_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/full_vessel" 8)
set_tests_properties(FVM_INS_3D_full_vessel PROPERTIES TIMEOUT 4000)
# add_hydra_test(
#   "FVM_INS_3D_heatedtube"
#  "ZEZO Hydra Hybrid Navier-Stokes Long Regression"
#  "${HYDRA_LONG_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/heatedtube" 8)
