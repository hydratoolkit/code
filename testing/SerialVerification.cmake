#
# FVM: Navier-Stokes
#
# The FVM_INS_2D_Porous_Differentially_Heated_Cavity test takes longer than
# 10 minutes so we need to manually specify a longer timeout for this test.
add_hydra_test(
  "FVM_INS_2D_Porous_Differentially_Heated_Cavity"
  "Hydra Hybrid Navier-Stokes Serial Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/porous_diff_heated_cavity" 1)
set_tests_properties(
  FVM_INS_2D_Porous_Differentially_Heated_Cavity PROPERTIES TIMEOUT 1800)
add_hydra_test(
  "FVM_INS_2D_Porous_Channel"
  "Hydra Hybrid Navier-Stokes Serial Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/porous_channel" 1)
add_hydra_test(
  "FVM_INS_2D_Beavers-Joseph_Porous_Channel"
  "Hydra Hybrid Navier-Stokes Serial Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/porous_Beavers-Joseph" 1)
add_hydra_test(
  "FVM_INS_2D_Beavers-Joseph_Porous_Channel_Fully_Implicit_verification"
  "Hydra Hybrid Navier-Stokes Serial Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/porous_Beavers-Joseph" 1)
add_hydra_test(
  "FVM_INS_2D_Poiseuille_flow_SI-P2"
  "Hydra Hybrid Navier-Stokes Serial Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/poiseuille" 1)
add_hydra_test(
  "FVM_INS_2D_Poiseuille_flow_FI-P2_small_dtmax"
  "Hydra Hybrid Navier-Stokes Serial Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/poiseuille" 1)
add_hydra_test(
  "FVM_INS_2D_Poiseuille_flow_FI-P2_large_dtmax"
  "Hydra Hybrid Navier-Stokes Serial Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/poiseuille" 1)
add_hydra_test(
  "FVM_INS_2D_k-epsilon_grid_turbulence_SI-P2"
  "Hydra Hybrid Navier-Stokes Serial Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/grid_turbulence" 1)
add_hydra_test(
  "FVM_INS_2D_k-epsilon_grid_turbulence_FI-P2_small_dtmax"
  "Hydra Hybrid Navier-Stokes Serial Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/grid_turbulence" 1)
add_hydra_test(
  "FVM_INS_2D_k-epsilon_grid_turbulence_FI-P2_large_dtmax"
  "Hydra Hybrid Navier-Stokes Serial Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/grid_turbulence" 1)
add_hydra_test(
  "FVM_INS_2D_Blasius_solution"
  "Hydra Hybrid Navier-Stokes Serial Verification"
  "${HYDRA_VERIFICATION_DIR}/FVM/CCIncNavierStokes/2D/blasius" 1)
