#
# FEM: Conduction Heat Transfer
#
#
# FEM: RigidBodyDynamics
#
#
# FVM: Navier-Stokes
#
add_hydra_test(
  "FVM_INS_3D_mixing_elbow"
  "Hydra Parallel Tutorials"
  "${HYDRA_TUTORIALS_DIR}/FVM/CCIncNavierStokes/elbow" 4 ON)
add_hydra_test(
  "FVM_INS_2D_conjugate-heat-transfer"
  "Hydra Parallel Tutorials"
  "${HYDRA_TUTORIALS_DIR}/FVM/CCIncNavierStokes/conjugate-heat-transfer" 8)
# add_hydra_test(
#   "FVM_INS_3D_les-statistics"
#   "Test Hydra Parallel Tutorials"
#   "${HYDRA_TUTORIALS_DIR}/FVM/CCIncNavierStokes/les-statistics" 8 ON)
# add_hydra_test(
#   "FVM_INS_3D_ahmed-body"
#   "Hydra Parallel Tutorials"
#   "${HYDRA_TUTORIALS_DIR}/FVM/CCIncNavierStokes/ahmed" 8)
# set_tests_properties(FVM_INS_3D_ahmed-body PROPERTIES TIMEOUT 3600)
add_hydra_test(
  "FVM_INS_3D_catalytic-converter"
  "Hydra Parallel Tutorials"
  "${HYDRA_TUTORIALS_DIR}/FVM/CCIncNavierStokes/catalytic-converter" 8)
set_tests_properties(FVM_INS_3D_catalytic-converter PROPERTIES TIMEOUT 4500)
