#
# FEM: Advection-Diffusion
#
# add_hydra_test(
#   "FEM_Advection-Diffusion_Test-1"
#   "Hydra Serial Regression"
#   "${HYDRA_REGRESSION_DIR}/FEM/AdvectionDiffusion/2D/basic" 1)
#
# FEM: Heat Conduction
#
add_hydra_test(
  "FEM_Conduction_2D_ell"
  "Hydra FEM Conduction Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FEM/Conduction/2D/ell" 1)
add_hydra_test(
  "FEM_Conduction_2D_box_w._heat_flux"
  "Hydra FEM Conduction Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FEM/Conduction/2D/box" 1)
add_hydra_test(
  "FEM_Conduction_3D_1-block"
  "Hydra FEM Conduction Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FEM/Conduction/3D/block" 1)
add_hydra_test(
  "FEM_Conduction_3D_8-block"
  "Hydra FEM Conduction Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FEM/Conduction/3D/8block" 1)
add_hydra_test(
  "FEM_Conduction_3D_block_heat_flux_on_tets"
  "Hydra FEM Conduction Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FEM/Conduction/3D/block" 1)
add_hydra_test(
  "FEM_Conduction_3D_PYR5_element_test"
  "Hydra FEM Conduction Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FEM/Conduction/3D/box_pyr5" 1)
add_hydra_test(
  "FEM_Conduction_3D_WEDGE6_element_test"
  "Hydra FEM Conduction Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FEM/Conduction/3D/step_wedge" 1)
#
# FEM: Rigid Body Dynamics
#
add_hydra_test(
  "FEM_Rigid_Body_Dynamics_3D_cannon_ball"
  "Hydra FEM Rigid Body Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FEM/RigidBodyDynamics/3D/cannon_ball" 1)
add_hydra_test(
  "FEM_Rigid_Body_Dynamics_3D_cone_test"
  "Hydra FEM Rigid Body Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FEM/RigidBodyDynamics/3D/cone" 1)
#
# FVM: Heat Conduction
#
add_hydra_test(
  "FVM_Conduction_3D_1-block"
  "Hydra FVM Conduction Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCConduction/3D/block" 1)
add_hydra_test(
  "FVM_Conduction_3D_manifold"
  "Hydra FVM Conduction Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCConduction/3D/manifold" 1)
#
# FVM: Advection/Burgers Eq.
#
add_hydra_test(
  "FVM_Advection_tri-mesh1"
  "Hydra FVM Advection Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCAdvection/2D/basic" 1)
#
# FVM: Euler Eq.s
#
add_hydra_test(
  "FVM_Euler_2D_Emery_problem_-_coarse_mesh"
  "Hydra FVM Euler Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCEuler/2D/emery" 1)
add_hydra_test(
  "FVM_Euler_2D_Emery_problem_-_normal_BCs"
  "Hydra FVM Euler Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCEuler/2D/emery" 1)
add_hydra_test(
  "FVM_Euler_2D_8:1_shock_tube"
  "Hydra FVM Euler Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCEuler/2D/sod" 1)
add_hydra_test(
  "FVM_Euler_2D_rotated_8:1_shock_tube"
  "Hydra FVM Euler Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCEuler/2D/sod_rot" 1)
add_hydra_test(
  "FVM_Euler_3D_airfoil"
   "Hydra FVM Euler Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCEuler/3D/airfoil" 1)
add_hydra_test(
  "FVM_Euler_3D_cylinder_flow"
   "Hydra FVM Euler Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCEuler/3D/cylinder" 1)
add_hydra_test(
  "FVM_Euler_3D_double_mach"
   "Hydra FVM Euler Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCEuler/3D/double_mach" 1)
add_hydra_test(
  "FVM_Euler_3D_sod1"
   "Hydra FVM Euler Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCEuler/3D/sod" 1)
#
# FVM: Navier-Stokes basic tests
#
add_hydra_test(
  "FVM_INS_2D_Coarse_BFS_k-e_model"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/bfs" 1)
add_hydra_test(
  "FVM_INS_2D_Short_BFS_k-e_model_-bad_test-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/bfs" 1)
add_hydra_test(
  "FVM_INS_2D_Short_BFS_S-A_model_-bad_test-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/bfs" 1)
add_hydra_test(
  "FVM_INS_2D_boundary_layer"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/blayer" 1)
add_hydra_test(
  "FVM_INS_2D_body-force_test-a"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/body_force" 1)
add_hydra_test(
  "FVM_INS_2D_body-force_test-b"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/body_force" 1)
add_hydra_test(
  "FVM_INS_2D_body-force_test-c"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/body_force" 1)
add_hydra_test(
  "FVM_INS_2D_Brake_valve_time_dependent_pressure"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/brake_valve" 1)
add_hydra_test(
  "FVM_INS_2D_heat_source_test"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/heat_source" 1)
add_hydra_test(
  "FVM_INS_2D_circular_bump_k-e_model"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/bump" 1)
add_hydra_test(
  "FVM_INS_2D_circular_bump_S-A_model"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/bump" 1)
add_hydra_test(
  "FVM_INS_2D_fixed_pressure_gradient_channel_k-e_model"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/channel" 1)
add_hydra_test(
  "FVM_INS_2D_fixed_pressure_gradient_channel_S-A_model"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/channel" 1)
add_hydra_test(
  "FVM_INS_2D_fixed_pressure_gradient_Pr=1_channel_k-e_model"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/channel" 1)
add_hydra_test(
  "FVM_INS_2D_fixed_pressure_gradient_Pr=10_channel_k-e_model"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/channel" 1)
add_hydra_test(
  "FVM_INS_2D_Poiseuille_flow_pressure-drop"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/poiseuille" 1)
add_hydra_test(
  "FVM_INS_2D_Poiseuille_flow_pressure-drop_fully-implicit"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/poiseuille" 1)
add_hydra_test(
  "FVM_INS_2D_Couette_flow_x-shear"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/couette" 1)
add_hydra_test(
  "FVM_INS_2D_Couette_flow_y-shear"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/couette" 1)
add_hydra_test(
  "FVM_INS_2D_Couette_flow_z-shear"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/couette" 1)
add_hydra_test(
  "FVM_INS_2D_Re=100_flow_past_circular_cylinder_-SI_P2_projection_pressure-gradient_form-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding" 1)
add_hydra_test(
  "FVM_INS_2D_Re=100_cylinder_flow_FI_Picard_pressure_gradient_scheme"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding/Picard/test.0" 1)
add_hydra_test(
  "FVM_INS_2D_Re=100_cylinder_FI_Picard_pressure_gradient_scheme"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding/Picard/test.1" 1)
add_hydra_test(
  "FVM_INS_2D_Re=100_flow_w._temperature"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding" 1)
add_hydra_test(
  "FVM_INS_2D_Re=100_flow_w._temperature:_internal_energy_formulation"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding/Int.Energy" 1)
add_hydra_test(
  "FVM_INS_2D_Re=100_flow_w._heat_flux"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding" 1)
add_hydra_test(
  "FVM_INS_2D_Re=10000_k-e_coarse_mesh"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding" 1)
add_hydra_test(
  "FVM_INS_2D_Re=10000_Spalart-Allmaras_coarse_mesh"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding" 1)
add_hydra_test(
  "FVM_INS_2D_Re=10000_Spalart-Allmaras_coarse_mesh:_internal_energy_formulation"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding/Int.Energy" 1)
add_hydra_test(
  "FVM_INS_2D_Cylinder_flow_Spalart-Allmaras_DES_mode"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding" 1)
add_hydra_test(
  "FVM_INS_2D_Cylinder_flow_Spalart-Allmaras_DES_model:_internal_energy_formulation"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding/Int.Energy" 1)
add_hydra_test(
  "FVM_INS_2D_Cylinder_flow_Smagorinsky_model"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding" 1)
add_hydra_test(
  "FVM_INS_2D_Cylinder_flow_WALE_model"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding" 1)
add_hydra_test(
  "FVM_INS_2D_Cylinder_flow_WALE_model:_internal_energy_formulation"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding/Int.Energy" 1)
add_hydra_test(
  "FVM_INS_2D_Cylinder_flow:_inst_and_stat_output"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding" 1)
add_hydra_test(
  "FVM_INS_2D_Re=100_flow_w._enthalpy"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding/Enthalpy" 1)
add_hydra_test(
  "FVM_INS_2D_Re=10000_Spalart-Allmaras_w._enthalpy"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding/Enthalpy" 1)
add_hydra_test(
  "FVM_INS_2D_Re=10000_K-epsilon_w._enthalpy"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/vortex_shedding/Enthalpy" 1)
add_hydra_test(
  "FVM_INS_2D_Momentum-SA_failure"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/error_handling" 1)
add_hydra_test(
  "FVM_INS_2D_Momentum-XZ_failure"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/error_handling" 1)
add_hydra_test(
  "FVM_INS_2D_PPE_convergence_failure"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/error_handling" 1)
add_hydra_test(
  "FVM_INS_2D_Ra=1.0e+4_Pr=1_differentially_heated_box"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/diff_heated_box" 1)
add_hydra_test(
  "FVM_INS_2D_Ra=1.0e+4_Pr=1_differentially_heated_box_-SSOR-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/diff_heated_box" 1)
add_hydra_test(
  "FVM_INS_2D_Ra=1.0e+4_Pr=1_differentially_heated_box_FI_Picard_with_temperature_formulation"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/diff_heated_box/Temperature.FImplicit" 1)
add_hydra_test(
  "FVM_INS_2D_Ra=1.0e+4_Pr=1_differentially_heated_box_FI_Picard_with_temperature_oscillations"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/diff_heated_box/Oscillations" 1)
add_hydra_test(
  "FVM_INS_3D_Ra=1.0e+4_Pr=1_differentially_heated_box"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/diff_heated_box" 1)
add_hydra_test(
  "FVM_INS_3D_Ra=1.0e+4_Pr=1_differentially_heated_box_--_PYR5_element"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/diff_heated_box" 1)
add_hydra_test(
  "FVM_INS_2D_Differentially_Heated_Box_w._enthalpy_Ra=1.e4_Pr=0.71_Cp=0.71"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/diff_heated_box/Enthalpy" 1)
add_hydra_test(
  "FVM_INS_2D_forward_step_wedges_k-e"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/forward_step" 1)
add_hydra_test(
  "FVM_INS_2D_forward_step_wedges_S-A"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/forward_step" 1)
add_hydra_test(
  "FVM_INS_2D_Momentum_Jet_Passive_Outflow_BC"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/momentum_jet" 1)
add_hydra_test(
  "FVM_INS_2D_k-epsilon_grid_turbulence_test"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/grid_turbulence" 1)
add_hydra_test(
  "FVM_INS_2D_15-deg_Re=100_lid_driven_cavity_-BE-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_2D_15-deg_Re=100_lid_driven_cavity_-TR-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_2D_30-deg_Re=100_lid_driven_cavity_-BE-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_2D_30-deg_Re=100_lid_driven_cavity_-TR-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_2D_45-deg_Re=100_lid_driven_cavity_-BE-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_2D_45-deg_Re=100_lid_driven_cavity_-TR-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_2D_60-deg_Re=100_lid_driven_cavity_-BE-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_2D_60-deg_Re=100_lid_driven_cavity_-TR-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_2D_90-deg_Re=100_lid_driven_cavity_-BE-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_2D_90-deg_Re=100_lid_driven_cavity_-TR-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_2D_90-deg_Re=100_lid_driven_cavity_hydrostat_test_-BE-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_2D_90-deg_Re=100_lid_driven_cavity_w._energy_eq._-TR-"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/lid_driven_cavity" 1)
#
# FVM: Navier-Stokes - Conjugate Heat Transfer
#
add_hydra_test(
  "FVM_INS_2D_ldc_cht"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/CHT" 1)
add_hydra_test(
  "FVM_INS_2D_poiseuille_rigid"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/2D/CHT" 1)
#
# FVM: Navier-Stokes - Boundary Conditions
#
add_hydra_test(
  "FVM_INS_2D_Euler_time-dependent_velocity_BC"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/BCs/euler_vbc" 1)
add_hydra_test(
  "FVM_INS_2D_cylinder_Re=100_mass_flow_BC"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/BCs/mass_flow" 1)
add_hydra_test(
  "FVM_INS_2D_cylinder_Re=100_mass_flux_BC"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/BCs/mass_flux" 1)
add_hydra_test(
  "FVM_INS_2D_cylinder_Re=100_volume_flow_BC"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/BCs/volume_flow" 1)
# add_hydra_test(
#   "FVM_INS_3D_merged_sets"
#   "Hydra Hybrid Navier-Stokes Serial Regression"
#   "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/BCs/merged_sets" 1)
add_hydra_test(
  "FVM_INS_3D_pipe_pvdep_BC"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/BCs/pvdep" 1)
#
# FVM: Navier-Stokes - 3D tests
#
add_hydra_test(
  "FVM_INS_3D_elbow_polyprop"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/elbow-polyprop" 1)
add_hydra_test(
  "FVM_INS_3D_Lid-driven_cavity_ILES_inst_and_stat_output"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_3D_Lid-driven_cavity_WALE_inst_and_stat_output"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_3D_laminar_lid-driven_cavity_PYR5_elements"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_3D_lid-driven_cavity_RNG_k-e"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_3D_lid-driven_cavity_Spalart-Allmaras"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_3D_lid-driven_cavity_Spalart-Allmaras_PYR5_element"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_3D_lid-driven_cavity_WALE_Picard"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/lid_driven_cavity" 1)
add_hydra_test(
  "FVM_INS_3D_post_n_plate"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/post_n_plate" 1)
add_hydra_test(
  "FVM_INS_3D_post_n_plate_w._symmetry_BCs"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/3D/post_n_plate" 1)
#
# FVM: Lagrangian - Basic Tests
#
add_hydra_test(
  "FVM_CCL_2D_saltzmann"
  "Hydra Cell-Centered Lagrangian Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCLagrangian/2D/saltzmann" 1)
add_hydra_test(
  "FVM_CCL_2D_sod_a"
  "Hydra Cell-Centered Lagrangian Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCLagrangian/2D/sod" 1)
#
# FVM: Navier-Stokes - User Defined Functions
#
add_hydra_test(
  "FVM_INS_2D_Poiseuille_User_Functions"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/UserFunctions/poiseuille" 1)
add_hydra_test(
  "FVM_INS_2D_HeatSource_User_Functions"
  "Hydra Hybrid Navier-Stokes Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCIncNavierStokes/UserFunctions/heat-source" 1)
#
# FVM: Multiphase
#
add_hydra_test(
  "FVM_MultiPhase_Poiseuille_nfields=1_prescribed_pressure_gradient"
  "Hydra Hybrid Multiphase Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCMultiPhase/2D/Poiseuille" 1)
add_hydra_test(
  "FVM_MultiPhase_Poiseuille_nfields=2_prescribed_pressure_gradient"
  "Hydra Hybrid Multiphase Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCMultiPhase/2D/Poiseuille" 1)
add_hydra_test(
  "FVM_MultiPhase_Poiseuille_prescribed_inlet_velocity"
  "Hydra Hybrid Multiphase Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCMultiPhase/2D/Poiseuille" 1)
add_hydra_test(
  "FVM_MultiPhase_Poiseuille_time-dependentconstant_inlet_velocity"
  "Hydra Hybrid Multiphase Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCMultiPhase/2D/Poiseuille" 1)
add_hydra_test(
  "FVM_MultiPhase_Poiseuille_prescribed_inlet_volume_fraction"
  "Hydra Hybrid Multiphase Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCMultiPhase/2D/Poiseuille" 1)
add_hydra_test(
  "FVM_MultiPhase_Euler_prescribed_inlet_volume_fraction"
  "Hydra Hybrid Multiphase Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCMultiPhase/2D/Poiseuille" 1)
add_hydra_test(
  "FVM_MultiPhase_Vortex_shedding"
  "Hydra Hybrid Multiphase Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCMultiPhase/2D/vortex_shedding" 1)
add_hydra_test(
  "FVM_MultiPhase_Vortex_shedding_--_restart"
  "Hydra Hybrid Multiphase Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCMultiPhase/2D/vortex_shedding" 1)
add_hydra_test(
  "FVM_MultiPhase_Poiseuille_N=2_const_drag"
  "Hydra Hybrid Multiphase Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCMultiPhase/2D/Poiseuille" 1)
add_hydra_test(
  "FVM_MultiPhase_Poiseuille_N=2_const_drag_unequal_volfrac"
  "Hydra Hybrid Multiphase Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCMultiPhase/2D/Poiseuille" 1)
add_hydra_test(
  "FVM_MultiPhase_Poiseuille_N=5_const_drag_5-field_decay"
  "Hydra Hybrid Multiphase Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCMultiPhase/2D/Poiseuille" 1)
add_hydra_test(
  "FVM_MultiPhase_Poiseuille_N=2_const_drag_terminal_velocity"
  "Hydra Hybrid Multiphase Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCMultiPhase/2D/body_force" 1)
add_hydra_test(
  "FVM_MultiPhase_2D_Couette_flow_x-shear_const_lift"
  "Hydra Hybrid Multiphase Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FVM/CCMultiPhase/2D/couette" 1)
#
# Utility Tests
#
add_hydra_test(
  "ex2msh_and_ascii_reader"
  "Hydra Utility Serial Regression"
  "${HYDRA_REGRESSION_DIR}/FEM/Conduction/2D/box" 1)

