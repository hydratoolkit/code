Export Compliance Information

Computational Sciences International, LLC is based in the United
States of America and all products are developed in the U.S or via
online collaboration in public forums and distributed from within the
U.S.  As a result, U.S. export laws and regulations apply to the
distribution of Computational Sciences International products, and
remain in force as products and technology are re-exported to
different parties and places around the world.

IF YOU EXPORT COMPUTATIONAL SCIENCES INTERNATIONAL PRODUCTS, YOU ARE
RESPONSIBLE FOR COMPLYING WITH THE REQUIREMENTS OF THE BUREAU OF
INDUSTRY AND SECURITY ("BIS"), EXPORT ADMINISTRATION REGULATIONS
("EAR"), OR OTHER U.S. EXPORT LAWS.

Classification Matrix

The Computational Sciences International Product and Services
Classification Matrix ("Matrix") is a general listing of Computational
Sciences International software products and services.  The matrix is
to be used in conjunction with the Export Administration Regulations
("EAR") in order to assist our customers in the export of
Computational Sciences International products.  All classification
information contained in the matrix is subject to change without
notice.

The Matrix is not intended to replace the EAR and other U.S. Export
laws, but is provided as an accommodation to our customers to be used
in conjunction with said laws to assist in the export of Computational
Sciences International products and services. The exporter is
responsible for exporting Computational Sciences International
products, in accordance with the requirements of the EAR and other
U.S. Export laws. End-user, end-use and country of ultimate
destination may affect export licensing requirements. All Export
Control Classification Numbers ("ECCN"), HTS Numbers and License
Authorization information are subject to change without
notice. Modification in any way to a Computational Sciences
International product voids the classification. It is your obligation
as an exporter to verify such information and comply with the current
applicable regulations.

Computational Sciences International makes no warranty or
representation that the information contained on this site is
accurate, current, or complete. It is your obligation as the exporter
to comply with the current applicable requirements of United States
export rules and regulations. Any use of such information by you is
without recourse to Computational Sciences International and is at
your own risk. Computational Sciences International is in no way
responsible for any damages whether direct, consequential, incidental,
or otherwise, suffered by you as a result of using or relying upon
such information for any purpose.
